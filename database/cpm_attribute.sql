-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 24, 2019 at 12:51 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_lite`
--

-- --------------------------------------------------------

--
-- Table structure for table `cpm_attribute`
--

DROP TABLE IF EXISTS `cpm_attribute`;
CREATE TABLE IF NOT EXISTS `cpm_attribute` (
  `attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_code` varchar(32) NOT NULL,
  `attribute_name` varchar(255) DEFAULT NULL,
  `group_code` varchar(128) DEFAULT NULL,
  `parent_attr_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `input` varchar(10) NOT NULL,
  `options` longtext NOT NULL,
  `default_value` int(11) NOT NULL,
  PRIMARY KEY (`attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cpm_attribute`
--

INSERT INTO `cpm_attribute` (`attribute_id`, `attribute_code`, `attribute_name`, `group_code`, `parent_attr_id`, `sort_order`, `input`, `options`, `default_value`) VALUES
(1, '', 'Hiện trạng', 'du1', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(2, '', 'Vị trí', 'du1', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(3, '', 'Dù đứng được', 'du1', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(4, '', 'Tình Trạng', 'du1', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(5, '', 'Ghi chú', 'du1', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(6, '', 'Hiện trạng', 'du2', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(7, '', 'Vị trí', 'du2', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(8, '', 'Dù đứng được', 'du2', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(9, '', 'Tình Trạng', 'du2', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(10, '', 'Ghi chú', 'du2', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(11, '', 'Hiện trạng', 'du3', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(12, '', 'Vị trí', 'du3', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(13, '', 'Dù đứng được', 'du3', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(14, '', 'Tình Trạng', 'du3', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(15, '', 'Ghi chú', 'du3', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(16, '', 'Hiện trạng', 'du4', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(17, '', 'Vị trí', 'du4', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(18, '', 'Dù đứng được', 'du4', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(19, '', 'Tình trạng', 'du4', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(20, '', 'Ghi chú', 'du4', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(21, '', 'Hiện trạng', 'du5', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(22, '', 'Vị trí', 'du5', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(23, '', 'Dù đứng được', 'du5', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(24, '', 'Tình trạng ', 'du5', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(25, '', 'Ghi chú', 'du5', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(26, '', 'Hiện trạng', 'du6', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(27, '', 'Vị trí', 'du6', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(28, '', 'Dù đứng được', 'du6', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(29, '', 'Tình trạng ', 'du6', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(30, '', 'Ghi chú', 'du6', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(31, '', 'Hiện trạng', 'du7', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(32, '', 'Vị trí', 'du7', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(33, '', 'Dù đứng được', 'du7', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(34, '', 'Tình trạng ', 'du7', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(35, '', 'Ghi chú', 'du7', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(36, '', 'Hiện trạng ', 'du8', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(37, '', 'Vị trí', 'du8', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(38, '', 'Dù đứng được', 'du8', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(39, '', 'Tình trạng ', 'du8', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(40, '', 'Ghi chú', 'du8', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(41, '', 'Hiện trạng', 'du9', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(42, '', 'Vị trí', 'du9', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(43, '', 'Dù đứng được', 'du9', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(44, '', 'Tình trạng ', 'du9', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(45, '', 'Ghi chú', 'du9', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(46, '', 'Hiện trạng', 'du10', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(47, '', 'Vị trí', 'du10', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(48, '', 'Dù đứng được', 'du10', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(49, '', 'Tình trạng ', 'du10', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(50, '', 'Ghi chú', 'du10', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(51, '', 'Hiện trạng', 'du11', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(52, '', 'Vị trí', 'du11', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(53, '', 'Dù đứng được', 'du11', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(54, '', 'Tình trạng ', 'du11', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(55, '', 'Ghi chú', 'du11', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(56, '', 'Hiện trạng ', 'du12', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(57, '', 'Vị trí', 'du12', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(58, '', 'Dù đứng được', 'du12', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(59, '', 'Tình trạng ', 'du12', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(60, '', 'Ghi chú', 'du12', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(61, '', 'Hiện trạng', 'du13', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(62, '', 'Vị trí', 'du13', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(63, '', 'Dù đứng được', 'du13', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(64, '', 'Tình trạng ', 'du13', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(65, '', 'Ghi chú', 'du13', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(66, '', 'Hiện trạng', 'du14', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(67, '', 'Vị trí', 'du14', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(68, '', 'Dù đứng được', 'du14', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(69, '', 'Tình trạng ', 'du14', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(70, '', 'Ghi chú', 'du14', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(71, '', 'Hiện trạng', 'du15', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(72, '', 'Vị trí ', 'du15', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(73, '', 'Dù đứng được', 'du15', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(74, '', 'Tình trạng', 'du15', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(75, '', 'Ghi chú', 'du15', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(76, '', 'Hiện trạng ', 'du16', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(77, '', 'trạng', 'du16', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(78, '', 'Dù đứng được', 'du16', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(79, '', 'Tình trạng ', 'du16', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(80, '', 'Ghi chú', 'du16', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(81, '', 'Hiện trạng', 'du17', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(82, '', 'Vị trí', 'du17', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(83, '', 'Dù đứng được', 'du17', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(84, '', 'Tình trạng', 'du17', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(85, '', 'Ghi chú', 'du17', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(86, '', 'Hiện trạng', 'du18', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(87, '', 'Vị trí', 'du18', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(88, '', 'Dù đứng được', 'du18', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(89, '', 'Tình trạng', 'du18', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(90, '', 'Ghi chú', 'du18', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(91, '', 'Hiện trạng', 'du19', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(92, '', 'Vị trí', 'du19', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(93, '', 'Dù đứng được', 'du19', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(94, '', 'Tình trạng', 'du19', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(95, '', 'Ghi chú', 'du19', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(96, '', 'Hiện trạng', 'du20', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(97, '', 'Vị trí', 'du20', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(98, '', 'Dù đứng được', 'du20', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(99, '', 'Tình trạng ', 'du20', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(100, '', 'Ghi chú', 'du20', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(101, '', 'Hiện trạng', 'du21', 0, 1, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(102, '', 'Vị trí', 'du21', 0, 2, 'select', '[{\"id\":\"1\",\"name\":\"Trong CH\"},{\"id\":\"2\",\"name\":\"Ngo\\u00e0i CH\"}]', 0),
(103, '', 'Dù đứng được', 'du21', 0, 3, 'select', '[{\"id\":\"1\",\"name\":\"C\\u00f3\"},{\"id\":\"2\",\"name\":\"Kh\\u00f4ng\"}]', 0),
(104, '', 'Tình trạng', 'du21', 0, 4, 'select', '[{\"id\":\"1\",\"name\":\"B\\u00ecnh th\\u01b0\\u1eddng\"},{\"id\":\"2\",\"name\":\"B\\u1ea1c M\\u00e0u\"},{\"id\":\"3\",\"name\":\"R\\u00e1ch do con ng\\u01b0\\u1eddi\\/ r\\u00e1ch to\"},{\"id\":\"4\",\"name\":\"R\\u00e1ch do nguy\\u00ean nh\\u00e2n kh\\u00e1ch quan\\/ r\\u00e1ch nh\\u1ecf\"}]', 0),
(105, '', 'Ghi chú', 'du21', 0, 5, 'select', '[{\"id\":\"1\",\"name\":\"Phai m\\u00e0u\"},{\"id\":\"2\",\"name\":\"V\\u00f5ng\\/gi\\u00e3n\"},{\"id\":\"3\",\"name\":\"C\\u1ea3 hai\"},{\"id\":\"4\",\"name\":\"Kh\\u00f4ng\"}]', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
