-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 24, 2019 at 12:54 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_lite`
--

-- --------------------------------------------------------

--
-- Table structure for table `cpm_attribute_group`
--

DROP TABLE IF EXISTS `cpm_attribute_group`;
CREATE TABLE IF NOT EXISTS `cpm_attribute_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_group_id` int(11) NOT NULL,
  `group_name` varchar(128) DEFAULT NULL,
  `group_code` varchar(128) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cpm_attribute_group`
--

INSERT INTO `cpm_attribute_group` (`group_id`, `parent_group_id`, `group_name`, `group_code`, `sort_order`) VALUES
(1, 0, 'MILO PARASOL', 'milo_parasol', 0),
(2, 1, 'Dù 1', 'du1', 1),
(3, 1, 'Dù 2', 'du2', 2),
(4, 1, 'Dù 3', 'du3', 3),
(5, 1, 'Dù 4', 'du4', 4),
(6, 1, 'Dù 5', 'du5', 5),
(7, 1, 'Dù 6', 'du6', 6),
(8, 1, 'Dù 7', 'du7', 7),
(9, 1, 'Dù 8', 'du8', 8),
(10, 1, 'Dù 9', 'du9', 9),
(11, 1, 'Dù 10', 'du10', 10),
(12, 1, 'Dù 11', 'du11', 11),
(13, 1, 'Dù 12', 'du12', 12),
(14, 1, 'Dù 13', 'du13', 13),
(15, 1, 'Dù 14', 'du14', 14),
(16, 1, 'Dù 15', 'du15', 15),
(17, 1, 'Dù 16', 'du16', 16),
(18, 1, 'Dù 17', 'du17', 17),
(19, 1, 'Dù 18', 'du18', 18),
(20, 1, 'Dù 19', 'du19', 19),
(21, 1, 'Dù 20', 'du20', 20),
(22, 1, 'Dù 21', 'du21', 21);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
