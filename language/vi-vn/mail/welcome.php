<?php
// Text
$_['text_subject']          = '%s - Welcome To CPM';
$_['text_greeting']         = 'Bạn đã đăng ký làm thành viên của CPM Việt Nam. Dưới đây là thông tin đăng nhập của bạn.';
$_['text_username']         = 'Username';
$_['text_password']         = 'Password';
$_['text_app_url']         = 'Download App';
$_['text_web_url']         = 'Đăng nhập vào website';
$_['text_footer']           = 'Nếu gặp vấn đề về tài khoản vui lòng gửi thông tin về email này để được hỗ trợ.';
