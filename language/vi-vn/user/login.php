<?php

// header
$_['heading_title']  = 'User login';

// Text
$_['text_heading']   = 'User login';
$_['text_login']     = 'Please enter your login details.';
$_['text_forgotten'] = 'Forgotten Password';

// Entry
$_['entry_username'] = 'Username';
$_['entry_password'] = 'Password';

// Button
$_['button_login']   = 'Login';

// Error
$_['text_error_login']    = 'No match for Username and/or Password.';
$_['text_error_token']    = 'Invalid token session. Please login again.';