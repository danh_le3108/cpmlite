<?php
// Heading
$_['heading_title']          = 'Nhóm';
$_['group_title']            = 'Nhân viên';
// Text
$_['text_success']           = 'Thành công: Sửa nhóm nhân viên thành công!';
$_['text_list']              = 'Nhóm';
$_['text_add']                = 'Thêm';
$_['text_edit']              = 'Sửa Nhóm';
$_['text_description']       = 'Mô tả';
$_['text_general']           = 'Thông tin chung';
// Column
$_['text_name']              = 'Tên nhóm';
$_['text_action']            = 'Thao tác';
// Entry
$_['text_system_permission'] = 'Quyền truy cập';
$_['text_tab_permission']    = 'Quyền truy cập';
$_['text_access_per']        = 'Xem';
$_['text_add_per']           = 'Thêm mới';
$_['text_edit_per']          = 'Sửa';
$_['text_delete_per']        = 'Xóa';
$_['text_read_per']        = 'Xem';
$_['text_download_per']        = 'Tải về';
// Error
$_['text_error_permission']  = 'Cảnh báo: You do not have permission to modify user groups!';
$_['text_error_name']        = 'User Group Name must be between 3 and 64 characters!';
$_['text_error_user']        = 'Cảnh báo: This user group cannot be deleted as it is currently assigned to %s users!';