<?php
// Text
$_['text_stores']           = 'Cửa hàng';
$_['text_store']            = 'Cửa hàng';
$_['text_store_type']       = 'Loại cửa hàng';
$_['text_audit_chart']     = 'Biểu đồ';


$_['text_user_group']     = 'Phân quyền nhóm';
$_['text_users']     = 'Nhân viên';
$_['text_system']           = 'Hệ thống';
$_['text_category']         = 'Danh mục';
$_['text_place_locality']   = 'Địa điểm chi tiết';
$_['text_project_type']     = 'Loại dự án';
$_['text_plans']            = 'Khảo sát';
$_['text_plan']             = 'Plan';
$_['text_filemanager']      = 'Filemanager';
$_['text_filemanager_plus'] = 'Filemanager+';
$_['text_plan_status']      = 'Plan status';
$_['text_image_type']       = 'Loại hình ảnh';
$_['text_plan_rating']      = 'Đánh giá plan';
$_['text_plan_position']    = 'Vị trí trưng bày';
$_['text_posm']        		= 'Vật phẩm trưng bày - Posm';
$_['text_survey']        	= 'Câu hỏi khảo sát';
$_['text_plan_map']         = 'Bản đồ';
$_['text_excel_template']   = 'Excel template';
$_['text_reports']   		= 'Báo cáo';
$_['text_plan_progess']   	= 'Tiến độ khảo sát';
$_['text_audit_info']   	= 'Báo cáo khách hàng';

