<?php
// Heading
$_['heading_title']       = 'Nhân viên KH';

// Text
$_['text_success']        = 'Thành công: Đã cập nhật Project Users!';
$_['text_list']           = 'Project Users List';
$_['text_edit']           = 'Edit Project Users';
$_['text_remove']         = 'Remove';
$_['text_route']          = 'Choose the store and routes to be used with this Project Users';
$_['text_module']         = 'Choose the position of the modules';
$_['text_default']        = 'Default';
$_['text_user']           = 'User';
$_['text_none_group_user']   = 'NV chưa phân nhóm';
$_['text_user_group']     = 'Role';
$_['text_add']     		= 'Thêm';
$_['text_add_user']     = 'Thêm nhân viên';
$_['text_add_group']     = 'Thêm nhóm';
$_['text_child_group']     = 'Child group';
$_['text_add_staff']     = 'Thêm staff';
$_['text_staff']     = '&raquo;';
$_['text_user']     = 'Nhân viên';
$_['text_group_name']     = 'Tên nhóm';
$_['text_description']     = 'Description';

// Quickadd
$_['text_fullname']     = 'Họ tên';
$_['text_identity_number']     = 'Số CMND';
$_['text_email']     = 'Email';
$_['text_phone']     = 'Điện thoại';
$_['text_workplace']     = 'Nơi làm việc';
$_['text_user_group']     = 'Nhóm';

// Column
$_['column_name']         = 'Dự án';
$_['column_action']       = 'Hành động';

// Entry
$_['entry_name']          = 'Dự án';
$_['entry_project']       = 'Dự án';
$_['entry_route']         = 'Route';
$_['entry_module']        = 'Module';

// Text
$_['text_success']               = 'Thành công: You have modified customer users!';
$_['text_group']                  = 'Nhóm';
$_['text_username']              = 'Username';
$_['text_fullname']              = 'Họ tên';
$_['text_email']              = 'Email';
$_['text_password']              = 'Mật khẩu';
$_['text_password_confirm']              = 'Nhập lại Mật khẩu';
$_['text_telephone']              = 'Telephone';
$_['text_status']              = 'Trạng thái';
$_['text_ip']              = 'IP';
$_['text_action']              = 'Hành động';

$_['text_filter']                = 'Search';
$_['help_filter']                = 'Support search: Username, Fullname, Email, Telephone, Code...';

// Error
$_['text_error_permission']      = 'Cảnh báo: Bạn không có quyền sửa!';
$_['text_error_name']        = 'Name  must be between 2 and 32 characters!';
$_['text_error_exists']          = 'Cảnh báo: E-Mail Address is already registered!';
$_['text_error_username_exists'] = 'Warning: Username is already registered!';
$_['text_error_fullname']        = 'Full Name must be between 1 and 32 characters!';
$_['text_error_lastname']        = 'Last Name must be between 1 and 32 characters!';
$_['text_error_email']           = 'E-Mail Address does not appear to be valid!';
$_['text_error_telephone']       = 'Telephone must be between 3 and 32 characters!';
$_['text_error_password']        = 'Password must be between 4 and 20 characters!';
$_['text_error_confirm']         = 'Password and password confirmation do not match!';