<?php
// Heading 
$_['heading_title']     = 'Sổ Địa Chỉ';

// Text
$_['text_account']      = 'Tài khoản';
$_['text_add']             = 'Bạn đăng vào thành công địa chỉ';
$_['text_edit']            = 'Bạn cập nhật thành công địa chỉ';
$_['text_insert']       = 'Địa chỉ của bạn đã thêm mới thành công';
$_['text_update']       = 'Địa chỉ của bạn đã cập nhật thành công';
$_['text_delete']       = 'Địa chỉ của bạn đã xóa thành công';
$_['text_empty']        = 'Địa chỉ của bạn vào hồ sơ bị khai thiếu.';

// Entry
$_['entry_firstname']   = 'Tên đầy đủ :';

// Error
$_['text_error_delete']      = 'Lỗi: bạn phải có ít nhất một địa chỉ!';
$_['text_error_default']     = 'Lỗi: Bạn không thể xóa địa chỉ mặc định!';
$_['text_error_firstname']   = 'Tên phải từ 1 đến 32 kí tự!';
?>