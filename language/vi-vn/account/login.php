<?php
// Heading 
$_['heading_title']                = 'Đăng nhập tài khoản';

// Text
$_['text_account']                 = 'Tài khoản';
$_['text_login']                   = 'Đăng nhập';
$_['text_new_customer']            = 'Khách hàng mới';
$_['text_register']                = 'Đăng kí tài khoản';
$_['text_register_account']        = 'Bằng cách tạo tài khoản bạn sẽ có thể mua sắm nhanh hơn, cập nhật tình trạng đơn hàng, theo dõi những đơn hàng đã đặt.';
$_['text_returning_customer']      = 'Khách hàng cũ';
$_['text_i_am_returning_customer'] = 'Tôi là khách hàng cũ';
$_['text_forgotten']               = 'Quên mật khẩu';

// Entry
$_['entry_username']                  = 'Username';
$_['entry_password']               = 'Mật khẩu:';

// Error
$_['text_error_login']                  = 'Lỗi: Email hoặc mật khẩu không chính xác.';
$_['text_error_approved']               = 'Thông báo: Tài khoản của bạn cần được kích hoạt trước rồi bạn mới đăng nhập được.'; 
$_['text_error_attempts']               = 'Warning: Your account has exceeded allowed number of login attempts. Please try again in 1 hour.';
