<?php
// Heading
$_['heading_title']           = 'Districts';
$_['group_title']           = 'System';

// Text
$_['text_Thành công']            = 'Thành công: You have modified Districts!';
$_['text_list']               = 'District List';
$_['text_add']                = 'Thêm';
$_['text_edit']               = 'Edit District';

$_['form_add'] = 'Thêm';
$_['form_edit'] = 'Sửa ID# %s';

// Column
$_['text_name']             = 'District Name';
$_['text_prefix']       = 'Level';
$_['text_action']           = 'Action';

// Entry
$_['text_name']              = 'District Name';
$_['text_code']        = 'Code';
$_['text_address_format']    = 'Address Format';
$_['text_postcode_required'] = 'Postcode Required';

// Help
$_['help_address_format']     = 'Full Name = {fullname}<br />Last Name = {lastname}<br />Company = {company}<br />Address 1 = {address_1}<br />Address 2 = {address_2}<br />City = {city}<br />Postcode = {postcode}<br />Zone = {zone}<br />Zone Code = {zone_code}<br />District = {country}';

// Error
$_['text_error_permission']        = 'Cảnh báo: Bạn không có quyền sửa Districts!';
$_['text_error_name']              = 'District Name must be between 3 and 128 characters!';
$_['text_error_default']           = 'Cảnh báo: This country cannot be deleted as it is currently assigned as the default store country!';
$_['text_error_store']             = 'Cảnh báo: This country cannot be deleted as it is currently assigned to %s stores!';
$_['text_error_address']           = 'Cảnh báo: This country cannot be deleted as it is currently assigned to %s address book entries!';
$_['text_error_affiliate']         = 'Cảnh báo: This country cannot be deleted as it is currently assigned to %s affiliates!';
$_['text_error_zone']              = 'Cảnh báo: This country cannot be deleted as it is currently assigned to %s zones!';
$_['text_error_zone_to_geo_zone']  = 'Cảnh báo: This country cannot be deleted as it is currently assigned to %s zones to geo zones!';