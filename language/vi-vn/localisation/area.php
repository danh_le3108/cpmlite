<?php
// Heading
$_['heading_title']           = 'Vùng';
$_['group_title']           = 'Hệ thống';

// Text
$_['text_Thành công']            = 'Thành công: Sửa thành công!';
$_['text_list']               = 'Danh sách Vùng';
$_['text_add']                = 'Thêm';
$_['text_edit']               = 'Sửa';

$_['form_add'] = 'Thêm';
$_['form_edit'] = 'Sửa ID# %s';
// Column
$_['text_name']             = 'Tên';
$_['text_level']       = 'Level';
$_['text_action']           = 'Thao tác';

// Entry
$_['text_code']        			= 'Code';
$_['text_status']            = 'Trạng thái';

// Error
$_['text_error_permission']        = 'Cảnh báo: Bạn không có quyền sửa!';
$_['text_error_name']              = 'Area Name must be between 3 and 128 characters!';
$_['text_error_default']           = 'Cảnh báo: This country cannot be deleted as it is currently assigned as the default store country!';