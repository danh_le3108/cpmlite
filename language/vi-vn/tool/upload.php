<?php
// Text
$_['text_upload']    = 'Your file was successfully uploaded!';

// Error
$_['text_error_filename'] = 'Filename must be between 3 and 64 characters!';
$_['text_error_filetype'] = 'Invalid file type!';
$_['text_error_upload']   = 'Upload required!';
