<?php

// Heading

$_['heading_title']           = 'Coupon Prefix';

$_['group_title']           = 'System';



// Text

$_['text_success']            = 'Success: You have modified Coupon Prefix!';

$_['text_list']               = 'Coupon Prefix List';

$_['text_add']                = 'Add Coupon Prefix';

$_['text_edit']               = 'Edit Coupon Prefix';



// Column

$_['text_action']           = 'Thao tác';

// Entry

$_['text_prefix_value']              = 'Coupon Value';


$_['text_status']            = 'Trạng thái';


// Error

$_['text_error_permission']        = 'Warning: You do not have permission to modify Coupon Prefix!';

$_['text_error_prefix_value']              = 'Coupon Value must be between 6 and 7 characters!';

