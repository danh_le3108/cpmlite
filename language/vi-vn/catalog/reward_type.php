<?php

// Heading

$_['heading_title']           = 'Loại thưởng';

$_['group_title']           = 'System';



// Text

$_['text_success']            = 'Success: You have modified Loại thưởng!';

$_['text_list']               = 'Loại thưởng List';

$_['text_add']                = 'Thêm';

$_['text_edit']               = 'Edit Loại thưởng';



// Column

$_['text_action']           = 'Thao tác';

// Entry

$_['text_name']              = 'Tên Loại thưởng ';

$_['text_code']        = 'Loại thưởng';

$_['text_status']            = 'Trạng thái';


// Error

$_['text_error_permission']        = 'Warning: You do not have permission to modify Codes!';

$_['text_error_name']              = 'Loại thưởng Name must be between 3 and 128 characters!';

