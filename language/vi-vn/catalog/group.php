<?php

// Heading

$_['heading_title']           = 'Group';

$_['group_title']           = 'System';



// Text

$_['text_success']            = 'Success: You have modified Group!';

$_['text_list']               = 'Group List';

$_['text_add']                = 'Add Group';

$_['text_edit']               = 'Edit Group';



// Column

$_['text_action']           = 'Thao tác';

// Entry

$_['text_name']              = 'Group Name';


$_['text_status']            = 'Trạng thái';


// Error

$_['text_error_permission']        = 'Warning: You do not have permission to modify Group!';

$_['text_error_name']              = 'Category Name must be between 3 and 128 characters!';

