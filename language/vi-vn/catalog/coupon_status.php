<?php

// Heading

$_['heading_title']           = 'CouponStatus';

$_['group_title']           = 'System';



// Text

$_['text_success']            = 'Success: You have modified CouponStatus!';

$_['text_list']               = 'CouponStatus List';

$_['text_add']                = 'Thêm';

$_['text_edit']               = 'Edit CouponStatus';



// Column

$_['text_action']           = 'Thao tác';

// Entry

$_['text_name']              = 'CouponStatus Name';

$_['text_code']        = 'CouponStatus';

$_['text_status']            = 'Trạng thái';


// Error

$_['text_error_permission']        = 'Warning: You do not have permission to modify Codes!';

$_['text_error_name']              = 'CouponStatus Name must be between 3 and 128 characters!';

