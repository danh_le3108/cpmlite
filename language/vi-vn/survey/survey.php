<?php

// Heading

$_['heading_title']           = 'Khảo sát';

$_['group_title']           = 'System';

$_['text_question_group']           = 'Nhóm câu hỏi';
$_['text_group_title']           = 'Tên nhóm';
$_['text_input_type']           = 'Input type';


$_['text_question']           = 'Câu hỏi';
$_['text_question_title']           = 'Tiêu đề';


// Text

$_['text_success']            = 'Success: You have modified Khảo sát!';

$_['text_list']               = 'Khảo sát List';

$_['text_add']                = 'Add Khảo sát';

$_['text_edit']               = 'Edit Khảo sát';
$_['text_target']               = 'Target';

$_['text_copy_from']               = 'Copy from';



// Column

$_['text_action']           = 'Thao tác';

// Entry

$_['text_name']              = 'Survey Name';

$_['text_status']            = 'Trạng thái';


// Error

$_['text_error_permission']        = 'Warning: You do not have permission to modify Khảo sát!';

$_['text_error_name']              = 'Category Name must be between 3 and 128 characters!';

