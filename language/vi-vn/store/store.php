<?php
// Heading
$_['heading_title']                = 'Cửa hàng';
$_['group_title']                  = 'Cửa hàng';

// Text
$_['text_general']                 = 'General';
$_['text_store_info']              = 'Thông tin';
$_['text_customer_code']           = 'Mã của KH';

$_['text_none']                    = ' -- None -- ';
$_['text_clear']                   = 'Clear';
$_['text_filter']                  = 'Search';
$_['help_filter']                  = 'Support search: Username, Fullname, Email, Telephone, Code...';


// Text
$_['text_place_variant']    = 'Variant';
$_['help_place']                   = 'Select the nearest place';

// Text
$_['text_success']                 = 'Thành công: Đã cập nhật stores!';
$_['text_list']                    = 'User List';
$_['text_add']                = 'Thêm';
$_['text_edit']                    = 'Edit User';
$_['text_avatar']                  = 'Avatar';
$_['text_phone']                   = 'Phone number';
$_['text_tax_code']                = 'Tax code';
$_['text_identity_number']         = 'Identity number';
$_['text_identity_issue_date']     = 'Identity issuedate';
$_['text_identity_issue_place']    = 'Identity issued place';
$_['text_insurance_num']           = 'Insurance number';
$_['text_social_insurance_num']    = 'Social insurance NUM';
$_['text_relative_telephone']      = 'Relative telephone';
$_['text_belonging_persons']       = 'Belonging persons';

// Column
$_['text_store_name']              = 'Tên';
$_['text_status']                  = 'Trạng thái';
$_['text_action']                  = 'Thao tác';
$_['text_store_register']          = 'Register by';
$_['text_delete_selected']         = 'Delete selected';

// Entry
$_['text_store_department']        = 'Department';
$_['text_store_type']              = 'Loại cửa hàng';
$_['text_store_owner']             = 'Chủ cửa hàng';
$_['text_telephone']               = 'Điện thoại';
$_['text_fullname']                = 'Full Name';
$_['text_lastname']                = 'Last Name';
$_['text_map']                     = 'Bản đồ';
$_['text_image']                   = 'Hình';

// Error
$_['text_error_permission']        = 'Warning: Bạn không có quyền sửa stores!';
$_['text_error_account']           = 'Warning: You can not delete your own account!';
$_['text_error_exists_store_code'] = 'Store code đã tồn tại! Vui lòng nhập store code khác!';
$_['text_error_exists_store_name'] = 'Warning: Username is already in use!';
$_['text_error_store_name']        = 'Chưa nhập store name!';
$_['text_error_store_code']        = 'Chưa nhập store code!';
$_['text_error_password']          = 'Password must be between 4 and 20 characters!';
$_['text_error_confirm']           = 'Password and password confirmation do not match!';
$_['text_error_store_owner']       = 'Store Owner must be between 2 and 32 characters!';
$_['text_error_lastname']          = 'Last Name must be between 1 and 32 characters!';
$_['text_error_email']             = 'E-Mail Address does not appear to be valid!';
$_['text_error_exists_email']      = 'Warning: E-Mail Address is already registered!';