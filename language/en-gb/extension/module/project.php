<?php
// Heading
$_['heading_title'] = 'Choose a Project';

// Text
$_['text_default']  = 'Default';
$_['text_project']    = 'Please choose the project you wish to visit.';