<?php
// Locale
$_['heading_title']                    = 'Online history';


$_['text_user']               = 'User';
$_['text_username']                    = 'Username';
$_['text_customer_group']             = 'Group';
$_['text_ip_address']                 = 'IP address';
$_['text_total_login']                = 'Total login';
$_['text_last_login']                 = 'Last login';
