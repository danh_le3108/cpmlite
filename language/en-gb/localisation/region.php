<?php
// Heading
$_['heading_title']           = 'Regions';
$_['group_title']           = 'System';
 
// Text
$_['text_success']            = 'Success: You have modified Regions!';
$_['text_list']               = 'Region List';
$_['text_add']                = 'Add Region';
$_['text_edit']               = 'Edit Region';
$_['form_add'] = 'Add new';
$_['form_edit'] = 'Edit ID# %s';

// Column
$_['text_name']             = 'Region Name';
$_['text_level']       = 'Level';
$_['text_action']           = 'Action';

// Entry
$_['text_name']              = 'Region Name';
$_['text_code']        			= 'Code';


// Error
$_['text_error_permission']        = 'Warning: You do not have permission to modify Regions!';
$_['text_error_name']              = 'Region Name must be between 3 and 128 characters!';
$_['text_error_default']           = 'Warning: This country cannot be deleted as it is currently assigned as the default store country!';
$_['text_error_store']             = 'Warning: This country cannot be deleted as it is currently assigned to %s stores!';
$_['text_error_address']           = 'Warning: This country cannot be deleted as it is currently assigned to %s address book entries!';
$_['text_error_affiliate']         = 'Warning: This country cannot be deleted as it is currently assigned to %s affiliates!';
$_['text_error_zone']              = 'Warning: This country cannot be deleted as it is currently assigned to %s zones!';
$_['text_error_zone_to_geo_zone']  = 'Warning: This country cannot be deleted as it is currently assigned to %s zones to geo zones!';