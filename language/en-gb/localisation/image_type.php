<?php

// Heading

$_['heading_title']           = 'Image type';

$_['group_title']           = 'System';



// Text

$_['text_success']            = 'Success: You have modified Image type!';

$_['text_list']               = 'ImageType List';

$_['text_add']                = 'Add';

$_['text_edit']               = 'Edit';



// Column

$_['text_action']           = 'Action';

// Entry

$_['text_name']              = 'Name';

$_['text_code']        = 'ImageType';




// Error

$_['text_error_permission']        = 'Warning: You do not have permission to modify Image type!';

$_['text_error_name']              = 'Image type must be between 3 and 128 characters!';