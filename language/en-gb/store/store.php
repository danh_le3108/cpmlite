<?php
// Heading
$_['heading_title']                = 'Stores';
$_['group_title']                  = 'Stores';

// Text
$_['text_general']                 = 'General';
$_['text_store_info']              = 'Store info';
$_['text_customer_code']           = 'Customer\'s Code';

$_['text_none']                    = ' -- None -- ';
$_['text_clear']                   = 'Clear';
$_['text_filter']                  = 'Search';
$_['help_filter']                  = 'Support search: Username, Fullname, Email, Telephone, Code...';


// Text
$_['text_phone']                   = 'Phone';
$_['text_locality']                = 'Locality';
$_['help_place']                   = 'Select the nearest place';

// Text
$_['text_success']                 = 'Success: You have modified stores!';
$_['text_list']                    = 'User List';
$_['text_add']                     = 'Add User';
$_['text_edit']                    = 'Edit User';
$_['text_avatar']                  = 'Avatar';
$_['text_phone']                   = 'Phone number';
$_['text_tax_code']                = 'Tax code';
$_['text_identity_number']         = 'Identity number';
$_['text_identity_issue_date']     = 'Identity issuedate';
$_['text_identity_issue_place']    = 'Identity issued place';
$_['text_insurance_num']           = 'Insurance number';
$_['text_social_insurance_num']    = 'Social insurance NUM';
$_['text_relative_telephone']      = 'Relative telephone';
$_['text_belonging_persons']       = 'Belonging persons';

// Column
$_['text_store_name']              = 'Name';

$_['text_action']                  = 'Action';
$_['text_store_register']          = 'Register by';
$_['text_delete_selected']         = 'Delete selected';

// Entry
$_['text_store_department']        = 'Department';
$_['text_store_type']              = 'Store type';
$_['text_store_owner']             = 'Store Owner';
$_['text_telephone']               = 'Telephone';
$_['text_fullname']                = 'Full Name';
$_['text_lastname']                = 'Last Name';
$_['text_map']                     = 'Map';
$_['text_image']                   = 'Image';

// Error
$_['text_error_permission']        = 'Warning: You do not have permission to modify stores!';
$_['text_error_account']           = 'Warning: You can not delete your own account!';
$_['text_error_exists_store_code'] = 'Warning: CPM code is already in use!';
$_['text_error_exists_store_name'] = 'Warning: Username is already in use!';
$_['text_error_store_name']        = 'Store name must be least 3 characters!';
$_['text_error_password']          = 'Password must be between 4 and 20 characters!';
$_['text_error_confirm']           = 'Password and password confirmation do not match!';
$_['text_error_store_owner']       = 'Store Owner must be between 2 and 32 characters!';
$_['text_error_lastname']          = 'Last Name must be between 1 and 32 characters!';
$_['text_error_email']             = 'E-Mail Address does not appear to be valid!';
$_['text_error_exists_email']      = 'Warning: E-Mail Address is already registered!';