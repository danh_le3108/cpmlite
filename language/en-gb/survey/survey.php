<?php

// Heading

$_['heading_title']           = 'Survey';

$_['group_title']           = 'System';


$_['text_question_group']           = 'Question group';
$_['text_group_title']           = 'Group title';
$_['text_input_type']           = 'Input type';


$_['text_question']           = 'Question';
$_['text_question_title']           = 'Question title';

// Text

$_['text_success']            = 'Success: You have modified Survey!';

$_['text_list']               = 'Survey List';

$_['text_add']                = 'Add Survey';

$_['text_edit']               = 'Edit Survey';

$_['text_target']               = 'Target';

$_['text_copy_from']               = 'Copy from';

// Column

$_['text_action']           = 'Action';

// Entry

$_['text_name']              = 'Survey Name';



// Error

$_['text_error_permission']        = 'Warning: You do not have permission to modify Survey!';

$_['text_error_name']              = 'Survey Name must be between 3 and 128 characters!';