<?php
// Heading
$_['heading_title']    = 'User Groups';
$_['group_title']    = 'Users';

// Text
$_['text_success']     = 'Success: You have modified user groups!';
$_['text_list']        = 'User Group';
$_['text_add']         = 'Add User Group';
$_['text_edit']        = 'Edit User Group';
$_['text_description'] = 'Description';

// Column
$_['text_name']      = 'User Group Name';
$_['text_level']      = 'Level';
$_['text_action']    = 'Action';

// Entry
$_['text_system_permission'] = 'System Permission';
$_['text_tab_permission'] = 'Permission';
$_['text_access_per']     = 'View';
$_['text_add_per']     = 'Add';
$_['text_edit_per']     = 'Edit';
$_['text_delete_per']     = 'Delete';

// Error
$_['text_error_permission'] = 'Warning: You do not have permission to modify user groups!';
$_['text_error_name']       = 'User Group Name must be between 3 and 64 characters!';
$_['text_error_user']       = 'Warning: This user group cannot be deleted as it is currently assigned to %s users!';