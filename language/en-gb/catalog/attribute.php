<?php

// Heading

$_['heading_title']           = 'Attributes';

$_['group_title']           = 'System';
$_['text_attribute_code']           = 'Attr Code';



// Text

$_['text_success']            = 'Success: You have modified Attributes!';

$_['text_list']               = 'Attribute List';

$_['text_add']                = 'Add Attribute';

$_['text_edit']               = 'Edit Attribute';



// Column

$_['text_name']             = 'Attribute Name';

$_['text_action']           = 'Action';

$_['text_code']           = 'Code';



// Entry

$_['text_name']              = 'Attribute Name';

$_['text_code']           = 'Code';





// Error

$_['text_error_permission']        = 'Warning: You do not have permission to modify Attributes!';

$_['text_error_name']              = 'Attribute Name must be between 3 and 128 characters!';