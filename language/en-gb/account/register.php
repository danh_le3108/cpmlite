<?php
// Heading
$_['heading_title']        = 'Register Account';

// Text
$_['text_account']         = 'Account';
$_['text_register']        = 'Register';
$_['text_account_already'] = 'If you already have an account with us, please login at the <a href="%s">login page</a>.';
$_['text_your_details']    = 'Your Personal Details';
$_['text_your_address']    = 'Your Address';
$_['text_your_password']   = 'Your Password';
$_['text_agree']           = 'I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'Customer Group';
$_['entry_fullname']      = 'Full Name';
$_['entry_email']          = 'E-Mail';
$_['entry_telephone']      = 'Telephone';
$_['entry_password']       = 'Password';
$_['entry_confirm']        = 'Password Confirm';

// Error
$_['text_error_exists']         = 'Warning: E-Mail Address is already registered!';
$_['text_error_fullname']      = 'Full Name must be between 1 and 32 characters!';
$_['text_error_email']          = 'E-Mail Address does not appear to be valid!';
$_['text_error_telephone']      = 'Telephone must be between 3 and 32 characters!';
$_['text_error_password']       = 'Password must be between 4 and 20 characters!';
$_['text_error_confirm']        = 'Password confirmation does not match password!';
$_['text_error_agree']          = 'Warning: You must agree to the %s!';