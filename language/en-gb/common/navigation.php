<?php
// Text
$_['text_stores']           = 'Stores';
$_['text_store']            = 'Store';
$_['text_store_type']       = 'Store type';
$_['text_audit_info']     = 'Audit info';
$_['text_audit_chart']     = 'Audit chart';


$_['text_user_group']     = 'Group permission';
$_['text_system']         = 'System';
$_['text_category']         = 'Categories';
$_['text_country']          = 'Countries';
$_['text_place_variant']    = 'Variant';
$_['text_project_type']     = 'Project type';
$_['text_plans']            = 'Plan';
$_['text_plan']             = 'Plan';
$_['text_filemanager']      = 'Filemanager';
$_['text_filemanager_plus'] = 'Filemanager+';
$_['text_plan_status']      = 'Plan status';
$_['text_image_type']       = 'Image type';
$_['text_plan_rating']      = 'Plan rating';
$_['text_plan_position']    = 'Position';
$_['text_posm']        		= 'Posm';
$_['text_survey']        	= 'Survey question';
$_['text_plan_map']         = 'Map';
$_['text_excel_template']   = 'Excel template';
$_['text_users']   = 'Users';
$_['text_reports']   		= 'Báo cáo';
$_['text_plan_progress']   	= 'Tiến độ khảo sát';


