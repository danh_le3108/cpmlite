<?php
// Text
$_['text_none']  = '-None-';
$_['text_select']  = '-Select-';
$_['text_footer']  = '<a href="https://cpm-vietnam.com">CPM</a> &copy; 2002-' . date('Y') . ' All Rights Reserved.';
$_['text_version'] = 'Version %s';
