<?php
// Heading
$_['heading_title']         = 'Project route';


$_['text_sup_user']         = 'Sup';
$_['text_staff_user']       = 'Audit Staff';
$_['text_province']         = 'Province';
$_['text_plan_total']       = 'Plan total';
$_['text_progress']         = 'Progress';
$_['text_progress_percent'] = '% Progress';
$_['text_has_qc']           = 'Has QC';
$_['text_sup_staff']        = 'Sup - Audit Staff';

$_['text_qc_by_sup']        = 'Sup QC';
$_['text_qc_by_pl']         = 'PL QC';
$_['text_qc_by_dc']         = 'DC QC';
$_['text_qc_by_pa']         = 'PA QC';
$_['text_total_qc']         = 'Total';
