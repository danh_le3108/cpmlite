<?php

// Heading

$_['heading_title']           = 'Coupon Scan Status';

$_['group_title']           = 'System';



// Text

$_['text_success']            = 'Success: You have modified Coupon Scan Status!';

$_['text_list']               = 'Coupon Scan Status List';

$_['text_add']                = 'Add Coupon Scan Status';

$_['text_edit']               = 'Edit Coupon Scan Status';



// Column

$_['text_action']           = 'Action';

// Entry

$_['text_name']              = 'Coupon Scan Status Name';

$_['text_code']        = 'Coupon Scan Status';




// Error

$_['text_error_permission']        = 'Warning: You do not have permission to modify Codes!';

$_['text_error_name']              = 'Coupon Scan Status Name must be between 3 and 128 characters!';