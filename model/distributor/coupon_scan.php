<?php
class ModelDistributorCouponScan extends Model {
	public function addCouponScan($data) {
		$this->pdb->query("INSERT INTO " . PDB_PREFIX . "coupon_scan SET coupon_scan_name = '" . $this->db->escape($data['coupon_scan_name']) . "'");
		
		return $this->db->getLastId();
	}

	public function editCouponScan($coupon_scan_id, $data) {
		$this->pdb->query("UPDATE " . PDB_PREFIX . "coupon_scan SET  
		coupon_scan_name = '" . $this->db->escape($data['coupon_scan_name']) . "'
		WHERE coupon_scan_id = '" . (int)$coupon_scan_id . "'");
	}

	public function deleteCouponScan($coupon_scan_id) {
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "coupon_scan WHERE coupon_scan_id = '" . (int)$coupon_scan_id . "'");
	}

	public function getCouponScan($coupon_scan_id) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "coupon_scan WHERE coupon_scan_id = '" . (int)$coupon_scan_id . "'");

		return $query->row;
	}

	
	public function getCouponScanes($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . PDB_PREFIX . "coupon_scan p WHERE 1";
			
			/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "p.coupon_scan_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 

			$sort_data = array(
				'p.coupon_scan_name'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY p.coupon_scan_id";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

		} else {
			$sql = "SELECT * FROM " . PDB_PREFIX . "coupon_scan p ORDER BY p.coupon_scan_id DESC";
		}
		$query = $this->pdb->query($sql);

		$status = array();
		if($query->rows){
			foreach ($query->rows as $row) {
				$status[$row['coupon_scan_id']] = $row;
			}
		}
		return $status;
	}

	public function getTotalCouponScanes($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "coupon_scan p WHERE 1";
			
			/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "p.coupon_scan_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 
			
			$query = $this->pdb->query($sql);

		return $query->row['total'];
	}
	public function getCouponScanesByIndex() {
		$query_data = array();
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "coupon_scan ORDER BY  coupon_scan_id ASC");
		
		$query_data[0] = array(
					'coupon_scan_id'=>0,
					'coupon_scan_name'=>$this->language->get('text_not_recovered')
				);	
		foreach ($query->rows as $row){
			$query_data[$row['coupon_scan_id']] = $row;
		}

		return $query_data;
	}
}