<?php
class ModelAccountActivity extends Model {
	public function addActivity($key, $data) {
		if (isset($data['customer_user_id'])) {
			$customer_user_id = $data['customer_user_id'];
		} else {
			$customer_user_id = 0;
		}

		$this->db->query("INSERT INTO `" . DB_PREFIX . "history_activity` SET `customer_user_id` = '" . (int)$customer_user_id . "', `key` = '" . $this->db->escape($key) . "', `data` = '" . $this->db->escape(json_encode($data)) . "', `ip` = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', `date_added` = NOW()");
	}
}