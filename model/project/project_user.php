<?php
class ModelProjectProjectUser extends Model {
	public function approveDevice($user_id) {
		$sql = "UPDATE " . DB_PREFIX . "project_user SET `request_count` = (request_count + 1), `device_imei` = `new_imei`, `device_model` = `new_model` WHERE user_id = '" . (int)$user_id . "' AND project_id = '".(int)$this->config->get('config_project_id')."'";
		$this->db->query($sql);
	}
	public function rejectDevice($user_id) {
		$sql = "UPDATE " . DB_PREFIX . "project_user SET `new_imei`=`device_imei`, `new_model`=`device_model` WHERE user_id = '" . (int)$user_id . "' AND project_id = '".(int)$this->config->get('config_project_id')."'";
		$this->db->query($sql);
	}
	public function getUserManagers($user_id) { 
		$user_manager = array();
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "project_user` WHERE user_id = '" . (int)$user_id . "' AND project_id = '".(int)$this->config->get('config_project_id')."'");
		if($query->row){
			$user_manager = !empty($query->row['user_managers'])?explode(',',$query->row['user_managers']):array();
		}
		return $user_manager;
	}
	public function updateRegion($user_id,$region_code) {
		$sql = "UPDATE " . DB_PREFIX . "project_user SET region_code = '" . $this->db->escape($region_code) . "' WHERE user_id = '" . (int)$user_id . "' AND project_id = '".(int)$this->config->get('config_project_id')."'";
		$this->db->query($sql);
	}
	
	public function updateManager($user_id,$data) {
		$user_manager = array();
		$user_managers = $this->getUserManagers($user_id);
		if(!empty($user_managers)){
			foreach($user_managers as $manager_id){
				$user_manager[] = $manager_id;
			}
		}
		if(in_array($data['user_id'],$user_manager)){
			$user_manager = array_diff($user_manager,array($data['user_id']));
		}
		if((int)$data['handle']==1){
			$user_manager[] = $data['user_id'];
		}
		$sql = "UPDATE " . DB_PREFIX . "project_user SET";
		if(is_array($user_manager)){
			$sql .= " user_managers = '" . implode(',',$user_manager). "',";
		}
		$sql .= " user_update = '" . $this->user->getUserName() . "' WHERE user_id = '" . (int)$user_id . "' AND project_id = '".(int)$this->config->get('config_project_id')."'";
		$this->db->query($sql);
	}
	
	public function updateUser($user_id,$data) {
		
		$sql = "UPDATE " . DB_PREFIX . "project_user SET";
		if(isset($data['user_parent_id'])){
			$sql .= " user_parent_id = '" . (int)$data['user_parent_id'] . "',";
		}
		if(isset($data['user_group_id'])){
			$sql .= " project_user_group = '" . (int)$data['user_group_id'] . "',";
		}
		$sql .= " user_status = '1', user_update = '" . $this->user->getUserName() . "' WHERE user_id = '" . (int)$user_id . "' AND project_id = '".(int)$this->config->get('config_project_id')."'";
				$this->db->query($sql);
	}
	public function removeUser($user_id) {
		$user_info = $this->getUser($user_id);
		$sql = "UPDATE " . DB_PREFIX . "project_user SET
				user_status = '0',
				project_user_group = '" . (int)$user_info['user_group_id']. "',
				user_update = '" . $this->user->getUserName() . "',
				`date_end` = NOW() WHERE user_id = '" . (int)$user_id . "' AND project_id = '".(int)$this->config->get('config_project_id')."'";
				$this->db->query($sql);
	}
	
	public function getUser($user_id) {
	  $sql = "SELECT DISTINCT pu.*, u.fullname,u.username, u.usercode,u.user_group_id, u.image, u.email, u.telephone,u.status,u.portrait, 
	  u.latitude as user_latitude,u.longitude as user_longitude,
	  (SELECT up.fullname FROM `" . DB_PREFIX . "user` up WHERE up.user_id = pu.user_parent_id LIMIT 0,1) AS parent_fullname, 
	  (SELECT ug.group_name FROM `" . DB_PREFIX . "user_group` ug WHERE ug.user_group_id = pu.project_user_group) AS group_name
		FROM " . DB_PREFIX . "project_user pu LEFT JOIN " . DB_PREFIX . "user u ON (u.user_id = pu.user_id)
		WHERE pu.project_id = '" . (int)$this->config->get('config_project_id') . "'
		AND pu.user_id = '" . (int)$user_id . "'";
	  $query = $this->db->query($sql);
	 
	  return $query->row;
	 }
	 
	public function getUserByUsername($username) {
	  $sql = "SELECT DISTINCT pu.*, u.fullname,u.username, u.usercode,u.image, u.email, u.telephone,u.status,
	  (SELECT ug.group_name FROM `" . DB_PREFIX . "user_group` ug WHERE ug.user_group_id = pu.project_user_group) AS group_name
		FROM " . DB_PREFIX . "project_user pu LEFT JOIN " . DB_PREFIX . "user u ON (u.user_id = pu.user_id)
		WHERE pu.project_id = '" . (int)$this->config->get('config_project_id') . "' AND u.username = '" . $this->db->escape($username). "'";
	  $query = $this->db->query($sql);
	 
	  return $query->row;
	 }
	public function getProjectUsersIndexBy($index_by = 'username') {
		$user_data = array();
		$project_id = $this->config->get('config_project_id');
		$sql = "SELECT DISTINCT pu.*,ug.group_name,ug.child_group_id, u.fullname, u.username, u.usercode, u.image, u.email, u.telephone,
			(SELECT up.fullname FROM `" . DB_PREFIX . "user` up WHERE up.user_id = pu.user_parent_id LIMIT 0,1) AS parent_fullname FROM " . DB_PREFIX . "project_user pu LEFT JOIN " . DB_PREFIX . "user u ON (u.user_id = pu.user_id) LEFT JOIN " . DB_PREFIX . "user_group ug ON (ug.user_group_id = pu.project_user_group) WHERE pu.project_id = '" . (int)$project_id . "'";
 
		$query = $this->db->query($sql);

		foreach($query->rows as $row){
			$user_data[$row[$index_by]] = $row;
		}		
		return $user_data;
	}
	public function getProjectUser($user_id, $data = array()) {
  $user_data = array();
  $sql = "SELECT DISTINCT pu.*, u.fullname,u.username, u.usercode,u.image, u.email, u.telephone,
  (SELECT ug.group_name FROM `" . DB_PREFIX . "user_group` ug WHERE ug.user_group_id = pu.project_user_group) AS group_name,
	(SELECT up.fullname FROM `" . DB_PREFIX . "user` up WHERE up.user_id = pu.user_parent_id LIMIT 0,1) AS parent_fullname 
    FROM " . DB_PREFIX . "project_user pu LEFT JOIN " . DB_PREFIX . "user u ON (u.user_id = pu.user_id)
    WHERE pu.project_id = '" . (int)$this->config->get('config_project_id') . "'
    AND pu.user_id = '" . (int)$user_id. "'";
  $query = $this->db->query($sql);
  $filter_round_name = isset($data['filter_round_name']) ? $data['filter_round_name'] : '';

		$count = $this->getTotalPlansGroupByUser($data); 
  foreach($query->rows as $k => $u){
   $user_data[$u['user_id']] = $u;
   $user_data[$u['user_id']]['plan_total'] = isset($count[$u['user_id']])?$count[$u['user_id']]:0;
  }
  return $user_data;
 }
	public function getProjectUsers($data = array()) {
		$user_data = array();
		$project_id = $this->config->get('config_project_id');
		$sql = "SELECT DISTINCT pu.*,ug.group_name,ug.user_group_id,ug.child_group_id, u.fullname, u.username, u.usercode, u.image, u.email, u.telephone,
			(SELECT up.fullname FROM `" . DB_PREFIX . "user` up WHERE up.user_id = pu.user_parent_id LIMIT 0,1) AS parent_fullname FROM " . DB_PREFIX . "project_user pu LEFT JOIN " . DB_PREFIX . "user u ON (u.user_id = pu.user_id) LEFT JOIN " . DB_PREFIX . "user_group ug ON (ug.user_group_id = pu.project_user_group) WHERE pu.project_id = '" . (int)$project_id . "'";
 
		if (isset($data['users_manager'])&& !empty($data['users_manager'])) {
			$sql .= " AND pu.`user_id` IN('".implode("','",$data['users_manager'])."')";
		}
		$query = $this->db->query($sql);
		$filter_round_name = isset($data['filter_round_name']) ? $data['filter_round_name'] : '';

		$count = $this->getTotalPlansGroupByUser($data); 

		foreach($query->rows as $k => $u){
			$user_data[$u['user_id']] = $u;
			$user_data[$u['user_id']]['plan_total'] = isset($count[$u['user_id']])?$count[$u['user_id']]:0;
		}		
		return $user_data;
	}
	public function getProjectUsersByParentID($user_parent_id, $data = array()) {
		$user_data = array();
		$sql = "SELECT DISTINCT pu.*, u.fullname,u.username, u.usercode, u.image, u.email, u.telephone, (SELECT up.fullname FROM `" . DB_PREFIX . "user` up WHERE up.user_id = pu.user_parent_id LIMIT 0,1) AS parent_fullname,
			(SELECT ug.group_name FROM `" . DB_PREFIX . "user_group` ug WHERE ug.user_group_id = pu.project_user_group) AS group_name
			 FROM " . DB_PREFIX . "project_user pu LEFT JOIN " . DB_PREFIX . "user u ON (u.user_id = pu.user_id) 
			 WHERE pu.project_id = '" . (int)$this->config->get('config_project_id') . "'
			 AND pu.user_parent_id = '" . (int)$user_parent_id. "'";
			 
		if (isset($data['users_manager'])&& !empty($data['users_manager'])) {
			$sql .= " AND pu.`user_id` IN('".implode("','",$data['users_manager'])."')";
		}
		$query = $this->db->query($sql);

		$filter_round_name = isset($data['filter_round_name']) ? $data['filter_round_name'] : '';
		$count = $this->getTotalPlansGroupByUser($data); 

		foreach($query->rows as $k => $u){
			$user_data[$u['user_id']] = $u;
			$user_data[$u['user_id']]['plan_total'] = isset($count[$u['user_id']])?$count[$u['user_id']]:0;
		}		
		return $user_data;
	}
	public function getProjectUsersByGroupID($user_group_id, $data = array()) {
		$user_data = array();
		$sql = "SELECT DISTINCT pu.*, u.fullname,u.username, u.usercode, u.image, u.email, u.telephone, (SELECT up.fullname FROM `" . DB_PREFIX . "user` up WHERE up.user_id = pu.user_parent_id LIMIT 0,1) AS parent_fullname,
			(SELECT ug.group_name FROM `" . DB_PREFIX . "user_group` ug WHERE ug.user_group_id = pu.project_user_group) AS group_name
			 FROM " . DB_PREFIX . "project_user pu LEFT JOIN " . DB_PREFIX . "user u ON (u.user_id = pu.user_id) 
			 WHERE pu.project_id = '" . (int)$this->config->get('config_project_id') . "'
			 AND pu.user_status = 1 AND pu.project_user_group = '" . (int)$user_group_id. "'";
			 
		if ($this->user->isLogged()) {
			$user_manager = $this->user->users_manager();
			if (!empty($user_manager) || $this->user->getGroupId() != 4) {
				$sql .= " AND pu.`user_id` IN('".implode("','",$user_manager)."')";
			}
		}
		$query = $this->db->query($sql);

		$filter_round_name = isset($data['filter_round_name']) ? $data['filter_round_name'] : '';
		$count = $this->getTotalPlansGroupByUser($data); 

		foreach($query->rows as $k => $u){
			$user_data[$u['user_id']] = $u;
			$user_data[$u['user_id']]['plan_total'] = isset($count[$u['user_id']])?$count[$u['user_id']]:0;
		}		
		return $user_data;
	}

	public static $plan_count = array();
	public function getTotalPlansGroupByUser($data = array()) {
		$filter_round_name = isset($data['filter_round_name'])?$data['filter_round_name']:0;
			
		if (isset(self::$plan_count[$filter_round_name])) {
			return self::$plan_count[$filter_round_name];
		}
			$sql = "SELECT pl.user_id, COUNT(*) AS plan_total FROM " . PDB_PREFIX . "plan pl WHERE pl.is_deleted = '0'";
			
			if(isset($data['filter_round_name'])){
				$sql .= " AND pl.round_name = '".$data['filter_round_name']."'";
			}
			if(isset($data['filter_group_id'])){
				$sql .= " AND pl.group_id = '".$data['filter_group_id']."'";
			}	
			if (isset($data['filter_plan_status'])&& !is_null($data['filter_plan_status'])) {
				$sql .= " AND pl.plan_status = '" . (int)$data['filter_plan_status'] . "'";
			}

			$sql .= " GROUP BY pl.user_id";

			$query = $this->pdb->query($sql);
			$plan_total = array();
		foreach($query->rows as $row){
			$plan_total[$row['user_id']] = $row['plan_total'];
		}
		return  self::$plan_count[$filter_round_name] = $plan_total;
	}
	public function getTotalProjects() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "project");

		return $query->row['total'];
	}

	public function getProjectsOfUser($user_id, $project_status = 1){
		$projects_user = array();
		$sql = "SELECT pu.*, p.project_status, p.project_type FROM `" . DB_PREFIX . "project_user` pu LEFT JOIN`" . DB_PREFIX . "project` p ON p.project_id = pu.project_id WHERE pu.`user_id` = '" . (int)$user_id . "' AND p.project_status = '".(int)$project_status."'";
		$query = $this->db->query($sql);

		if($query->rows){
			foreach($query->rows as $result){
				$projects_user[$result['project_id']] = array(
					'project_type' => $result['project_type'],
					'user_status' => $result['user_status']
				);
			}
		}
		return $projects_user;
	}
	public function getProjectType($project_id) { 
		$query = $this->db->query("SELECT DISTINCT project_type FROM `" . DB_PREFIX . "project` WHERE project_id = '" . (int)$project_id . "'");
		if($query->row){
			return $query->row['project_type'];
		}
	}

	public function addUserToProject($data = array()){
			$project_code = $this->config->get('project_code');
			$project_user_group = isset($data['user_group_id']) ? $data['user_group_id'] : 0;
			$this->db->query("INSERT INTO `" . DB_PREFIX . "project_user` SET
				`project_id` = '" . (int)$this->config->get('config_project_id') . "',
				`user_id` = '" . (int)$data['user_id'] . "',
				`usercode` = '" . $this->db->escape($data['usercode']) . "',
				`fullname` = '" . $this->db->escape($data['fullname']) . "',
				`project_code` = '" . $this->db->escape($project_code) . "',
				`project_user_group` = '" . $project_user_group ."',
				`user_update` = '" . $this->user->getUserName() . "',
				`date_start` = NOW(),
				`date_added` = NOW(), 
				`date_end` = ''");
	}
}