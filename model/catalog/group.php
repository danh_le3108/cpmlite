<?php
class ModelCatalogGroup extends Model {
	private $no_filter_key = array(
		'_url',
		'route',
		'page',
		'filter_global',
		'start',
		'limit',
		'sort',
		'order',
	);

	private $filter_global = array(
		'g1.group_code',
		'g1.group_name'
	);
	public function getGroups($data = array()) {
		$sql = "SELECT g1.*, g2.group_code as parent_group_code, g2.group_name as parent_group_name FROM " . DB_PREFIX . "attribute_group g1 
				LEFT JOIN " . DB_PREFIX . "attribute_group g2 ON g1.parent_group_id = g2.group_id WHERE 1";
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				if (!in_array($key, $this->no_filter_key)) {
					$key = str_replace('filter_', '', $key);
					$sql .= " AND g1." . $key . "='" . $this->pdb->escape($value) . "'";
				}	
			}

			if (!empty($data['filter_global'])) {
				$sql .= " AND (";
				foreach ($this->filter_global as $filter) {
					$implode[] = " LCASE(".$filter.") LIKE '%" . $this->pdb->escape(utf8_strtolower($data['filter_global'])) . "%'";
				}
				$sql .= " " . implode(" OR ", $implode) . "";
				$sql .= ")";
			}
		}

		if (!empty($data['sort'])) {
			$sql .= ' ORDER BY ' . $data['sort'];
		} else {
			$sql .= ' ORDER BY group_id DESC';
		}

		if (isset($data['start']) && isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		return $this->pdb->query($sql)->rows;
	}

	public function edit($group_id,$data) {
		$sql = "UPDATE " . DB_PREFIX . "attribute_group SET ";
		foreach ($data as $key => $value) {
			$sql .= "$key = '" . $this->pdb->escape($value) . "',";
		}
		$sql = trim($sql,",") . " WHERE group_id = " . (int)$group_id;
		$this->pdb->query($sql);
	}

	public function add($data) {
		$sql = "INSERT INTO " . DB_PREFIX . "attribute_group SET ";
		foreach ($data as $key => $value) {
			$sql .= "$key = '" . $this->pdb->escape($value) . "',";
		}
		$sql = trim($sql,",");
		// p($sql,1);
		$this->pdb->query($sql);
	}
}