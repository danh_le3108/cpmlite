<?php
class ModelCatalogCouponPrefix extends Model {
	public function addCouponPrefix($data) {
		$this->pdb->query("INSERT INTO " . PDB_PREFIX . "coupon_prefix SET 
		prefix_title = '" . $this->db->escape($data['prefix_title']) . "',
		prefix_value = '" . $this->db->escape($data['prefix_value']) . "',
		coupon_prefix = '" . $this->db->escape($data['coupon_prefix']) . "'");
		
		return $this->db->getLastId();
	}

	public function editCouponPrefix($prefix_id, $data) {
		$this->pdb->query("UPDATE " . PDB_PREFIX . "coupon_prefix SET  
		prefix_title = '" . $this->db->escape($data['prefix_title']) . "',
		prefix_value = '" . $this->db->escape($data['prefix_value']) . "',
		coupon_prefix = '" . $this->db->escape($data['coupon_prefix']) . "' WHERE prefix_id = '" . (int)$prefix_id . "'");
	}

	public function deleteCouponPrefix($prefix_id) {
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "coupon_prefix WHERE prefix_id = '" . (int)$prefix_id . "'");
	}

	public function getCouponPrefix($prefix_id) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "coupon_prefix WHERE prefix_id = '" . (int)$prefix_id . "'");

		return $query->row;
	}

	public function getCouponPrefixs($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . PDB_PREFIX . "coupon_prefix p WHERE 1";
			
			/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "p.prefix_value LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 

			$sort_data = array(
				'p.coupon_prefix'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY p.coupon_prefix";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->pdb->query($sql);

			return $query->rows;
		} else {
			$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "coupon_prefix p ORDER BY p.coupon_prefix ASC");

			$query_data = $query->rows;

			return $query_data;
		}
	}

	public function getTotalCouponPrefixs($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "coupon_prefix p WHERE 1";
			
			/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "p.prefix_value LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 
			
			$query = $this->pdb->query($sql);

		return $query->row['total'];
	}
	public static $coupon_prefix = array();
	public function getPrefixsIndexBy($index_by = 'prefix_id') {
		if (!empty(self::$coupon_prefix[$index_by])) {
			return self::$coupon_prefix[$index_by];
		}
		$prefix_data = array();
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "coupon_prefix p ORDER BY p.coupon_prefix ASC");

		if($query->num_rows){
			foreach($query->rows as $row){
				if(isset($row[$index_by])){
					$prefix_data[$row[$index_by]] = $row;
				}
			}
		}

		return  self::$coupon_prefix[$index_by] = $prefix_data;
	}
}