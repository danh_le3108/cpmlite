<?php

class ModelCatalogCoupon extends Model {
	
	public function coupon_fields(){
			return array(
                'A' => array('field' => 'stt','label' => 'STT'),
                'B' => array('field' => 'coupon_code','label' => 'Mã Coupon'),
                'C' => array('field' => 'sup_code','label' => 'Mã SUP')
			);
	}
	
	
	public function addCoupon($data) {
		$sql = "INSERT INTO " . PDB_PREFIX . "coupon SET `date_added` = NOW(),
		coupon_code = '" . $this->db->escape($data['coupon_code']) . "'";
		if (isset($data['coupon_prefix'])) {
			$sql .= ",coupon_prefix = '" . $this->db->escape($data['coupon_prefix']) . "'";
		}
		if (isset($data['sup_id'])) {
			$sql .= ",sup_id = '" . (int)$data['sup_id'] . "'";
		}
		if (isset($data['sup_code'])) {
			$sql .= ",sup_code = '" . $this->db->escape($data['sup_code']) . "'";
		}
		if (isset($data['import_id'])) {
			$sql .= ",import_id = '" . $this->db->escape($data['import_id']) . "'";
		}
		
		$this->pdb->query($sql);
		return $this->db->getLastId();
	}
	public function editCoupon($coupon_id, $data) {
		$sql = "UPDATE " . PDB_PREFIX . "coupon SET  
		coupon_code = '" . $this->db->escape($data['coupon_code']) . "'";
		
		if (isset($data['sup_id'])) {
			$sql .= ",sup_id = '" . (int)$data['sup_id'] . "'";
		}
		if (isset($data['sup_code'])) {
			$sql .= ",sup_code = '" . $this->db->escape($data['sup_code']) . "'";
		}
		
		$sql .= " WHERE coupon_id = '" . (int)$coupon_id . "'";
		
		$this->pdb->query($sql);
	}

	public function deleteCoupon($coupon_id) {
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "coupon WHERE coupon_id = '" . (int)$coupon_id . "'");
	}

	public function getCoupon($coupon_id) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "coupon WHERE coupon_id = '" . (int)$coupon_id . "'");

		return $query->row;
	}
	public function getCouponByCode($coupon_code) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "coupon WHERE coupon_code = '" . $this->pdb->escape($coupon_code) . "'");

		return $query->row;
	}

	public function getCoupons($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . PDB_PREFIX . "coupon d WHERE 1";
			
		/*Filter start*/ 
		$implode = array();

		if (isset($data['filter_name'])&&!empty($data['filter_name'])) {
			$implode[] = "d.coupon_code LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_sup_id'])&&!empty($data['filter_sup_id'])) {
			$implode[] = "d.sup_id = '" . $this->db->escape($data['filter_sup_id']) . "'";
		}

		if (isset($data['filter_coupon_status'])) {
			$implode[] = "d.coupon_status_id = '" . (int)$data['filter_coupon_status'] . "'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 

			$sort_data = array(
				'd.coupon_id'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY d.coupon_id";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}
			
		} else {
			$sql = "SELECT * FROM " . PDB_PREFIX . "coupon d ORDER BY d.coupon_id ASC";

		}
		
		$query = $this->pdb->query($sql);

		return $query->rows;
	}

	public function getTotalCoupons($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "coupon d WHERE 1";
			
		/*Filter start*/ 
		$implode = array();

		if (isset($data['filter_name'])&&!empty($data['filter_name'])) {
			$implode[] = "d.coupon_code LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if (isset($data['filter_sup_id'])&&!empty($data['filter_sup_id'])) {
			$implode[] = "d.sup_id = '" . $this->db->escape($data['filter_sup_id']) . "'";
		}

		if (isset($data['filter_coupon_status'])) {
			$implode[] = "d.coupon_status_id = '" . (int)$data['filter_coupon_status'] . "'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
		/*Filter end*/ 
		$query = $this->pdb->query($sql);
		
		return $query->row['total'];
	}
	/*Excel*/ 
	public function checkCoupon($data) {
		$sql = "SELECT COUNT(*) AS total FROM `" . PDB_PREFIX . "coupon` WHERE `coupon_code` = '" . $this->db->escape(trim($data['coupon_code'])) . "'";
		 
		$query = $this->pdb->query($sql);
			$return = $query->row['total'];
		if((int)$query->row['total']<1){
			$return = 0;
			$this->addCoupon($data);
		}else{
			$return = 2;
			$this->pdb->query("UPDATE ".PDB_PREFIX."coupon SET sup_id = '".(int)$data['sup_id']."', sup_code = '".$this->pdb->escape(trim($data['sup_code']))."' WHERE coupon_code = '".$this->pdb->escape(trim($data['coupon_code']))."' AND coupon_status_id=0");	
		}
		return $return;
	}
	
	public function getExportCoupons($data = array()) {
		$coupons_data = array();
		$coupons = $this->getCoupons($data);
			$i=0;
		foreach($coupons as $coupon){
			foreach($this->coupon_fields() as $col => $value){
					if(isset($coupon[$value['field']])){
						$coupons_data[$i][$col] = $coupon[$value['field']];
					}else{
						$coupons_data[$i][$col] = '';
					}
			}
			$i++;
		}
		return $coupons_data;
	}

	
	
}