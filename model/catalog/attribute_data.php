<?php
class ModelCatalogAttributeData extends Model {
	public function add($data) {
		$sql = "INSERT INTO " . PDB_PREFIX . "attribute_data SET ";
		foreach ($data as $key => $value) {
			$sql .= "{$key} = '" . $this->db->escape($value) . "',";
		}
		$sql .= "date_added = NOW()";
		$this->pdb->query($sql);
		return $this->pdb->getLastId();
	}

	public function update ($id, $data) {
		$sql = "UPDATE " . PDB_PREFIX . "attribute_data SET ";
		foreach ($data as $key => $value) {
			$sql .= "{$key} = '" . $this->db->escape($value) . "',";
		}
		$sql .= "date_modified = NOW() WHERE id = " . (int)$id;
		$this->pdb->query($sql);
	}

	public function getAttrsOfPlan($data = array(), $is_mapping = true) {
		$sql = "SELECT * FROM " . PDB_PREFIX . "attribute_data dt 
				LEFT JOIN " . PDB_PREFIX . "attribute a ON a.attribute_id = dt.attribute_id WHERE 1";
		if (!empty($data['group_codes'])) {
			$sql .= " AND group_code IN ('" . implode("','", $data['group_codes']) . "')";
			unset($data['group_codes']);
		}

		if (!empty($data['plan_ids'])) {
			$sql .= " AND plan_id IN ('" . implode("','", $data['plan_ids']) . "')";
			unset($data['plan_ids']);
		}	
		if (!empty($data['round_names'])) {
			$sql .= " AND round_name IN ('" . implode("','", $data['round_names']) . "')";
			unset($data['round_names']);
		}
		foreach ($data as $key => $value) {
			$sql .= " AND {$key}= '" . $this->db->escape($value) . "'";
		}


		if ($is_mapping) {
			$data = array();
			foreach ($this->pdb->query($sql)->rows as $value) {
				$data[$value['attribute_id']] = $value;
			}
			return $data;
		} else {
			return  $this->pdb->query($sql)->rows;
		}
	}

	public function getAttrData() {

	}
	
	// public function delete($id) {
	// 	$sql = "DELETE FROM " . PDB_PREFIX . "attribute_data WHERE id =" . (int)$id;
	// 	$this->pdb->query($sql);
	// }

	public function delete($data) {
		if (!empty($data)) {
			$sql = "DELETE FROM " . PDB_PREFIX . "attribute_data ";
			$condition = array();
			foreach ($data as $key => $value) {
				$condition[] = "{$key}='" . $this->pdb->escape($value) . "'";
			}
			$sql .= " WHERE " . implode(' AND ', $condition);
			
			$this->pdb->query($sql);
		}
	}

	public function countDataByPlan ($data = array()) {
		$sql = "SELECT da.plan_id,a.group_code,a.attribute_id,a.attribute_name, sum(da.value) as attr_number, sum(da.price_total) as price_total 
				FROM cpm_attribute_data da 
				JOIN cpm_plan_order o on da.order_id = o.order_id 
				JOIN cpm_attribute a on da.attribute_id = a.attribute_id 
				WHERE o.is_deleted = 0 AND a.group_code NOT IN ('APPROACHING','CUSTOMER')";
		$condition = '';
		if (!empty($data['plan_ids'])) {
			$condition = " AND da.plan_id IN (" . implode(',', $data['plan_ids']) . ")";
		}
		$sql = $sql.$condition." GROUP BY da.plan_id,a.group_code,a.attribute_id,a.attribute_name";
		$attrs = $this->pdb->query($sql)->rows;
		$attrs_data = array();
		foreach ($attrs as $value) {
			if (empty($attrs_data[$value['plan_id']])) {
				$attrs_data[$value['plan_id']] = array(
					'price_total' => 0
				);
			}
			if ($value['group_code'] == 'SKU') {
				$attrs_data[$value['plan_id']]['price_total'] += $value['price_total'];
			}
			$attrs_data[$value['plan_id']][$value['attribute_id']] = $value['attr_number'];
		}
		
		$sql = "SELECT da.plan_id,da.value as attribute_id,count(*) as attr_number
				FROM cpm_attribute_data da 
				JOIN cpm_plan_order o on da.order_id = o.order_id 
				JOIN cpm_attribute a on da.attribute_id = a.attribute_id 
				WHERE o.is_deleted = 0 AND a.group_code = 'APPROACHING'";
		$sql = $sql.$condition." GROUP BY da.plan_id,da.value";
		
		$attrs = $this->pdb->query($sql)->rows;
		foreach ($attrs as $value) {
			$attrs_data[$value['plan_id']][$value['attribute_id']] = $value['attr_number'];
		}
		
		return $attrs_data;
	}
}