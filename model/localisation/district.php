<?php
class ModelLocalisationDistrict extends Model {
	public function getDistrict($district_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "local_district WHERE district_id = '" . (int)$district_id . "'");

		return $query->row;
	}

	public function getDistricts($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "local_district d WHERE 1";

		/*Filter start*/
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "d.district_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if (!empty($data['filter_prefix'])) {
			$implode[] = "d.prefix LIKE '%" . $this->db->escape($data['filter_prefix']) . "%'";
		}

		if (!empty($data['filter_province_id'])) {
			$implode[] = "d.province_id = '" . (int)$data['filter_province_id'] . "'";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/

			$sort_data = array(
				'd.district_name',
				'd.prefix'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY d.district_name";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			$districts = array();

			if($query->rows){
				foreach($query->rows as $row){
					$districts[$row['district_id']] = $row;
				}
			}

			return $districts;
		} else {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "local_district d ORDER BY d.district_name ASC");

			$query_data = $query->rows;

			return $query_data;
		}
	}

	public function getTotalDistricts($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "local_district d WHERE 1";

		/*Filter start*/
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "d.district_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if (!empty($data['filter_prefix'])) {
			$implode[] = "d.prefix LIKE '%" . $this->db->escape($data['filter_prefix']) . "%'";
		}

		if (!empty($data['filter_province_id'])) {
			$implode[] = "d.province_id = '" . (int)$data['filter_province_id'] . "'";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
		/*Filter end*/

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	public function getDistrictsByProvinceId($province_id) {

			$sql = "SELECT d.`district_id`,d.`name`,d.`district_name`,d.`longitude`,d.`latitude`,d.`geo_longitude`,d.`geo_latitude` FROM " . DB_PREFIX . "local_district d";


			if($province_id!=0&&$province_id!=''){
				$sql .= " WHERE d.province_id = '" . (int)$province_id . "'";
			}

			$sql .= " ORDER BY d.district_name";

			$query = $this->db->query($sql);

			$district_data = $query->rows;

		return $district_data;
	}

	public function getDistrictNameById($district_id) {
		$query = $this->db->query("SELECT district_name FROM " . DB_PREFIX . "local_district WHERE district_id = '" . (int)$district_id . "'");
		if(isset($query->row['district_name'])){
			return $query->row['district_name'];
		}
	}

	public function getDistrictByAlias($district_alias){
		$query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "local_district` WHERE `district_alias` = '" . $this->db->escape($district_alias) . "'");

		return $query->row;
	}
}