<?php
class ModelLocalisationPlace extends Model {
	public function getPlace($place_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "local_places` WHERE place_id = '" . (int)$place_id . "'");

		return $query->row;
	}

	public function getPlaces($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM `" . DB_PREFIX . "local_places` pl WHERE 1";		
			/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "pl.place_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if (!empty($data['filter_name'])) {
			$implode[] = "pl.place_alias LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if (!empty($data['filter_prefix'])) {
			$implode[] = "pl.prefix LIKE '%" . $this->db->escape($data['filter_prefix']) . "%'";
		}

		if (!empty($data['filter_province_id'])) {
			$implode[] = "pl.province_id = '" . (int)$data['filter_province_id'] . "'";
		}
		if (!empty($data['filter_district'])) {
			$implode[] = "pl.district_id = '" . (int)$data['filter_district'] . "'";
		}
		if (!empty($data['filter_ward'])) {
			$implode[] = "pl.ward_id = '" . (int)$data['filter_ward'] . "'";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 

			$sort_data = array(
				'pl.place_name'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY pl.place_name";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "local_places` pl ORDER BY pl.place_name ASC");
			$query_data = $query->rows;

			return $query_data;
		}
	}

	public function getTotalPlaces($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "local_places` pl WHERE 1";

			/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "pl.place_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if (!empty($data['filter_name'])) {
			$implode[] = "pl.place_alias LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if (!empty($data['filter_prefix'])) {
			$implode[] = "pl.prefix LIKE '%" . $this->db->escape($data['filter_prefix']) . "%'";
		}

		if (!empty($data['filter_province_id'])) {
			$implode[] = "pl.province_id = '" . (int)$data['filter_province_id'] . "'";
		}
		if (!empty($data['filter_district'])) {
			$implode[] = "pl.district_id = '" . (int)$data['filter_district'] . "'";
		}
		if (!empty($data['filter_ward'])) {
			$implode[] = "pl.ward_id = '" . (int)$data['filter_ward'] . "'";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 
			
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
	public function getPlacesByDistrictId($district_id) {
			$sql = "SELECT * FROM `" . DB_PREFIX . "local_places` pl";
			
			if($district_id!=0&&$district_id!=''){
				$sql .= " WHERE pl.district_id = '" . (int)$district_id . "'";
			}
			
			$sql .= " ORDER BY pl.place_name";
			
			$query = $this->db->query($sql);

			$place_data = $query->rows;

		return $place_data;
	}
	public function getPlacesByWardId($ward_id) {
			$sql = "SELECT * FROM `" . DB_PREFIX . "local_places` pl";
			
			if($ward_id!=0&&$ward_id!=''){
				$sql .= " WHERE pl.ward_id = '" . (int)$ward_id . "'";
			}
			
			$sql .= " ORDER BY pl.place_name";
			
			$query = $this->db->query($sql);

			$place_data = $query->rows;

		return $place_data;
	}
}