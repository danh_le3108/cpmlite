<?php
class ModelLocalisationLocation extends Model {
	private $province_ids = array(79);
	public function getProvinces() {
		$sql = "SELECT * FROM " . DB_PREFIX . "local_province p WHERE 1";
		if (!empty($this->province_ids)) {
			$sql .= ' AND province_id IN (' . implode(',', $this->province_ids) . ')';
		}
		$sql .= " ORDER BY name";
		return $this->pdb->query($sql)->rows;
	}
	public function getDistricts() {
		$sql = "SELECT * FROM " . DB_PREFIX . "local_district p WHERE 1";
		if (!empty($this->province_ids)) {
			$sql .= ' AND province_id IN (' . implode(',', $this->province_ids) . ')';
		}
		$sql .= " ORDER BY name";
		return $this->pdb->query($sql)->rows;
	}
	public function getWards() {
		$sql = "SELECT * FROM " . DB_PREFIX . "local_ward p WHERE 1";
		if (!empty($this->province_ids)) {
			$sql .= ' AND province_id IN (' . implode(',', $this->province_ids) . ')';
		}
		$sql .= " ORDER BY name";
		return $this->pdb->query($sql)->rows;
	}
	public function getPlaces() {
		$sql = "SELECT * FROM " . DB_PREFIX . "local_streets p WHERE 1";
		if (!empty($this->province_ids)) {
			$sql .= ' AND province_id IN (' . implode(',', $this->province_ids) . ')';
		}
		$sql .= " ORDER BY name";
		return $this->pdb->query($sql)->rows;
	}

	public function getDistrict($district_id = 0) {
		$sql = "SELECT * FROM " . DB_PREFIX . "local_district p WHERE 1";
		if (!empty($district_id)) {
			$sql .= ' AND district_id = ' . (int)$district_id . '';
		}
		return $this->pdb->query($sql)->row;
	}
	public function getWard($ward_id = 0) {
		$sql = "SELECT * FROM " . DB_PREFIX . "local_ward p WHERE 1";
		if (!empty($ward_id)) {
			$sql .= ' AND ward_id = ' . (int)$ward_id . '';
		}
		return $this->pdb->query($sql)->row;
	}
}