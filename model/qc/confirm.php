<?php
class ModelQcConfirm extends Model {

	public function getGroupConfirms() {
		$groups = $this->config->get('config_group_sign');
			$sql = "SELECT * FROM " . DB_PREFIX . "user_group WHERE 1";

		$sql .= " ORDER BY group_level";

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);
		$user_groups = array();
		
	   if($query->num_rows){
		foreach($query->rows as $group){
			if(in_array($group['user_group_id'],$groups)){
			$user_groups[$group['user_group_id']] = $group;	
			}
		}
	   }

			return $user_groups;
	}
}