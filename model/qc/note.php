<?php
class ModelQcNote extends Model {
	
	public static $static_notes = array();
	public function getAllNotes() {
		if (!empty(self::$static_notes)) {
			return self::$static_notes;
		}
		$list_notes = $this->config->get('config_note');
		
		$notes_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "note_type n ORDER BY n.sort_order ASC");

		foreach($query->rows as $row){
			if(in_array($row['note_id'], $list_notes)){
				$notes_data[] = $row;
			}
		}
	 	return self::$static_notes = $notes_data;
	}
	
	
	public function addNoteType($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "note_type SET 
		sort_order = '" . (int)$data['sort_order'] . "',
		note_name = '" . $this->db->escape($data['note_name']) . "'");
		
		return $this->db->getLastId();
	}

	public function editNoteType($note_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "note_type SET  
		note_name = '" . $this->db->escape($data['note_name']) . "',
		sort_order = '" . (int)$data['sort_order'] . "',
		WHERE note_id = '" . (int)$note_id . "'");
	}

	public function deleteNoteType($note_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "note_type WHERE note_id = '" . (int)$note_id . "'");
	}

	public function getNoteType($note_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "note_type WHERE note_id = '" . (int)$note_id . "'");

		return $query->row;
	}

	public function getNoteTypes($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "note_type p WHERE 1";
			
			/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "p.note_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 

			$sort_data = array(
				'p.note_name'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY p.sort_order";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			$notes = array();
			if($query->rows){
				foreach($query->rows as $row){
					$notes[$row['note_id']] = $row;
				}
			}

			return $row;
		} else {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "note_type p ORDER BY p.sort_order ASC");

			$query_data = $query->rows;

			return $query_data;
		}
	}

	public function getTotalNoteTypes($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "note_type p WHERE 1";
			
			/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "p.note_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 
			
			$query = $this->db->query($sql);

		return $query->row['total'];
	}
}