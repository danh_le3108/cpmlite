<?php

static $registry = null;
// we use the PHPExcel package from http://phpexcel.codeplex.com/
			$cwd = getcwd();
			chdir( DIR_SYSTEM.'PHPExcel' );
			require_once( 'Classes/PHPExcel.php' );
			chdir( $cwd );
// Error Handler
function error_handler_for_export_import($errno, $errstr, $errfile, $errline) {
	global $registry;

	switch ($errno) {
		case E_NOTICE:
		case E_USER_NOTICE:
			$errors = "Notice";
			break;
		case E_WARNING:
		case E_USER_WARNING:
			$errors = "Warning";
			break;
		case E_ERROR:
		case E_USER_ERROR:
			$errors = "Fatal Error";
			break;
		default:
			$errors = "Unknown";
			break;
	}

	$config = $registry->get('config');
	$url = $registry->get('url');
	$request = $registry->get('request');
	$session = $registry->get('session');
	$log = $registry->get('log');

	if ($config->get('config_error_log')) {
		$log->write('PHP ' . $errors . ':  ' . $errstr . ' in ' . $errfile . ' on line ' . $errline);
	}

	if (($errors=='Warning') || ($errors=='Unknown')) {
		return true;
	}

	if (($errors != "Fatal Error") && isset($request->get['route']) && ($request->get['route']!='user/user_import/download'))  {
		if ($config->get('config_error_display')) {
			echo '<b>' . $errors . '</b>: ' . $errstr . ' in <b>' . $errfile . '</b> on line <b>' . $errline . '</b>';
		}
	} else {
		$session->data['export_import_error'] = array( 'errstr'=>$errstr, 'errno'=>$errno, 'errfile'=>$errfile, 'errline'=>$errline );
		$token = $request->get['token'];
		$link = $url->link( 'user/user_import', 'token='.$token, 'SSL' );
		header('Status: ' . 302);
		header('Location: ' . str_replace(array('&amp;', "\n", "\r"), array('&', '', ''), $link));
		exit();
	}

	return true;
}


function fatal_error_shutdown_handler_for_export_import(){
	$last_error = error_get_last();
	if ($last_error['type'] === E_ERROR) {
		// fatal error
		error_handler_for_export_import(E_ERROR, $last_error['message'], $last_error['file'], $last_error['line']);
	}
}



class ModelExcelHandle extends Model {
	private $error = array();
	protected $null_array = array();

	public function getTemplate($template_id) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "excel_template WHERE template_id = '" . (int)$template_id . "'");

		if ($query->num_rows) {
			$template_info = array(
				'template_id'   => $query->row['template_id'],
				'template_name' => $query->row['template_name'],
				'table'         => json_decode($query->row['table'], true),
				'settings'      => json_decode($query->row['settings'], true),
				'file_path'     => $query->row['file_path']
			);
		}else{
			$template_info = array();
		}

		return $template_info;
	}
	public function template($template) {//$offset=null, $rows=null, $min_id=null, $max_id=null
		// $template = $this->getTemplate($template_id);
		// we use our own error handler
		global $registry;
		$registry = $this->registry;
		set_error_handler('error_handler_for_export_import',E_ALL);
		register_shutdown_function('fatal_error_shutdown_handler_for_export_import');

		// Use the PHPExcel package from http://phpexcel.codeplex.com/
		$cwd = getcwd();
		chdir( DIR_SYSTEM.'PHPExcel' );
		require_once( 'Classes/PHPExcel.php' );
		PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_ExportImportValueBinder() );
		chdir( $cwd );
		// Memory Optimization
		if ($this->config->get( 'export_import_settings_use_export_cache' )) {
			$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
			$cacheSettings = array( 'memoryCacheSize'  => '64MB' );
			PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
		}

		try {
			// set appropriate timeout limit
			set_time_limit( 1800 );
			// create a new workbook
			if(empty($template['file_path'])) {
				$workbook = new PHPExcel();

				// set some default styles
				$workbook->getDefaultStyle()->getFont()->setName('Arial');
				$workbook->getDefaultStyle()->getFont()->setSize(10);
				//$workbook->getDefaultStyle()->getAlignment()->setIndent(0.5);
				$workbook->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$workbook->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$workbook->getDefaultStyle()->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);

				// create the worksheets
				$worksheet_index = 0;
				$settings = $template['settings'];

				// creating the Customers worksheet
				$workbook->setActiveSheetIndex($worksheet_index++);
				$worksheet = $workbook->getActiveSheet();
				$worksheet->setTitle($template['template_name']);

				$this->populateTemplateWorksheet($settings,$worksheet);
				$worksheet->freezePaneByColumnAndRow( 1, 2 );

				$workbook->setActiveSheetIndex(0);

				// redirect output to client browser
				$filename = ucwords(str_replace(' ','-',$template['template_name'])).'.xlsx';

				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$filename.'"');
				header('Cache-Control: max-age=0');
				$objWriter = PHPExcel_IOFactory::createWriter($workbook, 'Excel2007');
				$objWriter->setPreCalculateFormulas(false);
				$objWriter->save('php://output');
			} else {
				$filename = DIR_MEDIA.$template['file_path'];
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.basename($filename).'"');
				header('Cache-Control: max-age=0');
				readfile($filename);
			}

			// Clear the spreadsheet caches
			$this->clearSpreadsheetCache();
			exit;
		} catch (Exception $e) {
			$errstr = $e->getMessage();
			$errline = $e->getLine();
			$errfile = $e->getFile();
			$errno = $e->getCode();
			$this->session->data['export_import_error'] = array( 'errstr'=>$errstr, 'errno'=>$errno, 'errfile'=>$errfile, 'errline'=>$errline );
			if ($this->config->get('config_error_log')) {
				$this->log->write('PHP ' . get_class($e) . ':  ' . $errstr . ' in ' . $errfile . ' on line ' . $errline);
			}
			return;
		}
	}
	protected function populateTemplateWorksheet(&$settings, &$worksheet) {

		// The heading row and column styles
		$styles = array();
		$data = array();
		$i = 1;
		$j = 0;
		// Set the column widths
		$heading_color = isset($settings['heading_color'])?str_replace('#','',$settings['heading_color']):'CCCCCC';
		 foreach($settings['tables'] as $table => $fields){
			foreach($fields as $field){
				if(isset($field['heading'])){
					$styles[$j] = array(
									'font'  => array( 'color' => array('rgb' => $heading_color)
									),
									'fill' => array(
										'type'      => PHPExcel_Style_Fill::FILL_SOLID,
										'color'     => array('rgb' => str_replace('#','',$field['heading_bg']))
									),
									'numberformat' => array(
										'code' => PHPExcel_Style_NumberFormat::FORMAT_TEXT
									)
								);;
					$worksheet->getColumnDimensionByColumn($j++)->setWidth(strlen($field['label'])+4);
					$data[$j] = $field['label'];
				}
			}
		 }
		$worksheet->getRowDimension($i)->setRowHeight(30);
		$this->setCellRow($worksheet, $i, $data,$styles);

	}

	public function download($template_id, $data, $filename = '') {
		$template_info = $this->getTemplate($template_id);
		// we use our own error handler
		global $registry;
		$registry = $this->registry;
		set_error_handler('error_handler_for_export_import',E_ALL);
		register_shutdown_function('fatal_error_shutdown_handler_for_export_import');

		// Use the PHPExcel package from http://phpexcel.codeplex.com/
		$cwd = getcwd();
		chdir( DIR_SYSTEM.'PHPExcel' );
		require_once( 'Classes/PHPExcel.php' );
		PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_ExportImportValueBinder() );
		chdir( $cwd );

		// Memory Optimization
		if ($this->config->get( 'export_import_settings_use_export_cache' )) {
			$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
			$cacheSettings = array( 'memoryCacheSize'  => '64MB' );
			PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
		}

		try {
			// set appropriate timeout limit
			set_time_limit( 1800 );
			// create a new workbook
			$workbook = new PHPExcel();

			// set some default styles
			$workbook->getDefaultStyle()->getFont()->setName('Arial');
			$workbook->getDefaultStyle()->getFont()->setSize(10);
			//$workbook->getDefaultStyle()->getAlignment()->setIndent(0.5);
			$workbook->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$workbook->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$workbook->getDefaultStyle()->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);


			// create the worksheets
			$worksheet_index = 0;
			// creating the Customers worksheet
			$workbook->setActiveSheetIndex($worksheet_index++);
			$worksheet = $workbook->getActiveSheet();
			$worksheet->setTitle($template_info['template_name']);
			$this->populateDataToWorksheet($worksheet, $template_info, $data);
			$worksheet->freezePaneByColumnAndRow( 1, 2 );

			$workbook->setActiveSheetIndex(0);

			// redirect output to client browser
			if(empty($filename)){
				$filename = ucwords(str_replace(' ', '-', $template_info['template_name'])).'-'.date('Y-m-d-H-i-s').'_'.$this->user->getUsername().'.xlsx';
			}

			if(file_exists(DIR_MEDIA.$filename)){
				@unlink(DIR_MEDIA.$filename);
			}

			if(!is_dir(DIR_MEDIA.'files/'. $this->config->get('config_project_folder') .'/export/')){
	    	@mkdir(DIR_MEDIA.'files/'. $this->config->get('config_project_folder') .'/export/', 0777, true);
	    }

			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($workbook, 'Excel2007');
			$objWriter->setPreCalculateFormulas(false);
			$objWriter->save(DIR_MEDIA.'files/'. $this->config->get('config_project_folder') .'/export/'.$filename);
			$objWriter->save('php://output');

			// Clear the spreadsheet caches
			$this->clearSpreadsheetCache();
			exit;

		} catch (Exception $e) {
			$errstr = $e->getMessage();
			$errline = $e->getLine();
			$errfile = $e->getFile();
			$errno = $e->getCode();
			$this->session->data['export_import_error'] = array( 'errstr'=>$errstr, 'errno'=>$errno, 'errfile'=>$errfile, 'errline'=>$errline );
			if ($this->config->get('config_error_log')) {
				$this->log->write('PHP ' . get_class($e) . ':  ' . $errstr . ' in ' . $errfile . ' on line ' . $errline);
			}
			return;
		}
	}

	public function downloadWithTemplate($template_id, $data, $row = 2){
		/*$cwd = getcwd();
		chdir( DIR_SYSTEM.'PHPExcel' );
		require_once( 'Classes/PHPExcel.php' );*/ 
		
		// SET TEMPLATE FILE
		$this->load->model('excel/template');
		$template_info = $this->model_excel_template->getTemplate($template_id);
		$template_file = $template_info['file_path'];
		$settings = $template_info['settings'];

		if(!file_exists(DIR_MEDIA.$template_file)){
			return;
		}

		// Clone Template
		$getDate = date('Y-m-d-H-i-s');

		if(!empty($template_info['template_name'])){
			$export_file_name = ucwords(str_replace(' ', '-', $template_info['template_name'])).'_'.$getDate.'_'.$this->user->getUsername().'.xlsx';
		} else {
			$export_file_name = 'export_plan_'.$getDate.'_'.$this->user->getUsername().'.xlsx';
		}

		$export_file = 'files/'. $this->config->get('config_project_folder') .'/export/'.$export_file_name;
		if(file_exists(DIR_MEDIA.$export_file)){
			@unlink(DIR_MEDIA.$export_file);
		}

		if(!is_dir(DIR_MEDIA.'files/'. $this->config->get('config_project_folder') .'/export/')){
    	@mkdir(DIR_MEDIA.'files/'. $this->config->get('config_project_folder') .'/export/', 0777, true);
    }

		if(copy(DIR_MEDIA.$template_file, DIR_MEDIA.$export_file)) {
			try {
				// $objReader = PHPExcel_IOFactory::createReader('Excel2007');
				$inputFileType = PHPExcel_IOFactory::identify(DIR_MEDIA.$export_file);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objReader->setReadDataOnly(false);
				$objPHPExcel = $objReader->load(DIR_MEDIA.$template_file);
				$objSheet = $objPHPExcel->getActiveSheet();
				$warray = array();
				foreach($data as $data_row) {
					$objSheet->fromArray($data_row, NULL, 'A'.$row, true);
					$i = 0;
					foreach($data_row as $k=>$v){
						$leng = utf8_strlen(trim($v));
						$warray[$i][$leng] = $leng;
						$i++;
					}
					$row++;
				}
				// Set the column widths
				foreach($warray as $col => $len){
					if(is_array($len)){
						$collen = max($len);
						$colwidth = $objSheet->getColumnDimensionByColumn($col)->getWidth();
						if($colwidth <= $collen){
							if($collen<=100){
								$nlen = $collen;
							}else{
								$nlen = $collen/2;
								$colin = PHPExcel_Cell::stringFromColumnIndex($col);
								$objSheet->getStyle($colin)->getAlignment()->setWrapText(true);
							}
							$objSheet->getColumnDimensionByColumn($col)->setWidth($nlen);
						}
					}
				}
								//$objSheet->getStyle('V')->getAlignment()->setWrapText(true);
								//$objSheet->getStyle('W')->getAlignment()->setWrapText(true);

				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$export_file_name.'"');
				header("Content-Transfer-Encoding: binary ");
				$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

				$objWriter->save(DIR_MEDIA.$export_file);
				$objWriter->save('php://output');
				// return DIR_MEDIA.$export_file;
				/*header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header("Content-Transfer-Encoding: binary ");
				header('Content-Disposition: attachment;filename="'.$export_file_name.'"');
				$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
				$objWriter->save(DIR_MEDIA.$export_file);*/
			} catch(Exception $e){
			  echo $e->__toString();
			}
		}

		// return $file;
	}


	protected function populateDataToWorksheet( &$worksheet, &$template_info, &$data) {
		$settings = $template_info['settings'];
		// The heading row and column styles
		$styles = array();
		$hdata = array();
		$i = 1;
		$j = 0;

		// Set the column widths
		$heading_color = isset($settings['heading_color'])?str_replace('#','',$settings['heading_color']):'CCCCCC';
		foreach($settings['tables'] as $table => $fields){
			foreach($fields as $field){
				if(isset($field['heading'])){
					$styles[$j] = array(
									'font'  => array( 'color' => array('rgb' => $heading_color)
									),
									'fill' => array(
										'type'      => PHPExcel_Style_Fill::FILL_SOLID,
										'color'     => array('rgb' => str_replace('#','',$field['heading_bg']))
									),
									'numberformat' => array(
										'code' => PHPExcel_Style_NumberFormat::FORMAT_TEXT
									)
								);;
					$worksheet->getColumnDimensionByColumn($j++)->setWidth(strlen($field['label'])+4);
					$hdata[$j] = $field['label'];
				}
			}
		 }
		$worksheet->getRowDimension($i)->setRowHeight(30);
		$this->setCellRow($worksheet, $i, $hdata,$styles);

		// The actual customers data
		$alldata = array();
		$i += 1;
		$j = 0;
		$len = count($data);
		$rdata = array();
		foreach ($data as $row) {
			$worksheet->getRowDimension($i)->setRowHeight(26);
			foreach ($row as $key => $val) {
				$rdata[$j++] = $val;
			}
			$this->setCellRow($worksheet, $i, $rdata);

			$i += 1;
			$j = 0;
		}
	}
	protected function setCellRow( $worksheet, $row, $data, &$styles=null ) {
		//$row
		/*1-based*/
		if (!empty($styles)) {
			foreach ($styles as $col=>$style) {
				$worksheet->getStyleByColumnAndRow($col,$row)->applyFromArray($style,false);
			}
		}
		$worksheet->fromArray( $data, null, 'A'.$row, true );
	}


	function getCell(&$worksheet,$row,$col,$default_val='') {
		$col -= 1; // we use 1-based, PHPExcel uses 0-based column index
		$row += 1; // we use 0-based, PHPExcel uses 1-based row index
		$val = ($worksheet->cellExistsByColumnAndRow($col,$row)) ? $worksheet->getCellByColumnAndRow($col,$row)->getValue() : $default_val;
		if ($val===null) {
			$val = $default_val;
		}
		return $val;
	}

	protected function isInteger($input){
		return(ctype_digit(strval($input)));
	}



	protected function clearCache() {
		$this->cache->delete('*');
	}
	protected function setColumnStyles( &$worksheet, &$styles, $min_row, $max_row ) {
		if ($max_row < $min_row) {
			return;
		}
		foreach ($styles as $col=>$style) {
			$from = PHPExcel_Cell::stringFromColumnIndex($col).$min_row;
			$to = PHPExcel_Cell::stringFromColumnIndex($col).$max_row;
			$range = $from.':'.$to;
			$worksheet->getStyle( $range )->applyFromArray( $style, false );
		}
	}



	protected function setCell( &$worksheet, $row/*1-based*/, $col/*0-based*/, $val, &$style=null ) {
		$worksheet->setCellValueByColumnAndRow( $col, $row, $val );
		if (!empty($style)) {
			$worksheet->getStyleByColumnAndRow($col,$row)->applyFromArray( $style, false );
		}
	}


	protected function clearSpreadsheetCache() {
		$files = glob(DIR_CACHE . 'Spreadsheet_Excel_Writer' . '*');

		if ($files) {
			foreach ($files as $file) {
				if (file_exists($file)) {
					@unlink($file);
					clearstatcache();
				}
			}
		}
	}
	public function getImportHistoriesByTemplateID($template_id){
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "excel_import_history WHERE `template_id` = '".(int)$template_id."'");
		return $query->rows;
	}
	public function getTotalImportHistories(){
		$query = $this->pdb->query("SELECT `template_id`,COUNT(*) FROM " . PDB_PREFIX . "excel_import_history GROUP BY `template_id`");
		return $query->rows;
	}
	public function getExportHistoriesByTemplateID($template_id){
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "excel_export_history WHERE `template_id` = '".(int)$template_id."'");

		return $query->rows;
	}
	public function getImportHistories(){
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "excel_import_history");

		return $query->rows;
	}

	public function addImportHistory($template_id, $template_path, $import_info){
		$this->pdb->query("INSERT INTO `" . PDB_PREFIX . "excel_import_history` SET
			`template_id` = '" . (int)$template_id . "',
			`user_id` ='" .(int)$this->user->getId(). "',
			`template_path` = '" . $template_path . "',
			`import_info` = '" . $this->db->escape(json_encode($import_info, true)) . "',
			`date_added` = NOW()");
		return $this->pdb->getLastId();
	}

	public function getImportHistory($history_id){
		$history_info = array();
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "excel_import_history WHERE `import_id` = '" . $history_id . "'");
			$history_info = array(
				'import_id' => $query->row['import_id'],
				'project_id' => $query->row['project_id'],
				'template_id' => $query->row['template_id'],
				'user_id' => $query->row['user_id'],
				'template_path' => $query->row['template_path'],
				'import_info' => json_decode($query->row['import_info'], true),
				'date_added' => $query->row['date_added'],
				'download' => $this->url->link('excel/template/download_error', 'import_id=' . $query->row['import_id'])
			);

		return $history_info;
	}

	public function updateHistoryProgress($history_id, $data){
		$this->pdb->query("UPDATE " .PDB_PREFIX. "excel_import_history SET `import_info` = '" . $this->db->escape(json_encode($data, true)) . "' WHERE `import_id` = '" . (int)$history_id . "'");
	}
}
?>
