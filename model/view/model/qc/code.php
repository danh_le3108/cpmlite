<?php
class ModelQcCode extends Model {

	public function getGroupCodes() {
		$codes_data = array();
		$config_code = $this->config->get('config_qc_code');
		$codes = $this->getCodes();
		$problems = $this->getProblems();
		foreach($codes as $code){
			$code_id = $code['code_id'];
			$codes_data[$code_id]['code_id'] = $code_id;
			$codes_data[$code_id]['code_name'] = $code['code_name'];
			
		}
		foreach($problems as $problem){
			$code_id = $problem['code_id'];
			$problem_id = $problem['problem_id'];
			if (in_array($problem_id, $config_code)) {
				$codes_data[$code_id]['problem'][$problem_id] = $problem;	
			}
		}
		return $codes_data;
	}
	public function getCode($code_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "qc_code WHERE code_id = '" . (int)$code_id . "'");

		return $query->row;
	}

	public function getCodes() {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "qc_code p ORDER BY p.code_name ASC");

			$query_data = $query->rows;

			return $query_data;
	}

	public function getTotalCodes($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "qc_code p WHERE 1";
			
			/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "p.code_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 
			
			$query = $this->db->query($sql);

		return $query->row['total'];
	}
	/* Problem */ 
	
	public function getProblem($problem_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "qc_problem WHERE problem_id = '" . (int)$problem_id . "'");

		return $query->row;
	}

	public function getProblems() {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "qc_problem d ORDER BY d.problem_name ASC");

			$query_data = $query->rows;

			return $query_data;
	}

	public function getTotalProblems($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "qc_problem d WHERE 1";
			
		/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "d.problem_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if (!empty($data['filter_code'])) {
			$implode[] = "d.code_id = '" . (int)$data['filter_code'] . "'";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
		/*Filter end*/ 

		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	public function getProblemsByCodeId($code_id) {

			$sql = "SELECT * FROM " . DB_PREFIX . "qc_problem";
			
			if($code_id!=0&&$code_id!=''){
				$sql .= " WHERE code_id = '" . (int)$code_id . "'";
			}
			
			$sql .= " ORDER BY problem_name";
			
			$query = $this->db->query($sql);

			$problem_data = $query->rows;

		return $problem_data;
	}
}