<?php
class ModelFcmFcm extends Model {

   public function getNotificationByUserId($user_id) {
  		$sql = "SELECT * FROM " . PDB_PREFIX . "fcm_message us WHERE `nguoi_nhan`=" . (int)$user_id . " OR type = 'send_to_all' ORDER BY date_added DESC LIMIT 0,100";
  		$query = $this->pdb->query($sql);
		$return = array();
		if($query->rows){
			 foreach($query->rows as $row){
				 $row['message'] = html_entity_decode($row['message'], ENT_QUOTES, 'UTF-8');
				$return[] = $row; 
			 }
		}
  		return $return;
  	}

    // messaging in a chat room / to persional message
   public function addMessage($data = array()) {
      if(!empty($data)){
        if(isset($data['nguoi_nhan'])){
          if(is_array($data['nguoi_nhan'])){
            $nguoi_nhan = implode(',', $data['nguoi_nhan']);
          } else {
            $nguoi_nhan = $data['nguoi_nhan'];
          }
        } else {
          $nguoi_nhan = '';
        }
        $user_group_id = $data['user_group_id'];

        $title = isset($data['title']) ? stripslashes($data['title']) : '';

   		  $sql = "INSERT INTO " . PDB_PREFIX . "fcm_message SET
            `user_id` = '" . (int)$data['user_id'] . "',
            `nguoi_nhan` = '".$this->db->escape($nguoi_nhan)."',
            `user_group_id` = '" . (int)$user_group_id . "',
            `title` = '" . $this->db->escape($title) . "',
            `message` = '" . $this->db->escape($data['message']) . "',
            `type` = '".$this->db->escape($data['type'])."',
            date_added = NOW()";
			
   		  $query = $this->pdb->query($sql);

        $message_id = $this->pdb->getLastId();

        return $message_id;
      } else {
        return false;
      }
    }

    public function updateStatusFcm($message_id){
      $sql = "UPDATE ".PDB_PREFIX."fcm_message SET status = '1' WHERE message_id = '".(int)$message_id."'";
      $this->pdb->query($sql);
    }

    public function getHistory($user_id, $type, $data = array()){
      $sql = "SELECT * FROM ".PDB_PREFIX."fcm_message WHERE `type` = '".$this->db->escape($type)."'";
      if(($this->user->getGroupId() > 1)){
         $sql .= " AND nguoi_nhan = '".(int)$user_id."'";
      } elseif($this->user->getGroupId() == 1 && !empty($data['filter_global'])){
         $this->load->model('user/user');
         $user = $this->model_user_user->getUserByUsername($data['filter_global']);
         $sql .= " AND nguoi_nhan = '".(int)$user['user_id']."'";
      }
      $sql .= " ORDER BY date_added DESC LIMIT 0,50";

      $query = $this->pdb->query($sql);
	// print_r('<pre>'); print_r($sql); print_r('</pre>'); 
	  
      $query_data = array();

         	$this->load->model('project/project_user');
			
      if($query->rows){
		 
         if($this->user->getGroupId() > 1){
            $users[$user_id] = $this->model_project_project_user->getUser($user_id);
         } else {
            $users = $this->model_project_project_user->getProjectUsers();
         }
         foreach($query->rows as $row){
            if(isset($users[$row['user_id']])){
               $user = $users[$row['user_id']];
               $query_data[] = array(
                  'message_id' => $row['message_id'],
                  'user_id' => $user['user_id'],
                  'username' => $user['username'],
                  'fullname' => $user['fullname'],
                  'title' => $row['title'],
                  'message' => html_entity_decode($row['message'], ENT_QUOTES, 'UTF-8'),
                  'type' => $row['type'],
                  'status' => $row['status'],
                  'date_added' => datetimeConvert($row['date_added'], 'd/m/Y H:i:s')
               );
            }
         }
      }
	 // print_r('<pre>'); print_r($query_data); print_r('</pre>'); 
      return $query_data;
   }


   public function getAllUsers() {
	   
	    $sql = "SELECT DISTINCT pu.*, u.fullname,u.username, u.usercode,u.image, u.email, u.telephone,
		  (SELECT ug.group_name FROM `" . DB_PREFIX . "user_group` ug WHERE ug.user_group_id = pu.project_user_group) AS group_name
			FROM " . DB_PREFIX . "project_user pu LEFT JOIN " . DB_PREFIX . "user u ON (u.user_id = pu.user_id)
			WHERE pu.project_id = '" . (int)$this->config->get('config_project_id') . "'
			AND pu.google_token != ''";
	
	
        $query = $this->db->query($sql);

        $query_data = array();

        foreach ($query->rows as $row){
           $query_data[$row['user_id']] = array(
              'user_id' => $row['user_id'],
              'google_token' => $row['google_token'],
              'username' => $row['username'],
              'fullname' => $row['fullname'],
              'name' => $row['fullname'],
              'email' => $row['email'],
              'group_name' => $row['group_name']
           );
        }

        return $query_data;
   }
}
