<?php
class ModelLocalisationProvince extends Model {

	public function getProvince($province_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "local_province WHERE province_id = '" . (int)$province_id . "'");

		return $query->row;
	}
	public function getAreaByCode($code) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "local_area WHERE area_code = '" . $code . "'");

		return $query->row;
	}
	public function getRegionByCode($code) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "local_region WHERE region_code = '" . $code . "'");

		return $query->row;
	}

	public function getProvincesIndexBy($index_by = 'code') {
		$query_data = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "local_province p");

		foreach ($query->rows as $row){
			$query_data[$row[$index_by]] = $row;
		}

		return $query_data;
	}
	public function getProvinces($data = array()) {

		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "local_province p WHERE 1";

			/*Filter start*/
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "p.province_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if (!empty($data['filter_province_ids'])) {
			//$implode[] = "p.province_id IN (" . $this->db->escape($data['filter_province_ids']) . ")";
		}
		if (!empty($data['filter_prefix'])) {
			$implode[] = "p.prefix LIKE '%" . $this->db->escape($data['filter_prefix']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/

			$sort_data = array(
				'p.province_name',
				'p.prefix',
				'p.code'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY p.province_id";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}
			$query = $this->db->query($sql);

			$provinces = array();
			if($query->rows){
				foreach($query->rows as $row){
					$provinces[$row['province_id']] = $row;
				}
			}

			return $provinces;
		} else {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "local_province p ORDER BY p.province_id ASC");

			$query_data = $query->rows;

			return $query_data;
		}
	}

	public function getTotalProvinces($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "local_province p WHERE 1";

			/*Filter start*/
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "p.province_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if (!empty($data['filter_province_ids'])) {
			//$implode[] = "p.province_id IN (" . $this->db->escape($data['filter_province_ids']) . ")";
		}
		if (!empty($data['filter_prefix'])) {
			$implode[] = "p.prefix LIKE '%" . $this->db->escape($data['filter_prefix']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/

			$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getProvinceByAlias($province_alias){
		$query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "local_province` WHERE `province_alias` = '" . $this->db->escape($province_alias) . "'");

		return $query->row;
	}

	public function getProvinceNameById($province_id) {
		$query = $this->db->query("SELECT province_name FROM " . DB_PREFIX . "local_province WHERE province_id = '" . (int)$province_id . "'");
		if(isset($query->row['province_name'])){
			return $query->row['province_name'];
		}
	}
}