<?php
class ModelLocalisationReasonNotPass extends Model {
	public function addReason($data) {
		$this->pdb->query("INSERT INTO " . PDB_PREFIX . "catalog_reason_not_pass SET 
		sort_order = '" . (int)$data['sort_order'] . "', 
		reason_name = '" . $this->pdb->escape($data['reason_name']) . "'");
		return $this->pdb->getLastId();
	}

	public function editReason($not_pass_id, $data) {
		$this->pdb->query("UPDATE " . PDB_PREFIX . "catalog_reason_not_pass SET  
		sort_order = '" . (int)$data['sort_order'] . "', 
		reason_name = '" . $this->pdb->escape($data['reason_name']) . "'
		WHERE not_pass_id = '" . (int)$not_pass_id . "'");
	}

	public function deleteReason($not_pass_id) {
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "catalog_reason_not_pass WHERE not_pass_id = '" . (int)$not_pass_id . "'");
	}

	public function getReason($not_pass_id) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "catalog_reason_not_pass WHERE not_pass_id = '" . (int)$not_pass_id . "'");

		return $query->row;
	}

	public function getReasons($is_index = 0, $data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . PDB_PREFIX . "catalog_reason_not_pass a WHERE 1";

			/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "a.reason_name LIKE '%" . $this->pdb->escape($data['filter_name']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 
			
			$sort_data = array(
				'a.reason_name'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY a.sort_order";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

		
		} else {
			$sql = "SELECT * FROM " . PDB_PREFIX . "catalog_reason_not_pass a ORDER BY a.sort_order ASC";
		}
		$query = $this->pdb->query($sql);

			$reasons = array();
			if($query->rows){
				foreach($query->rows as $row){
					if($is_index==1){
						$reasons[$row['not_pass_id']] = $row;
					}else{
						$reasons[] = $row;
					}
				}
			}

			return $reasons;
	}

	public function getTotalReason($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "catalog_reason_not_pass a WHERE 1";
		
			/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "a.reason_name LIKE '%" . $this->pdb->escape($data['filter_name']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 

		$query = $this->pdb->query($sql);
		return $query->row['total'];
	}
}