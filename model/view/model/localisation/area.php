<?php
class ModelLocalisationArea extends Model {
	public function addArea($data) {
		$this->pdb->query("INSERT INTO " . PDB_PREFIX . "local_area SET
		area_name = '" . $this->pdb->escape($data['area_name']) . "',
		area_code = '" . $this->pdb->escape($data['area_code']) . "',
		sort_order = '" .(int)$data['sort_order'] . "'");
		return $this->pdb->getLastId();
	}

	public function editArea($area_id, $data) {
		$this->pdb->query("UPDATE " . PDB_PREFIX . "local_area SET
		area_name = '" . $this->pdb->escape($data['area_name']) . "',
		area_code = '" . $this->pdb->escape($data['area_code']) . "',
		sort_order = '" .(int)$data['sort_order'] . "'
		WHERE area_id = '" . (int)$area_id . "'");
	}

	public function deleteArea($area_id) {
		$this->pdb->query("DELETE FROM " . DB_PREFIX . "local_area WHERE area_id = '" . (int)$area_id . "'");
	}

	public function getArea($area_id) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "local_area WHERE area_id = '" . (int)$area_id . "'");

		return $query->row;
	}
	public function getAreaByCode($code) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "local_area WHERE area_code = '" . $code . "'");

		return $query->row;
	}

	public function getAreas($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . PDB_PREFIX . "local_area a WHERE 1";

			/*Filter start*/
		$implode = array();

		if (!empty($data['filter_area_codes'])) { 
			$implode[] = "a.area_code IN (" .$data['filter_area_codes'] . ")";
		}
		if (!empty($data['filter_name'])) {
			$implode[] = "a.area_name LIKE '%" . $this->pdb->escape($data['filter_name']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/

			$sort_data = array(
				'a.area_name'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY a.sort_order,a.area_id";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->pdb->query($sql);

			return $query->rows;
		} else {
			$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "local_area a ORDER BY a.sort_order,a.area_id ASC");

			$query_data = $query->rows;

			return $query_data;
		}
	}

	public function getTotalAreas($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "local_area a WHERE 1";

			/*Filter start*/
		$implode = array();
		if (!empty($data['filter_area_codes'])) { 
			$implode[] = "a.area_code IN (" .$data['filter_area_codes'] . ")";
		}
		if (!empty($data['filter_name'])) {
			$implode[] = "a.area_name LIKE '%" . $this->pdb->escape($data['filter_name']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/

		$query = $this->pdb->query($sql);
		return $query->row['total'];
	}

 	public function getAreasIndexBy($index_by = 'area_code') {
	  $query_data = array();
	  $query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "local_area p");

	  foreach ($query->rows as $row){
	   	$query_data[$row[$index_by]] = $row;
	  }

	  return $query_data;
 	}
}