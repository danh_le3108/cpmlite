<?php
class ModelLocalisationWard extends Model {
	public function getWard($ward_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "local_ward WHERE ward_id = '" . (int)$ward_id . "'");

		return $query->row;
	}

	public function getWards($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "local_ward w WHERE 1";
			/*Filter start*/
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "w.ward_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if (!empty($data['filter_prefix'])) {
			$implode[] = "w.prefix LIKE '%" . $this->db->escape($data['filter_prefix']) . "%'";
		}

		if (!empty($data['filter_province_id'])) {
			$implode[] = "w.province_id = '" . (int)$data['filter_province_id'] . "'";
		}
		if (!empty($data['filter_district'])) {
			$implode[] = "w.district_id = '" . (int)$data['filter_district'] . "'";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/

			$sort_data = array(
				'w.ward_name'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY w.ward_name";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);
			$wards = array();
			if($query->rows){
				foreach($query->rows as $row){
					$wards[$row['ward_id']] = $row;
				}
			}

			return $wards;
		} else {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "local_ward w");
			$query_data = $query->rows;

			return $query_data;
		}
	}

	public function getTotalWards($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "local_ward w WHERE 1";

/*Filter start*/
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "w.ward_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if (!empty($data['filter_prefix'])) {
			$implode[] = "w.prefix LIKE '%" . $this->db->escape($data['filter_prefix']) . "%'";
		}

		if (!empty($data['filter_province_id'])) {
			$implode[] = "w.province_id = '" . (int)$data['filter_province_id'] . "'";
		}
		if (!empty($data['filter_district'])) {
			$implode[] = "w.district_id = '" . (int)$data['filter_district'] . "'";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/

		$query = $this->db->query($sql);
		return $query->row['total'];
	}
	public function getWardsByDistrictId($district_id) {
			$sql = "SELECT  w.`ward_id`,w.`name`,w.`ward_name`,w.`longitude`,w.`latitude`,w.`geo_longitude`,w.`geo_latitude` FROM " . DB_PREFIX . "local_ward w";
			if($district_id!=0&&$district_id!=''){
				$sql .= " WHERE w.district_id = '" . (int)$district_id . "'";
			}

			$sql .= " ORDER BY w.ward_name";

			$query = $this->db->query($sql);

			$ward_data = $query->rows;

		return $ward_data;
	}

	public function getWardNameById($ward_id) {
		$query = $this->db->query("SELECT ward_name FROM " . DB_PREFIX . "local_ward WHERE ward_id = '" . (int)$ward_id . "'");
		if(isset($query->row['ward_name'])){
			return $query->row['ward_name'];
		}
	}

	public function getWardByAlias($ward_alias){
		$query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "local_ward` WHERE `ward_alias` = '" . $this->db->escape($ward_alias) . "'");

		return $query->row;
	}
}