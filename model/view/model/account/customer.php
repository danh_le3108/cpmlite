<?php
class ModelAccountCustomer extends Model {
	public function addCustomer($data) {
		$customer_group_id = $this->config->get('config_customer_group_id');
		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

		$this->pdb->query("INSERT INTO " . PDB_PREFIX . "customer_user SET 
		language_id = '" . (int)$this->config->get('config_language_id') . "', 
		fullname = '" . $this->db->escape($data['fullname']) . "', 
		email = '" . $this->db->escape($data['email']) . "', 
		telephone = '" . $this->db->escape($data['telephone']) . "', 
		salt = '" . $this->db->escape($salt = token(9)) . "', 
		password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', 
		real_token = '" . $this->db->escape(base64_encode(base64_encode($data['password']))) . "',
		ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', 
		status = '1', 
		approved = '" . (int)!$customer_group_info['approval'] . "', 
		date_added = NOW()");

		$customer_user_id = $this->pdb->getLastId();

		$this->load->language('mail/customer');

		$subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

		$message = sprintf($this->language->get('text_welcome'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";

		if (!$customer_group_info['approval']) {
			$message .= $this->language->get('text_login') . "\n";
		} else {
			$message .= $this->language->get('text_approval') . "\n";
		}

		$message .= $this->url->link('account/login', '', true) . "\n\n";
		$message .= $this->language->get('text_services') . "\n\n";
		$message .= $this->language->get('text_thanks') . "\n";
		$message .= html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');

		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		$mail->setTo($data['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
		$mail->setSubject($subject);
		$mail->setText($message);
		$mail->send();

		// Send to main admin email if new account email is enabled
		if (in_array('account', (array)$this->config->get('config_mail_alert'))) {
			$message  = $this->language->get('text_signup') . "\n\n";
			$message .= $this->language->get('text_website') . ' ' . html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8') . "\n";
			$message .= $this->language->get('text_fullname') . ' ' . $data['fullname'] . "\n";
			$message .= $this->language->get('text_customer_group') . ' ' . $customer_group_info['group_name'] . "\n";
			$message .= $this->language->get('text_email') . ' '  .  $data['email'] . "\n";
			$message .= $this->language->get('text_telephone') . ' ' . $data['telephone'] . "\n";

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode($this->language->get('text_new_customer'), ENT_QUOTES, 'UTF-8'));
			$mail->setText($message);
			$mail->send();

			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_alert_email'));

			foreach ($emails as $email) {
				if (utf8_strlen($email) > 0 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}

		return $customer_user_id;
	}

	public function editCustomer($data) {
		$customer_user_id = $this->customer->getId();

		$this->pdb->query("UPDATE " . PDB_PREFIX . "customer_user SET 
		email = '" . $this->db->escape($data['email']) . "', 
		telephone = '" . $this->db->escape($data['telephone']) . "'
		WHERE customer_user_id = '" . (int)$customer_user_id . "'");
		
		if(isset($data['fullname'])){
			$this->pdb->query("UPDATE " . PDB_PREFIX . "customer_user SET fullname = '" . $this->db->escape($data['fullname']) . "' WHERE customer_user_id = '" . (int)$customer_user_id . "'");
		}
		
	}

	public function editPassword($email, $password) {
		$this->pdb->query("UPDATE " . PDB_PREFIX . "customer_user SET 
		salt = '" . $this->db->escape($salt = token(9)) . "', 
		password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "', 
		real_token = '" . $this->db->escape(base64_encode(base64_encode($password))) . "',
		code = '' WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}

	public function editCode($email, $code) {
		$this->pdb->query("UPDATE `" . PDB_PREFIX . "customer_user` SET code = '" . $this->db->escape($code) . "' WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}

	public function getCustomer($customer_user_id) {
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "customer_user WHERE customer_user_id = '" . (int)$customer_user_id . "'");

		return $query->row;
	}

	public function getCustomerByUsername($username) {
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "customer_user WHERE `username` = '" . $this->pdb->escape($username) . "'");

		return $query->row;
	}

	public function getCustomerByEmail($email) {
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "customer_user WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

	public function getCustomerByCode($code) {
		$query = $this->pdb->query("SELECT customer_user_id, fullname, email FROM `" . PDB_PREFIX . "customer_user` WHERE code = '" . $this->db->escape($code) . "' AND code != ''");

		return $query->row;
	}

	public function getCustomerByToken($token) {
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "customer_user WHERE token = '" . $this->db->escape($token) . "' AND token != ''");

		$this->pdb->query("UPDATE " . PDB_PREFIX . "customer_user SET token = ''");

		return $query->row;
	}

	public function getTotalCustomersByEmail($email) {
		$query = $this->pdb->query("SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "customer_user WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row['total'];
	}

	public function addLoginAttempt($username) {
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "customer_login WHERE customer_user_id = '" .(int)$this->customer->getId() . "' AND DATE(`date_added`) = DATE('" . $this->db->escape(date('Y-m-d')) . "')");

		if (!$query->num_rows) {
			$this->pdb->query("INSERT INTO " . PDB_PREFIX . "customer_login SET customer_user_id = '" . (int)$this->customer->getId() . "', customer_level_id = '" . (int)$this->customer->getGroupId() . "',  username = '" . $this->db->escape((string)$username) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', total = 1, date_added = '" . $this->db->escape(date('Y-m-d H:i:s')) . "', date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "'");
		} else {
			$this->pdb->query("UPDATE " . PDB_PREFIX . "customer_login SET customer_user_id = '" . (int)$this->customer->getId() . "', total = (total + 1), date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "' WHERE customer_login_id = '" . (int)$query->row['customer_login_id'] . "'");
		}
	}

	public function getLoginAttempts($username) {
		$query = $this->pdb->query("SELECT * FROM `" . PDB_PREFIX . "customer_login` WHERE username = '" . $this->db->escape($username) . "'");

		return $query->row;
	}

	public function deleteLoginAttempts($username) {
		$this->pdb->query("DELETE FROM `" . PDB_PREFIX . "customer_login` WHERE username = '" . $this->db->escape($username) . "'");
	}
}
