<?php
class ModelAccountCustomerGroup extends Model {
	public function getCustomerGroup($customer_group_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer_group cg WHERE cg.customer_group_id = '" . (int)$customer_group_id . "'");

		return $query->row;
	}
	public function getCustomerGroups($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "customer_group cg WHERE 1";

		$query = $this->db->query($sql);

		return $query->rows;
	}
}