<?php
class ModelExcelTemplate extends Model {
	public function getTables() {
		$table_data = array();
		$query = $this->db->query("SHOW TABLES FROM `" . DB_DATABASE. "`");

		foreach ($query->rows as $result) {
			if (utf8_substr($result['Tables_in_' . DB_DATABASE], 0, strlen(DB_PREFIX)) == DB_PREFIX) {
				if (isset($result['Tables_in_' . DB_DATABASE])) {
					$table_data[] = $result['Tables_in_' . DB_DATABASE];
				}
			}
		}


		$query = $this->pdb->query("SHOW TABLES FROM `" . $this->config->get('project_db') . "`");

		foreach ($query->rows as $result) {
			if (utf8_substr($result['Tables_in_' . $this->config->get('project_db')], 0, strlen(DB_PREFIX)) == DB_PREFIX) {
				if (isset($result['Tables_in_' . $this->config->get('project_db')])) {
					$table_data[] = $result['Tables_in_' . $this->config->get('project_db')];
				}
			}
		}

		return $table_data;
	}

	public function getTablesFromProject() {
		$table_data = array();

		$query = $this->pdb->query("SHOW TABLES FROM `" . $this->config->get('project_db') . "`");

		foreach ($query->rows as $result) {
			if (utf8_substr($result['Tables_in_' . $this->config->get('project_db')], 0, strlen(DB_PREFIX)) == DB_PREFIX) {
				if (isset($result['Tables_in_' . $this->config->get('project_db')])) {
					$table_data[] = $result['Tables_in_' . $this->config->get('project_db')];
				}
			}
		}

		return $table_data;
	}

	public function getFields($table) {
		$field_data = array();
		$table_data = $this->getTablesFromProject();

		if(in_array(DB_PREFIX.$table, $table_data)){
			$query = $this->pdb->query("SHOW COLUMNS FROM `" . DB_PREFIX .$table . "`");
		} else {
			$query = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX .$table . "`");
		}

		foreach ($query->rows as $result) {
				if (isset($result['Field'])) {
					$field_data[] = array(
					'field'=>$result['Field'],
					'label'=>ucwords(str_replace('_',' ',$result['Field']))
					);
				}
		}
		return $field_data;
	}

	public function addTemplate($data) {
		$settings = isset($data['settings']) ? json_encode($data['settings']) : '';
		$table = isset($data['table']) ? json_encode($data['table']) : '';
		$sql = "INSERT INTO " . PDB_PREFIX . "excel_template SET
		`template_name` = '" . $this->db->escape($data['template_name']) . "',
		`table` = '" . $this->db->escape($table) . "',
		`settings` = '" . $this->db->escape($settings). "',
		`date_added` = NOW()";
		$this->pdb->query($sql);
		$template_id = $this->pdb->getLastId();
		return $template_id;
	}

	public function editTemplate($template_id, $data) {
		$settings = isset($data['settings']) ? json_encode($data['settings']) : '';
		$table = isset($data['table']) ? json_encode($data['table']) : '';
		$sql = "UPDATE " . PDB_PREFIX . "excel_template SET
		`template_name` = '" . $this->pdb->escape($data['template_name']) . "',
		`table` = '" . $table . "',
		`settings` = '" . $this->pdb->escape($settings). "',
		`date_modified` = NOW()
		WHERE template_id = '" . (int)$template_id . "'";
		$this->pdb->query($sql);
	}

	public function deleteTemplate($template_id) {
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "excel_template WHERE template_id = '" . (int)$template_id . "'");
	}

	public function getTemplate($template_id) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "excel_template WHERE template_id = '" . (int)$template_id . "'");

		if ($query->num_rows) {
			$template_info = array(
				'template_id' 	=> $query->row['template_id'],
				'template_name'       	=> $query->row['template_name'],
				'table'      	=> json_decode($query->row['table'], true),
				'settings' 		=> json_decode($query->row['settings'], true),
				'file_path' => $query->row['file_path']
			);
		}else{
			$template_info = array();
		}

		return $template_info;
	}

	public function getTemplates($data = array()) {
		$sql = "SELECT * FROM " . PDB_PREFIX . "excel_template WHERE 1";

		$sort_data = array('template_name');

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY template_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->pdb->query($sql);

		return $query->rows;
	}
	public function getTotalTemplates() {
		$query = $this->pdb->query("SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "excel_template WHERE 1");

		return $query->row['total'];
	}

	public function updateFilepath($template_id, $path){
		$sql = "UPDATE " . PDB_PREFIX . "excel_template SET `file_path` = '".$this->db->escape($path)."' WHERE `template_id` = '".(int)$template_id."'";
		$this->pdb->query($sql);
	}
}