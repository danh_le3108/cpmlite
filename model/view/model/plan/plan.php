<?php
class ModelPlanPlan extends Model {
	private $no_filter_key = array(
		'_url',
		'route',
		'page',
		'filter_global',
		'start',
		'limit',
		'sort',
		'order',
		'user_id',
		'sup_id',
		'date_from',
		'date_to'
	);

	private $filter_global = array(
		's.store_name',
		's.store_code',
		's.store_address',
		'pl.plan_id',
		'pl.store_id',
		// 'pl.usercode',
		'pl.plan_name',
		// 'pl.round_name',
	);

	public function countTotal($data = array(), $field = '', $ignore = array()) {
		$select = !empty($field) ? $field.',' : '';
		$sql = "SELECT {$select} count(*) as total FROM " . DB_PREFIX . "plan pl
				LEFT JOIN " . DB_PREFIX . "store s ON pl.store_id = s.store_id WHERE pl.is_deleted = 0";
				
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				if (!in_array($key, $this->no_filter_key)) {
					$key = str_replace('filter_', '', $key);
					$sql .= " AND {$key}= '" . $this->pdb->escape($value) . "'";
				}	
			}

			if (!empty($data['filter_global'])) {
				$sql .= " AND (";
				foreach ($this->filter_global as $filter) {
					$implode[] = " LCASE(".$filter.") LIKE '%" . $this->pdb->escape(utf8_strtolower($data['filter_global'])) . "%'";
				}
				$sql .= " " . implode(" OR ", $implode) . "";
				$sql .= ")";
			}

			if (!empty($data['date_from'])) {
				$sql .= " AND date >= '" . $this->pdb->escape($data['date_from']) . "'";
			}
			if (!empty($data['date_to'])) {
				$sql .= " AND date <= '" . $this->pdb->escape($data['date_to']) . "'";
			}
		}

		if (!empty($data['user_id']) || !empty($data['sup_id']) || ($this->user->isLogged() && $this->user->getGroupId() > $this->config->get('config_user_pa'))) {
			$condition = array();
			$user_sql = "SELECT plan_id FROM cpm_plan_user WHERE 1";
			if ($this->user->getGroupId() > $this->config->get('config_user_pa')) {
				$users_manager = $this->user->users_manager();
				$condition[] = !empty($users_manager) ? "user_id IN (" . implode(',', $this->user->users_manager()) . ")" : "user_id = 0";
			}
			if (!empty($data['sup_id'])) {
				$condition[] = "sup_id = " . (int)$data['sup_id'];
			}
			if (!empty($data['user_id'])) {
				$condition[] = "user_id = " . (int)$data['user_id'];
			}
			if (!empty($condition)) {
				$user_sql .= ' AND '.implode(' AND ', $condition);
			}
			$sql .= " AND pl.plan_id IN ($user_sql)";
		
		}

		// if ($this->customer->isLogged() || ($this->user->getGroupId() != 1 && $this->user->isLogged())) {
		// 	$sql .= " AND pl.plan_id IN (SELECT plan_id FROM cpm_plan_user WHERE user_id NOT IN (3519,3450,4884))";
		// }
		
		if ($field) {
			$sql .= " GROUP BY " . $field;
			$data = array();
			foreach ($this->pdb->query($sql)->rows as $value) {
				$data[$value[$field]] = $value['total'];
			}
			return $data;
		} else {
			return $this->pdb->query($sql)->row['total'];
		}
	}

	public function countGift($data = array()) {
		$sql = "SELECT sum(gift_11) as gift_11, sum(gift_12) as gift_12, sum(gift_13) as gift_13, sum(gift_14) as gift_14, sum(gift_36) as gift_36
				FROM " . DB_PREFIX . "plan pl
				LEFT JOIN " . DB_PREFIX . "store s ON pl.store_id = s.store_id WHERE pl.is_deleted = 0";
				
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				if (!in_array($key, $this->no_filter_key)) {
					$key = str_replace('filter_', '', $key);
					$sql .= " AND {$key}= '" . $this->pdb->escape($value) . "'";
				}	
			}
		}
		
		return $this->pdb->query($sql)->row;
	}

	public function getPlans($data = array()) { 
		$sql = "SELECT * FROM " . DB_PREFIX . "plan pl
				LEFT JOIN " . DB_PREFIX . "store s ON pl.store_id = s.store_id WHERE pl.is_deleted = 0";
		
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				if (!in_array($key, $this->no_filter_key)) {
					$key = str_replace('filter_', '', $key);
					$sql .= " AND {$key}= '" . $this->pdb->escape($value) . "'";
				}	
			}

			if (!empty($data['filter_global'])) {
				$sql .= " AND (";
				foreach ($this->filter_global as $filter) {
					$implode[] = " LCASE(".$filter.") LIKE '%" . $this->pdb->escape(utf8_strtolower($data['filter_global'])) . "%'";
				}
				$sql .= " " . implode(" OR ", $implode) . "";
				$sql .= ")";
			}

			if (!empty($data['date_from'])) {
				$sql .= " AND date >= '" . $this->pdb->escape($data['date_from']) . "'";
			}
			if (!empty($data['date_to'])) {
				$sql .= " AND date <= '" . $this->pdb->escape($data['date_to']) . "'";
			}
		}

		if (!empty($data['user_id']) || !empty($data['sup_id']) || ($this->user->isLogged() && $this->user->getGroupId() > $this->config->get('config_user_pa'))) {
			$condition = array();
			$user_sql = "SELECT plan_id FROM cpm_plan_user WHERE 1";
			if ($this->user->getGroupId() > $this->config->get('config_user_pa')) {
				$users_manager = $this->user->users_manager();
				$condition[] = !empty($users_manager) ? "user_id IN (" . implode(',', $this->user->users_manager()) . ")" : "user_id = 0";
			}
			if (!empty($data['sup_id'])) {
				$condition[] = "sup_id = " . (int)$data['sup_id'];
			}
			if (!empty($data['user_id'])) {
				$condition[] = "user_id = " . (int)$data['user_id'];
			}
			if (!empty($condition)) {
				$user_sql .= ' AND '.implode(' AND ', $condition);
			}
			$sql .= " AND pl.plan_id IN ($user_sql)";
		
		}

		// if ($this->customer->isLogged() || ($this->user->getGroupId() != 1 && $this->user->isLogged())) {
		// 	$sql .= " AND pl.plan_id IN (SELECT plan_id FROM cpm_plan_user WHERE user_id NOT IN (3519,3450,4884))";
		// }

		if (!empty($data['sort'])) {
			$sql .= ' ORDER BY ' . $data['sort'];
		} else {
			$sql .= ' ORDER BY pl.plan_id DESC';
		}

		if (isset($data['start']) && isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		return $this->pdb->query($sql)->rows;
	}

	public function getPlan($plan_id) {
		$sql = "SELECT * FROM " . DB_PREFIX . "plan pl
				LEFT JOIN " . DB_PREFIX . "store s ON pl.store_id = s.store_id WHERE pl.is_deleted = 0 AND plan_id = " . (int)$plan_id;

		if ($this->user->isLogged() && $this->user->getGroupId() > $this->config->get('config_user_pa')) {
			$condition = array();
			$user_sql = "SELECT plan_id FROM cpm_plan_user WHERE 1";
			$users_manager = $this->user->users_manager();
			if (!empty($users_manager)) {
				$user_sql .= " AND user_id IN (" . implode(',', $this->user->users_manager()) . ")";
			} else {
				$user_sql .= " AND user_id = 0";
			}
			$sql .= " AND pl.plan_id IN ($user_sql)";
		}
		// p($sql);
		return $this->pdb->query($sql)->row;
	}
	
	public function deletePlan($plan_id) {
		$this->pdb->query("UPDATE `" . PDB_PREFIX . "plan` SET is_deleted = '1', user_deleted = '" . $this->pdb->escape($this->user->getId()) . "' WHERE plan_id = '" . (int)$plan_id . "'");
	}

	public function updatePlan($plan_id, $data) {
		$sql = "UPDATE " . PDB_PREFIX . "plan SET ";
		if(isset($data['reason_id'])){
			$sql .= " `reason_id` = '" . (int)$data['reason_id'] . "',";
			if((int)$data['reason_id']>0) {
				$sql .= " `plan_rating` = '-2',";
				// $this->updatePlanNotPass($plan_id,$data);
			} else {
				$sql .= "`plan_rating` = '1',";
			}
			unset($data['reason_id']);
		}
		
		foreach ($data as $key => $value) {
			$sql .= "{$key} = '" . $this->pdb->escape($value) . "',";
		}

		$sql .= "date_modified = NOW() WHERE plan_id = '" . (int)$plan_id . "'";
		$this->pdb->query($sql);

	}

	public function addPlan($data) {
		$sql = "INSERT INTO " . PDB_PREFIX . "plan SET ";
		foreach ($data as $key => $value) {
			$sql .= "{$key} = '" . $this->pdb->escape($value) . "',";
		}
		$sql .= "date_added = NOW()";
		$this->pdb->query($sql);
		return $this->pdb->getLastId();
	}

	//PLAN NOTE
	public function getPlanNotes($data = array()) {
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "plan_note qn WHERE qn.is_deleted = '0' ORDER BY qn.date_added ASC ");
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				$sql .= "{$key} = '" . $this->pdb->escape($value) . "',";
			}
		}
		return $query->rows;
	}

	public function getPlanNotesByPlanId($plan_id) {
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "plan_note qn WHERE qn.is_deleted = '0' AND qn.plan_id = '" . (int)$plan_id . "' ORDER BY qn.date_added ASC ");
		return $query->rows;
	}

	public function updatePlanNote($plan_id,$data) {
		// if($data['note_id']==1){
		// 	$this->pdb->query("UPDATE " . PDB_PREFIX . "plan SET is_fix = '1' WHERE plan_id = '" . (int)$plan_id . "'");
		// }
		$comment = isset($data['comment'])?$data['comment']:'';
		$sql = "INSERT INTO " . PDB_PREFIX . "plan_note SET
		round_name = '" . $this->db->escape($data['round_name']) . "',
		plan_id = '" . (int)$plan_id . "',
		note_id = '" . (int)$data['note_id'] . "',
		comment = '" . $this->db->escape($comment) . "',
		user_id= '" .(int)$this->user->getId(). "',
		username = '" . $this->db->escape($this->user->getUserName()) . "',
		user_group_id= '" .(int)$this->user->getGroupId(). "', date_added = NOW()";
		$this->pdb->query($sql);
		return $this->pdb->getLastId();
	}

	public function deletePlanNotes($qc_note_id) {
		$sql = "UPDATE " . DB_PREFIX . "plan_note SET is_deleted = 1, user_deleted = " . $this->user->getId() . " WHERE qc_note_id = " . (int)$qc_note_id;
		$this->pdb->query($sql);
	}

	//PLAN QC
	public function getPlanCodes($plan_id) {
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "plan_code qp WHERE qp .plan_id = '" . (int)$plan_id . "'");
		return $query->rows;
	}

	public function deletePlanCode($plan_id) {
		$sql = "DELETE FROM " . DB_PREFIX . "plan_code WHERE plan_id = " . (int)$plan_id;
		$this->pdb->query($sql); 
	}

	public function updatePlanCode($data) {
		$sql = "INSERT INTO " . PDB_PREFIX . "plan_code SET ";
		foreach ($data as $key => $value) {
			$sql .= "{$key} = '" . $this->pdb->escape($value) . "', ";
		}
		$sql .= "date_added = NOW()";
		$this->pdb->query($sql);
	}

	//PLAN CONFIRM
	public function getPlanConfirms($plan_id) {
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "plan_confirm qc WHERE qc.plan_id = '" . (int)$plan_id . "'");
		return $query->rows;
	}

	public function getPlanConfirm($plan_id, $user_id = '', $user_group_id = '') {
		$sql = "SELECT * FROM " . PDB_PREFIX . "plan_confirm qc WHERE qc.plan_id = " . (int)$plan_id;
		if (!empty($user_id)) {
			$sql .= " AND user_id = " . (int)$user_id;
 		}
 		if (!empty($user_id)) {
			$sql .= " AND user_group_id = " . (int)$user_group_id;
 		}
 		
		return $this->pdb->query($sql)->row;
	}

	public function countConfirm($plan_id) {
		$query = $this->pdb->query("SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "plan_confirm
		WHERE plan_id = '" . (int)$plan_id . "' AND user_id= '" .(int)$this->user->getId(). "'");
	}

	public function updateConfirm($data) {
		$user_dc = $this->config->get('config_user_dc');
		$user_pa = $this->config->get('config_user_pa');
		$sql = "INSERT INTO " . DB_PREFIX . "plan_confirm SET ";
		foreach ($data as $key => $value) {
			$sql .= "{$key} = '" . $this->pdb->escape($value) ."',";
		}
		$sql .= "date_added = NOW()";
		$this->pdb->query($sql);
		//QC = 1 khi nhóm user == DC||PA
		$user_group_id = $this->user->getGroupId();
		if(in_array($user_group_id,array($user_dc,$user_pa))){
			$sql = "UPDATE " . PDB_PREFIX . "plan SET plan_qc = '1' WHERE plan_id = '" . (int)$data['plan_id'] . "'";
			$this->pdb->query($sql);
			
			//Lưu vào DB chung để check GPS - Luân
			$new_info = $this->getPlan($data['plan_id']);//***Hàm này nhớ coi lại mỗi dự án có thể thay đổi***
			$this->load->model('project/project_user');
			$user_info = $this->model_project_project_user->getUser($new_info['user_id']);
			
			$new_info['project_id'] = $user_info['project_id'];
			$new_info['user_fullname'] = $user_info['fullname'];
			$new_info['sup_id'] = (isset($new_info['sup_id'])&&$new_info['sup_id']>0)?$new_info['sup_id']:$user_info['user_parent_id'];
			$new_info['sup_fullname'] = isset($user_info['parent_fullname'])?$user_info['parent_fullname']:'';
			$new_info['latitude'] = $new_info['latitude'];
			$new_info['longitude'] = $new_info['longitude'];
			$this->user->checkVisit($new_info);
			//End - Lưu vào DB chung
			
		}
	}

	//PLAN IMAGE
	public function addPlanImage($data) {
		$sql = "INSERT INTO " . PDB_PREFIX . "plan_images SET ";
		foreach ($data as $key => $value) {
			$sql .= "{$key} = '" . $this->pdb->escape($value) . "',";
		}
		$sql .= "date_added = NOW()";
		
		$this->pdb->query($sql);
		return $this->pdb->getLastId();
	}

	public static $static_image = array();
	public function getPlanImages($plan_id, $order_id = 0) {
		// if (isset(self::$static_image[$plan_id])) {
		// 	return self::$static_image[$plan_id];
		// }
		$plan_images = array();
		$sql = "SELECT * FROM `" . PDB_PREFIX . "plan_images` si WHERE si.is_deleted = '0' AND si.plan_id = " . (int)$plan_id;
		if ($order_id) {
			$sql .= " AND order_id = " . (int)$order_id;
		}
		$sql .= " ORDER BY si.image_id ASC";
		
		// $query = $this->pdb->query($sql);
		// if($query->rows){
		// 	$plan_images = $query->rows;
		// }
	 // 	return self::$static_image[$plan_id] = $plan_images;
		return $this->pdb->query($sql)->rows;
	}

	public function getImage($image_id) {
		$sql = "SELECT * FROM " . PDB_PREFIX . "plan_images WHERE image_id = '" . (int)$image_id . "'";
		$query = $this->pdb->query($sql);
		return $query->row;
	}

	public function deleteImage($image_id,$manual = 1) {
		$config_project_folder = $this->config->get('config_project_folder');
		$image_info = $this->getImage($image_id);
		
		$plan_id = $image_info['plan_id'];
		$plan_info = $this->getPlan($plan_id);
		
		$image_info['old_path'] = $old_path = DIR_MEDIA.$plan_info['image_path'].'image/'.$image_info['filename'];
		$new_path = DIR_MEDIA.'files/'.$config_project_folder.'/deleted/'.$image_info['store_code'].'/image/';
		$new_name = $image_info['filename'];
		
		 
		$image_info['new_path'] = $new_path.$new_name;
		
		if (file_exists($old_path)) {
		 	@mkdir($new_path,  0777, true);
			copy($image_info['old_path'], $image_info['new_path']);
		}
		if (file_exists($old_path)&&file_exists($new_path)) {
				unlink($old_path);
		}
		if($manual==1){
			$sql = "UPDATE `" . PDB_PREFIX . "plan_images` SET is_deleted = '1', user_deleted = '" .$this->db->escape($this->user->getId()). "' WHERE image_id = '" . (int)$image_id . "'";
			$this->pdb->query($sql);
		}
	}

	public function updateImage($image_id,$data) {
		$sql = "UPDATE `" . PDB_PREFIX . "plan_images` SET ";
		foreach ($data as $key => $value) {
			$sql .= "{$key} = '" . $this->pdb->escape($value) . "',";
		}
		$sql .= "date_modified = NOW() WHERE image_id = '" . (int)$image_id . "'";
		$this->pdb->query($sql);
	}

	public function updatePlanImage($image_id,$data) {
		$sql = "UPDATE `" . PDB_PREFIX . "plan_images` SET filename = '" . $this->db->escape($data['filename']) . "' WHERE image_id = '" . (int)$image_id . "'";
		$this->pdb->query($sql);
		return $image_id;
	}

	public function getAudios($plan_id) {
		$sql = "SELECT * FROM " . DB_PREFIX . "audio WHERE is_deleted = 0 AND plan_id = " . $plan_id;
		return $this->pdb->query($sql)->rows;
	}

	public function addAudio($data) {
		$sql = "INSERT INTO " . DB_PREFIX . "audio SET ";
		foreach ($data as $key => $value) {
			$sql .= "{$key} = '" . $this->pdb->escape($value) . "',";
		}
		$sql .= "date_added = NOW()";
		$this->pdb->query($sql);
		return $this->pdb->getLastId();
	}

	public function getAudio($id) {
		$sql = "SELECT * FROM " . DB_PREFIX . "audio WHERE is_deleted = 0 AND id = " . $id;
		return $this->pdb->query($sql)->row;
	}
	
	public function deleteAudio($audio_id) {
		$config_project_folder = $this->config->get('config_project_folder');
		$audio = $this->getAudio($audio_id);
		$plan = $this->getPlan($audio['plan_id']);
		$old_path = DIR_MEDIA.$audio['filename'];
		$new_path = DIR_MEDIA.'files/'.$config_project_folder.'/deleted/'.$plan['store_code'].'/audio/';
		if (file_exists($old_path)) {
		 	@mkdir($new_path,  0777, true);
		 	$new_file_name = explode('/', $old_path);
		 	$new_file_name = end($new_file_name);
			copy($old_path, $new_path.$new_file_name);
		}
		if (file_exists($old_path)&&file_exists($new_path)) {
			unlink($old_path);
		}
		$sql = "UPDATE " . DB_PREFIX . "audio SET is_deleted = 1, user_deleted = " . $this->user->getId() . " WHERE id = " . (int)$audio_id;
		$this->pdb->query($sql);
	}

	public function getPlanUser($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "plan_user u
			JOIN cpm_plan pl ON pl.plan_id = u.plan_id
			WHERE pl.is_deleted = 0";
		if ($data) {
			if (!empty($data['plan_ids'])) {
				$sql .= " AND pl.plan_id IN (" . implode(",", $data['plan_ids']) . ")";
				unset($data['plan_ids']);
			}
			foreach ($data as $key => $value) {
				$sql .= " AND $key = '" . $this->pdb->escape($value) . "'";
			}
		}
		$sql .= " ORDER BY user_group,pl.time_checkin";
		return $this->pdb->query($sql)->rows;
	}

	public function addPlanUser($data) {
		$sql = "INSERT INTO " . DB_PREFIX . "plan_user SET";
		if ($data) {
			foreach ($data as $key => $value) {
				$sql .= " $key = '" . $this->pdb->escape($value) . "',";
			}
		}
		$sql .= ' date_added = NOW()';
		$this->pdb->query($sql);
		return $this->pdb->getLastId();
	}

	public function updatePlanUser($id = 0, $data = array()) {
		$sql = "UPDATE " . DB_PREFIX . "plan_user SET ";
		if ($data) {
			foreach ($data as $key => $value) {
				$sql .= " $key = '" . $this->pdb->escape($value) . "',";
			}
		}
		$sql .= ' date_modified = NOW() WHERE id = ' . (int)$id;
		$this->pdb->query($sql);
	}

	public function deletePlanUser($data) {
		$sql = "DELETE FROM " . DB_PREFIX . "plan_user WHERE 1";
		if ($data) {
			foreach ($data as $key => $value) {
				$sql .= " AND $key = '" . $this->pdb->escape($value) . "'";
			}
		}

		return $this->pdb->query($sql);
	}

	public function getPlanCheckin($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "plan_checkin WHERE 1";
		if ($data) {
			if (!empty($data['plan_ids'])) {
				$sql .= " AND plan_id IN ('" . implode("','", $data['plan_ids']) . "')";
				unset($data['plan_ids']);
			}
			foreach ($data as $key => $value) {
				$sql .= " AND $key = '" . $this->pdb->escape($value) . "'";
			}
		}
		$sql .= " ORDER BY time_checkin";
		return $this->pdb->query($sql)->rows;
	}

	public function addPlanCheckin($data) {
		$sql = "INSERT INTO " . DB_PREFIX . "plan_checkin SET";
		if ($data) {
			foreach ($data as $key => $value) {
				$sql .= " $key = '" . $this->pdb->escape($value) . "',";
			}
		}
		$sql .= ' date_added = NOW()';
		$this->pdb->query($sql);
		return $this->pdb->getLastId();
	}

	public function getPlanOrder($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "plan_order WHERE is_deleted = 0";
		if ($data) {
			if (!empty($data['plan_ids'])) {
				$sql .= " AND plan_id IN ('" . implode("','", $data['plan_ids']) . "')";
				unset($data['plan_ids']);
			}
			if (!empty($data['user_ids'])) {
				$sql .= " AND user_id IN ('" . implode("','", $data['user_ids']) . "')";
				unset($data['user_ids']);
			}
			if (!empty($data['user_id'])) {
				$sql .= " AND user_id =" . (int)$data['user_id'];
			}
			if (!empty($data)) {
				foreach ($data as $key => $value) {
					if (!in_array($key, $this->no_filter_key)) {
						$key = str_replace('filter_', '', $key);
						$sql .= " AND {$key}= '" . $this->pdb->escape($value) . "'";
					}	
				}

				if (!empty($data['filter_global'])) {
					$sql .= " AND order_code like '%" . $this->pdb->escape(utf8_strtolower($data['filter_global'])) . "%'";
				}
			}
		}
		if (isset($data['sort'])) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY order_id";
		}
		
		return $this->pdb->query($sql)->rows;
	}

	public function getPlanOrderDetail($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "plan_order o
				JOIN " . DB_PREFIX . "order_detail od ON o.order_id = od.order_id 
				WHERE o.is_deleted = 0 AND od.is_deleted = 0 ";
		if ($data) {
			if (!empty($data['plan_ids'])) {
				$sql .= " AND o.plan_id IN (" . implode(",", $data['plan_ids']) . ")";
				unset($data['plan_ids']);
			}
			foreach ($data as $key => $value) {
				if (!in_array($key, $this->no_filter_key)) {
					$key = str_replace('filter_', '', $key);
					$sql .= " AND {$key}= '" . $this->pdb->escape($value) . "'";
				}	
			}
		}
		$sql .= " ORDER BY o.plan_id,o.order_id";
		// p($sql,1);
		return $this->pdb->query($sql)->rows;
	}

	public function countOrders($data = array(), $group_by = 'pl.plan_id') {
		$sql = "SELECT {$group_by},count(*) as order_number, sum(price_total) as price_total 
				FROM " . DB_PREFIX . "plan_order o
				JOIN cpm_plan pl ON pl.plan_id = o.plan_id
				WHERE pl.is_deleted = 0 AND o.is_deleted = 0";
		if (!empty($data['plan_ids'])) {
			$sql .= " AND pl.plan_id IN (" . implode(",", $data['plan_ids']) . ")";
			unset($data['plan_ids']);
		}
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				if (!in_array($key, $this->no_filter_key)) {
					$key = str_replace('filter_', '', $key);
					$sql .= " AND {$key}= '" . $this->pdb->escape($value) . "'";
				}	
			}
		}
		$sql .= " GROUP BY {$group_by}";
		// p($sql,1);
		return $this->pdb->query($sql)->rows;
	}

	public function addOrder($data = array()) {
		$sql = "INSERT INTO " . DB_PREFIX . "plan_order SET ";
		if ($data) {
			foreach ($data as $key => $value) {
				$sql .= " $key = '" . $this->pdb->escape($value) . "',";
			}
		}
		$sql .= ' date_added = NOW()';
		$this->pdb->query($sql);
		return $this->pdb->getLastId();
	}

	public function updateOrder($order_id = 0, $data = array()) {
		$sql = "UPDATE " . DB_PREFIX . "plan_order SET ";
		if ($data) {
			foreach ($data as $key => $value) {
				$sql .= " $key = '" . $this->pdb->escape($value) . "',";
			}
		}
		$sql .= ' date_modified = NOW() WHERE order_id = ' . (int)$order_id;
		$this->pdb->query($sql);
	}

	public function deleteOrder($order_id = 0) {
		if ($order_id) {
			$sql = "UPDATE " . DB_PREFIX . "plan_order SET date_deleted = NOW(), is_deleted = 1, user_deleted = '" . $this->user->getUserName().'-'.$this->user->getFullName() . "' WHERE order_id = " . (int)$order_id;
			// p($sql,1);
			$this->pdb->query($sql);
		}
	}
	public function planGiftOutput ($plan_id) {
		$sql = "SELECT a.attribute_id,sum(value) as value 
				FROM `cpm_attribute_data` ad
				JOIN cpm_attribute a ON a.attribute_id = ad.attribute_id
				JOIN cpm_plan_order o ON o.order_id = ad.order_id
				WHERE a.group_code IN ('GIFT','SAMPLES') AND ad.plan_id = {$plan_id} AND o.is_deleted = 0
				GROUP BY a.attribute_id";
		// p($sql,1);
		$data = $this->pdb->query($sql)->rows;
		$plan = array(
			'gift_11' => 0,
			'gift_12' => 0,
			'gift_13' => 0,
			'gift_14' => 0,
			'gift_36' => 0,
		);

		foreach ($data as $value) {
			$attr_id = $value['attribute_id'];
			if ($attr_id == 11) {
				$plan['gift_11'] += $value['value'];
			} elseif ($attr_id == 12) {
				$plan['gift_12'] += $value['value'];
			} elseif ($attr_id == 13 || $attr_id == 8) {
				$plan['gift_13'] += $value['value'];
			} elseif ($attr_id == 14 || $attr_id == 7) {
				$plan['gift_14'] += $value['value'];
			} elseif ($attr_id == 36 || $attr_id == 40) {
				$plan['gift_36'] += $value['value'];
			}
		}
		
		$this->updatePlan($plan_id,$plan);
	}

	public function addOrderDetail($data = array()) {
		$sql = "INSERT INTO " . DB_PREFIX . "order_detail SET ";
		if ($data) {
			foreach ($data as $key => $value) {
				$sql .= " $key = '" . $this->pdb->escape($value) . "',";
			}
		}
		$sql .= ' date_added = NOW()';
		$this->pdb->query($sql);

		return $this->pdb->getLastId();
	}

	public function updateOrderDetail($detail_id = 0, $data = array()) {
		$sql = "UPDATE " . DB_PREFIX . "order_detail SET ";
		if ($data) {
			foreach ($data as $key => $value) {
				$sql .= " $key = '" . $this->pdb->escape($value) . "',";
			}
		}
		$sql .= ' date_modified = NOW() WHERE detail_id = ' . (int)$detail_id;
		$this->pdb->query($sql);
	}

	public function deleteOrderDetail($detail_id = 0) {
		if ($detail_id) {
			$sql = "UPDATE " . DB_PREFIX . "order_detail SET date_deleted = NOW(), is_deleted = 1, user_deleted = '" . $this->user->getUserName().'-'.$this->user->getFullName() . "' WHERE detail_id = " . (int)$detail_id;
			// p($sql,1);
			$this->pdb->query($sql);
		}
	}

	public function countSampleByPlan($data = array()) {
		$sql = "SELECT o.plan_id, current_brand, count(*) as total FROM " . DB_PREFIX . "plan_order o
				JOIN " . DB_PREFIX . "order_detail od ON o.order_id = od.order_id 
				WHERE o.is_deleted = 0 AND od.is_deleted = 0 AND current_brand > 0";
		if (!empty($data['plan_ids'])) {
			$sql .= " AND o.plan_id IN (" . implode(',', $data['plan_ids']) . ")";
			unset($data['plan_ids']);
		}
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				$sql .= " AND $key = '" . $this->pdb->escape($value) . "'";
			}
		}
		$sql .= " GROUP BY o.plan_id,current_brand";
		$results = $this->pdb->query($sql)->rows;
		$plan_samples = array();
		foreach ($results as $v) {
			$plan_samples[$v['plan_id']][$v['current_brand']] = $v['total'];
		}
		return $plan_samples;
	}

	public function countImage($data = array()) {
		$sql = "SELECT count(*) as total FROM `" . PDB_PREFIX . "plan_images` si WHERE si.is_deleted = '0'";
		foreach ($data as $key => $value) {
			$sql .= " AND {$key}= '" . $this->pdb->escape($value) . "'";
		}
		return $this->pdb->query($sql)->row['total'];
	}
}
