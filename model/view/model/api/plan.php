<?php
class ModelApiPlan extends Model {
	
	public function updatePlan($plan_id, $data = array()){
		$is_fix = isset($data['is_fix'])?$data['is_fix']:0;
		
		$sql = "UPDATE " . PDB_PREFIX . "plan SET `date_modified` = NOW() ";
		
		if($is_fix==0) {
			$sql .= ", `time_upload` = NOW()";
		}
		if($is_fix==1) {
			$sql .= ", `date_fixed` = NOW()";
			$sql .= ", `is_fix` = '2'";
		}
		if($is_fix==0&&isset($data['capture_total'])) {
			$sql .= ", `capture_total` = '" . $this->db->escape($data['capture_total']) . "'";
		}
		if(isset($data['note'])) {
			$sql .= ", `note` = '" . $this->db->escape($data['note']) . "'";
		}
		if(isset($data['note_ktc'])) {
			$sql .= ", `reason_id` = '" . (int)$data['note_ktc'] . "'";
			if((int)$data['note_ktc']>0) {
				$sql .= ", `plan_rating` = '-2'";
				$this->updatePlanNotPass($plan_id,$data);
			}else{
				$sql .= ", `plan_rating` = '1'";
			}
		}
		if(isset($data['note_ktc_other'])) {
			$sql .= ", `note_ktc` = '" . $this->db->escape($data['note_ktc_other']) . "'";
		}
		
		if(isset($data['call_recipient'])){
			$sql .= ",call_recipient = '" . $this->pdb->escape($data['call_recipient']) . "'";
		}
		
		if(isset($data['call_phone_number'])){
			$sql .= ",call_phone_number = '" . $this->pdb->escape($data['call_phone_number']) . "'";
		}
		
		if(isset($data['call_date'])){
			$sql .= ",call_date = '" . $this->pdb->escape($data['call_date']) . "'";
		}
		
		if(isset($data['call_num'])){
			$sql .= ",call_num = '" . $this->pdb->escape($data['call_num']) . "'";
		}
		
		if(isset($data['call_note'])){
			$sql .= ",call_note = '" . $this->pdb->escape($data['call_note']) . "'";
		}
		
		$sql .= " WHERE `plan_id` = '" . (int)$plan_id . "'";
		$this->pdb->query($sql);
	}
	private function updatePlanNotPass($plan_id,$data){
		
			$sql = "UPDATE `" . PDB_PREFIX . "plan_survey` SET rating_manual = '-2'";
			
			$sql .= ", `ktc_reason_id` = '" . (int)$data['note_ktc'] . "'";
			
			$sql .= ", `note_ktc` = '" . $this->db->escape($data['note_ktc_other']) . "'";
			
			$sql .= " WHERE plan_id = '" . (int)$plan_id . "'";
			$this->pdb->query($sql);
		
	}
}
?>