<?php
class ModelToolImage extends Model {
	
	public function resize($filename, $width, $height) {
		if (!is_file(DIR_MEDIA . $filename) || substr(realpath(DIR_MEDIA . $filename), 0, strlen(realpath(DIR_MEDIA))) != realpath(DIR_MEDIA)) {
			return;
		}
		$extension = pathinfo($filename, PATHINFO_EXTENSION);

		$image_old = $filename;
		
		if (filesize(DIR_MEDIA . $image_old)>0){
			$image_new = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . (int)$width . 'x' . (int)$height . '.' . $extension;
	
			if (filesize(DIR_MEDIA . $image_old)>0&&(!is_file(DIR_MEDIA . $image_new) || (filectime(DIR_MEDIA . $image_old) > filectime(DIR_MEDIA . $image_new)))) {
				
				list($width_orig, $height_orig, $image_type) = getimagesize(DIR_MEDIA . $image_old);
				
					 
				if (!in_array($image_type, array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF))) { 
					return DIR_MEDIA . $image_old;
				}
				
							
				$path = '';
	
				$directories = explode('/', dirname($image_new));
	
				foreach ($directories as $directory) {
					$path = $path . '/' . $directory;
	
					if (!is_dir(DIR_MEDIA . $path)) {
						@mkdir(DIR_MEDIA . $path, 0777);
					}
				}
	
				if ($width_orig != $width || $height_orig != $height) {
					$image = new Image(DIR_MEDIA . $image_old);
					$image->resize($width, $height);
					$image->save(DIR_MEDIA . $image_new);
				} else {
					copy(DIR_MEDIA . $image_old, DIR_MEDIA . $image_new);
				}
			}
			
			$image_new = str_replace(' ', '%20', $image_new);  // fix bug when attach image on email (gmail.com). it is automatic changing space " " to +
			
			return HTTP_SERVER. 'media/' . $image_new;
		}else{
			//Lỗi hình ảnh
			return HTTP_SERVER. 'media/error.png';
		}
	}
	public function best_fit($filename, $width, $height) {
		if (!is_file(DIR_MEDIA . $filename) || substr(realpath(DIR_MEDIA . $filename), 0, strlen(realpath(DIR_MEDIA))) != realpath(DIR_MEDIA)) {
			return;
		}
		$extension = pathinfo($filename, PATHINFO_EXTENSION);

		$image_old = $filename;
		$image_new = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . (int)$width . 'x' . (int)$height . '.' . $extension;

		if (!is_file(DIR_MEDIA . $image_new) || (filectime(DIR_MEDIA . $image_old) > filectime(DIR_MEDIA . $image_new))) {
			list($width_orig, $height_orig, $image_type) = getimagesize(DIR_MEDIA . $image_old);
				 
			if (!in_array($image_type, array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF))) { 
				return DIR_MEDIA . $image_old;
			}
						
			$path = '';

			$directories = explode('/', dirname($image_new));

			foreach ($directories as $directory) {
				$path = $path . '/' . $directory;

				if (!is_dir(DIR_MEDIA . $path)) {
					@mkdir(DIR_MEDIA . $path, 0777);
				}
			}

			if ($width_orig != $width || $height_orig != $height) {
				$image = new Image(DIR_MEDIA . $image_old);
				$image->best_fit($width, $height);
				$image->save(DIR_MEDIA . $image_new);
			} else {
				copy(DIR_MEDIA . $image_old, DIR_MEDIA . $image_new);
			}
		}
		
		$image_new = str_replace(' ', '%20', $image_new);  // fix bug when attach image on email (gmail.com). it is automatic changing space " " to +
		
		//return $this->config->get('config_url') . 'media/' . $image_new;
		return HTTP_SERVER. 'media/' . $image_new;
	}
	
	public function rotate($filename,$new_path,$angle) {
		if (!is_file(DIR_MEDIA . $filename) || substr(realpath(DIR_MEDIA . $filename), 0, strlen(realpath(DIR_MEDIA))) != realpath(DIR_MEDIA)) {
			return;
		}

		$extension = pathinfo($filename, PATHINFO_EXTENSION);

		$image_old = $filename;
			list($width_orig, $height_orig, $image_type) = getimagesize(DIR_MEDIA . $image_old);
				 
			if (!in_array($image_type, array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF))) { 
				return DIR_MEDIA . $image_old;
			}
					

			$image = new Image(DIR_MEDIA . $image_old);
			$image->rotate($angle);
			$image->save(DIR_MEDIA . $new_path);
			
		
		//return $this->config->get('config_url') . 'media/' . $newname;
		return HTTP_SERVER. 'media/' . $new_path;
	}
}
