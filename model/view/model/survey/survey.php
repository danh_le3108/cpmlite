<?php
class ModelSurveySurvey extends Model {
	
	public function inputs(){ 
		return array(
			'number'=>'Number',
			'text'=>'Text',
			'textarea'=>'Textarea',
			'select'=>'Select',
			'checkbox'=>'Checkbox',
			'radio'=>'Radio'
		);
	}
	/*Group*/ 
	public function getGroupsBySurveyID($survey_id) {
		$return = array();
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "survey_group WHERE survey_id = '" . (int)$survey_id . "' ORDER BY sort_order ASC");
		if($query->num_rows){
			foreach($query->rows as $group){
				$group['questions'] = $this->getQuestionsByGroupID($group['group_id']);
					$return[] = $group;	
			}
		}
		return $return;
	}
	public function getGroup($group_id) {
		$return = array();
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "survey_group WHERE group_id = '" . (int)$group_id . "' ORDER BY sort_order ASC");
	
		if($query->num_rows){
				$query->row['questions'] = $this->getQuestionsByGroupID($group_id);
					$return = $query->row;	
		}
		return $return;
	}
	public function deleteGroup($group_id) {
		$group_info = $this->getGroup($group_id);
		if(!empty($group_info['questions'])){
			foreach ($group_info['questions'] as $question) {
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "survey_answer WHERE question_id = '" . (int)$question['question_id'] . "'");
			}
		}
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "survey_group WHERE group_id = '" . (int)$group_id . "'");
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "survey_question WHERE group_id = '" . (int)$group_id . "'");
	}
	private $group_fields = array(
		'survey_description',
		'group_code',
		'group_target'
		);
	public function addGroup($survey_id,$data) {
		$sql = "INSERT INTO " . PDB_PREFIX . "survey_group SET 
		group_title = '" . $this->db->escape($data['group_title']) . "', 
		survey_id = '" . (int)$survey_id . "'"; 
		
		foreach ($data as $field => $value) {
			if (in_array($field,$this->group_fields)) {
			$sql .= ", $field = '" . $this->db->escape($value) . "'";
			}
		}
		$sql .= ", sort_order = '" . (int)$data['sort_order'] . "'";
		
		$this->pdb->query($sql);
		return $this->pdb->getLastId();
	}

	public function editGroup($group_id, $data) {
		
		$sql = "UPDATE " . PDB_PREFIX . "survey_group SET  
		group_title = '" . $this->db->escape($data['group_title']) . "',
		survey_id = '" . (int)$data['survey_id'] . "'";
		
		foreach ($data as $field => $value) {
			if (in_array($field,$this->group_fields)) {
			$sql .= ", $field = '" . $this->db->escape($value) . "'";
			}
		}
		
		$sql .= ",sort_order = '" . (int)$data['sort_order'] . "' WHERE group_id = '" . (int)$group_id . "'";
		
		$this->pdb->query($sql);
	}
	/*Question*/ 
	private $questions_fields = array(
		'question_description',
		'attribute_id',
		'attribute_code',
		'input_type',
		'target',
		'ratio',
		'sum_value',
		'sum_group'
	);
	//Gọi câu hỏi theo SurveyID
	public function getQuestionsBySurveyID($survey_id,$is_indexes=0) {
		$return = array();
		$sql = "SELECT * FROM " . PDB_PREFIX . "survey_question WHERE survey_id = '" . (int)$survey_id . "' ORDER BY sort_order ASC";
		$query = $this->pdb->query($sql);
		if($query->num_rows){
			foreach($query->rows as $question){
				$question['answers'] = $this->getAnswersByQuestionID($question['question_id'],$is_indexes);
				$return[$question['question_id']] = $question;	
			}
		}
		return $return;
	}
	//Gọi câu hỏi theo Nhóm
	public function getQuestionsByGroupID($group_id) {
		$return = array();
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "survey_question WHERE group_id = '" . (int)$group_id . "' ORDER BY sort_order ASC");
		if($query->num_rows){
			foreach($query->rows as $question){
				$question['answers'] = $this->getAnswersByQuestionID($question['question_id']);
				$return[] = $question;	
			}
		}
		return $return;
	}
	//Thêm câu hỏi 
	public function addQuestion($data) {
		$sql = "INSERT INTO " . PDB_PREFIX . "survey_question SET 
		question_title = '" . $this->db->escape($data['question_title']) . "', 
		group_id = '" . $this->db->escape($data['group_id']) . "', 
		survey_id = '" . (int)$data['survey_id'] . "'"; 
		
		foreach ($data as $field => $value) {
			if (in_array($field,$this->questions_fields)) {
			$sql .= ", $field = '" . $this->db->escape($value) . "'";
			}
		}
		$sql .= ",sort_order = '" . (int)$data['sort_order'] . "'";
		
		$this->pdb->query($sql);
		return $this->pdb->getLastId();
	} 
	//Sửa câu hỏi 
	public function editQuestion($question_id, $data) {
		$sql = "UPDATE " . PDB_PREFIX . "survey_question SET  
		question_title = '" . $this->db->escape($data['question_title']) . "', 
		survey_id = '" . (int)$data['survey_id'] . "',
		group_id = '" . $this->db->escape($data['group_id']) . "'";
		
		foreach ($data as $field => $value) {
			if (in_array($field,$this->questions_fields)) {
			$sql .= ", $field = '" . $this->db->escape($value) . "'";
			}
		}
		 
		$sql .= ",sort_order = '" . (int)$data['sort_order'] . "' WHERE question_id = '" . (int)$question_id . "'";
		$this->pdb->query($sql);
	}

	//Gọi câu hỏi 
	public function getQuestion($question_id) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "survey_question WHERE question_id = '" . (int)$question_id . "'");
		$query->row['answers'] = $this->getAnswersByQuestionID($question_id);
		return $query->row;
	}
	//Xóa câu hỏi & xóa Câu trả lời thuộc câu hỏi này
	public function deleteQuestion($question_id) {
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "survey_question WHERE question_id = '" . (int)$question_id . "'");
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "survey_answer WHERE question_id = '" . (int)$question_id . "'");
	}
	/*Answers*/ 
	public function getAnswersByQuestionID($question_id,$is_indexes=0) {
		$return = array();
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "survey_answer WHERE question_id = '" . (int)$question_id . "' ORDER BY answer_value ASC");
		if($query->num_rows){
			if($is_indexes==1){
				foreach($query->rows as $row){
					$return[$row['answer_id']] = $row;
				}
			}else{
				$return = $query->rows;
			}
		}
		return $return;
	}
	//Gọi câu trả lời
	public function getAnswer($answer_id) {
		$return = array();
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "survey_answer WHERE answer_id = '" . (int)$answer_id . "' ORDER BY answer_value ASC");
		if($query->num_rows){
			$return = $query->row;	
		}
		return $return;
	}
	//Thêm câu trả lời
	public function addAnswer($data) {
		$this->pdb->query("INSERT INTO " . PDB_PREFIX . "survey_answer SET 
		survey_id = '" . (int)$data['survey_id'] . "',
		answer_title = '" . $this->db->escape($data['answer_title']) . "', 
		answer_value = '" . $this->db->escape($data['answer_value']) . "', 
		question_id = '" . (int)$data['question_id'] . "'");
		return $this->pdb->getLastId();
	}

	//Sửa câu trả lời
	public function editAnswer($answer_id, $data) {
		$this->pdb->query("UPDATE " . PDB_PREFIX . "survey_answer SET  
		survey_id = '" . (int)$data['survey_id'] . "',
		answer_title = '" . $this->db->escape($data['answer_title']) . "',
		answer_value = '" . $this->db->escape($data['answer_value']) . "', 
		question_id = '" . (int)$data['question_id'] . "' WHERE answer_id = '" . (int)$answer_id . "'");
	}
	//Xóa câu trả lời
	public function deleteAnswer($answer_id) {
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "survey_answer WHERE answer_id = '" . (int)$answer_id . "'");
	}
	/*Survey*/ 
	public function addSurvey($data) {
		$this->pdb->query("INSERT INTO " . PDB_PREFIX . "survey SET 
		survey_name = '" . $this->db->escape($data['survey_name']) . "', 
		coupon_prefix = '" . $this->db->escape($data['coupon_prefix']) . "',
		pass_point = '" . (int)$data['pass_point'] . "',
		ratio = '" . $this->db->escape($data['ratio']) . "',
		is_rating = '" . (int)$data['is_rating'] . "',
		sort_order = '" . (int)$data['sort_order'] . "'");
		
		$survey_id =  $this->pdb->getLastId();
		
		if(isset($data['copy_id'])&&$data['copy_id']>0){
			$this->copySurvey($survey_id, $data['copy_id']);
		}
		return $survey_id;
	}

	public function editSurvey($survey_id, $data) {
		$this->pdb->query("UPDATE " . PDB_PREFIX . "survey SET  
		survey_name = '" . $this->db->escape($data['survey_name']) . "',
		coupon_prefix = '" . $this->db->escape($data['coupon_prefix']) . "',
		pass_point = '" . (int)$data['pass_point'] . "',
		ratio = '" . $this->db->escape($data['ratio']) . "', 
		is_rating = '" . (int)$data['is_rating'] . "',
		survey_description = '" . $this->db->escape($data['survey_description']) . "', 
		sort_order = '" . (int)$data['sort_order'] . "' WHERE survey_id = '" . (int)$survey_id . "'");
		if(isset($data['copy_id'])&&$data['copy_id']>0){
			$this->copySurvey($survey_id, $data['copy_id']);
		}
		$round_name = date('Y-m');
		$this->deleteSurveyHistory($survey_id,$round_name);
	}
	//Copy Survey theo SurveyID đích ($survey_id) <= SurveyID gốc ($copy_id)
	public function copySurvey($survey_id, $copy_id) {
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "survey_group WHERE survey_id = '" . (int)$survey_id . "'");
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "survey_question WHERE survey_id = '" . (int)$survey_id . "'");
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "survey_question WHERE survey_id = '" . (int)$survey_id . "'");
			
		$copy_groups = $this->getGroupsBySurveyID($copy_id);
		foreach($copy_groups as $group){
			$new_group_id = $this->addGroup($survey_id,$group);
			if(!empty($group['questions'])){
				foreach($group['questions'] as $question){
					$question['group_id'] = $new_group_id;
					$question['survey_id'] = $survey_id;
					 $new_question_id = $this->addQuestion($question);
					if(isset($question['answers'])&&!empty($question['answers'])){
						foreach($question['answers'] as $answer){
							$answer['question_id'] = $new_question_id;
							$answer['survey_id'] = $survey_id;
							$this->addAnswer($answer);
						}
					}
				}
			}
		}
	}

	public function deleteSurvey($survey_id) {
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "survey WHERE survey_id = '" . (int)$survey_id . "'");
	}

	public function getSurvey($survey_id) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "survey WHERE survey_id = '" . (int)$survey_id . "'");
		$query->row['groups'] = $this->getGroupsBySurveyID($survey_id);
		return $query->row;
	}
	
	public function getSurveysHistory($round_name) {
		$sql = "SELECT * FROM " . PDB_PREFIX . "survey p ORDER BY p.sort_order ASC";
	
		$query = $this->pdb->query($sql);
		$query_data = array();
		foreach ($query->rows as $row){
			$query_data[] = $this->getSurveyHistory($row['survey_id'],$round_name);
		}
		return $query_data;
	}
	public function getSurveyHistory($survey_id,$round_name) {
		$is_indexes = 1;
		$return = false;
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "survey_history WHERE survey_id = '" . (int)$survey_id . "' AND round_name = '" . $this->db->escape($round_name) . "'");
		if(isset($query->row['survey_id'])){
			$return = $query->row;
			$return['groups'] = json_decode($query->row['groups'],true);
			$return['questions'] = json_decode($query->row['questions'],true);
		}else{
			$return = $this->getSurvey($survey_id);
			$return['questions'] = $this->getQuestionsBySurveyID($survey_id,$is_indexes);
			$this->addSurveyHistory($survey_id,$round_name,$return);
			
		}
		return $return;
	}
	public function deleteSurveyHistory($survey_id,$round_name) {
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "survey_history WHERE survey_id = '" . (int)$survey_id . "' AND round_name = '" . $this->db->escape($round_name) . "'");
	}
	public function addSurveyHistory($survey_id,$round_name,$data) {
		$this->pdb->query("INSERT INTO " . PDB_PREFIX . "survey_history SET 
		survey_id = '" . (int)$survey_id . "', 
		round_name = '" . $this->db->escape($round_name) . "',
		survey_name = '" . $this->db->escape($data['survey_name']) . "',
		coupon_prefix = '" . $this->db->escape($data['coupon_prefix']) . "',
		pass_point = '" . (int)$data['pass_point'] . "',
		ratio = '" . $this->db->escape($data['ratio']) . "', 
		is_rating = '" . (int)$data['is_rating'] . "',
		survey_description = '" . $this->db->escape($data['survey_description']) . "', 
		sort_order = '" . (int)$data['sort_order'] . "',
		groups = '" . $this->db->escape(json_encode($data['groups'])) . "', 
		questions = '" . $this->db->escape(json_encode($data['questions'])) . "', date_added = NOW()");
	}
	
 
	public function getSurveys($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . PDB_PREFIX . "survey p WHERE 1";
			
			/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "p.survey_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 

			$sort_data = array(
				'p.sort_order'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY p.sort_order";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}
		} else {
			$sql = "SELECT * FROM " . PDB_PREFIX . "survey p ORDER BY p.sort_order ASC";
		}
		$query = $this->pdb->query($sql);
		$query_data = array();
		foreach ($query->rows as $row){
			$row['groups'] = $this->getGroupsBySurveyID($row['survey_id']);
			$query_data[] = $row;
		}
		return $query_data;
	}
	public function getTotalSurveys($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "survey p WHERE 1";
			
			/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "p.survey_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 
			
			$query = $this->pdb->query($sql);

		return $query->row['total'];
	}
	
	public function getSurveyIndexBy($index_by = 'survey_id',$show_groups = false) {
	  $query_data = array();
	  $query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "survey p");

	  foreach ($query->rows as $row){
		  if($show_groups==1){
			$row['groups'] = $this->getGroupsBySurveyID($row['survey_id']);
		  }
	   	$query_data[$row[$index_by]] = $row;
	  }

	  return $query_data;
 	}
}