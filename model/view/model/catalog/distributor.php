<?php
class ModelCatalogDistributor extends Model {
	public function distributor_fields(){
			return array(
                'A' => array('field' => 'stt','label' => 'STT'),
                'B' => array('field' => 'distributor_name','label' => 'Tên NPP'),
                'C' => array('field' => 'distributor_code','label' => 'Mã NPP'),
                'D' => array('field' => 'coupon_check','label' => 'Sử dụng Coupon'),
                'E' => array('field' => 'region_code','label' => 'Mã khu vực'),
                'F' => array('field' => 'distributor_address','label' => 'Mô tả')
			);
			
	}
	public function addDistributor($data) {
		$sql = "INSERT INTO " . PDB_PREFIX . "catalog_distributor SET `date_update` =NOW(),
		distributor_name = '" . $this->db->escape($data['distributor_name']) . "', 
		distributor_code = '" . $this->db->escape($data['distributor_code']) . "'";
		
		if (isset($data['distributor_address'])) {
			$sql .= ",distributor_address = '" . $this->db->escape($data['distributor_address']) . "'";
		}
		if (isset($data['email'])) {
			$sql .= ",email = '" . $this->db->escape($data['email']) . "'";
		}
		if (isset($data['telephone'])) {
			$sql .= ",telephone = '" . $this->db->escape($data['telephone']) . "'";
		}
		if (isset($data['region_code'])) {
			$sql .= ",region_code = '" . $this->db->escape($data['region_code']) . "'";
		}
		if (isset($data['region_id'])) {
			$sql .= ",region_id = '" . (int)$data['region_id'] . "'";
		}
		if (isset($data['area_code'])) {
			$sql .= ",area_code = '" . $this->db->escape($data['area_code']) . "'";
		}
		if (isset($data['area_id'])) {
			$sql .= ",area_id = '" . (int)$data['area_id'] . "'";
		}
		if (isset($data['province_code'])) {
			$sql .= ",province_code = '" . $this->db->escape($data['province_code']) . "'";
		}
		if (isset($data['coupon_check'])) {
			$sql .= ",coupon_check = '" . (int)$data['coupon_check'] . "'";
		}
		if (isset($data['status'])) {
			$sql .= ",status = '" . (int)$data['status'] . "'";
		}
		
		$this->pdb->query($sql);
		$distributor_id = $this->db->getLastId();
		
		
		if (isset($data['password'])&&!empty($data['password'])) {
			$this->pdb->query("UPDATE " . PDB_PREFIX . "catalog_distributor SET 
			salt = '" . $this->db->escape($salt = token(9)) . "', 
			password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', 
			real_token = '" . $this->db->escape(base64_encode(base64_encode($data['password']))) . "' 
			WHERE distributor_id = '" . (int)$distributor_id . "'");
		}
		
		return $distributor_id;
	}

	public function editDistributor($distributor_id, $data) {
		
		$sql = "UPDATE " . PDB_PREFIX . "catalog_distributor SET `date_update` =NOW(),
		distributor_name = '" . $this->db->escape($data['distributor_name']) . "', 
		distributor_code = '" . $this->db->escape($data['distributor_code']) . "'"; 
		
		if (isset($data['distributor_address'])) {
			$sql .= ",distributor_address = '" . $this->db->escape($data['distributor_address']) . "'";
		}
		if (isset($data['email'])) {
			$sql .= ",email = '" . $this->db->escape($data['email']) . "'";
		}
		if (isset($data['telephone'])) {
			$sql .= ",telephone = '" . $this->db->escape($data['telephone']) . "'";
		}
		if (isset($data['area_code'])) {
			$sql .= ",area_code = '" . $this->db->escape($data['area_code']) . "'";
		}
		if (isset($data['region_code'])) {
			$sql .= ",region_code = '" . $this->db->escape($data['region_code']) . "'";
		}
		if (isset($data['province_code'])) {
			$sql .= ",province_code = '" . $this->db->escape($data['province_code']) . "'";
		}
		if (isset($data['coupon_check'])) {
			$sql .= ",coupon_check = '" . (int)$data['coupon_check'] . "'";
		}
		if (isset($data['status'])) {
			$sql .= ",status = '" . (int)$data['status'] . "'";
		}
		$sql .= " WHERE distributor_id = '" . (int)$distributor_id . "'";
		
		$this->pdb->query($sql);
		
		if (isset($data['password'])&&!empty($data['password'])) {
			$this->pdb->query("UPDATE " . PDB_PREFIX . "catalog_distributor SET 
			salt = '" . $this->db->escape($salt = token(9)) . "', 
			password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', 
			real_token = '" . $this->db->escape(base64_encode(base64_encode($data['password']))) . "' WHERE distributor_id = '" . (int)$distributor_id . "'");
		}
		
	}

	public function updateDistributor($data) {
		$sql = "UPDATE " . PDB_PREFIX . "catalog_distributor SET  
		distributor_name = '" . $this->db->escape($data['distributor_name']) . "'"; 
		
		if (isset($data['coupon_check'])) {
			$sql .= ",coupon_check = '" . (int)$data['coupon_check'] . "'";
		}
		if (isset($data['distributor_address'])) {
			$sql .= ",distributor_address = '" . $this->db->escape($data['distributor_address']) . "'";
		}
		if (isset($data['region_code'])) {
			$sql .= ",region_code = '" . $this->db->escape($data['region_code']) . "'";
		}
		if (isset($data['region_id'])) {
			$sql .= ",region_id = '" . (int)$data['region_id'] . "'";
		}
		if (isset($data['area_code'])) {
			$sql .= ",area_code = '" . $this->db->escape($data['area_code']) . "'";
		}
		if (isset($data['area_id'])) {
			$sql .= ",area_id = '" . (int)$data['area_id'] . "'";
		}
		if (isset($data['province_code'])) {
			$sql .= ",province_code = '" . $this->db->escape($data['province_code']) . "'";
		}
		$sql .= " WHERE distributor_code = '" . $this->db->escape($data['distributor_code']) . "'";
		
		$this->pdb->query($sql);
	}
	public function deleteDistributor($distributor_id) {
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "catalog_distributor WHERE distributor_id = '" . (int)$distributor_id . "'");
	}

	public function getDistributor($distributor_id) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "catalog_distributor WHERE distributor_id = '" . (int)$distributor_id . "'");

		return $query->row;
	}

	public function getDistributors($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . PDB_PREFIX . "catalog_distributor d WHERE 1";
			
		$filter_global = array(
			'd.distributor_name',
			'd.distributor_code',
			'd.distributor_address'
		);

		if (!empty($data['filter_global'])) {
			$sql .= " AND (";
			foreach ($filter_global as $filter) {
				$fimplode[] = " LCASE(".$filter.") LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_global'])) . "%'";
			}
			$sql .= " " . implode(" OR ", $fimplode) . "";
			$sql .= ")";
		}
		
		/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_region_code'])) {
			$implode[] = "d.region_code = '" . $this->db->escape($data['filter_region_code']) . "'";
		}
		if (isset($data['filter_coupon_check'])) {
			$implode[] = "d.coupon_check = '" . (int)$data['filter_coupon_check'] . "'";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 

			$sort_data = array(
				'd.distributor_code'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY d.distributor_code";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}
			$query = $this->pdb->query($sql);

			return $query->rows;
		} else {
			$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "catalog_distributor d ORDER BY d.distributor_code ASC");

			$query_data = $query->rows;

			return $query_data;
		}
	}

	public function getTotalDistributors($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "catalog_distributor d WHERE 1";
		
		$filter_global = array(
			'd.distributor_name',
			'd.distributor_code',
			'd.distributor_address'
		);

		if (!empty($data['filter_global'])) {
			$sql .= " AND (";
			foreach ($filter_global as $filter) {
				$fimplode[] = " LCASE(".$filter.") LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_global'])) . "%'";
			}
			$sql .= " " . implode(" OR ", $fimplode) . "";
			$sql .= ")";
		}
		
		/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_region_code'])) {
			$implode[] = "d.region_code = '" . $this->db->escape($data['filter_region_code']) . "'";
		}
		if (isset($data['filter_coupon_check'])) {
			$implode[] = "d.coupon_check = '" . (int)$data['filter_coupon_check'] . "'";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
		/*Filter end*/ 

		$query = $this->pdb->query($sql);
		
		return $query->row['total'];
	}
	public function getDistributorsByAreaCode($area_code) {

			$sql = "SELECT * FROM " . PDB_PREFIX . "catalog_distributor";
			
			if(isset($area_code)){
				$sql .= " WHERE area_code = '" . $this->db->escape($area_code) . "'";
			}
			
			$sql .= " ORDER BY distributor_name";
			
			$query = $this->pdb->query($sql);

			$problem_data = $query->rows;

		return $problem_data;
	}
	public function getDistributorByCode($distributor_code){
  	$query = $this->pdb->query("SELECT * FROM ".PDB_PREFIX."catalog_distributor WHERE distributor_code = '".$this->db->escape($distributor_code)."'");
  	return $query->row;
  }
	/*Excel*/ 
	public function checkDistributor($data) {
		$sql = "SELECT COUNT(*) AS total FROM `" . PDB_PREFIX . "catalog_distributor` WHERE `distributor_code` = '" . $this->db->escape(trim($data['distributor_code'])) . "'";
		 
		$query = $this->pdb->query($sql);
			$return = $query->row['total'];
		if((int)$query->row['total']==0){
			$return = 0;
			$this->addDistributor($data);
		}else{
			$return = 2;
			$this->updateDistributor($data);
		}
		return $return;
	}
	public function getExportDistributors($data = array()) {
		$distributors_data = array();
		$distributors = $this->getDistributors($data);
			$i=0;
		foreach($distributors as $distributor){
			foreach($this->distributor_fields() as $col => $value){
					if(isset($distributor[$value['field']])){
						$distributors_data[$i][$col] = $distributor[$value['field']];
					}else{
						$distributors_data[$i][$col] = '';
					}
			}
			$i++;
		}
		return $distributors_data;
	}

	public function getDistributorIndexBy($index_by = 'distributor_code') {
	  $query_data = array();
	  $query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "catalog_distributor");

	  foreach ($query->rows as $row){
	   	$query_data[$row[$index_by]] = $row;
	  }

	  return $query_data;
 	}
}