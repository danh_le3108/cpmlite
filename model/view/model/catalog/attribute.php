<?php
class ModelCatalogAttribute extends Model {
	private $no_filter_key = array(
		'_url',
		'route',
		'page',
		'filter_global',
		'start',
		'limit',
		'sort',
		'order',
		'group_ids'
	);

	private $filter_global = array(
		'g.group_code',
		'g.group_name',
		'a.attribute_name',
		'a.attr_unit',
		'a.attr_type'
	);
	public function getAttributes($data = array()) {
		$sql = "SELECT g.*,a.*, (SELECT attribute_name FROM cpm_attribute b WHERE a.parent_attr_id = b.attribute_id) as parent_name FROM " . DB_PREFIX . "attribute a 
				LEFT JOIN cpm_attribute_group g ON a.group_code = g.group_code
				WHERE 1";
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				if (!in_array($key, $this->no_filter_key)) {
					$key = str_replace('filter_', '', $key);
					$sql .= " AND {$key}= '" . $this->pdb->escape($value) . "'";
				}	
			}

			if (!empty($data['filter_global'])) {
				$sql .= " AND (";
				foreach ($this->filter_global as $filter) {
					$implode[] = " LCASE(".$filter.") LIKE '%" . $this->pdb->escape(utf8_strtolower($data['filter_global'])) . "%'";
				}
				$sql .= " " . implode(" OR ", $implode) . "";
				$sql .= ")";
			}

			if (!empty($data['group_ids'])) {
				$sql .= " AND g.group_id IN (" . implode(',', $data['group_ids']) . ")"; 
			}
		}

		if (!empty($data['sort'])) {
			$sql .= ' ORDER BY ' . $data['sort'];
		} else {
			$sql .= ' ORDER BY a.attribute_id DESC';
		}

		if (isset($data['start']) && isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		// p($sql,1);
		return $this->pdb->query($sql)->rows;
	}

	public function edit($attribute_id,$data) {
		$sql = "UPDATE " . DB_PREFIX . "attribute SET ";
		foreach ($data as $key => $value) {
			$sql .= "$key = '" . $this->pdb->escape($value) . "',";
		}
		$sql = trim($sql,",") . " WHERE attribute_id = " . (int)$attribute_id;
		// p($sql,1);
		$this->pdb->query($sql);
	}

	public function add($data) {
		$sql = "INSERT INTO " . DB_PREFIX . "attribute SET ";
		foreach ($data as $key => $value) {
			$sql .= "$key = '" . $this->pdb->escape($value) . "',";
		}
		$sql = trim($sql,",");
		$this->pdb->query($sql);
	}
}