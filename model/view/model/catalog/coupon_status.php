<?php
class ModelCatalogCouponStatus extends Model {
	public function addCouponStatus($data) {
		$this->pdb->query("INSERT INTO " . PDB_PREFIX . "coupon_status SET coupon_status_name = '" . $this->db->escape($data['coupon_status_name']) . "'");
		
		return $this->db->getLastId();
	}

	public function editCouponStatus($coupon_status_id, $data) {
		$this->pdb->query("UPDATE " . PDB_PREFIX . "coupon_status SET  
		coupon_status_name = '" . $this->db->escape($data['coupon_status_name']) . "'
		WHERE coupon_status_id = '" . (int)$coupon_status_id . "'");
	}

	public function deleteCouponStatus($coupon_status_id) {
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "coupon_status WHERE coupon_status_id = '" . (int)$coupon_status_id . "'");
	}

	public function getCouponStatus($coupon_status_id) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "coupon_status WHERE coupon_status_id = '" . (int)$coupon_status_id . "'");

		return $query->row;
	}

	
	public function getCouponStatuses($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . PDB_PREFIX . "coupon_status p WHERE 1";
			
			/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "p.coupon_status_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 

			$sort_data = array(
				'p.coupon_status_name'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY p.coupon_status_id";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

		} else {
			$sql = "SELECT * FROM " . PDB_PREFIX . "coupon_status p ORDER BY p.coupon_status_id DESC";
		}
		$query = $this->pdb->query($sql);

		$status = array();
		if($query->rows){
			foreach ($query->rows as $row) {
				$status[$row['coupon_status_id']] = $row;
			}
		}
		return $status;
	}

	public function getTotalCouponStatuses($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "coupon_status p WHERE 1";
			
			/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "p.coupon_status_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 
			
			$query = $this->pdb->query($sql);

		return $query->row['total'];
	}
	public function getCouponStatusesIndexBy($index_by = 'coupon_status_id') {
		$query_data = array();
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "coupon_status");

		foreach ($query->rows as $row){
			$query_data[$row[$index_by]] = $row;
		}

		return $query_data;
	}
}