<?php
class ModelChartHighcharts extends Model {
	public function countTotalPlansByIDS($customers_manager=array()) {
			$sql = "SELECT pl.plan_rating, COUNT(*) AS plan_total FROM " . PDB_PREFIX . "plan pl WHERE pl.plan_qc = '1'";
		
		
		if (!empty($customers_manager)) {
			$sql .= " AND pl.`customer_user_id` IN('".implode("','",$customers_manager)."')";
		}
			 $sql .= " GROUP BY pl.plan_rating";
			$query = $this->pdb->query($sql);
			
			$plan_total['unsuccess'] = NULL;
			$plan_total['success'] = NULL;
			$plan_total['fail'] = NULL;
		foreach($query->rows as $row){
			if($row['plan_rating']==-2){
				$plan_total['unsuccess'] = $unsuccess = (int)$row['plan_total'];
			}
			if($row['plan_rating']==1){
				$plan_total['success'] = $row['plan_total'];
			}
			if($row['plan_rating']==-1){
				$plan_total['fail'] = $row['plan_total'];
			}
			
		}
		
		return $plan_total;
		
	}
	
	public function countTotalPlansBySup($customer_user_id=0) {
			$sql = "SELECT pl.plan_rating, COUNT(*) AS plan_total FROM " . PDB_PREFIX . "plan pl WHERE pl.plan_qc = '1'";
		
		
		if ($customer_user_id>0) {
			$sql .= " AND pl.customer_user_id = '" .(int)$customer_user_id . "'";
		}
			 $sql .= " GROUP BY pl.plan_rating";
			$query = $this->pdb->query($sql);
			
			$plan_total['unsuccess'] = NULL;
			$plan_total['success'] = NULL;
			$plan_total['fail'] = NULL;
		foreach($query->rows as $row){
			if($row['plan_rating']==-2){
				$plan_total['unsuccess'] = $unsuccess = (int)$row['plan_total'];
			}
			if($row['plan_rating']==1){
				$plan_total['success'] = $row['plan_total'];
			}
			if($row['plan_rating']==-1){
				$plan_total['fail'] = $row['plan_total'];
			}
			
		}
		
		return $plan_total;
		
	}
}