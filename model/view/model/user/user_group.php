<?php
class ModelUserUserGroup extends Model {
	public function getUserGroups($data = array()) {
		$level = $this->db->query("SELECT group_level FROM `" .DB_PREFIX."user_group` WHERE `user_group_id` = '" . (int)$this->user->getGroupId() ."'");
		$sql = "SELECT *, (SELECT p.group_description FROM `" . DB_PREFIX . "user_group` p WHERE p.user_group_id = ug.child_group_id) AS child_group FROM " . DB_PREFIX . "user_group ug WHERE ug.`group_level` > " . $level->row['group_level'] . " AND user_group_id < 17";

		$sql .= " ORDER BY ug.user_group_id";

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalUserGroups() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "user_group");

		return $query->row['total'];
	}

	public function getGroupNameById($user_group_id){
		$query = $this->db->query("SELECT group_name FROM `".DB_PREFIX."user_group` WHERE `user_group_id` = '" . (int)$user_group_id . "'");

		return isset($query->row['group_name'])?$query->row['group_name']:'';
	}
}