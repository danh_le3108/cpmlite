  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
     <div class="box box-info <?php echo (isset($form_class)&&$form_class=='hide')?'show':'hide';?>" id="filter-reason">
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label class="control-label" for="filter_name"><?php echo $text_search; ?></label>
            <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $text_search; ?>" id="filter_name" class="form-control" onchange="filter()"/>
          </div>
        </div>
        <div class="col-sm-3">
          <button type="button" id="button-clear" class="btn btn-primary pull-right"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>
        </div>
         <div class="col-sm-3 text-right">
      <?php if(isset($form_class)&&$form_class=='hide'){?>
      <?php if($user_add){?>
      <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
      <?php } ?>
      
      <?php if($user_delete){?>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-reason').submit() : false;"><i class="fa fa-trash-o"></i></button>
    <?php } ?>
        <?php }else{?>
     <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_back; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
        <?php }?>
      </div>
      
      </div>
    </div>
    <!--panel-body -->
  </div>
  <!--box -->
  
    <div class="box box-info <?php echo $form_class;?>">
      <div class="box-header">
        <h3 class="box-title"> <?php echo $form_heading; ?></h3>
        <div class="pull-right box-tools text-right">
         <button type="submit" form="form-country_form" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
         <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_back; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
        </div>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-country_form" class="form-horizontal">
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $text_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="reason_name" value="<?php echo $reason_name; ?>" placeholder="<?php echo $text_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-sort_order"><?php echo $text_sort_order; ?></label>
            <div class="col-sm-10">
              <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $text_sort_order; ?>" id="input-sort_order" class="form-control" />
            </div>
          </div>
        </form>
         
      </div>
    </div>
    
    
    <div class="box box-info <?php echo (isset($form_class)&&$form_class=='hide')?'show':'hide';?>">
      <div class="box-header">
        <h3 class="box-title"> <?php echo $heading_title; ?></h3>
        
        
      </div>
      <div class="panel-body">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-reason">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td style="width: 1px;" class="text-left">ID</td>
                  <td class="text-left"><?php if ($sort == 'reason_name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $text_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $text_name; ?></a>
                    <?php } ?></td>
                  <td style="width: 1px;" class="text-left">Sort</td>
                
                  <td class="text-right"><?php echo $text_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($reasons) { ?>
                <?php foreach ($reasons as $reason) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($reason['reason_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $reason['reason_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $reason['reason_id']; ?>" />
                    <?php } ?></td>
                  <td class="text-left"><?php echo $reason['reason_id']; ?></td>
                  <td class="text-left"><?php echo $reason['reason_name']; ?></td>
                  <td class="text-left"><?php echo $reason['sort_order']; ?></td>
                  <td class="text-right">
                  
                  <?php foreach($reason['action'] as $action){?>
                  <a class="btn btn-sm btn-primary" href="<?php echo $action['href'];?>" data-toggle="tooltip" title="<?php echo $action['text'];?>"><i class="fa <?php echo $action['icon'];?>"></i></a>
                <?php } ?> 
                  
                  </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form><div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      </div>
    </div>
  </div>

 
<script type="text/javascript"><!--
  function filter() {
  	url = 'index.php?route=localisation/reason_unsuccess';

	var filter_name = $('input[name=\'filter_name\']').val();

  	if (filter_name) {
  		url += '&filter_name=' + encodeURIComponent(filter_name);
  	}
  	var filter_prefix = $('select[name=\'filter_prefix\']').val();

  	if (filter_prefix != '*') {
  		url += '&filter_prefix=' + encodeURIComponent(filter_prefix);
  	}
  	location = url;
  }
  //-->
</script>