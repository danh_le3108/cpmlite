 <div class="modal-dialog modal-lg">
 
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="btn btn-xs btn-danger pull-right" data-dismiss="modal" aria-hidden="true">x</button>
            
     <a onClick="saveRoute('answer','<?php echo $survey_id;?>','<?php echo isset($info['answer_id'])?$info['answer_id']:0;?>');" class="btn btn-primary btn-xs pull-right"><i class="fa fa-save"></i> <?php echo $button_save; ?></a>
          </div>
            <div class="modal-body">
            
           <input type="hidden" name="question_id" value="<?php echo $question_id;?>"/>
           
           <?php if(isset($info['answer_id'])){ ?>
           	<input type="hidden" name="answer_id" value="<?php echo $info['answer_id'];?>"/>
           <?php } ?>
           
            <div class="row form-group">
          
            <div class="col-sm-6">

            <label class="control-label" for="answer_value">Value</label>

            <div class="clearfix">

              <input type="text" name="answer_value" value="<?php echo isset($info['answer_value'])?$info['answer_value']:'';?>" placeholder="Value" id="answer_value" class="form-control" />

            </div>

          </div>
            <div class="col-sm-6">

            <label class="control-label" for="answer_title">Label</label>

            <div class="clearfix">

              <input type="text" name="answer_title" value="<?php echo isset($info['answer_title'])?$info['answer_title']:'';?>" placeholder="Label" id="answer_title" class="form-control" />

            </div>

          </div>
          </div><!--//row--> 
          
            </div>
    <div class="modal-footer">
     <a onClick="saveRoute('answer','<?php echo $survey_id;?>','<?php echo isset($info['answer_id'])?$info['answer_id']:0;?>');" class="btn btn-primary btn-xs pull-right"><i class="fa fa-save"></i> <?php echo $button_save; ?></a>
    </div>
    <!--//box-footer -->
        </div>