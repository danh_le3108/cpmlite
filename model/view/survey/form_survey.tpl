

          <!-- Your Page Content Here -->
<style>
.btn+.btn{
	margin-right:5px;
}
.module_list {
    position: absolute;
    top: 0px;
    right: 0;
    left: 15px;
    bottom: 0px;
    padding: 0;
}

</style>
<script>
if (typeof rcookie == 'undefined') {
 jQuery.rcookie=function(key,value,options){if(arguments.length>1&&String(value)!=="[object Object]"){options=jQuery.extend({},options);if(value===null||value===undefined){options.expires=-1}if(typeof options.expires==="number"){var days=options.expires,t=options.expires=new Date();t.setDate(t.getDate()+days)}value=String(value);return(document.cookie=[encodeURIComponent(key),"=",options.raw?value:encodeURIComponent(value),options.expires?"; expires="+options.expires.toUTCString():"",options.path?"; path="+options.path:"",options.domain?"; domain="+options.domain:"",options.secure?"; secure":""].join(""))}options=value||{};var result,decode=options.raw?function(s){return s}:decodeURIComponent;return(result=new RegExp("(?:^|; )"+encodeURIComponent(key)+"=([^;]*)").exec(document.cookie))?decode(result[1]):null};
}
/*!
 * jQuery lockfixed plugin
 * http://www.directlyrics.com/code/lockfixed/
 *
 * Copyright 2012 Yvo Schaap
 * Released under the MIT license
 * http://www.directlyrics.com/code/lockfixed/license.txt
 *
 * Date: Sun Feb 9 2014 12:00:01 GMT
 */
 /**/ 
(function($, undefined){
	$.extend({
		"lockfixed": function(el, config){
			if (config && config.offset) {
				config.offset.bottom = parseInt(config.offset.bottom,10);
				config.offset.top = parseInt(config.offset.top,10);
			}else{
				config.offset = {bottom: 100, top: 0};	
			}
			var el = $(el);
			if(el && el.offset()){
				var el_position = el.css("position"),
					el_margin_top = parseInt(el.css("marginTop"),10),
					el_position_top = el.css("top"),
					el_top = el.offset().top,
					pos_not_fixed = false;
				if (config.forcemargin === true || navigator.userAgent.match(/\bMSIE (4|5|6)\./) || navigator.userAgent.match(/\bOS ([0-9])_/) || navigator.userAgent.match(/\bAndroid ([0-9])\./i)){
					pos_not_fixed = true;
				}

				$(window).on('scroll resize orientationchange load lockfixed:pageupdate',el,function(e){
					// if we have a input focus don't change this (for smaller screens)
					if(pos_not_fixed && document.activeElement && document.activeElement.nodeName === "INPUT"){
						return;	
					}

					var top = 0,
						el_height = el.outerHeight(),
						el_width = $('.module_list').outerWidth(),
						max_height = $(document).height() - config.offset.bottom,
						scroll_top = $(window).scrollTop();
 
					// if element is not currently fixed position, reset measurements ( this handles DOM changes in dynamic pages )
					if (el.css("position") !== "fixed" && !pos_not_fixed) {
						el_top = el.offset().top;
						el_position_top = el.css("top");
					}
					if (scroll_top >= (el_top-(el_margin_top ? el_margin_top : 0)-config.offset.top)){

						if(max_height < (scroll_top + el_height + el_margin_top + config.offset.top)){
							top = (scroll_top + el_height + el_margin_top + config.offset.top) - max_height;
						}else{
							top = 0;	
						}
						if (pos_not_fixed){
							el.css({'marginTop': (parseInt(scroll_top - el_top - top,10) + (2 * config.offset.top))+'px'});
						}else{
							el.css({'position': 'fixed','top':(config.offset.top-top)+'px','width':el_width +"px"});
						}
					}else{
						el.css({'position': el_position,'top': el_position_top, 'width':el_width +"px", 'marginTop': (el_margin_top && !pos_not_fixed ? el_margin_top : 0)+"px"});
					}
				});	
			}
		}
	});
})(jQuery);
</script>


    <div class="container-fluid">
    
    
    <div class="box">

      <div class="box-header">

        <h2 class="box-title"> <?php echo $heading_title; ?></h3>
        
    
          
<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default pull-right"><i class="fa fa-reply"></i></a>

        <button type="submit" form="form-survey" class="btn btn-success pull-right" style="margin-right: 10px;"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
         <div class="form-group pull-right" style="max-width:300px;margin-right: 10px;">

            <div class="clearfix">
 			<select name="change_survey" id="change_survey" class="form-control" onchange="changeSurvey();">
                <?php foreach ($surveys as $survey) { ?>
               
                <option value="<?php echo $survey['survey_id'];?>"  <?php if ($survey['survey_id']==$survey_id) { ?>selected="selected"<?php } ?>><?php echo $survey['survey_id'];?> - <?php echo $survey['survey_name'];?></option>
               <?php } ?>
              </select>

            </div>
          </div>
          
      </div>

      <div class="panel-body">

        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-survey" class="form-horizontal">

          <div class="col-sm-2">

            <label class="clearfix col-sm-12" for="input-name"><?php echo $text_name; ?></label>

            <div class="clearfix">

              <input type="text" name="survey_name" value="<?php echo $survey_name; ?>" placeholder="<?php echo $text_name; ?>" id="input-name" class="form-control" />

              <?php if ($error_name) { ?>

              <div class="text-danger"><?php echo $error_name; ?></div>

              <?php } ?>

            </div>

          </div>
          
        

          <div class="col-sm-2">

            
			<label class="clearfix col-sm-12" for="input-code"><?php echo $text_description; ?></label>

            <div class="clearfix">

              <input type="text" name="survey_description" value="<?php echo $survey_description; ?>" placeholder="<?php echo $text_description; ?>" id="input-code" class="form-control" />
            </div>
            
            <label class="clearfix col-sm-12" for="sort_order"><?php echo $text_sort_order; ?></label>
            <div class="clearfix">
              <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $text_sort_order; ?>" id="sort_order" class="form-control" />
            </div>
            
          </div>

          <div class="col-sm-2">
          
            <label class="clearfix" for="is_rating"><?php echo $text_is_rating; ?></label>
           <div class="clearfix">
            <select name="is_rating" id="is_rating" class="form-control">
                <option value="0" <?php if ($is_rating==0) { ?>selected="selected"<?php } ?>><?php echo $text_no;?></option>
                <option value="1" <?php if ($is_rating==1) { ?>selected="selected"<?php } ?>><?php echo $text_yes;?></option>
              </select>
            </div>
            
          </div>
          
          <div class="col-sm-2">
          
            
             <label class="clearfix" for="coupon_prefix"><?php echo $text_coupon_prefix; ?></label>
           <div class="clearfix">
            <select name="coupon_prefix" id="coupon_prefix" class="form-control">
                <option value="" <?php if ($coupon_prefix=='') { ?>selected="selected"<?php } ?>><?php echo $text_none;?></option>
                <?php foreach ($prefixs as $prefix){?>
                <option value="<?php echo $prefix['coupon_prefix'];?>" <?php if ($coupon_prefix==$prefix['coupon_prefix']) { ?>selected="selected"<?php } ?>><?php echo $prefix['coupon_prefix'];?> - <?php echo $prefix['prefix_value'];?></option>
                <?php } ?>
              </select>
            </div>
            
            
          </div>
          
          
          <div class="col-sm-2">
            <label class="clearfix col-sm-12" for="pass_point"><?php echo $text_pass_point; ?></label>
            <div class="clearfix">
              <input type="text" name="pass_point" value="<?php echo $pass_point; ?>" placeholder="<?php echo $text_pass_point; ?>" id="pass_point" class="form-control" />
            </div>
             <label class="clearfix col-sm-12" for="ratio"><?php echo $text_passratio; ?></label>
            <div class="clearfix">
              <input type="text" name="ratio" value="<?php echo $ratio; ?>" placeholder="<?php echo $text_passratio; ?>" id="ratio" class="form-control" />
            </div>
            
          </div>
          
          
        <?php if(empty($groups)){?>
          <?php } ?>
        <div class="col-sm-2">
          <div class="form-group">

            <label class="clearfix col-sm-12" for="sort_order"><?php echo $text_copy_from; ?></label>

            <div class="clearfix">
 			<select name="copy_id" id="copy_id" class="form-control">
                <option value="">-None-</option>
                <?php foreach ($surveys as $survey) { ?>
                <?php if ($survey['survey_id']!=$survey_id) { ?>
                <option value="<?php echo $survey['survey_id'];?>"><?php echo $survey['survey_id'];?> - <?php echo $survey['survey_name'];?></option>
               <?php } ?>
               <?php } ?>
              </select>

            </div>
          </div>
      </div>
      
        </form>

      </div>


  </div><!--//box--> 
  

  </div>


  <div class="container-fluid">

    <?php if ($error_warning) { ?>

    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>

      <button type="button" class="close" data-dismiss="alert">&times;</button>

    </div>

    <?php } ?>

    <?php if ($success) { ?>

    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>

      <button type="button" class="close" data-dismiss="alert">&times;</button>

    </div>

    <?php } ?>
  
  

<div class="row">

<div class="col-sm-3" style="min-height:800px;">
<div class="module_list">
<div id="module_list">

    <div class="box">

      <div class="box-header">

        <h2 class="box-title"> <?php echo $text_question_group; ?></h2>

      </div>

      <div class="panel-body" id="form-group" >

           <input type="hidden" name="survey_id" value="<?php echo $survey_id;?>"/>
          <div class="form-group">

            <label class="control-label" for="group_title"><?php echo $text_group_title; ?></label>

            <div class="clearfix">

              <input type="text" name="group_title" value="" placeholder="<?php echo $text_group_title; ?>" id="group_title" class="form-control" />

            </div>

          </div>
          
          <div class="row">
          <div class="col-sm-6">
            <label class="control-label" for="sort_order"><?php echo $text_sort_order; ?></label>
            <div class="clearfix">
              <input type="text" name="sort_order" value="" placeholder="<?php echo $text_sort_order; ?>" id="sort_order" class="form-control" />
            </div>
          </div>
          <div class="col-sm-6">
          
            <label class="control-label" for="group_title"><?php echo $text_target; ?></label>

            <div class="clearfix">

              <input type="text" name="group_target" value="" placeholder="<?php echo $text_target; ?>" id="group_target" class="form-control" />

            </div>
          </div><!--//col--> 
          </div><!--//row-->  
 			<div class="form-group">
            <label class="control-label" for="group_code"><?php echo $text_code; ?></label>
            <div class="clearfix">
             <select name="group_code" id="group_code" class="form-control">
                <option value="">-None-</option>
                <?php foreach ($attr_groups as $group) { ?>
                <option value="<?php echo $group['group_code'];?>"><?php echo $group['group_name'];?></option>
              <?php } ?>
              </select>
            </div>

          </div>
      </div>

    <div class="box-footer">
        <a onClick="addRoute('group','<?php echo $survey_id;?>');" class="btn btn-primary btn-xs pull-right"><i class="fa fa-plus"></i> <?php echo $button_add; ?></a>
    </div>
    <!--//box-footer -->

  </div><!--//box--> 
  
  
    <div class="box">

      <div class="box-header">

        <h2 class="box-title"> <?php echo $text_question; ?></h2>

      </div>

      <div class="panel-body" id="form-question" >

           <input type="hidden" name="survey_id" value="<?php echo $survey_id;?>"/>
          <div class="required">

            <label class="control-label" for="question_title"><?php echo $text_question_title; ?></label>

            <div class="clearfix">

              <input type="text" id="question_title" name="question_title" value="" placeholder="<?php echo $text_question_title; ?>" id="question_title" class="form-control" />

            </div>

          </div>
          <div class="form-group">

            <label class="control-label" for="question_group_id"><?php echo $text_group; ?></label>
            <div class="clearfix">
            <select name="group_id" id="question_group_id" class="form-control">
                <?php foreach ($groups as $group) { ?>
                <option value="<?php echo $group['group_id'];?>"><?php echo $group['group_title'];?></option>
              <?php } ?>
              </select>
            </div>

          </div>
          <div class="form-group">
            <label class="control-label" for="attribute_id"><?php echo $text_attribute; ?></label>
            <div class="clearfix">
            <select name="attribute_id" id="attribute_id" class="form-control chosen" onchange="addTitle('question_title',this.options[this.selectedIndex].text);">
                <option value="*">-None-</option>
                <?php foreach ($attr_groups as $group) { ?>
                <?php if(!empty($group['attributes'])) { ?>
                 <optgroup label="<?php echo $group['group_name'];?>">
                <?php foreach ($group['attributes'] as $attribute) { ?>
                <option value="<?php echo $attribute['attribute_id'];?>">#<?php echo $attribute['attribute_code'];?> <?php echo $attribute['attribute_name'];?></option>
              <?php } ?>
                </optgroup>
              <?php } ?>
              <?php } ?>
              </select>
            </div>
          </div> 
          
          <div class="row">
          <div class="col-sm-6">
            <label class="control-label" for="sort_order"><?php echo $text_sort_order; ?></label>
            <div class="clearfix">
              <input type="text" name="sort_order" value="" placeholder="<?php echo $text_sort_order; ?>" id="sort_order" class="form-control" />
            </div>
          </div>
          <div class="col-sm-6">

            <label class="control-label" for="input_type"><?php echo $text_input_type; ?></label>

            <div class="clearfix">

          
            <select name="input_type" id="input_type" class="form-control">

                <?php foreach ($inputs as $value=>$label) { ?>

                <option value="<?php echo $value;?>"><?php echo $label;?></option>

              <?php } ?>

              </select>
            </div>

          </div>
          
          </div><!--//row-->  

      </div>

    <div class="box-footer">
    
        <a onClick="addRoute('question','<?php echo $survey_id;?>');" class="btn btn-primary btn-xs pull-right"><i class="fa fa-plus"></i> <?php echo $button_add; ?></a>
    </div>
    <!--//box-footer -->

  </div><!--//box--> 
  
  
  
  </div><!--//#module_list-->
  </div><!--//mlist-->
  
  
  </div><!--//col--> 
  
<div class="col-sm-9">
  <div id="survey_group">
  <?php echo $survey_group;?>
  </div>
  
  </div><!--//col--> 

  
  </div><!--//row--> 

</div>

</section>

    <!-- /.content -->
<div id="modal_survey" class="modal fade">
       
</div>

<div id="modal-confirm" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        Bạn có chắc chắn muốn xóa <span></span>? 
      </div>
      <div class="modal-body">
        <div class="mpadding">
          Phần bị xóa sẽ không thể khôi phục! 
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success pull-right" data-dismiss="modal" aria-hidden="true"><?php echo $text_no;?></button>
        <a id="btn-modal-confirm" data-id="" class="btn btn-danger pull-right" style="margin-right:15px;"><?php echo $text_yes;?></a>
      </div>
    </div>
  </div>
</div>
<!--//module-modal -->

</div><!--//import_history -->
  <script type="text/javascript"><!--
  
	$(document).ready(function() {
		$.lockfixed("#module_list",{offset: {top: 10,right: 20, bottom: 250}});
	});

		function addTitle(elem,title){
			if(title!='-None-'){
				title = $.trim(title.replace(/\#\S+/g, '').replace(' ', ''));
				$('#'+elem).val(title);
			}
		}
	  function addAnswer(survey_id,question_id){
		  $.ajax({
				  url: 'index.php?route=survey/answer/form&survey_id='+survey_id+'&question_id='+question_id,
				  dataType: 'html',
				  beforeSend: function(){},
				  success: function(html) {
					$('#modal_survey').html(html);
					$('#modal_survey').modal('show');
					$('#modal_survey .chosen').chosen({height: "120px",width: "100%"});
				  }
			});
	  }
	  function addRoute(route,survey_id){
		  var group_form = '#form-'+route;
			$.ajax({
			  type: 'POST',
			  url:'index.php?route=survey/'+route+'/save&survey_id='+survey_id,
			 dataType: 'json',
			  data: $(group_form+' input[type=\'text\'],'+group_form+' input[type=\'hidden\'],'+group_form+' input[type=\'radio\']:checked,'+group_form+' input[type=\'checkbox\']:checked,'+group_form+' select,'+group_form+' textarea'),
			  success: function(json) {
					$('#survey_group').load('index.php?route=survey/group/info&survey_id='+survey_id);
					$(group_form+' input[type=\'text\']').val('');
				
			  }
			});
	  }
	  function editRoute(route,survey_id,route_id){
		   $.ajax({
				  url: 'index.php?route=survey/'+route+'/form&survey_id='+survey_id+'&'+route+'_id='+route_id,
				  dataType: 'html',
				  beforeSend: function(){},
				  success: function(html) {
					$('#modal_survey').html(html);
					$('#modal_survey').modal('show');
					$('#modal_survey .chosen').chosen({height: "120px",width: "100%"});
				  }
			});
     }
	  function saveRoute(route,survey_id,route_id){
		  var group_form = '#modal_survey';
			$.ajax({
			  type: 'POST',
			  url:'index.php?route=survey/'+route+'/save&survey_id='+survey_id+'&'+route+'_id='+route_id,
			 dataType: 'json',
			  data: $(group_form+' input[type=\'text\'],'+group_form+' input[type=\'hidden\'],'+group_form+' input[type=\'radio\']:checked,'+group_form+' input[type=\'checkbox\']:checked,'+group_form+' select,'+group_form+' textarea'),
			  success: function(json) {
				if(json['success']) {
					$('#survey_group').load('index.php?route=survey/group/info&survey_id='+survey_id);
					$('#modal_survey').modal('hide');
				}
			  }
			});
	  }
	  function deleteRoute(route,survey_id,route_id){
		  $('#modal-confirm .modal-header span').html(route);
		  $('#modal-confirm').modal('show');
		  $('#btn-modal-confirm').attr('data-id',route_id);
		  $('#btn-modal-confirm').on('click',function(e) {
				e.preventDefault();
				new_route_id =  $(this).attr('data-id');
				if(new_route_id==route_id){
					 $.ajax({
						  url: 'index.php?route=survey/'+route+'/delete&survey_id='+survey_id+'&'+route+'_id='+new_route_id,
						  dataType: 'json',
						  beforeSend: function(){},
						  success: function(json) {
							$('#survey_group').load('index.php?route=survey/group/info&survey_id='+survey_id);
							$('#modal-confirm').modal('hide');
						  }
					});
				}
		  });
     }
	
	  function changeSurvey(){
		  
     	 url = '<?php echo $change;?>';
    
      		var change_survey = $('select[name=\'change_survey\']').val();
		  if (change_survey != '*') {
			url += '&survey_id=' + encodeURIComponent(change_survey);
		  }
      	location = url;
     }	
		
		
    //-->
  </script>