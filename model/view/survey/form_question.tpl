 <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
            
          <div class="modal-header">
            <button type="button" class="btn btn-xs btn-danger pull-right" data-dismiss="modal" aria-hidden="true">x</button>
            
     <a onClick="saveRoute('question','<?php echo $survey_id;?>','<?php echo $question_id;?>');" class="btn btn-primary btn-xs pull-right"><i class="fa fa-save"></i> <?php echo $button_save; ?></a>
          </div>
            <div class="row form-group">
            <div class="col-sm-6">

            <label class="control-label" for="question_title"><?php echo $text_question_title; ?></label>

            <div class="clearfix">

              <input type="text" name="question_title" id="question_title<?php echo $info['question_id'];?>" value="<?php echo $info['question_title'];?>" placeholder="<?php echo $text_question_title; ?>" class="form-control" />

            </div>

          </div>
            <div class="col-sm-6">

             <label class="control-label" for="question_group_id"><?php echo $text_group; ?></label>
            <div class="clearfix">
            <select name="group_id" id="question_group_id" class="form-control">
                <?php foreach ($groups as $group) { ?>
                <option value="<?php echo $group['group_id'];?>" <?php echo ($group['group_id']==$info['group_id'])?'selected="selected"':'';?>><?php echo $group['group_title'];?></option>
              <?php } ?>
              </select>
            </div>

          </div>
          </div><!--//row--> 
          
            <div class="row form-group">
            <div class="col-sm-6">
           <label class="control-label" for="target"><?php echo $text_target; ?></label>

            <div class="clearfix">

              <input type="text" name="target" value="<?php echo $info['target'];?>" placeholder="<?php echo $text_target; ?>" class="form-control" />

            </div>
          </div>
            <div class="col-sm-6">
            <label class="control-label" for="attribute_id"><?php echo $text_attribute; ?></label>
            <div class="clearfix">
            <select name="attribute_id" id="attribute_id" class="form-control chosen" onchange="addTitle('question_title<?php echo $info['question_id'];?>',this.options[this.selectedIndex].text);">
                <option value="*">-None-</option>
                <?php foreach ($attr_groups as $group) { ?>
                <?php if(!empty($group['attributes'])) { ?>
                 <optgroup label="<?php echo $group['group_name'];?>">
                <?php foreach ($group['attributes'] as $attribute) { ?>
                <option value="<?php echo $attribute['attribute_id'];?>" <?php echo ($attribute['attribute_id']==$info['attribute_id'])?'selected="selected"':'';?>>#<?php echo $attribute['attribute_code'];?> <?php echo $attribute['attribute_name'];?></option>
               <?php } ?>
                </optgroup>
              <?php } ?>
              <?php } ?>
              </select>
            </div>
          </div>
          </div><!--//row--> 
          
            <div class="row form-group">
            <div class="col-sm-6">

           <label class="control-label" for="sum_group">Sum group</label>
            <div class="clearfix">
            <select name="sum_group" id="sum_group" class="form-control">
                <option value="0" <?php echo (0==$info['sum_group'])?'selected="selected"':'';?>><?php echo $text_no;?></option>
                <option value="1" <?php echo (1==$info['sum_group'])?'selected="selected"':'';?>><?php echo $text_yes;?></option>
              </select>
            </div>

          </div>
            <div class="col-sm-6">
            <label class="control-label" for="sum_value">Sum value</label>
            <div class="clearfix">
            <select name="sum_value" id="sum_value" class="form-control">
                <option value="0" <?php echo (0==$info['sum_value'])?'selected="selected"':'';?>><?php echo $text_no;?></option>
                <option value="1" <?php echo (1==$info['sum_value'])?'selected="selected"':'';?>><?php echo $text_yes;?></option>
              </select>
            </div>
          </div>
          </div><!--//row--> 
          
            <div class="row form-group">
            <div class="col-sm-6">
             <label class="control-label" for="input_type"><?php echo $text_input_type; ?></label>

            <div class="clearfix">

          
            <select name="input_type" id="input_type" class="form-control">

                <?php foreach ($inputs as $value=>$label) { ?>

                <option value="<?php echo $value;?>" <?php echo ($value==$info['input_type'])?'selected="selected"':'';?>><?php echo $label;?></option>

              <?php } ?>

              </select>
            </div>
          </div>
            <div class="col-sm-6">
            <label class="control-label" for="sort_order"><?php echo $text_sort_order; ?></label>
            <div class="clearfix">
              <input type="text" name="sort_order" value="<?php echo $info['sort_order'];?>" placeholder="<?php echo $text_sort_order; ?>" id="sort_order" class="form-control" />
            </div>
          </div>
          
          </div><!--//row--> 
          
            </div>
    <div class="modal-footer">
     <a onClick="saveRoute('question','<?php echo $survey_id;?>','<?php echo $question_id;?>');" class="btn btn-primary btn-sm pull-right"><i class="fa fa-save"></i> <?php echo $button_save; ?></a>
    </div>
    <!--//box-footer -->
        </div>