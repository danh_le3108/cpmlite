 <?php
 $i=1;
 $sum_all = array();
 $sum_group = array();
 $sum_question = 0;
  foreach($groups as $group){
 $id = $group['group_id'];
 ?>
 
 <div class="box box-info" id="group<?php echo $id; ?>">

      <div class="box-header">

        <h2 class="box-title"><small class="badge"><?php echo $group['sort_order']; ?></small> <?php echo $group['group_title']; ?></h2>

      </div>

      <div class="panel-body">

          <table class="table" style="margin-bottom:0px;">
          <thead>
          <tr>
          <td width="1"></td>
          <td width="1"><?php echo $group['sort_order']; ?></td>
          <td width="1"><?php echo $group['group_code']; ?></td>
          <td width="250"><?php echo $group['group_title']; ?></td>
          <td></td>
          <td><?php echo $group['group_target']; ?></td>
          <td></td>
          <td></td>
          <td width="100">
        <a onClick="editRoute('group','<?php echo $group['survey_id']; ?>','<?php echo $group['group_id']; ?>');" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary btn-xs pull-right"><i class="fa fa-pencil"></i></a>
        
        
          <a onclick="deleteRoute('group','<?php echo $group['survey_id']; ?>','<?php echo $group['group_id'];?>');" title="<?php echo $button_delete; ?>" class="btn btn-danger btn-xs pull-right"><i class="fa fa-trash"></i></a>
          
        </td>
          </tr>
          </thead>
          
          <tbody>
          <tr>
          <td width="1"></td>
          <th width="1">Sort</th>
          <th><?php echo $text_code;?></th>
          <th width="250">Title</th>
          <th>Input type</th>
          <th>Target</th>
          <th>Sum value</th>
          <th>Sum group</th>
          <th width="100"><?php echo $text_action;?></th>
          </tr>
          </tbody>
          
          <tbody>
           <?php if(!empty($group['questions'])){?>
           <?php foreach($group['questions'] as $question){
           
 				$sum_question++;
           
           ?>
          <tr>
          <td width="1"><?php echo $i; $i++;?></td>
          <th width="1"><?php echo $question['sort_order'];?></th>
          <td><?php echo $question['attribute_code'];?></td>
          <td width="250"><small title="<?php echo isset($attr[$question['attribute_id']])?$attr[$question['attribute_id']]['attribute_name']:'';?>"></small> 
          
          <strong><?php
           echo $question['question_title'];?></strong>
          
          
          </td>
          <td><?php echo $question['input_type'];?></td>
          <td><?php
          $sum_group[$id][] = $question['target'];
           echo $question['target'];?></td>
          <td><?php echo $question['sum_value'];?></td>
          <td><?php echo $question['sum_group'];?></td>
          <td>
          
          
          <a onclick="editRoute('question','<?php echo $group['survey_id']; ?>','<?php echo $question['question_id'];?>');" title="<?php echo $button_edit; ?>" class="btn btn-primary btn-xs pull-right"><i class="fa fa-pencil"></i></a>
          <a onclick="deleteRoute('question','<?php echo $group['survey_id']; ?>','<?php echo $question['question_id'];?>');" title="<?php echo $button_delete; ?>" class="btn btn-danger btn-xs pull-right"><i class="fa fa-trash"></i></a>
          
          <a onclick="addAnswer('<?php echo $group['survey_id']; ?>','<?php echo $question['question_id'];?>');" title="<?php echo $button_add; ?>" class="btn btn-success btn-xs pull-right"><i class="fa fa-plus"></i></a>
          </td>
          </tr>
           <?php if(isset($question['answers'])&&!empty($question['answers'])){?>
         
           <?php foreach($question['answers'] as $answer){?>
             <tr>
          <th></th>
          <th width="1">&nbsp;&nbsp;&nbsp;<small><?php echo $answer['answer_value'];?></small></th>
          <td width="250"><?php echo $answer['answer_title'];?>
          </td>
          <td colspan="4"></td>
          <td width="100">
          <a onclick="editRoute('answer','<?php echo $group['survey_id']; ?>','<?php echo $answer['answer_id'];?>');" title="<?php echo $button_edit; ?>" class="btn btn-primary btn-xs pull-right"><i class="fa fa-pencil"></i></a>
          <a onclick="deleteRoute('answer','<?php echo $group['survey_id']; ?>','<?php echo $answer['answer_id'];?>');" title="<?php echo $button_delete; ?>" class="btn btn-danger btn-xs pull-right"><i class="fa fa-trash"></i></a>
          </td>
          </tr>
  <?php } ?>
  <?php } ?>
          <!--//answers -->
  <?php } ?>
  <?php } ?>
          <tr>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th><?php echo $text_total;?></th>
          <th><strong><?php 
          $sum_all[] = $sum[$id] = isset($sum_group[$id])?array_sum($sum_group[$id]):0;
          echo $sum[$id];?></strong></th>
          <th></th>
          <th></th>
          <th></th>
          <td></td>
          </tr>
          </tbody>
          
          
          </table>

      </div><!--//panel-body -->

    <div class="box-footer">
    </div>
    <!--//box-footer -->
  </div><!--//box--> 
  
  <?php } ?>
  
  <div class="box box-info">

      <div class="box-header">
<table class="table" style="margin-bottom:0px;">
          
          <tr>
          <th width="100"><small class="badge"><?php echo $sum_question; ?> <?php echo $text_question;?></small> </th>
          <th></th>
          <th></th>
          <th></th>
          <th class="text-right"><?php echo $text_total;?> số mặt</th>
          <th><?php echo isset($sum_all)?array_sum($sum_all):0;?></th>
          <th></th>
          <th></th>
          <th></th>
          <td></td>
          </tr>
          
          
       
          </table>   

      </div>

  </div><!--//box--> 