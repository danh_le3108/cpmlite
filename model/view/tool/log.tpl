

  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="box">
            <div class="box-header">
              <h2 class="box-title"> <?php echo $text_list; ?></h3>
               <div class="pull-right">
        <a href="<?php echo $download; ?>" data-toggle="tooltip" title="<?php echo $button_download; ?>" class="btn btn-primary"><i class="fa fa-download"></i></a>
        <a onclick="confirmDelete();" data-toggle="tooltip" title="<?php echo $button_clear; ?>" class="btn btn-danger"><i class="fa fa-eraser"></i></a>
      </div>
      
      </div>
      <div class="panel-body">
        <div class="form-line">

<textarea wrap="off" rows="20" class="form-control"><?php echo $log; ?></textarea></div>
      </div>
    </div>
  </div>
  
<div id="modal-confirm" class="modal fade">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
       Xác nhận
      </div>
      <div class="modal-body">
        <div class="mpadding">
         <?php echo $text_confirm; ?>
        </div>
      </div>
      <div class="modal-footer">
        <a id="btn-modal-confirm" class="btn btn-danger pull-right">Xác nhận Xóa</a>
        <button type="button" class="btn btn-success pull-right" data-dismiss="modal" aria-hidden="true" style="margin-right:15px;">Không</button>
      </div>
    </div>
  </div>
</div>
<!--//module-modal -->
  
<script type="text/javascript"><!--
    function confirmDelete(){
      $('#modal-confirm').modal('show');
      $('#btn-modal-confirm').click(function(event) {
        event.preventDefault();
          $('#modal-confirm').modal('hide');
		   location.href='<?php echo $clear; ?>';
      });
    }
    //-->
</script>