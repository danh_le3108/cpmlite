<?php echo $header;?>
<style>
#chart-group .btn .badge {
	display:none;
    color: #999;
    background-color: #fafafa;
}
#chart-group .btn.active .badge {
	display: inline-block;
}
.progress-bar-grey {
    background-color: #898e92;
}
.progress-bar {
    color: black;
}
</style>

  
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
    </div>
  </div>
  <div class="container-fluid">
   <div class="box box-info" id="filter-area">
    <div class="panel-body">
     <div class="row">
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label" for="filter_round"><?php echo $text_round; ?></label>
            <input type="hidden" name="filter_level_id" value="<?php echo $filter_level_id;?>"/>
            <select name="filter_round" id="filter_round" class="form-control" onchange="filter();">
              <?php foreach ($rounds as $round) { ?>
              <option value="<?php echo $round['round_name']; ?>" <?php if ($round['round_name'] == $filter_round) { ?>selected="selected"<?php } ?>><?php echo $round['round_name']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-sm-9">
        
          <button type="button" id="button-clear" class="btn btn-primary pull-right"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button><!--//--> 
        </div>
      </div>
      
      <!--//row--> 
      <div class="row">
        <div class="col-sm-12">
          <div class="form-group">
            <div class="clearfix">
              <div id="chart-group" class="btn-group btn-group-justified">
                <?php foreach ($user_groups as $user_group) { ?>
                
                <?php if($isNationwide==1||($isLogged&&$logged_group<=$user_group['customer_level_id'])){ ?>
                <a onclick="$('input[name=\'filter_level_id\']').val('<?php echo $user_group['customer_level_id']; ?>');filter();" class="btn btn-<?php echo ($user_group['customer_level_id'] == $filter_level_id)?'success active':'primary'; ?>"><?php echo $user_group['level_name']; ?>
                </a>
                <?php } ?>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--panel-body -->
  </div>
  <!--box -->
  
          <?php
                $i=0;
           foreach($user_groups as $user_group){?>
           
           <?php foreach ($project_users as $user) {?>
                 
              <?php if($user['total']>0&&$user['customer_level_id']==$user_group['customer_level_id']){
              
              ?>
                
                <?php if(($isNationwide==1&&is_null($filter_level_id)&&$user['customer_level_id']==$config_customer_top&&$i==0)||($isNationwide==1&&$filter_level_id==$config_customer_top&&$i==0)||($isLogged==$user['customer_user_id']&&is_null($filter_level_id))||($filter_level_id==$user['customer_level_id']&&in_array($user['customer_user_id'],$customers_manager))){
                if(($isNationwide==1&&is_null($filter_level_id)&&$user['customer_level_id']==$config_customer_top&&$i==1)||($isNationwide==1&&$filter_level_id==$config_customer_top&&$i==1)){
               continue;
                 }
                 $i++;
                ?>
                <div class="row">
                <div class="col-sm-8"> 
             
   <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title"><?php if($user['customer_level_id']!=$config_customer_top){?><?php echo $user['fullname'];?><?php }else{?> <?php echo $user['level_name']; ?> <?php } ?></h3>
        </div>
        <div class="box-body chart-responsive">
          <div class="chart" id="chart<?php echo $user['customer_user_id'];?>" style="height: 300px;; position: relative;"></div>
        </div>
        <!--//box-body -->
      </div>
      <!--//box -->
         
     			 </div><!--//col -->  
                 <div class="col-sm-4"> 
                 
                     
             <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title"> &nbsp;#<?php echo $user['customer_level_id']; ?> -  <?php echo $user['level_name']; ?> <?php if($user['customer_level_id']!=$config_customer_top){?>-  <?php echo $user['fullname'];?> <?php } ?></h3>
        </div>
        <div class="box-body">
          <table class="table">
            <tr>
              <td width="60"><?php echo $text_total;?></td>
              <td width="140">
                <div class="progress">
                  <div class="progress-bar " role="progressbar" aria-valuenow="100"
                    aria-valuemin="0" aria-valuemax="100" style="width:100%">
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $user['total'];?></td>
              <td class="text-right"><a href="<?php echo $user['url_total']; ?>" target="_blank"><?php echo $text_details;?></a></td>
               <?php if(1==1){ ?> 
              <td class="text-right"><a href="<?php echo $user['down_total']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td><?php } ?>
            </tr>
            <tr>
              <td width="60"><?php echo $text_rating_passed;?>:</td>
              <td>
                  <?php 
                  $success_percent = ($user['total']>0)?round(($user['success']/$user['total'])*100,2):0;
                   ?>
                <div class="progress">
                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $success_percent;?>" aria-valuemin="0" aria-valuemax="<?php echo $success_percent;?>" style="width:<?php echo $success_percent;?>%"><?php echo $success_percent;?>%
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $user['success'];?> </td>
              <td class="text-right"><a href="<?php echo $user['url_success']; ?>" target="_blank"><?php echo $text_details;?></a></td>
               <?php if(1==1){ ?>
              <td class="text-right"><a href="<?php echo $user['down_success']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td> <?php } ?>
            </tr>
            <tr>
              <td width="60"><?php echo $text_rating_not_passed;?></td>
              <td>
                <div class="progress">
                  <?php $unsuccess_percent = ($user['total']>0)?round(($user['unsuccess']/$user['total'])*100,2):0;?>
                  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo $unsuccess_percent;?>" aria-valuemin="0" aria-valuemax="<?php echo $unsuccess_percent;?>" style="width:<?php echo $unsuccess_percent;?>%"><?php echo $unsuccess_percent;?>%
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $user['unsuccess'];?> </td>
              <td class="text-right"><a href="<?php echo $user['url_unsuccess']; ?>" target="_blank"><?php echo $text_details;?></a></td>
               <?php if(1==1){ ?>
              <td class="text-right"><a href="<?php echo $user['down_unsuccess']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td> <?php } ?>
            </tr>
            <tr>
              <td width="60"><?php echo $text_rating_unsuccess;?></td>
              <td>
                <div class="progress">
                  <?php $fail_percent = ($user['total']>0)?round(($user['fail']/$user['total'])*100,2):0;?>
                  <div class="progress-bar progress-bar-grey" role="progressbar" aria-valuenow="<?php echo $fail_percent;?>" aria-valuemin="0" aria-valuemax="<?php echo $fail_percent;?>" style="width:<?php echo $fail_percent;?>%"><?php echo $fail_percent;?>%
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $user['fail'];?> </td>
              <td class="text-right"><a href="<?php echo $user['url_fail']; ?>" target="_blank"><?php echo $text_details;?></a></td>
               <?php if(1==1){ ?>
              <td class="text-right"><a href="<?php echo $user['down_fail']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td> <?php } ?>
            </tr>
          </table>
        </div>
        <!--//box-body -->
      </div>
      <!--//box -->  
      
      
     			 </div><!--//col -->  
     			 </div><!--//row -->  
      <?php $ndata = array();?>
      <?php $is_children = array();?>
      
       <?php if($user['customer_level_id']==$config_customer_top){?>
           <?php foreach ($project_users as $staff) {  ?>
                <?php if($staff['customer_level_id']==($user['customer_level_id']+1)){?>
      <?php
             $is_children[] = 1;
            $ndata[] = '{y:"'.$staff['fullname'].' ('.$staff['total'].')",'.implode(",", $staff['chart']).'}';
        ?>         
                <?php } ?>
                
          <?php } ?>
      <?php }else{ ?>
      
           <?php foreach ($project_users as $staff) {  ?>
                <?php if($staff['customer_parent_id']==$user['customer_user_id']){?>
      <?php
             $is_children[] = 1;
            $ndata[] = '{y:"'.$staff['fullname'].' ('.$staff['total'].')",'.implode(",", $staff['chart']).'}';
        ?>         
                <?php } ?>
                
          <?php } ?>
      
      <?php } ?>
      
       <?php if(!in_array(1,$is_children)){?>
            
    	<?php $ndata[] = '{y:"'.$user['fullname'].' ('.$user['total'].')",'.implode(",", $user['chart']).'}';?>
            
      <?php } ?>
      
<script type="text/javascript"><!--
  $(document).ready(function(){

  	//BAR CHART
  	var nbar = new Morris.Bar({
  	  element: 'chart<?php echo $user['customer_user_id'];?>',
  	  barSizeRatio:0.3,
  	  data: [<?php echo implode(",",$ndata); ?>],
  	  barColors: [<?php echo implode(",", $colors)?>],
  	  xkey: 'y',
  	  ykeys: [<?php echo implode(",", $ykeys)?>],
  	  labels: [<?php echo implode(",", $label)?>],
	  labelTop: true
  	});

  });
    //-->
</script>
            <?php unset($is_children);?>
          
                <?php } ?>  
     
                
                <?php } ?>
              
          <?php 
		 
		  } ?>
          <?php } ?>
  </div>
</div>
 
<script type="text/javascript"><!--
  function filter() {
	  url ='index.php?route=chart/morris' ;
  	var filter_round = $('select[name=\'filter_round\']').val();
  
    if (filter_round!='*') {
      url += '&filter_round=' + encodeURIComponent(filter_round);
    }
	
  	var filter_level_id = $('input[name=\'filter_level_id\']').val();
  
    if (filter_level_id!='*') {
      url += '&filter_level_id=' + encodeURIComponent(filter_level_id);
    }
    location = url;
  }

  //-->
</script>


<?php echo $footer;?>

