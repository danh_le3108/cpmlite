<?php echo $header;?>
<style>
#chart-group .btn .badge {
	display:none;
    color: #999;
    background-color: #fafafa;
}
#chart-group .btn.active .badge {
	display: inline-block;
}
.progress-bar-grey {
    background-color: #898e92;
}
.progress-bar {
    color: black;
}
</style>

<script type="text/javascript"><!--
(function() {
  var $, MyMorris;

  MyMorris = window.MyMorris = {};
  $ = jQuery;

  MyMorris = Object.create(Morris);

  MyMorris.Bar.prototype.defaults["labelTop"] = false;

  MyMorris.Bar.prototype.drawLabelTop = function(xPos, yPos, text) {
    var label;
    return label = this.raphael.text(xPos, yPos, text).attr('font-size', this.options.gridTextSize).attr('font-family', this.options.gridTextFamily).attr('font-weight', this.options.gridTextWeight).attr('fill', this.options.gridTextColor);
  };

  MyMorris.Bar.prototype.drawSeries = function() {
    var barWidth, bottom, groupWidth, idx, lastTop, left, leftPadding, numBars, row, sidx, size, spaceLeft, top, ypos, zeroPos;
    groupWidth = this.width / this.options.data.length;
    numBars = this.options.stacked ? 1 : this.options.ykeys.length;
    barWidth = (groupWidth * this.options.barSizeRatio - this.options.barGap * (numBars - 1)) / numBars;
    if (this.options.barSize) {
      barWidth = Math.min(barWidth, this.options.barSize);
    }
    spaceLeft = groupWidth - barWidth * numBars - this.options.barGap * (numBars - 1);
    leftPadding = spaceLeft / 2;
    zeroPos = this.ymin <= 0 && this.ymax >= 0 ? this.transY(0) : null;
    return this.bars = (function() {
      var _i, _len, _ref, _results;
      _ref = this.data;
      _results = [];
      for (idx = _i = 0, _len = _ref.length; _i < _len; idx = ++_i) {
        row = _ref[idx];
        lastTop = 0;
        _results.push((function() {
          var _j, _len1, _ref1, _results1;
          _ref1 = row._y;
          _results1 = [];
          for (sidx = _j = 0, _len1 = _ref1.length; _j < _len1; sidx = ++_j) {
            ypos = _ref1[sidx];
            if (ypos !== null) {
              if (zeroPos) {
                top = Math.min(ypos, zeroPos);
                bottom = Math.max(ypos, zeroPos);
              } else {
                top = ypos;
                bottom = this.bottom;
              }
              left = this.left + idx * groupWidth + leftPadding;
              if (!this.options.stacked) {
                left += sidx * (barWidth + this.options.barGap);
              }
              size = bottom - top;
              if (this.options.verticalGridCondition && this.options.verticalGridCondition(row.x)) {
                this.drawBar(this.left + idx * groupWidth, this.top, groupWidth, Math.abs(this.top - this.bottom), this.options.verticalGridColor, this.options.verticalGridOpacity, this.options.barRadius, row.y[sidx]);
              }
              if (this.options.stacked) {
                top -= lastTop;
              }
              this.drawBar(left, top, barWidth, size, this.colorFor(row, sidx, 'bar'), this.options.barOpacity, this.options.barRadius);
              _results1.push(lastTop += size);

              if (this.options.labelTop && !this.options.stacked) {
                label = this.drawLabelTop((left + (barWidth / 2)), top - 10, row.y[sidx]);
                textBox = label.getBBox();
                _results.push(textBox);
              }
            } else {
              _results1.push(null);
            }
          }
          return _results1;
        }).call(this));
      }
      return _results;
    }).call(this);
  };
}).call(this);
    //-->
</script>  
  
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
    </div>
  </div>
  <div class="container-fluid">
   <div class="box box-info" id="filter-area">
    <div class="panel-body">
     <div class="row">
        <div class="col-sm-3">
          <label class="control-label" for="filter_area_code"><?php echo $text_group; ?></label>
            <select name="filter_level_id" id="filter_level_id" class="form-control" onchange="filter();">
              <option value="*"><?php echo $text_all; ?></option>
              <?php foreach ($user_groups as $user_group) { ?>
              <option value="<?php echo $user_group['customer_level_id']; ?>" <?php if ($user_group['customer_level_id'] == $filter_level_id) { ?>selected="selected"<?php } ?>><?php echo $user_group['level_name']; ?></option>
              
                
              <?php } ?>
            </select>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label" for="filter_area_code"><?php echo $text_round; ?></label>
            <input type="hidden" name="filter_level_id" value="<?php echo $filter_level_id;?>"/>
            <select name="filter_round" id="filter_round" class="form-control" onchange="filter();">
              <option value="*"><?php echo $text_all; ?></option>
              <?php foreach ($rounds as $round) { ?>
              <option value="<?php echo $round['round_name']; ?>" <?php if ($round['round_name'] == $filter_round) { ?>selected="selected"<?php } ?>><?php echo $round['round_name']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-sm-6">
        
          <button type="button" id="button-clear" class="btn btn-primary pull-right"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button><!--//--> 
        </div>
      </div>
      
      <!--//row--> 
      
    </div>
    <!--panel-body -->
  </div>
  <!--box -->
  
          <?php
           foreach($user_groups as $user_group){?>
                <?php foreach ($project_users as $user) {?>
                <?php if($user['customer_level_id']==$user_group['customer_level_id']&&(is_array($customers_manager)&&in_array($user['customer_user_id'],$customers_manager)||$customers_manager=='all')){?>
				
                <?php if(($isNationwide==1&&is_null($filter_level_id)&&$user['customer_level_id']==$config_customer_top||$isLogged==$user['customer_user_id']&&is_null($filter_level_id))||($filter_level_id==$user['customer_level_id'])){?>
                <div class="row">
                <div class="col-sm-8"> 
             
   <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title"><?php echo $user['fullname'];?></h3>
        </div>
        <div class="box-body chart-responsive">
          <div class="chart" id="chart<?php echo $user['customer_user_id'];?>" style="height: 300px;"></div>
        </div>
        <!--//box-body -->
      </div>
      <!--//box -->
         
     			 </div><!--//col -->  
                 <div class="col-sm-4"> 
                 
                     
             <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title"> &nbsp;#<?php echo $user_group['customer_level_id']; ?> -  <?php echo $user_group['level_name']; ?> -  <?php echo $user['fullname'];?> </h3>
        </div>
        <div class="box-body">
          <table class="table">
            <tr>
              <td width="60"><?php echo $text_total;?></td>
              <td width="140">
                <div class="progress">
                  <div class="progress-bar " role="progressbar" aria-valuenow="100"
                    aria-valuemin="0" aria-valuemax="100" style="width:100%">
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $user['total'];?></td>
              <td class="text-right"><a href="<?php echo $user['url_total']; ?>" target="_blank"><?php echo $text_details;?></a></td>
               <?php if(1==1){ ?> 
              <td class="text-right"><a href="<?php echo $user['down_total']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td><?php } ?>
            </tr>
            <tr>
              <td width="60"><?php echo $text_rating_passed;?>:</td>
              <td>
                  <?php 
                  $success_percent = ($user['total']>0)?round(($user['success']/$user['total'])*100,2):0;
                   ?>
                <div class="progress">
                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $success_percent;?>" aria-valuemin="0" aria-valuemax="<?php echo $success_percent;?>" style="width:<?php echo $success_percent;?>%"><?php echo $success_percent;?>%
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $user['success'];?> </td>
              <td class="text-right"><a href="<?php echo $user['url_success']; ?>" target="_blank"><?php echo $text_details;?></a></td>
               <?php if(1==1){ ?>
              <td class="text-right"><a href="<?php echo $user['down_success']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td> <?php } ?>
            </tr>
            <tr>
              <td width="60"><?php echo $text_rating_not_passed;?></td>
              <td>
                <div class="progress">
                  <?php $unsuccess_percent = ($user['total']>0)?round(($user['unsuccess']/$user['total'])*100,2):0;?>
                  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo $unsuccess_percent;?>" aria-valuemin="0" aria-valuemax="<?php echo $unsuccess_percent;?>" style="width:<?php echo $unsuccess_percent;?>%"><?php echo $unsuccess_percent;?>%
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $user['unsuccess'];?> </td>
              <td class="text-right"><a href="<?php echo $user['url_unsuccess']; ?>" target="_blank"><?php echo $text_details;?></a></td>
               <?php if(1==1){ ?>
              <td class="text-right"><a href="<?php echo $user['down_unsuccess']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td> <?php } ?>
            </tr>
            <tr>
              <td width="60"><?php echo $text_rating_unsuccess;?></td>
              <td>
                <div class="progress">
                  <?php $fail_percent = ($user['total']>0)?round(($user['fail']/$user['total'])*100,2):0;?>
                  <div class="progress-bar progress-bar-grey" role="progressbar" aria-valuenow="<?php echo $fail_percent;?>" aria-valuemin="0" aria-valuemax="<?php echo $fail_percent;?>" style="width:<?php echo $fail_percent;?>%"><?php echo $fail_percent;?>%
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $user['fail'];?> </td>
              <td class="text-right"><a href="<?php echo $user['url_fail']; ?>" target="_blank"><?php echo $text_details;?></a></td>
               <?php if(1==1){ ?>
              <td class="text-right"><a href="<?php echo $user['down_fail']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td> <?php } ?>
            </tr>
          </table>
        </div>
        <!--//box-body -->
      </div>
      <!--//box -->  
      
      
     			 </div><!--//col -->  
     			 </div><!--//row -->  
      <?php $ndata = array();?>
      <?php $is_children = array();?>
      
       <?php if($user['customer_level_id']==$config_customer_top){?>
           <?php foreach ($project_users as $staff) {  ?>
                <?php if($staff['customer_level_id']==($user['customer_level_id']+1)){?>
      <?php
             $is_children[] = 1;
            $ndata[] = '{y:"'.$staff['fullname'].' ('.$staff['total'].')",'.implode(",", $staff['chart']).'}';
        ?>         
                <?php } ?>
                
          <?php } ?>
      <?php }else{ ?>
      
           <?php foreach ($project_users as $staff) {  ?>
                <?php if($staff['customer_parent_id']==$user['customer_user_id']){?>
      <?php
             $is_children[] = 1;
            $ndata[] = '{y:"'.$staff['fullname'].' ('.$staff['total'].')",'.implode(",", $staff['chart']).'}';
        ?>         
                <?php } ?>
                
          <?php } ?>
      
      <?php } ?>
      
       <?php if(!in_array(1,$is_children)){?>
            
    	<?php $ndata[] = '{y:"'.$user['fullname'].' ('.$user['total'].')",'.implode(",", $user['chart']).'}';?>
            
      <?php } ?>
      
<script type="text/javascript"><!--
  $(document).ready(function(){
  	//BAR CHART
  	var nbar = new Morris.Bar({
  	  element: 'chart<?php echo $user['customer_user_id'];?>',
  	  barSizeRatio:0.3,
  	  resize: true,
  	  data: [<?php echo implode(",",$ndata); ?>],
  	  barColors: [<?php echo implode(",", $colors)?>],
  	  xkey: 'y',
  	  ykeys: [<?php echo implode(",", $ykeys)?>],
  	  labels: [<?php echo implode(",", $label)?>],
  	  hideHover: 'auto',
	  labelTop: true
  	});

  });
    //-->
</script>
            <?php unset($is_children);?>
          
                <?php } ?>  
     
                
                <?php } ?>
              
          <?php } ?>
          <?php } ?>
  </div>
</div>
 
<script type="text/javascript"><!--
  function filter() {
	  url ='index.php?route=chart/highcharts' ;
	  
	
  	var filter_level_id = $('select[name=\'filter_level_id\']').val();
  
    if (filter_level_id!='*') {
      url += '&filter_level_id=' + encodeURIComponent(filter_level_id);
    }
	
  	var filter_round = $('select[name=\'filter_round\']').val();
  
    if (filter_round!='*') {
      url += '&filter_round=' + encodeURIComponent(filter_round);
    }
    location = url;
  }

  //-->
</script>


<?php echo $footer;?>

