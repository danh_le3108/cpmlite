<style>
#chart-group .btn .badge {
	display:none;
    color: #999;
    background-color: #fafafa;
}
#chart-group .btn.active .badge {
	display: inline-block;
}
.progress-bar-grey {
    background-color: #898e92;
}
</style>
<script type="text/javascript"><!--

    //-->
</script>  
  
  <div class="container-fluid">
  
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
      <div class="box box-info" id="filter-area">
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-12">
          <div class="form-group">
            <div class="clearfix">
              <div id="chart-group" class="btn-group btn-group-justified">
          <?php if($isNationwide==1){ ?>
          		<a href="<?php echo $href; ?>" class="btn btn-<?php echo ($filter_area_code==1)?'primary active':'primary'; ?>">Nationwide</a>
            <?php } ?>
                <?php foreach ($areas as $area) { ?>
                
                <?php if($isNationwide==1||($isLogged&&$sup_area==$area['area_code'])){ ?>
                <a href="<?php echo $area['href']; ?>" class="btn btn-<?php echo ($area['area_code'] == $filter_area_code)?'success active':'primary'; ?>"><?php echo $area['area_name']; ?>
                </a>
                <?php } ?>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--panel-body -->
  </div>
  <!--box -->
  
  <?php
   if((is_null($filter_area_code)||$filter_area_code==1)&&$isNationwide==1){ ?>
  
  <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title"> &nbsp; Nationwide - Overview</h3>
        </div>
        <div class="box-body">
          <table class="table">
            <tr>
              <td width="100">Đã thực hiện:</td>
              <td>
                <div class="progress">
                  <div class="progress-bar " role="progressbar" aria-valuenow="100"
                    aria-valuemin="0" aria-valuemax="100" style="width:100%">
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $nationwide['total'];?></td>
              <td class="text-right"><a href="<?php echo $nationwide['url_total']; ?>" target="_blank">Chi tiết</a></td>
               <?php if(1==1){ ?> 
              <td class="text-right"><a href="<?php echo $nationwide['down_total']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td><?php } ?>
            </tr>
            <tr>
              <td width="100"><?php echo $text_rating_passed;?>:</td>
              <td>
                <div class="progress">
                  <?php $success_percent = round(($nationwide['success']/$nationwide['total'])*100,2);?>
                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $success_percent;?>" aria-valuemin="0" aria-valuemax="<?php echo $success_percent;?>" style="width:<?php echo $success_percent;?>%"><?php echo $success_percent;?>%
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $nationwide['success'];?> </td>
              <td class="text-right"><a href="<?php echo $nationwide['url_success']; ?>" target="_blank">Chi tiết</a></td>
               <?php if(1==1){ ?>
              <td class="text-right"><a href="<?php echo $nationwide['down_success']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td> <?php } ?>
            </tr>
            <tr>
              <td width="100">Không đạt:</td>
              <td>
                <div class="progress">
                  <?php $unsuccess_percent = round(($nationwide['unsuccess']/$nationwide['total'])*100,2);?>
                  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo $unsuccess_percent;?>" aria-valuemin="0" aria-valuemax="<?php echo $unsuccess_percent;?>" style="width:<?php echo $unsuccess_percent;?>%"><?php echo $unsuccess_percent;?>%
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $nationwide['unsuccess'];?> </td>
              <td class="text-right"><a href="<?php echo $nationwide['url_unsuccess']; ?>" target="_blank">Chi tiết</a></td>
               <?php if(1==1){ ?>
              <td class="text-right"><a href="<?php echo $nationwide['down_unsuccess']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td> <?php } ?>
            </tr>
            <tr>
              <td width="100">KTC:</td>
              <td>
                <div class="progress">
                  <?php $fail_percent = round(($nationwide['fail']/$nationwide['total'])*100,2);?>
                  <div class="progress-bar progress-bar-grey" role="progressbar" aria-valuenow="<?php echo $fail_percent;?>" aria-valuemin="0" aria-valuemax="<?php echo $fail_percent;?>" style="width:<?php echo $fail_percent;?>%"><?php echo $fail_percent;?>%
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $nationwide['fail'];?> </td>
              <td class="text-right"><a href="<?php echo $nationwide['url_fail']; ?>" target="_blank">Chi tiết</a></td>
               <?php if(1==1){ ?>
              <td class="text-right"><a href="<?php echo $nationwide['down_fail']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td> <?php } ?>
            </tr>
          </table>
        </div>
        <!--//box-body -->
      </div>
      <!--//box -->
      
      
   <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title"> Nationwide - Overview</h3>
        </div>
        <div class="box-body chart-responsive">
         	<canvas id="bar_chart" height="150"></canvas>

        </div>
        <!--//box-body -->
      </div>
      <!--//box -->
      
      <?php
      $ndata = array();  
         foreach ($all_cats as $area_code =>$cats) { ?>
          <?php foreach ($cats as $cat) {  ?>
          <?php
    	$ndata[] = '{y:"'.$cat['area_code'].'",'.implode(",", $cat['chart']).'}';
    ?>
    <?php }  ?>
    <?php }  ?>

<script type="text/javascript"><!--
  $(document).ready(function(){
  	//CHART JS
	 chartData = {
            type: 'bar',
            data: {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [
				{
                    label: "My First dataset",
                    data: [65, 59, 80, 81, 56, 55, 40],
                    backgroundColor: 'rgba(0, 188, 212, 0.8)'
                },
				{
                        label: "My Second dataset",
                        data: [28, 48, 40, 19, 86, 27, 90],
                        backgroundColor: 'rgba(233, 30, 99, 0.8)'
                  }
				  ]
            },
            options: {
                responsive: true,
                legend: false,
				title: {
					display: true,
					text: 'Custom Chart Title'
				}
            }
        }
  new Chart(document.getElementById("bar_chart").getContext("2d"),chartData);

  });
    //-->
</script>     
                

      
  <?php } ?>   
  
  <?php if((!is_null($filter_area_code)||!is_null($sup_area))&&$filter_area_code!=1){ ?>       
        <?php foreach ($all_cats as $area_code =>$cats) { ?>
  <?php if($filter_area_code==$area_code||$sup_area==$area_code){ ?>    
                <?php foreach ($cats as $cat) {  ?>
            
    <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">CAT: <?php echo $area_code; ?> - <?php echo $cat['fullname']; ?></h3>
        </div>
        <div class="box-body">
          <?php
            ?>
          <table class="table">
            <tr>
              <td width="100">Đã thực hiện:</td>
              <td>
                <div class="progress">
                  <div class="progress-bar " role="progressbar" aria-valuenow="100"
                    aria-valuemin="0" aria-valuemax="100" style="width:100%">
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $cat['total'];?></td> 
              <td class="text-right"><a href="<?php echo $cat['url_total']; ?>" target="_blank">Chi tiết</a></td>
               <?php if(1==1){ ?> 
              <td class="text-right"><a href="<?php echo $cat['down_total']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td><?php } ?>
            </tr>
            <tr>
              <td width="100"><?php echo $text_rating_passed;?>:</td>
              <td>
                  <?php
                   $rs_percent = ((int)$cat['success']>0)?round(($cat['success']/$cat['total'])*100,2):0;
                   
                   ?>
                  
                <div class="progress">
                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $rs_percent;?>" aria-valuemin="0" aria-valuemax="<?php echo $rs_percent;?>" style="width:<?php echo $rs_percent;?>%"><?php echo $rs_percent;?>%
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $cat['success'];?></td>
              <td class="text-right"><a href="<?php echo $cat['url_success']; ?>" target="_blank">Chi tiết</a></td>
               <?php if(1==1){ ?> 
              <td class="text-right"><a href="<?php echo $cat['down_success']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td><?php } ?>
            </tr>
            <tr>
              <td width="100">Không đạt:</td>
              <td>
                <div class="progress">
                  <?php $ru_percent = ((int)$cat['unsuccess']>0)?round(($cat['unsuccess']/$cat['total'])*100,2):0;
                  ?>
                  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo $ru_percent;?>" aria-valuemin="0" aria-valuemax="<?php echo $ru_percent;?>" style="width:<?php echo $ru_percent;?>%"><?php echo $ru_percent;?>%
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $cat['unsuccess'];?></td>
              <td class="text-right"><a href="<?php echo $cat['url_unsuccess']; ?>" target="_blank">Chi tiết</a></td>
               <?php if(1==1){ ?>
              <td class="text-right"><a href="<?php echo $cat['down_unsuccess']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td> <?php } ?>
            </tr>
            <tr>
              <td width="100">KTC:</td>
              <td>
                <div class="progress">
                  <?php $f_percent = ((int)$cat['fail']>0)?round(($cat['fail']/$cat['total'])*100,2):0;
                  ?>
                  <div class="progress-bar progress-bar-grey" role="progressbar" aria-valuenow="<?php echo $f_percent;?>" aria-valuemin="0" aria-valuemax="<?php echo $f_percent;?>" style="width:<?php echo $f_percent;?>%"><?php echo $f_percent;?>%
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $cat['fail'];?></td>
              <td class="text-right"><a href="<?php echo $cat['url_fail']; ?>" target="_blank">Chi tiết</a></td>
               <?php if(1==1){ ?>
              <td class="text-right"><a href="<?php echo $cat['down_fail']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td> <?php } ?>
            </tr>
          </table>
        </div>
        <!--//box-body -->
      </div>
      <!--//box -->
      
    
    
           
    <div class="box">
      <div class="box-header">
        <h2 class="box-title"> Chart </h2>
      </div>
      <div class="panel-body">
        
        <div class="box-body chart-responsive">
          <div class="chart" id="cat_chart<?php echo $cat['customer_user_id'];?>" style="height: 300px;"></div>
        </div>
      </div>
      
    </div>
    
    
    <?php
    $rdata = array();
    ?>
  <?php foreach ($cat['sups'] as $sup) { ?>
               
  	<?php
    	$rdata[] = '{y:"'.$sup['fullname'].'",'.implode(",", $sup['chart']).'}';
    ?>
     <div class="row">
    <div class="col-sm-12">           
    <div class="box">
      <div class="box-header">
        <h2 class="box-title"> SUP - <?php echo $sup['fullname']; ?> </h2>
      </div>
      <div class="panel-body">
        <div class="box-body chart-responsive">
         <table class="table">
            <tr>
              <td width="100">Đã thực hiện:</td>
              <td>
                <div class="progress">
                  <div class="progress-bar " role="progressbar" aria-valuenow="100"
                    aria-valuemin="0" aria-valuemax="100" style="width:100%">
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $sup['total'];?></td> 
              <td class="text-right"><a href="<?php echo $sup['url_total']; ?>" target="_blank">Chi tiết</a></td>
               <?php if(1==1){ ?> 
              <td class="text-right"><a href="<?php echo $sup['down_total']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td><?php } ?>
            </tr>
            <tr>
              <td width="100"><?php echo $text_rating_passed;?>:</td>
              <td>
                  <?php
                   $rs_percent = ((int)$sup['success']>0)?round(($sup['success']/$sup['total'])*100,2):0;
                   
                   ?>
                  
                <div class="progress">
                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $rs_percent;?>" aria-valuemin="0" aria-valuemax="<?php echo $rs_percent;?>" style="width:<?php echo $rs_percent;?>%"><?php echo $rs_percent;?>%
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $sup['success'];?></td>
              <td class="text-right"><a href="<?php echo $sup['url_success']; ?>" target="_blank">Chi tiết</a></td>
               <?php if(1==1){ ?> 
              <td class="text-right"><a href="<?php echo $sup['down_success']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td><?php } ?>
            </tr>
            <tr>
              <td width="100">Không đạt:</td>
              <td>
                <div class="progress">
                  <?php $ru_percent = ((int)$sup['unsuccess']>0)?round(($sup['unsuccess']/$sup['total'])*100,2):0;
                  ?>
                  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo $ru_percent;?>" aria-valuemin="0" aria-valuemax="<?php echo $ru_percent;?>" style="width:<?php echo $ru_percent;?>%"><?php echo $ru_percent;?>%
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $sup['unsuccess'];?></td>
              <td class="text-right"><a href="<?php echo $sup['url_unsuccess']; ?>" target="_blank">Chi tiết</a></td>
               <?php if(1==1){ ?>
              <td class="text-right"><a href="<?php echo $sup['down_unsuccess']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td> <?php } ?>
            </tr>
            <tr>
              <td width="100">KTC:</td>
              <td>
                <div class="progress">
                  <?php $f_percent = ((int)$sup['fail']>0)?round(($sup['fail']/$sup['total'])*100,2):0;
                  ?>
                  <div class="progress-bar progress-bar-grey" role="progressbar" aria-valuenow="<?php echo $f_percent;?>" aria-valuemin="0" aria-valuemax="<?php echo $f_percent;?>" style="width:<?php echo $f_percent;?>%"><?php echo $f_percent;?>%
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $sup['fail'];?></td>
              <td class="text-right"><a href="<?php echo $sup['url_fail']; ?>" target="_blank">Chi tiết</a></td>
               <?php if(1==1){ ?>
              <td class="text-right"><a href="<?php echo $sup['down_fail']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td> <?php } ?>
            </tr>
          </table>
        
        </div>
      </div>
      
    </div>
    </div><!--//col8--> 
    
    </div><!--//row--> 

                <?php } ?><!--//sups--> 
    

                <?php } ?>

<script type="text/javascript"><!--
  $(document).ready(function(){
  	//CHART JS
  

  });
    //-->
</script>     
                

     
    
                <?php } ?>



                <?php } ?>
                
                <?php } ?>
                
                
            <!--///////////////////////////////////////////////////////////////////////////-->     
                
    <?php if(!empty($sup_chart)){?>
            <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title"> &nbsp; SUP - Overview</h3>
        </div>
        <div class="box-body">
          <table class="table">
            <tr>
              <td width="100">Đã thực hiện:</td>
              <td>
                <div class="progress">
                  <div class="progress-bar " role="progressbar" aria-valuenow="100"
                    aria-valuemin="0" aria-valuemax="100" style="width:100%">
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $sup_chart['total'];?></td>
              <td class="text-right"><a href="<?php echo $sup_chart['url_total']; ?>" target="_blank">Chi tiết</a></td>
               <?php if(1==1){ ?> 
              <td class="text-right"><a href="<?php echo $sup_chart['down_total']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td><?php } ?>
            </tr>
            <tr>
              <td width="100"><?php echo $text_rating_passed;?>:</td>
              <td>
                <div class="progress">
                  <?php $success_percent = round(($sup_chart['success']/$sup_chart['total'])*100,2);?>
                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $success_percent;?>" aria-valuemin="0" aria-valuemax="<?php echo $success_percent;?>" style="width:<?php echo $success_percent;?>%"><?php echo $success_percent;?>%
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $sup_chart['success'];?> </td>
              <td class="text-right"><a href="<?php echo $sup_chart['url_success']; ?>" target="_blank">Chi tiết</a></td>
               <?php if(1==1){ ?>
              <td class="text-right"><a href="<?php echo $sup_chart['down_success']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td> <?php } ?>
            </tr>
            <tr>
              <td width="100">Không đạt:</td>
              <td>
                <div class="progress">
                  <?php $unsuccess_percent = round(($sup_chart['unsuccess']/$sup_chart['total'])*100,2);?>
                  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo $unsuccess_percent;?>" aria-valuemin="0" aria-valuemax="<?php echo $unsuccess_percent;?>" style="width:<?php echo $unsuccess_percent;?>%"><?php echo $unsuccess_percent;?>%
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $sup_chart['unsuccess'];?> </td>
              <td class="text-right"><a href="<?php echo $sup_chart['url_unsuccess']; ?>" target="_blank">Chi tiết</a></td>
               <?php if(1==1){ ?>
              <td class="text-right"><a href="<?php echo $sup_chart['down_unsuccess']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td> <?php } ?>
            </tr>
            <tr>
              <td width="100">KTC:</td>
              <td>
                <div class="progress">
                  <?php $fail_percent = round(($sup_chart['fail']/$sup_chart['total'])*100,2);?>
                  <div class="progress-bar progress-bar-grey" role="progressbar" aria-valuenow="<?php echo $fail_percent;?>" aria-valuemin="0" aria-valuemax="<?php echo $fail_percent;?>" style="width:<?php echo $fail_percent;?>%"><?php echo $fail_percent;?>%
                  </div>
                </div>
              </td>
              <td class="text-right"><?php echo $sup_chart['fail'];?> </td>
              <td class="text-right"><a href="<?php echo $sup_chart['url_fail']; ?>" target="_blank">Chi tiết</a></td>
               <?php if(1==1){ ?>
              <td class="text-right"><a href="<?php echo $sup_chart['down_fail']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Excel</a></td> <?php } ?>
            </tr>
          </table>
        </div>
        <!--//box-body -->
      </div>
      <!--//box -->
      
      
   <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title"> SUP - Overview</h3>
        </div>
        <div class="box-body chart-responsive">
          <div class="chart" id="sup-chart" style="height: 300px;"></div>
        </div>
        <!--//box-body -->
      </div>
      <!--//box -->
      
      <?php
    	$xdata[] = '{y:"'.$sup_chart['fullname'].'",'.implode(",", $sup_chart['chart']).'}';
     ?>
<script type="text/javascript"><!--
  $(document).ready(function(){
  	//CHART JS
  

  });
    //-->
</script>     
                
     <?php } ?>
              
    
  </div>

