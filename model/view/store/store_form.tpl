<div class="page-header">
  <div class="container-fluid">
    <div class="pull-right">
      <?php if(($hasEdit && !empty($store)) || ($hasAdd && empty($store))) { ?>
      <button type="submit" form="form-store" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
      <?php } ?>
      <a href="index.php?route=store/store" data-toggle="tooltip" title="<?php echo $button_back; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
    </div>
  </div>
</div>

<div class="container-fluid" style="margin-top: 20px">
  <?php if (!empty($success)) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if (!empty($error_warning)) { ?>
    <div class="alert alert-danger"><i class="fa fa-check-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
  <?php } ?>
</div>

<div class="container-fluid">
  <form method="post" action="" id="form-store" class="form-horizontal">
    <div class="row">
      <!--THONG TIN -->
      <div class="col-md-6">
        <div class="box box-info">
          <div class="box-header">
            <h3 class="box-title"><?php echo $text_store_info;?></h3>
          </div>
          <div class="panel-body">
            <?php if(!empty($store)) {
                if(is_file(DIR_MEDIA.$store['store_image'])) {
                  $popup = HTTP_SERVER.'media/'.$store['store_image'];
                  $thumb = $model_tool_image->resize($store['store_image'],100,100);
                } else {
                  $popup = '';
                  $thumb = $model_tool_image->resize('store.jpg',100,100);
                }
              ?>
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label" for="input-image"><?php echo $text_image; ?>
              </label>
              <div class="col-sm-9 img-popup">
                <a href="<?=$popup?>" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?=$thumb?>"/></a>
              </div>
            </div>
            <?php } ?>
             <!--STORE CODE -->
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Mã cửa hàng</label>
              <div class="col-sm-9">
                <input type="text" name="store_code" value="<?=!empty($store['store_code']) ? $store['store_code'] : ''?>" class="form-control" />
                <?php if (!empty($error['error_store_code'])) { ?>
                <div class="text-danger"><?php echo $error['error_store_code']; ?></div>
                <?php } ?>
              </div>
            </div>
            <!--STORE NAME -->
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Tên cửa hàng</label>
              <div class="col-sm-9">
                <input type="text" name="store_name" value="<?=!empty($store['store_name']) ? $store['store_name'] : ''?>" class="form-control" />
                <?php if (!empty($error['error_store_name'])) { ?>
                <div class="text-danger"><?php echo $error['error_store_name']; ?></div>
                <?php } ?>
              </div>
            </div>  
            <!--STORE OWNER -->
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Người kinh doanh</label>
              <div class="col-sm-9">
                <input type="text" name="store_owner" value="<?=!empty($store['store_owner']) ? $store['store_owner'] : ''?>" class="form-control" />
              </div>
            </div>
            <!--STORE PHONE -->
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Số điện thoại</label>
              <div class="col-sm-9">
                <input type="text" name="store_phone" value="<?=!empty($store['store_phone']) ? $store['store_phone'] : ''?>" class="form-control" />
              </div>
            </div>
            <!--CHANEL -->
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Kênh</label>
              <div class="col-sm-9">
                <select class="form-control" name="chanel">
                    <option value=""><?php echo $text_select;?></option>
                    <?php foreach($store_chanels as $c) {
                      $selected = !empty($c['id']) && $store['chanel'] == $c['id'] ? 'selected' : '';
                    ?>
                    <option <?=$selected?> value="<?=$c['id']?>"><?=$c['name']?></option>
                    <?php } ?>
                </select>
              </div>
            </div> 
            <!--STORE TYPE -->
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Loại cửa hàng</label>
              <div class="col-sm-9">
                <select class="form-control" name="store_type_id">
                    <option value=""><?php echo $text_select;?></option>
                    <?php foreach($store_types as $c) {
                      $selected = !empty($c['store_type_id']) && $store['store_type_id'] == $c['store_type_id'] ? 'selected' : '';
                    ?>
                    <option <?=$selected?> value="<?=$c['store_type_id']?>"><?=$c['type_name']?></option>
                    <?php } ?>
                </select>
              </div>
            </div> 
            <!--STORE ADDRESS -->
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Địa chỉ</label>
              <div class="col-sm-9">
                <input type="text" name="store_address" value="<?=!empty($store['store_address']) ? $store['store_address'] : ''?>" class="form-control" />
              </div>
            </div>
            <!--STORE PLACE -->
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Đường/ Ấp</label>
              <div class="col-sm-9">
                <input type="text" name="store_place" value="<?=!empty($store['store_place']) ? $store['store_place'] : ''?>" class="form-control" />
              </div>
            </div>
            <!--STORE WARD -->
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Phường xã</label>
              <div class="col-sm-9">
                <input type="text" name="store_ward" value="<?=!empty($store['store_ward']) ? $store['store_ward'] : ''?>" class="form-control" />
              </div>
            </div>
            <!--STORE DISTRICT -->
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Quận huyện</label>
              <div class="col-sm-9">
                <input type="text" name="store_district" value="<?=!empty($store['store_district']) ? $store['store_district'] : ''?>" class="form-control" />
              </div>
            </div>
            <!--STORE PROVINCE -->
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Tỉnh thành</label>
              <div class="col-sm-9">
                <select class="form-control chosen" name="province_id" id="store_province">
                     <option value=""><?php echo $text_select;?></option>
                    <?php foreach($provinces as $p) {
                      $selected = !empty($store['province_id']) && $store['province_id'] == $p['province_id'] ? 'selected' : '';
                    ?>
                    <option <?=$selected?> value="<?=$p['province_id']?>"><?=$p['name']?></option>
                    <?php } ?>
                </select>
              </div>
              <input type="hidden" name="store_province" value="<?=!empty($store['store_province']) ? $store['store_province'] : ''?>" />
            </div> 
            <!--STORE REGION -->
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Khu vực</label>
              <div class="col-sm-9">
                <select class="form-control" name="region_code">
                     <option value=""><?php echo $text_select;?></option>
                    <?php foreach($regions as $g) {
                      $selected = !empty($store['region_code']) && $store['region_code'] == $g['region_code'] ? 'selected' : '';
                    ?>
                    <option <?=$selected?> value="<?=$g['region_code']?>"><?=$g['region_name']?></option>
                    <?php } ?>
                </select>
              </div>
            </div> 

            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Ter</label>
              <div class="col-sm-9">
                <input type="text" name="ter" value="<?=!empty($store['ter']) ? $store['ter'] : ''?>" class="form-control" />
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">ASM</label>
              <div class="col-sm-9">
                <input type="text" name="asm" value="<?=!empty($store['asm']) ? $store['asm'] : ''?>" class="form-control" />
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">SS</label>
              <div class="col-sm-9">
                <input type="text" name="ss" value="<?=!empty($store['ss']) ? $store['ss'] : ''?>" class="form-control" />
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Tên DCR</label>
              <div class="col-sm-9">
                <input type="text" name="dcr" value="<?=!empty($store['dcr']) ? $store['dcr'] : ''?>" class="form-control" />
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Mã NPP</label>
              <div class="col-sm-9">
                <input type="text" name="distributor_code" value="<?=!empty($store['distributor_code']) ? $store['distributor_code'] : ''?>" class="form-control" />
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">NPP</label>
              <div class="col-sm-9">
                <input type="text" name="distributor" value="<?=!empty($store['distributor']) ? $store['distributor'] : ''?>" class="form-control" />
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Route</label>
              <div class="col-sm-9">
                <input type="text" name="route" value="<?=!empty($store['route']) ? $store['route'] : ''?>" class="form-control" />
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Mã QR</label>
              <div class="col-sm-9">
                <input type="text" name="qc_code" value="<?=!empty($store['qc_code']) ? $store['qc_code'] : ''?>" class="form-control" />
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Dung Tích</label>
              <div class="col-sm-9">
                <input type="text" name="dung_tich" value="<?=!empty($store['dung_tich']) ? $store['dung_tich'] : ''?>" class="form-control" />
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Đăng ký CLUB</label>
              <div class="col-sm-9">
                <input type="text" name="club" value="<?=!empty($store['club']) ? $store['club'] : ''?>" class="form-control" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <!--BẢN ĐỒ-->
        <div class="box box-info">
          <div class="box-header">
            <h3 class="box-title"> CCTB</h3>
          </div>
          <div class="panel-body">
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Tủ lạnh</label>
              <div class="col-sm-9">
                <input type="text" name="fridge" value="<?=!empty($store['fridge']) ? $store['fridge'] : ''?>" class="form-control" />
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Kệ CVS</label>
              <div class="col-sm-9">
                <input type="text" name="cvs_shelf" value="<?=!empty($store['cvs_shelf']) ? $store['cvs_shelf'] : ''?>" class="form-control" />
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Kệ CSD</label>
              <div class="col-sm-9">
                <input type="text" name="csd_shelf" value="<?=!empty($store['csd_shelf']) ? $store['csd_shelf'] : ''?>" class="form-control" />
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Kệ AQF</label>
              <div class="col-sm-9">
                <input type="text" name="aqf_shelf" value="<?=!empty($store['aqf_shelf']) ? $store['aqf_shelf'] : ''?>" class="form-control" />
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Kệ Tra</label>
              <div class="col-sm-9">
                <input type="text" name="tea_shelf" value="<?=!empty($store['tea_shelf']) ? $store['tea_shelf'] : ''?>" class="form-control" />
              </div>
            </div>
          </div>
        </div>
        <div class="box box-info">
          <div class="box-header">
            <h3 class="box-title"> <?php echo $text_map; ?></h3>
          </div>
          <div class="panel-body">
            <div class="row" style="margin-bottom: 20px">
              <div class="col-sm-6">
                <label class="control-label" for="store_latitude"><?php echo $text_latitude; ?></label>
                <div class="clearfix">
                  <input type="text" name="store_latitude" value="<?=!empty($store['store_latitude']) ? $store['store_latitude'] : ''?>" placeholder="<?php echo $text_latitude; ?>" id="latitude" class="form-control" />
                </div>
              </div>
              <div class="col-sm-6">
                <label class="control-label" for="store_longitude"><?php echo $text_longitude; ?></label>
                <div class="clearfix">
                  <input type="text" name="store_longitude" value="<?=!empty($store['store_longitude']) ? $store['store_longitude'] : ''?>" placeholder="<?php echo $text_longitude; ?>" id="longitude" class="form-control" />
                </div>
              </div>
            </div>
            <div id="mapBox" style="height: 400px;"></div>
          </div><!--panel-body --> 
        </div><!--box -->
      </div>
  </form>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('.img-popup').magnificPopup({
      type:'image',
      delegate: 'a.img-thumbnail',
      gallery: {
        enabled:true
      }
    });
  });

  var map, marker;
  var pos = [];
  pos = [$("#latitude").val(),$("#longitude").val()];
  initMap();
  map.setZoom(15);
  dragableListener();

  $('#store_province,#customer_username').change(function(){
    var text = $(this).find('option:selected').text();
    console.log(text);
    $('input[name='+ $(this).attr('id') +']').val(text);
  })
 
</script>