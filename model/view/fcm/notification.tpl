<?php echo $header;?>
<div class="container-fluid">
  <div class="card" id="filter-area">
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-4 col-md-4">
          <label for="input-filter-global">Tìm</label>
          <div class="input-group">
            <input type="text" class="form-control" name="filter_global" value="<?php echo $filter_global; ?>" placeholder="Tìm tin nhắn của nhân viên...">
            <span class="input-group-btn">
              <button id="button-clear" class="btn btn-danger" type="button" data-toggle="tooltip" title="Xóa bộ lọc">x</button>
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="alert alert-success"><i class="fa fa-info-circle"></i> Chỉ hiển thị 50 tin nhắn mới nhất</div>
  <div class="card">
    <div class="panel-body form-horizontal">
      <fieldset id="send_to_user">
        <legend>Gửi thông báo cho nhân viên</legend>
        <div class="row">
          <div class="col-sm-8 col-md-8">
            <div class="form-group required">
              <div class="col-sm-12 col-md-12">
                <select class="form-control chosen">
                  <option value=""><?php echo $text_select; ?></option>
                  <?php foreach ($users_google_id as $user) { ?>
                  <option value="<?php echo $user['user_id']; ?>"><?php echo $user['username']; ?> - <?php echo $user['fullname']; ?> -  (<?php echo $user['email']; ?>)</option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-12 col-md-12" for="">Tiêu đề</label>
              <div class="col-sm-12 col-md-12">
                <input type="text" class="form-control title" placeholder="Tiêu đề" />
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-12 col-md-12" for="">Nội dung</label>
              <div class="col-sm-12 col-md-12">
                <textarea id="send_to_user_editor" class="form-control message" placeholder="Nội dung" rows="5"></textarea>
              </div>
            </div>
            <?php if($has_add){ ?>
            <div class="form-group">
              <div class="col-sm-12 col-md-12">
                <button type="button" class="btn btn-primary" onclick="sendMessage('send_to_user');">Gửi <i class="fa fa-paper-plane-o"></i></button>
              </div>
            </div>
            <?php } ?>
          </div>
          <div id="history-to-user" class="col-sm-4 col-md-4"></div>
        </div>
      </fieldset>
      <fieldset id="send_to_multiple">
        <legend>Gửi thông báo cho nhiều nhân viên</legend>
        <div class="row">
          <div class="col-sm-8 col-md-8">
            <div class="form-group required">
              <div class="col-sm-12 col-md-12">
                <select multiple class="form-control">
                  <option value=""><?php echo $text_select; ?></option>
                  <?php foreach ($users_google_id as $key => $user) { ?>
                  <option value="<?php echo $user['user_id']; ?>"><?php echo $user['username']; ?> - <?php echo $user['fullname']; ?> - (<?php echo $user['email']; ?>)</option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-12 col-md-12" for="">Tiêu đề</label>
              <div class="col-sm-12 col-md-12">
                <input type="text" class="form-control title" placeholder="Tiêu đề" />
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-12 col-md-12" for="">Nội dung</label>
              <div class="col-sm-12 col-md-12">
                <textarea id="send_to_multiple_editor" class="form-control message" placeholder="Nội dung" rows="5"></textarea>
              </div>
            </div>
            <?php if($has_add){ ?>
            <div class="form-group">
              <div class="col-sm-12 col-md-12">
                <button type="button" class="btn btn-primary" onclick="sendMessage('send_to_multiple');">Gửi <i class="fa fa-paper-plane-o"></i></button>
              </div>
            </div>
            <?php } ?>
          </div>
          <div id="history-to-multiple" class="col-sm-4 col-md-4"></div>
        </div>
      </fieldset>
      <fieldset id="send_to_all">
        <legend>Gửi tin cho tất cả nhân viên sử dụng app</legend>
        <div class="row">
          <div class="col-sm-8 col-md-8">
            <div class="form-group required">
              <label class="col-sm-12 col-md-12" for="">Tiêu đề</label>
              <div class="col-sm-12 col-md-12">
                <input type="text" class="form-control title" placeholder="Tiêu đề" />
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-12 col-md-12" for="">Nội dung</label>
              <div class="col-sm-12 col-md-12">
                <textarea id="send_to_all_editor" class="form-control message" placeholder="Nội dung" rows="5"></textarea>
              </div>
            </div>
            <?php if($has_add){ ?>
            <div class="form-group">
              <div class="col-sm-12 col-md-12">
                <button type="button" class="btn btn-primary" onclick="sendMessage('send_to_all');">Gửi <i class="fa fa-paper-plane-o"></i></button>
              </div>
            </div>
            <?php } ?>
          </div>
          <div id="history-to-all" class="col-sm-4 col-md-4"></div>
        </div>
      </fieldset>
    </div>
  </div>
</div>
<script type="text/javascript"><!--

var user_id = '<?php echo $logged_id ?>';
function loadHistory(){
  var t = new Date().getTime();
  $('#history-to-all').load('<?php echo $href_history_to_all; ?>&time='+t);
  $('#history-to-multiple').load('<?php echo $href_history_to_multiple; ?>&time='+t);
  $('#history-to-user').load('<?php echo $href_history_to_user; ?>&time='+t);
}

function sendMessage(type){
  $('.text-danger').remove();

  var title = $('#'+type+' input.title').val();

  if (title === null || title.trim().length === 0) {
      $('#'+type+' input.title').after('<div class="text-danger"><i class="fa fa-info-circle"></i> Vui lòng nhập tiêu đề tin nhắn!</div>');
      return;
  }

  // var msg = $('textarea#'+type+'_editor').val();
  switch (type) {
    case 'send_to_user':
     // var msg = CKEDITOR.instances.send_to_user_editor.getData();
      var msg = $('#send_to_user_editor').val();
      break; 
    case 'send_to_multiple':
      var msg = CKEDITOR.instances.send_to_multiple_editor.getData();
      break;
    case 'send_to_all':
      var msg = CKEDITOR.instances.send_to_all_editor.getData();
      break;
    default:
      break;

  }

  if (msg === null || msg.trim().length === 0) {
      $('#'+type+' #'+type+'_editor').after('<div class="text-danger"><i class="fa fa-info-circle"></i> Vui lòng nhập nội dung tin nhắn!</div>');
      return;
  }
  var to = $('#'+type+' select').val();

  if ((type == 'send_to_user' || type == 'send_to_multiple') && to === null) {
      $('#'+type+' select').after('<div class="text-danger"><i class="fa fa-info-circle"></i> Vui lòng chọn nhân viên!</div>');
      return;
  }

  if(type == 'send_to_multiple'){
    var selMulti = $.map($("#send_to_multiple select option:selected"), function (el, i) {
        return $(el).val();
    });
    to = selMulti.join(",");
  }

  $.ajax({
    url: '<?php echo $url; ?>/'+type,
    type: 'POST',
    dataType: 'json',
    data: 'user_id='+user_id+'&title='+title+'&message='+msg+'&to_user='+to,
    beforeSend: function() {
      $('#'+type+' .alert').remove();
      $('#'+type+' .text-danger').remove();
      $('#'+type+' button').button('loading');
    },
    complete: function() {
      $('#'+type+' button').button('reset');
    },
    success: function(json) {
      //$('textarea#'+type+'_editor').val('');
     // $('#'+type+' input.title').val('');

      if(type == 'send_to_all'){
        if(json['message_id'] > 0){
          $('#'+type+' button').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> Gửi tin nhắn thành công</div>');
          loadHistory();
        } else {
          $('#'+type+' button').before('<div class="alert alert-danger"><i class="fa fa-close-circle"></i> Gửi tin nhắn thất bại</div>');
        }
      } else {
        if(json['success']==1){
          $('#'+type+' button').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> Gửi tin nhắn thành công</div>');
          loadHistory();
        } else if(json['failure']==1){
          $('#'+type+' button').before('<div class="alert alert-danger"><i class="fa fa-close-circle"></i> Gửi tin nhắn thất bại</div>');
        }
      }

    }
  });
}

function filter(){
  url = 'index.php?route=fcm/fcm';
  var filter_global = $('input[name=\'filter_global\']').val();
  if (filter_global) {
    url += '&filter_global=' + encodeURIComponent(filter_global);
  }
  location = url;
}

$(document).ready(function () {
  loadHistory();

  $('input[name=\'filter_global\']').autocomplete({
    'source': function(request, response) {
    if(request.length>0){
      $.ajax({
        url: 'index.php?route=fcm/fcm/autocomplete&filter_global=' +  encodeURIComponent(request),
        dataType: 'json',
        success: function(json) {
          response($.map(json, function(item) {
            return {
              label: item['username'] + ' - ' +item['fullname'],
              value: item['user_id'],
              username: item['username']
            }
          }));
        }
      });
    }
    },
    'select': function(item) {
      $('input[name=\'filter_global\']').val(item['username']);
      filter();
    }
  });


  $('#filter-area input').on('keydown', function(e) {
      if (e.keyCode == 13) {
        filter();
      }
  });
});
$(document).ready(function () {
 // CKEDITOR.replace( 'send_to_user_editor' );
  CKEDITOR.replace( 'send_to_multiple_editor' );
  CKEDITOR.replace( 'send_to_all_editor' );
});
//--></script>
<?php echo $footer;?>
