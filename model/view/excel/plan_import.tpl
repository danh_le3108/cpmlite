<div class="form-group clearfix" style="margin-top:20px;">
   
      <form action="<?php echo $form;?>" enctype="multipart/form-data" method="post">
		<?php if($has_import) { ?>
	  <label class="control-label col-sm-1" for="input-import"><?php echo $text_import; ?></label>
	  <div class="col-sm-6">
	    <div class="input-group">
	      <div class="input-group-btn">
	    <a href="<?php echo $template_url; ?>" class="btn btn-success"><i class="fa fa-download"></i> Tải mẫu excel</a>
	    </div>
        
	      <input type="file" id="file_import" name="file_import" class="form-control">
	      <div class="input-group-btn">
	        <button class="btn btn-default" type="submit" data-loading-text="<?php echo $text_importing; ?>"><?php echo $text_import; ?></button>
	      </div>
        
	    </div>
         </div>
        <div class="col-sm-3">
    	<a onclick="loadImport('<?php echo $template_id;?>');" class="btn btn-default pull-right"><i class="fa fa-clock-o"></i> <?php echo $text_import_history; ?></a>
	  </div>
	  <?php } ?>
	</form>
	  <?php if(!empty($history_id)){?>
		  <script type="text/javascript"><!--
           loadImport('<?php echo $template_id;?>');
            //-->
          </script>
	 <?php } ?>
</div>

<?php if($has_export_detail || $has_export || $has_report_qc) { ?>
<div class="menu-goo">
  <input type="checkbox" href="#" class="menu-goo-open" name="menu-goo-open" id="menu-open"/>
  <label class="menu-goo-open-button" for="menu-open">
    <span class="fa fa-download"></span>
  </label>
  <?php if($has_export) { ?>
  <!-- <a href="index.php?route=excel/plan_import/export/<?=$url?>" class="menu-goo-item btn-primary" data-toggle="tooltip" title="Download"><i class="fa fa-download"></i></a> -->
  <?php } ?>
  <?php if($has_export_detail) { ?>
  <a href="index.php?route=excel/plan_export/display_report<?=$url?>" class="menu-goo-item btn-danger" data-toggle="tooltip" title="Báo cáo trưng bày"><i class="fa fa-bar-chart"></i></a>
  <a href="index.php?route=excel/plan_export/survey_report<?=$url?>" class="menu-goo-item btn-success" data-toggle="tooltip" title="Báo cáo khảo sát & POSM"><i class="fa fa-users"></i></a>
  <a href="index.php?route=excel/plan_export/sumary_report<?=$url?>" class="menu-goo-item btn-warning" data-toggle="tooltip" title="Báo cáo tổng kết"><i class="fa fa-file-excel-o"></i></a>
  <?php } ?>
 <!--  <?php if($has_report_qc) { ?>
  <a href="index.php?route=excel/plan_report/qc_report<?=$url?>" class="menu-goo-item btn-warning" data-toggle="tooltip" title="Kết quả QC"><i class="fa fa-file-excel-o"></i></a>
  <?php } ?> -->
</div>
<?php } ?>

<script type="text/javascript"><!--
	function export_filter(otp_value){
		  org_href = $('#export_download').attr('data-href');
		  if(otp_value!='*'){
		    org_href += '&filter_round=' + encodeURIComponent(otp_value);
			$('#export_download').attr('href',org_href).show();
		  }else{
			 $('#export_download').hide(); 
		  }
	}
    function exportAlert(url){
		$('#export_download').attr('href','');
		$('#export_download').attr('data-href',url);
		$('#export_download').hide();
        $('#export_round option:selected').removeAttr('selected');
        $('#export_round option').first().attr('selected', 'selected');
      	$('#modal-export').modal('show');
    }  //-->
</script>
