
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-template').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-template">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td style="width: 1px;" class="text-center">#</td>
                  <td class="text-left"><?php if ($sort == 'template_name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                    <?php } ?></td>
                  <td class="text-right"></td>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
                <?php if ($templates) { ?>
                <?php foreach ($templates as $template) { ?>
        <tbody id="tr_place<?php echo $template['template_id']; ?>">
                <tr class="tr_info">
                  <td class="text-center">
 <input type="checkbox" name="selected[]" value="<?php echo $template['template_id']; ?>" 
 <?php if (in_array($template['template_id'], $selected)) { ?>checked="checked" <?php } ?>/>
                   </td>
                  <td class="text-left"><?php echo $template['template_id']; ?></td>
                  <td class="text-left"><?php echo $template['template_name']; ?></td>
                  <td class="text-left">
                  
                  
                  <?php if($template['total_export']>0){ ?>
                    <a onclick="loadExport('<?php echo $template['template_id']; ?>',0);" class="btn btn-primary"><i class="fa fa-download"></i> <?php echo $text_export_history; ?></a>
                    <?php } ?>
                  <?php if($template['total_import']>0){ ?>
                    <a onclick="loadImportHistory('<?php echo $template['template_id']; ?>');" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo $text_import_history; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-right">

                 <?php foreach ($template['action'] as $action) { ?>
                  <a href="<?php echo $action['href']; ?>" data-toggle="tooltip" title="<?php echo $action['text']; ?>" class="btn btn-<?php echo $action['btn']; ?>"><i class="fa <?php echo $action['icon']; ?>"></i></a>
                 <?php } ?>
                  </td>
                </tr>
              </tbody>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="3"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
            </table>
          </div>
        </form><div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript"><!--
  function loadExport(template_id){
    $('tr.tr_project').remove();
    $('#tr_place'+template_id +' tr.tr_info').after('<tr class="tr_project"><td colspan="11"></td></tr>');
    $('#tr_place'+template_id +' tr.tr_project td').load('index.php?route=excel/template/export_history&template_id='+template_id);
  }
  function loadImportHistory(template_id){
    $('tr.tr_project').remove();
    $('#tr_place'+template_id +' tr.tr_info').after('<tr class="tr_project"><td colspan="11"></td></tr>');
    $('#tr_place'+template_id +' tr.tr_project td').load('index.php?route=excel/template/import_history&template_id='+template_id);
  }
  
	$(document).on('click', '.tr_project .pagination-sm a', function(event) {
		event.preventDefault();
		$(this).parents('td').load(this.href);
	});

  //-->
</script>