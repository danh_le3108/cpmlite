
<div class="container-fluid">

<div class="message" id="message"></div>
<div class="box">
    <div class="box-header">
      <h2 class="box-title">Error Log</h2>
      
        <div class="pull-right">
          <button type="button" onClick="clearLogs();" class="btn btn-danger"><i class="fa fa-eraser"></i> Clear All</button>
          <button type="button" onClick="clearContains();" class="btn btn-success"><i class="fa fa-eraser"></i> Clear by ...</button>
          </div>
          
    </div>
    <div class="container-fluid" id="filter-area">
  <div class="box box-info">
    <div class="panel-body">
      <div class="row">
          <div class="col-sm-12">
            <button type="button" id="button-clear" class="btn btn-primary btn-sm pull-right" autocomplete="off"><i class="fa fa-eraser"></i><?=$button_clear?></button>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label class="control-label" for="input-log_type">Error type</label>
              <select name="filter_log_type" id="input-log_type" class="form-control filter" onchange="javascript:filter()">
                <option value="*"><?php echo $text_select; ?></option>
                <option value="0" <?=isset($filter_log_type) && $filter_log_type == '0' ? 'selected' : ''?>>Api error</option>
                <option value="1" <?=isset($filter_log_type) && $filter_log_type == 1 ? 'selected' : ''?>>App error</option>
              </select>
            </div>
          </div>
          <div class="clearfix">
            <div class="col-sm-12"><?php echo $import_store; ?></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
    <div class="panel-body">
    
<div class="row">
<div class="col-sm-6 text-left">
          <nav><?php echo $pagination; ?></nav>
        </div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        
      </div>
      
      <br/> 
<div class="table-responsive">
<table class="table" style="background:#fff; margin:0px auto; border:1px solid #fafafa;">
<tr>
<td width="2%">#ID</td>
<td width="10%">Time</td>
<td width="12%">User ID</td>
<td width="7%">IP</td>
<td width="7%">Device</td>
<td width="7%">App Ver</td>
<td width="7%">Android Ver</td>
<td width="55%">Error message</td>
</tr>
<?php foreach ($errors as $error){?>
<tr>
<td><?php echo $error['log_id'];?></td>
<td><?php echo $error['date_added'];?></td>
<td><?php echo $error['user_id'];?> - <?php echo $error['usercode'];?> </td>
<td><?php echo $error['user_ip'];?></td>
<td><?php echo $error['model'];?></td>
<td><?php echo $error['app_version'];?></td>
<td><?php echo $error['android_version'];?></td>
<td>
<?php echo html_entity_decode($error['error_message'], ENT_QUOTES, 'UTF-8');?>

</td>
</tr>

<?php } ?>
</table>
</div>
<br/> 
<div class="col-sm-6 text-left">
          <nav><?php echo $pagination; ?></nav>
        </div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        
      </div>
</div><!--//box-->

      </div>
</div>


<script type="text/javascript"><!--
    function clearLogs(){
      $('#modal-confirm').modal('show');
      $('#btn-modal-confirm').click(function(event) {
        event.preventDefault();
          $('#modal-confirm').modal('hide');
		  $.ajax({
				url:'index.php?route=api/log_error/clear',
				type: 'POST',
				dataType: 'json',
				data: 'hidden_key=' + $('#hidden_key').val(),
				success: function(json) {
					if(json['success']){
						$('#hidden_key').val('');
						$('#message').html('<div class="alert alert-success alert-sm"><i class="fa fa-check-circle"></i> '+json['success']+'<button type="button" class="close" data-dismiss="alert">×</button></div>');
					
						if(json['redirect']) {
							location = json['redirect'];
						}
					}
					if(json['error']){
						$('#message').html('<div class="alert alert-danger alert-sm"><i class="fa fa-check-circle"></i> '+json['error']+'<button type="button" class="close" data-dismiss="alert">×</button></div>');
					}
				}
			});
      });
    }
	function clearContains(){
		 $('#modal-clear').modal('show');
		  $('#btn-modal-clear').click(function(event) {
			event.preventDefault();
			  $('#modal-clear').modal('hide');
			  $.ajax({
					url:'index.php?route=api/log_error/clear_contains',
					type: 'POST',
					dataType: 'json',
					data: 'hidden_key=' + $('#hidden_key2').val()+'&clear_contains=' + $('#clear_contains').val(),
					success: function(json) {
						if(json['success']){
							$('#hidden_key').val('');
							$('#message').html('<div class="alert alert-success alert-sm"><i class="fa fa-check-circle"></i> '+json['success']+'<button type="button" class="close" data-dismiss="alert">×</button></div>');
						
							if(json['redirect']) {
								location = json['redirect'];
							}
						}
						if(json['error']){
							$('#message').html('<div class="alert alert-danger alert-sm"><i class="fa fa-check-circle"></i> '+json['error']+'<button type="button" class="close" data-dismiss="alert">×</button></div>');
						}
					}
				});
		  });
	}
    //-->
</script>

<div id="modal-clear" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        Xóa log theo nội dung
      </div>
      <div class="modal-body">
        <div class="mpadding">
        <label class="control-label">Nội dung chứa</label>
         <input type="text" name="clear_contains" value="" placeholder="...OutOfMemoryError..." id="clear_contains" class="form-control"/><br/> 
        <label class="control-label">Khóa bí mật</label>
         <input type="text" name="hidden_key2" value="" placeholder="Khóa bí mật" id="hidden_key2" class="form-control"/><br/> 

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success pull-right" data-dismiss="modal" aria-hidden="true">Không</button>
        <a id="btn-modal-clear" class="btn btn-danger pull-right" style="margin-right:15px;">Xác nhận Xóa</a>
      </div>
    </div>
  </div>
</div>
<!--//module-modal -->
<div id="modal-confirm" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        Vui lòng điền khóa bí mật:
      </div>
      <div class="modal-body">
        <div class="mpadding">
         <input type="text" name="hidden_key" value="" placeholder="Khóa bí mật" id="hidden_key" class="form-control"/>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success pull-right" data-dismiss="modal" aria-hidden="true">Không</button>
        <a id="btn-modal-confirm" class="btn btn-danger pull-right" style="margin-right:15px;">Xác nhận Xóa</a>
      </div>
    </div>
  </div>
</div>
<!--//module-modal -->
<script type="text/javascript">
 function filter(){
    url = 'index.php?route=api/log_error/errors';
    var filter_log_type = $('select[name=\'filter_log_type\']').val();
    if (filter_log_type != '*') {
      url += '&filter_log_type=' + encodeURIComponent(filter_log_type);
    }
    location = url;
  }
</script>