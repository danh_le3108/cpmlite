<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>POSM| Documentation</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo $http_server;?>assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $http_server;?>assets/plugins/font-awesome/css/font-awesome.min.css"> 
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $http_server;?>assets/admin-lte/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo $http_server;?>assets/admin-lte/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="<?php echo $http_server;?>/html/docs/style.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue fixed" data-spy="scroll" data-target="#scrollspy">
    <div class="wrapper">
      <header class="main-header">
          <a href="../index2.html" class="logo">
             <!-- mini logo for sidebar mini 50x50 pixels -->
             <span class="logo-mini"><b>C</b>PM</span>
             <!-- logo for regular state and mobile devices -->
             <span class="logo-lg"><b>CPM</b>CMS</span>
          </a>
          <!-- Header Navbar: style can be found in header.less -->
          <nav class="navbar navbar-static-top" role="navigation">
             <!-- Sidebar toggle button-->
             <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
             <span class="sr-only">Toggle navigation</span>
             </a>
             <!-- Navbar Right Menu -->
          </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
          <!-- sidebar: style can be found in sidebar.less -->
          <div class="sidebar" id="scrollspy">
             <!-- sidebar menu: : style can be found in sidebar.less -->
             <ul class="nav sidebar-menu">
                <li class="header">TABLE OF CONTENTS</li>
                <li><a href="#introduction"><i class="fa fa-circle-o"></i> Danh sách APIs</a></li>
                <li><a href="#user_login"><i class="fa fa-circle-o"></i> 1,2: User</a></li>
                <li><a href="#get_time"><i class="fa fa-circle-o"></i>  3,4: Time</a></li>
                <li><a href="#localisation"><i class="fa fa-circle-o"></i> 5,6,7: Địa lý</a></li>
                <li><a href="#get_rounds"><i class="fa fa-circle-o"></i> 8: List Tháng</a></li>
                <li><a href="#get_image_type"><i class="fa fa-circle-o"></i> 9: List Camera</a></li>
                <li><a href="#get_plan_reason"><i class="fa fa-circle-o"></i> 10: List Lý do KTC</a></li>
                <li><a href="#get_store_type"><i class="fa fa-circle-o"></i> 11: List loại cửa hàng</a></li>
                <li><a href="#get_plans"><i class="fa fa-circle-o"></i> 12: List plans</a></li>
                <li><a href="#get_posm"><i class="fa fa-circle-o"></i> 13: List posm thực tế</a></li>
                <li><a href="#check_in"><i class="fa fa-circle-o"></i> 15: NV checkin</a></li>
                <li><a href="#upload_images"><i class="fa fa-circle-o"></i> 16: Upload image</a></li>
                <li><a href="#upload_audio"><i class="fa fa-circle-o"></i> 17: Upload audio</a></li>
                 <li><a href="#upload_attribute"><i class="fa fa-circle-o"></i> 18: Upload attribtute </a></li>
                <li><a href="#upload_plan_info"><i class="fa fa-circle-o"></i> 19: Upload Plan Info</a></li>
                <li><a href="#update_status"><i class="fa fa-circle-o"></i> 20: Update_status </a></li>
                <li><a href="#add_store"><i class="fa fa-circle-o"></i> 21: Tạo store và plan </a></li>
             </ul>
          </div>
          <!-- /.sidebar -->
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
           <h1>
              POSM
           </h1>
           <ol class="breadcrumb">
              <li class="active"><a href="https://retailbuild.vn/pepsit7/index.php?route=api/docs"><i class="fa fa-dashboard"></i> Home / API docs</a></li>
           </ol>
        </div>
        <div class="content body">
          <div class="row">
            <div class="col-sm-5"></div>
            <!--//col--> 
            <div class="col-sm-7"></div>
            <!--//col--> 
          </div>
          <section id="description">
            <h2 class="page-header"><a href="#introduction">Mô tả</a></h2>
            <pre class="hierarchy bring-up">
              <code class="language-bash" data-lang="bash">
              - App menu 
                + Checkin
                + Chụp hình
                + Nhập liệu 
                + Ghi chú
              </code>
            </pre>
          </section>
          <section id="introduction">
            <h2 class="page-header"><a href="#introduction">Danh sách API có sẵn</a></h2>
            <pre class="hierarchy bring-up">
              <code class="language-bash" data-lang="bash">
              User
                1. https://retailbuild.vn/pepsit7/index.php?route=api/user_login     - Đăng nhập
                2. https://retailbuild.vn/pepsit7/index.php?route=api/user_info   - Thông tin NV

              Thời gian
                3. https://retailbuild.vn/pepsit7/index.php?route=api/get_time    - Định dạng Timestamp : {"data":1495773550
                4. https://retailbuild.vn/pepsit7/index.php?route=api/get_time_full   - Định dạng d/m/Y h:i:s : {"data":"26/05/2017 11:39:24"

              Địa lý
                5. https://retailbuild.vn/pepsit7/index.php?route=api/local_provinces   - List tỉnh thành
                6. https://retailbuild.vn/pepsit7/index.php?route=api/local_districts   - List quận huyện
                7. https://retailbuild.vn/pepsit7/index.php?route=api/local_wards   - List phường xã

              Plan Data
                8. https://retailbuild.vn/pepsit7/index.php?route=api/get_rounds    - List các Tháng có plan
                9. https://retailbuild.vn/pepsit7/index.php?route=api/get_image_type   - List Camera
                10. https://retailbuild.vn/pepsit7/index.php?route=api/get_plan_reason    - List Lý do KTC
                11. https://retailbuild.vn/pepsit7/index.php?route=api/get_store_type    - List loại cửa hàng
                12. https://retailbuild.vn/pepsit7/index.php?route=api/get_plans   - List Plan của Nhân viên
                13. https://retailbuild.vn/pepsit7/index.php?route=api/get_attributes&group_id=1    - Nhập liệu
                15. https://retailbuild.vn/pepsit7/index.php?route=api/check_in   - NV checkin tại CH
                16. https://retailbuild.vn/pepsit7/index.php?route=api/upload_images    - API upload hình ảnh
                17. https://retailbuild.vn/pepsit7/index.php?route=api/upload_audio    - API upload ghi âm
                18. https://retailbuild.vn/pepsit7/index.php?route=api/upload_attribute    - Api upload attribute 
                19. https://retailbuild.vn/pepsit7/index.php?route=api/upload_plan_info - API upload thông tin Plan
                20. https://retailbuild.vn/pepsit7/index.php?route=api/update_status    - Api upload trạng thái 
                21. https://retailbuild.vn/pepsit7/index.php?route=api/add_store    - Tạo store và plan 
              </code>
            </pre>
          </section>
          <br><br><br><br><br><br><br><br><br><br>

          <!-- USER -->
          <section id="user_login">
            <h2 class="page-header"><a href="#user_login">1,2: User</a></h2>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/login</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                Post lên
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                    api_key: xxx
                    username: xxx
                    password: xxx   - Khi test app sử dụng username/password test quy ước nha Nhựt 
                    imei: xxx 
                    device_model: xxx 
                  </code>
                </pre>
                Trả về
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "data": {
                      "user_id": $user_id,
                      "workplace": "Bình Dương",
                      "p_re_ward": "Tân Phú",
                      ...
                      "latitude": "",
                      "longitude": ""
                    },
                    "logged": $user_id,
                    "status": 1,
                    "message": "Success: API session successfully started!"
                  }
                  </code>
                </pre>
              </div>
            </div>

            <p >API thông tin nhân viên </p>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/user_info</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                Post lên
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                    api_key: xxx
                    username: xxx
                    password: xxx  
                  </code>
                </pre>
                Trả về
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "user_id": $user_id,
                    "usercode": $usercode,
                    "username": $username,
                    "fullname": "CPM TEST STAFF",
                    "email": "teo@meo.com",
                    "telephone": "47658964",
                    "image": "",
                    ...
                    "area_id": "0",
                    "region_id": "0",
                    "province_id": "74",
                    "district_id": "0",
                    "ward_id": "0",
                    "address": "",
                    "success": "Success: API session successfully started!"
                  }
                  </code>
                </pre>
              </div>
            </div>
          </section>
          <br><br><br><br><br><br><br><br><br><br>

          <!-- TIME -->
          <section id="get_time">
            <h2 class="page-header"><a href="#get_time">3,4: Time</a></h2>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/get_time</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                    "data": 1495782144,
                    "status": 1,
                    "message": "Get time success"
                  </code>
                </pre>
              </div>
            </div>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/get_time_full</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                    "data": "26/05/2017 14:03:26",
                    "status": 1,
                    "message": "Get full time success"
                  </code>
                </pre>
              </div>
            </div>
          </section>
          <br><br><br><br><br><br><br><br><br><br>

          <!-- LOCATION -->
          <section id="localisation">
            <h2 class="page-header"><a href="#localisation">5,6,7: Tỉnh thành</a></h2>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/local_provinces</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "status": 1,
                    "message": "Success: API session successfully started!",
                    "data": [
                      {
                        "province_id": "89",
                        "prefix": "Tỉnh",
                        "name": "An Giang",
                        "province_name": "An Giang",
                        "province_alias": "an giang",
                        "latitude": "10.371350",
                        "longitude": "105.444966",
                        "province_full": "Tỉnh An Giang"
                      },
                      ...
                    }
                  </code>
                </pre>
              </div>
            </div>
            <br><br><br>
            <h2 class="page-header"><a href="">Quận huyện</a></h2>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/local_districts</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "status": 1,
                    "message": "Success: API session successfully started!",
                    "data": [
                      {
                        "district_id": "353",
                        "province_id": "46",
                        "prefix": "Huyện",
                        "name": "A Lưới",
                        "district_name": "A Lưới",
                        "district_alias": "a luoi",
                        "district_full": "Huyện A Lưới"
                      },
                      ...
                    }
                  </code>
                </pre>
              </div>
            </div>
            <br><br><br>
            <h2 class="page-header"><a href="">Phường xã</a></h2>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/local_wards</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "status": 1,
                    "message": "Success: API session successfully started!",
                    "data": [
                      {
                        "ward_id": "1",
                        "name": "Liên Hồng",
                        "ward_name": "Liên Hồng",
                        "district_id": "1",
                        "province_id": "1",
                        "latitude": "",
                        "longitude": ""
                      },
                      ...
                    }
                  </code>
                </pre>
              </div>
            </div>
          </section>
          <br><br><br><br><br><br><br><br><br><br>

          <!-- ROUND -->
          <section id="get_rounds">
            <h2 class="page-header"><a href="#get_rounds">8: Rounds - Danh sách các tháng có plan</a></h2>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/get_rounds</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  "total": 5,
                  "data": [
                    {
                      "round_name": "2017-05"
                    },
                    {
                      "round_name": "2017-04"
                    },
                  </code>
                </pre>
              </div>
            </div>
          </section>
          <br><br><br><br><br><br><br><br><br><br>

          <!-- IMAGE TYPE -->
          <section id="get_image_type">
            <h2 class="page-header"><a href="#get_image_type">9: List Camera</a></h2>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/get_image_type</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "data": [
                      {
                        "image_type_id": "1",
                        "project_id": "0",
                        "image_type": "Tác phong",
                        "upload_limit": "2",
                        "required": "0",
                        "sort_order": "1"
                      },
                      {
                      ...
                      }

                    ],
                    "status": 1,
                    "message": "Load Image Type Success"
                  }
                  </code>
                </pre>
              </div>
            </div>
          </section>
          <br><br><br><br><br><br><br><br><br><br>

          <!-- KTC -->
          <section id="get_plan_reason">
            <h2 class="page-header"><a href="#get_reward_types">10: Lý do KTC</a></h2>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/get_plan_reason</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "data": [
                      {
                        "reason_id": "1",
                        "reason_name": "Cửa hàng đóng cửa",
                        "sort_order": "1"
                      },
                      {
                      ...
                      }

                    ],
                    "status": 1,
                    "message": "Load Success"
                  }
                  </code>
                </pre>
              </div>
            </div>
          </section>
          <br><br><br><br><br><br><br><br><br><br>

          <!-- STORE TYPE -->
          <section id="get_store_type">
            <h2 class="page-header"><a href="#get_store_type">11: Danh sách loại cửa hàng</a></h2>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/get_store_type</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "api_name": "get_store_type",
                    "data": [
                        {
                            "store_type_id": "3",
                            "type_name": "Cà phê",
                            "type_description": "",
                        },
                        {
                            "store_type_id": "5",
                            "type_name": "Khác",
                            "type_description": "",
                        }
                    ],
                    "status": 1,
                    "message": "Load Store type Success"
                  }
                  </code>
                </pre>
              </div>
            </div>
          </section>
          <br><br><br><br><br><br><br><br><br><br>

          <!-- GET PLAN -->
          <section id="get_plans">
            <h2 class="page-header"><a href="#get_plans">12: Danh sách cửa hàng (PLAN) của nhân viên</a></h2>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/get_plans</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                POST LÊN
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "api_key": "xxx",
                    'user_id': "xxx",
                    'plan_id': "xxx"
                  }
                  </code>
                </pre>
                TRẢ VỀ
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "api_name": "get_plan",
                    "message": "Success.",
                    "status": 1,
                    "total": 5,
                    "data": [
                        {
                            "plan_id": "1",
                            "round_name": "2017-11",
                            "plan_name": "201711",
                            "plan_status": "0",
                            "plan_rating": "1",
                            "upload_count": "0",
                            "date_start": "2017-11-07",
                            "date_end": "2017-12-10",
                            "user_id": "3519",
                            "import_id": "24",
                            "store_id": "1",
                            "store_code": "PJMHCMC000001",
                            "pepsi_code": "1",
                            "store_type_id": "1",
                            "store_type": "CF",
                            "store_owner": "AA",
                            "store_address": "",
                            "store_place": "",
                            "store_ward": "AA",
                            "store_district": "Ho Chi Minh",
                            "store_province": "Hồ Chí Minh",
                            "province_id": "79",
                            "region_code": "HCMC",
                            "store_address_raw": "AA, Ho Chi Minh, Hồ Chí Minh",
                            "store_phone": "1111",
                            "latitude": "",
                            "longitude": "",
                            "store_image": "https://retailbuild.vn/media/cache/files/PEPSI_baychtet18/2017-11/PJMHCMC000001/image/1-cpmtest-qwewqeq-300x300.png",
                            "visicooler": "1",
                            "posm": [
                                {
                                    "name": "Poster",
                                    "value": "1"
                                },
                                {
                                    "name": "Kệ carton",
                                    "value": "2"
                                },
                                {
                                    "name": "Tranh tường",
                                    "value": "3"
                                }
                            ]
                        },
                        ...
                      ]

                  }
                  </code>
                </pre>
              </div>
            </div>
          </section>
          <br><br><br><br><br><br><br><br><br><br>

          <!-- GET POSM -->
          <section id="get_posm">
            <h2 class="page-header"><a href="#get_posm">13: List POSM thực tế</a></h2>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/get_attributes&group_id=1</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                TRẢ VỀ
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "api_name": "get_attributes",
                    "data": [
                        {
                            "attribute_id": "6",
                            "attribute_code": "0",
                            "attribute_name": "Kệ carton",
                            "capacity": "",
                            "group_code": "REAL_POSM",
                            "sort_order": "2",
                            "sort_excel": "0",
                            "input": "text",
                            "group_id": "2",
                            "group_name": "Lắp đặt POSM thực tế"
                        },
                        {
                            "attribute_id": "7",
                            "attribute_code": "0",
                            "attribute_name": "Tranh nhựa 3D",
                            "capacity": "",
                            "group_code": "REAL_POSM",
                            "sort_order": "2",
                            "sort_excel": "0",
                            "input": "text",
                            "group_id": "2",
                            "group_name": "Lắp đặt POSM thực tế"
                        }
                    ]
                  }
                  </code>
                </pre>
              </div>
            </div>
          </section>
          <br><br><br><br><br><br><br><br><br><br>

          <!-- GET GIFT -->
          <section id="get_gift">
            <h2 class="page-header"><a href="#get_gift">14: List trả thưởng</a></h2>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/get_attributes&group_id=2</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                TRẢ VỀ
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "api_name": "get_attributes",
                    "data": [
                        {
                            "attribute_id": "6",
                            "attribute_code": "0",
                            "attribute_name": "Kệ carton",
                            "capacity": "",
                            "group_code": "REAL_POSM",
                            "sort_order": "2",
                            "sort_excel": "0",
                            "input": "text",
                            "group_id": "2",
                            "group_name": "Lắp đặt POSM thực tế"
                        },
                        {
                            "attribute_id": "7",
                            "attribute_code": "0",
                            "attribute_name": "Tranh nhựa 3D",
                            "capacity": "",
                            "group_code": "REAL_POSM",
                            "sort_order": "2",
                            "sort_excel": "0",
                            "input": "text",
                            "group_id": "2",
                            "group_name": "Lắp đặt POSM thực tế"
                        }
                    ]
                  }
                  </code>
                </pre>
              </div>
            </div>
          </section>
          <br><br><br><br><br><br><br><br><br><br>

          <!-- CHECKIN -->
          <section id="check_in">
            <h2 class="page-header"><a href="#check_in">15: Check in</a></h2>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/check_in</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                POST LÊN
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "api_key": "xxx",
                    'user_id': "xxx",
                    'plan_id': "xxx",
                    'version': 'xxx',
                    'imei': xxx,
                    'model': xxx,
                    'import_id': xxx,
                    'latitude': xxx,
                    'longitude': xxx,
                  }
                  </code>
                </pre>
                TRẢ VỀ
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "api_name": "check_in",
                    "message": "",
                    "status": 1/0,
                    "plan_id": x
                  }
                  </code>
                </pre>
              </div>
            </div>
          </section>
          <br><br><br><br><br><br><br><br><br><br>

          <!-- UPLOAD IMAGE -->
          <section id="upload_images">
            <h2 class="page-header"><a href="#upload_images">17: Upload image</a></h2>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/upload_images</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                POST LÊN
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "api_key": "xxx",
                    'user_id': "xxx",
                    'plan_id': "xxx",
                    'image_type': 'xxx',
                    'sha1': xxx,
                  }
                  </code>
                </pre>
                TRẢ VỀ
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "api_name": "upload_images",
                    "message": "",
                    "status": 1/0
                  }
                  </code>
                </pre>
              </div>
            </div>
          </section>
          <br><br><br><br><br><br><br><br><br><br>

          <!-- UPLOAD AUDIO -->
          <section id="upload_audio">
            <h2 class="page-header"><a href="#upload_audio">17: Upload audio</a></h2>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/upload_audio</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                POST LÊN
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "api_key": "xxx",
                    'user_id': "xxx",
                    'plan_id': "xxx"
                  }
                  </code>
                </pre>
                TRẢ VỀ
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "api_name": "upload_audio",
                    "message": "",
                    "status": 1/0
                  }
                  </code>
                </pre>
              </div>
            </div>
          </section>
          <br><br><br><br><br><br><br><br><br><br>

          <!-- UPLOAD ATTR -->
          <section id="upload_attribute">
            <h2 class="page-header"><a href="#upload_attribute">18: Upload attribute</a></h2>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/upload_attribute</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                POST LÊN
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "api_key": "xxx",
                    'user_id': "xxx",
                    'plan_id': "xxx",
                    attribute_list: [
                      {
                        'attribute_id': xxx,
                        'value': xxx
                      },
                      {
                        'attribute_id': xxx,
                        'value': xxx
                      }
                    ]
                  }
                  </code>
                </pre>
                TRẢ VỀ
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "api_name": "upload_attibute",
                    "message": "",
                    "status": 1/0
                  }
                  </code>
                </pre>
              </div>
            </div>
          </section>
          <br><br><br><br><br><br><br><br><br><br>

          <!-- UPLOAD PLAN INFO -->
          <section id="upload_plan_info">
            <h2 class="page-header"><a href="#upload_plan_info">19: Upload PLAN INFO</a></h2>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/upload_plan_info</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                POST LÊN
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "api_key": "xxx",
                    'user_id': "xxx",
                    'plan_id': "xxx",
                    note    : Note TC,
                    note_ktc    :Note KTC,
                    note_ktc_other  : Note khác,
                    rating: 0/1,
                    total_image: x,
                    count_audio: x,
                    store_type_id: x
                  }
                  </code>
                </pre>
                TRẢ VỀ
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "api_name": "upload_plan_info",
                    "message": "",
                    "status": 1/0
                  }
                  </code>
                </pre>
              </div>
            </div>
          </section>
          <br><br><br><br><br><br><br><br><br><br>

           <!-- UPLOAD STATUS -->
          <section id="update_status">
            <h2 class="page-header"><a href="#update_status">20: Update status</a></h2>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/pepsit7/index.php?route=api/update_status</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                POST LÊN
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "api_key": "xxx",
                    'user_id': "xxx",
                    'plan_id': "xxx"
                  }
                  </code>
                </pre>
                TRẢ VỀ
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    "api_name": "update_status",
                    "message": "",
                    "status": 1/0
                  }
                  </code>
                </pre>
              </div>
            </div>
          </section>
          <br><br><br><br><br><br><br><br><br><br>

            <!-- ADD STORE -->
          <section id="add_store">
            <h2 class="page-header"><a href="#add_store">21: Add store</a></h2>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">https://retailbuild.vn/464646index.php?route=api/add_store</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                POST LÊN
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                    api_key: xxx
                    user_id: xxx
                    store_name: xxx    // Tên cửa hiệu
                    store_owner: xxx  // Người kinh doanh
                    store_address: xxx  // Số nhà
                    store_place: xxx  // Đường/ Ấp
                    store_ward: xxx  // Phường/ Xã
                    store_district: xxx  // Quận/ Huyện
                    store_phone: xxx  // Số điện thoại
                    province_id: xxx //  Tỉnh thành (api local_provinces)
                    posm: xxx         // Loại posm lặp đặt (api get_posm)
                  }
                  </code>
                </pre>
                TRẢ VỀ 1 Plan mới
                <pre class="hierarchy bring-up">
                  <code class="language-bash" data-lang="bash">
                  {
                        "api": "add_store",
                        "status": 1,
                        "message": "Success",
                        "data": {
                        "plan_id": "6",
                        "is_fix": "0",
                        "fix_notes": null,
                        "round_name": "2017-12",
                        "plan_name": "201712",
                        "plan_status": "0",
                        "plan_rating": "0",
                        "upload_count": "0",
                        "date_start": "2017-12-11",
                        "date_end": null,
                        "user_id": "3519",
                        "import_id": "0",
                        "store_id": "30000",
                        "store_code": "PSPHCM30000",
                        "store_name": "asd",
                        "pepsi_code": "",
                        "store_type_id": "0",
                        "store_type": "",
                        "store_owner": "asd",
                        "store_address": "60",
                        "store_place": "nguyen dinh chieu",
                        "store_ward": "da kao",
                        "store_district": "quan 1",
                        "store_province": "Hồ Chí Minh",
                        "province_id": "79",
                        "region_code": "",
                        "store_address_raw": "60, nguyen dinh chieu, da kao, quan 1, Hồ Chí Minh",
                        "store_phone": "090909090",
                        "latitude": null,
                        "longitude": null,
                        "store_image": null,
                        "posm": {
                            "id": "1",
                            "value": "Khung Tranh Nhựa"
                        }
                        }
                    }
                  </code>
                </pre>
              </div>
            </div>
          </section>
          <br><br><br><br><br><br><br><br><br><br>
        </div>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
           All rights reserved.
        </div>
        <strong>Copyright &copy; 2001-2017 <a href="https://cpm-vietnam.com/">CPM Vietnam</a>.</strong>
      </footer>
      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <div class="pad">
           This is an example of the control sidebar.
        </div>
      </aside>
      <!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
        immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
    <!-- jQuery 2.2.3 -->
    <script src="<?php echo $http_server;?>assets/plugins/jquery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo $http_server;?>assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo $http_server;?>assets/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $http_server;?>assets/admin-lte/js/app.min.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="<?php echo $http_server;?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo $http_server;?>/html/docs/docs.js"></script>
  </body>
</html>