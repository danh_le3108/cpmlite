<table id="user" class="table table-striped table-bordered table-hover">
    <?php foreach ($staff_customers as $user) {
      if($user['status']==0||$user['customer_level_id']==0){
      ?>
  <tbody>
    <tr>
      <td width="1"><div class="img-thumbnail" title="<?php echo $user['customer_user_id']; ?> - <?php echo $user['fullname']; ?>"><img src="<?php echo $user['thumb'];?>" /></div>
      </td>
      <td class="text-left">
         <span data-toggle="tooltip" title="Last update: <?php echo $user['user_update'];?>"><?php echo $user['fullname'];?></span>
      <br /> 
       <span class="badge"> <?php echo $user['username'];?></span>
      </td>
    </tr>
    <tr>
      <td class="text-center" colspan="2">
        <span class="text-<?php echo ($user['status']==1)?'success':'primary ';?>">
        <?php echo $user['text_status'];?></span>
         <button type="button" onclick="addUser('<?php echo $user['customer_level_id']; ?>','<?php echo $user['customer_parent_id']; ?>','<?php echo $user['customer_level_id']; ?>','<?php echo $user['customer_user_id']; ?>');" data-toggle="tooltip" title="<?php echo $text_reactive;?>" class="btn btn-xs btn-success"><i class="fa fa-plus-circle"></i></button>
         
         <button type="button" onclick="editRoute('customer_user','<?php echo $user['customer_user_id']; ?>');" data-toggle="tooltip" title="<?php echo $text_edit;?>" class="btn btn-xs btn-success"><i class="fa fa-edit"></i></button>
          <?php if(!empty($user['real_token'])){?>
          <a type="button" onclick="decodeVal('<?php echo $user['real_token']; ?>');" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>          
            <?php } ?>
      </td>
    </tr>
  </tbody>
    <?php } ?>
    <?php } ?>
  <tfoot>
    <tr>
      <td colspan="2"></td>
    </tr>
  </tfoot>
</table>