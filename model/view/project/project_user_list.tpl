<table id="user" class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <td class="text-left" colspan="2"> <input type="text" name="project_user" value="" placeholder="<?php echo $text_user; ?>" class="form-control" /></td>
    </tr>
    <!--//--> 
    <tr>
      <td class="text-left" width="1"></td>
      <td class="text-left" width="250"><?php echo $text_user; ?></td>
    </tr>
  </thead>
    <?php foreach ($project_users as $user) {
      if($user['user_status']==0){
      ?>
  <tbody>
    <tr>
      <td class="text-left"><div class="img-thumbnail" title="<?php echo $user['user_id']; ?> - <?php echo $user['fullname']; ?>"><img src="<?php echo $user['thumb'];?>" /></div>
      </td>
      <td class="text-left">
      <span class="badge"> <?php echo $user['username'];?></span> 
<?php if($user['user_status']==0){
        $user_group_id = ($user['user_group_id']==0)?10:$user['user_group_id'];
        ?>
          <button type="button" onclick="addUser('<?php echo $user_group_id; ?>','0','<?php echo $user_group_id; ?>','<?php echo $user['user_id']; ?>');" data-toggle="tooltip" title="<?php echo $text_reactive;?>" class="btn btn-xs btn-success pull-right"><i class="fa fa-plus-circle"></i></button>
                              <?php } ?>
      
      <br />
<span data-toggle="tooltip" title="Last update: <?php echo $user['user_update'];?>"><?php echo $user['fullname'];?></span>
      <br /> 
       
       <span class="text-<?php echo ($user['user_status']==1)?'success':'error';?>">
        <?php echo $text_status;?>:<?php echo ($user['user_status']==1)?$text_active:$text_deactive;?></span>
        
                              
      </td>
    </tr>
   
  </tbody>
    <?php } ?>
    <?php } ?>
  <tfoot>
    <tr>
      <td colspan="2"></td>
    </tr>
  </tfoot>
</table>
<script type="text/javascript" charset="utf-8" async defer>
  $('#none_group_user input[name=\'project_user\']').autocomplete({
      'source': function(request, response) {
          $.ajax({
              url: 'index.php?route=project/project_user/autocomplete&filter_global=' + encodeURIComponent(request),
              dataType: 'json',
              success: function(json) {
                  response($.map(json, function(item) {
                      return {
                          label: item['username'] + ' - ' +item['fullname'],
                          value: item['user_id']
                      }
                  }));
              }
          });
      },
      'select': function(item) {
          $.ajax({
              url: 'index.php?route=project/project_user/addUser2Project',
              type: 'POST',
              dataType: 'json',
              data: 'user_id='+item['value'],
              beforeSend: function() {
                $('.alert').remove();
              },
              success: function(json) {
                if(json['success']){
                  $('#user input[name=\'project_user\']').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> '+json['success']+'<button type="button" class="close" data-dismiss="alert">×</button></div>');
				  
					$('#none_group_user').load('index.php?route=project/project_user/listuser');
                }
                if(json['error']){
                  $('#user input[name=\'project_user\']').after('<div class="alert alert-danger"><i class="fa fa-check-circle"></i> '+json['error']+'<button type="button" class="close" data-dismiss="alert">×</button></div>');
                }
              }
          });
      }
  });
</script>
