          <?php foreach($user_groups as $user_group){?>
          <?php if($user_group['user_group_id']==$filter_group_id){?>
            <div class="box-header with-border">
              <h3 class="box-title" style="min-width:200px; float:left;"><?php echo $user_group['user_group_id']; ?> - <?php echo $user_group['group_name']; ?></h3>
              
            <div class="clearfix" style="max-width:300px;min-width:250px; float:left;">
              <?php if($user_group['user_group_id']>$logged_group &&$user_group['user_group_id']!=$user_staff){?>
              <select onchange="addUser('<?php echo $user_group['user_group_id'];?>','0','<?php echo $user_group['user_group_id']; ?>',this.options[this.selectedIndex].value);" class="form-control chosen" style="width:250px;">
                <option value="">--<?php echo $text_add;?> <?php echo $user_group['group_description']; ?>--</option>
                <?php foreach ($project_users as $staff) { ?>
                    <?php if($staff['user_status']==0) { ?>
                <option value="<?php echo $staff['user_id'];?>"><?php echo $staff['fullname'];?> - <?php echo $staff['usercode'];?></option>
                <?php } ?>
                <?php } ?>
              </select>
              <?php } ?>

            </div>
            <!--//&&$staff['group_id']>=$user_group['user_group_id']--> 
            <div class="pull-right">
             <button type="button" onclick="loadGroup('<?php echo $user_group['user_group_id'];?>');" data-toggle="tooltip" title="<?php echo $button_refresh;?>" class="btn btn-xs btn-primary"><i class="fa fa-refresh"></i></button>
            </div>
            
            
            </div>

            <div class="panel-body">
              <table class="table" id="user_group<?php echo $user_group['user_group_id'];?>">
                <?php foreach ($project_users as $user) { ?>
                <?php if($user['user_group_id']==$user_group['user_group_id'] && $user['user_status']==1){?>
                <tbody id="user-row<?php echo $user['user_id']; ?>">
                  <tr>
                    <td width="1">
                    <div class="img-thumbnail" title="<?php echo $user['user_id']; ?> - <?php echo $user['fullname']; ?>"><img src="<?php echo $user['thumb'];?>" />
                    
                    </div>
                    
                    </td>
                    <td>
                  <!--
                      <small class="help"><?php echo $user['parent_fullname'];?> &raquo; </small>//--> 
                      <span class="fullname" data-toggle="tooltip" title="#<?php echo $user['user_id'];?> - Last update: <?php echo $user['user_update'];?>"><?php echo $user['fullname'];?></span>
                      

                      - <small class="help">Usercode: <?php echo $user['usercode'];?></small> - <small class="help">Username: <?php echo $user['username'];?></small> 
                     <!-- <br /> <span><?php echo $user_group['group_description'];?> - </span>//--> 
                       <?php echo ($user['user_group_id']==$user_sup)?'<br/>'.$user['region_code']:'';?> 
                       
                      
                <?php if($user['user_group_id']==$user_staff ){?>
<?php echo ($user['user_parent_id']==0)?'<br/><span class="badge bg-red">Chưa có SUP</span>':'';?> 
 <br />Device: <span class="badge"><?php echo $user['device_model'];?></span> <?php if($user['new_model']!=$user['device_model']&&$user['new_model']!=''){?>- New device: <span class="badge"><?php echo $user['new_model'];?> <a onclick="approveDevice('<?php echo $user['user_group_id'];?>','<?php echo $user['user_id'];?>')" class="btn btn-xs btn-success"><?php echo $text_approved;?></a></span> <?php } ?>
 				<?php } ?>

                    </td>
                    
                     <td class="text-left">
                    
           <?php if($user_group['user_group_id']==$user_sup){?>
                    <div class="input-group">
            <select name="region_code" id="region_code" class="form-control" onchange="updateRegion('<?php echo $user_group['user_group_id'];?>','<?php echo $user['user_id']; ?>',this.options[this.selectedIndex].value)" style="width:150px;">
              <option value="*"> - <?php echo $text_belong_to;?> <?php echo $text_region;?> - </option>
              <?php if(!empty($regions)){?>
              <?php foreach($regions as $region){?>
              <option value="<?php echo $region['region_code'];?>" <?php echo ($region['region_code']==$user['region_code'])?'selected="selected"':'';?> ><?php echo $region['region_code'];?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </div>
              <?php } ?>
                    
                    </td>
                    
                    <td class="text-right"> 
                    <span class="text-<?php echo ($user['user_status']==1)?'success':'error';?>">
                    <?php echo $text_status;?>:
        <?php echo ($user['user_status']==1)?$text_active:$text_deactive;?></span>
                    </td>
                    <td class="text-right">
                      <button type="button" onclick="deleteUser('<?php echo $user_group['user_group_id'];?>','<?php echo $user['user_id']; ?>');" data-toggle="tooltip" title="<?php echo $text_deactive;?>" class="btn btn-xs btn-danger"><i class="fa fa-minus-circle"></i></button>
                    </td>
                    
                  </tr>
                     <tr>
                    <td class="text-left"></td>
                    <td class="text-left">
                    <!--// Nếu không phải nhóm Staff--> 
                    <?php if($user_group['child_group_id']!= 0){?>
                      <!--//User is DC--> 
                      <?php if($user['user_group_id']==$user_dc){?>    
         <select onchange="updateManager('<?php echo $user['user_group_id']; ?>','<?php echo $user['user_id']; ?>',this.options[this.selectedIndex].value,'1');" class="form-control chosen" style="width:250px;" title="<?php echo $user_group['child_group_id'];?>">
                        <option value="">-- <?php echo $text_manager;?> <?php echo $user_group['child_group'];?> --</option>
                        <?php foreach ($project_users as $staff) {?>
                        <?php if(!in_array($staff['user_id'],$user['user_managers'])&&$staff['user_group_id']==$user_group['child_group_id']){?>
	 <option value="<?php echo $staff['user_id'];?>"><?php echo $staff['fullname'];?> - <?php echo $staff['usercode'];?></option>
	                        <?php } ?>
                        <?php } ?>
                      </select>
         
         
                      <!--//not is DC--> 
                      <?php }else{?>      
         <select onchange="addUser('<?php echo $user['user_group_id']; ?>','<?php echo $user['user_id']; ?>','<?php echo $user['child_group_id']; ?>',this.options[this.selectedIndex].value);" class="form-control chosen" style="width:250px;" title="<?php echo $user_group['child_group_id'];?>">
                        <option value="">-- <?php echo $text_manager;?> <?php echo $user_group['child_group'];?> --</option>
                        <?php foreach ($project_users as $staff) {?>
	              			<?php if($staff['user_group_id']==$user['child_group_id']&&($staff['user_parent_id']!=$user['user_id']||$staff['user_status']==0)) { ?>
	 <option value="<?php echo $staff['user_id'];?>"><?php echo $staff['fullname'];?> - <?php echo $staff['usercode'];?></option>
	                        <?php } ?>
                        <?php } ?>
                      </select>
         
                      <?php } ?>
                      <!--//end not is DC--> 
         
                    	<?php } ?>
                    </td>
                    <td class="text-left"></td>
                    <td class="text-left"></td>
                    <td class="text-left"></td>
                     </tr>
                  <!-- Staft user here -->
           <?php if($user_group['user_group_id']!=$user_staff){?>
                  <tr>
                    <td width="100">
                      &raquo; <?php echo $user_group['child_group'];?>
                    </td>
                    <td class="text-left" colspan="4">
                     <table class="table">
                      <!--//USER DC START--> 
                      <?php if($user['user_group_id']==$user_dc){?>
                        <?php
                         foreach ($project_users as $staff) {  ?>
                        <?php if(in_array($staff['user_id'],$user['user_managers'])){?>
                        <tbody id="user-row<?php echo $staff['user_id']; ?>">
                          <tr>
                            <td>
                    
                             <small class="help"><?php echo $user['fullname'];?>&raquo; </small>
                             <span class="fullname" data-toggle="tooltip" title="Last update: <?php echo $staff['user_update'];?>"><?php echo $staff['fullname'];?></span>
                         - <small class="help">Usercode: <?php echo $staff['usercode'];?></small>  - <small class="help">Username: <?php echo $staff['username'];?></small>  
                               </td>
                               <td>
                            </td>
                           
                    <td class="text-right"> 
                    <span class="text-<?php echo ($staff['user_status']==1)?'success':'error';?>">
                    <?php echo $text_status;?>:
        <?php echo ($staff['user_status']==1)?$text_active:$text_deactive;?></span>
                    </td>
                            <td class="text-right">
                              <button type="button" onclick="updateManager('<?php echo $user['user_group_id'];?>','<?php echo $user['user_id'];?>','<?php echo $staff['user_id']; ?>','0');" data-toggle="tooltip" title="<?php echo $button_remove;?> <?php echo $staff['user_id']; ?>" class="btn btn-xs btn-danger"><i class="fa fa-minus-circle"></i></button>
                            </td>
                          </tr>
                        </tbody>
                        <?php } ?>
                        <?php } ?>
                      
                      <!--//USER DC --> 
                      <?php }else{?>
                       <?php foreach ($project_users as $staff) {  ?>
                        <?php if($staff['user_group_id']==$user['child_group_id']&&$staff['user_parent_id']==$user['user_id']){?>
                        <tbody id="user-row<?php echo $staff['user_id']; ?>">
                          <tr>
                            <td>
                        <small class="help"><?php echo $user['fullname'];?>&raquo; </small>
                             <span class="fullname" data-toggle="tooltip" title="Last update: <?php echo $staff['user_update'];?>"><?php echo $staff['fullname'];?></span>
                         
                         - <small class="help">Usercode: <?php echo $staff['usercode'];?></small>  - <small class="help">Username: <?php echo $staff['username'];?></small>  
                            
                <?php if($staff['user_group_id']==$user_staff ){?>
 <br />Device: <span class="badge"><?php echo $staff['device_model'];?></span> <?php if($staff['new_model']!=$staff['device_model']&&$staff['new_model']!=''){?>- New device: <span class="badge"><?php echo $staff['new_model'];?> <a onclick="approveDevice('<?php echo $staff['user_group_id'];?>','<?php echo $staff['user_id'];?>')" class="btn btn-xs btn-success"><?php echo $text_approved;?></a></span> <?php } ?>
 				<?php } ?>
                               </td>
                               <td>

                            </td>
                    <td class="text-right"> 
                    <span class="text-<?php echo ($staff['user_status']==1)?'success':'error';?>">
                    <?php echo $text_status;?>:
        <?php echo ($staff['user_status']==1)?$text_active:$text_deactive;?></span>
                    </td>
                            <td class="text-right">
                            <?php if($staff['user_status']==1){?>
                              <button type="button" onclick="deleteUser('<?php echo $user_group['user_group_id'];?>','<?php echo $staff['user_id']; ?>');" data-toggle="tooltip" title="<?php echo $text_deactive;?>" class="btn btn-xs btn-danger"><i class="fa fa-minus-circle"></i></button>
                              <?php }else{?>
                              <button type="button" onclick="addUser('<?php echo $user['user_group_id']; ?>','<?php echo $user['user_id']; ?>','<?php echo $user['child_group_id']; ?>','<?php echo $staff['user_id']; ?>');" data-toggle="tooltip" title="<?php echo $text_reactive;?>" class="btn btn-xs btn-success"><i class="fa fa-plus-circle"></i></button>
                              <?php } ?>
                            </td>
                          </tr>
                        </tbody>
                        <?php } ?>
                        <?php } ?>
                      <?php } ?>
                      <!--//end not is DC--> 
                      </table>
                    </td>
                  </tr>
                        <?php } ?>
                  <!-- End staft user -->
                </tbody>
                <?php } ?>
                <?php } ?>
                   <tfoot></tfoot>
              </table>
            </div>
            <div class="box-footer with-border"></div>
  <script type="text/javascript"><!--
		$(".chosen").chosen({ search_contains: true });
    //-->
  </script>
<?php } ?>
<?php } ?>
        