          <?php foreach($customer_groups as $level){?>
          <?php if($level['customer_level_id']==$filter_group_id){?>
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $level['customer_level_id']; ?> - <?php echo $level['level_name']; ?></h3>
              
            
        <div class="pull-right box-tools text-right">
                 <a type="button" onclick="editRoute('customer_level','<?php echo $level['customer_level_id']; ?>');" data-toggle="tooltip" title="<?php echo $button_edit;?>" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a> 
             </div> 
             
            </div>
            <div class="box-header no-border">

            <div class="clearfix" style="max-width:300px;">
              <select onchange="addUser('<?php echo $level['customer_level_id'];?>','0','<?php echo $level['customer_level_id']; ?>',this.options[this.selectedIndex].value);" class="form-control chosen" style="width:250px;">
                <option value="">--<?php echo $text_add;?> <?php echo $level['level_name']; ?>--</option>
                <?php foreach ($staff_customers as $staff) { ?>
                    <?php if($staff['status']==0||$staff['customer_level_id']==0) { ?>
                <option value="<?php echo $staff['customer_user_id'];?>"><?php echo $staff['fullname'];?></option>
                <?php } ?>
                <?php } ?>
              </select>

            </div>   
            </div>
            <div class="panel-body">
              <table class="table" id="user_group<?php echo $level['customer_level_id'];?>">
                <?php foreach ($staff_customers as $user) {?>
                <?php if($user['customer_level_id']==$level['customer_level_id']){?>
                <tbody>
                   <tr>
                    <td width="1">
                    <div class="img-thumbnail" title="<?php echo $user['customer_user_id']; ?> - <?php echo $user['fullname']; ?>"><img src="<?php echo $user['thumb'];?>" /></div>
                    
                    </td>
                    <td>
                      <span class="fullname"><?php echo $user['fullname'];?></span> 
                      <?php if($user['customer_level_id']!=$config_customer_top){?>
                          <?php if(!empty($user['parent_fullname'])){?>
                          <small class="help"> &laquo; <?php echo $user['parent_fullname'];?></small>
                          <?php }else{?>
                          <span class="badge alert-error"> &laquo; Chưa có quản lý</span>
                          <?php } ?>
                      <?php } ?>
                      
                      <br /> <span class="badge" data-toggle="tooltip" title="Last update: <?php echo $user['user_update'];?>"><?php echo $user['customer_user_id']; ?> - <?php echo $user['username'];?></span>



                    </td>

                    <td class="text-left">
                     
                      <?php if($level['customer_level_id'] !=$config_customer_sup){?>
                    	<?php } ?>
                      <select onchange="addUser('<?php echo $user['customer_level_id']; ?>','<?php echo $user['customer_user_id']; ?>','<?php echo $level['child_level_id']; ?>',this.options[this.selectedIndex].value);" class="form-control chosen" style="width:250px;">
                        <option value="">--<?php echo $text_manager;?> <?php echo $level['child_level']; ?>--</option>
                        <?php foreach ($staff_customers as $staff) {?>
	              			<?php if(($staff['customer_level_id']==0||$staff['customer_level_id']==$level['child_level_id'])&&$staff['customer_parent_id']!=$user['customer_user_id']) { ?>
	 <option value="<?php echo $staff['customer_user_id'];?>"><?php echo $staff['fullname'];?></option>
	                        <?php } ?>
                        <?php } ?>
                      </select>
                    </td>
                    <td class="text-right">
                    
                 <span class="text-<?php echo ($user['status']==1)?'success':'danger';?>"><?php echo $user['text_status'];?></span> 
                
                 <a type="button" onclick="editRoute('customer_user','<?php echo $user['customer_user_id']; ?>');" data-toggle="tooltip" title="<?php echo $button_edit;?>" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>      
                 <?php if(!empty($user['real_token'])){?>
         <a type="button" onclick="decodeVal('<?php echo $user['real_token']; ?>');" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>    
                   <?php } ?>
                    </td>
                    <td class="text-right">
           <?php if($user['customer_level_id']!=$config_customer_sup){?>
           				 <?php if($user['status']==1){?>
                      <button type="button" onclick="deleteUser('<?php echo $level['customer_level_id'];?>','<?php echo $user['customer_user_id']; ?>');" data-toggle="tooltip" title="<?php echo $text_deactive;?>" class="btn btn-xs btn-danger"><i class="fa fa-minus-circle"></i></button>
                        <?php }else{ ?>
                       <button type="button" onclick="addUser('<?php echo $level['customer_level_id'];?>','<?php echo $user['customer_parent_id']; ?>','<?php echo $level['customer_level_id']; ?>','<?php echo $user['customer_user_id']; ?>');" data-toggle="tooltip" title="<?php echo $text_reactive;?>" class="btn btn-xs btn-success"><i class="fa fa-plus-circle"></i></button>
                        <?php } ?>
                        <?php } ?>
                    </td>
                  </tr>
                  <!-- STAFF USER START -->

           <?php if($user['customer_level_id']!=$config_customer_sup){?>
                  <tr>
                    <td width="1">
                      &raquo; <?php echo $level['child_level'];?>
                    </td>
                    <td class="text-left" colspan="4">
                       
                    </td>
                  </tr>
                  <?php foreach ($staff_customers as $staff) {  ?>
                        <?php if($staff['customer_parent_id']==$user['customer_user_id']&&$user['customer_user_id']!=$staff['customer_user_id']){?>

                          <tr>
                    		<td width="1"></td>
                            <td> 
                           <span class="fullname"><?php echo $staff['fullname'];?></span> 
                             
                       <?php if($staff['customer_level_id']!=$config_customer_top){?>
                          <?php if(!empty($staff['parent_fullname'])){?>
                          <small class="help"> &laquo; <?php echo $staff['parent_fullname'];?></small>
                          <?php }else{?>
                          <span class="badge alert-error"> &laquo; Chưa có quản lý</span>
                          <?php } ?>
                      <?php } ?>
                                              
<br />
<span class="badge" data-toggle="tooltip" title="Last update: <?php echo $staff['user_update'];?>"><?php echo $staff['customer_user_id']; ?> -  <?php echo $staff['username'];?></span>  


                               </td>
                               <td>
                               </td>
                    <td class="text-right">
                 <span class="text-<?php echo ($staff['status']==1)?'success':'danger';?>"><?php echo $staff['text_status'];?> </span> 

                 <a type="button" onclick="editRoute('customer_user','<?php echo $staff['customer_user_id']; ?>');" data-toggle="tooltip" title="<?php echo $button_edit;?>" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>  
                 <?php if(!empty($staff['real_token'])){?>
         <a type="button" onclick="decodeVal('<?php echo $staff['real_token']; ?>');" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>          
                    <?php } ?>      
                            </td>
                            <td class="text-right"> <!--  -->
                            
                         <?php if($staff['status']==1){?>
                              <button type="button" onclick="deleteUser('<?php echo $level['customer_level_id'];?>','<?php echo $staff['customer_user_id']; ?>');" data-toggle="tooltip" title="<?php echo $text_deactive;?>" class="btn btn-xs btn-danger"><i class="fa fa-minus-circle"></i></button>
                        <?php }else{ ?>
                              <button type="button" onclick="addUser('<?php echo $level['customer_level_id'];?>','<?php echo $user['customer_user_id']; ?>','<?php echo $level['child_level_id']; ?>','<?php echo $staff['customer_user_id']; ?>');" data-toggle="tooltip" title="<?php echo $text_reactive;?>" class="btn btn-xs btn-success"><i class="fa fa-plus-circle"></i></button>
                        <?php } ?>
                        
                            </td>
                          </tr>
                        <?php } ?>
                        <?php } ?>
                        
                        <?php } ?>

                  <!-- STAFF USER START END -->
                </tbody>
                <?php } ?>
                <?php } ?>
                   <tfoot></tfoot>
              </table>
            </div>
            <div class="box-footer with-border"></div>

<?php } ?>
<?php } ?>
<script type="text/javascript"><!--
	$(".chosen").chosen({ search_contains: true });
    //-->
</script>