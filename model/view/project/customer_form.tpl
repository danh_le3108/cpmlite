 <div class="modal-dialog modal-lg">
        <div class="modal-content">
        
    <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal">×</button>
    </div>
            <div class="modal-body">
             
               
              <div class="form-group row">
                    <div class="col-sm-6">
                        <label class="control-label" for="username"><?php echo $text_username;?></label>
                        <div class="clearfix">
                          <input type="text" name="username" value="<?php echo $info['username']; ?>" placeholder="<?php echo $text_username;?>" id="username" class="form-control" autocomplete="false" />
                          <small> Không gõ khoảng trắng (space bar).</small>
                        </div>
                      </div>
                    <div class="col-sm-6">
                        <label class="col-sm-2 control-label" for="input-fullname"><?php echo $text_fullname; ?></label>
                        <div class="clearfix">
                          <input type="text" name="fullname" value="<?php echo $info['fullname']; ?>" placeholder="<?php echo $text_fullname; ?>" id="input-fullname" class="form-control" autocomplete="false" />
                        </div>
                      </div>
               </div>
               
              <div class="form-group row">
              
                  <div class="col-sm-4">
                    <label class="control-label" for="user_group"><?php echo $text_user_group; ?></label>
                     <div class="clearfix">
                       <select name="customer_level_id" id="customer_level_id" class="form-control">
                <option value="0"><?php echo $text_select; ?></option>
                <?php foreach($customer_groups as $level){?>
                <option value="<?php echo $level['customer_level_id'];?>" <?php echo ($info['customer_level_id']==$level['customer_level_id'])?'selected="selected"':'';?>><?php echo $level['level_name'];?></option>
                <?php } ?>  
              </select>
              
                    </div>
                  </div><!--//col--> 
                  <div class="col-sm-4">
                    <label class="control-label" for="input-email"><?php echo $text_email; ?></label>
                     <div class="clearfix">
                      <input type="text" name="email" value="<?php echo $info['email']; ?>" placeholder="<?php echo $text_email; ?>" id="input-email" class="form-control" autocomplete="false" />
                      
                    </div>
                  </div>
                 <div class="col-sm-4"> 
                  <label class=" control-label" for="input-telephone"><?php echo $text_telephone; ?></label>
                 <div class="clearfix">
                  <input type="text" name="telephone" value="<?php echo $info['telephone']; ?>" placeholder="<?php echo $text_telephone; ?>" id="input-telephone" class="form-control" autocomplete="false" />
                </div>
                   
                  </div>
               </div><!--//--> 
              <div class="form-group row">
                  <div class="col-sm-6">
               <label class="control-label" for="input-password"><?php echo $text_password; ?></label>
                     <div class="clearfix">
                      <input type="password" name="password" value="" placeholder="<?php echo $text_password; ?>" id="input-password" class="form-control" autocomplete="false" />
                    </div>
              </div>
                  <div class="col-sm-6">
                <label class="control-label" for="input-confirm"><?php echo $text_password_confirm; ?></label>
                 <div class="clearfix">
                  <input type="password" name="confirm" value="" placeholder="<?php echo $text_password_confirm; ?>" autocomplete="false" id="input-confirm" class="form-control" />
                </div>
              </div>
              
               </div><!--//--> 
               
            </div>
            <!--panel body --> 
          
    <div class="modal-footer">
    <div class="row">
    
    <div class="col-sm-6 message">
    </div>
    
    <div class="col-sm-6">
     <a onClick="saveRoute('customer_user','<?php echo $customer_user_id;?>','<?php echo $info['customer_level_id'];?>');" class="btn btn-primary btn-sm pull-right"><i class="fa fa-save"></i> <?php echo $button_save; ?></a>
    </div>
    
    </div>
    </div>
    <!--//modal-footer -->
            </div>
    </div>
    <!--// modal -->