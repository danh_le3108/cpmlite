<div class="page-header">
  
</div>
<div class="container-fluid" id="filter-area">
  <div class="box box-info">
    <div class="panel-body">
      <div class="row">          
          <div class="col-sm-3">
            <div class="form-group">
              <label class="control-label" for="input-filter_global">Lọc</label>
              <input type="text" name="filter_global" value="" id="input-filter_global" class="form-control filter" onchange="javascript:filter()"/>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
                <label class="control-label" for="input-date_to">Ngày</label>
                <input type="text" id="date" value="" name="date" class="form-control datepicker filter" onchange="javascript:filter()" autocomplete="off">
             </div>
          </div>
          <div class="col-sm-6">
            <button type="button" id="button-clear" class="btn btn-primary btn-sm pull-right" autocomplete="off"><i class="fa fa-eraser"></i>Xóa bộ lọc</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="ajax_list"></div>
<div id="modal-confirm" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        Bạn có chắc chắn muốn xóa Store <span></span>?
      </div>
      <div class="modal-body">
        <div class="mpadding">
          Store này không thể khôi phục nhưng lịch sử sẽ được lưu lại.
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success pull-right" data-dismiss="modal" aria-hidden="true">Không</button>
        <a id="btn-modal-confirm" class="btn btn-danger pull-right" style="margin-right:15px;">Xác nhận Xóa</a>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
  $(document).ready(function(){
    var url = '<?=str_replace("amp;","",$url)?>';
    $('#ajax_list').load(url);

    <?php if(!empty($filter_global)){?>
    $("td span:contains(<?php echo $filter_global;?>)").css({"color":"red","background":"yellow"});
    <?php } ?>
  });

  // $('input[name=\'filter_global\']').autocomplete({
  //   'source': function(request, response) {
  //     $.ajax({
  //       url: 'index.php?route=store/store/autocomplete&filter_global=' +  encodeURIComponent(request),
  //       dataType: 'json',
  //       success: function(json) {
  //         response($.map(json, function(item) {
  //           return {
  //             label: item['store_code'] + ' - ' + item['store_name'],
  //             value: item['store_id']
  //           }
  //         }));
  //       }
  //     });
  //   },
  //   'select': function(item) {
  //     $('input[name=\'filter_global\']').val(item['label']);
  //   }
  // });

  // function deleteStore(store_id,store_name){
  //     $('#modal-confirm .modal-header span').html(store_name);
  //     $('#modal-confirm').modal('show');
  //     $('#btn-modal-confirm').click(function(event) {
  //       event.preventDefault();
  //       $('#btn-modal-confirm').unbind('click');
  //       $.get('index.php?route=store/store/delete&store_id='+store_id, function(data){
  //         $('#modal-confirm').modal('hide');
  //         $('#store_id_'+store_id).remove();
  //       })
  //     });
  // }
</script>