


    <div class="col-sm-6">
    <div class="box box-info">
    <div class="box-header">
        <h3 class="box-title"><?php echo $heading_plan_info; ?></h3>
      </div>
      <div class="box-body">

<table class="table table-bordered table-striped">
                        <tbody>
                         
                           
                            <tr>
                                 <td width="20%"><?php echo $text_staff_user;?></td>
                                <td width="30%"><div class="form-control input-sm">
                                <?php echo isset($staffs[$plan_info['user_id']])?$staffs[$plan_info['user_id']]['fullname']:'';?>
                                </div>
                               
                                </td>
                                
        <td width="20%"><?php echo $text_plan_survey;?></td>
        <td width="30%"> <?php foreach ($surveys as $survey) {?>
            <?php if(in_array($survey['survey_id'],$survey_ids)&&$survey['survey_id']!=1){?>
                    <?php echo $survey['survey_name'];?><br>
                    <?php } ?>
                    <?php } ?>
                  </div>
        </td>
                            </tr>
                        
                            
                             <tr>
                                <td><?php echo $text_time_checkin;?></td>
                                <td><?php echo $plan_info['time_checkin'];?></td>
                                
                                <td>Device:<br />
                                App version:</td>
                                <td><?php echo ucwords($plan_info['model']);?><br />
                                <?php echo $plan_info['version'];?>
</td>
                            </tr>
                            
                            <tr>
                                <td style="color: red;"><?php echo $text_status;?></td>
                                <td colspan="3"><select disabled="disabled" name="plan[plan_status]" class="form-control input-sm">
                                    <?php foreach($statuses as $value=>$label){?>
                                        <option value="<?php echo $value;?>" <?php echo ($value==$plan_info['plan_status'])?'selected="selected"':'';?>><?php echo $label;?></option>
                                      <?php } ?>                                            
                                    </select>
                                    </td>
                            </tr>
                            
                             <tr>
                                 <td>Ghi chú TC</td>
                                <td colspan="3">
                                   <input disabled="disabled" type="text" name="plan[note]" value="<?php echo $plan_info['note'];?>" class="form-control"/>
                                </td>
                                   
                            </tr>
                             <tr>
                                 <td><?php echo $text_unsuccess_reason;?></td>
                                <td colspan="3">
                                    <select disabled="disabled" name="plan[reason_id]" class="form-control input-sm">
                                        <option value="">----</option>
                                    <?php foreach($reasons as $reason){?>
                                        <option value="<?php echo $reason['reason_id'];?>" <?php echo ($plan_info['reason_id']==$reason['reason_id'])?'selected="selected"':'';?>><?php echo $reason['reason_name'];?></option>
                                      <?php } ?>                                            
                                    </select>
                                </td>
                            </tr>
                            
                             <tr>
                                 <td><?php echo $text_unsuccess_comment;?></td>
                                <td colspan="3">
                                  <input disabled="disabled" type="text" name="plan[note_ktc]" value="<?php echo $plan_info['note_ktc'];?>" class="form-control"/>
                                </td>
                                   
                            </tr>
                            
                             
                            
                        </tbody>
                    </table>
                    
      </div>
        <?php if($plan_info['reason_id']>0){?>  
                         
 <div class="box-header">
        <h3 class="box-title"><?php echo $text_call_confirm; ?></h3>
      </div>   
      <div class="box-body">
      <table class="table">
                             <tr>
                                <td><?php echo $text_call_recipient;?></td>
                                <td colspan="3"><input disabled="disabled" type="text" name="plan[call_recipient]" value="<?php echo $plan_info['call_recipient'];?>" class="form-control"/></td>
                               
                            </tr>
                            
                             <tr>
                                <td ><?php echo $text_call_phone_number;?></td>
                                <td colspan="3"><input disabled="disabled" type="text" name="plan[call_phone_number]" value="<?php echo $plan_info['call_phone_number'];?>" class="form-control"/></td>
                               
                            </tr>
                            
                             <tr>
                                <td ><?php echo $text_call_date;?></td>
                                <td colspan="3"><input disabled="disabled" type="text" name="plan[call_date]" value="<?php echo ($plan_info['call_date']!='0000-00-00')?$plan_info['call_date']:'';?>" class="form-control datepicker" data-date-format="YYYY-MM-DD"/></td>
                                
                            </tr>
                            
                             <tr>
                                <td ><?php echo $text_call_num;?></td>
                                <td colspan="3"><input disabled="disabled" type="text" name="plan[call_num]" value="<?php echo $plan_info['call_num'];?>" class="form-control"/></td>
                               
                            </tr>
                            
                             <tr>
                                <td ><?php echo $text_call_note;?></td>
                                <td colspan="3"><input disabled="disabled" type="text" name="plan[call_note]" value="<?php echo $plan_info['call_note'];?>" class="form-control"/></td>
                               
                            </tr>
                    </table>
                    
      </div>
                            <?php } ?>
    </div><!--//box--> 
    </div><!-- //col--> 
    
    <div class="col-sm-6">
     <div class="box box-info">
    <div class="box-header">
        <h3 class="box-title"><?php echo $heading_store_info; ?></h3>
      </div>
      <div class="box-body">

<table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td width="20%">ID</td>
                                <td width="30%">#<?php echo $plan_info['store_id']; ?></td>
                                
                            </tr>
                            <tr>
                                <td width="20%">Avatar</td>
                                <td width="30%"><img height="100" src="<?php echo $store_image;?>"/></td>
                                
                            </tr>
                            <tr>
                                <td width="20%"><?php echo $text_store_name;?></td>
                                <td colspan="3"><?php echo $plan_info['store_name']; ?></td>
                                
                            </tr>
                            <tr>
                                <td><?php echo $text_store_code;?></td>
                                <td colspan="3">
                                   <?php echo $plan_info['store_code']; ?>
                                </td>
                                
                            </tr>
                            <tr>
                                <td ><?php echo $text_store_customer_code;?></td>
                                <td colspan="3">
                                    <?php echo $plan_info['store_customer_code']; ?>
                                </td>
                            </tr>
                            
                            <tr>
                                <td ><?php echo $text_store_ad_code;?></td>
                                <td colspan="3">
                                    <?php echo $plan_info['store_ad_code']; ?>
                                </td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $text_store_distributor;?></td>
                                <td colspan="3">
                                   <?php echo $plan_info['distributor_code']; ?> - <?php echo $plan_info['store_distributor']; ?>
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo $text_address;?></td>
                                <td colspan="3"><?php echo $plan_info['store_address_raw'];?></td>
                            </tr>
                            
                        </tbody>
                    </table>
                    
      </div>
       
    </div><!--//box--> 
    </div><!-- //col--> 
    