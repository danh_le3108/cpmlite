<style>
.highcharts-credits{display:none !important;}
</style>

  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
      <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#filter-area" data-toggle="tab"><?php echo $text_filter;?></a></li>
      <!-- <li><a href="#import-export" data-toggle="tab">Import/Export</a></li> -->
      <li class="pull-right"><button type="button" id="button-clear" class="btn btn-primary btn-sm pull-right"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button></li>
  
  
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="filter-area">
      
    <div class="panel-body">
      <div class="row">
      
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label" for="filter_round"><?php echo $text_round; ?></label>
            <select name="filter_round" id="filter_round" class="form-control" onchange="filter()">
              <option value="*"><?php echo $text_select; ?></option>
               <?php foreach($rounds as $round){?>
<option value="<?php echo $round['round_name'];?>" <?php echo ($round['round_name']==$filter_round)?'selected="selected"':'';?>><?php echo $round['round_name'];?></option>
              <?php } ?>  
            </select>
          </div>
        </div><!--col -->
        <div class="col-sm-3">
          <div class="form-group <?php echo ($isNationwide==1||$getLevel==$config_rsm)?'':'hide';?>">
        
            <label class="control-label" for="filter_rsm"><?php echo $text_nationwide; ?> / <?php echo $text_rsm;?></label>
            <select name="filter_rsm" id="filter_rsm" class="form-control chosen" onchange="filter()">
            
              <option value="*"> - <?php echo ($isNationwide==1)?$text_nationwide:$text_rsm; ?> - </option>
            
              <?php foreach ($rsms as $rsm) {?>
              <option value="<?php echo $rsm['customer_user_id']; ?>" <?php if ($rsm['customer_user_id'] == $filter_rsm) { ?>selected="selected"<?php } ?>><?php echo $rsm['fullname']; ?> </option>
              <?php } ?>
            </select>
          </div>
        </div><!--col -->
        <div class="col-sm-3">
          <div class="form-group <?php echo ($isNationwide==1||$getLevel==$config_rsm||$getLevel==$config_asm)?'':'hide';?>">
            <label class="control-label" for="filter_asm"><?php echo $text_asm;?></label>
            <select name="filter_asm" id="filter_asm" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_select; ?></option>
              <?php foreach ($asms as $asm) {?>
              <?php if (is_null($filter_rsm)||$asm['customer_parent_id']==$filter_rsm) { ?>
              <option value="<?php echo $asm['customer_user_id']; ?>" <?php if ($asm['customer_user_id'] == $filter_asm) { ?>selected="selected"<?php } ?>><?php echo $asm['fullname']; ?> </option>
              <?php } ?>
               <?php } ?>
            </select>
          </div>
        </div><!--col -->
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label" for="filter_customer_user_id"><?php echo $text_ssup;?></label>
            <select name="filter_customer_user_id" id="filter_customer_user_id" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_select; ?></option>
              <?php foreach ($sups as $sup) {?>
              <?php if (is_null($filter_asm)||$sup['customer_parent_id']==$filter_asm) { ?>
              <option value="<?php echo $sup['customer_user_id']; ?>" <?php if ($sup['customer_user_id'] == $filter_customer_user_id) { ?>selected="selected"<?php } ?>><?php echo $sup['fullname']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </div>
        </div><!--col -->
        
        
      </div><!--//row -->
     
        
    </div><!--panel-body -->
  </div>
  <!--box -->
  
      </div>
    </div>
    <!-- /.tab-content -->
  
  
  
  
    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title"> Skin Care - Mức đăng ký <span class="badge label-danger"><?php echo $skin_result['total'];?></span></h3>
      </div>
      <div class="panel-body">
      <div class="row">
      
       <div class="col-sm-3 col-xs-12">
       <table class="table">
            <tbody>
            
            
       <?php
               
               if(isset($skin_result['data'])&&!empty($skin_result['data'])){
               	foreach($skin_result['data'] as $survey_id => $sresult){
                   if(in_array($survey_id,$survey_skin)){?>
    				<tr>
                        <td>
                         #<?php echo $survey_id;?> - 
                         <?php echo $sresult['survey_name'];?>
                          <span class="label label-success pull-right">
                          <?php echo $sresult['total'];?> </span>
                   		</td>
                   </tr>
                  
                   <?php
                   }
                }
               
               }
        ?>
       
        
        </tbody>
        </table>
      </div><!--//col--> 
       <div class="col-sm-9 col-xs-12">
      
         <div class="chart" id="skin_chart" style="position: relative;"></div>
               <?php
               
                $skin_chart = array();
               if(isset($skin_result['data'])&&!empty($skin_result['data'])){
               	foreach($skin_result['data'] as $survey_id => $sresult){
                
                   if(in_array($survey_id,$survey_skin)){
                   		$a = isset($sresult['rating']['1'])?$sresult['rating']['1']:0;
                   		$b = isset($sresult['rating']['-1'])?$sresult['rating']['-1']:0;
                        $c = isset($sresult['rating']['-2'])?$sresult['rating']['-2']:0;
    					$skin_chart[] = "{ y: '#".$survey_id."', 'a': ".$a.", 'b': ".$b.", 'c': ".$c." }";
                  
                   }
                }
               
               }
               ?>
                 <?php  if(!empty($skin_chart)){?>
               <script type="text/javascript">
Morris.Bar({
  element: 'skin_chart',
  labelTop: true,
  data: [<?php echo implode(",", $skin_chart)?>],
  barColors: [<?php echo implode(",", $colors)?>],
  xkey: 'y',
  ykeys: ['a', 'b', 'c'],
  labels: ['ĐẠT', 'KHÔNG ĐẠT','KTC']
});
		</script>
        <?php } ?>
        
      </div><!--//col--> 
      </div><!--//row--> 
      </div><!--//panel-body--> 
      </div><!--//box--> 
     
      <div class="row">
       <div class="col-sm-6 col-xs-12">
      
    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title"> Skin Care - Đạt/Không đạt/KTC <span class="badge label-danger"><?php echo $skin_rating['total'];?></span></h3>
      </div>
      <div class="panel-body">
              
                <div class="chart" id="skin_rating" style=" position: relative;"></div>
                
               
                 <?php
                $nskin_data = array();
                 if(isset($skin_rating['data'])&&!empty($skin_rating['data'])){
                    foreach ($skin_rating['data'] as $val => $rating){
                    	if(isset($rating_result[$val])){
                        	$nskin_data[] = "{ name: '".$rating_result[$val].'('.$rating['total'].')'."', y: ".round(($rating['total']/$skin_rating['total'])*100,2)." }";
                    	}
                    }
                }
                ?>
                 <?php  if(!empty($nskin_data)){?>
		<script type="text/javascript">
$(document).ready(function () {

    // Build the chart
    Highcharts.chart('skin_rating', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },/*
        title: {
            text: 'Thống kê KTC '
        },*/ 
		colors: [<?php echo implode(",", $colors)?>],
        title: false,
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [<?php echo implode(",", $nskin_data)?>]
        }]
    });
});
		</script>
        <?php } ?>
        
      </div><!--//panel-body--> 
      </div><!--//box--> 
      </div><!--//col--> 
      <!--//--> 
       <div class="col-sm-6 col-xs-12">
      
    <div class="box box-info hide">
      <div class="box-header">
        <h3 class="box-title">Skin Care - KTC - <span class="badge label-danger"><?php echo $skin_unsuccess['total'];?></span></h3>
      </div>
      <div class="panel-body">
        <div class="chart" id="skin_unsuccess" style=" position: relative;"></div>
                
               
                 <?php
                $uskin_data = array();
               if(isset($skin_unsuccess['data'])&&!empty($skin_unsuccess['data'])){
                foreach ($skin_unsuccess['data'] as $su_reason){
                	$uskin_data[] = "{ name: '".$su_reason['reason_name'].'('.$su_reason['total'].')'."', y: ".round(($su_reason['total']/$skin_unsuccess['total'])*100,2)." }";
                }
                }
                ?>
                 <?php  if(!empty($uskin_data)){?>
		<script type="text/javascript">
$(document).ready(function () {

    // Build the chart
    Highcharts.chart('skin_unsuccess', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },/*
        title: {
            text: 'Thống kê KTC '
        },*/ 
        title: false,
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [<?php echo implode(",", $uskin_data)?>]
        }]
    });
});
		</script>
        <?php } ?>
      </div><!--//panel-body--> 
      </div><!--//box--> 
      </div><!--//col--> 
      </div><!--//row--> 
        
      
  
    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title"> Adhoc - Mức đăng ký <span class="badge label-danger"><?php echo $adhoc_result['total'];?></span></h3>
      </div>
      <div class="panel-body">
          <div class="row">
      
       <div class="col-sm-3 col-xs-12">
       <table class="table">
            <tbody>
            
            
       <?php
               
               if(isset($adhoc_result['data'])&&!empty($adhoc_result['data'])){
               	foreach($adhoc_result['data'] as $survey_id => $a_result){
                   if(in_array($survey_id,$survey_adhoc)){?>
    				<tr>
                        <td>
                         #<?php echo $survey_id;?> - 
                         <?php echo $a_result['survey_name'];?>
                          <span class="label label-success pull-right">
                          <?php echo $a_result['total'];?> </span>
                   		</td>
                   </tr>
                  
                   <?php
                   }
                }
               
               }
        ?>
       
        
        </tbody>
        </table>
      </div><!--//col--> 
       <div class="col-sm-9 col-xs-12">
       
       
         <div class="chart" id="adhoc_chart" style="position: relative;"></div>
               <?php
               
                $adhoc_chart = array();
               if(isset($adhoc_result['data'])&&!empty($adhoc_result['data'])){
               	foreach($adhoc_result['data'] as $survey_id => $a_result){
                   if(in_array($survey_id,$survey_adhoc)){
                    
                   		$a = isset($a_result['rating']['1'])?$a_result['rating']['1']:0;
                   		$b = isset($a_result['rating']['-1'])?$a_result['rating']['-1']:0;
                        $c = isset($a_result['rating']['-2'])?$a_result['rating']['-2']:0;
    					$adhoc_chart[] = "{ y: '#".$survey_id."', 'a': ".$a.", 'b': ".$b.", 'c': ".$c." }";
                        
                   }
                }
               
               }
               ?>
                 <?php  if(!empty($adhoc_chart)){?>
                
               <script type="text/javascript">
Morris.Bar({
  element: 'adhoc_chart',
  labelTop: true,
  data: [<?php echo implode(",", $adhoc_chart)?>],
  barColors: [<?php echo implode(",", $colors)?>],
  xkey: 'y',
  ykeys: ['a', 'b', 'c'],
  labels: ['ĐẠT', 'KHÔNG ĐẠT','KTC']
});
		</script>
        <?php } ?>
      </div><!--//col--> 
      </div><!--//row--> 
      </div><!--//panel-body--> 
      </div><!--//box--> 
    
      <?php if(isset($aaa)){?>
      <div class="row">
       <div class="col-sm-6 col-xs-12">
      
    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title">Adhoc - Không đạt -  <span class="badge label-danger"><?php echo $adhoc_not_pass['total'];?></span></h3>
      </div>
      <div class="panel-body">
                
                <div class="chart" id="adhoc_not_pass" style=" position: relative;"></div>
                
               
                 <?php
                $nadhoc_data = array();
               if(isset($adhoc_not_pass['data'])&&!empty($adhoc_not_pass['data'])){
                foreach ($adhoc_not_pass['data'] as $an_reason){
                	
                    $nadhoc_data[] = "{ name: '".$an_reason['reason_name'].'('.$an_reason['total'].')'."', y: ".round(($an_reason['total']/$adhoc_not_pass['total'])*100,2)." }";
                }
              }
                ?>
                 <?php  if(!empty($nadhoc_data)){?>
		<script type="text/javascript">
$(document).ready(function () {

    // Build the chart
    Highcharts.chart('adhoc_not_pass', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },/*
        title: {
            text: 'Thống kê KTC '
        },*/ 
        title: false,
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [<?php echo implode(",", $nadhoc_data)?>]
        }]
    });
});
		</script>
        <?php } ?>
        
      </div><!--//panel-body--> 
      </div><!--//box--> 
      </div><!--//col--> 
       <div class="col-sm-6 col-xs-12">
      
    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title">Adhoc - KTC - <span class="badge label-danger"><?php echo $adhoc_unsuccess['total'];?></span></h3>
      </div>
      <div class="panel-body">
        <div class="chart" id="adhoc_unsuccess" style=" position: relative;"></div>
                
               
                 <?php
                $uadhoc_data = array();
               if(isset($adhoc_unsuccess['data'])&&!empty($adhoc_unsuccess['data'])){
                foreach ($adhoc_unsuccess['data'] as $au_reason){
                	$uadhoc_data[] = "{ name: '".$au_reason['reason_name'].'('.$au_reason['total'].')'."', y: ".round(($au_reason['total']/$adhoc_unsuccess['total'])*100,2)." }";
                }
                }
                ?>
                 <?php  if(!empty($uadhoc_data)){?>
		<script type="text/javascript">
$(document).ready(function () {

    // Build the chart
    Highcharts.chart('adhoc_unsuccess', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },/*
        title: {
            text: 'Thống kê KTC '
        },*/ 
        title: false,
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [<?php echo implode(",", $uadhoc_data)?>]
        }]
    });
});
		</script>
        <?php } ?>
      </div><!--//panel-body--> 
      </div><!--//box--> 
      </div><!--//col--> 
      </div><!--//row--> 
        
      
        <?php } ?><!--//aaa--> 
  
      
      
  </div>
  <script type="text/javascript"><!--
  function filter() {
  	url = 'index.php?route=client/dashboard';

  	var filter_round = $('select[name=\'filter_round\']').val();

  	if (filter_round != '*') {
  		url += '&filter_round=' + encodeURIComponent(filter_round);
  	}
	
  	var filter_rsm = $('select[name=\'filter_rsm\']').val();

  	if (filter_rsm != '*') {
  		url += '&filter_rsm=' + encodeURIComponent(filter_rsm);
  	}
  	var filter_asm = $('select[name=\'filter_asm\']').val();

  	if (filter_asm != '*') {
  		url += '&filter_asm=' + encodeURIComponent(filter_asm);
  	}
  	var filter_customer_user_id = $('select[name=\'filter_customer_user_id\']').val();

  	if (filter_customer_user_id != '*') {
  		url += '&filter_customer_user_id=' + encodeURIComponent(filter_customer_user_id);
  	}
	
  	location = url;
  }
  
  //-->
</script>