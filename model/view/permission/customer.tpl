
    <div class="box">
      <div class="box-header">
        <h2 class="box-title">
        <?php echo $text_customer_role; ?></h3>
      </div>
      <div class="panel-body">
        <?php $x=0;
          foreach ($areas as $area) {?>
        <h3 class="text-center"><?php echo $area['label'];?></h3>
        <div class="table-responsive">
        <table class="table">
             <?php
                  $v= $a = $e= $d=0;
                   foreach ($area['folders'] as $folder) {?>
        <thead>
            <tr class="clearfix">
             <td class="text-right"><h3><?php echo $folder['label'];?></h3></td>
             <td style="vertical-align:bottom"><?php echo $text_access_per; ?></td>
             <td style="vertical-align:bottom"><?php echo $text_add_per; ?></td>
             <td style="vertical-align:bottom"><?php echo $text_edit_per; ?></td>
             <td style="vertical-align:bottom"><?php echo $text_delete_per; ?></td>
            </tr>
        </thead>
           
            <?php 
                  $v1= $a1 = $e1= $d1 =0;
                    foreach ($folder['permissions'] as $permission) { ?>
        <tbody>
                     <tr class="clearfix">
             <td class="text-right"><?php echo $permission['label']; ?></td>
             <td width="200" class="v<?php echo $x.'-'.$v;?>">
             <label for="v<?php echo $x.'-'.$v.'-'.$v1;?>">
             &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type="checkbox" name="config_customer_permission[access][]" id="v<?php echo $x.'-'.$v.'-'.$v1;?>" value="<?php echo $permission['value']; ?>" <?php if (in_array($permission['value'], $access)) { ?>checked="checked"<?php } ?>/>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                    </label>
             </td>
             <td width="200" class="a<?php echo $x.'-'.$a;?>">
             <label for="a<?php echo $x.'-'.$a.'-'.$a1;?>">
             &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type="checkbox" name="config_customer_permission[add][]" id="a<?php echo $x.'-'.$a.'-'.$a1;?>" value="<?php echo $permission['value']; ?>" <?php if (in_array($permission['value'], $add)) { ?>checked="checked"<?php } ?>/>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </label>
             
             </td>
             <td width="200" class="e<?php echo $x.'-'.$e;?>">
             <label for="e<?php echo $x.'-'.$e.'-'.$e1;?>">
              &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type="checkbox" name="config_customer_permission[edit][]" id="e<?php echo $x.'-'.$e.'-'.$e1;?>" value="<?php echo $permission['value']; ?>" <?php if (in_array($permission['value'], $edit)) { ?>checked="checked"<?php } ?>/>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </label>
             </td>
             <td width="200" class="d<?php echo $x.'-'.$d;?>">
               <label for="d<?php echo $x.'-'.$d.'-'.$d1;?>">
              &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type="checkbox" name="config_customer_permission[delete][]" id="d<?php echo $x.'-'.$d.'-'.$d1;?>" value="<?php echo $permission['value']; ?>" <?php if (in_array($permission['value'], $delete)) { ?>checked="checked"<?php } ?>/>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </label>
             </td>
            </tr>
                  <?php
                   $v1++; 
                   $a1++; 
                   $e1++; 
                   $d1++; 
                  }
                  ?>
                  
               
            
             <tr class="clearfix">
                 <td></td>
                 <td>
                      <a onclick="$(this).parents().find('.v<?php echo $x.'-'.$v;?> input').prop('checked', true);"><?php echo $text_select_all; ?></a>
                      / <a onclick="$(this).parents().find('.v<?php echo $x.'-'.$v;?> input').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                 </td>
                 <td>
                      <a onclick="$(this).parents().find('.a<?php echo $x.'-'.$a;?> input').prop('checked', true);"><?php echo $text_select_all; ?></a>
                      / <a onclick="$(this).parents().find('.a<?php echo $x.'-'.$a;?> input').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                 </td>
                 <td>
                 
                      <a onclick="$(this).parents().find('.e<?php echo $x.'-'.$e;?> input').prop('checked', true);"><?php echo $text_select_all; ?></a>
                      / <a onclick="$(this).parents().find('.e<?php echo $x.'-'.$e;?> input').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                 </td>
                 <td>
                      <a onclick="$(this).parents().find('.d<?php echo $x.'-'.$d;?> input').prop('checked', true);"><?php echo $text_select_all; ?></a>
                      / <a onclick="$(this).parents().find('.d<?php echo $x.'-'.$d;?> input').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                 </td>
            </tr>   
                <?php 
                $v++; 
                $a++; 
                $e++; 
                $d++; 
                } ?>
            
        </table>
    </div>
        <!-- table--> 
        <?php $x++;
          } ?>
      </div>
    </div>