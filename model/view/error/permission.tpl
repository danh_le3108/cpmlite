

<div id="content">
<div class="page-header">
  <div class="container-fluid">
    <h1><?php echo $heading_title; ?></h1>
  </div>
</div>
<div class="container-fluid">
  <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"> <?php echo $heading_title; ?></h3>
    </div>
    <div class="panel-body">
      <p class="text-center"><?php echo $text_permission; ?></p>
    </div>
  </div>
</div>