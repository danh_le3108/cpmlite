<style type="text/css">
  .date {
    width: 25px;
    float: left;
    text-align: center;
  }
  .status_0 {
    color: #F00;
  }
  .status_2 {
    color: #3c8dbc;
  }
  .status_1 {
    color: #00a65a;
  }
</style>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-7"><?php echo $pagination;?> </div>
    <div class="col-sm-5 text-right"><?php echo $results;?></div>
  </div>
  <div class="box box-info">
    <div class="box-header">
      <h3 class="box-title"><?php echo $heading_title;?></h3>
    </div>
    <div class="box-body table-responsive no-padding">
      <div class="panel-body">
        <table class="table table-hover table table-bordered">
          <thead>
            <tr>
              <th class="text-center" width="1">ID</th>
              <th>Cửa hàng</th>
              <th class="text-center" width="10%">Plan name / Date start</th>  
              <th class="text-center" width="10%">Trạng thái</th> 
              <?php if($user_id) { ?>
              <th class="text-center" width="5%">QC</th>
              <?php } ?>
              <th class="text-center" width="20%">NV thực hiện</th>
              <th class="text-center" width="15%">Ngày</th>
              <?php if($hasDel) {?>
              <th width="5%" class="text-center"> Xóa </th>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <?php  foreach($plans as $plan){
              $filename = DIR_MEDIA . $plan['image_overview'];
              if (is_file($filename)) {
                $avatar = $model_tool_image->resize($plan['image_overview'], 60, 60);
                $popup = HTTP_SERVER.'media/'.$plan['image_overview'];
              }else{
                $avatar = $thumb;
                $popup = '';
              }
            ?>
            <tr class="tr_info" id="plan_id_<?=$plan['plan_id']?>">
              <td class="img-popup text-center"><a href="<?=$popup?>" class="img-thumbnail"><img src="<?=$avatar?>" alt=""/></a><br/><?php echo $plan['plan_id'];?></td>
              <td>
                <a target="_blank" href="index.php?route=plan/plan/edit&plan_id=<?=$plan['plan_id']?>"><span <?=!empty($plan['users_read']) && in_array($user_id,explode(',',$plan['users_read'])) ? 'style="color:#800000"' : ''?>><strong><?php echo $plan['store_code'];?> - <?php echo $plan['store_name'];?>
                <span class="blur">
                  <br>
                  <i class="fa fa-map-marker"></i> &nbsp<?php echo $plan['store_address_raw'];?><br> 
                  <?php echo $plan['category'];?>
                </span>
              </td>
              <td class="text-center">
                <a target="_blank" href="index.php?route=plan/plan/edit&plan_id=<?=$plan['plan_id']?>"><?=$plan['plan_name']?></a><br>
                (<?=$plan['date']?>)
              </td>
              <td class="text-center">
                <span class="status_<?=$plan['plan_status']?>">
                  <?=isset($pl_status[$plan['plan_status']]) ? $pl_status[$plan['plan_status']] : 'Chưa thực hiện'?>
                </span>
              </td>
              <?php if($user_id) { ?>
              <td class="text-center"><i class="fa fa-<?php echo ($plan['plan_qc']==1)?'check text-success':'text-warning';?>"></i></td>
              <?php } ?>
              <td>
                <table>
                  <?php if(!empty($plan['users'])) {
                    foreach($plan['users'] as $user) {
                  ?>
                  <tr>
                    <td class="date">
                      <i class="fa fa-user-o text-primary" aria-hidden="true"></i>
                    </td>
                    <td style="color: <?=$user['user_group'] == '9' ? '#337ab7' : ''?>">
                      <?=$user['fullname']?>
                    </td>
                  </tr>
                  <?php }} ?>
                </table>
              </td>
              
              <td>
                <table>
                  <tr>
                    <td class="date">
                      <i class="fa fa-map-marker <?php echo (!empty($plan['time_checkin']) && $plan['time_checkin'] != '0000-00-00 00:00:00')?'check text-success':'text-grey';?>" title="Checkin"></i> </td>
                    <td><?=$plan['time_checkin']?></td>
                  </tr>
                  <?php if($user_id) { ?>
                  <!-- <tr>
                    <td class="date">
                      <i class="fa fa-cloud-upload <?php echo (!empty($plan['time_upload']) && $plan['time_upload'] != '0000-00-00 00:00:00')?'text-success':'text-grey';?>" title="Upload"></i> 
                    </td>
                    <td><?=$plan['time_upload']?></td>
                  </tr> -->
                  <?php if(!empty($plan['longitude']) && !empty($plan['latitude'])) { ?>
                  <tr>
                    <td class="date text-primary">
                      <i class="fa fa-map-marker" title="Tọa độ checkin"></i>
                    </td>
                    <td>
                      <a target="_blank" href="<?='https://www.google.com/maps/search/'.$plan['latitude'].','.$plan['longitude'].'/@'.$plan['latitude'].','.$plan['longitude'].',17z'?>"><?=$plan['latitude'].', '.$plan['longitude']?></a>
                    </td>
                  </tr>
                  <?php } ?>
                  <?php if(!empty($plan['version'])) { ?>
                  <tr>
                    <td class="date">
                      <i class="fa fa-mobile text-primary"></i>
                    </td>
                    <td>App version: <?=$plan['version']; ?></td>
                  </tr>
                  <?php } ?>
                  <?php } ?>
                </table>
              </td>
            
              <?php if($hasDel) {?>
              <td class="text-center">
                <a class="btn" style="color:#F00;" onclick="deletePlan('<?php echo $plan['plan_id']; ?>','<?php echo $plan['plan_name'].' - '.$plan['store_name']; ?>')"><i class="fa fa-trash"></i></a>
              </td>
              <?php } ?>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-7"><?php echo $pagination;?> </div>
    <div class="col-sm-5 text-right"><?php echo $results;?></div>
  </div>
</div>

<script type="text/javascript">
  $('.img-popup').magnificPopup({
    type:'image',
    delegate: 'a.img-thumbnail',
    gallery: {
      enabled:true
    }
  });
</script>