<div class="box box-info" >
  <div class="box-header">
    <h3 class="box-title">Trưng bày</h3>
  </div>
  <div class="box-body"  id="plan_merchandising">
    <?php if($hasEdit) {?>
    <div class="row" style="margin-bottom: 20px">
      <div class="col-sm-1 pull-right">
        <button class="btn btn-primary pull-right" id="add_merchandising">Thêm mới</button>
      </div>
      <div class="col-sm-3 pull-right">
        <select class="form-control chosen" id="merchandising-select">
          <option value="0">--- Nhãn hiệu thuốc ---</option>
          <?php 
          foreach ($attrs as $attr) { ?>
          <option value="<?=$attr['attribute_id']?>"><?=$attr['attribute_name']?></option>
          <?php }?>
        </select>
      </div> 
    </div>
    <?php } ?>
    <form class="row" action="index.php?route=plan/plan_merchandising/save&plan_id=<?=$plan_id?>" style="display:none" id="attribute_id_0">
      <div class="col-sm-12">
        <div class="box box-warning box-solid">
          <div class="box-header with-border">
            <h3 class="box-title"></h3>
          </button>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-sm-12">
                <table class="table">
                  <tr>
                    <th width="15%" style="line-height: 30px"></th>
                    <th width="20%">Loại trưng bày</th>
                    <th width="20%">Tình trạng</th>
                    <th width="20%">Khuyến mãi</th>
                    <th width="20%">Vị trí</th>
                    <?php if($hasDel) {?>
                    <td width="5%"></td>
                    <?php } ?>
                  </tr>
                  <tr id="item">
                    <td width="15%" style="line-height: 30px">
                      Vật dụng trưng bày 1
                    </td>
                    <td width="20%">
                      <select class="form-control">
                        <option value="0">--- Loại vật dụng ---</option>
                        <?php foreach($list_type as $t) { ?>
                        <option value="<?=$t['attribute_code']?>"><?=$t['attribute_name']?></option>
                        <?php }?>
                      </select>
                    </td>
                    <td width="20%">
                      <select class="form-control">
                        <option value="0">--- Tình trạng ---</option>
                        <?php foreach($list_status as $st) { ?>
                        <option value="<?=$st['attribute_code']?>"><?=$st['attribute_name']?></option>
                        <?php }?>
                      </select>
                    </td>
                    <td width="20%">
                      <select class="form-control">
                        <option value="0">--- Khuyến mãi ---</option>
                        <?php foreach($list_promotion as $pr) { ?>
                        <option value="<?=$pr['attribute_code']?>"><?=$pr['attribute_name']?></option>
                        <?php }?>
                      </select>
                    </td>
                    <td width="20%"> 
                      <select class="form-control">
                        <option value="0">--- Vị trí ---</option>
                        <?php foreach($list_position as $p) { ?>
                        <option value="<?=$p['attribute_code']?>"><?=$p['attribute_name']?></option>
                        <?php }?>
                      </select>
                    </td>
                    <?php if($hasDel) {?>
                    <td>
                      <button type="button" class="btn btn-danger del-mer"><i class="fa fa-remove"></i></button>
                    </td>
                    <?php } ?>
                  </tr>
                </table>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
          <?php if($hasEdit) {?>
          <div class="box-footer clearfix">
            <div class="col-sm-8 message">
            </div>
            <div class="col-sm-4">
              <button type="submit" class="btn btn-success btn-sm pull-right" autocomplete="off"><i class="fa fa-save"></i> Lưu</button>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  var attr_value = <?=json_encode($attr_value)?>;
  if(attr_value){
    for (var i in attr_value) {
      var attr = $('#attribute_id_0').clone();
      attr.attr('id','attribute_id_'+i);
      attr.find('#item').remove();
      attr.find('.box-title').html(attr_value[i][0].attribute_name);
      attr.css('display','block');
      attr.find('#item').remove();
      var stt = 1;
      for (var j in attr_value[i]) {
        var item = $('#item').clone();
        item.attr('id', 'item_'+attr_value[i][j].id);
        item.find('td:first').html('Vật dụng trưng bày ' + stt + '<input type="hidden" name="ids['+attr_value[i][j].id+']"></input>');
        var selects = item.find('select');
        selects.eq(0).val(attr_value[i][j].type);
        selects.eq(0).attr('name','type['+attr_value[i][j].id+']');
        selects.eq(1).val(attr_value[i][j].status);
        selects.eq(1).attr('name','status['+attr_value[i][j].id+']');
        selects.eq(2).val(attr_value[i][j].promotion);
        selects.eq(2).attr('name','promotion['+attr_value[i][j].id+']');
        selects.eq(3).val(attr_value[i][j].position);
        selects.eq(3).attr('name','position['+attr_value[i][j].id+']');
        attr.find('table').append(item);
        stt++;
      }
      attr.appendTo('#plan_merchandising');
    }
  }

  $('#add_merchandising').click(function(){
    var attribute_id = $('#merchandising-select').val();
    if (attribute_id > 0) {
      $.get('index.php?route=plan/plan_merchandising/add&plan_id=<?=$plan_id?>&attribute_id='+attribute_id, function(id){
        if (id) {
          if ($('#attribute_id_'+attribute_id).length > 0) {
            var item = $('#item').clone();
            $('#attribute_id_'+attribute_id).find('table').append(item);
          } else {
            var attr = $('#attribute_id_0').clone();
            attr.attr('id','attribute_id_'+attribute_id);
            attr.find('.box-title').html($('#merchandising-select option:selected').text());
            var item = attr.find('#item');
            attr.css('display','block');
            attr.appendTo('#plan_merchandising');
          }
          var stt =  $('#attribute_id_'+attribute_id).find('tr').length - 1;
          item.find('td:first').html('Vật dụng trưng bày ' + stt + '<input type="hidden" name="ids['+id+']"></input>');
          item.attr('id', 'item_'+ id);
          var selects = item.find('select');
          selects.eq(0).attr('name','type['+id+']');
          selects.eq(1).attr('name','status['+id+']');
          selects.eq(2).attr('name','promotion['+id+']');
          selects.eq(3).attr('name','position['+id+']');
        }
      })
    } else {
      alert('Vui lòng chọn nhãn hiệu thuốc!');
    }
  })

  $('#plan_merchandising').on('click', '.del-mer', function(){
    var tr = $(this).closest('tr');
    var id = tr.attr('id').replace('item_','');
    $.get('index.php?route=plan/plan_merchandising/delete&id='+id, function(data){
      if(data == 1){
        if (tr.closest('table').find('tr').length == 2) {
          tr.closest('form').remove();
        } else {
          tr.remove();  
        }
      }
    })
  })
</script>