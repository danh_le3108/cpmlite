<div class="box box-info">
  <div class="box-header">
     <h3 class="box-title">Note</h3>
  </div>
  <div class="box-body">
    <?php foreach($note_types as $type) { ?>
    <div id="note_id_<?=$type['note_id']?>">
      <label class="col-sm-12">
      <?=$type['note_name']?>
      <?php if (in_array($user_group_id,$group_edit)&&$type['note_id'] == 1 && $plan_info['is_fix']>0) {?>
        <a onclick="confirmRemoveFix();" class="btn btn-danger btn-sm pull-right">Hủy bỏ Yêu cầu khắc phục</a>
      <?php } ?>
      </label>
      <?php if(!empty($notes[$type['note_id']])) { 
      if ($type['note_id'] != 1) {
      foreach($notes[$type['note_id']] as $note) { ?>
      <div id="qc_note_id_<?=$note['qc_note_id']?>" class="col-sm-12 form-group clearfix ">
        <div class="input-group">
          <span class="input-group-addon" style="width: 15%"><i class="fa fa-comment-o"> <?=$note['fullname']?></i></span>
          <div class="form-control"><?=$note['comment']?></div>
          <span class="input-group-addon"><i class="fa fa-clock-o"> <?=$note['date_added']?></i></span>
          <?php if($hasDel) {?>
          <a class="input-group-addon del-note" style="color: red" ><i class="fa fa-trash"></i></a>
          <?php } ?> 
        </div>
      </div>
      <?php }} else { 
      foreach($notes[$type['note_id']] as $note) { ?>
      <div id="qc_note_id_<?=$note['qc_note_id']?>" class="form-group clearfix">
        <div class="col-sm-7">
          <div class="input-group">
            <span class="input-group-addon" style="width: 15%"><i class="fa fa-comment-o"> <?=$note['fullname']?></i></span>
            <div class="form-control"><?=$note['comment']?></div>
            <span class="input-group-addon"><i class="fa fa-clock-o"> <?=$note['date_added']?></i></span>
          </div>
        </div>
        <div class="col-sm-5">
           <div class="input-group">
              <div class="form-control"><?=$note['date_approved']?></div>
              
            <?php if(in_array($user_group_id,$group_edit)){  ?>
              <a onclick="updateFixed('<?=$note['qc_note_id']?>','1');" class="input-group-addon <?=$note['date_approved'] != '0000-00-00 00:00:00' ? 'text-success' : 'text-error' ?>">
              <i class="fa fa-check"></i> Xác nhận đã KP
              </a>
              <a onclick="updateFixed('<?=$note['qc_note_id']?>','0');" class="input-group-addon text-error">
              <i class="fa fa-times"></i> Reset
              </a>
            <?php } ?>
              
           </div>
           <!--//input-group-->
        </div>
      </div>
      <?php }}} ?>
    </div>
    <?php } ?>
  </div>
  <?php if($hasEdit) { ?>
  <div class="box-footer clearfix" id="status_note">
   <div class="col-sm-4 message">
   </div>
   <div class="col-sm-8 pull-right">
      <div class="input-group btn-block">
         <div class="input-group-btn">
            <select id="note-select" name="note_id" class="form-control" style="min-width:200px;">
              <option value="0"> --- Chọn --- </option>
              <?php if($user_group_id == 9) {?>
                <option value="4">Sup note</option>
                <option value="8">Note App - ghi rõ lỗi</option>
              <?php } else { ?>
                <?php foreach($note_types as $type){ ?>
                <option value="<?=$type['note_id']?>"><?=$type['note_name']?></option>
                <?php } ?>
              <?php } ?>
            </select>
         </div>
         <input type="text" id="note-comment" name="comment" class="form-control" placeholder="Ghi chú">
         <span class="input-group-btn">
         <button type="button" id="button-note" class="btn btn-primary" data-loading-text="Đang tải..."><i class="fa fa-save"></i> Cập nhật</button>
         </span>
      </div>
   </div>
  </div>
  <?php } ?>
</div>


<!-- <div id="qc_note_id_0" class="col-sm-12 form-group clearfix" style="display: none">
  <div class="input-group">
    <span class="input-group-addon" style="width: 15%"><i class="fa fa-comment-o">username</i></span>
    <div class="form-control">comment</div>
    <span class="input-group-addon"><i class="fa fa-clock-o">date_added</i></span>
    <?php if($hasDel) {?>
    <a class="input-group-addon del-note" style="color: red"><i class="fa fa-trash"></i></a> 
    <?php } ?>
  </div>
</div> -->

