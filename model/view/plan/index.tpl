<style>
   .tr_info.is_read .store_title{
   color:#800000;
   }
   .text-grey{color:#666;}
   .red{color:#f00;}
   .blur{color:#999;}
   .orange{color:#F60;}
   .table-list thead tr td{ font-weight:600;}
   .t_map td{position:relative;}
   .t_map .btn{
   z-index:999;
   position:absolute;
   right:10px;
   top:10px;
   }
   .box-map #bodymap .btn{display:none;}
   #bodymap{ }
   #bodymap.mopen{}
   .table .table>thead>tr>th,
   .table .table>tbody>tr>th,
   .table .table>tfoot>tr>th,
   .table .table>thead>tr>td,
   .table .table>tbody>tr>td,
   .table .table>tfoot>tr>td {
   border-top: none !important;
   font-weight: 500;
   }
   .table-responsive table  table{
   margin-bottom: 20px;
   }
   .btn-xs {
   padding: 1px 2px;
   font-size: 8px;
   line-height: 1;
   border-radius: 3px;
   }
   .color-red{color:#F00;}
  .menu-goo-open:checked ~ .menu-goo-item:nth-child(3) {
    -webkit-transform: translate3d(-70.425px, 34.481px, 0) !important;
    transform: translate3d(-70.425px, 34.481px, 0) !important;
  }
  .menu-goo-open:checked ~ .menu-goo-item:nth-child(4) {
    -webkit-transform: translate3d(-64.185px, -23.249px, 0) !important;
    transform: translate3d(-64.185px, -23.249px, 0) !important;
  }
  .menu-goo-open:checked ~ .menu-goo-item:nth-child(5) {
    -webkit-transform: translate3d(-20.908px, -65px, 0) !important;
    transform: translate3d(-20.908px, -65px, 0) !important;
  }
  .menu-goo-open:checked ~ .menu-goo-item:nth-child(6) {
    -webkit-transform: translate3d(35.04956px, -42.372px, 0) !important;
    transform: translate3d(35.04956px, -42.372px, 0) !important;
  }
  .menu-goo {
    right: 28px;
  }
</style>
<div class="container-fluid" style="margin-top: 20px">
  <?php if (!empty($success)) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if (!empty($error_warning)) { ?>
  <div class="alert alert-danger"><i class="fa fa-check-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
</div>
<div class="container-fluid" id="filter-area">
  <div class="box box-info">
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label" for="input-filter_global"><?php echo $text_filter; ?></label>
            <input type="text" name="filter_global" value="" placeholder="<?php echo $text_filter; ?>" id="input-filter_global" class="form-control filter" onchange="javascript:filter()"/>
          </div>
        </div>
        <!-- round name-->
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label"><?=$text_month?></label>
            <select name="filter_round_name" class="form-control filter" onchange="filter()">
               <option value="*"> --- Chọn --- </option>
               <?php foreach($rounds as $r => $total) { ?>
               <option value="<?=$r?>"><?=$r?> (<?=$total?>)</option>
               <?php } ?>
            </select>
          </div>
        </div>
        
        <!-- province -->
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label"><?=$text_province?></label>
            <select name="filter_province_id" class="form-control chosen filter" onchange="filter()">
               <option value="*"> --- Chọn --- </option>
               <?php foreach($provinces as $p) { 
                  $count = !empty($count_province[$p['province_id']]) ? $count_province[$p['province_id']] : 0;
                  
                  ?>
               <option  value="<?=$p['province_id']?>"><?=$p['name']?> (<?=$count?>)</option>
               <?php } ?>
            </select>
          </div>
        </div>
        <?php if($isUser) {?>
        <!-- staff -->
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label"><?=$text_staff_user?></label>
            <select name="filter_user_id" class="form-control chosen filter" onchange="filter()">
               <option value="*"> --- Chọn --- </option>
               <?php foreach($staffs as $s) { 
                  $count = !empty($count_staff[$s['user_id']]) ? $count_staff[$s['user_id']] : 0;
                  
                  ?>
               <option value="<?=$s['user_id']?>"><?=$s['username'].' - '.$s['fullname']?> (<?=$count?>)</option>
               <?php } ?>
            </select>
          </div>
        </div>
        <!-- plan status -->
        <div class="col-sm-3">
           <div class="form-group">
              <label class="control-label"><?=$text_plan_status?></label>
              <select name="filter_plan_status" class="form-control filter" onchange="filter()">
                 <option value="*"> --- Chọn --- </option>
                 <option value="0"><?=$text_no_perform?> (<?=!empty($count_status[0]) ? $count_status[0] : 0?>)</option>
                 <option value="2">Đang thực hiện (<?=!empty($count_status[2]) ? $count_status[2] : 0?>)</option>
                 <option value="1"><?=$text_has_perform?> (<?=!empty($count_status[1]) ? $count_status[1] : 0?>)</option>
              </select>
           </div>
        </div>
        <!--plan QC -->
        <div class="col-sm-3">
           <div class="form-group">
              <label class="control-label"><?=$text_qc_status?></label>
              <select name="filter_plan_qc" class="form-control filter" onchange="filter()" autocomplete="off">
                 <option value="*"> --- Chọn --- </option>
                 <option value="1"><?=$text_has_qc?> (<?=!empty($count_qc[1]) ? $count_qc[1] : 0?>)</option>
                 <option value="0"><?=$text_no_qc?> (<?=!empty($count_qc[0]) ? $count_qc[0] : 0?>)</option>
              </select>
           </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label" for="filter_is_fix">Yêu cầu khắc phục</label>
            <select name="filter_is_fix" id="filter_is_fix" class="form-control filter" onchange="filter()" >
              <option value="*"> --- Chọn --- </option>
              <option value="0">Không </option>
              <option value="1">Có - chưa KP </option>
              <option value="2">Có - đã KP chưa xác nhận </option>
              <option value="3">Có - đã xác nhận KP </option>
            </select>
          </div>
        </div>
        <?php } ?>
        <!-- upload time-->
        <div class="col-sm-3">
           <div class="form-group">
              <label class="control-label" for="input-filter_date"><?=$text_date?></label>
              <div class="input-group">
                 <input type="text" id="filter_date" value="" name="filter_date" placeholder="<?=$text_date?>" class="form-control datepicker filter" onchange="javascript:filter()" autocomplete="off">
                 <a class="input-group-addon" onclick="$('input[name=\'filter_date\']').val('<?=date('Y-m-d')?>');javascript:filter();">
                 <small>Hôm nay</small>
                 </a>
              </div>
           </div>
        </div>
      </div> 
      <div class="row">
        <div class="col-sm-12">
          <button type="button" id="button-clear" class="btn btn-primary btn-sm pull-right" autocomplete="off"><i class="fa fa-eraser"></i><?=$button_clear?></button>
        </div>
      </div>
      <?php if($isAdmin) { ?>
      <div class="row">
         <div class="col-sm-2">
            <div class="form-group">
               <label class="control-label">Version</label>
               <select name="filter_version" class="form-control filter" onchange="filter()" autocomplete="off">
                  <option value="*"> --- Chọn --- </option>
                  <?php foreach($versions as $v => $total) { 
                     if (!empty($v)) {
                     
                     ?>
                  <option value="<?=$v?>"><?=$v?> (<?=$total?>)</option>
                  <?php }} ?>
               </select>
            </div>
         </div>
         <div class="col-sm-2">
            <div class="form-group">
               <label class="control-label">Lần upload</label>
               <select name="filter_upload_count" class="form-control filter" onchange="filter()" autocomplete="off">
                  <option value="*"> --- Chọn --- </option>
                  <?php foreach($upload_count as $v => $total) { ?>
                  <option value="<?=$v?>"><?=$v?> (<?=$total?>)</option>
                  <?php } ?>
               </select>
            </div>
         </div>
      </div>
      <?php } ?>
      <div class="row">
        <?php if(!empty($import)) { ?>
        <div class="col-sm-12">
          <?=$import;?>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div id="ajax_list"></div>
<div id="modal-confirm" class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            Bạn có chắc chắn muốn xóa Plan <span></span>?
         </div>
         <div class="modal-body">
            <div class="mpadding">
               Plan này không thể khôi phục nhưng lịch sử sẽ được lưu lại.
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-success pull-right" data-dismiss="modal" aria-hidden="true">Không</button>
            <a id="btn-modal-confirm" class="btn btn-danger pull-right" style="margin-right:15px;">Xác nhận Xóa</a>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
   $(document).ready(function(){
   
     var url = '<?=str_replace("amp;","",$url)?>';
   
     $('#ajax_list').load(url);
   
     <?php if(!empty($filter_global)){?>
   
     $("td span:contains(<?php echo $filter_global;?>)").css({"color":"red","background":"yellow"});
   
     <?php } ?>
   
   });
   
   function deletePlan(plan_id,store_name){
   
       $('#modal-confirm .modal-header span').html(store_name);
   
       $('#modal-confirm').modal('show');
   
       $('#btn-modal-confirm').click(function(event) {
   
         event.preventDefault();
   
         $('#btn-modal-confirm').unbind('click');
   
         $.get('index.php?route=plan/plan/delete&plan_id='+plan_id, function(data){
   
           $('#modal-confirm').modal('hide');
   
           $('#plan_id_'+plan_id).remove();
   
         })
   
       });
   
   }
   
   
   
   $('input[name=\'filter_global\']').autocomplete({
   
     'source': function(request, response) {
   
       $.ajax({
   
         url: 'index.php?route=plan/plan/autocomplete&filter_global=' +  encodeURIComponent(request),
   
         dataType: 'json',
   
         success: function(json) {
   
           response($.map(json, function(item) {
   
             return {
   
               label: item['store_code'] + ' - ' + item['store_name'],
               value: item['plan_id'],
                store_code: item['store_code']
             }
   
           }));
   
         },
   
       });
   
     },
   
     'select': function(item) {
        console.log(item);
       $('input[name=\'filter_global\']').val(item['store_code']);
        filter();
     }
   
   });
   
</script>