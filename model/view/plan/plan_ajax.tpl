
        <div class="scrolldiv">
        <div class="table-responsive" style="background: #fff;">
          <table class="table table-bordered">
         
            <tbody id="tr_place">
            <?php if ($plans) { ?>
            <?php foreach ($plans as $plan) {?>
            <?php if(!in_array($user_id,$plan['users_read'])){?>
              <tr class="tr_info ">
                <td class="text-left" style="vertical-align:top">
                  <a href="<?php echo $plan['edit']; ?>" title="<?php echo $plan['plan_name']; ?>" class="store_title"><strong><?php echo $plan['store_name']; ?></strong></a> <span class="blur">- <?php echo $plan['store_code']; ?></span><br />
                 
                  <a href="<?php echo $plan['edit']; ?>" class="text-center clearfix" style="display:block;"><img width="180" src="<?php echo $plan['store_image']; ?>" /></a>
                  
                  
                
                  <span class="blur"><?php echo $plan['address_raw']; ?></span><br /> 
                  <?php echo $plan['fullname']; ?>
               
                </td>
               
              </tr>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row" style="margin-top:10px;">
        <div class="col-sm-12 text-center"><?php echo str_replace('class="pagination"','class="pagination pagination-sm"',$pagination); ?></div>
      </div>
  <script type="text/javascript">
   $(".control-sidebar .scrolldiv").slimscroll({
		height: ($(window).height() - 50) + "px",
		color: "rgba(0,0,0,0.2)",
		size: "7px"
	  });
  </script>