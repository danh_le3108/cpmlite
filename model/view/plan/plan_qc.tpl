<form method="post" action="index.php?route=plan/plan_qc/save&plan_id=<?=$plan_id?>">
  <div class="box box-info" id="plan_qc">
    <div class="box-header">
      <h3 class="box-title">QC Note</h3>
    </div>
    <div class="box-body">
       <div class="clearfix">      
          <?php
             foreach($code_groups as $code){
             if(!empty($code['problem'])){?>
          <div class="col-sm-4" style="margin-bottom:15px;">
             <div class="button-group">
                <button type="button" class="btn btn-default btn-sm dropdown-toggle btn-group-justified" data-toggle="dropdown"><?php echo $code['code_name'];?>
                <?php
                   $i = 0;
                        foreach ($code['problem'] as $problem) { 
                         if (in_array($problem['problem_id'], $plan_codes)) { 
                         $i++;
                          } 
                        } 
                        ?>
                <span class="badge"><?php echo $i;?></span> <span class="caret"></span>
                <?php  $i = 0; ?>
                </button>
                <ul class="dropdown-menu">
                   <?php foreach ($code['problem'] as $problem) { ?>
                   <li><a><label>
                      <input type="checkbox" name="codes[<?php echo $problem['problem_id']; ?>]" value="<?php echo $problem['problem_id']; ?>" <?php if (in_array($problem['problem_id'], $plan_codes)) { ?>checked="checked"<?php } ?>/> <?php echo $problem['problem_id']; ?> -  <?php echo $problem['problem_name']; ?></label></a>
                   </li>
                   <?php } ?>
                </ul>
             </div>
          </div>
          <?php } ?>
          <?php }   ?>
       </div>
    </div>
    <?php if($hasEdit) {?>
    <div class="box-footer">
      <div class="col-sm-8 message">
      </div>
      <div class="col-sm-4">
        <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Lưu</button>
      </div>
    </div>
    <?php } ?>
  </div>
</form>