<div class="row">
<div class="col-md-12" id="posm">
  <form method="post" action="index.php?route=plan/plan_survey/save&plan_id=<?=$plan['plan_id']?>" id="plan_survey">
    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title">Nhâp liệu</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="box-body">
        <div class="col-md-6">
          <h5 class="text-success"><?=$attrs1[0]['group_name']?></h5>
          <table class="table">
          <?php foreach($attrs1 as $attr) { ?>
             <tr>
              <td><?=$attr['attribute_name']?></td>
              <td>
                <?php if ($attr['input'] == 'select') { 
                  $attr['options'] = json_decode($attr['options']);
                ?>
                <select id="<?=$attr['attribute_code']?>" class="form-control" name="attrs[<?=$attr['attribute_id']?>]">
                  <option value=""> --- Chọn --- </option>
                  <?php foreach($attr['options'] as $key => $op) {
                   ?>
                  <option value="<?=$op->id?>" <?=!empty($attr_data[0][$attr['attribute_id']]) && $op->id == $attr_data[0][$attr['attribute_id']] ? 'selected' : ''?>><?=$op->name?></option>
                  <?php } ?>
                </select>
                <?php } elseif ($attr['input'] == 'multiselect') { 
                  $options = json_decode($attr['options']);
                  foreach ($options as $op) {
                ?>
                  <div class="col-sm-6">
                    <input type="checkbox" name="attrs[<?=$attr['attribute_id']?>]" value="<?=$op->id?>" <?=!empty($attr_data[0][$attr['attribute_id']]) && $op->id == $attr_data[0][$attr['attribute_id']] ? 'checked' : ''?>> <?=$op->name?>
                  </div>
                <?php }} else { ?>
                <input id="<?=$attr['attribute_code']?>" type="text" class="form-control" name="attrs[<?=$attr['attribute_id']?>]" value="<?=!empty($attr_data[0][$attr['attribute_id']]) ? $attr_data[0][$attr['attribute_id']] : '' ?>">
                <?php } ?>
              </td>
            </tr>
          <?php } ?>
          </table>
        </div>
        <div class="col-md-6">
          <?php $attr_array = array($attrs2,$attrs3); 
          foreach ($attr_array as $attrs) { ?>
          <h5 class="text-success"><?=$attrs[0]['group_name']?></h5>
          <table class="table">
            <?php foreach($attrs as $attr) { ?>
            <tr>
              <td><?=$attr['attribute_name']?></td>
              <td>
                <?php if ($attr['input'] == 'select') { 
                  $attr['options'] = json_decode($attr['options']);
                ?>
                <select id="<?=$attr['attribute_code']?>" class="form-control" name="attrs[<?=$attr['attribute_id']?>]">
                  <option value=""> --- Chọn --- </option>
                  <?php foreach($attr['options'] as $key => $op) {
                   ?>
                  <option value="<?=$op->id?>" <?=!empty($attr_data[0][$attr['attribute_id']]) && $op->id == $attr_data[0][$attr['attribute_id']] ? 'selected' : ''?>><?=$op->name?></option>
                  <?php } ?>
                </select>
                <?php } elseif ($attr['input'] == 'multiselect') { 
                  $options = json_decode($attr['options']);
                  $attr_ids = !empty($attr_data[0][$attr['attribute_id']]) ? explode(',',$attr_data[0][$attr['attribute_id']]) : array();
                  foreach ($options as $op) {
                ?>
                  <div class="col-sm-6">
                    <input type="checkbox" name="attrs[<?=$attr['attribute_id']?>][]" value="<?=$op->id?>" <?=in_array($op->id,$attr_ids) ? 'checked' : ''?>> <?=$op->name?>
                  </div>
                <?php }} else { ?>
                <input id="<?=$attr['attribute_code']?>" type="text" class="form-control" name="attrs[<?=$attr['attribute_id']?>]" value="<?=!empty($attr_data[0][$attr['attribute_id']]) ? $attr_data[0][$attr['attribute_id']] : '' ?>">
                <?php } ?>
              </td>
            </tr>
            <?php } ?>
          </table>
          <?php } ?>
        </div>
      </div>
      <?php if($hasEdit) { ?>
      <div class="box-footer clearfix">
        <div class="col-sm-8  message">
          
        </div>
        <div class="col-sm-4">
          <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Lưu</button>
        </div>
      </div>
      <?php } ?>
    </div>
  </form>
</div>

<div class="col-md-12">
  <div class="box box-info">
    <div class="box-header">
      <h3 class="box-title">TRƯNG BÀY</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <?php 
      $attr_array = array($attrs5,$attrs6); 
      $attr_array2 = array($attrs7,$attrs8); 
      $status = array('','Đạt','Rớt');
      $status_color = array('','green','red');
      for($i=1;$i<=$cctb;$i++) { ?>
      <form method="post" action="index.php?route=plan/plan_survey/save&plan_id=<?=$plan['plan_id']?>&item_number=<?=$i?>">
        <div class="box box-info box-solid" style="border:none">
          <div class="box-header">
            <h3 class="box-title">TỦ / KỆ <?=$i?></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="box-body">
            <?php foreach($attr_array as $attrs) { ?>
            <div class="col-md-6">
              <h4 class="text-danger"><?=$attrs[0]['group_name']?></h4>
              <h5>Hình ảnh</h5>
              <div class="box-body thumbnails display_image">
                <?php $image_type_id = $attrs[0]['group_id'] == 10 ? 67 : 68; ?>
                <div class="row" id="item_number_<?=$i?>_<?=$image_type_id?>">
                  <? foreach ($images[$i][$image_type_id] as $img) { ?>
                  <div class="col-sm-4" id="img_<?=$img['image_id']?>">
                    <div class="thumbnail">
                      <?php if($has_edit_image) { ?>
                      <button type="button" class="btn btn-success icon btn-rotate" data-toggle="popover" title="Xoay" style="top: 4px;left: 4px" autocomplete="off"><i class="fa fa-refresh"></i></button>
                      <?php } ?>
                      <?php if($img['manual']) { ?>
                      <button type="button" class="btn btn-primary icon btn-manual" style="bottom: 4px;left: 4px;display: block;" title="Hình upload thủ công" autocomplete="off"><i class="fa fa-hand-paper-o"></i></button>
                      <?php } ?>
                      <?php if($has_del_image) { ?>
                      <button type="button" class="btn btn-danger icon btn-del-img" style="right: 4px" autocomplete="off"><i class="fa fa-remove"></i></button>
                      <?php } ?>
                      <a href="<?=$img['popup']?>"  class="img">
                        <img src="<?=$img['thumb']?>" alt="">
                      </a>
                    </div>
                  </div>  
                  <?php } ?>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <?php if ($has_add_image) { ?>
                    <button type="button" class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#modal-upload" data-item_number="<?=$i?>" data-image_type_id="<?=$image_type_id?>"><i class="fa fa-upload"></i> Upload hình</button>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <table class="table">
              <?php foreach($attrs as $attr) { ?>
              <tr>
                <td><?=$attr['attribute_name']?></td>
                <td>
                  <?php if ($attr['input'] == 'select') { 
                    $attr['options'] = json_decode($attr['options']);
                  ?>
                  <select id="<?=$attr['attribute_code']?>" class="form-control" name="attrs[<?=$attr['attribute_id']?>]">
                    <option value=""> --- Chọn --- </option>
                    <?php foreach($attr['options'] as $key => $op) {
                     ?>
                    <option value="<?=$op->id?>" <?=!empty($attr_data[$i][$attr['attribute_id']]) && $op->id == $attr_data[$i][$attr['attribute_id']] ? 'selected' : ''?>><?=$op->name?></option>
                    <?php } ?>
                  </select>
                  <?php } else { ?>
                  <input id="<?=$attr['attribute_code']?>" type="text" class="form-control" name="attrs[<?=$attr['attribute_id']?>]" value="<?=!empty($attr_data[$i][$attr['attribute_id']]) ? $attr_data[$i][$attr['attribute_id']] : '' ?>">
                  <?php } ?>
                </td>
              </tr>
              <?php } ?>
              </table>
            </div>
            <?php } ?>
          </div>
          <?php if($hasEdit) { ?>
          <div class="box-footer clearfix">
            <div class="col-sm-8  message">
              
            </div>
            <div class="col-sm-4">
              <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Lưu</button>
            </div>
          </div>
          <?php } ?>
          <div class="box-body">
             <?php foreach($attr_array2 as $attrs) { ?>
            <div class="col-md-6">
              <h4 class="text-danger" style="padding:10px 5px; color: #fff;background: #87ceeb;background-color: #87ceeb;"><?=$attrs[0]['group_name']?></h4>
              <table class="table">
                <?php foreach($attrs as $attr_2) { ?>
                <tr>
                  <td><?=$attr_2['attribute_name']?></td>
                  <td>
                    <?php 
                    if (!empty($attr_data[$i][$attr_2['attribute_id']])) {
                      $value = $attr_2['input'] == 'text' ? $status[(int)$attr_data[$i][$attr_2['attribute_id']]] : $attr_data[$i][$attr_2['attribute_id']].' %'; ?>
                    <span style="color: <?=$status_color[(int)$attr_data[$i][$attr_2['attribute_id']]]?>"><?=$value?></span>
                    <?php } ?>
                  </td>
                </tr>
                <?php } ?>
              </table>
            </div>
            <?php } ?>
          </div>
        </div>
      </form>
      <?php } ?>
    </div>
  </div>
</div>


<div class="col-md-12">
  <form method="post" action="index.php?route=plan/plan_survey/save&plan_id=<?=$plan['plan_id']?>">
    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title">KHẢO SÁT</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="box-body">
        <?php foreach ($attrs4 as $group_name => $attrs) { ?>
        <div class="col-md-6">
          
        <h5 class="text-success"><?=$attrs[0]['group_name']?></h5>
        <table class="table">
          <?php 
          $group_name = '';
          foreach($attrs as $attr) { 
          ?>
          <tr>
            <td><?=$attr['attribute_name']?></td>
            <td>
              <?php if ($attr['input'] == 'select') { 
                $attr['options'] = json_decode($attr['options']);
              ?>
              <select id="<?=$attr['attribute_code']?>" class="form-control" name="attrs[<?=$attr['attribute_id']?>]">
                <option value=""> --- Chọn --- </option>
                <?php foreach($attr['options'] as $key => $op) {
                 ?>
                <option value="<?=$op->id?>" <?=!empty($attr_data[0][$attr['attribute_id']]) && $op->id == $attr_data[0][$attr['attribute_id']] ? 'selected' : ''?>><?=$op->name?></option>
                <?php } ?>
              </select>
              <?php } else { ?>
              <input id="<?=$attr['attribute_code']?>" type="text" class="form-control" name="attrs[<?=$attr['attribute_id']?>]" value="<?=!empty($attr_data[0][$attr['attribute_id']]) ? $attr_data[0][$attr['attribute_id']] : '' ?>">
              <?php } ?>
            </td>
          </tr>
          <?php } ?>
        </table>
        </div>
        <?php } ?>
      </div>
      <?php if($hasEdit) { ?>
      <div class="box-footer clearfix">
        <div class="col-sm-8  message">
          
        </div>
        <div class="col-sm-4">
          <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Lưu</button>
        </div>
      </div>
      <?php } ?>
    </div>
  </form>
  </div>
</div>

<script type="text/javascript">
  $('.display_image').popover({
    html: true,
    placement: 'left',
    selector: '[data-toggle="popover"]',
    title: 'Xoay',
    trigger: 'focus',
    content: function() {
      var img_id = $(this).closest('.col-sm-4').attr('id').replace('img_','');
      var popover = '<a type="button" title="-90" data-toggle="tooltip" class="btn btn-primary btn-xs" onclick="rotate('+img_id+','+"-90"+')"><i class="fa fa-rotate-left"></i></a>';
      popover += '<a type="button" title="90" data-toggle="tooltip" class="btn btn-primary btn-xs" onclick="rotate('+img_id+','+"90"+')"><i class="fa fa-rotate-right"></i></a>';
      popover += '<a type="button" title="180" data-toggle="tooltip" class="btn btn-primary btn-xs" onclick="rotate('+img_id+','+"180"+')"><i class="fa fa-arrow-up"></i></a>';
      return popover;
    }
  });

  $('.display_image').on('click', '.btn-del-img', function() {
    var img_cur = $(this);
    $('#btn-confirm-delete').unbind('click');
    $('#modal-confirm-delete').modal('show');
    $('#btn-confirm-delete').click(function(event) {
        event.preventDefault();
        var img_id = img_cur.closest('.col-sm-4').attr('id').replace('img_','');
        $.get("index.php?route=plan/plan_image_audio/delete_image&plan_id=<?=$plan_id?>&image_id="+img_id, function(data) {
          $('#img_'+img_id).remove();
          $('#modal-confirm-delete').modal('hide');
        })
    });

  })
</script>
