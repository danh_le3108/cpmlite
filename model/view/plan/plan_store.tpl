<style type="text/css">
  .chosen-container-multi .chosen-choices {
    min-height: 34px !important;
    padding: 3px 10px;
  }
  .chosen-container-multi .chosen-results .g_option {
    font-weight: 600;
  }
  .chosen-container-multi .chosen-results .s_option {
    margin-left: 10px;
  }
  .chosen-container-multi .search-choice {
    width: 100%;
    background-color: #fff !important;
    border: none !important; 
  }
</style>
<form method="post" action="index.php?route=plan/plan_store/save&plan_id=<?=$plan['plan_id']?>" id="plan_store">
  <div class="box box-info">
    <div class="box-header">
      <h3 class="box-title">Plan #<?=$plan['plan_id']?> - <?=$plan['plan_name']?></h3>
    </div>
    <div class="box-body">
      <table class="table">
        <?php if($is_user) { ?>
        <tr>
          <td width="30%">Overview</td>
          <td class="thumbnails">
            <div class="thumbnail" id="overview" style="max-width: 110px">
              <a class="img" href="<?=$plan['popup']?>"><img src="<?=$plan['thumb']?>"/></a>
            </div>
          </td>
        </tr>
        <tr>
          <td>SUP</td>
          <td>
            <?=$sup['usercode']?> - <?=$sup['fullname']?>
          </td>
        </tr>
        <tr>
          <td>Nhân viên KS</td>
          <td>
            <?=$plan['usercode']?>
          </td>
        </tr>
        <tr>
          <td>Trạng thái</td>
          <td >
            <select class="form-control" name="plan_status">
              <option value="0" >Chưa thực hiện</option>
              <option value="2" <?=$plan['plan_status'] == 2 ? 'selected' : ''?>>Đang thực hiện</option>
              <option value="1" <?=$plan['plan_status'] == 1 ? 'selected' : ''?>>Đã thực hiện</option>
            </select>
          </td>
        </tr>
        <tr>
          <td>Tọa độ Checkin</td>
          <td>
            <div class="row">
              <div class="col-md-6" style="">
                <small style="color: red">Vĩ độ</small>
                <input type="text" class="form-control" name="latitude" value="<?=$plan['latitude']?>">
              </div>
              <div class="col-md-6">
                <small style="color: red">Kinh độ</small>
                <input type="text" class="form-control" name="longitude" value="<?=$plan['longitude']?>">
              </div>
            </div>
            <br>
            <a target="_blank" href="<?='https://www.google.com/maps/search/'.$plan['latitude'].','.$plan['longitude'].'/@'.$plan['latitude'].','.$plan['longitude'].',17z'?>"><?=$plan['latitude'].', '.$plan['longitude']?></a>
            <br/> <a class="btn btn-xs btn-<?php echo ($plan['is_verify']==1)?'success':'danger';?>" onclick="verifyLocation()"><i class="fa <?php echo ($plan['is_verify']==1)?'fa-check-circle':'fa-times-circle';?>"></i> Xác thực tọa độ</a>
          </td>
        </tr>
        <tr>
          <td>Ngày Checkin</td>
          <td><input type="text" name="time_checkin" value="<?=$plan['time_checkin'] == '0000-00-00 00:00:00' ? '' : $plan['time_checkin']?>" class="form-control datetime" data-date-format="YYYY-MM-DD HH:mm:ss" autocomplete="off"></td>
        </tr>
        <tr>
          <td>Ngày Upload</td>
          <td><input type="text" name="time_upload" value="<?=$plan['time_upload']=='0000-00-00 00:00:00' ? '' : $plan['time_upload']?>" class="form-control datetime" data-date-format="YYYY-MM-DD HH:mm:ss" autocomplete="off">
              <br>
            Device: <?=$plan['model']?> - <span style="color: red">App version</span>: <?=$plan['version']?>
          </td>
          <!-- <td>Device:<br>App version:</td>
          <td><?=$plan['model']?><br><?=$plan['version']?></td> -->
        </tr>
        <?php } else {?>
        <tr>
          <td>Ngày thực hiện</td>
          <td><?=$plan['time_upload']=='0000-00-00 00:00:00' ? '' : date('d-m-Y',strtotime($plan['time_upload']))?></td>
        </tr>
        <?php } ?>
        <tr>
          <td>Ghi chú thành công</td>
          <td colspan="3">
            <select class="form-control" name="problem">
              <option value="0"> --- </option>
              <?php foreach($problems as  $p) { ?>
                <option value="<?=$p['reason_id']?>" <?=$plan['problem'] == $p['reason_id'] ? 'selected' : ''?>><?=$p['reason_name']?></option>
              <?php } ?>
            </select>
          </td>
        </tr>
        <tr>
          <td>Ghi chú thành công khác</td>
          <td colspan="3"><input type="text" name="note" value="<?=$plan['note']?>" class="form-control"></td>
        </tr>
        <tr>
          <td>Khách hàng Phản hồi</td>
          <td colspan="3"><input type="text" name="feedback" value="<?=$plan['feedback']?>" class="form-control">
          </td>
        </tr>
      </table> 
    </div>

    <div class="box-header">
      <h3 class="box-title" style="color: red">Trường hợp đặt biệt</h3>
    </div>
    <div class="box-body">
      <table class="table">
        <tr>
          <td width="30%" style="color: red">Lý do</td>
          <td colspan="3">
            <select data-placeholder=" --- " class="form-control chosen" multiple name="reason_id[]" id="abc">
            <option value="0" > --- </option>
              <?php 
              $reason_ids = explode(',',$plan['reason_id']);
              foreach($reasons as $s) { ?>
                <option class="g_option" value="<?=$s['reason_id']?>" <?=in_array($s['reason_id'], $reason_ids)  ? 'selected' : ''?>><?=$s['code']?> - <?=$s['reason_name']?></option>
                <?php if ($s['childs']) { 
                 foreach($s['childs'] as $c) {
                ?>
                 <option class="s_option" value="<?=$c['reason_id']?>" <?=in_array($c['reason_id'], $reason_ids)  ? 'selected' : ''?>><?=$c['code']?> - <?=$c['reason_name']?></option>
              <?php }}} ?>
            </select> 
          </td>
        </tr>
        <tr>
          <td style="color: red">Ghi chú khác</td>
          <td colspan="3"> 
            <input type="text" name="note_ktc" value="<?=$plan['note_ktc']?>" class="form-control">
          </td>
        </tr>
      </table>
    </div>      
    <?php if($hasEdit) { ?>
    <div class="box-footer clearfix">
      <div class="col-sm-8  message">
        
      </div>
      <div class="col-sm-4">
        <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Lưu</button>
      </div>
    </div>
    <?php } ?>
  </div>
</form>


<div id="compare-image" class="modal fade in">
  
</div>
<script type="text/javascript">
  $('#btn_search_image').click(function(){
    $.get('index.php?route=tool/search_image/compare&filter_store_code=<?=$plan['store_code']?>', function(data){
      $('#compare-image').html(data)
      $('#compare-image').modal('show');
    })
  });
  
function verifyLocation() {
		$.ajax({
			url: 'index.php?route=plan/plan_store/verify&plan_id=<?php echo $plan['plan_id'];?>',
			dataType: 'json',
			type: 'post',
			data: 'plan_id=<?php echo $plan['plan_id'];?>',
			success: function(json) {
				if (json['success']) {
					$('#plan_store').load('index.php?route=plan/plan_store/info&plan_id=<?php echo $plan['plan_id'];?>');
				}
			},
		});
}
</script>