<?php echo $header;?>
<style>
  #content{position: absolute;top:50px;left:0;right:0;bottom:0;}
  .sidebar-open #content{left:200px;}
  #content>.wrap{position:relative; width:100%; height:100%;}
  #map {
	  min-height:500;
	  width: 100%;
	  height:100%;
	  position: absolute;
	  top:0px;left:0;right:0;bottom:0;
  }
  .map-info {
	  background-color: #000;
	  color: #fff;
	  min-height: 122px;
	  opacity: 0.7;
	  position: absolute;
	  right: 0;
	  top: 0;
	  width: 150px;
  	z-index: 9990;
  }
  .map-info>div {
	  display: block;
	  line-height: 25px;
	  margin: 5px 0;
	  overflow: hidden;
	  padding-bottom: 5px;
	  padding-left: 50px;
	  padding-top: 5px;
	  width: 100%;
  }
  .map-info div.icon-green {
  background: rgba(0, 0, 0, 0) url("<?php echo $http_server; ?>assets/image/marker/green.png") no-repeat scroll 0 0;
  }
  .map-info div.icon-red {
  background: rgba(0, 0, 0, 0) url("<?php echo $http_server; ?>assets/image/marker/red.png") no-repeat scroll 0 0;
  }
  .map-info div.icon-yellow {
  background: rgba(0, 0, 0, 0) url("<?php echo $http_server; ?>assets/image/marker/grey.png") no-repeat scroll 0 0;
  }
  #filter-area {
	  background-color: #000;
	  color: #fff;
	  opacity: 0.8;
	  position: absolute;
	  z-index: 99;
	  top: 0;
	  left: 120px;
	  right: 160px;
	  padding: 5px 0 10px;
  }
  #filter-area input, #filter-area select  { opacity: 1; color:#000000; }
  #filter-area .form-group { margin-bottom: 5px;}
  #filter-area .form-control,
  #filter-area .chosen-container .chosen-selection--single {
	  height: 28px;
	  font-size: 14px;
	  padding: 0 10px;
  }
  #map-container h4.map-title {
	  font-family: 'Open Sans Condensed', sans-serif;
	  font-size: 22px;
	  font-weight: 400;
	  padding: 10px;
	  background-color: #48b5e9;
	  color: white;
	  margin: 0;
	  width: 100%;
	  border-radius: 0;
  }
  #map-container #map-content {
  	padding: 10px;
  }
  .gm-style-iw {
	  width: 520px !important;
	  top: 15px !important;
	  left: 0px !important;
	  background-color: #fff;
	  box-shadow: 0 1px 6px rgba(178, 178, 178, 0.6);
	  border: 1px solid rgba(72, 181, 233, 0.6);
	  border-radius: 0;
  }
  .gm-style-iw img {max-width: 100%;}
  .store_image { padding: 0 0 10px 0; }
  .breadcrumb { margin: 0; }
  .content { padding: 0 15px; }
  .ml-30{margin-left:30px;}
</style>

      <!--
      <div class="col-sm-2">
         <div class="form-group">
           <label class="control-label" for="filter_area_code"><?php echo $text_nationwide; ?></label>
           <select name="filter_area_code" id="filter_area_code" class="form-control chosen" onchange="javascript:filter()">
             <option value="*"> -- <?php echo $text_all; ?> --</option>
             <?php foreach($areas as $area){?>
              <option value="<?php echo $area['area_code'];?>" <?php echo ($area['area_code']==$filter_area_code)?'selected="selected"':'';?>><?php echo $area['area_name'];?></option>

             <?php } ?>
           </select>
         </div>
       </div>
      <div class="col-sm-2">
        <div class="form-group">
          <label class="control-label" for="region_code"><?php echo $text_region; ?></label>
          <select name="filter_region_code" id="region_code" class="form-control chosens" onchange="javascript:filter()">
            <option value="*"><?php echo $text_all; ?></option>
            <?php foreach($regions as $region){?>
            <option value="<?php echo $region['region_code'];?>" <?php echo ($region['region_code']==$filter_region_code)?'selected="selected"':'';?>><?php echo $region['region_name'];?></option>
            <?php } ?>
          </select>
        </div>
      </div>//--> 
      
<div id="content">
  <div class="wrap">
    <div id="filter-area">
    <div class="clearfix">
      <div class="col-sm-10">
    
    <div class="row">
      <div class="col-sm-3">
      
        <div class="form-group">
          <label class="control-label" for="filter_global"><?php echo $text_search; ?></label>
          <input type="text" id="filter_global" name="filter_global" value="<?php echo $filter_global;?>" class="form-control" onchange="javascript:filter()">
        </div>
      </div>
      
        <div class="col-sm-3">
          <div class="form-group">
         
            <label class="control-label" for="filter_user"><?php echo $text_staff_user; ?></label>
            <select name="filter_user" id="filter_user" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_select; ?></option>
             <?php foreach ($staffs as $user) { ?>
                	<?php if(in_array($user['user_id'],$users_manager)||isset($province_users[$filter_province_id])&&in_array($user['user_id'],$province_users[$filter_province_id])){?>  
                <option value="<?php echo $user['user_id']; ?>" <?php if ($user['user_id'] == $filter_user) { ?>selected="selected"<?php } ?>><?php echo $user['usercode']; ?> - <?php echo $user['fullname']; ?> <?php echo '('.$user['plan_total'].')';?></option>
                	<?php } ?>
                <?php } ?>
            </select>
          </div>
        </div><!--col -->
      
      <div class="col-sm-3">
        <div class="form-group">
          <label class="control-label" for="filter_round"><?php echo $text_round; ?></label>
          <select name="filter_round" id="filter_round" class="form-control" onchange="javascript:filter()">
            <option value="*"><?php echo $text_all; ?></option>
            <?php foreach($rounds as $round){?>
            <option value="<?php echo $round['round_name'];?>" <?php echo ($round['round_name']==$filter_round)?'selected="selected"':'';?>><?php echo $round['round_name'];?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      
      <div class="col-sm-3">
        <div class="form-group">
          <label class="control-label" for="input-filter_date"><?php echo $text_date; ?></label>
          <input type="text" id="input-filter_date" name="filter_date" value="<?php echo $filter_date;?>" placeholder="<?php echo $text_date; ?>" class="form-control datepicker" onchange="javascript:filter()">
        </div>
      </div><!--//col-->
     </div> <!--//row-->
     
    <div class="row">
    
      <div class="col-sm-3">
        <div class="form-group">
          <label class="control-label" for="filter_province_id"><?php echo $text_province; ?></label>
          <select name="filter_province_id" id="filter_province_id" class="form-control chosen" onchange="javascript:filter()">
            <option value="*"><?php echo $text_all; ?></option>
            <?php foreach($provinces as $province){?>
            <option value="<?php echo $province['province_id'];?>" <?php echo ($province['province_id']==$filter_province_id)?'selected="selected"':'';?>><?php echo $province['name'];?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="form-group">
          <label class="control-label" for="input-rating"><?php echo $text_rating; ?></label>
          <select name="filter_rating_status" id="input-rating" class="form-control chosen" onchange="javascript:filter()">
            <option value="*"><?php echo $text_all; ?></option>
             <?php foreach($rstatuses as $value=>$label){?>
                <option value="<?php echo $value;?>" <?php echo ($value==$filter_rating_status)?'selected="selected"':'';?>><?php echo $label;?></option>
             <?php } ?>  
          </select>
        </div>
      </div>
      <div class="col-sm-3">
      </div><!--//col-->
     </div> <!--//row-->
      
      
      </div><!--//col-->
      
      <div class="col-sm-2">
        <div class="clearfix" style="margin-top:22px;">
      <button type="button" id="button-clear" class="btn btn-primary btn-sm"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>
      
        <span class="badge alert-danger"><?php echo $total;?><?php echo $text_store;?> </span>
        </div>
      </div><!--//col-->
     </div> <!--//clearfix-->
     </div> <!--//filter-area-->
    <!--panel-body -->
    <div id="map"></div>
    <div class="map-info">
      <div class="icon-green dat" style="height: 40px;"><?php echo $text_rating_passed;?> <span class="badge"></span></div>
      <div class="icon-red rot" style="height: 40px;"><?php echo $text_rating_not_passed;?><span class="badge"></span></div>
      <div class="icon-yellow ktc" style="height: 40px;">KTC <span class="badge"></span></div>
    </div>
  </div>
</div>

<div id="modal-alert" class="modal fade">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="mpadding"> Số cửa hàng quá nhiều (<?php echo $total;?> địa điểm), không đủ tài nguyên để hiển thị, vui lòng chọn thêm tiêu chí tìm kiếm!</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-hidden="true">Ok</button>
               <!-- <a id="btn-modal-alert" class="btn btn-primary pull-right" style="margin-right:15px;">Ok</a>//--> 
            </div>
        </div>
      </div>
</div><!--//module-modal -->
<script type="text/javascript"><!--
  var map;
  var markers = [];
  var coordinates = <?php echo $coordinates;?>;
  var locations = <?php echo $locations;?>;
  var latitude = '<?php echo $latitude;?>';
  var longitude = '<?php echo $longitude;?>';
  var count = <?php echo $count;?>;
  var marker_url = '<?php echo $marker_url;?>';
  var plan_edit = '<?php echo $plan_edit;?>';
  var http_server = '<?php echo $http_server;?>';

   function initMap() {
		if ((typeof latitude !== 'undefined') && (typeof longitude !== 'undefined')) {
			lat = parseFloat(latitude);
			long = parseFloat(longitude);
		}else {
			lat = 10.824725;
			long = 106.640540;
		}
	
		 map = new google.maps.Map(document.getElementById('map'), {
		   zoom: 9,
		   center: {lat: lat, lng: long}
	
		 });
	
		if((typeof count != 'undefined')){
			$('.map-info .dat > span').html(count.dat);
			$('.map-info .rot > span').html(count.rot);
			$('.map-info .ktc > span').html(count.ktc);
		}
   }
   
   function renderData(){
		addMarkers();
		
   }

   function clearMarkers() {
     for (var i = 0; i < markers.length; i++) {
       markers[i].setMap(null);
     }
   }

   function addMarkers(){
	   
		
		var i;
		var infowindow = new google.maps.InfoWindow();

     for (i = 0; i < locations.length; i++) {
     	var image_icon = new google.maps.MarkerImage(
  			marker_url+locations[i]['marker'],
  			new google.maps.Size(50, 50), // size
  			new google.maps.Point(0, 0), // origin
  			new google.maps.Point(16, 32) // anchor
  		);

       marker = new google.maps.Marker({
         position: new google.maps.LatLng(locations[i]['lat'], locations[i]['long']),
         icon: image_icon,
         animation: google.maps.Animation.DROP,
         map: map

       });

       markers.push(marker);

       google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {

         return function() {
         	var content = '';
         	 		content += '<div id="map-container">';
	  					content += '<h4 class="map-title">'+ locations[i]['code'] + ' - ' + locations[i]['name'] + '</h4>';
	  					content += '<div id="map-content">';

  					//dia chi
					
	  				if(locations[i]['img']){
	  					content += '<div class="store_image col-md-4"><img src="'+http_server+locations[i]['img']+'" class="img-responsive" /></div>';
	  				}
					
	  				content += '<div class="col-md-8">';
  						if(locations[i]['add']){
							content += '<p><strong>Địa chỉ:</strong> '+locations[i]['add']+'</p>';
						}
						if(locations[i]['store_name']){
							content += '<p><strong>Chủ cửa hàng:</strong> '+locations[i]['name']+'</p>';
						}
						if(locations[i]['store_phone']){
							content += '<p><strong>Điện thoại:</strong> '+locations[i]['phone']+'</p>';
						}
						if(locations[i]['sup']){
							content += '<p><strong>Sup KH:</strong> '+locations[i]['sup']+'</p>';
						}
  					content += '</div>';
					
					
	  				content += '<div class="col-md-12">';
					
	  				content += '<table class="table">';
	  				content += '<tr>';
					content += '<td>Plan</td>';
					content += '<td><?php echo $text_planogram;?></td>';
					content += '<td>Thực hiện</td>';
					content += '<td>Điểm</td>';
	  				content += '<tr>';
					
						$.each( locations[i]['plans'], function(k,plan) {
	  				content += '<tr>';
					content += '<td><a target="_blank" href="'+plan_edit+plan.plan_id+'">'+plan.round_name+'</a></td>';
					content += '<td><a target="_blank" href="'+plan_edit+plan.plan_id+'">'+plan.survey_name+'</a></td>';
					content += '<td><a target="_blank" href="'+plan_edit+plan.plan_id+'">'+plan.rating_result+'</a></td>';
					content += '<td><a target="_blank" href="'+plan_edit+plan.plan_id+'">'+plan.survey_total+'</a></td>';
	  				content += '<tr>';
					
							
						});
						
  					content += '</table>';

  						content += '</div>';
						
  					
					
  						content += '</div></div>';

           infowindow.setContent(content);

           infowindow.open(map, marker);

         }

       })(marker, i));


    // *
    // START INFOWINDOW CUSTOMIZE.
    // The google.maps.event.addListener() event expects
    // the creation of the infowindow HTML structure 'domready'
    // and before the opening of the infowindow, defined styles are applied.
    // *
    google.maps.event.addListener(infowindow, 'domready', function() {

      // Reference to the DIV that wraps the bottom of infowindow
      var iwOuter = $('.gm-style-iw');

      iwOuter.children(':nth-child(1)').css({'display' : 'block'});

      /* Since this div is in a position prior to .gm-div style-iw.
       * We use jQuery and create a iwBackground variable,
       * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
      */
      var iwBackground = iwOuter.prev();

      // Removes background shadow DIV
      iwBackground.children(':nth-child(2)').css({'display' : 'none'});

      // Removes white background DIV
      iwBackground.children(':nth-child(4)').css({'display' : 'none'});

      // Changes the desired tail shadow color.
      iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});

      // Reference to the div that groups the close button elements.
      var iwCloseBtn = iwOuter.next();

      // Apply the desired effect to the close button
      iwCloseBtn.css({opacity: '1', right: '40px', top: '3px', height: '27px', width: '27px', border: '7px solid #48b5e9', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9'});

      // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
      iwCloseBtn.mouseout(function(){
        $(this).css({opacity: '1'});
      });
    });
	
     }
	 //end for

   }



   
</script>
<script type="text/javascript"><!--
  function filter(){

  	url = 'index.php?route=plan/map';

  	var filter_global = $('input[name=\'filter_global\']').val();
  	if (filter_global) {
  		url += '&filter_global=' + encodeURIComponent(filter_global);
  	}
	
	var filter_round = $('select[name=\'filter_round\']').val();
  	if (filter_round != '*') {
  		url += '&filter_round=' + encodeURIComponent(filter_round);

  	}/*
	 var filter_area_code = $('select[name=\'filter_area_code\']').val();
	 	if (filter_area_code != '*') {
     	url += '&filter_area_code=' + encodeURIComponent(filter_area_code);
		
   	}

  	var filter_region_code = $('select[name=\'filter_region_code\']').val();
  	if (filter_region_code != '*') {
  		url += '&filter_region_code=' + encodeURIComponent(filter_region_code);
  	}
*/ 
  	var filter_province_id = $('select[name=\'filter_province_id\']').val();
  	if (filter_province_id != '*') {
  		url += '&filter_province_id=' + encodeURIComponent(filter_province_id);
  	}

  	var filter_rating_status = $('select[name=\'filter_rating_status\']').val();
  	if (filter_rating_status != '*') {
  		url += '&filter_rating_status=' + encodeURIComponent(filter_rating_status);
  	}
	
  	var filter_user = $('select[name=\'filter_user\']').val();
  	if (filter_user != '*') {
  		url += '&filter_user=' + encodeURIComponent(filter_user);
  	}
	
	
/**/
  	var filter_date = $('input[name=\'filter_date\']').val();
  	if (filter_date) {
  		url += '&filter_date=' + encodeURIComponent(filter_date);
  	} 
	
    location = url;


  }
  function getStores(){
	  /*
	var total = <?php echo $total;?>;
	if(total>2500){
		initMap();
		$('#modal-alert').modal('show');
	}else{
		$.ajax({
		 url: '<?php echo $stores_url;?>',
		 dataType: 'json',
		 beforeSend: function() {
			clearMarkers();
			locations = [];
			markers = [];
		 },
		 complete: function() { },
		 success: function(json) {
				if(json['count']){
					count = json['count'];
				}

				if(json['stores']){
					locations = json['stores']; 
					initMap();
				}
			}
	
		});  
	}*/ 
  }
  //--></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXTWtYMVxYaJ2EXPHinUNUx1hygW0T7eU"></script>
<script type="text/javascript"><!--
	initMap();
	getStores();
  //--></script>
<?php echo $footer;?>