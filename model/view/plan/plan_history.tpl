 <div class="box-header">
        <h3 class="box-title"><?php echo $heading_plan_history; ?></h3>
      </div>
      <div class="box-body">
                    <div id="status_history">
             
             <div class="clearfix">
  <table class="table table-bordered">
    <thead>
      <tr>
        <td class="text-left"><?php echo $text_user; ?></td>
        <td class="text-left"><?php echo $text_route; ?></td>
        <td class="text-left"><?php echo $text_result_before; ?></td>
        <td class="text-left"><?php echo $text_result_after; ?></td>
        <td class="text-left"><?php echo $text_date_added; ?></td>
      </tr>
    </thead>
    <tbody>
      <?php if ($histories) { ?>
      <?php foreach ($histories as $history) { ?>
      <tr>
        <td class="text-left"><?php echo $history['fullname']; ?></td>
        <td class="text-left"><?php echo $history['route']; ?></td>
        <td class="text-left"><?php echo $history['rating_old']; ?></td>
        <td class="text-left"><?php echo $history['rating_new']; ?></td>
        <td class="text-left" width="150"><?php echo $history['date_added']; ?></td>
      </tr>
      <?php } ?>
      <?php } else { ?>
      <tr>
        <td class="text-center" colspan="5"><?php echo $text_no_changes; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<div class="clearfix">
  <div class="col-sm-6 text-left"><?php echo $results; ?></div>
  <div class="col-sm-6 text-right"><?php echo str_replace('class="pagination"','class="pagination pagination-sm"',$pagination); ?></div>
</div>      
            
</div>

 </div>
<script type="text/javascript"><!--
/*
$('#status_history').delegate('.pagination a', 'click', function(e) {
	e.preventDefault();

	$('#plan_history').load(this.href);
});*/ 

//--></script>    