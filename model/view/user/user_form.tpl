
  <!-- Your Page Content Here -->
  <div id="content">
    <div class="page-header">
      <div class="container-fluid">
        <div class="pull-right">
          <button type="submit" form="form-user" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
          <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_back; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <?php if ($error_warning) { ?>
      <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
       <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> <?php echo $success; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
    	<?php } ?>
    
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-user" class="form-horizontal">
  			<div class="row">
					<div class="col-md-6">
                    <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua-active">
              <h3 class="widget-user-username"><?php echo $fullname; ?></h3>
              <h5 class="widget-user-desc"><?php echo $group_name;?></h5>
            </div>
            <div class="widget-user-image img_upload clearfix" id="img-thumbnail">
              <img class="img-circle" src="<?php echo $thumb; ?>" alt="<?php echo $text_avatar; ?>" id="thumb">
              <input type="hidden" name="image" value="<?php echo $image; ?>" id="image" />
                <div>
                 <a class="ajax_upload"><i class="fa fa-cloud-upload"></i></a>
                </div>
            </div>
            <div class="box-footer" style="padding-top:50px;">
              <div class="row">
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header"><?php echo $username; ?></h5>
                    <span class="description-text"><?php echo $text_username; ?></span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header"><?php echo $email; ?></h5>
                    <span class="description-text"><?php echo $text_email; ?></span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4">
                  <div class="description-block">
                    <h5 class="description-header"><?php echo $telephone; ?></h5>
                    <span class="description-text"><?php echo $text_phone; ?></span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
          
          
            <div class="box box-info">
              <div class="box-header">
                <h3 class="box-title"> <?php echo $text_password; ?></h3>
                <div class="pull-right box-tools"> </div>
              </div>
              <div class="panel-body">
              
                <div class="form-group required">
                  <label class="col-sm-3 control-label" for="input-password"><?php echo $text_password; ?></label>
                  <div class="col-sm-9">
                    <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $text_password; ?>" id="input-password" class="form-control" autocomplete="false" />
                    <?php if ($error_password) { ?>
                    <div class="text-danger"><?php echo $error_password; ?></div>
                    <?php  } ?>
                  </div>
                </div>
                <div class="form-group required">
                  <label class="col-sm-3 control-label" for="input-confirm"><?php echo $text_password_confirm; ?></label>
                  <div class="col-sm-9">
                    <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $text_password_confirm; ?>" id="input-confirm" class="form-control" />
                    <?php if ($error_confirm) { ?>
                    <div class="text-danger"><?php echo $error_confirm; ?></div>
                    <?php  } ?>
                  </div>
                </div>
                
              </div>
            </div>
            <!-- //box-->
              
             <div class="box box-info">
              <div class="box-header">
                <h3 class="box-title"> <?php echo $text_personal_info; ?></h3>
              </div>
              <div class="panel-body">
                <div class="form-group required">
                  <label class="col-sm-3 control-label" for="input-address"><?php echo $text_address; ?></label>
                  <div class="col-sm-9">
                    <input type="text" name="address" value="<?php echo $address; ?>" placeholder="<?php echo $text_address; ?>" id="input-address" class="form-control" />
                  </div>
                </div>
                <div class="form-group required">
                  <label class="col-sm-3 control-label" for="input-identity_number"><?php echo $text_identity_number; ?></label>
                  <div class="col-sm-9">
                    <input type="text" name="p_identity_number" value="<?php echo $p_identity_number; ?>" placeholder="<?php echo $text_identity_number; ?>" id="input-identity_number" class="form-control" />
                  </div>
                </div>
                <div class="form-group required">
                  <label class="col-sm-3 control-label" for="input-identity_issue_date"><?php echo $text_identity_issue_date; ?></label>
                  <div class="col-sm-9">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="p_identity_issue_date" id="input-identity_issue_date" value="<?php echo ($p_identity_issue_date!='0000-00-00')?$p_identity_issue_date:''; ?>" placeholder="<?php echo $text_identity_issue_date; ?>" class="form-control pull-right datepicker"/>
                    </div>
                    <!-- /.input group -->
                  </div>
                </div>
                <div class="form-group required">
                  <label class="col-sm-3 control-label" for="input-p_identity_issue_place"><?php echo $text_identity_issue_place; ?></label>
                  <div class="col-sm-9">
                    <input type="text" name="p_identity_issue_place" value="<?php echo $p_identity_issue_place; ?>" placeholder="<?php echo $text_identity_issue_place; ?>" id="input-p_identity_issue_place" class="form-control" />
                  </div>
                </div>
                <div class="form-group required">
                  <label class="col-sm-3 control-label" for="input-p_tax_code"><?php echo $text_tax_code; ?></label>
                  <div class="col-sm-9">
                    <input type="text" name="p_tax_code" value="<?php echo $p_tax_code; ?>" placeholder="<?php echo $text_tax_code; ?>" id="input-p_tax_code" class="form-control" />
                  </div>
                </div>
                <div class="form-group required">
                  <label class="col-sm-3 control-label" for="input-p_tax_code"><?php echo $text_insurance_num; ?></label>
                  <div class="col-sm-9">
                    <input type="text" name="p_insurance_num" value="<?php echo $p_insurance_num; ?>" placeholder="<?php echo $text_insurance_num; ?>" id="input-p_insurance_num" class="form-control" />
                  </div>
                </div>
                <div class="form-group required">
                  <label class="col-sm-3 control-label" for="input-p_belonging_persons"><?php echo $text_belonging_persons; ?></label>
                  <div class="col-sm-9">
                    <input type="text" name="p_belonging_persons" value="<?php echo $p_belonging_persons; ?>" placeholder="<?php echo $text_belonging_persons; ?>" id="input-p_belonging_persons" class="form-control" />
                  </div>
                </div>
                <div class="form-group required">
                  <label class="col-sm-3 control-label" for="input-p_relative_telephone"><?php echo $text_relative_telephone; ?></label>
                  <div class="col-sm-9">
                    <input type="text" name="p_relative_telephone" value="<?php echo $p_relative_telephone; ?>" placeholder="<?php echo $text_relative_telephone; ?>" id="input-p_relative_telephone" class="form-control" />
                  </div>
                </div>
              </div>
              <!--panel-body -->
            </div>
            <!--box -->
            
             <div class="box box-info">
              <div class="box-header">
                <h3 class="box-title"> <?php echo $text_user_image; ?></h3>
              </div>
              <div class="panel-body">
              <div class="row thumbnails">
              
            
              <?php foreach($user_images as $key=>$img){?>
              <div class="col-sm-6">
                  <label class="control-label" for="input-image"><?php echo $img['label']; ?></label>
                  <div class="clearfix">
                      <div class="img-thumbnail img_upload" id="<?php echo $key; ?>">
                          <a href="<?php echo $img['popup']; ?>" class="popup"><img src="<?php echo $img['thumb']; ?>" alt="<?php echo $img['label']; ?>" title="<?php echo $img['label']; ?>" data-placeholder="<?php echo $placeholder; ?>" id="thumb"/></a>
                            <input type="hidden" name="images[<?php echo $key; ?>]" value="<?php echo $img['value']; ?>" id="image" />
                            <div class="clearfix">
                             <a class="btn btn-default ajax_upload"><i class="fa fa-cloud-upload"></i></a>
                            </div>
                       </div>
                  </div>
                  </div><!--//col--> 
              <?php }?>
              
              
              </div><!--//row--> 
                  
                </div> <!--panel-body -->
              </div> <!--box -->
              
              
            
          </div>
          <!--col -->
          <div class="col-md-6">
           
          
             <div class="box box-info">
              <div class="box-header">
                <h3 class="box-title"> <?php echo $text_bank_account; ?></h3>
                <div class="pull-right box-tools"> </div>
              </div>
              <div class="panel-body">
		        		<div class="form-group required">
		              <label class="col-sm-3 control-label" for="input-bank_holder"><?php echo $text_bank_holder; ?></label>
		              <div class="col-sm-9">
		                <input type="text" name="p_bank_holder" value="<?php echo $p_bank_holder; ?>" placeholder="<?php echo $text_bank_holder; ?>" id="input-bank_holder" class="form-control" />
		              </div>
		            </div>
		        		<div class="form-group required">
		              <label class="col-sm-3 control-label" for="input-bank_number"><?php echo $text_bank_number; ?></label>
		              <div class="col-sm-9">
		                <input type="text" name="p_bank_number" value="<?php echo $p_bank_number; ?>" placeholder="<?php echo $text_bank_number; ?>" id="input-bank_number" class="form-control" />
		              </div>
		            </div>
		        		<div class="form-group required">
		              <label class="col-sm-3 control-label" for="input-bank_name"><?php echo $text_bank_name; ?></label>
		              <div class="col-sm-9">
		                <input type="text" name="p_bank_name" value="<?php echo $p_bank_name; ?>" placeholder="<?php echo $text_bank_name; ?>" id="input-bank_name" class="form-control" />
		              </div>
		            </div>
		          </div>
	          </div><!--//box --> 
              <div class="box box-info">
              <div class="box-header">
                <h3 class="box-title"> <?php echo $text_residence_information; ?></h3>
                <div class="pull-right box-tools"> </div>
              </div>
              <div class="panel-body">
              	<div class="form-group">
                  <label class="col-sm-3 control-label" for="input-p_re_address"><?php echo $text_address; ?></label>
                  <div class="col-sm-9">
                    <input type="type" name="p_re_address" value="<?php echo $p_re_address; ?>" placeholder="<?php echo $text_address; ?>" id="input-p_re_address" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-province"><?php echo $text_province; ?></label>
                  <div class="col-sm-9">
                    <select name="p_re_province_id" data-selected="<?php echo $p_re_province_id;?>" id="input-p_re_province" class="form-control load_provinces chosen" onchange="load_province(this, 'p_re_district_id','<?php echo $p_re_district_id;?>');">
                      <option value="" <?php if (!$p_re_province_id) { ?>selected="selected" <?php } ?>><?php echo $text_none;?></option>
                      <?php foreach ($provinces as $province) { ?>
                      <option value="<?php echo $province['province_id']; ?>" <?php if ($province['province_id']==$p_re_province_id) { ?>selected="selected" <?php } ?>><?php echo $province['name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-p_re_district"><?php echo $text_district; ?></label>
                  <div class="col-sm-9">
                    <select name="p_re_district_id" data-selected="<?php echo $p_re_district_id;?>" id="input-p_re_district" class="form-control chosen" onchange="load_district(this, 'p_re_ward_id','<?php echo $p_re_ward_id;?>');">
                      <option value=""  <?php if (!$p_re_district_id) { ?>selected="selected" <?php } ?>><?php echo $text_none;?></option>
                      <?php foreach ($districts as $district) { ?>
                      <option value="<?php echo $district['district_id']; ?>" <?php if ($district['district_id']==$p_re_district_id) { ?>selected="selected" <?php } ?>><?php echo $district['name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-p_re_ward"><?php echo $text_ward; ?></label>
                  <div class="col-sm-9">
                    <select name="p_re_ward_id" data-selected="<?php echo $p_re_ward_id;?>" id="input-p_re_ward" class="form-control chosen">
                      <option value=""  <?php if (!$p_re_ward_id) { ?>selected="selected" <?php } ?>><?php echo $text_none;?></option>
                      <?php foreach ($wards as $ward) { ?>
                      <option value="<?php echo $ward['ward_id']; ?>" <?php if ($ward['ward_id']==$p_re_ward_id) { ?>selected="selected" <?php } ?>><?php echo $ward['name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
            </div><!--//box -->
            <div class="box box-info">
              <div class="box-header">
                <h3 class="box-title"> <?php echo $text_sojourn_information; ?></h3>
                <div class="pull-right box-tools"> </div>
              </div>
              <div class="panel-body">
              	<div class="form-group">
                  <label class="col-sm-3 control-label" for="input-p_so_address"><?php echo $text_address; ?></label>
                  <div class="col-sm-9">
                    <input type="type" name="p_so_address" value="<?php echo $p_so_address; ?>" placeholder="<?php echo $text_address; ?>" id="input-p_so_address" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-p_so_province"><?php echo $text_province; ?></label>
                  <div class="col-sm-9">
                    <select name="p_so_province_id" data-selected="<?php echo $p_so_province_id;?>" id="input-p_so_province" class="form-control load_provinces chosen" onchange="load_province(this, 'p_so_district_id','<?php echo $p_so_district_id;?>');">
                      <option value="" <?php if (!$p_so_province_id) { ?>selected="selected" <?php } ?>><?php echo $text_none;?></option>
                      <?php foreach ($provinces as $province) { ?>
                      <option value="<?php echo $province['province_id']; ?>" <?php if ($province['province_id']==$p_so_province_id) { ?>selected="selected" <?php } ?>><?php echo $province['name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-p_so_district"><?php echo $text_district; ?></label>
                  <div class="col-sm-9">
                    <select name="p_so_district_id" data-selected="<?php echo $p_so_district_id;?>" id="input-p_so_district" class="form-control chosen" onchange="load_district(this, 'p_so_ward_id','<?php echo $p_so_ward_id;?>');">
                      <option value=""  <?php if (!$p_so_district_id) { ?>selected="selected" <?php } ?>><?php echo $text_none;?></option>
                      <?php foreach ($districts as $district) { ?>
                      <option value="<?php echo $district['district_id']; ?>" <?php if ($district['district_id']==$p_so_district_id) { ?>selected="selected" <?php } ?>><?php echo $district['name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-p_so_ward"><?php echo $text_ward; ?></label>
                  <div class="col-sm-9">
                    <select name="p_so_ward_id" data-selected="<?php echo $p_so_ward_id;?>" id="input-p_so_ward" class="form-control chosen">
                      <option value=""  <?php if (!$p_so_ward_id) { ?>selected="selected" <?php } ?>><?php echo $text_none;?></option>
                      <?php foreach ($wards as $ward) { ?>
                      <option value="<?php echo $ward['ward_id']; ?>" <?php if ($ward['ward_id']==$p_so_ward_id) { ?>selected="selected" <?php } ?>><?php echo $ward['name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
            </div><!-- //box-->  
            
        		<div class="box box-info">
              <div class="box-header">
                <h3 class="box-title"> <?php echo $text_birth_information; ?></h3>
                <div class="pull-right box-tools"> </div>
              </div>
              <div class="panel-body">
              	<div class="form-group">
                  <label class="col-sm-3 control-label" for="input-p_bi_address"><?php echo $text_address; ?></label>
                  <div class="col-sm-9">
                    <input type="type" name="p_bi_address" value="<?php echo $p_bi_address; ?>" placeholder="<?php echo $text_address; ?>" id="input-p_bi_address" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-p_bi_province"><?php echo $text_province; ?></label>
                  <div class="col-sm-9">
                    <select name="p_bi_province_id" data-selected="<?php echo $p_bi_province_id;?>" id="input-p_bi_province" class="form-control load_provinces chosen" onchange="load_province(this, 'p_bi_district_id','<?php echo $p_bi_district_id;?>');">
                      <option value="" <?php if (!$p_bi_province_id) { ?>selected="selected" <?php } ?>><?php echo $text_none;?></option>
                      <?php foreach ($provinces as $province) { ?>
                      <option value="<?php echo $province['province_id']; ?>" <?php if ($province['province_id']==$p_bi_province_id) { ?>selected="selected" <?php } ?>><?php echo $province['name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-p_bi_district"><?php echo $text_district; ?></label>
                  <div class="col-sm-9">
                    <select name="p_bi_district_id" data-selected="<?php echo $p_bi_district_id;?>" id="input-p_bi_district" class="form-control chosen" onchange="load_district(this, 'p_bi_ward_id','<?php echo $p_bi_ward_id;?>');">
                      <option value=""  <?php if (!$p_bi_district_id) { ?>selected="selected" <?php } ?>><?php echo $text_none;?></option>
                      <?php foreach ($districts as $district) { ?>
                      <option value="<?php echo $district['district_id']; ?>" <?php if ($district['district_id']==$p_bi_district_id) { ?>selected="selected" <?php } ?>><?php echo $district['name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-p_bi_ward"><?php echo $text_ward; ?></label>
                  <div class="col-sm-9">
                    <select name="p_bi_ward_id" data-selected="<?php echo $p_bi_ward_id;?>" id="input-p_bi_ward" class="form-control chosen">
                      <option value=""  <?php if (!$p_bi_ward_id) { ?>selected="selected" <?php } ?>><?php echo $text_none;?></option>
                      <?php foreach ($wards as $ward) { ?>
                      <option value="<?php echo $ward['ward_id']; ?>" <?php if ($ward['ward_id']==$p_bi_ward_id) { ?>selected="selected" <?php } ?>><?php echo $ward['name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-protector"><?php echo $text_protector; ?></label>
                  <div class="col-sm-9">
                    <input type="type" name="p_protector" value="<?php echo $p_protector; ?>" placeholder="<?php echo $text_protector; ?>" id="input-protector" class="form-control" />
                  </div>
                </div>
            </div><!--//box --> 
            
        		<div class="box box-info">
              <div class="box-header">
                <h3 class="box-title"> <?php echo $text_other_info; ?></h3>
                <div class="pull-right box-tools"> </div>
              </div>
              <div class="panel-body">
                <div class="form-group required">
                  <label class="col-sm-3 control-label" for="input-birthday"><?php echo $text_birthday; ?></label>
                  <div class="col-sm-9">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="p_birthday" id="input-birthday" value="<?php echo $p_birthday; ?>" placeholder="<?php echo $text_birthday; ?>" class="form-control pull-right datepicker" />
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-place_of_birth"><?php echo $text_place_of_birth; ?></label>
                  <div class="col-sm-9">
                    <select name="p_place_of_birth" data-selected="<?php echo $p_place_of_birth;?>" id="input-place_of_birth" class="form-control chosen">
                      <option value="" <?php if (!$p_place_of_birth) { ?>selected="selected" <?php } ?>><?php echo $text_none;?></option>
                      <?php foreach ($provinces as $province) { ?>
                      <option value="<?php echo $province['province_id']; ?>" <?php if ($province['province_id']==$p_place_of_birth) { ?>selected="selected" <?php } ?>><?php echo $province['name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-nationality"><?php echo $text_nationality; ?></label>
                  <div class="col-sm-9">
                    <input type="text" name="p_nationality" value="<?php echo $p_nationality; ?>" placeholder="<?php echo $text_nationality; ?>" id="input-nationality" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-nation"><?php echo $text_nation; ?></label>
                  <div class="col-sm-9">
                    <input type="text" name="p_nation" value="<?php echo $p_nation; ?>" placeholder="<?php echo $text_nation; ?>" id="input-nation" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-degree"><?php echo $text_degree; ?></label>
                  <div class="col-sm-9">
                    <input type="type" name="p_degree" value="<?php echo $p_degree; ?>" placeholder="<?php echo $text_degree; ?>" id="input-degree" class="form-control" />
                  </div>
                </div>
              </div>
            </div><!--//box --> 
            
            
          </div>
          <!--col -->
        </div>
        <!--row -->
       
      </form>
    </div>
  </div>
</section>
<!-- /.content -->
<script type="text/javascript"><!--//
$('.ajax_upload').each(function() {
	var elem = $(this);
	var elem_id = $(this).parents('.img_upload').attr('id');
	new AjaxUpload(elem, {
		action: 'index.php?route=user/user/image',
		name: 'file',
		autoSubmit: false,
		responseType: 'json',
		onChange: function(file, extension) {
			this.submit();
		},		
		onSubmit: function(file, extension) {
			$('#'+elem_id).after('<img src="<?php echo $http_server;?>assets/editor/img/loading.gif" id="loading" style="padding-left: 5px;"/>');
		},
		onComplete: function(file, json) {
			if (json['success']) {
				$('#'+elem_id + ' input').attr('value', json['filename']);
				$('#'+elem_id + ' img').attr('src', json['thumb']);
			}
			if (json['error']) {
				$('#'+elem_id).after('<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> '+json['error']+'<button type="button" class="close" data-dismiss="alert">×</button></div>');
			}
			$('#loading').remove();	
		}
	});	
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a.popup',
		gallery: {
			enabled:true
		}
	});
});

//--></script>

