<div id="quick-add-user" class="box box-info">
  <div class="box-header">
    <h3 class="box-title"> <?php echo $text_add; ?></h3>
    <div class="pull-right">
    	<button type="button" id="button-quick-add-save" class="btn btn-primary"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
    </div>
  </div>
  <div class="panel-body">
  	<div class="row">
   <div class="col-sm-6">
   	<div class="form-group required">
	      <label class="control-label" for="input-fullname"><?php echo $text_fullname; ?></label>
	      <div class="clearfix">
	        <input type="text" name="fullname" value="" placeholder="<?php echo $text_fullname; ?>" id="input-fullname" class="form-control" />
	      </div>
	    </div>
   </div> <!--//col -->
   <div class="col-sm-6">

	    <div class="form-group required">
	      <label class="control-label" for="input-identity_number"><?php echo $text_identity_number; ?></label>
	      <div class="clearfix">
	        <input type="text" name="p_identity_number" value="" placeholder="<?php echo $text_identity_number; ?>" id="input-identity_number" class="form-control" />
	      </div>
	    </div>
   </div> <!--//col -->
    </div>

  	<div class="row">
   <div class="col-sm-6">
	    <div class="form-group required">
	      <label class="control-label" for="input-email"><?php echo $text_email; ?></label>
	      <div class="clearfix">
	        <input type="text" name="email" value="" placeholder="<?php echo $text_email; ?>" id="input-email" class="form-control" />
	        <?php if ($error_email) { ?>
	        <div class="text-danger"><?php echo $error_email; ?></div>
	        <?php } ?>
	      </div>
	    </div>
   </div>  <!--//col -->
   <div class="col-sm-6">
	    <div class="form-group required">
	      <label class="control-label" for="input-telephone"><?php echo $text_phone; ?></label>
	      <div class="clearfix">
	        <input type="text" name="telephone" value="" placeholder="<?php echo $text_phone; ?>" id="input-identity_number" class="form-control" />
	        <?php if ($error_telephone) { ?>
	        <div class="text-danger"><?php echo $error_telephone; ?></div>
	        <?php } ?>
	      </div>
	    </div>
   </div> <!--//col -->
    </div>

	<div class="row">
   <div class="col-sm-6">
	  	<div class="form-group required">
	      <label class="control-label" for="input-workplace"><?php echo $text_workplace; ?></label>
	      <div class="clearfix">
	       	<select name="province_id" id="input-province_id" class="form-control chosen">
	          <option value="*"><?php echo $text_select; ?></option>
	          <?php foreach($provinces as $province){?>
	          <option value="<?php echo $province['province_id'];?>"><?php echo $province['name'];?></option>
	          <?php } ?>
	        </select>
	      </div>
	    </div>
   </div>  <!--//col -->
   <div class="col-sm-6">
   </div> <!--//col -->
  </div>

  </div>
</div>
<!-- //box-->
<script type="text/javascript">
	$('#button-quick-add-save').on('click', function() {
		var province_id     = $('#quick-add-user select[name=\'province_id\']').val();
		var fullname        = $('#quick-add-user input[name=\'fullname\']').val();
		var email           = $('#quick-add-user input[name=\'email\']').val();
		var identity_number = $('#quick-add-user input[name=\'p_identity_number\']').val();
		var telephone       = $('#quick-add-user input[name=\'telephone\']').val();
			$.ajax({
				type: 'POST',
				url: 'index.php?route=user/user/quickAddUser',
				dataType: 'json',
				data: 'province_id=' + province_id + '&fullname=' + fullname+'&email=' + email+'&p_identity_number=' + identity_number+'&telephone=' + telephone,
				beforeSend: function(){
					$('#button-quick-add-save').button('loading');
					$('.text-danger').remove();
				},
				success: function(json) {
					$('#button-quick-add-save').button('reset');

					if(json['error']) {
						if(json['error']['province_id']){
							$('#quick-add-user select[name=\'province_id\']').after('<div class="text-danger">'+ json['error']['province_id'] +'</div>');
						}

						if(json['error']['fullname']){
							$('#quick-add-user input[name=\'fullname\']').after('<div class="text-danger">'+ json['error']['fullname'] +'</div>');
						}

						if(json['error']['email']){
							$('#quick-add-user input[name=\'email\']').after('<div class="text-danger">'+ json['error']['email'] +'</div>');
						}

						if(json['error']['identity_number']){
							$('#quick-add-user input[name=\'p_identity_number\']').after('<div class="text-danger">'+ json['error']['identity_number'] +'</div>');
						}

						if(json['error']['telephone']){
							$('#quick-add-user input[name=\'telephone\']').after('<div class="text-danger">'+ json['error']['telephone'] +'</div>');
						}

					}

					if(json['success']) {
						$('#quick-add-user').before('<div class="alert alert-success alert-sm">'+json['success']+'<button type="button" class="close" data-dismiss="alert">x</button></div>');

						$('#quick-add-user input[name=\'fullname\']').val('');
						$('#quick-add-user input[name=\'email\']').val('');
						$('#quick-add-user input[name=\'p_identity_number\']').val('');
						$('#quick-add-user input[name=\'telephone\']').val('');
					}
				}
			});
	});

	$('#button-quick-add-close').on('click', function() {
		$('#quick-add-user').remove();
	});
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('.chosen').chosen({height: "120px",width: "100%"});
});
</script>