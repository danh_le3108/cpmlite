
  <div class="page-header">
  <div class="container-fluid">
    <div class="pull-right">
      <button type="submit" form="form-user-group" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
      <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_back; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a> 
    </div>
  </div>
</div>
<div class="container-fluid">
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-user-group" class="form-horizontal">
    <div class="box">
      <div class="box-header">
        <h2 class="box-title">
        <?php echo $text_general; ?></h3>
        <div class="pull-right box-tools">
        </div>
      </div>
      <div class="panel-body">
        <div class="form-group required">
          <label class="col-sm-2 control-label" for="input-name"><?php echo $text_name; ?></label>
          <div class="col-sm-10">
            <?php echo $group_name; ?>
           
          </div>
        </div>
      </div>
    </div>
    <div class="box">
      <div class="box-header">
        <h2 class="box-title">
        <?php echo $text_system_permission; ?></h3>
      </div>
      <div class="panel-body">
        <?php $x=0;
          foreach ($areas as $area) {?>
        <h3 class="text-center"><?php echo $area['label'];?></h3>
        <div class="row">
          <div class="col-sm-3">
            <h4 class="col-sm-12 text-center"><?php echo $text_access_per; ?></h4>
            <div class="clearfix">
              <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>
              / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a><br/>
              <div class="well well-sm" style="height: 500px; overflow: auto;">
                <?php
                  $q= 0;
                   foreach ($area['folders'] as $folder) {?>
                <h4 class="text-center"><?php echo $folder['label'];?></h4>
                <div class="clearfix">
                  <?php $a=0;
                    foreach ($folder['permissions'] as $permission) { ?>
                  <div class="xcheckbox">
                    <input type="checkbox" name="permission[access][]" id="a<?php echo $x.'-'.$q.'-'.$a;?>" value="<?php echo $permission['value']; ?>" <?php if (in_array($permission['value'], $access)) { ?>checked="checked"<?php } ?>/>
                    <label for="a<?php echo $x.'-'.$q.'-'.$a;?>"><?php echo $permission['label']; ?>
                    </label>
                  </div>
                  <?php $a++; } ?><br/> 
                  <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>
                  / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                </div>
                <?php $q++; } ?>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <h4 class="col-sm-12 text-center"><?php echo $text_add_per; ?></h4>
            <div class="clearfix">
              <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>
              / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a><br/>
              <div class="well well-sm" style="height: 500px; overflow: auto;">
                <?php
                  $w = 0;
                   foreach ($area['folders'] as $folder) {?>
                <h4 class="text-center"><?php echo $folder['label'];?></h4>
                <div class="clearfix">
                  <?php $b=0;
                    foreach ($folder['permissions'] as $permission) { ?>
                  <div class="xcheckbox">
                    <input type="checkbox" name="permission[add][]" id="b<?php echo $x.'-'.$w.'-'.$b;?>" value="<?php echo $permission['value']; ?>" <?php if (in_array($permission['value'], $add)) { ?>checked="checked"<?php } ?>/>
                    <label for="b<?php echo $x.'-'.$w.'-'.$b;?>">  <?php echo $permission['label']; ?>
                    </label>
                  </div>
                  <?php $b++; } ?><br/> 
                  <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>
                  / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                </div>
                <?php
                  $w++; } ?>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <h4 class="col-sm-12 text-center"><?php echo $text_edit_per; ?></h4>
            <div class="clearfix">
              <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>
              / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a><br/> 
              <div class="well well-sm" style="height: 500px; overflow: auto;">
                <?php
                  $e=0;
                   foreach ($area['folders'] as $folder) {?>
                <div class="clearfix">
                  <h4 class="text-center"><?php echo $folder['label'];?></h4>
                  <?php
                    $c=0; foreach ($folder['permissions'] as $permission) { ?>
                  <div class="xcheckbox">
                    <input type="checkbox" name="permission[edit][]" id="c<?php echo $x.'-'.$e.'-'.$c;?>" value="<?php echo $permission['value']; ?>" <?php if (in_array($permission['value'], $edit)) { ?>checked="checked"<?php } ?>/>
                    <label for="c<?php echo $x.'-'.$e.'-'.$c;?>"><?php echo $permission['label']; ?></label>
                  </div>
                  <?php
                    $c++;
                     } ?><br/> 
                  <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>
                  / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                </div>
                <?php $e++;} ?>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <h4 class="col-sm-12 text-center"><?php echo $text_delete_per; ?></h4>
            <div class="clearfix">
              <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>
              / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a><br/> 
              <div class="well well-sm" style="height: 500px; overflow: auto;">
                <?php $r=0; 
                  foreach ($area['folders'] as $folder) {?>
                <div class="clearfix">
                  <h4 class="text-center"><?php echo $folder['label'];?></h4>
                  <?php
                    $d=0; foreach ($folder['permissions'] as $permission) { ?>
                  <div class="xcheckbox">
                    <input type="checkbox" name="permission[delete][]" id="d<?php echo $x.'-'.$r.'-'.$d;?>" value="<?php echo $permission['value']; ?>" <?php if (in_array($permission['value'], $delete)) { ?>checked="checked"<?php } ?>/>
                    <label for="d<?php echo $x.'-'.$r.'-'.$d;?>"> <?php echo $permission['label']; ?>
                    </label>
                  </div>
                  <?php $d++;
                    } ?><br/> 
                  <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>
                  / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                </div>
                <?php $r++; } ?>
              </div>
            </div>
          </div>
        </div>
        <!-- row--> 
        <?php $x++;
          } ?>
      </div>
    </div>
    
    
 
  </form>
</div>
</div>
<!-- /.content -->

