
  <div class="page-header">
  </div>
  <div class="container-fluid">
  
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="box">
      <div class="box-header">
        <h2 class="box-title"> <?php echo $heading_title; ?></h3>
        
        <div class="pull-right box-tools">
        </div>
      </div>
      <div class="panel-body">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-user-group">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-left"><?php if ($sort == 'group_name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $text_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $text_name; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php echo $text_description; ?></td>
                  <td class="text-left"><?php echo $text_level; ?></td>
                  <td class="text-right"><?php echo $text_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($user_groups) { ?>
                <?php foreach ($user_groups as $user_group) { ?>
                <tr>
                  <td class="text-center">
                    <input type="checkbox" name="selected[]" value="<?php echo $user_group['user_group_id']; ?>" <?php if (in_array($user_group['user_group_id'], $selected)) { ?>checked="checked"<?php } ?> id="pr<?php echo $user_group['user_group_id']; ?>"/><label for="pr<?php echo $user_group['user_group_id']; ?>">&nbsp;</label>
                    </td>
                  <td class="text-left"><?php echo $user_group['group_name']; ?></td>
                  <td class="text-left"><?php echo $user_group['group_description']; ?></td>
                  <td class="text-left"><?php echo $user_group['group_level']; ?></td>
                  <td class="text-right"><a href="<?php echo $user_group['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="3"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $results; ?></div>
          <div class="col-sm-6 text-right"><nav><?php echo $pagination; ?></nav></div>
        </div>
      </div>
    </div>
      </div>