<div class="page-header">
  <div class="container-fluid">
    <div class="pull-right">
      <?php if(($hasEdit && !empty($warehouse)) || ($hasAdd && empty($warehouse))) { ?>
      <button type="submit" form="form-store" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
      <?php } ?>
      <a href="index.php?route=warehouse/warehouse" data-toggle="tooltip" title="<?php echo $button_back; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
    </div>
  </div>
</div>

<div class="container-fluid" style="margin-top: 20px">
  <?php if (!empty($success)) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if (!empty($error_warning)) { ?>
    <div class="alert alert-danger"><i class="fa fa-check-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
  <?php } ?>
</div>

<div class="container-fluid">
  <form method="post" action="" id="form-store" class="form-horizontal">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-header">
            <h3 class="box-title"></h3>
          </div>
          <div class="panel-body">
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Tỉnh thành</label>
              <div class="col-sm-9">
                <select class="form-control chosen" name="province_id">
                    <option value="">--- Chọn ---</option>
                    <?php foreach($provinces as $p) {
                      $selected = !empty($warehouse['province_id']) && $warehouse['province_id'] == $p['province_id'] ? 'selected' : '';
                    ?>
                    <option <?=$selected?> value="<?=$p['province_id']?>"><?=$p['name']?></option>
                    <?php } ?>
                </select>
              </div>
            </div>
            <?php foreach($gifts as $g) { 
              $gift = 'gift_'.$g['attribute_id'];
            ?>
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label"><?=$g['attribute_name']?></label>
              <div class="col-sm-9">
                <input type="text" name="<?=$gift?>" value="<?=!empty($warehouse[$gift]) ? $warehouse[$gift] : ''?>" class="form-control" onkeypress="return isNumberKey(event);"/>
              </div>
            </div>
            <?php } ?>
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label">Ghi chú</label>
              <div class="col-sm-9">
              <textarea class="form-control" rows="3" name="comment"><?=!empty($warehouse['comment']) ? $warehouse['comment'] : ''?></textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
     
  </form>
</div>