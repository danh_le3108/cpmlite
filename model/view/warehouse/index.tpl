<div class="page-header">
  <div class="container-fluid">
    <div class="pull-right">
      <?php if($hasAdd) { ?>
      <a href="index.php?route=warehouse/warehouse/add" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
      <?php } ?>
    </div>
  </div>
</div>
<div class="container-fluid" style="margin-top: 20px">
  <?php if (!empty($success)) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if (!empty($error_warning)) { ?>
    <div class="alert alert-danger"><i class="fa fa-check-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
  <?php } ?>
</div>
<div class="container-fluid" id="filter-area">
  <div class="box box-info">
    <div class="panel-body">
      <div class="row">
          <div class="col-sm-12">
            <button type="button" id="button-clear" class="btn btn-primary btn-sm pull-right" autocomplete="off"><i class="fa fa-eraser"></i><?=$button_clear?></button>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label class="control-label" for="input-province_id"><?=$text_province?></label>
              <select name="filter_province_id" id="input-province_id" class="form-control filter chosen" onchange="javascript:filter()">
                <option value="*">Toàn quốc</option>
                <?php foreach($provinces as $p) { ?>
                <option value="<?=$p['province_id']?>"><?=$p['name']?></option>
                <?php }?>
              </select>
            </div>
          </div>
          <div class="clearfix">
            <div class="col-sm-12"><?php //echo $import_store; ?></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <div class="container-fluid">
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title"><?=!empty($province) ? strtoupper($province['name']) : 'TOÀN QUỐC'?></h3>
        </div>
        <div class="box-body table-responsive no-padding">
          <div class="panel-body">
            <table class="table table-hover table table-bordered">
              <thead>
                <tr>
                  <th class="text-center" width="1">#</th>
                  <th class="text-center">Quà tặng</th>
                  <th class="text-center">Nhập</th>
                  <th class="text-center">Xuất</th>  
                  <th class="text-center">Tồn</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $stt = 1;
                foreach($gifts as $g) { 
                  $f = 'gift_'.$g['attribute_id'];
                  $input = !empty($wh_gift[$f]['input']) ? $wh_gift[$f]['input'] : 0;
                  $output = !empty($wh_gift[$f]['output']) ? $wh_gift[$f]['output']*-1 : 0;
                ?>
                <tr>
                  <td><?=$stt?></td>
                  <td><?=$g['attribute_name']?></td>
                  <td><?=$input?></td>
                  <td><?=$output?></td>
                  <td><?=$input - $output?></td>
                </tr>
                <?php 
                $stt++;
                } 
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="ajax_list" class="col-sm-12">

  </div>
</div>
<div id="modal-confirm" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        Bạn có chắc chắn muốn xóa <span></span>?
      </div>
      <!-- <div class="modal-body">
        <div class="mpadding">
          Thông tin này không thể khôi phục nhưng lịch sử sẽ được lưu lại.
        </div>
      </div> -->
      <div class="modal-footer">
        <button type="button" class="btn btn-success pull-right" data-dismiss="modal" aria-hidden="true">Không</button>
        <a id="btn-modal-confirm" class="btn btn-danger pull-right" style="margin-right:15px;">Xác nhận Xóa</a>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
  $(document).ready(function(){
    var url = '<?=str_replace("amp;","",$url)?>';
    $('#ajax_list').load(url);
    <?php if(!empty($filter_global)){?>
    $("td span:contains(<?php echo $filter_global;?>)").css({"color":"red","background":"yellow"});
    <?php } ?>
  });

  function deleteStore(warehouse_id){
      // $('#modal-confirm .modal-header span').html();
      $('#modal-confirm').modal('show');
      $('#btn-modal-confirm').click(function(event) {
        event.preventDefault();
        $('#btn-modal-confirm').unbind('click');
        $.get('index.php?route=warehouse/warehouse/delete&warehouse_id='+warehouse_id, function(data){
          $('#modal-confirm').modal('hide');
          $('#warehouse_id_'+warehouse_id).remove();
        })
      });
  }
</script>