 <!--
<div class="page-header">
  <div class="container-fluid">
    <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
      <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-problem').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
  </div>
</div>//--> 
<div class="container-fluid">
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="box" id="filter-area">
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label" for="filter_name"><?php echo $text_search; ?></label>
            <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $text_search; ?>" id="filter_name" class="form-control" onchange="filter()"/>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label" for="filter_coupon_status"><?php echo $text_coupon_status; ?></label>
            <select name="filter_coupon_status" id="filter_coupon_status" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_all; ?></option>
              <?php foreach ($coupon_status as $status) { ?>
              <option value="<?php echo $status['coupon_status_id']; ?>" <?php if ($status['coupon_status_id'] == $filter_coupon_status) { ?>selected="selected"<?php } ?>><?php echo $status['coupon_status_name']; ?></option>
              <?php } ?>
              <option value="0" <?php if (!is_null($filter_coupon_status)&&0 == $filter_coupon_status) { ?>selected="selected"<?php } ?>><?php echo $text_unused; ?> </option>
            </select>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="filter_sup_id"><?php echo $text_sup_user; ?></label>
            <select name="filter_sup_id" id="filter_sup_id" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_all; ?></option>
              <?php foreach ($users as $user) { ?>
              <option value="<?php echo $user['user_id']; ?>" <?php if ($user['user_id'] == $filter_sup_id) { ?>selected="selected"<?php } ?>><?php echo $user['usercode'].' - '.$user['fullname']; ?></option>
              <?php } ?>
            </select>
          </div>
          
          
          
        </div>
        <div class="col-sm-2">
          <button type="button" id="button-clear" class="btn btn-primary pull-right"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>
        </div>
      </div>
    </div>
    <!--panel-body -->
  </div>
  <!--box -->
  <div class="box" id="import-area">
    <div class="box-header"><a onclick="loadImport('<?php echo $template_id;?>');" class="btn-xs pull-right"><i class="fa fa-download"></i> <?php echo $text_import_history; ?></a></div>
    <div class="panel-body">
      <div class="form-group clearfix" style="margin-top:20px;">
        <form action="<?php echo $form;?>" id="form-upload" enctype="multipart/form-data" method="post">
          <?php if($has_add) { ?>
          <label class="control-label col-sm-2" for="input-import"><?php echo $text_import; ?></label>
          <div class="col-sm-4">
            <div class="input-group">
              <input type="file" id="file_import" name="file_import" class="form-control">
              <div class="input-group-btn">
                <button class="btn btn-primary" type="submit" data-loading-text="Importing..." id="button-import"><?php echo $text_import; ?></button>
              </div>
            </div>
          </div>
          <?php } ?>
          <div class="col-sm-6">
            <a href="<?php echo $template_url; ?>" class="btn btn-success"><i class="fa fa-download"></i> Tải mẫu excel</a>
            <!-- //--> 
            <?php if($has_edit && isset($export)) { ?>
            <a  href="<?php echo $export;?>" class="btn btn-primary btn-flat"><i class="fa fa-download"></i> <?php echo $text_download_results; ?></a>
          

            <button id="huy_coupon" type="button" class="btn btn-danger">Hủy coupon</button>
            
            <button id="unlock_coupon" type="button" class="btn btn-warning">Unlock coupon</button>
              <?php } ?>
          </div>
          <input type="hidden" name="template_id" value="<?php echo $template_id; ?>">
          
          <?php if($history_id>0){?>
              <script type="text/javascript"><!--
               loadImport('<?php echo $template_id;?>');
                //-->
              </script>
         <?php } ?>
         
        </form>
      </div>
    </div>
  </div>
</div>
<!-- /.box -->
<div class="container-fluid">
  <div class="box">
    <div class="box-header">
      <h2 class="box-title">
      <?php echo $heading_title; ?></h3>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $results; ?></div>
        <div class="col-sm-6 text-right">
          <nav><?php echo $pagination; ?></nav>
        </div>
      </div>
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-problem">
        <div class="table-responsive">
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <th style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></th>
                <th class="text-left"><?php if ($sort == 'coupon_code') { ?>
                  <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $text_name; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_name; ?>"><?php echo $text_name; ?></a>
                  <?php } ?>
                </th>
                <th class="text-left"><?php echo $text_coupon_prefix; ?></th>
                <th class="text-left">SUP code</th>
                <th class="text-left"><?php echo $text_round_month; ?></th>
                <th class="text-left"><?php echo $text_coupon_status; ?></th>
              <!--  <td class="text-right"><?php echo $text_action; ?></td>//--> 
              </tr>
            </thead>
            <tbody>
              <?php if ($coupons) { ?>
              <?php foreach ($coupons as $coupon) { ?>
              <tr>
                <td class="text-center">
                  <input type="checkbox" name="selected[]" value="<?php echo $coupon['coupon_id']; ?>" <?php if (in_array($coupon['coupon_id'], $selected)) { ?>checked="checked"<?php } ?> id="pr<?php echo $coupon['coupon_id']; ?>"/><label for="pr<?php echo $coupon['coupon_id']; ?>">&nbsp;</label>
                </td>
                <td class="text-left"><?php echo $coupon['coupon_code']; ?></td>
                <td class="text-left"><?php echo $coupon['coupon_prefix']; ?></td>
                <td class="text-left"><?php echo $coupon['sup_code']; ?></td>
                <td class="text-left">
                <?php if(!empty($coupon['plan_href'])){?>
                <a href="<?php echo $coupon['plan_href']; ?>" target="_blank"><?php echo $coupon['round_name']; ?></a>
              <?php } ?>
                
                </td>
                <td class="text-left"><?php echo $coupon['coupon_status']; ?></td>
               <!-- <td class="text-right"><a href="<?php echo $coupon['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>//--> 
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="text-center" colspan="5"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </form>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $results; ?></div>
        <div class="col-sm-6 text-right">
          <nav><?php echo $pagination; ?></nav>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript"><!--
  function filter() {
    url = 'index.php?route=catalog/coupon';
  
    var filter_name = $('input[name=\'filter_name\']').val();
  
    if (filter_name) {
      url += '&filter_name=' + encodeURIComponent(filter_name);
    }
    var filter_sup_id = $('select[name=\'filter_sup_id\']').val();
  
    if (filter_sup_id != '*') {
      url += '&filter_sup_id=' + encodeURIComponent(filter_sup_id);
    }

    var filter_coupon_status = $('select[name=\'filter_coupon_status\']').val();
  
    if (filter_coupon_status != '*') {
      url += '&filter_coupon_status=' + encodeURIComponent(filter_coupon_status);
    }

    
    location = url;
  }

  $('#huy_coupon').on('click',function(){
    $('#form-upload > label').html('Hủy Coupon');
    $('#form-upload').attr('action', 'index.php?route=catalog/coupon/huy_coupon').submit();
  });
   
  $('#unlock_coupon').on('click',function(){
    $('#form-upload > label').html('Unlock coupon');
    $('#form-upload').attr('action', 'index.php?route=catalog/coupon/unlock_coupon').submit();
  });
   
  //-->
</script>

