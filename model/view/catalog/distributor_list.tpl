

<div class="page-header">
  <div class="container-fluid">
    <div class="pull-right">
    
    <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
     <!--  //--> <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-problem').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
  </div>
</div>
<div class="container-fluid">
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="box" id="filter-area">
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label" for="filter_global"><?php echo $text_search; ?></label>
            <input autocomplete="false" type="text" name="filter_global" value="<?php echo $filter_global; ?>" placeholder="<?php echo $text_search; ?>" id="filter_global" class="form-control" onchange="filter()"/>
          </div>
        </div> <!--//readonly onfocus="this.removeAttribute('readonly');"-->
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label" for="filter_region_code"><?php echo $text_region; ?></label>
            <select name="filter_region_code" id="filter_region_code" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_all; ?></option>
              <?php foreach ($regions as $region) { ?>
                <option value="<?php echo $region['region_code'];?>" <?php if ($region['region_code']==$filter_region_code) { ?>selected="selected"<?php } ?>><?php echo $region['region_name'];?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label" for="filter_coupon_check"><?php echo $text_coupon_check; ?></label>
            <select name="filter_coupon_check" id="filter_coupon_check" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_all; ?></option>
                <option value="1" <?php if ($filter_coupon_check==1) { ?>selected="selected"<?php } ?>><?php echo $text_yes;?></option>
                <option value="0" <?php if ($filter_coupon_check==0&&!is_null($filter_coupon_check)) { ?>selected="selected"<?php } ?>><?php echo $text_no;?></option>
            </select>
          </div>
        </div>
        <div class="col-sm-3">
          <button type="button" id="button-clear" class="btn btn-primary pull-right"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>
        </div>
      </div>
    </div>
    <!--panel-body -->
  </div>
  <!--box -->
  <div class="box" id="filter-area">
    <div class="panel-header"><a onclick="loadImport('<?php echo $template_id;?>');" class="btn-xs pull-right"><i class="fa fa-download"></i> <?php echo $text_import_history; ?></a></div>
    <div class="panel-body">
      <div class="form-group clearfix" style="margin-top:20px;">
        <form action="<?php echo $form;?>" id="form-upload" enctype="multipart/form-data" method="post">
          <?php if($has_add) { ?>
          <label class="control-label col-sm-2" for="input-import"><?php echo $text_import; ?></label>
          <div class="col-sm-6">
            <div class="input-group">
              <input type="file" id="file_import" name="file_import" class="form-control">
              <div class="input-group-btn">
                <button class="btn btn-primary" type="submit" data-loading-text="Importing..." id="button-import"><?php echo $text_import; ?></button>
              </div>
            </div>
          </div>
          <?php } ?>
          <div class="col-sm-4">
            <a href="<?php echo $template_url; ?>" class="btn btn-success"><i class="fa fa-download"></i> Tải mẫu excel</a>
            <!-- //--> 
            <?php if($has_edit && isset($export)) { ?>
            <a  href="<?php echo $export;?>" class="btn btn-primary btn-flat pull-right"><i class="fa fa-download"></i> <?php echo $text_download_results; ?></a>
            <?php } ?>
          </div>
          <input type="hidden" name="template_id" value="<?php echo $template_id; ?>">
          <?php if($history_id>0){?>
              <script type="text/javascript"><!--
               loadImport('<?php echo $template_id;?>');
                //-->
              </script>
         <?php } ?>
         

        </form>
      </div>
    </div>
  </div>
  <!-- /.box -->
  <div class="box">
    <div class="box-header">
      <h2 class="box-title">
      <?php echo $heading_title; ?></h3>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $results; ?></div>
        <div class="col-sm-6 text-right">
          <nav><?php echo $pagination; ?></nav>
        </div>
      </div>
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-problem">
        <div class="table-responsive">
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                <td class="text-left"><?php if ($sort == 'distributor_name') { ?>
                  <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $text_name; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_name; ?>"><?php echo $text_name; ?></a>
                  <?php } ?>
                </td>
                <td class="text-left"><?php echo $text_code; ?></td>
                <td class="text-left"><?php echo $text_coupon_check; ?></td>
                <td class="text-left"><?php echo $text_region; ?></td>
                <td class="text-left"><?php echo $text_description; ?></td>
                <td class="text-left"></td>
                
                <td class="text-right"><?php echo $text_action; ?></td>
              </tr>
            </thead>
            <tbody>
              <?php if ($problems) { ?>
              <?php foreach ($problems as $problem) { ?>
              <tr>
                <td class="text-center">
                  <input type="checkbox" name="selected[]" value="<?php echo $problem['distributor_id']; ?>" <?php if (in_array($problem['distributor_id'], $selected)) { ?>checked="checked"<?php } ?> id="pr<?php echo $problem['distributor_id']; ?>"/><label for="pr<?php echo $problem['distributor_id']; ?>">&nbsp;</label>
                </td>
                <td class="text-left"><?php echo $problem['distributor_name']; ?></td>
                <td class="text-left"><?php echo $problem['distributor_code']; ?></td>
                <td class="text-left"><?php echo ($problem['coupon_check']==1)?$text_yes:$text_no; ?></td>
                <td class="text-left"><?php echo $problem['region_code']; ?></td>
                <td class="text-left"><?php echo $problem['distributor_address']; ?></td>
                <td class="text-left">
              <?php if(!empty($problem['real_token'])){?>
                <a onclick="decodeVal('<?php echo $problem['real_token']; ?>');"><i class="fa fa-eye"></i></a>
             
               <?php } ?>
                </td>
                <td class="text-right"><a href="<?php echo $problem['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </form>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $results; ?></div>
        <div class="col-sm-6 text-right">
          <nav><?php echo $pagination; ?></nav>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="modal-pass" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        Vui lòng điền khóa bí mật:
      </div>
      <div class="modal-body">
        <div class="mpadding">
        
            <div class="input-group">
         <input type="text" name="hidden_key" value="" placeholder="Khóa bí mật" id="hidden_key" class="form-control">
 			<span class="input-group-btn">
              <input type="text" name="decode_val" value="" id="decode_val" class="form-control" style="min-width:400px; text-align:center;"/>
              </span>
         </div>
            
            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success pull-right" data-dismiss="modal" aria-hidden="true" onclick="$('#hidden_key,#decode_val').val('');">Đóng</button>
        <a id="btn-modal-pass" class="btn btn-danger pull-right" style="margin-right:15px;">Tìm</a>
      </div>
    </div>
  </div>
</div>
<!--//module-modal -->


<script type="text/javascript"><!--
    function decodeVal(encode_val){
      $('#modal-pass').modal('show');
      $('#btn-modal-pass').click(function(event) {
        event.preventDefault();
		  $.ajax({
				url:'index.php?route=plan/plan/decodeVal',
				type: 'POST',
				dataType: 'json',
				data: 'hidden_key=' + $('#hidden_key').val()+'&encode_val=' + encode_val,
				success: function(json) {
					if(json['success']){
						$('#message').html('<div class="alert alert-success alert-sm"><i class="fa fa-check-circle"></i> '+json['success']+'<button type="button" class="close" data-dismiss="alert">×</button></div>');
					
						if(json['decode_val']) {
							$('#decode_val').val(json['decode_val']);
						}
					}
					if(json['error']){
						$('#message').html('<div class="alert alert-danger alert-sm"><i class="fa fa-check-circle"></i> '+json['error']+'<button type="button" class="close" data-dismiss="alert">×</button></div>');
					}
				}
			});
      });
    }
    //-->
</script>



<script type="text/javascript"><!--
  function filter() {
    url = 'index.php?route=catalog/distributor';
  
  var filter_global = $('input[name=\'filter_global\']').val();
  
    if (filter_global) {
      url += '&filter_global=' + encodeURIComponent(filter_global);
    }
    var filter_region_code = $('select[name=\'filter_region_code\']').val();
  
    if (filter_region_code != '*') {
      url += '&filter_region_code=' + encodeURIComponent(filter_region_code);
    }
    var filter_coupon_check = $('select[name=\'filter_coupon_check\']').val();
  
    if (filter_coupon_check != '*') {
      url += '&filter_coupon_check=' + encodeURIComponent(filter_coupon_check);
    }
	
	
    location = url;
  }

  //-->
</script>
