<?php echo $header;?>
          <!-- Your Page Content Here -->
          
             

  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-code" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="box">
      <div class="box-header">
        <h2 class="box-title"> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-code" class="form-horizontal">
        
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-coupon_code"><?php echo $text_code; ?></label>
            <div class="col-sm-10">
              <input type="text" name="coupon_code" value="<?php echo $coupon_code; ?>" placeholder="<?php echo $coupon_code; ?>" id="input-coupon_code" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sup_code"><?php echo $text_sup_user; ?></label>
            <div class="col-sm-10">
            
            <select name="sup_code" id="input-sup_code" class="form-control chosen">
                <?php foreach ($users as $user) { ?>
                <option value="<?php echo $user['usercode'];?>" <?php if ($user['usercode']==$sup_code) { ?>selected="selected"<?php } ?>><?php echo $user['usercode'];?> - <?php echo $user['fullname'];?></option>
              <?php } ?>
              </select>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</section>
    <!-- /.content -->

<?php echo $footer;?>