  <?php if($user_logged){?>
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="<?php echo $user['image'];?>" class="user-image" alt="<?php echo $user['fullname'];?>">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xxs"><?php echo $user['fullname'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="<?php echo $user['image'];?>" class="img-circle" alt="<?php echo $user['fullname'];?>">

                <p>
                  <?php echo $user['fullname'];?>
                  <small><?php echo $user['group'];?></small>
                </p>
              </li>
            
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo $user['edit'];?>" class="btn btn-default btn-flat"><?php echo $text_profile;?></a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo $user['logout'];?>" class="btn btn-default btn-flat"><?php echo $text_logout;?></a>
                </div>
              </li>
            </ul>
          </li>
  <?php } ?>