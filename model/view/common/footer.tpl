</div> <!-- /.content -->
</div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      <?php echo $text_version;?><!-- To the right
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-eye"></i> Plan chưa xem</a> -->
    </div>
    <!-- Default to the left -->
    <?php echo $text_footer;?>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <div class="tab-content">
         <div class="tab-plan">
    </div>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
<script src="<?php echo $http_server;?>assets/project/init2.js?time=<?php echo rand(5, 15);?>" type="text/javascript"></script>

<script type="text/javascript"><!--
$(document).on('keydown', function(e) {
	if ($("body").hasClass('modal-open')&&e.keyCode == 27) { 
		$(".modal.in").modal('hide');
	} 
});
//-->

  var params = location.search.split('&');
  for (var i = 0; i < params.length; i++) {
    var ele = params[i].split('=');
    $("[name='"+ele[0]+"']").val(decodeURIComponent(ele[1]));
    if ($("[name='"+ele[0]+"']").hasClass('chosen')) {
      $("[name='"+ele[0]+"']").trigger("chosen:updated");
    }
  }

  function filter(){
    var url = 'index.php'+params[0];
    var inputs = $('.filter');
    for (var i = 0; i < inputs.length; i++) {
      var input = inputs.eq(i);
      var tagName = input.prop("tagName");
      var val = input.val();
      if ((tagName != 'SELECT' && val != '') || (tagName == "SELECT" && val != '*')) {
        url += '&' + input.attr('name') + '=' + val;
      }
    }
    location = url;
  }

</script>

</body>
</html>
