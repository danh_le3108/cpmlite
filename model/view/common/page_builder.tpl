<?php echo $header;?>
<?php if(!empty($breadcrumbs)){?>
<ul class="breadcrumb">
<?php foreach ($breadcrumbs as $breadcrumb) { ?>
<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
<?php } ?>
</ul>
<?php } ?>

<?php
if($layout['num_widget']>0){
$i=0;?>
<section class="clearfix" style="padding-bottom: 200px">
          <!-- Your Page Content Here -->
  <div id="id_<?php echo $layout['layout_id'];?>" class="clearfix">
 	<?php foreach ($layout['layouts'] as $row) {
    $i++; ?>
<div id="<?php echo $layout['layout_id'].'_'.$i;?>" class="relative <?php echo $row->elem_class?> clearfix <?php echo ($row->elem_bg_paralax!='')?'ave_parallax ave_parallax'.$row->elem_bg_paralax:'';?>">
   <div class="parallax-overlay"></div>
            <div <?php echo (!empty($row->elem_id))?'id="'.$row->elem_id.'"':'';?> class="<?php echo $row->elem_container?> clearfix">
            <div class="row">
                <?php foreach( $row->cols as $col ) {
                $split_col = isset($col->rows)?'split-col':'';?>
                    <div class="<?php echo  $split_col;?> col-lg-<?php echo $col->col_lg; ?> col-md-<?php echo $col->col_md;?> col-sm-<?php echo $col->col_sm;?> col-xs-<?php echo $col->col_xs;?>">
                    <div class="child-col clearfix">
                        <?php foreach ( $col->widgets as $widget ){ ?>
                            <?php if( isset($widget->content) ) { ?>
                                    <?php echo $widget->content; ?>
                            <?php } ?>
                        <?php } ?>
                        <?php if (isset($col->rows)&&$col->rows) { ?>
                           <?php foreach ($col->rows as $row) {?>
                                    <div class="child-row clearfix">
                                        <div class="row <?php echo $row->elem_class?>">
                                            <?php foreach( $row->cols as $col ) { ?>
                                                <div class="col-lg-<?php echo $col->col_lg; ?> col-md-<?php echo $col->col_md;?> col-sm-<?php echo $col->col_sm;?> col-xs-<?php echo $col->col_xs;?>">
                                                     <div class="child-col clearfix">
                                                    <?php foreach ( $col->widgets as $widget ){ ?>
                                                        <?php if( isset($widget->content) ) { ?>
                                                                <?php echo $widget->content; ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    </div>
                                                </div><!--//col --> 
                                            <?php } ?>
                                        </div><!--//row --> 
                                    </div><!--//child-row --> 
                            <?php } ?>
                            <!-- $col->rows--> 
                        <?php } ?>
                        </div><!--//bordered --> 
                    </div><!--//col --> 
                <?php } ?>
            </div><!--//row --> 
        </div><!--//container --> 
        </div ><!--//section --> 
    <?php } ?>
  </div><!--//end position -->
</section>
    <!-- /.content -->
<?php } ?>
<?php echo $footer;?>