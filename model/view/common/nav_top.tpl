   <?php
   	$i = 1;
    foreach ($menus as $menu) {
		  //if($i>=5){ continue; }
		   ?>
      <?php if ($menu['children']) { ?>
      <li class="dropdown">
          <a <?php if ($menu['href']) { ?>href="<?php echo $menu['href']; ?>"<?php } ?> class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa <?php echo $menu['icon']; ?>"></i> <span><?php echo $menu['name']; ?></span>
            <span class="caret"></span>
          </a>
      <ul class="dropdown-menu" role="menu">
        <?php foreach ($menu['children'] as $children_1) { ?>
        <li>
          <?php if ($children_1['href']) { ?>
          <a href="<?php echo $children_1['href']; ?>"><?php echo $children_1['name']; ?></a>
          <?php } else { ?>
          <a class="parent"><?php echo $children_1['name']; ?></a>
          <?php } ?>
          <?php if ($children_1['children']) { ?>
          <ul>
            <?php foreach ($children_1['children'] as $children_2) { ?>
            <li>
              <?php if ($children_2['href']) { ?>
              <a href="<?php echo $children_2['href']; ?>"><?php echo $children_2['name']; ?></a>
              <?php } else { ?>
              <a class="parent"><?php echo $children_2['name']; ?></a>
              <?php } ?>
              <?php if ($children_2['children']) { ?>
              <ul>
                <?php foreach ($children_2['children'] as $children_3) { ?>
                <li><a href="<?php echo $children_3['href']; ?>"><?php echo $children_3['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
          <?php } ?>
        </li>
        <?php } ?>
      </ul>
      <?php }else{ ?>
      
    <li id="<?php echo $menu['id']; ?>">
      <a <?php if ($menu['href']) { ?>href="<?php echo $menu['href']; ?>"<?php } ?>><i class="fa <?php echo $menu['icon']; ?>"></i> <span><?php echo $menu['name']; ?></span></a>
        <?php } ?>
    </li>
    <?php  $i++; } ?>