<div class="clearfix">
    <div id="content" class="col-sm-12">
    
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  
     <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title"><?php echo $text_my_account; ?></h3>
        <div class="pull-right box-tools"> </div>
      </div>
      <div class="panel-body">
       <ul class="list-unstyled">
        <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
        <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
      </ul>
      </div><!--panel-body --> 
      </div><!--box -->
      
    </div>
</div>