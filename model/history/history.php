<?php
class ModelHistoryHistory extends Model {
	public function add($data = array()) {
		if (!empty($data)) {
			$sql = "INSERT INTO " . PDB_PREFIX . "log_history SET ";
			foreach ($data as $key => $value) {
				$sql .= "{$key} = '" . $this->pdb->escape($value) . "',";
			}
			$sql .= "date_added = NOW()";
			$this->pdb->query($sql);
		}
	}

	public function getHistories($data = array()) {
		$sql =  "SELECT * FROM " . PDB_PREFIX . "log_history h WHERE 1";

		if(isset($data['plan_id']) && !empty($data['plan_id'])) {
			$sql .= " AND h.table_primary_id = '".(int)$data['plan_id']."'";
		}

		$sql .= " ORDER BY date_added DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 6;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->pdb->query($sql);

		return $query->rows;
	}

	public function getHistory($history_id) {
		
		$sql = "SELECT * FROM " . PDB_PREFIX . " log_history l WHERE id = '" .(int) $history_id. "'";

		$query = $this->pdb->query($sql);

		return $query->row;
	}

	public function getTotalHistories($plan_id) {

		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "log_history WHERE table_primary_id ='" . (int)$plan_id . "'";

		$query = $this->pdb->query($sql);

		return $query->row['total'];
	}
}	