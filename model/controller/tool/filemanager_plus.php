<?php class ControllerToolFilemanagerPlus extends Controller { 
	private $error = array();		
    private $file = 'image_log.txt';
	public function thumb(){	
		$this->load->model('tool/image');

		if (isset($this->request->get['image'])) {
			$thumb = $this->model_tool_image->resize(html_entity_decode(str_replace('media/files/','files/',$this->request->get['image']), ENT_QUOTES, 'UTF-8'), 100, 100);
			$this->response->setOutput($thumb);
		}
	
	}
	public function index() {	
		if (!$this->config->has('image_manager_plus_installed')) {
			$this->response->redirect($this->url->link('tool/filemanager_plus/install', '' , true));
		}
	   $this->document->addScript('assets/editor/plugins/jquery-migrate-1.2.1.min.js');
	   $this->document->addStyle('assets/editor/plugins/jquery-ui/jquery-ui.css');
	   $this->document->addScript('assets/editor/plugins/jquery-ui/jquery-ui.js');  	   
	   $this->document->addStyle('assets/editor/plugins/elfinder/css/elfinder.min.css');   
	   $this->document->addScript('assets/editor/plugins/elfinder/js/elFinder.js');	
	   $this->document->addScript('assets/editor/plugins/elfinder/js/ui/elfinder-ui.js');	
	   $this->document->addScript('assets/editor/plugins/elfinder/js/commands/commands.js');
	   $this->document->addScript('assets/editor/plugins/elfinder/js/i18n/elfinder.en.js');	
	   $this->document->addScript('assets/editor/plugins/elfinder/js/proxy/elFinderSupportVer1.js');
   	
		$language_data = $this->language->load('tool/filemanager_plus');
		foreach($language_data as $key=>$value){
			$data[$key] = $value;
		}	
		$this->document->setTitle($this->language->get('heading_title'));
		$data['image_manager_plus_command']  = $this->config->get('image_manager_plus_command'); 
		$data['image_manager_plus_status']  = $this->config->get('image_manager_plus_status'); 
		
		$this->load->model('user/user');		
       $user_info = $this->model_user_user->getUser($this->user->getId());
	   
       if(!empty($user_info)){
       		$data['user_group_id'] = $user_info['user_group_id'];
       }else{
       		$data['user_group_id'] = FALSE;
       }
	   
		if (isset($this->session->data['error'])) {
    		$data['error_warning'] = $this->session->data['error'];
    
			unset($this->session->data['error']);
 		} elseif (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', '' , true),     		
      		'separator' => false
   		);
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('tool/filemanager_plus', '' , true),
      		'separator' => ' :: '
   		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);
		
		$data['token'] = '';
   		$data['filemanager'] = $this->url->link('tool/filemanager_plus/popup', '' , true);
   		$data['action'] = $this->url->link('tool/filemanager_plus', '' , true);
		$data['clear'] = $this->url->link('tool/filemanager_plus/clear', '' , true);
		$data['uninstall'] = $this->url->link('tool/filemanager_plus/uninstall', '' , true);
		$data['log_url'] = $this->url->link('tool/filemanager_plus/log_file', '' , true);
		$data['clear_cache'] = $this->url->link('tool/filemanager_plus/clear_cache', '' , true);
		
		
		/*Setting*/ 
		
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->load->model('setting/setting');
			$this->model_setting_setting->editSetting('image_manager_plus', $this->request->post);		
					
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('tool/filemanager_plus', '' , true));		
		}
		/*User_group*/ 
		$user_group_data = array();
		$this->load->model('user/user_group');
		$data['user_groups'] = array();
		$user_group_results = $this->model_user_user_group->getUserGroups($user_group_data);
		foreach ($user_group_results as $result) {		
			$data['user_groups'][] = array(
				'user_group_id' => $result['user_group_id'],
				'group_name'          => $result['group_name']
			);
		}
		
		$data['footer'] = $this->load->controller('common/footer');
		$data['breadcrumb'] = $this->load->controller('common/breadcrumb',$data['breadcrumbs']);
		$data['header'] = $this->load->controller('common/header');
				
		
			$this_template = 'tool/elfinder';
		
		$this->response->setOutput($this->load->view($this_template, $data));
	}
	public function clear() {
		$this->load->language('tool/filemanager_plus');
		
		$handle = fopen(DIR_LOGS.$this->file, 'w+'); 
						
		fclose($handle); 			
		
		$this->session->data['success'] = $this->language->get('text_success');
		
		$this->response->redirect($this->url->link('tool/filemanager_plus', '' , true));		
	}
	
	public function log_file() {
		/*Log*/
		if (file_exists(DIR_LOGS.$this->file)) {
			$data['log'] = file_get_contents(DIR_LOGS.$this->file, FILE_USE_INCLUDE_PATH, null);
		} else {
			$data['log'] = '';
		}	
		
		$this_template = 'tool/elfinder_log';
		$this->response->setOutput($this->load->view($this_template, $data));
	}

    protected function log_write($log) {	
		$file = DIR_LOGS . $this->file;		
		$handle = fopen($file, 'a+'); 		
		fwrite($handle, $log);			
		fclose($handle); 
    }
	public function popup(){			
		$elfinder_path = DIR_ROOT .'assets/editor/plugins/elfinder/php/';
		error_reporting(0); 
		ini_set('max_file_uploads', 50);
		ini_set('upload_max_filesize','50M'); 
		require_once($elfinder_path . 'elFinderConnector.class.php');
		require_once($elfinder_path . 'elFinder.class.php');
		require_once($elfinder_path . 'elFinderSimpleLogger.class.php');
		require_once($elfinder_path . 'elFinderVolumeDriver.class.php');
		require_once($elfinder_path . 'elFinderVolumeLocalFileSystem.class.php');
		
		$myLogger = new elFinderSimpleLogger(DIR_LOGS.$this->file);
		
		if (isset($this->session->data['image_access'])) {
			$image_access = $this->session->data['image_access'];
			$session_token = $this->session->data['token'];
			if($image_access!=$session_token){
				$log = 'At time: ['.date('d.m H:s')."]\n";
				$log .= "\tUser Access: ".$this->user->getUserName()."\n";
				$log .= "\tIP Address: ".$this->request->server['REMOTE_ADDR'];
				unset($this->session->data['image_access']);
			}
		} else {
			$this->session->data['image_access'] = $this->session->data['token'];
		}
		
		
		$image_path = HTTP_SERVER;
		//$DIR_MEDIA = str_replace('/','\\',DIR_MEDIA);
		$DIR_MEDIA = (strpos(DIR_MEDIA,':')!== false)?str_replace('/','\\',DIR_MEDIA):DIR_MEDIA;
		
		$opts = array(
		'bind' => array(
			'mkdir mkfile rename duplicate upload rm paste' => array($myLogger, 'log'),
		),
		'roots' => array(
				array(
					'driver'     => 'LocalFileSystem',
					'path'       => $DIR_MEDIA.'files/'.$this->config->get('config_project_folder'), 
					'startPath'  => $DIR_MEDIA.'files/'.$this->config->get('config_project_folder'), 
					'URL'        => $image_path.'media/files/'.$this->config->get('config_project_folder'), 
					// 'alias'      => 'File system',
					'uploadOrder'  => 'deny,allow',
					'mimeDetect' => 'internal',
					'tmbPath'    => $DIR_MEDIA.'thumb/',         // tmbPath to files (REQUIRED)
					'tmbURL'     => $image_path.'media/thumb/',
					'utf8fix'    => true,
					//'uploadMaxSize'    => '0',
					'uploadMaxSize'    => '128M',
					'tmbCrop'    => false,
					'tmbBgColor' => 'transparent',
					'accessControl' => 'access',
					'copyOverwrite' => false,
					'uploadOverwrite' => false,
					// 'uploadDeny' => array('application', 'text/xml')
				)		
			)
		);
		// run elFinder
		$this->log_write($log);
		$connector = new elFinderConnector(new elFinder($opts));
		$connector->run();
	}
	public function clear_cache() {
		$this->language->load('tool/filemanager_plus');
		if ($this->user->hasPermission('edit', 'tool/filemanager_plus')) {
			$imgfiles = glob(DIR_MEDIA . 'cache/*');
			foreach($imgfiles as $imgfile){
						 $this->deldir($imgfile);
			}
			$this->session->data['success'] = $this->language->get('text_success');
    	}else{		
      		$this->session->data['error'] = $this->language->get('error_permission');  
		}		
		$this->response->redirect($this->url->link('tool/filemanager_plus', '' , true));		
	}
    private function deldir($dirname){         
		if(file_exists($dirname)) {
			if(is_dir($dirname)){
                            $dir=opendir($dirname);
                            while($filename=readdir($dir)){
                                    if($filename!="." && $filename!=".."){
                                        $file=$dirname."/".$filename;
					$this->deldir($file); 
                                    }
                                }
                            closedir($dir);  
                            rmdir($dirname);
                        }
			else {@unlink($dirname);}			
		}
	}
	protected function validate() {
		if (!$this->user->hasPermission('edit', 'tool/filemanager_plus')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	public function install() {
		//image_manager_status
		$this->load->model('setting/setting');
		$this->model_setting_setting->editSetting('image_manager_plus_installed', array('image_manager_plus_installed' => 1));
	
			$this->db->query("DELETE FROM `". DB_PREFIX ."setting` WHERE `code` = 'image_manager_plus'");	
			$this->db->query("INSERT INTO `". DB_PREFIX ."setting` (`project_id`,`code`, `key`, `value`, `serialized`) VALUES
	(0,'image_manager_plus', 'image_manager_plus_command', 'a:1:{i:1;a:20:{s:5:\"mkdir\";s:1:\"1\";s:6:\"mkfile\";s:1:\"1\";s:6:\"upload\";s:1:\"1\";s:6:\"reload\";s:1:\"1\";s:7:\"getfile\";s:1:\"1\";s:2:\"up\";s:1:\"1\";s:8:\"download\";s:1:\"1\";s:2:\"rm\";s:1:\"1\";s:9:\"duplicate\";s:1:\"1\";s:6:\"rename\";s:1:\"1\";s:4:\"copy\";s:1:\"1\";s:3:\"cut\";s:1:\"1\";s:5:\"paste\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:7:\"extract\";s:1:\"1\";s:7:\"archive\";s:1:\"1\";s:4:\"view\";s:1:\"1\";s:6:\"resize\";s:1:\"1\";s:4:\"sort\";s:1:\"1\";s:6:\"search\";s:1:\"1\";}}', 1);");
			$this->db->query("INSERT INTO `". DB_PREFIX ."setting` (`project_id`,`code`, `key`, `value`, `serialized`) VALUES
	(0,'image_manager_plus', 'image_manager_plus_status', '1', 0);");	

				$this->session->data['session_redirect'] = $this->url->link('tool/filemanager_plus', '' , true);
				$this->response->redirect($this->url->link('common/home', '' , true));
			
	}
	public function uninstall() {	
		if($this->validate()){		
			$this->db->query("DELETE FROM `" . DB_PREFIX. "setting` WHERE `key` = 'image_manager_plus_installed'");			
			$this->db->query("DELETE FROM `". DB_PREFIX ."setting` WHERE `code` = 'image_manager_plus'");	
			$this->db->query("DELETE FROM `" . DB_PREFIX. "modification` WHERE `code` LIKE '%image_manager_plus%'");
			$this->response->redirect($this->url->link('common/home', '' , true));
		}else{
			$this->response->redirect($this->url->link('tool/filemanager_plus', '' , true));
		}
	}
}
?>