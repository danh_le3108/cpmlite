<?php
class ControllerCatalogAttribute extends Controller{
	private $error = array();
	
	private $field_data = array(
			'attribute_id'=>'',
			'attribute_name'=>'',
			'attribute_code'=>'',
			'capacity'=>'',
			'group_code'=>'',
			'sort_order'=>''
		);
	private $filter_key = array(
			'filter_global',
			'filter_group_code',
			'filter_name'
		);
	private function _url(){
		$url = '';
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(trim(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8')));
			}
		}
		return $url;
	}
	
	public function index() {
		p(json_encode(array(array('id'=>'1','name'=>'Có'),array('id'=>'2','name'=>'Không'))));
		// p(json_encode(array(array('id'=>'1','name'=>'ON'),array('id'=>'2','name'=>'OFF'))));
		// p(json_encode(array(
		// 	array('id'=>'1','name'=>'Poster'),
		// 	array('id'=>'2','name'=>'Sticker dán bàn'),
		// 	array('id'=>'3','name'=>'Sticker tủ lạnh'),
		// 	array('id'=>'4','name'=>'Banner'),
		// 	array('id'=>'5','name'=>'Bảng hiệu'),
		// 	array('id'=>'6','name'=>'Mái che di động'),
		// 	array('id'=>'7','name'=>'Tranh treo tường'),
		// 	array('id'=>'9','name'=>'Standee'),
		// 	array('id'=>'10','name'=>'Khác')
		// )));
		// p(json_encode(array(
		// 	array('id'=>'1','name'=>'Pepsi'),
		// 	array('id'=>'2','name'=>'7Up'),
		// 	array('id'=>'3','name'=>'Sting'),
		// 	array('id'=>'4','name'=>'Mirinda'),
		// 	array('id'=>'5','name'=>'Twister'),
		// 	array('id'=>'6','name'=>'Revive'),
		// 	array('id'=>'7','name'=>'Trà Oolong Tea+'),
		// 	array('id'=>'8','name'=>'Mountain Dew'),
		// 	array('id'=>'9','name'=>'Lipton'),
		// 	array('id'=>'10','name'=>'Khác')
		// )));
		// p(json_encode(array(
		// 	array('id'=>'1','name'=>'Trên công cụ trưng bày'),
		// 	array('id'=>'2','name'=>'Khu vực bán hàng'),
		// 	array('id'=>'3','name'=>'Mặt tiền cửa hàng'),
		// 	array('id'=>'4','name'=>'Trên tường cửa hàng')
		// )));

		// p(json_encode(array(
		// 	array('id'=>'1','name'=>'TL 1200L','type'=>1),
		// 	array('id'=>'2','name'=>'TL 1300L','type'=>1),
		// 	array('id'=>'3','name'=>'TL 150L','type'=>1),
		// 	array('id'=>'4','name'=>'TL 250L','type'=>1),
		// 	array('id'=>'5','name'=>'TL 400L (3 ngăn, 1 combo)','type'=>1),
		// 	array('id'=>'6','name'=>'TL 400L (4 ngăn, 1 combo)','type'=>1),
		// 	array('id'=>'7','name'=>'TL 400L (5 ngăn)','type'=>1),
		// 	array('id'=>'8','name'=>'TL 450L (Classic)','type'=>1),
		// 	array('id'=>'9','name'=>'TL 500L','type'=>1),
		// 	array('id'=>'10','name'=>'TL 650L (5 ngăn)','type'=>1),
		// 	array('id'=>'11','name'=>'Kệ CVS 60cm','type'=>0),
		// 	array('id'=>'12','name'=>'Kệ CVS 90 cm','type'=>0),
		// 	array('id'=>'13','name'=>'Kệ RGB (2 ket)','type'=>0),
		// 	array('id'=>'14','name'=>'Kệ RGB (3 ket)','type'=>0),
		// 	array('id'=>'15','name'=>'Kệ RGB (1 ket)','type'=>0),
		// 	array('id'=>'16','name'=>'Kệ trà','type'=>0),
		// 	array('id'=>'17','name'=>'Kệ AQF','type'=>0),
		// )));

		// p(json_encode(array(
		// 	array('id'=>'1','name'=>'Mới lắp'),
		// 	array('id'=>'2','name'=>'Đúng DS'),
		// 	array('id'=>'3','name'=>'Chuyển CH')
		// )));
		// p(json_encode(array(
		// 	array('id'=>'1','name'=>'1mặt'),
		// 	array('id'=>'2','name'=>'Không quan sát được')
		// )));
		// p(json_encode(array(
		// 	array('id'=>'1','name'=>'CC Lemon'),
		// 	array('id'=>'2','name'=>'Mountain Dew'),
		// 	array('id'=>'3','name'=>'Tea+'),
		// 	array('id'=>'4','name'=>'7UP')
		// )));
		// $sql = "SELECT * FROM `cpm_local_district` WHERE `province_id` = 79 ORDER BY `district_alias`";
		// $districts = $this->pdb->query($sql)->rows;
		// $ds = array();
		// foreach ($districts as $d) {
		// 	$ds[] = array(
		// 		'id' => $d['district_name'],
		// 		'value' => $d['district_name'],
		// 		'province_id' => 79
		// 	);
		// }
		// p($ds);exit();
		// echo 'district';
		// p(json_encode($ds));
		// echo 'province';
		// p(json_encode(array(array('id'=>'79','value'=>'Hồ Chí Minh'))));
		
		// $sql = "SELECT * FROM `cpm_local_ward` WHERE `province_id` = 79 order by `ward_alias`";
		// $wards = $this->pdb->query($sql)->rows;
		// $ds = array();
		// foreach ($wards as $key => $d) {
		// 	$ds[] = array(
		// 		'id' => $d['ward_name'],
		// 		'value' => $d['ward_name'],
		// 		'store_district' => $d['district_name']
		// 	);
		// }
		// p($wards);
		// p($ds);exit();
		// echo 'ward';
		// p(json_encode($ds));

		// $sql = "SELECT * FROM `cpm_local_streets` WHERE `province_id` = 79 order by street_name";
		// $streets = $this->pdb->query($sql)->rows;
		// $ds = array();
		// foreach ($streets as $key => $d) {
		// 	$ds[] = array(
		// 		'id' => $d['name'],
		// 		'value' => $d['name'],
		// 		'store_ward' => $d['ward_name']
		// 	);
		// }

		// echo 'street';
		// p($streets);exit();
		// p(json_encode($ds));
		$this->load->model('localisation/province');
		$province = $this->model_localisation_province->getProvinces();
		$p = array();
		// foreach ($province as $v) {
		// 	$p[] = array(
		// 		'id' => $v['province_id'],
		// 		'name' => $v['name']
		// 	);
		// }
		// p(json_encode($p),1);
		$data['ajax_list'] = $this->ajax_list();
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$template = 'common/ajax_list';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
	public function getList() {
		$this->response->setOutput($this->ajax_list());
	}
	public function ajax_list() {
		$languages = $this->load->language('catalog/attribute');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'icon' => 'fa-dashboard',
			'href' => $this->url->link('common/home')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/attribute')
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);
		
		if (!empty($this->session->data['error_warning'])) {
			$data['error_warning'] = $this->session->data['error_warning'];
			unset($this->session->data['error_warning']);
		}
		if (!empty($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}
		$this->load->model('catalog/attribute');
		$data['attributes'] = $this->model_catalog_attribute->getAttributes($this->request->get);
		$this->load->model('catalog/group');
		$data['groups'] = $this->model_catalog_group->getGroups();
		
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$template = 'catalog/attribute_list';
		return $this->load->view($template, $data);
	}

	public function save() {
		$this->load->model('catalog/attribute');
		if (!empty($this->request->get['attribute_id'])) {
			$this->request->post['options'] = str_replace('&quot;', '"', $this->request->post['options']);
			$attribute_id = $this->request->get['attribute_id'];
			$this->model_catalog_attribute->edit($attribute_id, $this->request->post);
		} else {
			$this->request->post['options'] = str_replace('&quot;', '"', $this->request->post['options']);
			$this->model_catalog_attribute->add($this->request->post);
		}	
	}

	public function delete() {
		if (!empty($this->request->post['attribute_ids'])) {
			$sql = "DELETE FROM cpm_attribute WHERE attribute_id IN (" . implode(',', $this->request->post['attribute_ids']) . ")";
			$this->pdb->query($sql);
		}
	}
}