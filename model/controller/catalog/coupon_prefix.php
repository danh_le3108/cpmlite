<?php
class ControllerCatalogCouponPrefix extends Controller{
	private $error = array();
	
	private $field_data = array(
			'prefix_id'=>'',
			'prefix_title'=>'',
			'coupon_prefix'=>'',
			'prefix_value'=>''
		);

	private function _url(){
		$url = '';
		$url_key = array(
			'filter_prefix',
			'filter_code'
		);
		foreach($url_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}
	public function delete() {
		$languages = $this->load->language('catalog/coupon_prefix');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/coupon_prefix');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $prefix_id) {
				$this->model_catalog_coupon_prefix->deleteCouponPrefix($prefix_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = $this->_url();

			$this->response->redirect($this->url->link('catalog/coupon_prefix',  $url , true));
		}

		return $this->index();
	}

	public function index() {
		$data['ajax_list'] = $this->ajax_list();
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$template = 'common/ajax_list';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
	public function getList() {
		$this->response->setOutput($this->ajax_list());
	}
	public function ajax_list() {
		$languages = $this->load->language('catalog/coupon_prefix');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/coupon_prefix');
		
		$filter_key = array(
			'filter_name'=>null
		);
		foreach($filter_key as $key=>$value){
			if (isset($this->request->get[$key])) {
				$$key = $this->request->get[$key];
			} else {
				$$key = $value;
			}
		}
		
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'prefix_value';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = $this->_url();
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['add'] = $this->url->link('catalog/coupon_prefix/add',  $url , true);
		$data['delete'] = $this->url->link('catalog/coupon_prefix/delete',  $url , true);

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '' , true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/coupon_prefix',  $url , true)
		);

		$this->document->setBreadcrumbs($data['breadcrumbs']);

		$data['prefixs'] = array();

		$filter_data = array(
			'filter_name'=>$filter_name,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_project'),
			'limit' => $this->config->get('config_limit_project')
		);

		$query_total = $this->model_catalog_coupon_prefix->getTotalCouponPrefixs($filter_data);

		$results = $this->model_catalog_coupon_prefix->getCouponPrefixs($filter_data);

		foreach ($results as $result) {
			$data['prefixs'][] = array(
				'prefix_id' => $result['prefix_id'],
				'prefix_title' => $result['prefix_title'],
				'coupon_prefix'       => $result['coupon_prefix'],
				'prefix_value'       => $result['prefix_value'],
				'edit'       => $this->url->link('catalog/coupon_prefix/edit', '&prefix_id=' . $result['prefix_id'] . $url , true)
			);
		}


		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = $this->_url();

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/coupon_prefix', '&sort=name' . $url , true);

		$url = $this->_url();

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $query_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_project');
		$pagination->url = $this->url->link('catalog/coupon_prefix/getList', $url . '&page={page}' , true);

		$data['pagination'] = $pagination->render();

		$data['results'] = $pagination->results();

		$data['sort'] = $sort;
		$data['order'] = $order;

		
		$data['delete'] = $this->url->link('catalog/coupon_prefix/delete',  $url , true);

		$data['filter_name'] = $filter_name;
		
		
		
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$template = 'catalog/coupon_prefix_list';
		return $this->load->view($template, $data);
	}

	public function add() {
		$languages = $this->load->language('catalog/coupon_prefix');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/coupon_prefix');

		if ($this->user->hasPermission('add', 'catalog/coupon_prefix')) {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
				
				$this->model_catalog_coupon_prefix->addCouponPrefix($this->request->post);
	
				$this->session->data['success'] = $this->language->get('text_success');
	
				$url = $this->_url();
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
	
				$this->response->redirect($this->url->link('catalog/coupon_prefix',  $url , true));
			}
		}else{
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return $this->getForm();
	}

	public function edit() {
		$languages = $this->load->language('catalog/coupon_prefix');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/coupon_prefix');

		if ($this->user->hasPermission('edit', 'catalog/coupon_prefix')) {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
				$this->model_catalog_coupon_prefix->editCouponPrefix($this->request->get['prefix_id'], $this->request->post);
				$this->session->data['success'] = $this->language->get('text_success');
	
				$url = $this->_url();
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
	
				$this->response->redirect($this->url->link('catalog/coupon_prefix',  $url , true));
			}
		}else{
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return $this->getForm();
	}

	protected function getForm() {
		$languages = $this->load->language('catalog/coupon_prefix');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));
		
		
		$error_data = array(
			'error_warning'=>'',
			'error_prefix_value'=>''
		);

		foreach($error_data as $key=>$default_value){
			if (isset($this->error[$key])) {
				$data[$key] = $this->error[$key];
			} else {
				$data[$key] = $default_value;
			}
		}

		$url = $this->_url();
		
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '' , true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/coupon_prefix',  $url , true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);

		if (!isset($this->request->get['prefix_id'])) {
			$data['action'] = $this->url->link('catalog/coupon_prefix/add',  $url , true);
		} else {
			$data['action'] = $this->url->link('catalog/coupon_prefix/edit', '&prefix_id=' . $this->request->get['prefix_id'] . $url , true);
		}

		$data['cancel'] = $this->url->link('catalog/coupon_prefix',  $url , true);

		if (isset($this->request->get['prefix_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$query_info = $this->model_catalog_coupon_prefix->getCouponPrefix($this->request->get['prefix_id']);
		}
		foreach($this->field_data as $key=>$default_value){
			if (isset($this->request->post[$key])) {
				$data[$key] = $this->request->post[$key];
			} elseif (!empty($query_info)) {
				$data[$key] = $query_info[$key];
			} else {
				$data[$key] = $default_value;
			}
		}
		

		$template = 'catalog/coupon_prefix_form';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('delete', 'catalog/coupon_prefix')) {
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return !$this->error;
	}
	protected function validateAdd() {
		if (!$this->user->hasPermission('add', 'catalog/coupon_prefix')) {
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		if ((utf8_strlen($this->request->post['prefix_value']) < 6) || (utf8_strlen($this->request->post['prefix_value']) > 7)) {
			$this->error['error_prefix_value'] = $this->language->get('text_error_prefix_value');
		}

		return !$this->error;
	}
	protected function validateForm() {

		if (!$this->user->hasPermission('add', 'catalog/coupon_prefix')) {
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}
		if ((utf8_strlen($this->request->post['prefix_value']) < 6) || (utf8_strlen($this->request->post['prefix_value']) > 7)) {
			$this->error['error_prefix_value'] = $this->language->get('text_error_prefix_value');
		}


		return !$this->error;
	}
}