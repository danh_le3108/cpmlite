<?php
class ControllerEventTheme extends Controller {
	public function index(&$view, &$data, &$output) {
		// This is only here for compatibility with older extensions
		if (substr($view, -3) == 'tpl') {
			$view = substr($view, 0, -3);
		}
		
		if (!is_file(DIR_TEMPLATE  . $view . '.tpl')) {
			exit('Error: Template '.$view.' not found!');
		}
	}
}
