<?php
class ControllerSurveyGroup extends Controller{
	public function index() {
		if (isset($this->request->get['survey_id'])) {
			$data['survey_id'] = $survey_id = $this->request->get['survey_id'];
			$languages = $this->load->language('survey/survey');
			foreach($languages as $key=>$value){
					$data[$key] = $value;
			}
			$this->load->model('survey/survey');
			$data['groups'] =$this->model_survey_survey->getGroupsBySurveyID($survey_id);
			$this->load->model('catalog/attribute');
			$data['attr'] = $this->model_catalog_attribute->getAttributesIndexBy('attribute_id');
			$template = 'survey/group_info';
			return $this->load->view($template, $data);
		}else{
			echo '<div class="alert alert-info">Không có tham số yêu cầu!</div>';
		}
	}
	public function form() {
		if (isset($this->request->get['group_id'])) {
			$data['group_id'] = $group_id = $this->request->get['group_id'];
			$languages = $this->load->language('survey/survey');
			foreach($languages as $key=>$value){
					$data[$key] = $value;
			}
			$this->load->model('survey/survey');
			$data['info'] = $this->model_survey_survey->getGroup($group_id);
			
			
			$this->load->model('catalog/attribute');
			$data['attr_groups'] = $this->model_catalog_attribute->getAttributesGroups();
			
			$template = 'survey/form_group';
			$this->response->setOutput($this->load->view($template, $data));
		}else{
			echo '<div class="alert alert-info">Không có tham số yêu cầu!</div>';
		}
	}
	public function delete() {
		$this->load->language('plan/plan');
			$data['user_id'] = $this->user->getId();
		
		$json = array();
		
		$this->load->model('survey/survey');
			
		if ($this->document->hasPermission('edit', 'survey/survey')&&isset($this->request->post)&&isset($this->request->get['group_id'])) {
			$data['survey_id'] = $survey_id = $this->request->get['survey_id'];
			$this->model_survey_survey->deleteGroup($this->request->get['group_id']);
			$this->session->data['success'] = $json['success'] = $this->language->get('text_success');
		}else{
			$this->session->data['error_warning'] = $json['error'] = $this->language->get('text_error_permission');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function save() {
		$this->load->language('plan/plan');
			$data['user_id'] = $this->user->getId();
		
		$json = array();
		
		$this->load->model('survey/survey');
			
		if ($this->document->hasPermission('edit', 'survey/survey')&&isset($this->request->post)) {
			$data['survey_id'] = $survey_id = $this->request->get['survey_id'];
			$this->request->post['survey_id'] = $survey_id;
			if(isset($this->request->get['group_id'])){
				$this->model_survey_survey->editGroup($this->request->get['group_id'],$this->request->post);
			}else{
				$this->model_survey_survey->addGroup($survey_id,$this->request->post);
			}
			$this->session->data['success'] = $json['success'] = $this->language->get('text_success');
		}else{
			$this->session->data['error_warning'] = $json['error'] = $this->language->get('text_error_permission');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function info() {
		$this->response->setOutput($this->index());
	}
	
}