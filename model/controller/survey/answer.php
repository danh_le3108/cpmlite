<?php
class ControllerSurveyAnswer extends Controller{
	public function index() {
			echo '<div class="alert alert-info">Không có tham số yêu cầu!</div>';
	}
	public function form() {
		if (isset($this->request->get['survey_id'])) {
			$data['survey_id'] = $survey_id = $this->request->get['survey_id'];
			if (isset($this->request->get['question_id'])) {
				$data['question_id'] = $question_id = $this->request->get['question_id'];
				$data['info'] = array('question_id'=>$question_id);
			}
			
			$languages = $this->load->language('survey/survey');
			foreach($languages as $key=>$value){
					$data[$key] = $value;
			}
				$this->load->model('survey/survey');
			
			if (isset($this->request->get['answer_id'])) {
				$data['info'] = $this->model_survey_survey->getAnswer($this->request->get['answer_id']);
				$data['question_id'] = $data['info']['question_id'];
			}
			
			$template = 'survey/form_answer';
			$this->response->setOutput($this->load->view($template, $data));
		}else{
			echo '<div class="alert alert-info">Không có tham số yêu cầu!</div>';
		}
	}
	public function delete() {
		$this->load->language('plan/plan');
			$data['user_id'] = $this->user->getId();
		
		$json = array();
		
		$this->load->model('survey/survey');
			
		if ($this->document->hasPermission('edit', 'survey/survey')&&isset($this->request->post)&&isset($this->request->get['answer_id'])) {
			$data['survey_id'] = $survey_id = $this->request->get['survey_id'];
			$this->model_survey_survey->deleteAnswer($this->request->get['answer_id']);
			$this->session->data['success'] = $json['success'] = $this->language->get('text_success');
		}else{
			$this->session->data['error_warning'] = $json['error'] = $this->language->get('text_error_permission');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function save() {
		$this->load->language('plan/plan');
		$data['user_id'] = $this->user->getId();
		
		$json = array();
		
		$this->load->model('survey/survey');
		if ($this->document->hasPermission('edit', 'survey/survey')&&isset($this->request->post)) {
			
			$data['survey_id'] = $survey_id = $this->request->get['survey_id'];
			
			$this->request->post['survey_id'] = $survey_id;
			
			if(isset($this->request->get['answer_id'])&&$this->request->get['answer_id']!=0){
				$data['answer_id'] = $answer_id = $this->request->get['answer_id'];
				$this->model_survey_survey->editAnswer($answer_id,$this->request->post);
			}else{
				$this->model_survey_survey->addAnswer($this->request->post);
			}
			$this->session->data['success'] = $json['success'] = $this->language->get('text_success');
		}else{
			$this->session->data['error_warning'] = $json['error'] = $this->language->get('text_error_permission');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function info() {
		$this->response->setOutput($this->index());
	}
	
}