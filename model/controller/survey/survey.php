<?php
class ControllerSurveySurvey extends Controller{
	private $error = array();
	
	private $field_data = array(
			'survey_id'=>'',
			'survey_name'=>'',
			'survey_description'=>'',
			'coupon_prefix'=>'',
			'pass_point'=>'',
			'ratio'=>'',
			'is_rating'=>'',
			'sort_order'=>''
		);
		

	private function _url(){
		$url = '';
		$url_key = array(
			'filter_prefix',
			'filter_name'
		);
		foreach($url_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}
	public function delete() {
		$languages = $this->load->language('survey/survey');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('survey/survey');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $survey_id) {
				$this->model_survey_survey->deleteSurvey($survey_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = $this->_url();

			$this->response->redirect($this->url->link('survey/survey',  $url , true));
		}

		return $this->index();
	}

	public function index() {
		$data['ajax_list'] = $this->ajax_list();
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$template = 'common/ajax_list';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
	public function getList() {
		$this->response->setOutput($this->ajax_list());
	}
	public function ajax_list() {
		$languages = $this->load->language('survey/survey');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('survey/survey');
		
		$filter_key = array(
			'filter_name'=>null
		);
		foreach($filter_key as $key=>$value){
			if (isset($this->request->get[$key])) {
				$$key = $this->request->get[$key];
			} else {
				$$key = $value;
			}
		}
		
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'survey_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = $this->_url();
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['add'] = $this->url->link('survey/survey/add',  $url , true);
		$data['delete'] = $this->url->link('survey/survey/delete',  $url , true);

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '' , true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('survey/survey',  $url , true)
		);

		$this->document->setBreadcrumbs($data['breadcrumbs']);

		$data['surveys'] = array();

		$filter_data = array(
			'filter_name'=>$filter_name,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_project'),
			'limit' => $this->config->get('config_limit_project')
		);

		$query_total = $this->model_survey_survey->getTotalSurveys($filter_data);

		$results = $this->model_survey_survey->getSurveys($filter_data);

		foreach ($results as $result) {
			$data['surveys'][] = array(
				'survey_id' => $result['survey_id'],
				'survey_name'       => $result['survey_name'],
				'survey_description'       => $result['survey_description'],
				'coupon_prefix'       => $result['coupon_prefix'],
				'pass_point'       => $result['pass_point'],
				'ratio'       => $result['ratio'],
				'sort_order'       => $result['sort_order'],
				'edit'       => $this->url->link('survey/survey/build', '&survey_id=' . $result['survey_id'] . $url , true)
			);
		}


		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = $this->_url();

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('survey/survey', '&sort=name' . $url , true);

		$url = $this->_url();

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $query_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_project');
		$pagination->url = $this->url->link('survey/survey/getList', $url . '&page={page}' , true);

		$data['pagination'] = $pagination->render();

		$data['results'] = $pagination->results();

		$data['sort'] = $sort;
		$data['order'] = $order;

		
		$data['delete'] = $this->url->link('survey/survey/delete',  $url , true);

		$data['filter_name'] = $filter_name;
		
		
		$this->load->model('catalog/coupon_prefix');
		$data['prefixs'] = $this->model_catalog_coupon_prefix->getCouponPrefixs();
		
		$data['coupon_prefix'] = $this->model_catalog_coupon_prefix->getPrefixsIndexBy('coupon_prefix');
		
		$template = 'survey/survey_list';
		return $this->load->view($template, $data);
	}

	public function add() {
		$languages = $this->load->language('survey/survey');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));
			
		$url = $this->_url();
	
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
	
		$this->load->model('survey/survey');

		if ($this->user->hasPermission('add', 'survey/survey')) {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
				
				$survey_id = $this->model_survey_survey->addSurvey($this->request->post);
	
				$this->session->data['success'] = $this->language->get('text_success');
	
			
				$this->response->redirect($this->url->link('survey/survey/build', '&survey_id=' . $survey_id . $url , true));
			}
		}else{
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		$this->response->redirect($this->url->link('survey/survey',  $url , true));
	}

	public function build() {
		if (isset($this->request->get['survey_id'])) {
			$data['survey_id'] = $survey_id = $this->request->get['survey_id'];
			$languages = $this->load->language('survey/survey');
			foreach($languages as $key=>$value){
					$data[$key] = $value;	
			}
			$this->document->setTitle($this->language->get('heading_title'));
			
			$this->load->model('survey/survey');
			$data['surveys'] =$this->model_survey_survey->getSurveys();
			
			
			$error_data = array(
				'error_warning'=>'',
				'error_name'=>''
			);
	
			foreach($error_data as $key=>$default_value){
				if (isset($this->error[$key])) {
					$data[$key] = $this->error[$key];
				} else {
					$data[$key] = $default_value;
				}
			}

			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];
	
				unset($this->session->data['success']);
			} else {
				$data['success'] = '';
			}

			$url = $this->_url();
			
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			if ($this->user->hasPermission('edit', 'survey/survey')) {
				if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
					
					$this->model_survey_survey->editSurvey($survey_id, $this->request->post);
					$this->session->data['success'] = $this->language->get('text_success');
		
					$this->response->redirect($this->url->link('survey/survey/build', '&survey_id=' . $survey_id . $url , true));
				}
			}else{
				$this->error['error_warning'] = $this->language->get('text_error_permission');
			}
	
	
			$data['breadcrumbs'] = array();
	
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home', '' , true)
			);
	
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('survey/survey',  $url , true)
			);
			$this->document->setBreadcrumbs($data['breadcrumbs']);
	
			$data['change'] = str_replace('&amp;','&',$this->url->link('survey/survey/build', '' , true));
			
			$data['action'] = $this->url->link('survey/survey/build', '&survey_id=' . $survey_id . $url , true);
			
	
			$data['cancel'] = $this->url->link('survey/survey',  $url , true);
	
			if ($this->request->server['REQUEST_METHOD'] != 'POST') {
				$query_info = $this->model_survey_survey->getSurvey($survey_id);
				
				//print_r('<pre>'); print_r($query_info); print_r('</pre>'); 
			}
			foreach($this->field_data as $key=>$default_value){
				if (isset($this->request->post[$key])) {
					$data[$key] = $this->request->post[$key];
				} elseif (!empty($query_info)) {
					$data[$key] = $query_info[$key];
				} else {
					$data[$key] = $default_value;
				}
			}
			$data['inputs'] = $this->model_survey_survey->inputs();
			$data['groups'] = $this->model_survey_survey->getGroupsBySurveyID($survey_id);
			
			$this->load->model('catalog/attribute');
			$data['attr_groups'] = $this->model_catalog_attribute->getAttributesGroups();
			$data['survey_group'] = $this->load->controller('survey/group'); 
			
				
			$this->load->model('catalog/coupon_prefix');
			$data['prefixs'] = $this->model_catalog_coupon_prefix->getCouponPrefixs();
		
		
			
			$template = 'survey/form_survey';
			return $this->load->controller('startup/builder',$this->load->view($template, $data));
		}else{
			echo '<div class="alert alert-info">Không có tham số yêu cầu!</div>';
		}
		
	}
	protected function validateDelete() {
		if (!$this->user->hasPermission('delete', 'survey/survey')) {
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}
		return !$this->error;
	}
	protected function validateAdd() {
		if (!$this->user->hasPermission('add', 'survey/survey')) {
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}
		if ((utf8_strlen($this->request->post['survey_name']) < 3) || (utf8_strlen($this->request->post['survey_name']) > 128)) {
			$this->error['error_name'] = $this->language->get('text_error_name');
		}
		return !$this->error;
	}
	protected function validateForm() {
		if (!$this->user->hasPermission('add', 'survey/survey')) {
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}
		return !$this->error;
	}
}