<?php
class ControllerAccountReset extends Controller {
	private $error = array();

	public function index() {
		if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/account', '', true));
		}

		if (isset($this->request->get['code'])) {
			$code = $this->request->get['code'];
		} else {
			$code = '';
		}
		$languages= $this->load->language('account/reset');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}

		$this->load->model('account/customer');

		$customer_info = $this->model_account_customer->getCustomerByCode($code);

		if ($customer_info) {

			$this->document->setTitle($this->language->get('heading_title'));

			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
				$this->model_account_customer->editPassword($customer_info['email'], $this->request->post['password']);

				if ($this->config->get('config_history_activity')) {
					$this->load->model('account/activity');

					$activity_data = array(
						'customer_user_id' => $customer_info['customer_user_id'],
						'name'        => $customer_info['fullname'] 
					);

					$this->model_account_activity->addActivity('reset', $activity_data);
				}

				$this->session->data['success'] = $this->language->get('text_success');

				$this->response->redirect($this->url->link('account/login', '', true));
			}

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/reset', '', true)
			);

			$error_data = array(
			'error_password'=>'',
			'error_confirm'=>''
			);
			foreach($error_data as $key=>$default_value){
				if (isset($this->error[$key])) {
					$data[$key] = $this->error[$key];
				} else {
					$data[$key] = $default_value;
				}
			}

			$data['action'] = $this->url->link('account/reset', 'code=' . $code, true);

			$data['back'] = $this->url->link('account/login', '', true);

			if (isset($this->request->post['password'])) {
				$data['password'] = $this->request->post['password'];
			} else {
				$data['password'] = '';
			}

			if (isset($this->request->post['confirm'])) {
				$data['confirm'] = $this->request->post['confirm'];
			} else {
				$data['confirm'] = '';
			}
			
			$template = 'account/reset';
			$this->document->setBreadcrumbs($data['breadcrumbs']);
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
			
		} else {
			$this->load->language('account/reset');

			$this->session->data['error'] = $this->language->get('text_error_code');

			return new Action('account/login');
		}
	}

	protected function validate() {
		if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
			$this->error['error_password'] = $this->language->get('text_error_password');
		}

		if ($this->request->post['confirm'] != $this->request->post['password']) {
			$this->error['error_confirm'] = $this->language->get('text_error_confirm');
		}

		return !$this->error;
	}
}
