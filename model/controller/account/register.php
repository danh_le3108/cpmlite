<?php
class ControllerAccountRegister extends Controller {
	private $error = array();

	public function index() {
		if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/account', '', true));
		}
		$languages= $this->load->language('account/register');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$this->document->addScript('assets/plugins/datetimepicker/moment.js');
		$this->document->addScript('assets/plugins/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('assets/plugins/datetimepicker/bootstrap-datetimepicker.min.css');

		$this->load->model('account/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$customer_user_id = $this->model_account_customer->addCustomer($this->request->post);


			$this->customer->login($this->request->post['username'], $this->request->post['password']);

			unset($this->session->data['guest']);

			// Add to activity log
			if ($this->config->get('config_history_activity')) {
				$this->load->model('account/activity');

				$activity_data = array(
					'customer_user_id' => $customer_user_id,
					'name'        => $this->request->post['fullname'] 
				);

				$this->model_account_activity->addActivity('register', $activity_data);
			}

			$this->response->redirect($this->url->link('account/success'));
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_register'),
			'href' => $this->url->link('account/register', '', true)
		);
		$data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', true));


		$error_data = array(
		'error_warning'=>'',
		'error_fullname'=>'',
		'error_email'=>'',
		'error_telephone'=>'',
		'error_password'=>'',
		'error_confirm'=>''
		);
		foreach($error_data as $key=>$default_value){
			if (isset($this->error[$key])) {
				$data[$key] = $this->error[$key];
			} else {
				$data[$key] = $default_value;
			}
		}

		$data['action'] = $this->url->link('account/register', '', true);


		if (isset($this->request->post['fullname'])) {
			$data['fullname'] = $this->request->post['fullname'];
		} else {
			$data['fullname'] = '';
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} else {
			$data['email'] = '';
		}

		if (isset($this->request->post['telephone'])) {
			$data['telephone'] = $this->request->post['telephone'];
		} else {
			$data['telephone'] = '';
		}
		
		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}

		if (isset($this->request->post['confirm'])) {
			$data['confirm'] = $this->request->post['confirm'];
		} else {
			$data['confirm'] = '';
		}
		
		$template = 'account/register';
		$this->document->setBreadcrumbs($data['breadcrumbs']);
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}

	private function validate() {
		if ((utf8_strlen(trim($this->request->post['fullname'])) < 1) || (utf8_strlen(trim($this->request->post['fullname'])) > 32)) {
			$this->error['error_fullname'] = $this->language->get('text_error_fullname');
		}
		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['error_email'] = $this->language->get('text_error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['error_warning'] = $this->language->get('text_error_exists');
		}

		if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->error['error_telephone'] = $this->language->get('text_error_telephone');
		}

		if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
			$this->error['error_password'] = $this->language->get('text_error_password');
		}

		if ($this->request->post['confirm'] != $this->request->post['password']) {
			$this->error['error_confirm'] = $this->language->get('text_error_confirm');
		}

		return !$this->error;
	}
}