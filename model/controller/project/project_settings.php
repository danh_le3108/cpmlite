<?php
class ControllerProjectProjectSettings extends Controller {
	private $error = array();
	private $error_data = array(
			'error_warning',
			'error_limit_project',
			'error_limit_ajax',
			'error_name',
			'error_project_code',
			'error_url',
			'error_customer_group',
			'error_user_id',
			'error_meta_title',
			'error_db',
			'error_note',
			'error_image_project',
			'error_qc_code',
			'error_api',
			'error_filename',
			'error_group_sign',
			'error_project_folder'

	); 
	private $form_data = array(
				'config_project_folder'=>'',
				'config_error_filename'=>'error.log',
				'config_coupon_valid'=>1,
				'config_coupon_wrong_distributor'=>2,
				'config_coupon_wrong_sup'=>7,
				'config_coupon_duplicate'=>3,
				'config_coupon_has_deleted'=>4,
				'config_coupon_not_exist'=>5,
				'config_coupon_has_paid'=>6,
				'config_dist_valid'=>1,
				'config_day_valid'=>75,
				'config_dist_expires'=>1,
				
				'config_reward_audit'=>1,
				'config_reward_old'=>2,
				'config_reward_sales'=>3,
				'config_reward_recall'=>4,
				'config_customer_top'=>0,
				'config_customer_rsm'=>0,
				'config_customer_asm'=>0,
				'config_customer_sup'=>0,
				'config_db_hostname'=>DB_HOSTNAME,
				'config_api'=>'',
				'config_fcm_api_key'=>'',
				'config_test_users'=>'3519',
				'config_url'=>HTTP_SERVER.'x/',
				'config_ssl'=>'',
				'config_layout_id'=>'',
				'config_meta_title'=>'x',
				'config_email'=>'x',
				'config_telephone'=>'',
				'config_language'=>'',
				'config_user'=>'',
				'config_project_pl'=>1,
				'config_limit_project'=>50,
				'config_limit_ajax'=>10,
				'config_customer_group_id'=>'',
				'config_logo'=>'',
				'config_icon'=>'',
				'config_secure'=>'',
				'config_error_display'=>0,
				'config_nav'=>'nav_sidebar',//nav_top
				'config_color_mode'=>'skin-blue',//nav_top
				'config_group_sign'=>array(),
				'config_project_images'=>array(),
				'config_image_ignore'=>array(),
				'config_image_selfie'=>1,
				'config_image_overview'=>2,
				'config_image_store'=>array(),
				'config_image_audit'=>array(),
				'config_image_coupon'=>array(),
				'config_qc_code'=>array(),
				'config_ignore_code'=>array(),
				'config_survey_general'=>1,
				'config_survey_audit'=>array(),
				'config_survey_adhoc'=>array(),
				'config_fields_update'=>array(),
				'config_note'=>array(),
				'config_check_coupon_user_ids'=>array(),
				'config_app_versions'=>array()
				
		);
	private $project_data = array(
				'project_id'=>0,
				'project_name'=>'',
				'project_db'=>'',
				'project_type'=>'',
				'project_code'=>'',
				'project_db_user'=>DB_USERNAME,
				'project_db_pass'=>DB_PASSWORD,
				'project_source'=>'default',
				'project_user_id'=>'',
				'customer_group_id'=>'',
				'project_status'=>1
	);
	private $filter_key = array(
			'filter_status',
			'filter_type',
			'filter_global'
		);
	private function _url(){
		$url = '';
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}
	public function index() {
		$languages= $this->load->language('project/project_settings');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		$this->document->setTitle($this->language->get('heading_title'));
		foreach($this->error_data as $key){
			if (isset($this->error[$key])) {
				$data[$key] = $this->error[$key];
			} else {
				$data[$key] = '';
			}
		}
		$data['user_id'] = $this->user->getId();
		$data['config_user_trusted'] = $this->config->get('config_user_trusted');
		
		
		$this->load->model('project/project');


		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '' , true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('project/project', '' , true)
		);

		$url = $this->_url();

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->document->hasPermission('edit', 'project/project_settings')) {
			if ($this->validateForm()) {
				$this->model_project_project->editProject($this->request->post);

				$this->load->model('setting/setting');

				$this->model_setting_setting->editSetting('config', $this->request->post);
				/*Create Project Image Folder*/
				if(!is_dir(DIR_MEDIA.'files/'.$this->request->post['config_project_folder'])){
					@mkdir(DIR_MEDIA.'files/'.$this->request->post['config_project_folder'], 0777);
					@touch(DIR_MEDIA.'files/'.$this->request->post['config_project_folder']. 'index.html');
				}

				$this->session->data['success'] = $this->language->get('text_success');

				$this->response->redirect($this->url->link('project/project_settings',$url, true));
			}
		}else{
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_settings'),
			'href' => $this->url->link('project/project_settings', '' , true)
		);
		
		$this->document->setBreadcrumbs($data['breadcrumbs']);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['action'] = $this->url->link('project/project_settings', $url , true);
	

		$data['cancel'] = $this->url->link('common/home', '' , true);

			$this->load->model('project/project');
		if ($this->document->hasPermission('edit', 'project/project_settings') && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$this->load->model('setting/setting');
			$project_config = $this->model_setting_setting->getSetting('config');


			$project_info = $this->model_project_project->getProject($this->config->get('config_project_id'));
		}

		foreach($this->project_data as $key=>$value){
			if (isset($this->request->post[$key])) {
				$data[$key] = $this->request->post[$key];
			} elseif (isset($project_info[$key])) {
				$data[$key] = $project_info[$key];
			} else {
				$data[$key] = $value;
			}
		}



		foreach($this->form_data as $key=>$value){
			if (isset($this->request->post[$key])) {
				$data[$key] = $this->request->post[$key];
			} elseif (isset($project_config[$key])) {
				$data[$key] = $project_config[$key];
			} else {
				$data[$key] = $value;
			}
		}

		$data['project_id'] = $project_id = $this->config->get('config_project_id');

		$data['token'] = '';
		
		$data['config_user_pl'] = $this->config->get('config_user_pl');

		$this->load->model('account/customer_group');


		$data['customer_groups'] = $this->model_account_customer_group->getCustomerGroups();


		$this->load->model('tool/image');

		if (isset($this->request->post['config_icon']) && is_file(DIR_MEDIA . $this->request->post['config_icon'])) {
			$data['icon'] = $this->model_tool_image->resize($this->request->post['config_icon'], 100, 100);
		} elseif (isset($project_config['config_icon']) && is_file(DIR_MEDIA . $project_config['config_icon'])) {
			$data['icon'] = $this->model_tool_image->resize($project_config['config_icon'], 100, 100);
		} else {
			$data['icon'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);


		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['config_logo']) && is_file(DIR_MEDIA . $this->request->post['config_logo'])) {
			$data['logo'] = $this->model_tool_image->resize($this->request->post['config_logo'], 100, 100);
		} elseif (isset($project_config['config_logo']) && is_file(DIR_MEDIA . $project_config['config_logo'])) {
			$data['logo'] = $this->model_tool_image->resize($project_config['config_logo'], 100, 100);
		} else {
			$data['logo'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);


		$data['sources'] = array();

		$directories = glob(DIR_PROJECT. '*', GLOB_ONLYDIR);

		foreach ($directories as $directory) {
			$value = basename($directory);
			$data['sources'][] = array(
				'value' => $value,
				'label' => $value
				);
		}

		$this->load->model('design/layout');
		
		$data['layouts'] = $this->model_design_layout->getLayouts();
		
		$data['clear_tables'] = $this->clear_tables;


		$this->load->model('project/customer_level');
		
		$data['levels'] = $this->model_project_customer_level->getCustomerLevels();
		
		
		$this->load->model('project/customer');
		$data['customers'] = $this->model_project_customer->getCustomers();

		$this->load->model('localisation/image_type');
		$data['image_types'] = $this->model_localisation_image_type->getImageTypes();
		
			$this->load->model('qc/problem');
			$this->load->model('qc/code');
		$codes = $this->model_qc_code->getCodes();

		$data['problems'] = $this->model_qc_problem->getProblems();
		
		
		$data['codes'] = array();
		foreach ($codes as $result) {
			$data['codes'][$result['code_id']] = array(
				'code_id' => $result['code_id'],
				'code_name'       => $result['code_name'],
				'problem'       => $this->model_qc_problem->getProblemsByCodeId($result['code_id'])
			);
		}

		$data['config_user_cb'] = $this->config->get('config_user_cb');
		$data['config_user_staff'] = $this->config->get('config_user_staff');
		$this->load->model('user/user_group');
		$data['user_groups'] = $this->model_user_user_group->getUserGroups();
		
		
		$this->load->model('catalog/coupon_status');
		$data['coupon_statuses'] = $this->model_catalog_coupon_status->getCouponStatuses();
		
		$this->load->model('catalog/reward_type');
		$data['reward_types'] = $this->model_catalog_reward_type->getRewardTypes();
		
		$this->load->model('qc/note');
		$data['note_types'] = $this->model_qc_note->getNoteTypes();
		
		
		$this->load->model('survey/survey');
		$data['surveys'] =$this->model_survey_survey->getSurveyIndexBy('survey_id');
			
			
		$this->load->model('distributor/coupon_scan');
		$data['coupon_scans'] =$this->model_distributor_coupon_scan->getCouponScanesByIndex();
			
		/*Field update*/ 
			
		$data['color_modes'] = array(
			'skin-black'=>'Black', 
			'skin-black-light'=>'Black light', 
			'skin-blue'=>'Blue', 
			'skin-blue-light'=>'Blue light', 
			'skin-green'=>'Green', 
			'skin-green-light'=>'Green light', 
			'skin-purple'=>'Purple', 
			'skin-purple-light'=>'Purple light', 
			'skin-red'=>'Red', 
			'skin-red-light'=>'Red Light', 
			'skin-yellow'=>'Yellow', 
			'skin-yellow-light'=>'Yellow Light', 
		);
		
		$data['fields_update'] = array(
			'store_name'=>array('label'=>'Tên cửa hàng','key'=>'update_name'), 
			'store_owner'=>array('label'=>'Tên chủ CH','key'=>'update_owner'),
			'store_phone'=>array('label'=>'SĐT cửa hàng','key'=>'update_phone'),
			'store_address'=>array('label'=>'Số nhà','key'=>'update_address'), 
			'store_place'=>array('label'=>'Đường/Địa điểm','key'=>'update_place'),
			'store_ward'=>array('label'=>'Phường/xã','key'=>'update_ward'),
			'store_district'=>array('label'=>'Quận/huyện','key'=>'update_district')
		);
		
		
		/*Customer Permission*/ 
			
		$folder_project_ignore = array(
			'api',
			'account',
			'catalog',
			'common',
			'extension',
			'event',
			'error',
			'information',
			'localisation',
			// 'plan',
			'project',
			'report',
			'survey',
			// 'store',
			'tool',
			'user',
			'startup');
		$route_project_ignore = array('common/login','account/login');

		$files = array();

		// Make path into an array
		$path = array(DIR_APPLICATION . 'controller/*');

		// While the path array is still populated keep looping through
		while (count($path) != 0) {
			$next = array_shift($path);

			foreach (glob($next) as $file) {
				// If directory add to path array
				if (is_dir($file)) {
					$path[] = $file . '/*';
				}

				// Add the file to the files to be deleted array
				if (is_file($file)) {
					$files[] = $file;
				}
			}
		}
		// Sort the file array
		sort($files);
		
		$setting['areas']['project']['label'] = 'Project';
		foreach ($files as $file) {
			$controller = substr($file, strlen(DIR_APPLICATION . 'controller/'));

			$permission = substr($controller, 0, strrpos($controller, '.'));
			$part = explode('/', $permission);
			$label = ucwords(str_replace(array('/','_'),array(' &raquo; ',' '),$permission));
			if (!in_array($permission, $route_project_ignore)&&!in_array($part[0], $folder_project_ignore)) {
				$setting['areas']['project']['folders'][$part[0]]['label'] = ucwords($part[0]);
				$route = array('value'=>$permission, 'label'=>$label);
				$setting['areas']['project']['folders'][$part[0]]['permissions'][] = $route;
			}
		}
		
		$data['distributor_permission'] = $this->load->controller('project/project_settings/distributor_permission',$setting); 
		$data['customer_permission'] = $this->load->controller('project/project_settings/customer_permission',$setting); 
		
		/*End Permission*/ 
		$template = 'project/project_settings';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
	public function customer_permission($setting=array()) {
		if(!empty($setting)){
			$languages= $this->load->language('project/project_settings');
			foreach($languages as $key=>$value){
					$data[$key] = $value;
			}
		
			$data['areas'] = $setting['areas'];
			
			$data['config_customer_permission'] = $config_permission = ($this->config->has('config_customer_permission'))?$this->config->get('config_customer_permission'):array();
			
			
			if (isset($this->request->post['config_customer_permission']['access'])) {
				$data['access'] = $this->request->post['config_customer_permission']['access'];
			} elseif (isset($config_permission['access'])) {
				$data['access'] = $config_permission['access'];
			} else {
				$data['access'] = array();
			}
			if (isset($this->request->post['config_customer_permission']['add'])) {
				$data['add'] = $this->request->post['config_customer_permission']['add'];
			} elseif (isset($config_permission['add'])) {
				$data['add'] = $config_permission['add'];
			} else {
				$data['add'] = array();
			}
	
			if (isset($this->request->post['config_customer_permission']['edit'])) {
				$data['edit'] = $this->request->post['config_customer_permission']['edit'];
			} elseif (isset($config_permission['edit'])) {
				$data['edit'] = $config_permission['edit'];
			} else {
				$data['edit'] = array();
			}
			if (isset($this->request->post['config_customer_permission']['delete'])) {
				$data['delete'] = $this->request->post['config_customer_permission']['delete'];
			} elseif (isset($config_permission['delete'])) {
				$data['delete'] = $config_permission['delete'];
			} else {
				$data['delete'] = array();
			}
			$template = 'permission/customer';
			return $this->load->view($template, $data);
		}
	}

	public function distributor_permission($setting=array()) {
		if(!empty($setting)){
			$languages= $this->load->language('project/project_settings');
			foreach($languages as $key=>$value){
					$data[$key] = $value;
			}
			$data['config_distributor_permission'] = $distributor_permission = ($this->config->has('config_distributor_permission'))?$this->config->get('config_distributor_permission'):array();
		
			$data['areas'] = $setting['areas'];
			
			

			if (isset($this->request->post['config_distributor_permission']['access'])) {
				$data['access'] = $this->request->post['config_distributor_permission']['access'];
			} elseif (isset($distributor_permission['access'])) {
				$data['access'] = $distributor_permission['access'];
			} else {
				$data['access'] = array();
			}
			if (isset($this->request->post['config_distributor_permission']['add'])) {
				$data['add'] = $this->request->post['config_distributor_permission']['add'];
			} elseif (isset($distributor_permission['add'])) {
				$data['add'] = $distributor_permission['add'];
			} else {
				$data['add'] = array();
			}
	
			if (isset($this->request->post['config_distributor_permission']['edit'])) {
				$data['edit'] = $this->request->post['config_distributor_permission']['edit'];
			} elseif (isset($distributor_permission['edit'])) {
				$data['edit'] = $distributor_permission['edit'];
			} else {
				$data['edit'] = array();
			}
			if (isset($this->request->post['config_distributor_permission']['delete'])) {
				$data['delete'] = $this->request->post['config_distributor_permission']['delete'];
			} elseif (isset($distributor_permission['delete'])) {
				$data['delete'] = $distributor_permission['delete'];
			} else {
				$data['delete'] = array();
			}
			
		
			$template = 'permission/distributor';
			return $this->load->view($template, $data);
		}
	}
	protected function validateForm() {
		if (!$this->request->post['config_url']) {
			$this->error['error_url'] = $this->language->get('text_error_url');
		}
		
		if (!$this->request->post['config_error_filename']) {
			$this->error['error_filename'] = $this->language->get('text_error_filename');
		} else {
			if (preg_match('/\.\.[\/\\\]?/', $this->request->post['config_error_filename'])) {
				$this->error['error_filename'] = $this->language->get('text_error_malformed_filename');
			}
		}

		if (!isset($this->request->post['config_project_images'])) {
			$this->error['error_image_project'] = $this->language->get('text_error_image');
		}

		if (!$this->request->post['config_meta_title']) {
			$this->error['error_meta_title'] = $this->language->get('text_error_meta_title');
		}

		if (!isset($this->request->post['config_group_sign'])) {
			$this->error['error_group_sign'] = $this->language->get('text_error_group_sign');
		}
		if (!isset($this->request->post['config_api'])) {
			$this->error['error_api'] = $this->language->get('text_error_api');
		}
		if (!$this->request->post['project_db']) {
			$this->error['error_db'] = $this->language->get('text_error_db');
		}
		if ($this->error && !isset($this->error['error_warning'])) {
			$this->error['error_warning'] = $this->language->get('text_error_warning');
		}
		if (!$this->request->post['config_limit_project']) {
			$this->error['error_limit_project'] = $this->language->get('text_error_limit');
		}
		if (!$this->request->post['config_limit_ajax']) {
			$this->error['error_limit_ajax'] = $this->language->get('text_error_limit');
		}
		
		if (!$this->request->post['config_project_folder']) {
			$this->error['error_project_folder'] = 'Project folder required!';
		}
		return !$this->error;
	}

	public function checkdb() {
		 $this->load->language('project/project_settings');

		$json = array();
		if (!$this->document->hasPermission('edit', 'project/project_settings')) {
			$json['error'] = $this->language->get('text_error_permission');
		}
		if (!isset($this->request->post['project_code'])||(isset($this->request->post['project_code'])&&empty($this->request->post['project_code']))) {
			$json['error'] = $this->language->get('text_error_projectcode');
		}
		if (isset($this->request->post['project_code'])&&isset($this->request->post['project_source'])) {
			$project_id = $this->request->post['project_id'];
			$project_source = $this->request->post['project_source'];
			$project_code = $this->request->post['project_code'];
			$config_db_hostname = $this->request->post['config_db_hostname'];

			$this->load->model('project/project');
			$query_total = $this->model_project_project->getTotalProjectsByProjectCode($project_code,$project_id);
			if($query_total>0){
				$json['error'] = $this->language->get('text_error_projectcode_exists');
			}
			$project_db = DB_DATABASE."-".strtolower($project_code);

			$json['project_db'] = $project_db;

			if(isset($this->request->post['project_db_user'])&&isset($this->request->post['project_db_pass'])){
				$db_data = array(
					'project_db'=>$project_db,
					'config_db_hostname'=>$config_db_hostname,
					'project_source'=>$project_source,
					'project_db_user'=>$this->request->post['project_db_user'],
					'project_db_pass'=>$this->request->post['project_db_pass']
				);

				$db_total = $this->model_project_project->checkDB($db_data);
				if($db_total<1){
					$json['error'] = sprintf($this->language->get('text_error_db'), $project_db);
				}else{
					$json['success'] = sprintf($this->language->get('text_success_db'), $project_db);
				}

			}else{
				$json['error'] = sprintf($this->language->get('text_error_db'), $project_db);
			}

		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function clone_source() {
		 $this->load->language('project/project_settings');

		$json = array();
		if (!$this->document->hasPermission('edit', 'project/project_settings')) {
			$json['error'] = $this->language->get('text_error_permission');
		}
		if (!isset($this->request->post['copy_from'])||(isset($this->request->post['copy_from'])&&empty($this->request->post['copy_from']))) {
			$json['error'] = $this->language->get('text_error_copy_from');
		}
		if (!isset($this->request->post['copy_to'])||(isset($this->request->post['copy_to'])&&empty($this->request->post['copy_to']))) {
			$json['error'] = $this->language->get('text_error_copy_to');
		}
		if(isset($this->request->post['copy_from'])&&isset($this->request->post['copy_to'])){
			if($this->request->post['copy_from']==$this->request->post['copy_to']){
				$json['error'] = $this->language->get('text_error_same_path');
			}elseif(empty($this->request->post['copy_to'])){
				$json['error'] = $this->language->get('text_error_copy_to');
			}else{
				$copy_from = DIR_PROJECT.$this->request->post['copy_from'];
				$copy_to = DIR_PROJECT.$this->request->post['copy_to'];
				$this->recursiveCopy($copy_from,$copy_to);

				$json['sources'] = array();

				$directories = glob(DIR_PROJECT. '*', GLOB_ONLYDIR);

				foreach ($directories as $directory) {
					$value = basename($directory);
					$json['sources'][] = array(
						'value' => $value,
						'label' => $value
						);
				}

				$json['success'] = $this->language->get('text_success_copy');

			}

		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}
	private function recursiveCopy($source, $destination) {
		$directory = opendir($source);

		@mkdir($destination);
		@touch($destination. 'index.html');

		while (false !== ($file = readdir($directory))) {
			if (($file != '.') && ($file != '..')) {
				if (is_dir($source . '/' . $file)) {
					$this->recursiveCopy($source . '/' . $file, $destination . '/' . $file);
				} else {
					copy($source . '/' . $file, $destination . '/' . $file);
				}
			}
		}

		closedir($directory);
	}
	private $clear_tables = array(
				'catalog_distributor' => '1.Danh sách Nhà phân phối',
				'coupon' => '2.Danh sách coupon',
				'customer_user' => '3.Danh sách User khách hàng',
				'store' =>'4.Danh sách cửa hàng',
				'plan' => '5.Danh sách Plan',
				'plan_survey' => '6.Mức đăng ký của Plan',
				'plan_coupon_history' => '7.Danh sách Plan Coupon',
				'plan_survey_data' => '8.Data khảo sát của Plan',
				'plan_code' => '9.Danh sách Plan bị check QC Code',
				'plan_confirm' => '10.Danh sách QC ký tên trên Plan',
				'plan_images' => '11.Hình ảnh Plan',
				'plan_note' => '12.Plan Note',
				'plan_history' => '13.Lịch sử thay đổi Plan',
				'excel_import_history' => '14.Lịch sử import Excel',
				'excel_export_history' => '15.Lịch sử export Excel',
				'app_log' => '16.App error logs',
		); 
	public function clear_data() {
		 $this->load->language('project/project_settings');

		if(($this->request->server['REQUEST_METHOD'] == 'POST')&&$this->request->post['hidden_key']=='banatuan') {
			$clear_tables = $this->clear_tables;
			foreach($clear_tables as $table => $label){
				$this->pdb->query("TRUNCATE " . PDB_PREFIX . "$table");
			}
			$json['success'] = $this->language->get('Xóa dữ liệu thành công!');
		
			$json['redirect'] = str_replace('&amp;', '&', $this->url->link('api/log_error/errors', '', true));
		}else{
			$json['error'] =  $this->language->get('Bạn không có quyền xóa!');
			
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
