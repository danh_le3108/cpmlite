<?php
class ControllerProjectProjectUser extends Controller {
	private $error = array();
	private function _url(){
		$url = '';
		return $url;
	}
	private function project_users() {
		
		$this->load->model('user/user_group');
		
		
		$this->load->model('tool/image');
		$thumb = $this->model_tool_image->resize('avatar.jpg', 100, 100);
		$this->load->model('project/project_user');
		$results = $this->model_project_project_user->getProjectUsers();
		$users = array();
		foreach($results as $result){
			$imag_path = 'files/users/' . $result['username'].'/';
			$img_href = HTTP_SERVER.'media/files/users/' . $result['username'].'/';
			if (!empty($result) && $result['image'] && is_file(DIR_MEDIA .$imag_path. $result['image'])) {
				$avatar[$result['user_id']] = $this->model_tool_image->resize($imag_path.$result['image'], 100, 100);
			}else{
				$avatar[$result['user_id']] = $thumb;
			}
			$users[] = array(
				'project_user_id' => $result['project_user_id'],
				'region_code' => $result['region_code'],
				'thumb' => $avatar[$result['user_id']],
				'project_id' => $result['project_id'],
				'user_id' => $result['user_id'],
				'usercode' => $result['usercode'],
				'username' => $result['username'],
				'plan_total' => $result['plan_total'],
				'email' => $result['email'],
				'telephone' => $result['telephone'],
				'group_id' => $result['user_group_id'],
				'user_group_id' => $result['project_user_group'],
				'user_managers' => !empty($result['user_managers'])?explode(',',$result['user_managers']):array(),
				'user_parent_id' => $result['user_parent_id'],
				'child_group_id' => $result['child_group_id'],
				'parent_fullname' => $result['parent_fullname'],
				'user_status' => $result['user_status'],
				'user_update' =>  $result['user_update'],
				'date_added' =>  $result['date_added'],
				'fullname' => $result['fullname'],
				'device_model' => $result['device_model'],
				'new_model' => $result['new_model'],
				'status' => $this->language->get(($result['user_status']==1)?'text_active':'text_deactive'),
				'group_name' => $result['group_name']
			);
		}
		return $users;
	}
	public function index() {
		
		$this->load->model('localisation/region');
		$data['regions'] = $this->model_localisation_region->getRegions();
		
		
		$data['user_sup'] = $this->config->get('config_user_sup');
		$data['user_staff'] = $this->config->get('config_user_staff');
		$data['user_dc'] = $this->config->get('config_user_dc');


		$languages = $this->load->language('project/project_user');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		$this->document->setTitle($this->language->get('heading_title'));


		$this->load->model('project/project_user');
		

		$data['text_form'] = $this->language->get('heading_title');

		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		$errors = array('province_id', 'fullname', 'email', 'identity_number', 'telephone');
		foreach($errors as $error){
			if (isset($this->error['error_'.$error])) {
				$data['error_'.$error] = $this->error['error_'.$error];
			} else {
				$data['error_'.$error] = '';
			}
		}

		$url = $this->_url();

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('project/project_user',  $url, true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);


		$data['action'] = $this->url->link('project/project_user/edit', $url, true);

		$this->load->model('tool/image');
		$data['thumb'] = $thumb = $this->model_tool_image->resize('avatar.jpg', 100, 100);

		$data['project_users'] = $this->project_users();

		$this->load->model('user/user_group');
		$data['user_groups'] = $this->model_user_user_group->getUserGroups();
		
		$data['logged_group'] = $this->user->getGroupId();
		$data['logged_id'] = $this->user->getId();

		/* Quick add user */
		$language_data = $this->load->language('user/user');
		foreach($language_data as $key=>$value){
			$data[$key] = $value;
		}
		$this->load->model('localisation/province');
		$data['provinces'] = $this->model_localisation_province->getProvinces();
		
		$this->load->model('user/user_group');
		$data['user_groups'] = $this->model_user_user_group->getUserGroups();
		/* End quick add user */

		$template = 'project/project_user';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));

	}
	
	public function load_group() {
		
		if (isset($this->request->get['user_group_id'])) {
			$data['filter_group_id'] = $this->request->get['user_group_id'];
			$data['user_sup'] = $this->config->get('config_user_sup');
			$data['user_staff'] = $this->config->get('config_user_staff');
			$data['user_dc'] = $this->config->get('config_user_dc');
	
	
			$languages = $this->load->language('project/project_user');
			foreach($languages as $key=>$value){
					$data[$key] = $value;
			}
	
			$this->load->model('tool/image');
			$data['thumb'] = $thumb = $this->model_tool_image->resize('avatar.jpg', 100, 100);
	
			$data['project_users'] = $this->project_users();
	
			$this->load->model('user/user_group');
			$data['user_groups'] = $this->model_user_user_group->getUserGroups();
	
			$data['logged_group'] = $this->user->getGroupId();
			$data['logged_id'] = $this->user->getId();
	
			$this->load->model('localisation/region');
			$data['regions'] = $this->model_localisation_region->getRegions();
	
			$template = 'project/project_user_ajax';
			$this->response->setOutput($this->load->view($template, $data));
		}else{
			echo 'Không có tham số yêu cầu!';
		}
	}
	
	public function listuser() {
		$languages = $this->load->language('project/project_user');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}

		$project_id = $this->config->get('config_project_id');

		$data['project_users'] = $this->project_users();

		$template = 'project/project_user_list';
		$this->response->setOutput($this->load->view($template, $data));

	}
	
	public function update_region() {
		$user_sup = $this->config->get('config_user_sup');
		$json = array();
		if (isset($this->request->post['user_id'])&&isset($this->request->post['user_group_id'])&&isset($this->request->post['region_code'])&&$this->document->hasPermission('edit', 'project/project_user')) {
			if($this->request->post['user_group_id']==$user_sup){
				$this->load->model('project/project_user');
				$this->model_project_project_user->updateRegion($this->request->post['user_id'],$this->request->post['region_code']);
			}
			$json['success'] = 'Success!';
		}else {
			$json['error'] = 'Yêu cầu không hợp lệ!';
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function approveDevice() {
		$json = array();
		if ($this->request->server['REQUEST_METHOD'] == 'POST' &&$this->document->hasPermission('edit', 'project/project_user')&&isset($this->request->post['user_id']) &&isset($this->request->post['status'])) {
				$this->load->model('project/project_user');
			if ($this->request->post['status']==1) {
				$this->model_project_project_user->approveDevice($this->request->post['user_id']);
			}else{
				$this->model_project_project_user->rejectDevice($this->request->post['user_id']);
			}
			$json['success'] = 'Success!';
		}else {
			$json['error'] = 'Nhân viên không tồn tại';
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function update_manager() {
			$user_dc = $this->config->get('config_user_dc');
		$json = array();
		if (isset($this->request->post['user_parent_id'])&&$this->document->hasPermission('edit', 'project/project_user')) {
			$this->load->model('project/project_user');
			
			$this->model_project_project_user->updateManager($this->request->post['user_parent_id'],$this->request->post);
			
			if((int)$this->request->post['handle']==1){
				$project_users = $this->project_users();
				foreach($project_users as $user){
					if($user['user_group_id']==$user_dc&&$user['user_id']!=$this->request->post['user_parent_id']){
						$this->request->post['handle'] = 0;
						$this->model_project_project_user->updateManager($user['user_id'],$this->request->post);
						
					}
				}
			}
				
			$json['success'] = 'Success!';
		}else {
			$json['error'] = 'Nhân viên không tồn tại';
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function remove_user() {
		$json = array();
		if (isset($this->request->get['user_id'])&&$this->document->hasPermission('edit', 'project/project_user')) {
			$this->load->model('project/project_user');
			$this->model_project_project_user->removeUser($this->request->get['user_id']);
			$json['success'] = 'Success!';
		}else {
			$json['error'] = 'Nhân viên không tồn tại';
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function add_user() {
		$json = array();
		if (isset($this->request->post['user_id']) && $this->request->server['REQUEST_METHOD'] == 'POST' &&$this->document->hasPermission('edit', 'project/project_user')) {
			$this->load->model('project/project_user');
			$this->model_project_project_user->updateUser($this->request->post['user_id'], $this->request->post);
			$json['success'] = 'Success!';
		}else {
			$json['error'] = 'Nhân viên không tồn tại';
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function addUser2Project(){
		$json = array();
		
		$test_ids = !empty($this->config->get('config_test_users'))?explode(',',$this->config->get('config_test_users')):array(3519);

		if(isset($this->request->post['user_id']) && $this->request->server['REQUEST_METHOD'] == 'POST'&&$this->document->hasPermission('edit', 'project/project_user')){
			$data['user_id'] = $user_id = $this->request->post['user_id'];
			$this->load->model('project/project_user');
			$this->load->model('user/user');
			$user_info = $this->model_user_user->getUser($this->request->post['user_id']);
			$data['usercode'] = $user_info['usercode'];
			$data['fullname'] = $user_info['fullname'];
			$data['project_id'] = $config_project_id = $this->config->get('config_project_id');
			$data['user_group_id'] = $user_group_id = $user_info['user_group_id'];
			
						
			if($user_info){
				if($user_info['status'] != $this->config->get('config_status_block')){
					$projects_user_current = $this->model_project_project_user->getProjectsOfUser($user_id, 1);
						
					$project_type_current = $this->model_project_project_user->getProjectType($config_project_id);


					foreach($projects_user_current as $project_id => $project){
						if($project_id == $config_project_id){
							$json['error'] = 'Nhân viên đã tham gia dự án này';
							break;
						}
						if(!in_array($user_id,$test_ids)&&$user_info['user_group_id']==$this->config->get('config_user_staff')&& $project['user_status']==1){
							if($project_type_current == 0 && $project['project_type'] == 0 ){
								$json['error'] = 'Không thể tham gia 2 dự án dài hạn';
							} elseif($project_type_current == 0 && $project['project_type'] == 1){
								$json['error'] = 'Nhân viên đã tham gia dự án ngắn hạn không thể tham gia thêm dự án dài hạn';
							} elseif($project_type_current == 1 && $project['project_type'] == 0){
								$json['error'] = 'Nhân viên đã tham gia dự án dài hạn không thể tham gia thêm dự án ngắn hạn';
							}
						}
					}
				} else {
						$json['error'] = 'Nhân viên đã bị block vĩnh viễn, không được tham gia dự án. Liên hệ QC để biết chi tiết';
				}
			} else {
				$json['error'] = 'Nhân viên không tồn tại';
			}

			if(!$json){
				$this->model_project_project_user->addUserToProject($data);
				$json['success'] = 'Thêm nhân viên thành công';
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function autocomplete() {;
			$this->load->model('project/project_user');
		$json = array();
		$filter_data = array(
			'sort'        => 'fullname',
			'order'       => 'ASC',
			'start'       => 0,
			'limit'       => 20
		);
		if (isset($this->request->get['filter_global'])) {
			$filter_data['filter_global'] = $this->request->get['filter_global'];
		}
		if (isset($this->request->get['filter_username'])) {
			$filter_data['filter_username'] = $this->request->get['filter_username'];
		}
		if (isset($this->request->get['filter_usercode'])) {
			$filter_data['filter_usercode'] = $this->request->get['filter_usercode'];
		}
		if (isset($this->request->get['filter_fullname'])) {
			$filter_data['filter_fullname'] = $this->request->get['filter_fullname'];
		}
		if (isset($this->request->get['filter_user_group_id'])) {
			$filter_data['filter_user_group_id'] = $this->request->get['filter_user_group_id'];
		}
			$this->load->model('user/user');

			$results = $this->model_user_user->getUsers($filter_data);
			
		$this->load->model('tool/image');
		$thumb = $this->model_tool_image->resize('avatar.jpg', 100, 100);
		

			foreach ($results as $result) {
					$imag_path = 'files/users/' . $result['username'].'/';
					$img_href = HTTP_SERVER.'media/files/users/' . $result['username'].'/';
					if (!empty($result) && $result['image'] && is_file(DIR_MEDIA .$imag_path. $result['image'])) {
						$avatar[$result['user_id']] = $this->model_tool_image->resize($imag_path.$result['image'], 100, 100);
					}else{
						$avatar[$result['user_id']] = $thumb;
					}
					$json[] = array(
						'user_id' => $result['user_id'],
						'user_group_id' => $result['user_group_id'],
						'group_name' => $result['group_name'],
						'username' => $result['username'],
						'usercode' => $result['usercode'],
						'email' => $result['email'],
						'telephone' => $result['telephone'],
						'thumb' => $avatar[$result['user_id']],
						'fullname'        => html_entity_decode($result['fullname'], ENT_QUOTES, 'UTF-8')
					);
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['fullname'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
