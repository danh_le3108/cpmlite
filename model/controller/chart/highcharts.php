<?php
class ControllerChartHighcharts extends Controller{
	
	private function _url(){
		$url = '';
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}

	private $filter_key = array(
			'filter_level_id',
			'filter_round'
		);
	private function staff_users() {
		$config_customer_sup = $this->config->get('config_customer_sup');
		$this->load->model('chart/highcharts');
		$this->load->model('tool/image');
		$this->load->model('project/customer');
		$results = $this->model_project_customer->getProjectCustomers();
		
		$thumb = $this->model_tool_image->resize('avatar.jpg', 100, 100);
		$users = array();
		 
		foreach($results as $result){
			if($config_customer_sup==$result['customer_level_id']){
				$filter = 'filter_customer_user_id=';
			}else{
				$filter = 'filter_customer_parent_id=';
			}
			$customers_manager = $this->customer->getSupsByParentID($result['customer_user_id']);
				$rating = $this->model_chart_highcharts->countTotalPlansByIDS($customers_manager);
				$success =  isset($rating['success'])?$rating['success']:0;
				$unsuccess =  isset($rating['unsuccess'])?$rating['unsuccess']:0;
				$fail =  isset($rating['fail'])?$rating['fail']:0;
			
			$users[] = array(
				'customer_user_id' => $result['customer_user_id'],
				'username' => $result['username'],
				'fullname' => $result['fullname'],
				'customer_level_id' => $result['customer_level_id'],
				'customer_parent_id' => $result['customer_parent_id'],
				'customers_manager' => $customers_manager,
				'level_name' => $result['level_name'],
				'chart' =>array("success:$success","unsuccess:$unsuccess","fail:$fail"),
				'success'=>$rating['success'],
				'unsuccess'=>$rating['unsuccess'],
				'fail'=>$rating['fail'],
				'total'=>array_sum($rating),
				'url_success'   => $this->url->link('client/plan','filter_rating_status=1&'.$filter.$result['customer_user_id'],true),
				'url_unsuccess' => $this->url->link('client/plan','filter_rating_status=-2&'.$filter.$result['customer_user_id'],true),
				'url_fail'      => $this->url->link('client/plan','filter_rating_status=-1&'.$filter.$result['customer_user_id'],true),
				'url_total'     => $this->url->link('client/plan',$filter.$result['customer_user_id'],true),
				'down_success'   => $this->url->link('client/export_plan/export','filter_rating_status=1&'.$filter.$result['customer_user_id'],true),
				'down_unsuccess' => $this->url->link('client/export_plan/export','filter_rating_status=-2&'.$filter.$result['customer_user_id'],true),
				'down_fail'      => $this->url->link('client/export_plan/export','filter_rating_status=-1&'.$filter.$result['customer_user_id'],true),
				'down_total'     => $this->url->link('client/export_plan/export',$filter.$result['customer_user_id'],true)
			);
			unset($user_ids);
			unset($rating);
		}
		return $users;
	}
	public function index() {
		$data['config_customer_top'] = $this->config->get('config_customer_top');
		$data['config_customer_rsm'] = $this->config->get('config_customer_rsm');
		$data['config_customer_asm'] = $this->config->get('config_customer_asm');
		$data['config_customer_sup'] = $this->config->get('config_customer_sup');

		$this->load->model('plan/round');
		$data['rounds'] =  $this->model_plan_round->getRounds();

		$languages = $this->load->language('chart/highcharts');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		$this->document->setTitle($this->language->get('heading_title'));

		$data['ykeys'] = $ykeys = array('"success"','"unsuccess"','"fail"');
		$data['label'] = $label = array('"Đạt"','"Không đạt"','"KTC"');
		$data['colors'] = $colors = array('"#00a65a"','"#dd4b39"','"#666"');


		$data['heading_title'] = $this->language->get('heading_title');

		$url = $this->_url();

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('chart/highcharts',  $url, true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);


		
		$this->load->model('tool/image');
		$data['thumb'] = $thumb = $this->model_tool_image->resize('avatar.jpg', 100, 100);

		$data['project_users'] = $this->staff_users();

		$this->load->model('project/customer_level');
		$data['user_groups'] = $this->model_project_customer_level->getCustomerLevels();
		
		if($this->customer->isLogged()&&!$this->user->isLogged()){
			$data['logged_group'] = $this->customer->getGroupId();
			$data['isLogged'] = $this->customer->isLogged();
			$data['customers_manager'] = $this->customer->getSupsByParentID($this->customer->isLogged());
			
		}else{
			$data['customers_manager'] = 'all'; 
			$data['logged_group'] = 1;
			$data['isLogged'] = 0;
		}
			
		if($this->user->isLogged()||$this->customer->getLevel()==$this->config->get('config_customer_top')){
			$data['isNationwide'] = 1;
		}else{
			$data['isNationwide'] = 0;
		}
		
		
		$this->document->addScript('asset/highcharts/code/highcharts.js');
		$this->document->addScript('asset/highcharts/code/modules/exporting.js');

		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$data[$key] = trim($this->request->get[$key]);
			} else{
				$data[$key] = null;
			}
		}
		
		$data['header'] = $this->load->controller('common/header');	
		$data['footer'] = $this->load->controller('common/footer');
		$template = 'chart/highcharts';
		
		$this->response->setOutput($this->load->view($template, $data));
	}

}