<?php
class ControllerApiChangePass extends Controller {
	public function index() {
		$json = array();
		$json['api_name'] = 'change_pass';
		
		
		$this->load->language('api/api');

		if(!isset($this->request->post['api_key'])){
			$json['message'] = $this->language->get('text_error_key');
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}
		// Login with API Key
		if (isset($this->request->post['api_key'])&&trim($this->request->post['api_key']) ==$this->config->get('config_api')&&isset($this->request->post['username'])&&isset($this->request->post['new_pass'])&&isset($this->request->post['old_pass'])) {
			$username = $this->request->post['username'];
			$new_pass = $this->request->post['new_pass'];
			$old_pass = $this->request->post['old_pass'];

			$this->load->model('user/user');
			$user_info = $this->model_user_user->getUserByUsername($username);

			if(!empty($user_info) && !empty($new_pass) && !empty($old_pass)){
				$old_pass_encode = sha1($user_info['salt'] . sha1($user_info['salt'] . sha1($old_pass)));
				if($old_pass_encode == $user_info['password']){
					$json['status'] = $this->model_user_user->changePassword($new_pass);
					$json['message'] = $this->language->get('text_success_changepass');
				} else {
					$json['status'] = 0;
					$json['message'] = 'Mật khẩu cũ không đúng';
				}
			}else{
				$json['status'] = 0;
				$json['message'] = $this->language->get('text_error_changepass');
			}
		}else {
			$json['message'] = $this->language->get('text_error_key');
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
