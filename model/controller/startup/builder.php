<?php
class ControllerStartupBuilder extends Controller {
	private $main_output = '';
	
	private $num_widget = 0;
	
	public function index($output) {
			$this->main_output = $output;
		
		//print_r('<pre>'); print_r($this->getLayoutID()); print_r('</pre>'); 
			$this->num_widget = 0;
		$layout_id = $this->getLayoutID();
			$setting = json_decode($this->layout_builder);

			$data['layout'] = array(
					'layout_id'=>$layout_id,
					'layouts'=>$this->buildLayoutData($setting ,1),
					'num_widget'=>$this->num_widget?$this->num_widget:0,
			);
		
			
		$data['breadcrumbs'] = $this->document->getBreadcrumbs();
		$data['header'] = $this->load->controller('common/header');	
		$data['footer'] = $this->load->controller('common/footer');
		
		$this_template = 'common/page_builder';
		$this->response->setOutput($this->load->view($this_template, $data));
	}
	
	
    public function getLayoutID() {
            $route = 'common/home';
			
        if (isset($this->request->request['route'])) {
            $route = (string)$this->request->request['route'];
        }
        $layout_id = $this->getLayoutIDByRoute($route);
        return $layout_id;
    }
    /*LayoutID*/
    private static $layout_builder = '[{"index":0,"elem_id":"","elem_margin":"","elem_padding":"","elem_container":"","elem_class":"","elem_bg_image":"","elem_bg_size":"cover","elem_bg_color":"rgba(0,0,0,0)","elem_bg_attachment":"fixed","elem_bg_repeat":"repeat","elem_bg_position":"center top","elem_bg_paralax":"2","cols":[{"index":0,"elem_class":"","inrow":0,"col_lg":12,"col_md":12,"col_sm":12,"col_xs":12,"widgets":[{"module":"main_output"}],"rows":[]}]}]';
    private static $all_layout = array();
	
    public function getLayoutIDByRoute($route) {
        $route_str = str_replace('/', '_', $route);
        $layout_id = 0;
        if (!empty(self::$all_layout)) {
            if (isset(self::$all_layout[$route_str])) {
                $layout_id = self::$all_layout[$route_str];
            }
        } else {
			$sql = "SELECT * FROM " . DB_PREFIX . "layout_route WHERE '" . $this->db->escape($route) . "' LIKE CONCAT(route,'%') AND project_id = '" . (int) $this->config->get('config_project_id') . "' ORDER BY route DESC LIMIT 0,1";
            $query = $this->db->query($sql);
            if ($query->num_rows) {
                $this->layout_builder = $query->row['layout_builder'];
                $layout_id = $query->row['layout_id'];
            }
            self::$all_layout[$route_str] = $layout_id;
        }
        return $layout_id;
    }
	
	/**
	 * looping render layout structures and module content inside cols and rows.
	 *
	 * @return Array $layout
	 */
    private function buildLayoutData($rows,$rl=1){
        $layout = array();
			foreach( $rows as $rkey =>  $row ){
				$row->level=$rl;
	
				$row = $this->mergeRowData( $row );
	
				foreach( $row->cols as $ckey => $col ){
					$col = $this->mergeColData( $col );
					foreach( $col->widgets as  $wkey => $w ){
					   if( isset($w->module) ){
							$w->content = ($w->module=='main_output')?$this->main_output:$this->renderModule(array('code'=>$w->module));
						   $this->num_widget++;
					   }
					}
					if( isset($col->rows) ){
						$col->rows = $this->buildLayoutData($col->rows, $rl+1);
					}
					$row->cols[$ckey] = $col;
				}
	
				$layout[$rkey] = $row;
			}
        return $layout;
    }

    /**
	 * direct rendering content of module by code
	 *
	 * @return HTML Stream
	 */
	private function renderModule($module){
		$part = explode('.', $module['code']);

		if (isset($part[0]) && $this->config->get($part[0] . '_status')) {
			return $this->load->controller('extension/module/' . $part[0]);
		}

		if (isset($part[1])) {
			$this->load->model('extension/module');
			$setting_info = $this->model_extension_module->getModule($part[1]);
			if ($setting_info && $setting_info['status']) {
				return $this->load->controller('extension/module/' . $part[0], $setting_info);
			}
		}
		return ;
	}

	/**
	 * make attributes information for column
	 *
	 * @param Array $col
	 * @return Array $col
	 */
	private function mergeColData( $col ){
		$col->attrs = '';
        if( isset($col->elem_class) && $col->elem_class ){
			$col->elem_class = trim($col->elem_class);
		}else {
			$col->elem_class = '';
		}
        return $col;
	}

	/**
	 * make attributes information for rows such as background,padding, margin
	 *
	 * @param Array $row
	 * @return Array $row
	 */
	private function mergeRowData( $row ){
		$row->attrs = '';
		$styles = array();

		if( isset($row->elem_class) && $row->elem_class ){
			$row->elem_class =  trim( $row->elem_class );
		}else {
			$row->elem_class = '';
		}

		return $row;
	}

}