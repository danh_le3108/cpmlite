<?php
class ControllerStartupPermission extends Controller {
	public function index() {
		if (isset($this->request->get['route'])) {
			$route = '';
			
			$part = explode('/', $this->request->get['route']);

			if (isset($part[0])) {
				$route .= $part[0];
			}

			if (isset($part[1])) {
				$route .= '/' . $part[1];
			}

			// If a 3rd part is found we need to check if its under one of the extension folders.
			$extension = array(
				'extension/module'
			);

			if (isset($part[2]) && in_array($route, $extension)) {
				$route .= '/' . $part[2];
			}
			
			// We want to ingore some pages from having its permission checked. 
			$ignore = array(
				'user/login',
				'user/logout',
				'account/account',
				'account/password',
				'account/edit',
				'account/login',
				'account/logout',
				'account/forgotten',
				'account/reset',
				'common/home',
				'common/language',
				'common/maintenance',
				'error/not_found',
				'error/permission',
				'localisation/place'
			);

			if ($part[0]!='api'&&!in_array($route, $ignore) && !$this->document->hasPermission('access', $route)) {
				return new Action('account/login');
				//$this->response->redirect($this->url->link('common/home', '', true));
			}
		}
	}
}
