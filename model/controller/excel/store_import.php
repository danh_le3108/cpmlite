<?php

class ControllerExcelStoreImport extends Controller {
	private $import_id = 1;
	private $export_id = 1;
	private $no_filter_key = array('_url','route');
	private $excel_header = array(
		'A'=>'Location',
		'I'=>'Mã CPM',
		'O'=>'Tỉnh/ Thành Phố',
		'Q'=>'Loại hình cửa hàng',
		// 'R'=>'Kênh',
		'J'=>'Tên cửa hiệu',
	);
	private function _url(){
		$url = '';
		foreach ($this->request->get as $key => $value) {
			if (!in_array($key, $this->no_filter_key)) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($value, ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}	

	public function index($filter_data){
		if ($this->document->hasPermission('access','excel/store_import')) {
			$this->document->addStyle('assets/plugins/menu-material/collapzion.min.css');
			$languages= $this->load->language('excel/template');
			foreach($languages as $key=>$value){
					$data[$key] = $value;
			}
			if(!empty($this->session->data['history_id'])) {
				$data['history_id'] = $this->session->data['history_id'];
				unset($this->session->data['history_id']);
			}
			$data['template_id'] = $this->import_id;
			$data['has_import'] = $this->document->hasPermission('add','excel/store_import');
			$data['has_export'] = $this->document->hasPermission('edit','excel/store_import');
			$data['export'] = $this->url->link('excel/store_import/export',$this->_url());
			$data['template_url'] = $this->url->link('excel/template/template',  '&template_id='.$this->import_id, true);
			$data['form'] = $this->url->link('excel/store_import/import');
			$template = 'excel/store_import';
			return $this->load->view($template, $data);
		}
	}

	public function import(){
		if ($this->document->hasPermission('add','excel/store_import') && !empty($this->request->files['file_import'])) {
			if (empty($this->request->files['file_import']['name'])) {
				$this->session->data['error_warning'] = 'Warning: Chưa chọn file!';
				$this->response->redirect($this->url->link('store/store'));
			} 
			if (substr($this->request->files['file_import']['name'], -5) != '.xlsx') {
				$this->session->data['error_warning'] = 'Warning: File không đúng định dạng!';
				$this->response->redirect($this->url->link('store/store'));
			}
			$file_name = 'Store-Import-by-' . $this->user->getUserName(). '-at-'. date('Ymd-His') .'.xlsx';
			$dir = DIR_MEDIA . 'files/'. $this->config->get('config_project_folder') .'/import/store/';
		    if(!is_dir($dir)){
		      	@mkdir($dir, 0777, true);
				@touch($dir. 'index.html');
		    }
			$file = $dir . $file_name;
		    move_uploaded_file($this->request->files['file_import']['tmp_name'], $file);
		    if(file_exists($file)){
		    	if (!$this->excel->readFileCellKey($file, 3, $this->excel_header)) {
		    		$this->session->data['error_warning'] = 'Warning: Sai template';
					$this->response->redirect($this->url->link('store/store'));
		    	} else {
		    		$template_info = $this->excel->getTemplate($this->import_id);
			      	$filesize = $this->request->files['file_import']['size'];
			      	$import_info = $this->excel->getColRow($file);
			      	$import_id = $this->excel->addImportHistory($template_info['template_id'], $file, $import_info);
					$data['callback'] = str_replace('&amp;','&',$this->url->link('excel/store_import/import_data',   '&import_id='.$import_id."&file=". $file, true));
		    	}
			}
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
			
			$template = 'excel/redirect';
			$this->response->setOutput($this->load->view($template, $data));
		}
  	}

  	public function import_data() {
  		if ($this->document->hasPermission('add','excel/store_import') && !empty($this->request->get['import_id']) && !empty($this->request->get['file'])) {

	  		$this->load->model('store/store_type');
			$this->load->model('localisation/province');
			$this->load->model('localisation/region');
			$this->load->model('store/store');
			$this->load->model('catalog/attribute');
			
			$region_list = $this->model_localisation_region->getRegions();
			$regions = array();
			foreach ($region_list as $r) {
				$regions[alias($r['region_code'])] = $r;
			}

			$province_list = $this->model_localisation_province->getProvinces();
			$provinces = array();
			foreach ($province_list as $p) {
				$provinces[alias($p['name'])] = $p;
			}

			$store_type_list = $this->model_store_store_type->getStoreTypes();
			$store_types = array();
			foreach ($store_type_list as $value) {
				$store_types[alias($value['type_name'])] = $value['store_type_id'];
			}
			
			$chanel_list = $this->model_store_store->getChanels();
			$chanel = array();
			foreach ($chanel_list as $value) {
				$chanel[alias($value['name'])] = $value['id'];
			}

			$this->load->model('history/history');
			$history = array(
				'table_name' => 'store',
				'user_id' => $this->user->getId(),
				'user_update' => $this->user->getUserName(),
				'controller' => 'excel/store_import'
			);
	  		$template_info = $this->excel->getTemplate($this->import_id);
		    $settings = $template_info['settings'];
	  		$result = $this->excel->read_all($this->request->get['file'], $settings['start_row']);
			$data_row = array();
		    $error_row = $success_row = 0;
		    $error = array();
		    $require = array('I','O');
		    $store_address = array('K','L','M','N');
		    $check_exist = array(
		    	'A' => $regions,
		    	'O' => $provinces,
		    	'Q' => $store_types,
		    	'R' => $chanel
		    );
		    
	  		foreach ($result['data'] as $rid => $row) {
	  			$error_message = array();
	  			$store_address_raw = array();
	  			if (!empty($row)) {
 	  				foreach ($row as $col => $value) {
	  					$value = trim($value);
	  					if (!empty($value)) {
	  						if (array_key_exists($col,$check_exist) && !isset($check_exist[$col][alias($value)])) {
	  							$error_message[] = $this->excel_header[$col] .' không tồn tại.';
	  						} elseif (in_array($col, $store_address)) {
	  							$store_address_raw[] = $value;
	  						}
	  					} elseif (in_array($col, $require) && empty($value)) {
	  						$error_message[] = $this->excel_header[$col] .' không được trống';
	  					}
			    	}
			    	
			    	if (empty($error_message)) {
			  			$province = $provinces[alias($row['O'])];
			  			$store_address_raw[] = $province['name'];
		  				$data = array(
		  					'region_code' => isset($regions[alias($row['A'])]) ? $regions[alias($row['A'])]['region_code'] : '',
		  					'ter' => trim($row['B']),
		  					'asm' => trim($row['C']),
			  				'ss' => trim($row['D']),
			  				'dcr' => trim($row['E']),
			  				'distributor_code' => trim($row['F']),
			  				'distributor' => trim($row['G']),
		  					'customer_code' => trim($row['H']),
			  				'store_code' => trim($row['I']),
			  				'store_name' => trim($row['J']),
			  				'store_address' => trim($row['K']),
			  				'store_place' => trim($row['L']),
			  				'store_ward' => trim($row['M']),
			  				'store_district' => trim($row['N']),
			  				'store_province' => $province['name'],
							'province_id' => $province['province_id'],
			  				'store_phone' => trim($row['P']),
							'store_type_id' => isset($store_types[alias(trim($row['Q']))]) ? $store_types[alias(trim($row['Q']))] : '',
							'chanel' => isset($chanel[alias(trim($row['R']))]) ? $chanel[alias(trim($row['R']))] : '',
							'store_address_raw' => implode(', ', $store_address_raw),

							'route' => trim($row['S']),
			  				'qc_code' => trim($row['T']),
		  					'dung_tich' => trim($row['U']),
			  				'club' => trim($row['V']),
			  				'fridge' => trim($row['W']),
			  				'cvs_shelf' => trim($row['X']),
			  				'csd_shelf' => trim($row['Y']),
			  				'aqf_shelf' => trim($row['Z']),
			  				'tea_shelf' => trim($row['AA']),
			  				'import_id' => $this->request->get['import_id'],
			  				'store_latitude' => trim($row['AB']),
			  				'store_longitude' => trim($row['AC'])
			  			);
			  			$store = $this->model_store_store->getStoreByStoreCode(trim($row['I']));
			  			if (!empty($store)) {
			  				$store_id = $store['store_id'];

			  				$history['table_primary_id'] = $store_id;
							$history['data'] = json_encode($data);
							$history['old_data'] = json_encode($store);
							$this->model_history_history->add($history);

							$this->model_store_store->updateStore($store_id,$data);
						} else {
							$data['user_added'] = $this->user->getUserName();
							$this->model_store_store->addStore($data);
						}
						$success_row++;
		  			} else {
		  				$error[$rid] = $row;
						$error[$rid]['message'] = implode(' | ', $error_message);
						$error_row++;
		  			}
	  			}
	  		}
	  		$history_info = $this->excel->getImportHistory($this->request->get['import_id']);
	  		if ($error_row < 1000) {
	  			$history_info['import_info']['error'] = $error;
	  		}
		    $history_info['import_info']['error_row'] = $error_row;
		    $history_info['import_info']['success_row'] = $success_row;
		  	$this->excel->updateHistoryProgress($this->request->get['import_id'], $history_info['import_info']);
		  	$this->session->data['history_id'] = $this->request->get['import_id'];
		  	$this->response->redirect($this->url->link('store/store'));
  		}
  	}

  	public function export() {
  		if ($this->document->hasPermission('edit','excel/store_import')) {
  			$this->load->model('store/store_type');
  			$store_types = $this->model_store_store_type->getAllTypes();
			$this->load->model('store/store');
  			$filter_data = array();
  			foreach ($this->request->get as $key => $value) {
				if (!in_array($key, $this->no_filter_key)) {
					$filter_data[$key] = $this->request->get[$key];
				}
			}
  			$stores = $this->model_store_store->getStores($filter_data);
  			$excel_data = array();
  			$stt = 1;
  			foreach ($stores as $store) {
  				$excel_data[] = array(
  					$stt,
  					$store['customer_code'],
  					$store['store_code'],
  					$store['store_name'],
  					$store['store_owner'],
  					$store['store_phone'],
  					$store['store_address'],
  					$store['store_place'],
  					$store['store_ward'],
  					$store['store_district'],
  					$store['store_province'],
  					$store_types[$store['store_type_id']] ? $store_types[$store['store_type_id']]['type_name'] : ''
  				);
  				$stt++;
  			}
  			$export_data = array(
				'excel_data' => $excel_data,
				'template_id' => $this->export_id,
				'template_name'  => 'Store-Export-by-'.$this->user->getUsername().'-at-'. date('Ymd-His').'.xlsx'
			);
  			$file = $this->excel->exportWithTemplate($export_data);
			$this->response->setOutput(file_get_contents($file, FILE_USE_INCLUDE_PATH, null));
			$url = $this->_url();
			$this->response->redirect($this->url->link('store/store',  $url, true));
  		}
  	}
}