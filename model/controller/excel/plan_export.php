<?php

class ControllerExcelPlanExport extends Controller {
	private $export_id = 3;
	private $no_filter_key = array('_url','route');
	private $round = array();

	private function _url(){
		$url = '';
		foreach ($this->request->get as $key => $value) {
			if (!in_array($key, $this->no_filter_key)) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($value, ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}

	public function export_detail() {
		if ($this->document->hasPermission('add','excel/plan_export')) {
			if (empty($this->request->get['filter_round_name'])) {
				$this->session->data['error_warning'] = "Vui lòng chọn tháng!";
				$this->response->redirect($this->url->link('plan/plan'));
			}
			$this->load->model('plan/plan');
			$filter = $this->request->get;
			$filter['user_id !'] = 3519;
			$plans = $this->model_plan_plan->getPlans($filter);
			if (!empty($plans)) {
				set_time_limit(1800);
				$template_id = $this->export_id;
				$template_info = $this->excel->getTemplate($template_id,0);
				$template_file = DIR_MEDIA.$template_info['file_path'];
				$settings = $template_info['settings'];
				if(!file_exists($template_file)){
					return;
				}
				$template_name = 'Export_detail_PEPSI_POSM_TET-'.$this->request->get['filter_round_name'].'-by-'.$this->user->getUsername().'-at-'. date('Ymd-His').'.xlsx';
				$file_name = ucwords(str_replace(' ', '-', $template_name));
				$folder = isset($data['folder']) ? $data['folder'] .'/' : '';
				$template_path = DIR_MEDIA.'files/'. $this->config->get('config_project_folder') .'/export/'.$folder;
				if(!is_dir($template_path)){
					@mkdir($template_path, 0777, true);
				}

				$export_file = $template_path.$file_name;
				if(file_exists($export_file)){
					@unlink($export_file);
				}
				if(copy($template_file, $export_file)) {
					try {
						$inputFileType = PHPExcel_IOFactory::identify($export_file);
						$objReader = PHPExcel_IOFactory::createReader($inputFileType);
						$workbook = $objReader->load($export_file);
						$worksheet  = $workbook->setActiveSheetIndex($settings['sheet_index']);
						$row = isset($settings['start_row']) ? $settings['start_row'] : 2;
						// p($row,1);
						$this->load->model('project/project_user');
						$users = $this->model_project_project_user->getProjectUsers();

						$this->load->model('localisation/reason_unsuccess');
						$reasons = $this->model_localisation_reason_unsuccess->getReasons(1);
						$this->load->model('catalog/attribute');
						$this->load->model('catalog/attribute_data');
						$attrs = $this->model_catalog_attribute->getAttributes(array('sort'=>'g.sort_order'));
						foreach ($attrs as &$attr) {
							if ($attr['input'] == 'select') {
								$options = json_decode($attr['options']);
								$attr['options'] = array();
								foreach ($options as $op) {
									$attr['options'][$op->id] = $op->name;
								}
							}
							unset($attr);
						}
						
						$plan_data = $this->model_catalog_attribute_data->getAttrsOfPlan(array('round_name' => $this->request->get['filter_round_name']), false);
						$plandata = array();
						foreach ($plan_data as $value) {
							$val = $value['value'];
							$plandata[$value['plan_id']][$value['attribute_id']] = $value['value'];
						}
						// p($attrs,1);
						// $chanels = array(
						// 	1 => 'ON',
						// 	2 => 'OFF'
						// );
						$this->load->model('store/store_type');
						$store_types = $this->model_store_store_type->getAllTypes();

						$stt = 1;
						foreach ($plans as $key => $p) {
							$data = array(
								$stt,
								$p['customer_code'],
								$p['store_code'],
								$p['store_name'],
								$p['store_owner'],
								$p['store_phone'],
								$p['store_address'],
								$p['store_place'],
								$p['store_ward'],
								$p['store_district'],
								$p['store_province'],
								$p['picture_pepsi_7up'],
								$p['sticker_pushcart'],
								// isset($chanels[$p['chanel']]) ? $chanels[$p['chanel']] : '',
								isset($store_types[$p['store_type_id']]) ? $store_types[$p['store_type_id']]['type_name'] : '',
							);
							
							$data[] = $p['time_upload'] == '0000-00-00 00:00:00' ? '' : datetimeConvert($p['time_upload'],'Y-m-d');

							$plan_rating = '';
							if ($p['plan_status'] == 1) {
								if ($p['plan_rating'] == 1) {
									$plan_rating = 'Thành công';
								} elseif ($p['plan_rating'] == -2) {
									$plan_rating = 'Trường hợp đặc biệt';
								}
							}
							$data[] = $plan_rating;
							$data[] = $p['plan_rating'] == -2 && !empty($reasons[$p['reason_id']]) ? $reasons[$p['reason_id']]['reason_name'] : '';
							$data[] = $p['note_ktc'];
							$data[] = $p['note'];
							// if ($p['reason_id'] > 0 || $p['plan_rating'] == -2) {
							// 	$note = $p['note_ktc'];
							// } else {
							// 	$note = $p['note'];
							// }
							foreach ($attrs as $attr) {
								if (!empty($plandata[$p['plan_id']][$attr['attribute_id']])) {
									$pd = $plandata[$p['plan_id']][$attr['attribute_id']];
									if ($attr['input'] == 'select' && isset($attr['options'][$pd])) {
										$val = $attr['options'][$pd];
									} else {
										$val = $pd;
									}
									// $val = $plandata[$p['plan_id']][$attr['attribute_id']];
								} else {
									$val = '';
								}
								$data[] = $val;
							}

							$data[] = isset($users[$p['user_id']]) ? $users[$p['user_id']]['fullname'] : '';
							$data[] = '=HYPERLINK("'.str_replace('&amp;', '&', $this->url->link('plan/plan/edit', 'plan_id='.$p['plan_id'], true)).'", "Click here")';
							$worksheet->fromArray($data, NULL, 'A'.$row, false);
							$row++;
							$stt++;
							
						}
						header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
						header('Content-Disposition: attachment;filename="'.$file_name.'"');
						header('Cache-Control: max-age=0');
						header("Content-Transfer-Encoding: binary");

						$objWriter = PHPExcel_IOFactory::createWriter($workbook, 'Excel2007');
						$objWriter->setPreCalculateFormulas(false);
						$objWriter->save($export_file);
						$objWriter->save('php://output');
						$this->clearSpreadsheetCache();
						exit;
					} catch(Exception $e){
						echo $e->__toString();
					}
				}
			} else {
				$this->session->data['error_warning'] = "Không có dữ liệu!";
				$this->response->redirect($this->url->link('plan/plan'));
			}
 		}
	}

	public function display_report() {
		if ($this->document->hasPermission('add','excel/plan_export')) {
			// if (empty($this->request->get['filter_round_name'])) {
			// 	$this->session->data['error_warning'] = "Vui lòng chọn tháng!";
			// 	$this->response->redirect($this->url->link('plan/plan'));
			// }
			$this->load->model('plan/plan');
			$filter = $this->request->get;
			$filter['user_id !'] = 3519;
			$filter['sort'] = "time_upload ASC";
			$plans = $this->model_plan_plan->getPlans($filter);
			if (!empty($plans)) {
				set_time_limit(1800);
				$template_id = 7;
				$template_info = $this->excel->getTemplate($template_id,0);
				$template_file = DIR_MEDIA.$template_info['file_path'];
				$settings = $template_info['settings'];
				if(!file_exists($template_file)){
					return;
				}
				$template_name = 'Bao_cao_trung_bay-'.$this->request->get['filter_round_name'].'-by-'.$this->user->getUsername().'-at-'. date('Ymd-His').'.xlsx';
				$file_name = ucwords(str_replace(' ', '-', $template_name));
				$folder = isset($data['folder']) ? $data['folder'] .'/' : '';
				$template_path = DIR_MEDIA.'files/'. $this->config->get('config_project_folder') .'/export/'.$folder;
				if(!is_dir($template_path)){
					@mkdir($template_path, 0777, true);
				}

				$export_file = $template_path.$file_name;
				if(file_exists($export_file)){
					@unlink($export_file);
				}
				if(copy($template_file, $export_file)) {
					try {
						$inputFileType = PHPExcel_IOFactory::identify($export_file);
						$objReader = PHPExcel_IOFactory::createReader($inputFileType);
						$workbook = $objReader->load($export_file);
						$worksheet  = $workbook->setActiveSheetIndex($settings['sheet_index']);
						$row = isset($settings['start_row']) ? $settings['start_row'] : 2;
						// p($row,1);
						$this->load->model('project/project_user');
						$users = $this->model_project_project_user->getProjectUsers();

						$this->load->model('localisation/reason_unsuccess');
						$reasons = $this->model_localisation_reason_unsuccess->getReasons(1);
						$this->load->model('catalog/attribute');
						$this->load->model('catalog/attribute_data');
						$attrs = $this->model_catalog_attribute->getAttributes(array('sort'=>'g.sort_order,a.sort_order','group_codes' => array('sub_display1','sub_display2')));
						foreach ($attrs as &$attr) {
							if ($attr['input'] == 'select') {
								$options = json_decode($attr['options']);
								$attr['options'] = array();
								foreach ($options as $op) {
									$attr['options'][$op->id] = $op->name;
								}
							}
							unset($attr);
						}
						
						$filter['select'] = 'DISTINCT (round_name)';
						$rounds = $this->model_plan_plan->getPlans($filter);
						$round_names = array();
						foreach ($rounds as $r) {
							$round_names[] = $r['round_name'];
						}
						$filter_attr_data = array(
							'round_names' => $round_names,
							'group_codes'=> array('sub_display1','sub_display2')
						);
						$plan_data = $this->model_catalog_attribute_data->getAttrsOfPlan($filter_attr_data, false);
						$plandata = array();
						foreach ($plan_data as $value) {
							$val = $value['value'];
							$plandata[$value['plan_id']][$value['item_number']][$value['attribute_id']] = $value['value'];
						}
						// p($plandata,1);
						$chanels = array(
							1 => 'ON',
							2 => 'OFF',
							3 => 'MT'
						);
						$this->load->model('store/store_type');
						$store_types = $this->model_store_store_type->getAllTypes();
						$tan_suat = array();
						$plan_mapping = array();
						$stt = 1;
						foreach ($plans as $key => $p) {
							$tan_suat[$p['store_id']] = !empty($tan_suat[$p['store_id']]) ? $tan_suat[$p['store_id']]+1 : 1;
							$store_info = array(
								$tan_suat[$p['store_id']],
								$p['region_code'],
								$p['ter'],
								$p['asm'],
								$p['ss'],
								$p['dcr'],
								$p['distributor_code'],
								$p['distributor'],
								$p['customer_code'],
								$p['store_code'],
								$p['store_name'],
								$p['store_address'],
								$p['store_place'],
								$p['store_ward'],
								$p['store_district'],
								$p['store_province'],
								$p['store_phone'],
								isset($store_types[$p['store_type_id']]) ? $store_types[$p['store_type_id']]['type_name'] : '',
								isset($chanels[$p['chanel']]) ? $chanels[$p['chanel']] : '',
								$p['route'],
								$p['qc_code'],
								$p['dung_tich'],
								$p['club'],
								$p['fridge'],
								$p['cvs_shelf'],
								$p['csd_shelf'],
								$p['aqf_shelf'],
								$p['tea_shelf'],
								$p['time_upload'] == '0000-00-00 00:00:00' ? '' : datetimeConvert($p['time_upload'],'Y-m-d'),
								!empty($users[$p['user_id']]) ? $users[$p['user_id']]['fullname'] : '',
							);

							$plan_rating = '';
							if ($p['plan_status'] == 1) {
								if ($p['plan_rating'] == 1) {
									$plan_rating = 'Thành công';
									$note = !empty($reasons[$p['problem']]) ? $reasons[$p['problem']]['code'].' - '.$reasons[$p['problem']]['reason_name'] : '';
									$other_note = $p['note'];
								} elseif ($p['plan_rating'] == -2) {
									$plan_rating = 'Trường hợp đặc biệt';
									$reason_ids = explode(',', $p['reason_id']);
									$note = array();
									if (!empty($reason_ids)) {
										foreach ($reason_ids as $r_id) {
											$note[] = !empty($reasons[$r_id]) ? $reasons[$r_id]['code'].' - '.$reasons[$r_id]['reason_name'] : '';
										}
									}
									$note = implode(', ', $note);
									$other_note = $p['note_ktc'];
								}
							}
							$store_info[] = $plan_rating;
							$store_info[] = $note;
							$store_info[] = $other_note;

							if ($p['plan_rating'] == 1 && !empty($plandata[$p['plan_id']])) {
								foreach ($plandata[$p['plan_id']] as $item_number => $rowdata) {
									$data = $store_info;
									array_unshift($data,$stt);

									foreach ($attrs as $key => $attr) {
										$val = '';
										if (!empty($rowdata[$attr['attribute_id']])) {
											$rd = $rowdata[$attr['attribute_id']];				
											if ($attr['input'] == 'select' && isset($attr['options'][$rd])) {
												$val = $attr['options'][$rd];
											} else {
												$val = $rd;
											}
										} 
										if ($attr['excel_formula']) {
											$val = str_replace('{row}', $row, $attr['excel_formula']);
										}
										
										$data[] = $val;
									}
									
									//Kết quả MHSKU
									$data[] = '=IF(OR(AND(T'.$row.'="OFF",BD'.$row.'>=15,BF'.$row.'>=7),AND(T'.$row.'="ON",S'.$row.'="ITN",BF'.$row.'>=6,BD'.$row.'>=8),AND(T'.$row.'="ON",BF'.$row.'>=7,BD'.$row.'>=9)),"Đạt","Rớt")';
									//FULL CHARGE
									$data[] = '=IF(AZ'.$row.'="","",IF(AZ'.$row.'="Có","Đạt","Rớt"))';
									//HOTZONE
									$data[] = '=IF(AM'.$row.'="","",IF(AM'.$row.'="Có","Đạt","Rớt"))';
									//PURITY
									$data[] = '=IF(AW'.$row.'="","",IF(AW'.$row.'="Không","Đạt","Rớt"))';
									//VC WORKING
									$data[] = '=IF(BB'.$row.'="Có","Đạt","")';
									//POSM 
									$data[] = '=IF(AU'.$row.'="","","Đạt")';
									//PLANNOGRAM
									$data[] = '=IF(BA'.$row.'="","",IF(AND(BA'.$row.'="Có",BF'.$row.'>=BE'.$row.'),"Đạt","Rớt"))';

									//Kết quả trưng bày
									$data[] = '=IF(CN'.$row.'="Đạt",1/1,0/1)';
									$data[] = '=IF(CO'.$row.'="Đạt",1/1,0/1)';
									$data[] = '=IF(CP'.$row.'="Đạt",1/1,0/1)';
									$data[] = '=IF(CQ'.$row.'="Đạt",1/1,0/1)';
									$data[] = '=IF(OR(CR'.$row.'="Đạt",CR'.$row.'=""),1/1,"")';
									$data[] = '=IF(BA'.$row.'="","",IF(AND(BA'.$row.'="Có",BF'.$row.'>=BE'.$row.'),1/1,IF(AND(BA'.$row.'="Có",BF'.$row.'<BE'.$row.'),80%,60%)))';
									$data[] = '=IF(ISNUMBER(SEARCH("TL",AI8)),((CT'.$row.'*40%)+(CU'.$row.'*20%)+(CV'.$row.'*20%)+(CW'.$row.'*10%)+(CX'.$row.'*10%)),(CT'.$row.'*50%)+(CU'.$row.'*20%)+(CV'.$row.'*20%)+(CX'.$row.'*10%))';
									$data[] = '=(CT'.$row.'*50%)+(CV'.$row.'*30%)+(CY'.$row.'*20%)';
									$data[] = '=IF(DA'.$row.'>=80%,"Đạt","Rớt")';
									//link hinh
									$data[] = '=HYPERLINK("'.str_replace('&amp;', '&', $this->url->link('plan/plan/edit', 'plan_id='.$p['plan_id'], true)).'", "Link hình")';

									$worksheet->fromArray($data, NULL, 'A'.$row, false);
									$row++;
									$stt++;
								}
							} else {
								$data = $store_info;
								array_unshift($data,$stt);
								for ($i=0; $i < 72; $i++) { 
									$data[] = '';
								}
								$data[] = '=HYPERLINK("'.str_replace('&amp;', '&', $this->url->link('plan/plan/edit', 'plan_id='.$p['plan_id'], true)).'", "Link hình")';
								$worksheet->fromArray($data, NULL, 'A'.$row, false);
								$row++;
								$stt++;
							}
							
							
							// 
							
							
						}
						header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
						header('Content-Disposition: attachment;filename="'.$file_name.'"');
						header('Cache-Control: max-age=0');
						header("Content-Transfer-Encoding: binary");

						$objWriter = PHPExcel_IOFactory::createWriter($workbook, 'Excel2007');
						$objWriter->setPreCalculateFormulas(false);
						$objWriter->save($export_file);
						$objWriter->save('php://output');
						$this->clearSpreadsheetCache();
						exit;
					} catch(Exception $e){
						echo $e->__toString();
					}
				}
			} else {
				$this->session->data['error_warning'] = "Không có dữ liệu!";
				$this->response->redirect($this->url->link('plan/plan'));
			}
 		}
	} 

	public function survey_report() {
		if ($this->document->hasPermission('add','excel/plan_export')) {
			// if (empty($this->request->get['filter_round_name'])) {
			// 	$this->session->data['error_warning'] = "Vui lòng chọn tháng!";
			// 	$this->response->redirect($this->url->link('plan/plan'));
			// }
			$this->load->model('plan/plan');
			$filter = $this->request->get;
			$filter['user_id !'] = 3519;
			// $filter['plan_id'] = 1;
			$filter['sort'] = "time_upload ASC";
			$plans = $this->model_plan_plan->getPlans($filter);
			if (!empty($plans)) {
				set_time_limit(1800);
				$template_id = 6;
				$template_info = $this->excel->getTemplate($template_id,0);
				$template_file = DIR_MEDIA.$template_info['file_path'];
				$settings = $template_info['settings'];
				if(!file_exists($template_file)){
					return;
				}
				$template_name = 'Bao_cao_khao_sat-'.$this->request->get['filter_round_name'].'-by-'.$this->user->getUsername().'-at-'. date('Ymd-His').'.xlsx';
				$file_name = ucwords(str_replace(' ', '-', $template_name));
				$folder = isset($data['folder']) ? $data['folder'] .'/' : '';
				$template_path = DIR_MEDIA.'files/'. $this->config->get('config_project_folder') .'/export/'.$folder;
				if(!is_dir($template_path)){
					@mkdir($template_path, 0777, true);
				}

				$export_file = $template_path.$file_name;
				if(file_exists($export_file)){
					@unlink($export_file);
				}
				if(copy($template_file, $export_file)) {
					try {
						$inputFileType = PHPExcel_IOFactory::identify($export_file);
						$objReader = PHPExcel_IOFactory::createReader($inputFileType);
						$workbook = $objReader->load($export_file);
						$worksheet  = $workbook->setActiveSheetIndex($settings['sheet_index']);
						$row = isset($settings['start_row']) ? $settings['start_row'] : 2;
						// p($row,1);
						$this->load->model('project/project_user');
						$users = $this->model_project_project_user->getProjectUsers();

						$this->load->model('localisation/reason_unsuccess');
						$reasons = $this->model_localisation_reason_unsuccess->getReasons(1);
						$this->load->model('catalog/attribute');
						$this->load->model('catalog/attribute_data');
						$store_info_attrs = $this->model_catalog_attribute->getAttributes(array('sort'=>'g.sort_order,a.sort_order','g.group_code' => 'store_info'));
						$survey_info_attrs = $this->model_catalog_attribute->getAttributes(array('sort'=>'g.sort_order,a.sort_order','g.parent_group_id' => 2));
						$posm_attrs = $this->model_catalog_attribute->getAttributes(array('sort'=>'g.sort_order,a.sort_order','group_codes' => array('posm_spvb','install_new')));
						$attr_groups = array($store_info_attrs,$survey_info_attrs,$posm_attrs);
						foreach ($attr_groups as &$g) {
							foreach ($g as &$attr) {
								if ($attr['input'] == 'select' || $attr['input'] == 'multiselect') {
									$options = json_decode($attr['options']);
									$attr['options'] = array();
									foreach ($options as $op) {
										$attr['options'][$op->id] = $op->name;
									}
								}
								unset($attr);
							}
							unset($g);
						}
						
						$filter['select'] = 'DISTINCT (round_name)';
						$rounds = $this->model_plan_plan->getPlans($filter);
						$round_names = array();
						foreach ($rounds as $r) {
							$round_names[] = $r['round_name'];
						}
						$filter_attr_data = array(
							'round_names' => $round_names,
							'group_codes'=> array('store_info','sub_survey1','sub_survey2','sub_survey3','sub_survey4','posm_spvb','install_new')
						);
						$plan_data = $this->model_catalog_attribute_data->getAttrsOfPlan($filter_attr_data, false);
						$plandata = array();
						foreach ($plan_data as $value) {
							$val = $value['value'];
							$plandata[$value['plan_id']][$value['attribute_id']] = $value['value'];
						}
						// p($attr_groups,1);
						$chanels = array(
							1 => 'ON',
							2 => 'OFF',
							3 => 'MT'
						);
						$this->load->model('store/store_type');
						$store_types = $this->model_store_store_type->getAllTypes();
						$tan_suat = array();
						$plan_mapping = array();
						$stt = 1;
						foreach ($plans as $key => $p) {
							$tan_suat[$p['store_id']] = !empty($tan_suat[$p['store_id']]) ? $tan_suat[$p['store_id']]+1 : 1;
							$data = array(
								$stt,
								$tan_suat[$p['store_id']],
								$p['region_code'],
								$p['ter'],
								$p['asm'],
								$p['ss'],
								$p['dcr'],
								$p['distributor_code'],
								$p['distributor'],
								$p['customer_code'],
								$p['store_code'],
								$p['store_name'],
								$p['store_address'],
								$p['store_place'],
								$p['store_ward'],
								$p['store_district'],
								$p['store_province'],
								$p['store_phone'],
								isset($store_types[$p['store_type_id']]) ? $store_types[$p['store_type_id']]['type_name'] : '',
								isset($chanels[$p['chanel']]) ? $chanels[$p['chanel']] : '',
								$p['route'],
								$p['qc_code'],
								$p['dung_tich'],
								$p['club'],
								$p['fridge'],
								$p['cvs_shelf'],
								$p['csd_shelf'],
								$p['aqf_shelf'],
								$p['tea_shelf'],
								$p['time_upload'] == '0000-00-00 00:00:00' ? '' : datetimeConvert($p['time_upload'],'Y-m-d'),
								!empty($users[$p['user_id']]) ? $users[$p['user_id']]['fullname'] : '',
							);
							
							$plan_rating = '';
							if ($p['plan_status'] == 1) {
								if ($p['plan_rating'] == 1) {
									$plan_rating = 'Thành công';
									$note = !empty($reasons[$p['problem']]) ? $reasons[$p['problem']]['code'].' - '.$reasons[$p['problem']]['reason_name'] : '';
									$other_note = $p['note'];
								} elseif ($p['plan_rating'] == -2) {
									$plan_rating = 'Trường hợp đặc biệt';
									$reason_ids = explode(',', $p['reason_id']);
									$note = array();
									if (!empty($reason_ids)) {
										foreach ($reason_ids as $r_id) {
											$note[] = !empty($reasons[$r_id]) ? $reasons[$r_id]['code'].' - '.$reasons[$r_id]['reason_name'] : '';
										}
									}
									$note = implode(', ', $note);
									$other_note = $p['note_ktc'];
								}
							}
							$data[] = $plan_rating;
							$data[] = $note;
							$data[] = $other_note;
							if ($p['plan_rating'] == 1 && !empty($plandata[$p['plan_id']])) {

								foreach ($attr_groups as $attrs) {
									foreach ($attrs as $key => $attr) {
										$val = '';
										if (!empty($plandata[$p['plan_id']][$attr['attribute_id']])) {
											$rd = $plandata[$p['plan_id']][$attr['attribute_id']]; 
											if ($attr['input'] == 'multiselect') {
												$option_ids = explode(',', $rd);
												$opt_arr = array();
												foreach ($option_ids as $op_id) {
													$opt_arr[] = $attr['options'][$op_id];
												}
												$val = implode(', ', $opt_arr);
											} elseif ($attr['input'] == 'select') {
												$val = $attr['options'][$rd];
											} else {
												$val = $rd;
											}
										}
										$data[] = $val;
									}
								}
							} else {
								for ($i=0; $i < 29; $i++) { 
									$data[] = '';
								}
							}
							$data[] = $p['manual'];
							$data[] = $p['feedback'];
							$data[] = $p['spvb_feedback'];
							$data[] = '=HYPERLINK("'.str_replace('&amp;', '&', $this->url->link('plan/plan/edit', 'plan_id='.$p['plan_id'], true)).'", "Link hình")';
							$worksheet->fromArray($data, NULL, 'A'.$row, false);
							$row++;
							$stt++;
						}
						// p(1,1);
						header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
						header('Content-Disposition: attachment;filename="'.$file_name.'"');
						header('Cache-Control: max-age=0');
						header("Content-Transfer-Encoding: binary");

						$objWriter = PHPExcel_IOFactory::createWriter($workbook, 'Excel2007');
						$objWriter->setPreCalculateFormulas(false);
						$objWriter->save($export_file);
						$objWriter->save('php://output');
						$this->clearSpreadsheetCache();
						exit;
					} catch(Exception $e){
						echo $e->__toString();
					}
				}
			} else {
				$this->session->data['error_warning'] = "Không có dữ liệu!";
				$this->response->redirect($this->url->link('plan/plan'));
			}
 		}
	}

	public function sumary_report() {
		if ($this->document->hasPermission('add','excel/plan_export')) {
			// if (empty($this->request->get['filter_round_name'])) {
			// 	$this->session->data['error_warning'] = "Vui lòng chọn tháng!";
			// 	$this->response->redirect($this->url->link('plan/plan'));
			// }
			$this->load->model('plan/plan');
			$filter = $this->request->get;
			$filter['user_id !'] = 3519;
			$filter['sort'] = "time_upload ASC";
			$plans = $this->model_plan_plan->getPlans($filter);
			if (!empty($plans)) {
				set_time_limit(1800);
				$template_id = 8;
				$template_info = $this->excel->getTemplate($template_id,0);
				$template_file = DIR_MEDIA.$template_info['file_path'];
				$settings = $template_info['settings'];
				if(!file_exists($template_file)){
					return;
				}
				$template_name = 'Bao_cao_tong_ket-'.$this->request->get['filter_round_name'].'-by-'.$this->user->getUsername().'-at-'. date('Ymd-His').'.xlsx';
				$file_name = ucwords(str_replace(' ', '-', $template_name));
				$folder = isset($data['folder']) ? $data['folder'] .'/' : '';
				$template_path = DIR_MEDIA.'files/'. $this->config->get('config_project_folder') .'/export/'.$folder;
				if(!is_dir($template_path)){
					@mkdir($template_path, 0777, true);
				}

				$export_file = $template_path.$file_name;
				if(file_exists($export_file)){
					@unlink($export_file);
				}
				if(copy($template_file, $export_file)) {
					try {
						$inputFileType = PHPExcel_IOFactory::identify($export_file);
						$objReader = PHPExcel_IOFactory::createReader($inputFileType);
						$workbook = $objReader->load($export_file);
						$worksheet  = $workbook->setActiveSheetIndex($settings['sheet_index']);
						$row = isset($settings['start_row']) ? $settings['start_row'] : 2;
						// p($row,1);
						$this->load->model('project/project_user');
						$users = $this->model_project_project_user->getProjectUsers();

						$this->load->model('localisation/reason_unsuccess');
						$reasons = $this->model_localisation_reason_unsuccess->getReasons(1);
						$this->load->model('catalog/attribute');
						$this->load->model('catalog/attribute_data');
						
						$store_info_attrs = $this->model_catalog_attribute->getAttributes(array('sort'=>'g.sort_order,a.sort_order','g.group_code' => 'store_info'));
						foreach ($store_info_attrs as &$attr) {
							if ($attr['input'] == 'select' || $attr['input'] == 'multiselect') {
								$options = json_decode($attr['options']);
								$attr['options'] = array();
								foreach ($options as $op) {
									$attr['options'][$op->id] = $op->name;
								}
							}
							unset($attr);
						}

						$sub_survey1_posm_attrs = $this->model_catalog_attribute->getAttributes(array('sort'=>'g.sort_order,a.sort_order','group_codes' => array('sub_survey1','posm_spvb','install_new')));
						foreach ($sub_survey1_posm_attrs as &$attr) {
							if ($attr['input'] == 'select' || $attr['input'] == 'multiselect') {
								$options = json_decode($attr['options']);
								$attr['options'] = array();
								foreach ($options as $op) {
									$attr['options'][$op->id] = $op->name;
								}
							}
							unset($attr);
						}

						$sub_display1_attrs = $this->model_catalog_attribute->getAttributes(array('sort'=>'g.sort_order,a.sort_order','group_codes' => array('sub_display1')));
						p($sub_display1_attrs,1);
						$filter['select'] = 'DISTINCT (round_name)';
						$rounds = $this->model_plan_plan->getPlans($filter);
						$round_names = array();
						foreach ($rounds as $r) {
							$round_names[] = $r['round_name'];
						}
						$filter_attr_data = array(
							'round_names' => $round_names,
							'group_codes'=> array('store_info','sub_survey1','posm_spvb','install_new','sub_display1','displan_result','displan_result2')
						);
						$plan_data = $this->model_catalog_attribute_data->getAttrsOfPlan($filter_attr_data, false);
						$plandata = array();
						foreach ($plan_data as $value) {
							$val = $value['value'];
							$plandata[$value['plan_id']][$value['item_number']][$value['attribute_id']] = $value['value'];
						}
						// p($plandata,1);
						$chanels = array(
							1 => 'ON',
							2 => 'OFF',
							3 => 'MT'
						);
						$status = array('','Đạt','Rớt');
						$this->load->model('store/store_type');
						$store_types = $this->model_store_store_type->getAllTypes();
						$tan_suat = array();
						$plan_mapping = array();
						$stt = 1;
						foreach ($plans as $key => $p) {
							$tan_suat[$p['store_id']] = !empty($tan_suat[$p['store_id']]) ? $tan_suat[$p['store_id']]+1 : 1;
							$store_info = array(
								$tan_suat[$p['store_id']],
								$p['region_code'],
								$p['ter'],
								$p['asm'],
								$p['ss'],
								$p['dcr'],
								$p['distributor_code'],
								$p['distributor'],
								$p['customer_code'],
								$p['store_code'],
								$p['store_name'],
								$p['store_address'],
								$p['store_place'],
								$p['store_ward'],
								$p['store_district'],
								$p['store_province'],
								$p['store_phone'],
								isset($store_types[$p['store_type_id']]) ? $store_types[$p['store_type_id']]['type_name'] : '',
								isset($chanels[$p['chanel']]) ? $chanels[$p['chanel']] : '',
								$p['route'],
								$p['qc_code'],
								$p['dung_tich'],
								$p['club'],
								$p['fridge'],
								$p['cvs_shelf'],
								$p['csd_shelf'],
								$p['aqf_shelf'],
								$p['tea_shelf'],
								$p['time_upload'] == '0000-00-00 00:00:00' ? '' : datetimeConvert($p['time_upload'],'Y-m-d'),
								!empty($users[$p['user_id']]) ? $users[$p['user_id']]['fullname'] : '',
							);

							$plan_rating = '';
							if ($p['plan_status'] == 1) {
								if ($p['plan_rating'] == 1) {
									$plan_rating = 'Thành công';
									$note = !empty($reasons[$p['problem']]) ? $reasons[$p['problem']]['code'].' - '.$reasons[$p['problem']]['reason_name'] : '';
									$other_note = $p['note'];
								} elseif ($p['plan_rating'] == -2) {
									$plan_rating = 'Trường hợp đặc biệt';
									$reason_ids = explode(',', $p['reason_id']);
									$note = array();
									if (!empty($reason_ids)) {
										foreach ($reason_ids as $r_id) {
											$note[] = !empty($reasons[$r_id]) ? $reasons[$r_id]['code'].' - '.$reasons[$r_id]['reason_name'] : '';
										}
									}
									$note = implode(', ', $note);
									$other_note = $p['note_ktc'];
								}
							}
							$store_info[] = $plan_rating;
							$store_info[] = $note;
							$store_info[] = $other_note;

							foreach ($store_info_attrs as $attr) {
								$val = '';
								if (!empty($plandata[$p['plan_id']][0][$attr['attribute_id']])) {
									$rd = $plandata[$p['plan_id']][0][$attr['attribute_id']]; 
									if ($attr['input'] == 'multiselect') {
										$option_ids = explode(',', $rd);
										$opt_arr = array();
										foreach ($option_ids as $op_id) {
											$opt_arr[] = $attr['options'][$op_id];
										}
										$val = implode(', ', $opt_arr);
									} elseif ($attr['input'] == 'select') {
										$val = $attr['options'][$rd];
									} else {
										$val = $rd;
									}
								}
								$store_info[] = $val;
							}
							for ($i=0; $i < 21; $i++) { 
								$store_info[] = '';
							}

							foreach ($sub_survey1_posm_attrs as $attr) {
								$val = '';
								if (!empty($plandata[$p['plan_id']][0][$attr['attribute_id']])) {
									$rd = $plandata[$p['plan_id']][0][$attr['attribute_id']]; 
									if ($attr['input'] == 'multiselect') {
										$option_ids = explode(',', $rd);
										$opt_arr = array();
										foreach ($option_ids as $op_id) {
											$opt_arr[] = $attr['options'][$op_id];
										}
										$val = implode(', ', $opt_arr);
									} elseif ($attr['input'] == 'select') {
										$val = $attr['options'][$rd];
									} else {
										$val = $rd;
									}
								}
								$store_info[] = $val;
							}
							$store_info[] = $p['manual'];
							$store_info[] = $p['feedback'];
							$store_info[] = $p['spvb_feedback'];
							$store_info[] = '=HYPERLINK("'.str_replace('&amp;', '&', $this->url->link('plan/plan/edit', 'plan_id='.$p['plan_id'], true)).'", "Link hình")';
							
							if ($p['plan_rating'] == 1 && !empty($plandata[$p['plan_id']])) {
								foreach ($plandata[$p['plan_id']] as $item_number => $rowdata) {
									if ($item_number > 0) {
										$data = $store_info;
										array_unshift($data,$stt);
										$data[43] = '';
										$data[44] = '';
										$data[45] = '';
										$data[46] = !empty($rowdata[84]) ? $rowdata[84] : '';
										$data[47] = !empty($rowdata[92]) ? $status[$rowdata[92]] : '';
										$data[48] = !empty($rowdata[93]) ? $status[$rowdata[93]] : '';
										$data[49] = !empty($rowdata[94]) ? $status[$rowdata[94]] : '';
										$data[50] = !empty($rowdata[95]) ? $status[$rowdata[95]] : '';
										$data[51] = !empty($rowdata[96]) ? $status[$rowdata[96]] : '';
										$data[51] = !empty($rowdata[97]) ? $status[$rowdata[97]] : '';
										$data[52] = !empty($rowdata[98]) ? $status[$rowdata[98]] : '';
										$data[53] = !empty($rowdata[99]) ? $rowdata[99].'%' : '0%';
										$data[53] = !empty($rowdata[100]) ? $rowdata[100].'%' : '0%';
										$data[54] = !empty($rowdata[101]) ? $rowdata[101].'%' : '0%';
										$data[55] = !empty($rowdata[102]) ? $rowdata[102].'%' : '0%';
										$data[56] = !empty($rowdata[103]) ? $rowdata[103].'%' : '0%';
										$data[57] = !empty($rowdata[104]) ? $rowdata[104].'%' : '0%';
										$data[58] = !empty($rowdata[105]) ? $rowdata[105].'%' : '0%';
										$data[59] = !empty($rowdata[106]) ?  $status[$rowdata[106]] : '';
										$worksheet->fromArray($data, NULL, 'A'.$row, false);
										$row++;
										$stt++;
									}
								}
							} else {
								$data = $store_info;
								array_unshift($data,$stt);
								$worksheet->fromArray($data, NULL, 'A'.$row, false);
								$row++;
								$stt++;
							}
							
							
						}
						header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
						header('Content-Disposition: attachment;filename="'.$file_name.'"');
						header('Cache-Control: max-age=0');
						header("Content-Transfer-Encoding: binary");

						$objWriter = PHPExcel_IOFactory::createWriter($workbook, 'Excel2007');
						$objWriter->setPreCalculateFormulas(false);
						$objWriter->save($export_file);
						$objWriter->save('php://output');
						$this->clearSpreadsheetCache();
						exit;
					} catch(Exception $e){
						echo $e->__toString();
					}
				}
			} else {
				$this->session->data['error_warning'] = "Không có dữ liệu!";
				$this->response->redirect($this->url->link('plan/plan'));
			}
 		}
	} 
}