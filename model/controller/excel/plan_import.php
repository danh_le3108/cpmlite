<?php

class ControllerExcelPlanImport extends Controller {
	private $import_id = 2;
	private $export_id = 2;
	private $no_filter_key = array('_url','route');
	private $excel_header = array(
		'A' => 'Mã cửa hàng',
		'B' => 'Mã nhân viên',
		'C' => 'Plan Name',
		'D' => 'Start Day',
		'E' => 'End Day'
	);
	private function _url(){
		$url = '';
		foreach ($this->request->get as $key => $value) {
			if (!in_array($key, $this->no_filter_key)) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($value, ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}

	public function index($filter_data){
		if ($this->document->hasPermission('access','excel/plan_import')) {
			$this->document->addStyle('assets/plugins/menu-material/collapzion.min.css');
			$languages= $this->load->language('excel/template');
			foreach($languages as $key=>$value){
					$data[$key] = $value;
			}
			if(!empty($this->session->data['history_id'])) {
				$data['history_id'] = $this->session->data['history_id'];
				unset($this->session->data['history_id']);
			}
			$data['template_id'] = $this->import_id;
			$data['has_import'] = $this->document->hasPermission('add','excel/plan_import');
			$data['has_export'] = $this->document->hasPermission('edit','excel/plan_import');
			$data['has_export_detail'] = $this->document->hasPermission('add','excel/plan_export');
			$data['has_export_merchandising'] = $this->document->hasPermission('edit','excel/plan_export');
			
			$data['has_report_qc'] = $this->document->hasPermission('add','excel/plan_report');
			$data['is_admin'] = $this->user->getGroupId();
			$data['url'] = $this->_url();
			$data['template_url'] = $this->url->link('excel/template/template',  '&template_id='.$this->import_id, true);
			$data['form'] = $this->url->link('excel/plan_import/import');
			$template = 'excel/plan_import';
			return $this->load->view($template, $data);
		}
	}

	public function import() {
		if ($this->document->hasPermission('add','excel/plan_import') && !empty($this->request->files['file_import'])) {
			if (empty($this->request->files['file_import']['name'])) {
				$this->session->data['error_warning'] = 'Warning: Chưa chọn file!';
				$this->response->redirect($this->url->link('plan/plan'));
			} 
			if (substr($this->request->files['file_import']['name'], -5) != '.xlsx') {
				$this->session->data['error_warning'] = 'Warning: File không đúng định dạng!';
				$this->response->redirect($this->url->link('plan/plan'));
			}
			$file_name = 'Plan-Import-by-' . $this->user->getUserName(). '-at-'. date('Ymd-His') .'.xlsx';
			$dir = DIR_MEDIA . 'files/'. $this->config->get('config_project_folder') .'/import/plan/';
		    if(!is_dir($dir)){
		      	@mkdir($dir, 0777, true);
				@touch($dir. 'index.html');
		    }
			$file = $dir . $file_name;
		    move_uploaded_file($this->request->files['file_import']['tmp_name'], $file);
		    if(file_exists($file)){
		    	if (!$this->excel->readFileCellKey($file, 4, $this->excel_header)) {
		    		$this->session->data['error_warning'] = 'Warning: Sai template';
					$this->response->redirect($this->url->link('plan/plan'));
		    	} else {
		    		$template_info = $this->excel->getTemplate($this->import_id);
			      	$filesize = $this->request->files['file_import']['size'];
			      	$import_info = $this->excel->getColRow($file);
			      	$import_id = $this->excel->addImportHistory($template_info['template_id'], $file, $import_info);
					$data['callback'] = str_replace('&amp;','&',$this->url->link('excel/plan_import/import_data',   '&import_id='.$import_id."&file=". $file, true));
		    	}
			}
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
			
			$template = 'excel/redirect';
			$this->response->setOutput($this->load->view($template, $data));
		}
	}

	public function import_data() {
		if ($this->document->hasPermission('add','excel/plan_import') && !empty($this->request->get['import_id']) && !empty($this->request->get['file'])) {
			$this->load->model('store/store');
			$stores = array();
			foreach ($this->model_store_store->getStores() as $s) {
				$stores[$s['store_code']] = $s;
			}
			$this->load->model('project/project_user');
			$users = $this->model_project_project_user->getProjectUsersByGroupID($this->config->get('config_user_staff'));
			$user_mapping = array();
			foreach ($users as $key => $value) {
				$user_mapping[$value['usercode']] = $value;
			}
			$this->load->model('history/history');
			$history = array(
				'table_name' => 'plan',
				'user_id' => $this->user->getId(),
				'user_update' => $this->user->getUserName(),
				'controller' => 'excel/plan_import'
			);
			$template_info = $this->excel->getTemplate($this->import_id);
		    $settings = $template_info['settings'];
	  		$result = $this->excel->read_all($this->request->get['file'], $settings['start_row']);
			$data_row = array();
		    $error_row = $success_row = 0;
		    $error = array();
		    $config_project_folder = $this->config->get('config_project_folder');
		    $require = array('A','B','C','D','E');
		    $check_exist = array(
		    	'A' => $stores,
		    	'B' => $user_mapping
		    );
		    $check_time = array('D','E');
		    $this->load->model('plan/plan');

		    foreach ($result['data'] as $rid => $row) {
		    	$error_message = array();

	  			if (!empty($row)) {
	  				foreach ($row as $col => $value) {
	  					$value = trim($value);
			    		if (in_array($col,$require) && empty($value)) {
			    			$error_message[] = $this->excel_header[$col] .' không được trống';
			    		} else {
				    		if (array_key_exists($col,$check_exist) && !isset($check_exist[$col][$value])) {
				    			$error_message[] = $this->excel_header[$col] .' không tồn tại.';
				    		} elseif (in_array($col, $check_time)) {
				    			if (!DateTime::createFromFormat("d/m/Y", $value)) {
				    				$error_message[] = $this->excel_header[$col] .' không đúng định dạng.';
				    			}
				    		} 
			    			
			    		}
			    	}
	  			}
	  			
	  			if (empty($error_message)) {
	  				$store = $stores[trim($row['A'])];
	  				$user = $user_mapping[trim($row['B'])];
	  				$round_name = datetimeConvert($row['D'],'Y-m');
	  				$data = array(
		  				'store_id' => $store['store_id'],
		  				'plan_name' => trim($row['C']),
		  				'round_name' => $round_name,
		  				'store_code' => $store['store_code'],
		  				'date_start' => datetimeConvert($row['D'],'Y-m-d'),
		  				'date_end' => datetimeConvert($row['E'],'Y-m-d'),
		  				'user_id' => $user['user_id'],
		  				'usercode' => $user['username'].' - '.$user['fullname'],
		  				'image_path' => 'files/'.$config_project_folder.'/'.$round_name.'/'. $store['store_code'].'/',
		  				'import_id' => $this->request->get['import_id'],
		  				// 'picture_pepsi_7up' => $row['F'],
		  				// 'sticker_pushcart' => $row['G'],
		  			);
		  			
		  			$plan = $this->model_plan_plan->getPlans(array('plan_name'=>$data['plan_name'],'s.store_id'=>$store['store_id']));
		  			if (!empty($plan)) {
						$plan_id = $plan[0]['plan_id'];
						// $history['table_primary_id'] = $plan_id;
						// $history['data'] = json_encode($data);
						// $history['old_data'] = json_encode($plan[0]);
						// $this->model_history_history->add($history);
						$this->model_plan_plan->updatePlan($plan_id,$data);
					} else {
						$this->model_plan_plan->addPlan($data);
					}
					$success_row++;
	  			} else {
	  				$error[$rid] = $row;
					$error[$rid]['message'] = implode(' | ', $error_message);
					$error_row++;
	  			}
	  		}

	  		$history_info = $this->excel->getImportHistory($this->request->get['import_id']);
	  		$history_info['import_info']['error'] = $error;
		    $history_info['import_info']['error_row'] = $error_row;
		    $history_info['import_info']['success_row'] = $success_row;
		  	$this->excel->updateHistoryProgress($this->request->get['import_id'], $history_info['import_info']);
		  	$this->session->data['history_id'] = $this->request->get['import_id'];
		  	$this->response->redirect($this->url->link('plan/plan'));
		}
	}

	public function export() {
		if ($this->document->hasPermission('edit','excel/plan_import')) {
			$filter_data = array(
				// 'user_id !' => 3519
			);
			foreach ($this->request->get as $key => $value) {
				if (!in_array($key, $this->no_filter_key)) {
					$filter_data[$key] = $this->request->get[$key];
				}
			}
			$this->load->model('plan/plan');
			$plans = $this->model_plan_plan->getPlans($filter_data);
			$excel_data = array();
  			foreach ($plans as $plan) {
  				$user = explode(' - ', $plan['usercode']);
  				$excel_data[] = array(
  					$plan['store_code'],
  					$user[0],
  					$plan['plan_name'],
  					datetimeConvert($plan['date_start'],'d/m/Y'),
  					datetimeConvert($plan['date_end'],'d/m/Y'),
  					// $plan['picture_pepsi_7up'],
  					// $plan['sticker_pushcart']
  				);
  			}
  			// p($excel_data,1);
  			if (!empty($excel_data)) {
	  			$export_data = array(
					'excel_data' => $excel_data,
					'template_id' => $this->export_id,
					'template_name'  => 'Plan-Export-by-'.$this->user->getUsername().'-at-'. date('Ymd-His').'.xlsx'
				);
	  			$file = $this->excel->exportWithTemplate($export_data);
				$this->response->setOutput(file_get_contents($file, FILE_USE_INCLUDE_PATH, null));
  			} else {
  				$url = $this->_url();
  				$this->session->data['error_warning'] = 'Không có dữ liệu!';
				$this->response->redirect($this->url->link('plan/plan',  $url, true));
  			}
			
		}
	}
}



