<?php
class ControllerExcelTemplate extends Controller {
	private $error = array();
	private $limit = 10;
	private function _url(){
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		return $url;
	}

	public function template() {
		if ($this->document->hasPermission('access', 'excel/template')&&isset($this->request->get['template_id'])) {
			$this->load->language('excel/template');
			$url = $this->_url();
			$template_info = $this->excel->getTemplate($this->request->get['template_id']);

			if($template_info){
				$this->response->setOutput($this->excel->template($template_info));
			} else {
				$this->session->data['error'] = $this->language->get('text_error_template');
			}
			$this->response->redirect($this->url->link('excel/template', $url.'' , true));
			
		}else{
			echo $this->language->get('text_error_template');
		}
	}
	public function export_history() {

		$languages= $this->load->language('excel/template');

		foreach($languages as $key=>$value){

				$data[$key] = $value;

		}


		if(isset($this->request->get['template_id'])){
			$this->load->model('excel/handle');
	
	
	
	
			$histories = $this->excel->getExportHistoriesByTemplateID($this->request->get['template_id']);
	
	
	
			$data['histories'] = array();
	
	
			$http_server = HTTP_SERVER;
		
	
			foreach($histories as $history){
	
				$info = json_decode($history['export_info'], true);
				$href = str_replace(DIR_MEDIA,$http_server.'media/',$history['template_path']);
	
				$data['histories'][] = array(
	
					'import_id' => $history['import_id'],
	
					'project_id' => $history['project_id'],
	
					'template_id' => $history['template_id'],
	
					'user_id' => $history['user_id'],
	
					'href' => $href,
					'template_path' => $history['template_path'],
					
					'info' => $info,
	
					'rows' => isset($info['rows']) ? $info['rows'] : 0,
					'success_row' => isset($info['success_row']) ? $info['success_row'] : 0,
					'update_row' => isset($info['update_row']) ? $info['update_row'] : 0,
	
					'error_row' => isset($info['error_row']) ? $info['error_row'] : 0,
	
					'date_added' => $history['date_added'],
	
					'download' => $this->url->link('excel/template/download_error', '&import_id=' .$history['export_id'] , true),
	
				);
	
			}
	
	
			$template = 'excel/import_history';
	
			$this->response->setOutput($this->load->view($template, $data));
		}else{
			echo 'Không tìm thấy!';
		}
	}
	public function import_history() {
		$languages= $this->load->language('excel/template');

		foreach($languages as $key=>$value){

				$data[$key] = $value;

		}
		
		if ($this->document->hasPermission('access', 'excel/template') &&isset($this->session->data['history_id'])) {
			$data['history_id'] = $this->session->data['history_id'];
			
			unset($this->session->data['history_id']);
		
		} else {
			$data['history_id'] = null;
		}
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$filter_data = array();
		
		if(isset($this->request->get['template_id'])){
			$this->load->model('excel/handle');
	
			$filter_data['start'] = ($page - 1) * $this->limit;
			$filter_data['limit'] = $this->limit;
	
			$histories = $this->excel->getImportHistoriesByTemplateID($this->request->get['template_id'],$filter_data);
	
			
			$data['histories'] = array();
	
			$http_server = HTTP_SERVER;
	
	
			foreach($histories as $history){
	
				$info = json_decode($history['import_info'], true);
				$href = str_replace(DIR_MEDIA,$http_server.'media/',$history['template_path']);
				$data['histories'][] = array(
	
					'import_id' => $history['import_id'],
	
					'project_id' => $history['project_id'],
	
					'template_id' => $history['template_id'],
	
					'user_id' => $history['user_id'],
	
					'href' => $href,
					'template_path' => $history['template_path'],
					
					'info' => $info,
	
					'rows' => isset($info['rows']) ? $info['rows'] : 0,
					'success_row' => isset($info['success_row']) ? $info['success_row'] : 0,
					'update_row' => isset($info['update_row']) ? $info['update_row'] : 0,
	
					'error_row' => isset($info['error_row']) ? $info['error_row'] : 0,
	
					'date_added' => $history['date_added'],
	
					'download' => $this->url->link('excel/template/download_error', '&import_id=' .$history['import_id'] , true),
	
				);
	
			}
	
			$import_total = $this->excel->countTotalImportHistories($this->request->get['template_id']);
			
			$pagination = new Pagination($this->language->get('text_pagination'));
			$pagination->total = $import_total;
			$pagination->page = $page;
			$pagination->limit = $this->limit;
			$pagination->url = $this->url->link('excel/template/import_history', 'template_id=' . $this->request->get['template_id'] . '&page={page}', true);
			
			
			$data['pagination'] = $pagination->render();
	
			$data['results'] = $pagination->results();
		
			$template = 'excel/import_history';
	
			$this->response->setOutput($this->load->view($template, $data));
		}else{
			echo 'Không tìm thấy!';
		}
	}
	public function download_error(){

		if(isset($this->request->get['import_id'])){
			$import_id = $this->request->get['import_id'];


			$this->load->model('excel/handle');

			$history_data = $this->excel->getImportHistory($import_id);

			$error = $history_data['import_info']['error'];

			$template_id = $history_data['template_id'];

			$template_info = $this->excel->getTemplate($template_id);

			if(!empty($template_info['file_path'])){
				
				$export_data = array(
					'excel_data' => $error,
					'template_id' => $template_id,
					'template_name' => 'Import-Error-'.$import_id.'-'. date('Ymd-His') .'.xlsx'
				);

				$this->excel->exportWithTemplate($export_data);
				
			} else { 
				$this->model_excel_handle->download($template_id, $error);
			}

		}

		$this->response->redirect($this->url->link('excel/template',   '' , true));
	}
	
	public function index() {
		$this->load->language('excel/template');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('excel/template');

		$this->getList();
	}

	public function add() {
		$this->load->language('excel/template');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('excel/template');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$template_id = $this->model_excel_template->addTemplate($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = $this->_url();

			if ($template_id) {
				$url .= '&template_id=' . $template_id;
				$href = $this->url->link('excel/template/edit',  $url. '' , true);
			}else{
				$href = $this->url->link('excel/template',  $url. '' , true);
			}

			$this->response->redirect($href);
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('excel/template');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('excel/template');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_excel_template->editTemplate($this->request->get['template_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = $this->_url();

			$this->response->redirect($this->url->link('excel/template',  $url. '' , true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('excel/template');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('excel/template');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $template_id) {
				$this->model_excel_template->deleteTemplate($template_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = $this->_url();

			$this->response->redirect($this->url->link('excel/template',  $url. '' , true));
		}

		$this->getList();
	}

	protected function getList() {
		$languages = $this->load->language('excel/template');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'template_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = $this->_url();

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('excel/template',  $url. '' , true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);

		$data['add'] = $this->url->link('excel/template/add',  $url. '' , true);
		$data['delete'] = $this->url->link('excel/template/delete',  $url. '' , true);

		$data['templates'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_project'),
			'limit' => $this->config->get('config_limit_project')
		);
		$total_import = $this->excel->getTotalImportHistories();
		$total_export = $this->excel->getTotalExportHistories();
		$template_total = $this->excel->getTotalTemplates();

		$results = $this->excel->getTemplates($filter_data);

		foreach ($results as $result) {
			$action = array();
			$action['download'] = array(
				'text'=>$this->language->get('button_download'),
				'icon'=>'fa-download',
				'btn' =>'warning',
				'href'=>$this->url->link('excel/template/template',  '&template_id=' . $result['template_id'] . $url. '' , true)
			);
			$action['edit'] = array(
				'text'=>$this->language->get('button_edit'),
				'icon'=>'fa-pencil',
				'btn' =>'primary',
				'href'=>$this->url->link('excel/template/edit',  '&template_id=' . $result['template_id'] . $url. '' , true)
			);
			$data['templates'][] = array(
				'template_id' => $result['template_id'],
				'template_name'      => $result['template_name'],
				'total_import'      => isset($total_import[$result['template_id']])?$total_import[$result['template_id']]:0,
				'total_export'      => isset($total_export[$result['template_id']])?$total_export[$result['template_id']]:0,
				'table'      => $result['table'],
				'action'      => $action
			);
		}


		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('excel/template',  '&sort=name' . $url. '' , true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $template_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_project');
		$pagination->url = $this->url->link('excel/template',  $url . '&page={page}'. '' , true);

		$data['pagination'] = $pagination->render();

		$data['results'] = $pagination->results();

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$template = 'excel/template_list';

		 //$this->response->setOutput($this->load->view($template, $data));
    return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}

	protected function getForm() {
		$languages = $this->load->language('excel/template');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		$data['text_form'] = !isset($this->request->get['template_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		$this->document->addStyle('assets/editor/plugins/spectrum/spectrum.css');
		$this->document->addScript('assets/editor/plugins/spectrum/spectrum-admin.js');


		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['error_name'])) {
			$data['error_name'] = $this->error['error_name'];
		} else {
			$data['error_name'] = '';
		}
		if (isset($this->error['error_table'])) {
			$data['error_table'] = $this->error['error_table'];
		} else {
			$data['error_table'] = '';
		}

		$url = $this->_url();

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('excel/template',  $url. '' , true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);

		if (!isset($this->request->get['template_id'])) {
			$data['action'] = $this->url->link('excel/template/add',  $url. '' , true);
		} else {
			$data['action'] = $this->url->link('excel/template/edit',  '&template_id=' . $this->request->get['template_id'] . $url. '' , true);
		}

		$data['cancel'] = $this->url->link('excel/template',  $url. '' , true);

		$data['token'] ='';

		if (isset($this->request->get['template_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$template_info = $this->model_excel_template->getTemplate($this->request->get['template_id']);
		}

		if (isset($this->request->post['template_name'])) {
			$data['template_name'] = $this->request->post['template_name'];
		} elseif (!empty($template_info)) {
			$data['template_name'] = $template_info['template_name'];
		} else {
			$data['template_name'] = '';
		}

		if (isset($this->request->post['table'])) {
			$table = $this->request->post['table'];
		} elseif (!empty($template_info)) {
			$table = $template_info['table'];
		} else {
			$table = array();
		}

			$data['table'] = $table;


		$table_fields = array();
		if(!empty($table)){
			foreach($table as $tab){
				$table_fields[$tab] = $this->model_excel_template->getFields($tab);
			}
		}
		//$tab_label = ucwords(str_replace('_',' ',$tkey));
		$fields['tables'] = array();
		if(!empty($table_fields)){
			foreach($table_fields as $tkey => $tab){
        foreach($tab as $value){
				  $fields['tables'][$tkey][$value['field']] = array(
				  	'table'      => $tkey,
						'heading'    => true,
						'overwrite'  => false,
						'heading_bg' => 'DDDDDD',
						'field'      => $value['field'],
						'required'   =>	false,
						'based'      => $tkey.'.'.$value['field'],
						'get_based'  => $value['field'],
						'label'      => $value['label']
				  );
			  }
			}
		}

		$data['fields'] = $fields;

		if (isset($this->request->post['settings'])) {
			$settings = $this->request->post['settings'];
		} elseif (!empty($template_info)) {
			$settings = $template_info['settings'];
		} else {
			$settings = array();
		}

		$data['settings'] = $settings;


		$data['tables'] = array();
		$tables = $this->model_excel_template->getTables();
		$replace = DB_PREFIX;
		foreach($tables as $table){
			$value = !empty($replace)?str_replace($replace,'',$table):$table;
			$data['tables'][] = array(
				'value'=>$value,
				'label'=>$value
			);
		}


		$data['dowload_url'] = HTTP_SERVER.'media/';
		
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$data['url_upload'] = $this->url->link('excel/template/upload_file', '' , true);
		$data['template_id'] = isset($this->request->get['template_id']) ? $this->request->get['template_id'] : 0;
		$this->document->addScript('assets/editor/plugins/ajaxupload.js');

		if (isset($this->request->post['file_path'])) {
			$data['file_path'] = $this->request->post['file_path'];
		} elseif (!empty($template_info)) {
			$data['file_path'] = $template_info['file_path'];
		} else {
			$data['file_path'] = '';
		}

		// $this->response->setOutput($this->load->view('excel/template_form', $data));
		$template = 'excel/template_form';

    return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('edit', 'excel/template')) {
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		if (!isset($this->request->post['table'])) {
			$this->error['error_table'] = $this->language->get('text_error_table');
		}
		if ((utf8_strlen($this->request->post['template_name']) < 3) || (utf8_strlen($this->request->post['template_name']) > 64)) {
			$this->error['error_name'] = $this->language->get('text_error_name');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('edit', 'excel/template')||$this->user->getGroupId()!=1) {
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return !$this->error;
	}

	public function upload_file(){
		$json = array();
		if (!$json) {
			if (!empty($this->request->files['file']['name'])) {
				$upload_file = $this->request->files['file']['name'];
				$ext = pathinfo($upload_file, PATHINFO_EXTENSION);
				$new_name = str_replace('.'.$ext,'',$upload_file);
				$ext_arr = array('xlsx', 'xls');
				if(in_array($ext, $ext_arr)){
					$this->load->model('excel/template');
					if(isset($this->request->post['template_id'])){
						$template_id = $this->request->post['template_id'];
					} else {
						$template_id = 0;
					}
					$template_info = $this->model_excel_template->getTemplate($template_id);
					
					$file_name = $new_name . '-' . date('Ymd-His') .'.xlsx';

					$dir = DIR_MEDIA . 'files/'. $this->config->get('config_project_folder') .'/excel_template/';
					if(!is_dir($dir)){
						@mkdir($dir, 0777, true);
						@touch($dir. 'index.html');
					}

				$project_folder = $this->config->get('config_project_folder');	
		      	$file = $dir . $file_name;
				if (is_file($file)&&strpos($file,$project_folder) !== false) {
					unlink($file);
				}

		      move_uploaded_file($this->request->files['file']['tmp_name'], $file);

		      if(file_exists($file)){
			 	 if (is_file(DIR_MEDIA .$template_info['file_path'])) {
					unlink(DIR_MEDIA .$template_info['file_path']);
				}
		      	$file_path = 'files/'. $this->config->get('config_project_folder') .'/excel_template/'.$file_name;
		      	$this->model_excel_template->updateFilepath($template_id, $file_path);
		      	$json['filename'] = $file_path;
		      	$json['success'] = $this->language->get('text_success');
		      }
				} else {
					$json['error'] = $this->language->get('text_error_filetype');
				}
			}
		} else {
			$json['error'] = $this->language->get('text_error_upload');
		}

		$this->response->addHeader('Content-Type: application/json');

		$this->response->setOutput(json_encode($json));
	}
}