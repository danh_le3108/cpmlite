<?php

class ControllerExcelPlanReport extends Controller {
	private $import_id = 3;
	private $export_id = 3;
	private $report_id = 5;
	private $qc_report_id = 4;
	private $no_filter_key = array('_url','route');
	private $round = array();
	private $filter_key = array(
		'filter_round_name',
		'filter_global',
		'filter_user_id',
		'filter_province_id',
		'filter_plan_status',
		'filter_plan_qc',
		'filter_region_code',
		'filter_rating_status',
		'filter_date',
		'filter_is_fix'
	);
	private $plans_key =  array(
						'A' => '',
						'B' => '',
						'C' => '',
						'D' => '',
						'E' => '',
						'F' => '',
						'G' => '',
						'H' => '',
						'I' => '',
						'J' => '',
						'K' => '',
						'L' => '',
						'M' => '',
						'N' => '',
						'O' => '',
						'P' => '',
						'Q' => '',
						'R' => '',
						'S' => '',
						'T' => '',
						'U' => '',
						'V' => '',
						'W' => '',
						'X' => '',
						'Y' => '',
						'Z' => '',
					
					);
	 private $survey_key = array(
                'G' => array('survey_id' => 2,'label' => 'BLUE-HCM'),//#2
                'H' => array('survey_id' => 7,'label' => 'SILVER-HCM'),//#7
				
                'I' => array('survey_id' => 3,'label' => 'BLUE-NAM'),//#3
                'J' => array('survey_id' => 4,'label' => 'DIAMOND-NAM'),//#4
                'K' => array('survey_id' => 5,'label' => 'GOLD-NAM'),//#5
                'L' => array('survey_id' => 6,'label' => 'SILVER-NAM'),//#6
				
                'M' => array('survey_id' => 8,'label' => 'BLUE-BAC'),//#8
                'N' => array('survey_id' => 9,'label' => 'DIAMOND-BAC'),//#9
                'O' => array('survey_id' => 10,'label' => 'GOLD-BAC'),//#10
                'P' => array('survey_id' => 11,'label' => 'SILVER-BAC'),//#11
                'Q' => array('survey_id' => 12,'label' => 'ADHOC-RealMadrid'),//#12
                'R' => array('survey_id' => 13,'label' => 'ADHOC-Shampoo'),//#13
                'S' => array('survey_id' => 14,'label' => 'ADHOC-CounterTopVisage'),//#14
				'T' => array('survey_id' => 15,'label' => 'Cool Powder'),//#15
				'U' => array('survey_id' => 16,'label' => 'SENSE'),//#16
		);
			
	private $coupon_key = array(
					1  => array('coupon_prefix' => 'W', 'ignore' => 'X'), 
					2  => array('coupon_prefix' => 'Y', 'ignore' => 'Z'), 
					3  => array('coupon_prefix' => 'AA', 'ignore' => 'AB'), 
					4  => array('coupon_prefix' => 'AC', 'ignore' => 'AD'),
					5  => array('coupon_prefix' => 'AE', 'ignore' => 'AF'),
					6  => array('coupon_prefix' => 'AG', 'ignore' => 'AH'), 
					7  => array('coupon_prefix' => 'AI', 'ignore' => 'AJ'), 
					8  => array('coupon_prefix' => 'AK', 'ignore' => 'AL'), 
					9  => array('coupon_prefix' => 'AM', 'ignore' => 'AN'), 
					10 => array('coupon_prefix' => 'AO', 'ignore' => 'AP'), 
					11 => array('coupon_prefix' => 'AQ', 'ignore' => 'AR'), 
					12 => array('coupon_prefix' => 'AS', 'ignore' => 'AT'), 
	    );
			
	private function _url(){
		$url = '';
		foreach ($this->request->get as $key => $value) {
			if (!in_array($key, $this->no_filter_key)) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($value, ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}
	/*Excel*/ 
  
	
	private function getLastRound($round_name){
		
				$explode = explode('-',$round_name);
				$curr_year= (int)$explode[0];
				$curr_month = (int)end($explode);
				if($curr_month==1){
					$last_month = 12;
					$last_year = $curr_year-1;
				}else{
					$last_month = $curr_month-1;
					$last_year = $curr_year;	
				}
				$last_round = $last_year.'-'.sprintf('%02d',$last_month);
				
				
		return $last_round;
	}
	
	public function index() {
		
		if ($this->document->hasPermission('access', 'excel/plan_report')) {
		
			$this->load->language('plan/plan');
			$this->load->model('plan/plan');
			
			$filter_data = array();
			foreach($this->filter_key as $key){
				if (isset($this->request->get[$key])) {
					$filter_data[$key] = $this->request->get[$key];
				}
			}
			if($this->user->isLogged()&&$this->user->getGroupId()==$this->config->get('config_user_sup')){
				$filter_data['filter_user_ids'] = $this->user->user_ids();
			}elseif($this->user->isLogged()&&$this->user->getGroupId()!=$this->config->get('config_user_sup')){
				$filter_data['filter_user_ids'] = $this->user->users_manager();
			}
	
	
			if(isset($this->request->get['filter_plan_status'])){
				$filter_data['filter_plan_status'] = $this->request->get['filter_plan_status'];
			} else {
				//$filter_data['filter_plan_status'] = 1;
			}
	
			$template_id = $this->report_id;//5
			
			
					/**/
					$starttime = explode(' ',microtime());
					$start_time =  $starttime['1'] + $starttime['0'];
					$results = $this->model_plan_plan->getPlans($filter_data);
			
					$endtime                     = microtime();
					$endtime                     = explode(' ', $endtime);
					$endtime                     = $endtime['1'] + $endtime['0'];
				$loadingtime = number_format($endtime - $start_time, 4);
				
			$this->log->write('User: #'.$this->user->isLogged().' #'.$this->user->getUserName().' GetPlans: '.$loadingtime); 
			
		
			//Lý do KTC
			$this->load->model('localisation/reason_unsuccess');
			$reason_unsuccess = $this->model_localisation_reason_unsuccess->getReasons(1);
			
			
			//Lý do Không đạt
			$this->load->model('localisation/reason_not_pass');
			$reason_not_pass = $this->model_localisation_reason_not_pass->getReasons(1);
	
			$config_reward_audit = $this->config->get('config_reward_audit');
			$config_reward_sales = $this->config->get('config_reward_sales');
			$config_reward_old = $this->config->get('config_reward_old');
			
			//User group id
			$pa_id = $this->config->get('config_user_pa');
			$dc_id = $this->config->get('config_user_dc');
			$sup_id = $this->config->get('config_user_sup');
			$pl_id = $this->config->get('config_user_pl');
			$qc_id = $this->config->get('config_user_qc');
			
			$coupons = $this->getThisPlanCoupons($filter_data);
			
			//Tất cả câu trả lời
			$survey_answer = $this->getThisSurveys($filter_data);
			
			$survey_audit = $this->config->get('config_survey_audit');
			$survey_adhoc = $this->config->get('config_survey_adhoc');
			
			$rating_result = array(
				1=>'Đạt',
				0=>'',
				-1=>'Không đạt',
				-2=>'KTC',
			);
			
			$this->load->model('survey/survey');
			$surveys = $this->model_survey_survey->getSurveyIndexBy('survey_id');
			
			$plan_surveys = $this->model_plan_plan->getPlansSurveys($filter_data);
			
			$s_arr = array(2,3,4,5,6,7,8,9,10,11);
			
			$adhoc_arr = array(12,13,14,15);
			
			$qc_users = $this->getAllUserQc($filter_data);
			
			$this->load->model('project/project_user');
			$user = $this->model_project_project_user->getProjectUsers();
		
			$qc_note = $this->getQcNote($filter_data);
		
			$i=1;
			
			$plans = array();
				
			foreach($results as $plan){
					$plan_id = $plan['plan_id'];
					$answer = isset($survey_answer[$plan_id])?$survey_answer[$plan_id]:array();
					
					$plan_survey = isset($plan_surveys[$plan_id])?$plan_surveys[$plan_id]:array();
					$plan_reward_audit = isset($coupons[$plan_id][$config_reward_audit])?$coupons[$plan_id][$config_reward_audit]:array();
					$plan_reward_old = isset($coupons[$plan_id][$config_reward_old])?$coupons[$plan_id][$config_reward_old]:array();
					
			
					$plan_reason = '';
					if(isset($reason_unsuccess[$plan['reason_id']])) {
						$plan_reason .= $reason_unsuccess[$plan['reason_id']]['reason_name'];
						if(!empty($plan['note_ktc'])){
							$plan_reason .= ' - '.$plan['note_ktc'];
						}
					}
	
			
			
					$survey_audit_name = '';
					$plan_survey_audit = '';
					
					$audit_reason = '';
					
					$adhoc_result = array();
					$adhoc_reason = array();
					
					$survey_total=0;
					
					if($plan['reason_id']>0){
						$plan_survey_audit[] = $rating_result[-2];
					}
					//$reason_unsuccess KTC => ktc_reason_id
					//$reason_not_pass  Không đạt => not_pass_id
				
					foreach($plan_survey as $sid => $sdata){
						if(isset($sdata['survey_id'])){
							
							if(isset($survey_audit[$sdata['survey_id']])){
								$survey_audit_name = 	$surveys[$sdata['survey_id']]['survey_name'];
								if($plan['reason_id']>0){
									$plan_survey_audit = $rating_result[-2];
									$audit_reason = $plan_reason;
								}else{
									$plan_survey_audit = 	$rating_result[$sdata['rating_manual']];
									if($sdata['not_pass_id']>0){
										$audit_reason = $reason_not_pass[$sdata['not_pass_id']]['reason_name'].' - '. html_entity_decode($sdata['note']);
									}
									if($sdata['ktc_reason_id']>0){
										$audit_reason = $reason_unsuccess[$sdata['ktc_reason_id']]['reason_name'].' - '. html_entity_decode($sdata['note_ktc']);
									}
								}
							}
							if(isset($survey_adhoc[$sdata['survey_id']])){
									if($plan['reason_id']>0){
										$adhoc_result[$sdata['survey_id']] = $rating_result[-2];
										$adhoc_reason[$sdata['survey_id']] = $plan_reason;
									}else{
										$adhoc_result[$sdata['survey_id']] = $rating_result[$sdata['rating_manual']];
										
										if($sdata['not_pass_id']>0){
											$not_pass_note = $reason_not_pass[$sdata['not_pass_id']]['reason_name'];
											if(!empty($sdata['note'])){
												$not_pass_note .= ' - '. html_entity_decode($sdata['note']);
											}
											$adhoc_reason[$sdata['survey_id']] = $not_pass_note;
										}
										if($sdata['ktc_reason_id']>0){
											$ktc_reason_note = $reason_unsuccess[$sdata['ktc_reason_id']]['reason_name'];
											if(!empty($sdata['note_ktc'])){
												$ktc_reason_note .= ' - '. html_entity_decode($sdata['note_ktc']);
											}
											$adhoc_reason[$sdata['survey_id']] = $ktc_reason_note;
										}
									
									}
							}
							if(isset($s_arr[$sid])){
								$survey_total+=	$sdata['survey_total'];
							}
						}
					}
					
					
					$last_round = $this->getLastRound($plan['round_name']);
					
	
					// 7- Note POSM
					$note_posm = array();
					if(isset($qc_note[$plan_id]) && isset($qc_note[$plan_id][7])&&!empty($qc_note[$plan_id][7]['notes'])){
						$note_posm['comment'] = implode(' + ', $qc_note[$plan_id][7]['notes']);
						$note_posm['date_added'] = $qc_note[$plan_id][7]['date_added'];
					}
					
				$maplink = '=HYPERLINK("https://www.google.com/maps/search/'.$plan['latitude'].','.$plan['longitude'].'/@'.$plan['latitude'].','.$plan['longitude'].',17z", "'.$plan['latitude'].','.$plan['longitude'].'")';
					$plan_link = '=HYPERLINK("'.str_replace('&amp;', '&', $this->url->link('plan/plan/edit', 'plan_id='.$plan_id, true)).'", "Click here")';
					
					$trong_cho = isset($answer[1][10])?$answer[1][10]['answer_value']:'';//TRONG CHỢ
					$ngoai_cho = isset($answer[1][18])?$answer[1][18]['answer_value']:'';//NGOÀI CHỢ
					$round_month = datetimeConvert($plan['round_name'], 'm');
					
					$staff_note = array();
					
					if(!empty($plan['note'])){
						$staff_note[] =  html_entity_decode($plan['note']);
					}
					if(!empty($plan['note_ktc'])){
						$staff_note[] =  html_entity_decode($plan['note_ktc']);
					}
					$plans[] = array(
						'A' => $i,
						'B' => $plan['store_distributor'],//NPP
						'C' => $plan['distributor_code'],//CODE NPP
						'D' => $plan['store_customer_code'],//DEM CODE 
						'E' => $plan['store_ad_code'],//MÃ HỆ THỐNG NIVEA - AD CODE
						'F' => $plan['sale_name'],//SALE
						'G' => $plan['customer_user'],//SUP
						'H' => $plan['store_code'],//MÃ SỐ  TRƯNG BÀY CPM
						'I' => $plan['store_name'],//TÊN CỬA HÀNG
						'J' => $plan['store_address'],//SỐ NHÀ
						'K' => $plan['store_place'],//ĐỊA CHỈ
						'L' => $plan['store_ward'],//PHƯỜNG
						'M' => $plan['store_district'],//QUẬN
						'N' => $plan['store_province'],//TỈNH/ THÀNH PHỐ
						'O' => $plan['region_code'],//KHU VỰC
						'P' => !empty($survey_audit_name)?$survey_audit_name:'',// MỨC TB SKIN CARE ,//KIỂU CỬA HÀNG
						'Q' => $plan['store_phone'],//ĐiỆN THOẠI
						'R' => $plan['update_name'],//Update -TÊN CỬA HÀNG 
						'S' => $plan['update_address'],//Update -ĐỊA CHỈ
						'T' => $plan['update_phone'],//Update -ĐIỆN THOẠI
						'U' => $user[$plan['user_id']]['fullname'],//NHÂN VIÊN CHẤM ĐiỂM
						'V' => $plan['time_checkin'],//NGÀY CHẤM ĐiỂM
						'W' => $survey_total,//TỒNG SỐ MẶT
						'X' => isset($answer[1][26])?$answer[1][26]['answer_value']:'',//Có khay xanh
						'Y' => isset($answer[1][37])?$answer[1][37]['answer_value']:'',//Có counter top Serum Deo
						'Z' => isset($answer[1][45])?$answer[1][45]['answer_value']:'',//Hiện trạng TB counter top khi NV đến chấm điểm
						'AA' => isset($answer[1][34])?$answer[1][34]['answer_value']:'',//TẬP TRUNG/KHÔNG TẬP TRUNG
						'AB' => isset($answer[1][19])?$answer[1][19]['answer_value']:'',//Vị trí trưng bày ngang tầm mắt
						'AC' => isset($answer[1][27])?$answer[1][27]['answer_value']:'',// VỊ TRÍ  TRƯNG BÀY DO AUDITOR CẬP NHẬT 
						'AD' => $trong_cho.' - '.$ngoai_cho,// TRONG CHỢ/NGOÀI CHỢ
						'AE' => !empty($plan_survey_audit)?$plan_survey_audit:'',// KẾT QUẢ SKIN CARE
						'AF' => (isset($plan_reward_audit[$round_month])&&!empty($plan_reward_audit[$round_month]))?implode(',',$plan_reward_audit[$round_month]):'',// SỐ SERIES COUPON SKIN CARE $plan_reward_audit
						'AG' => isset($adhoc_result[12])?$adhoc_result[12]:'',//KẾT QUẢ ADHOC REAL MADRID #12
						'AH' => isset($adhoc_result[13])?$adhoc_result[13]:'',//KẾT QUẢ ADHOC SHAMPOO #13
						'AI' => isset($adhoc_result[14])?$adhoc_result[14]:'',//KẾT QUẢ ADHOC VISAGE #14
						'AJ' => isset($adhoc_result[15])?$adhoc_result[15]:'',//KẾT QUẢ ADHOC COOL POWDER #15
						'AK' => isset($adhoc_result[16])?$adhoc_result[16]:'',//KẾT QUẢ ADHOC SENSE #16
						'AL' => (isset($audit_reason)&&!empty($audit_reason))?$audit_reason:'',//SKIN CARE LÝ DO : RỚT/KTC
						'AM' => isset($adhoc_reason[12])?$adhoc_reason[12]:'',//REAL MADRID LÝ DO :RỚT/KTC #12
						'AN' => isset($adhoc_reason[13])?$adhoc_reason[13]:'',//SHAMPOO LÝ DO :RỚT/KTC #13
						'AO' => isset($adhoc_reason[14])?$adhoc_reason[14]:'',//VISAGE LÝ DO :RỚT/KTC #14
						'AP' => isset($adhoc_reason[15])?$adhoc_reason[15]:'',//COOL POWDER LÝ DO :RỚT/KTC #15
						'AQ' => isset($adhoc_reason[16])?$adhoc_reason[16]:'',//COOL POWDER LÝ DO :RỚT/KTC #16
						'AR' => !empty($staff_note)?implode(',',$staff_note):'',//NVCĐ NOTE 
						'AS' => html_entity_decode($plan['note_admin']),//ADMIN NOTE
						'AT' => isset($note_posm['comment']) ? html_entity_decode($note_posm['comment']): '',//NOTE POSM
						//'AT' => $plan['time_checkin'],//'AS' => $maplink,
						'AU' => $plan_link,
						'AV' => isset($qc_users[$plan_id][$dc_id]) ? $qc_users[$plan_id][$dc_id] : '',//DC
						'AW' => isset($qc_users[$plan_id][$pa_id]) ? $qc_users[$plan_id][$pa_id] : '',//DCH
						'AX' => isset($qc_users[$plan_id][$sup_id]) ? $qc_users[$plan_id][$sup_id] : '',//SUP
						'AY' => isset($qc_users[$plan_id][$qc_id]) ? $qc_users[$plan_id][$qc_id] : '',//QC
						'AZ' => isset($qc_users[$plan_id][$pl_id]) ? $qc_users[$plan_id][$pl_id] : '',//PL
						'BA' => (isset($plan_reward_old['01'])&&!empty($plan_reward_old['01']))?implode(',',$plan_reward_old['01']):'',//01
						'BB' => (isset($plan_reward_old['02'])&&!empty($plan_reward_old['02']))?implode(',',$plan_reward_old['02']):'',//2 
						'BC' => (isset($plan_reward_old['03'])&&!empty($plan_reward_old['03']))?implode(',',$plan_reward_old['03']):'',//3
						'BD' => (isset($plan_reward_old['04'])&&!empty($plan_reward_old['04']))?implode(',',$plan_reward_old['04']):'',//4
						'BE' => (isset($plan_reward_old['05'])&&!empty($plan_reward_old['05']))?implode(',',$plan_reward_old['05']):'',//5
						'BF' => (isset($plan_reward_old['06'])&&!empty($plan_reward_old['06']))?implode(',',$plan_reward_old['06']):'',//6
						'BG' => (isset($plan_reward_old['07'])&&!empty($plan_reward_old['07']))?implode(',',$plan_reward_old['07']):'',//7
						'BH' => (isset($plan_reward_old['08'])&&!empty($plan_reward_old['08']))?implode(',',$plan_reward_old['08']):'',//8
						'BI' => (isset($plan_reward_old['09'])&&!empty($plan_reward_old['09']))?implode(',',$plan_reward_old['09']):'',//9
						'BJ' => (isset($plan_reward_old['10'])&&!empty($plan_reward_old['10']))?implode(',',$plan_reward_old['10']):'',//10
						'BK' => (isset($plan_reward_old['11'])&&!empty($plan_reward_old['11']))?implode(',',$plan_reward_old['11']):'',//11
						'BL' => (isset($plan_reward_old['12'])&&!empty($plan_reward_old['12']))?implode(',',$plan_reward_old['12']):'',//12
					
					);
					if(isset($survey_total)){
						unset($survey_total);
					}
					
				$i++;
			}
				$endtime2                     = microtime();
					$endtime2                     = explode(' ', $endtime2);
					$endtime2                     = $endtime2['1'] + $endtime2['0'];
				$loadingtime2 = number_format($endtime2 - $start_time, 4); 
					
				$this->log->write('Handle: '.$loadingtime2); 
				
				
			if($this->user->getId()==1){		
				//$this->log->write($plans);  
			}
			$export_data = array(
				'excel_data' => $plans,
				'template_id' => $template_id,
				'template_name' => 'ReportPlan-by-'.$this->user->getUsername().'-at-'. date('Ymd-His').'.xlsx'
			);
			if(!empty($plans)){
				$file =  $this->excel->exportWithTemplate($export_data);
				$endtime3                     = microtime();
				$endtime3                     = explode(' ', $endtime3);
				$endtime3                     = $endtime3['1'] + $endtime3['0'];
				$loadingtime3 = number_format($endtime3 - $start_time, 4); 
				$this->log->write('Export: '.$loadingtime3); 	
	
				$this->response->setOutput(file_get_contents($file, FILE_USE_INCLUDE_PATH, null));
			}
			$url = $this->_url();
	
			$this->response->redirect($this->url->link('plan/plan',  $url, true));
		}else{
			echo 'Thiếu tham số yêu cầu!';	exit();	
		}
	}

	//report
  private function getAllUserQc($filter=array()){
		$this->load->model('project/project_user');
		$user = $this->model_project_project_user->getProjectUsers();
			
		$results = array();
		$sql = "SELECT * FROM " . PDB_PREFIX . "plan_confirm qc WHERE 1";
		
		if (isset($filter['filter_round'])&&!empty($filter['filter_round'])) {
			$sql .= " AND round_name = '" . $this->pdb->escape($filter['filter_round']) . "'";
		}
		
		$query = $this->pdb->query($sql);
		foreach($query->rows as $row){
			
			$results[$row['plan_id']][$row['user_group_id']] = isset($user[$row['user_id']])?$user[$row['user_id']]['username']:''.' - '.isset($user[$row['user_id']])?$user[$row['user_id']]['fullname']:'';
		}
		return $results;
  }
	//report
  private function getThisSurveys($filter=array()){
  	$sql = "SELECT * FROM `".PDB_PREFIX."plan_survey_data` WHERE 1";
	
	if (isset($filter['filter_round'])&&!empty($filter['filter_round'])) {
		$sql .= " AND round_name = '" . $this->pdb->escape($filter['filter_round']) . "'";
	}
	
  	$query = $this->pdb->query($sql);

  	$survey = array();
  	if($query->rows){
  		foreach($query->rows as $row){
  			$survey[$row['plan_id']][$row['survey_id']][$row['question_id']] = $row;
  		}
  	}

  	return $survey;
  }
	//report
  private function getThisPlanCoupons($filter=array()){
  	$sql = "SELECT plan_id,coupon_prefix,reward_type_id,coupon_code,yearmonth,coupon_status_id FROM ".PDB_PREFIX."plan_coupon_history WHERE `coupon_code`!=''";
	
	$allow_status = $this->config->get('config_coupon_has_paid');
		
	if (isset($filter['filter_round'])&&!empty($filter['filter_round'])) {
		$sql .= " AND round_name = '" . $this->pdb->escape($filter['filter_round']) . "'";
	}
	
  	$query = $this->pdb->query($sql);

  	$coupon = array();
  	if($query->rows){
  		foreach($query->rows as $row){//[$row['reward_type_id']]
  			if(!empty($row['coupon_code'])){
				$yearmonth = datetimeConvert($row['yearmonth'], 'm');
  				$coupon[$row['plan_id']][$row['reward_type_id']][$yearmonth][] = ($row['coupon_status_id']==$allow_status)?$row['coupon_code']:$row['coupon_prefix'];
  			}
  		}
  	}

  	return $coupon;
  }
	//report + qc_report
	private function getQcNote($filter=array()){
		$sql = "SELECT * FROM `".PDB_PREFIX."plan_note` WHERE is_deleted = '0' ";
	
		if (isset($filter['filter_round'])&&!empty($filter['filter_round'])) {
			$sql .= " AND round_name = '" . $this->pdb->escape($filter['filter_round']) . "'";
		}
			$sql .= " ORDER BY date_added ASC";
	
		$query = $this->pdb->query($sql);

		$plan_note = array();
		if($query->rows){
			foreach($query->rows as $row){
				$plan_note[$row['plan_id']][$row['note_id']]['date_added'] = $row['date_added'];
				$plan_note[$row['plan_id']][$row['note_id']]['notes'][] = html_entity_decode($row['comment']);
			}
		}
		return $plan_note;
	}

	//qc_report
  

	// public function qc_report(){
	// 	if (empty($this->request->get['filter_round_name'])) {
	// 		$this->session->data['error_warning'] = "Vui lòng chọn tháng!";
	// 		$this->response->redirect($this->url->link('plan/plan'));
	// 		exit();
	// 	}
	// 	if ($this->document->hasPermission('add','excel/plan_report')) {

	// 		$this->load->language('plan/plan');
	// 		$this->load->model('plan/plan');
	// 		$this->load->model('excel/handle');

	// 		$filter_data = array(
	// 			'not_user_id' => '3519'
	// 		);
	// 		foreach($this->filter_key as $key){
	// 			if (isset($this->request->get[$key])) {
	// 				$filter_data[$key] = $this->request->get[$key];
	// 			}
	// 		}
	// 		//print_r('<pre>'); print_r($setting); print_r('</pre>'); die();

	// 		$this->load->model('project/project_user');
	// 		$users = $this->model_project_project_user->getProjectUsers();
			
	// 		$this->load->model('qc/note');
	// 		$note_type = $this->model_qc_note->getNoteTypes();

	// 		$qc_confirm = $this->getQcConfirm($filter_data);
	// 		$qc_note = $this->getQcNote($filter_data);
			
	// 		$rating_result = array(
	// 			1=>'Thành công',
	// 			-2=>'KTC'
	// 		);
			
			
	// 		$qc_code = $this->getCodes($filter_data);

	// 		$pa_id = $this->config->get('config_user_pa');
	// 		$dc_id = $this->config->get('config_user_dc');
	// 		$sup_id = $this->config->get('config_user_sup');
	// 		$pl_id = $this->config->get('config_user_pl');

	// 		// $survey_audit = $this->config->get('config_survey_audit');
				
	// 		$this->load->model('catalog/attribute');
	// 		$posm_attrs = $this->model_catalog_attribute->getAttributesByGroupId(1);
	// 		unset($posm_attrs[0]);
	// 		unset($posm_attrs[1]);
	// 		$this->load->model('catalog/attribute_data');
	// 		$plan_data = $this->model_catalog_attribute_data->getAttrsOfPlan(array('round_name'=>$filter_data['filter_round_name']),false);
	// 		$plandata = array();
	// 		foreach ($plan_data as $value) {
	// 			$val = $value['value'];
	// 			if ($value['input'] == 'select') {
	// 				$options = explode('|', $value['options']);
	// 				$val -= 1;
	// 				$val = isset($options[$val]) ? $options[$val] : '';
	// 			}
	// 			$plandata[$value['plan_id']][$value['attribute_id']] = $val;
	// 		}
	// 		$this->load->model('localisation/reason_unsuccess');
	// 		$reason_list = $this->model_localisation_reason_unsuccess->getReasons();
	// 		$reasons = array();
	// 		foreach ($reason_list as $value) {
	// 			$reasons[$value['reason_id']] = $value['reason_name'];
	// 		}

	// 		$i = 1;
	// 		/*Template Info */ 
	// 		$template_id = $this->qc_report_id;
	// 		$template_info = $this->excel->getTemplate($template_id);
	// 		$setting = $template_info['settings'];
	// 		/*Template Info */ 
			
	// 		$row = $setting['start_row'];
	// 		$plans = array();
			
	// 		// $plan_surveys = $this->model_plan_plan->getPlansSurveys($filter_data);
				
	// 		$results = $this->model_plan_plan->getPlans($filter_data);

	// 		foreach($results as $result){
	// 			$plan_id = $result['plan_id']; 
	// 			$so_ngay_tre = floor(abs(strtotime($result['time_upload']) - strtotime($result['time_checkin'])) / (60 * 60 * 24));
	// 			$date_checkin = $result['time_checkin']!='0000-00-00 00:00:00' ? datetimeConvert($result['time_checkin'], 'd-m-Y') : '';
	// 			$date_upload = $result['time_upload']!='0000-00-00 00:00:00' ? datetimeConvert($result['time_upload'], 'd-m-Y') : '';

	// 			$gio_checkin = $result['time_checkin']!='0000-00-00 00:00:00' ? datetimeConvert($result['time_checkin'], 'H:i:s') : '';
	// 			$gio_upload = $result['time_upload']!='0000-00-00 00:00:00' ? datetimeConvert($result['time_upload'], 'H:i:s') : '';

	// 			$thoi_gian_tre = array();
	// 			if(!empty($gio_checkin) && !empty($gio_upload)){
	// 				$format_time_checkin	= date_parse_from_format('H:i:s', $gio_checkin);
	// 				$format_time_upload	= date_parse_from_format('H:i:s', $gio_upload);
	// 				$thoi_gian_tre['gio_tre'] = str_replace('-', '', $format_time_upload['hour'] - $format_time_checkin['hour']);
	// 				$thoi_gian_tre['phut_tre'] = str_replace('-', '', $format_time_upload['minute'] - $format_time_checkin['minute']);
	// 				$thoi_gian_tre['giay_tre'] = str_replace('-', '', $format_time_upload['second'] - $format_time_checkin['second']);
	// 			}

	// 			$pa = $dc = $sup = $pl = $dc_date_stick  = $sup_date_stick ='';
	// 			if(isset($qc_confirm[$plan_id])){
	// 				if(isset($qc_confirm[$plan_id][$pa_id]) && isset($users[$qc_confirm[$plan_id][$pa_id]['user_id']])){
	// 					$pa = $users[$qc_confirm[$plan_id][$pa_id]['user_id']]['username'] . ' - '.$users[$qc_confirm[$plan_id][$pa_id]['user_id']]['fullname'];
	// 				}
	// 				if(isset($qc_confirm[$plan_id][$dc_id])&&isset($users[$qc_confirm[$plan_id][$dc_id]['user_id']])){
	// 					$dc = $users[$qc_confirm[$plan_id][$dc_id]['user_id']]['username']. ' - '.$users[$qc_confirm[$plan_id][$dc_id]['user_id']]['fullname'];
	// 					$dc_date_stick = $qc_confirm[$plan_id][$dc_id]['date_added'];
	// 				}

	// 				if(isset($qc_confirm[$plan_id][$sup_id])&&isset($users[$qc_confirm[$plan_id][$sup_id]['user_id']])){
	// 					$sup = $users[$qc_confirm[$plan_id][$sup_id]['user_id']]['username'] . ' - '.$users[$qc_confirm[$plan_id][$sup_id]['user_id']]['fullname'];
	// 					$sup_date_stick = $qc_confirm[$plan_id][$sup_id]['date_added'];
	// 				}

	// 				if(isset($qc_confirm[$plan_id][$pl_id])&&isset($users[$qc_confirm[$plan_id][$pl_id]['user_id']])){
	// 					$pl = $users[$qc_confirm[$plan_id][$pl_id]['user_id']]['username']. ' - '.$users[$qc_confirm[$plan_id][$pl_id]['user_id']]['fullname'];
	// 				}
	// 			}

	// 			// QC Note
	// 			// 1 - Note cho khac phuc
	// 			$note_cho_khac_phuc = array();
	// 			if(isset($qc_note[$plan_id]) && isset($qc_note[$plan_id][1])&&!empty($qc_note[$plan_id][1]['notes'])){
	// 				$note_cho_khac_phuc['comment'] = implode(' + ', $qc_note[$plan_id][1]['notes']);
	// 				$note_cho_khac_phuc['date_added'] = $qc_note[$plan_id][1]['date_added'];
	// 			}

	// 			// 2- Da khac phuc
	// 			$note_da_khac_phuc = array();
	// 			if(isset($qc_note[$plan_id]) && isset($qc_note[$plan_id][2])&&!empty($qc_note[$plan_id][2]['notes'])){
	// 				$note_da_khac_phuc['comment'] = implode(' + ', $qc_note[$plan_id][2]['notes']);
	// 				$note_da_khac_phuc['date_added'] = $qc_note[$plan_id][2]['date_added'];
	// 			}

	// 			// 3 - Data sai
	// 			$note_data_sai = array();
	// 			if(isset($qc_note[$plan_id]) && isset($qc_note[$plan_id][3])&&!empty($qc_note[$plan_id][3]['notes'])){
	// 				$note_data_sai['comment'] = implode(' + ', $qc_note[$plan_id][3]['notes']);
	// 				$note_data_sai['date_added'] = $qc_note[$plan_id][3]['date_added'];
	// 			}

	// 			// 4 - Note sup
	// 			$note_sup = array();
	// 			if(isset($qc_note[$plan_id]) && isset($qc_note[$plan_id][4])&&!empty($qc_note[$plan_id][4]['notes'])){
	// 				$note_sup['comment'] = implode(' + ', $qc_note[$plan_id][4]['notes']);
	// 				$note_sup['date_added'] = $qc_note[$plan_id][4]['date_added'];
	// 			}
				
	// 			// 5- Note Khac
	// 			$note_khac = array();
	// 			if(isset($qc_note[$plan_id]) && isset($qc_note[$plan_id][5])&&!empty($qc_note[$plan_id][5]['notes'])){
	// 				$note_khac['comment'] = implode(' + ', $qc_note[$plan_id][5]['notes']);
	// 				$note_khac['date_added'] = $qc_note[$plan_id][5]['date_added'];
	// 			}
	// 			// 6 - Note Admin
	// 			$note_admin = array();
	// 			if(isset($qc_note[$plan_id]) && isset($qc_note[$plan_id][6])&&!empty($qc_note[$plan_id][6]['notes'])){
	// 				$note_admin['comment'] = implode(' + ', $qc_note[$plan_id][6]['notes']);
	// 				$note_admin['date_added'] = $qc_note[$plan_id][6]['date_added'];
	// 			}
	// 			// 7- Note POSM
	// 			$note_posm = array();
	// 			if(isset($qc_note[$plan_id]) && isset($qc_note[$plan_id][7])&&!empty($qc_note[$plan_id][7]['notes'])){
	// 				$note_posm['comment'] = implode(' + ', $qc_note[$plan_id][7]['notes']);
	// 				$note_posm['date_added'] = $qc_note[$plan_id][7]['date_added'];
	// 			}
	// /*
	// 			if($result['plan_status']==1&&$result['plan_rating']==1){
	// 				$plan_rating_text = $this->language->get('text_rating_success');
	// 				//$this->language->get('text_rating_passed');
	// 			}elseif($result['plan_status']==1&&$result['plan_rating']==-1){
	// 				$plan_rating_text = $this->language->get('text_rating_not_passed');
	// 			}elseif($result['plan_status']==1&&$result['plan_rating']==-2){
	// 				$plan_rating_text = $this->language->get('text_rating_unsuccess');
	// 			}else{
	// 				$plan_rating_text = '';
	// 			}*/ 

	// 			$maplink = '=HYPERLINK("https://www.google.com/maps/search/'.$result['latitude'].','.$result['longitude'].'/@'.$result['latitude'].','.$result['longitude'].',17z", "'.$result['latitude'].','.$result['longitude'].'")';
	// 			$plan_link = '=HYPERLINK("'.str_replace('&amp;', '&', $this->url->link('plan/plan/edit', 'plan_id='.$plan_id, true)).'", "Vào web")';
	// 			$user_sup_id = 	isset($users[$result['user_id']]['user_parent_id']) ? $users[$result['user_id']]['user_parent_id'] : '';
	// 			$p = array(
	// 				/* Store info */
	// 				'A'=>$result['store_code'], // Ma CH CPM
	// 				'B'=>$result['store_name'],
	// 				'C'=>$result['store_address'],
	// 				'D'=>$result['store_place'],
	// 				'E'=>$result['store_ward'],
	// 				'F'=>$result['store_district'],
	// 				'G'=>$result['store_province'],
	// 				'H'=>$result['store_phone'],
	// 				'I'=>isset($users[$user_sup_id]['parent_fullname']) ? $users[$user_sup_id]['parent_fullname'] : '',// name sup
	// 				'J'=>isset($users[$result['user_id']]) ? $users[$result['user_id']]['usercode'] : '',
	// 				'K'=>isset($users[$result['user_id']]) ? $users[$result['user_id']]['fullname'] : '',
	// 				'L'=>$maplink,//$result['latitude'].','.$result['longitude'], // GPS
	// 				'M'=>$date_checkin, // checkin
	// 				'N'=>$gio_checkin,
	// 				'O'=>$date_upload, // upload
	// 				'P'=>$gio_upload,
	// 				'Q'=>$so_ngay_tre,
	// 				'R'=>$thoi_gian_tre ? implode(':', $thoi_gian_tre) : '',
	// 				'S'=>$pa,
	// 				'T'=>$dc,
	// 				'U'=>$sup,
	// 				'V'=>$pl,
	// 				'W'=>($result['is_fix']>0&&isset($note_cho_khac_phuc['date_added'])) ? 1 : '',
	// 				'X'=>isset($note_cho_khac_phuc['date_added'])?$note_cho_khac_phuc['date_added']:'',//Ngày đề nghị KP,
	// 				'Y'=>($result['is_fix']>1&&$result['date_fixed']!='0000-00-00 00:00:00') ? $result['date_fixed'] : '',
	// 				'Z'=>($result['is_fix']==3)?1:'',
	// 				'AA'=>'=IF(AB'.$row.'>0,AB'.$row.'-BP'.$row.',AB'.$row.')',//Lỗi Sup
	// 				'AB'=>'=IF(SUM(AC'.$row.':AH'.$row.')>0,1,0)', // Lỗi Auditor
	// 				'AC'=>'=IF(SUM(AJ'.$row.':AM'.$row.')>0,1,0)', //Code 1: Nội Qui
	// 				'AD'=>'=IF(SUM(AN'.$row.':AW'.$row.')>0,1,0)',//Code 2: Chất lượng hình ảnh
	// 				'AE'=>'=IF(SUM(AX'.$row.':BC'.$row.')>0,1,0)',//Code 3: Thông tin và  số liệu sai 
	// 				'AF'=>'=IF(SUM(BD'.$row.':BE'.$row.')>0,1,0)',//Code 4: Kiến thức sản phẩm/chương trình
	// 				'AG'=>'=BF'.$row.'',//Code 5: Lỗi trưng bày
	// 				'AH'=>'=IF(SUM(BG'.$row.':BM'.$row.')>0,1,0)',//Code 6: Nghi ngờ cheating Hình ảnh
	// 				'AI'=>'=BN'.$row.'', //Code7: Khác
	// 				//Code 1 AJ->AM, codes: 1,22,23,39
	// 				'AJ'=>isset($qc_code[$plan_id][1]) ? 1 : '0', // 1 - Code 1.1- Selfie không theo qui định
	// 				'AK'=>isset($qc_code[$plan_id][22]) ? 1 : '0', // 22 - Code 1.2 Upload trễ
	// 				'AL'=>isset($qc_code[$plan_id][23]) ? 1 : '0', // 23 - Code 1.3 Chấm ngoài giờ quy định
	// 				'AM'=>isset($qc_code[$plan_id][39]) ? 1 : '0',
	// 				//code 2 AN->AW, codes: 2,3,5,9,26,27,28,31,33,35
	// 				'AN'=>isset($qc_code[$plan_id][2]) ? 1 : '0', 
	// 				'AO'=>isset($qc_code[$plan_id][3]) ? 1 : '0',
	// 				'AP'=>isset($qc_code[$plan_id][5]) ? 1 : '0',
	// 				'AQ'=>isset($qc_code[$plan_id][9]) ? 1 : '0', 
	// 				'AR'=>isset($qc_code[$plan_id][26]) ? 1 : '0',
	// 				'AS'=>isset($qc_code[$plan_id][27]) ? 1 : '0',
	// 				'AT'=>isset($qc_code[$plan_id][28]) ? 1 : '0', 
	// 				'AU'=>isset($qc_code[$plan_id][31]) ? 1 : '0',
	// 				'AV'=>isset($qc_code[$plan_id][33]) ? 1 : '0',
	// 				'AW'=>isset($qc_code[$plan_id][35]) ? 1 : '0',
	// 				//code 3 AX->BC, codes: 10,24,29,30,38,44
	// 				'AX'=>isset($qc_code[$plan_id][10]) ? 1 : '0', 
	// 				'AY'=>isset($qc_code[$plan_id][24]) ? 1 : '0', 
	// 				'AZ'=>isset($qc_code[$plan_id][29]) ? 1 : '0', 
	// 				'BA'=>isset($qc_code[$plan_id][30]) ? 1 : '0', 
	// 				'BB'=>isset($qc_code[$plan_id][38]) ? 1 : '0', 
	// 				'BC'=>isset($qc_code[$plan_id][44]) ? 1 : '0', 
	// 				//code 4 BD->BE, codes: 11,25
	// 				'BD'=>isset($qc_code[$plan_id][11]) ? 1 : '0', 
	// 				'BE'=>isset($qc_code[$plan_id][25]) ? 1 : '0', 
	// 				//code 5 BF, codes: 13
	// 				'BF'=>isset($qc_code[$plan_id][13]) ? 1 : '0', 
	// 				//code 6 BG->BM, codes: 14,15,16,17,18,19,20
	// 				'BG'=>isset($qc_code[$plan_id][14]) ? 1 : '0', 
	// 				'BH'=>isset($qc_code[$plan_id][15]) ? 1 : '0',
	// 				'BI'=>isset($qc_code[$plan_id][16]) ? 1 : '0',
	// 				'BJ'=>isset($qc_code[$plan_id][17]) ? 1 : '0',
	// 				'BK'=>isset($qc_code[$plan_id][18]) ? 1 : '0',
	// 				'BL'=>isset($qc_code[$plan_id][19]) ? 1 : '0',
	// 				'BM'=>isset($qc_code[$plan_id][20]) ? 1 : '0',
	// 				//code 7 BK, codes: 21
	// 				'BN'=>isset($qc_code[$plan_id][21]) ? 1 : '0',
	// 				//code 8 BD, codes: 34
	// 				'BO'=>isset($qc_code[$plan_id][34]) ? 1 : '0',

	// 				'BP'=>!empty($sup) ? 1 : 0,// Stick sup
	// 				'BQ'=>isset($note_data_sai['comment']) ? html_entity_decode($note_data_sai['comment']): '',
					
	// 				'BR'=>$dc_date_stick,
	// 				'BS'=>isset($note_sup['comment']) ? html_entity_decode($note_sup['comment']): '',
	// 				'BT'=>isset($note_admin['comment']) ? html_entity_decode($note_admin['comment']): '',
	// 				// 'BK'=>isset($note_khac['comment']) ? html_entity_decode($note_khac['comment']): '',
	// 				'BU'=>isset($note_cho_khac_phuc['comment']) ? html_entity_decode($note_cho_khac_phuc['comment']): '',
	// 				// 'BL'=>isset($note_khac['comment']) ? html_entity_decode($note_khac['comment']): '',
	// 				// 'BM'=>isset($note_admin['comment']) ? html_entity_decode($note_admin['comment']): '',
	// 				// 'BN'=>isset($note_khac['comment']) ? html_entity_decode($note_khac['comment']): '',
	// 				// 'BO'=>isset($note_posm['comment']) ? html_entity_decode($note_posm['comment']): '',
	// 				// 'BP'=>$result['model'],
	// 				// 'BQ'=>!empty($plan_survey_audit)?implode('|',$plan_survey_audit):'',// KẾT QUẢ SKIN CARE
	// 				// 'BR'=>$plan_link,
	// 				'BV' => $result['posm_1'],
	// 				'BW' => $result['posm_2'],
	// 				'BX' => $result['posm_3'],
	// 				'BY' => $result['posm_4'],
	// 				'BZ' => $result['posm_5'],

	// 			);
	// 			foreach ($posm_attrs as $attr) {
	// 				$p[] = isset($plandata[$result['plan_id']][$attr['attribute_id']]) ? $plandata[$result['plan_id']][$attr['attribute_id']] : '0';
	// 			}

	// 			$rating = '';
	// 			if ($result['plan_rating'] == 1) {
	// 				$rating = 'Thành công';
	// 			} elseif ($result['plan_rating'] == -2) {
	// 				$rating = 'Không thành công';
	// 			}
	// 			$p[] = $rating;
	// 			$p[] = isset($reasons[$result['reason_id']]) ? $reasons[$result['reason_id']] : '';
	// 			$p[] = $result['note_ktc'];
	// 			$p[] = $result['note'];
	// 			$plans[] = $p;
	// 			$i++;
	// 			$row++;
	// 		}
	// 		if(!empty($plans)){
	// 			$export_data = array(
	// 				'excel_data' => $plans,
	// 				'template_id' => $template_id,
	// 				'template_name' => 'Qc-Report-plan-by-'.$this->user->getUsername().'-at-'. date('Ymd-His').'.xlsx'
	// 			);
	// 			$file = $this->excel->exportWithTemplate($export_data);
	// 		} else {
	// 			$this->session->data['error_warning'] = "Không có dữ liệu!";
	// 			$this->response->redirect($this->url->link('plan/plan',  $this->_url(), true));
	// 		}
	// 	}
	// }

	private function getQcConfirm($filter = array()){
		
			$sql = "SELECT * FROM " . PDB_PREFIX . "plan_confirm WHERE 1";

		if (isset($filter['filter_round'])&&!empty($filter['filter_round'])) {
			$sql .= " AND round_name = '" . $this->pdb->escape($filter['filter_round']) . "'";
		}
			$query = $this->pdb->query($sql);
			

		$qc_confirm = array();
		if($query->rows){
			foreach($query->rows as $row){
				$qc_confirm[$row['plan_id']][$row['user_group_id']] = $row;
			}
		}
		return $qc_confirm;
	}

	private function getCodes($filter = array()) {
			$sql = "SELECT * FROM " . PDB_PREFIX . "plan_code WHERE 1";

		if (isset($filter['filter_round'])&&!empty($filter['filter_round'])) {
			$sql .= " AND round_name = '" . $this->pdb->escape($filter['filter_round']) . "'";
		}
			$query = $this->pdb->query($sql);
			$codes = array();
			if($query->rows){
				foreach($query->rows as $row){
					$codes[$row['plan_id']][$row['problem_id']] = $row;
				}
			}

			return $codes;
	}
	public function getProblemsIndexBy($index_by = 'problem_id') {
		$query_data = array();
		$query = $this->db->query("SELECT * FROM " . PDB_PREFIX . "qc_problem");

		foreach ($query->rows as $row){
			$query_data[$row[$index_by]] = $row;
		}


		return $query_data;
	}

	public function getProblems() {
		$config_code = implode(',', $this->config->get('config_qc_code'));
		$sql = 'SELECT * FROM ' . DB_PREFIX . "qc_problem d WHERE problem_id IN ({$config_code}) ORDER BY d.code_id,d.problem_id,d.problem_name";
		// p($sql,1);
		$problems = $this->db->query($sql)->rows;
		return $problems;
	}
	public function qc_report() {
		if (empty($this->request->get['filter_round_name'])) {
			$this->session->data['error_warning'] = "Vui lòng chọn tháng!";
			$this->response->redirect($this->url->link('plan/plan'));
			exit();
		}
		if ($this->document->hasPermission('add','excel/plan_report')) {
			$this->load->model('plan/plan');
			$filter_data = $this->request->get;
			$filter_data['user_id !'] = 3519;
			// $filter_data['plan_id'] = 1109;
			$plans = $this->model_plan_plan->getPlans($filter_data);
			if (!empty($plans)) {
				/*Template Info */ 
				$template_id = $this->qc_report_id;
				$template_info = $this->excel->getTemplate($template_id);
				$setting = $template_info['settings'];
				/*Template Info */ 
				
				$row = $setting['start_row'];
				
				// $plan_surveys = $this->model_plan_plan->getPlansSurveys($filter_data);
				
				set_time_limit(1800);
				$template_id = $this->qc_report_id;
				$template_info = $this->excel->getTemplate($template_id,0);
				$template_file = DIR_MEDIA.$template_info['file_path'];
				$settings = $template_info['settings'];
				if(!file_exists($template_file)){
					return;
				}
				$template_name = 'Qc_report_'.$this->request->get['filter_round_name'].'-by-'.$this->user->getUsername().'-at-'. date('Ymd-His').'.xlsx';
				$file_name = ucwords(str_replace(' ', '-', $template_name));
				$folder = isset($data['folder']) ? $data['folder'] .'/' : '';
				$template_path = DIR_MEDIA.'files/'. $this->config->get('config_project_folder') .'/export/'.$folder;
				if(!is_dir($template_path)){
					@mkdir($template_path, 0777, true);
				}

				$export_file = $template_path.$file_name;
				if(file_exists($export_file)){
					@unlink($export_file);
				}
				if(copy($template_file, $export_file)) {
					try {
						$inputFileType = PHPExcel_IOFactory::identify($export_file);
						$objReader = PHPExcel_IOFactory::createReader($inputFileType);
						$workbook = $objReader->load($export_file);
						$worksheet  = $workbook->setActiveSheetIndex($settings['sheet_index']);
						
						$row = isset($settings['start_row']) ? $settings['start_row'] : 2;
						$pa_group_id = $this->config->get('config_user_pa');
						$dc_group_id = $this->config->get('config_user_dc');
						$sup_group_id = $this->config->get('config_user_sup');
						$fl_group_id = $this->config->get('config_user_fl');

						$pl_group_id = $this->config->get('config_user_pl');
						$qc_confirms = $this->getQcConfirm();
						$qc_notes = $this->getQcNote();
						// $qc_problems = $this->getProblems();
						$qc_problems = array(
							1,22,
							2,3,5,9,31,35,54,
							10,24,44,52,
							14,15,16,17,18,19,20,
							21,46, 	
							34
						);
						$qc_code = $this->getCodes();
						$rating = array(0=>'',1=>'Thành công',-2=>'Không thành công');

						$this->load->model('project/project_user');
						$users = $this->model_project_project_user->getProjectUsers();
						
						$this->load->model('localisation/reason_unsuccess');
						$reason_list = $this->model_localisation_reason_unsuccess->getReasons();
						$reasons = array();
						foreach ($reason_list as $value) {
							$reasons[$value['reason_id']] = $value['reason_name'];
						}
						$attr_status = array(
							1 => 'Có',
							2 => 'Không'
						);
						$this->load->model('store/store_type');
						$store_types = $this->model_store_store_type->getAllTypes();
						$this->load->model('catalog/attribute');
						$this->load->model('catalog/attribute_data');
						$attrs = $this->model_catalog_attribute->getAttributes(array('sort'=>'g.sort_order,a.sort_order'));
						// p($attrs,1);
						$plan_data = $this->model_catalog_attribute_data->getAttrsOfPlan(array('round_name' => $this->request->get['filter_round_name']), false);
						$plandata = array();
						foreach ($plan_data as $value) {
							$val = $value['value'];
							$plandata[$value['plan_id']][$value['attribute_id']] = $value['value'];
						}
						// p($plans,1);
						foreach($plans as $p){
							$plan_id = $p['plan_id']; 
							//A -> H
							$data = array(
								$p['store_code'],
								$p['store_name'],
								$p['store_address'],
								$p['store_place'],
								$p['store_ward'],
								$p['store_district'],
								$p['store_province'],
								$p['store_phone']
							);

							//I -> L
							$sup_name = $staff_code = $staff_name = '';
							$staff_id = $p['user_id'];
							if (isset($users[$staff_id])) {
								$staff_code =  $users[$staff_id]['usercode'];
								$staff_name =  $users[$staff_id]['fullname'];
								$sup_id = $users[$staff_id]['user_parent_id'];
								$sup_name =  isset($users[$sup_id]) ? $users[$sup_id]['fullname'] : '';
							}
							$data[] = $sup_name; 
							$data[] = $staff_code;
							$data[] = $staff_name;
							$data[] = $p['latitude'].','.$p['longitude'];

							//M -> R
							$checkin = explode(' ',$p['time_checkin']);
							$upload = explode(' ',$p['time_upload']);
							$days_late = $time_late = '';
							if ($p['time_checkin'] != '0000-00-00 00:00:00' && $p['time_upload'] != '0000-00-00 00:00:00') {
								$start = new DateTime($p['time_checkin']);
								$end = new DateTime($p['time_upload']);
								$diff = $start->diff($end);
								$days_late = $diff->d;
								$time_late = $diff->h.':'.$diff->i.':'.$diff->s;
							}
							$data[] = $checkin[0];
							$data[] = $checkin[1];
							$data[] = $upload[0];
							$data[] = $upload[1];
							$data[] = $days_late;
							$data[] = $time_late;
							
							// S-> Z
							$data[] = isset($qc_confirms[$plan_id][$pa_group_id]['user_id']) ? $users[$qc_confirms[$plan_id][$pa_group_id]['user_id']]['fullname'] : '';
							$data[] = isset($qc_confirms[$plan_id][$dc_group_id]['user_id']) ? $users[$qc_confirms[$plan_id][$dc_group_id]['user_id']]['fullname'] : '';
							$data[] = isset($qc_confirms[$plan_id][$sup_group_id]['user_id']) ? $users[$qc_confirms[$plan_id][$sup_group_id]['user_id']]['fullname'] : '';
							$data[] = isset($qc_confirms[$plan_id][$pl_group_id]['user_id']) ? $users[$qc_confirms[$plan_id][$pl_group_id]['user_id']]['fullname'] : '';
							$data[] = isset($qc_notes[$plan_id][1]) ? 1 : '';
							$data[] = isset($qc_notes[$plan_id][1]) ?  $qc_notes[$plan_id][1]['date_added'] : '';
							// $data[] = isset($qc_notes[$plan_id][2]) ? 1 : '';
							// $data[] = isset($qc_notes[$plan_id][2]) ?  $qc_notes[$plan_id][2]['date_added'] : '';
							if ($p['is_fix'] > 1 && $p['date_fixed'] != '0000-00-00 00:00:00') {
								$data[] = 1;
								$data[] = $p['date_fixed'];
							} else {
								$data[] = '';
								$data[] = '';
							}
							

							//AA -> AI
							$data[] = '=IF(AB'.$row.'>0,AB'.$row.'-BG'.$row.',AB'.$row.')';//Lỗi Sup
							$data[] = '=IF(SUM(AC'.$row.':AH'.$row.')>0,1,0)'; // Lỗi Auditor
							$data[] = '=IF(SUM(AJ'.$row.':AK'.$row.')>0,1,0)'; //Code 1: Nội Qui
							$data[] = '=IF(SUM(AL'.$row.':AR'.$row.')>0,1,0)';//Code 2: Chất lượng hình ảnh
							$data[] = '=IF(SUM(AS'.$row.':AV'.$row.')>0,1,0)';//Code 3: Thông tin và  số liệu sai 
							$data[] = '';//Code 4: Kiến thức sản phẩm/chương trình
							$data[] = '';//Code 5: Lỗi trưng bày
							$data[] = '=IF(SUM(AW'.$row.':BC'.$row.')>0,1,0)';//Code 6: Nghi ngờ cheating Hình ảnh
							$data[] = '=BD'.$row; //Code7: Khác
						
							foreach ($qc_problems as $problem_id) {
								$data[] = isset($qc_code[$plan_id][$problem_id]) ? 1 : '';
							}

							$stick_sup = '';
							if (isset($qc_confirms[$plan_id][$sup_group_id])) {
								$date_sup_confirm = $qc_confirms[$plan_id][$sup_group_id]['date_added'];
								$date_limit = date('Y-m-d H:i:s', strtotime("+36 hours", strtotime($p['time_upload'])));
								// $date_sup_confirm = '2017-12-16 04:30:22';
								if ($date_sup_confirm <= $date_limit) {
									$stick_sup = 1;
								}
							}
							
							// echo date('d-m-Y', strtotime("+1 day", strtotime("10-12-2011")));exit;
							$data[] = $stick_sup;
							$data[] = isset($qc_notes[$plan_id][3]) ? implode(', ', $qc_notes[$plan_id][3]['notes']) : ''; //note data sai
							$data[] =  isset($qc_confirms[$plan_id][$dc_group_id]) ? $qc_confirms[$plan_id][$dc_group_id]['date_added'] : ''; //ngày stick của mod dc
							$data[] = isset($qc_notes[$plan_id][4]) ? implode(', ', $qc_notes[$plan_id][4]['notes']) : '';
							$data[] = isset($qc_notes[$plan_id][6]) ? implode(', ', $qc_notes[$plan_id][6]['notes']) : '';
							$data[] = isset($qc_notes[$plan_id][9]) ? implode(', ', $qc_notes[$plan_id][9]['notes']) : '';

							$data[] = $rating[$p['plan_rating']];
							$data[] = isset($reasons[$p['reason_id']]) ? $reasons[$p['reason_id']] : '';
							$data[] = isset($qc_notes[$plan_id][1]) ? implode(', ', $qc_notes[$plan_id][1]['notes']) : '';
							$data[] = $p['is_fix'] == 3 && $p['date_fixed'] != '0000-00-00 00:00:00' ? 1 : '';
							// $data[] = isset($chanels[$p['chanel']]) ? $chanels[$p['chanel']] : '';
							// $data[] = isset($store_types[$p['store_type_id']]) ? $store_types[$p['store_type_id']]['type_name'] : '';
							foreach ($attrs as $attr) {
								if (!empty($plandata[$p['plan_id']][$attr['attribute_id']])) {
									$val = $attr['input'] == 'select' ? $attr_status[$plandata[$p['plan_id']][$attr['attribute_id']]] : $plandata[$p['plan_id']][$attr['attribute_id']];
								} else {
									$val = '';
								}
								$data[] = $val;
							}
							// p($data,1);
							// $data[] = isset($qc_confirms[$plan_id][8]['user_id']) ? $users[$qc_confirms[$plan_id][8]['user_id']]['fullname'] : '';
							if ($p['plan_rating'] == 1) {
								$data[] = $p['note'];
							} elseif ($p['plan_rating'] == -2) {
								$data[] = $p['note_ktc'];
							}
							
							$worksheet->fromArray($data, NULL, 'A'.$row, false);
							$row++;
						}

						header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
						header('Content-Disposition: attachment;filename="'.$file_name.'"');
						header('Cache-Control: max-age=0');
						header("Content-Transfer-Encoding: binary");

						$objWriter = PHPExcel_IOFactory::createWriter($workbook, 'Excel2007');
						$objWriter->setPreCalculateFormulas(false);
						$objWriter->save($export_file);
						$objWriter->save('php://output');
						$this->clearSpreadsheetCache();
						exit();
					} catch(Exception $e){
						echo $e->__toString();
					}
				}
			} else {
				$this->session->data['error_warning'] = "Không có dữ liệu!";
				$this->response->redirect($this->url->link('plan/plan'));
				exit();
			}
		} 
	}
}



