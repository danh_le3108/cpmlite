<?php
class ControllerErrorPermission extends Controller {
	public function index() {
		$this->load->language('error/permission');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_permission'] = $this->language->get('text_permission');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link($this->request->get['route'], '', true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);

		
		$template = 'error/permission';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
		
	}
}
