<?php
class ControllerLocalisationPlace extends Controller{
	private $field_data = array(
			'place_id'=>'',
			'prefix'=>'',
			'geo_latitude'=>'',
			'geo_longitude'=>'',
			'place_name'=>'',
			'province_id'=>'',
			'district_id'=>'',
			'ward_id'=>''
		);
	public function place() {
		$json = array();

		$this->load->model('localisation/place');

		$query_info = $this->model_localisation_place->getPlace($this->request->get['place_id']);

		if ($query_info) {
			foreach($this->field_data as $key=>$default_value){
				if (!empty($query_info)) {
					$json[$key] = $query_info[$key];
				} else {
					$json[$key] = $default_value;
				}
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}