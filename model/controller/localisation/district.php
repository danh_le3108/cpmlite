<?php
class ControllerLocalisationDistrict extends Controller{
	private $field_data = array(
			'district_id'=>'',
			'name'=>'',
			'district_name'=>'',
			'prefix'=>'',
			'province_id'=>''
		);
	public function district() {
		$json = array();

		$this->load->model('localisation/district');

		$query_info = $this->model_localisation_district->getDistrict($this->request->get['district_id']);

		if ($query_info) {
			foreach($this->field_data as $key=>$default_value){
				if (!empty($query_info)) {
					$json[$key] = $query_info[$key];
				} else {
					$json[$key] = $default_value;
				}
			}
			$this->load->model('localisation/ward');
			$json['ward'] = $this->model_localisation_ward->getWardsByDistrictId($this->request->get['district_id']);
			
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}