<?php
class ControllerPlanPlanSurvey extends Controller {
	public function index($plan = array()) {
		if (!empty($plan) && $this->document->hasPermission('access','plan/plan_survey')) {
			$data['plan_id'] = $plan_id = $plan['plan_id'];
			$data['plan'] = $plan;
			$data['cctb'] = $plan['fridge'] + $plan['cvs_shelf'] + $plan['csd_shelf'] + $plan['aqf_shelf'] + $plan['tea_shelf'];
			$this->load->model('catalog/attribute');
			$data['attrs1'] = $this->model_catalog_attribute->getAttributes(array('sort'=>'g.sort_order,a.sort_order','group_id'=>1));
			$data['attrs2'] = $this->model_catalog_attribute->getAttributes(array('sort'=>'g.sort_order,a.sort_order','group_id'=>7));
			$data['attrs3'] = $this->model_catalog_attribute->getAttributes(array('sort'=>'g.sort_order,a.sort_order','group_id'=>8));
			$attrs4 = $this->model_catalog_attribute->getAttributes(array('sort'=>'g.sort_order,a.sort_order','parent_group_id'=>2));
			foreach ($attrs4 as $v) {
				$data['attrs4'][$v['group_name']][] = $v; 
			}
			//trung bày
			$data['attrs5'] = $this->model_catalog_attribute->getAttributes(array('sort'=>'g.sort_order,a.sort_order','group_id'=>10,'a.hide'=>0));
			$data['attrs6'] = $this->model_catalog_attribute->getAttributes(array('sort'=>'g.sort_order,a.sort_order','group_id'=>11,'a.hide'=>0));
			//kết qua trung bày
			$data['attrs7'] = $this->model_catalog_attribute->getAttributes(array('sort'=>'g.sort_order,a.sort_order','group_id'=>14));
			$data['attrs8'] = $this->model_catalog_attribute->getAttributes(array('sort'=>'g.sort_order,a.sort_order','group_id'=>15));
			
			$this->load->model('catalog/attribute_data');
			$attr_data = $this->model_catalog_attribute_data->getAttrsOfPlan(array('plan_id'=>$plan_id),0);

			foreach ($attr_data as $attr) {
				$data['attr_data'][$attr['item_number']][$attr['attribute_id']] = $attr['value'];
			}
			
			$this->load->model('localisation/image_type');
			$data['image_types'] = $this->model_localisation_image_type->getAllTypes();
			$image_list = $this->model_plan_plan->getPlanImages($plan_id);
			$data['images'] = array();
			if (!empty($image_list)) {
				$this->load->model('tool/image');
				$plan = $this->model_plan_plan->getPlan($this->request->get['plan_id']);
				$config_project_folder =  $this->config->get('config_project_folder');
				$file_path = 'files/'.$config_project_folder.'/'.$plan['round_name'].'/'.$plan['store_code'].'/image/'.$plan_id.'/';
				foreach ($image_list as $key => $value) {
					if (in_array($value['image_type_id'], array(67,68))) {
						if (file_exists(DIR_MEDIA.$file_path.$value['filename'])) {
							$value['thumb'] = $this->model_tool_image->resize($file_path.$value['filename'], 250, 250);
							$value['popup'] = HTTP_SERVER . 'media/' . $file_path.$value['filename'];
						} else {
							$value['thumb'] = $this->model_tool_image->resize('no_image.jpg', 250, 250);
							$value['popup'] = '';
						}
						$data['images'][$value['item_number']][$value['image_type_id']][] = $value;
					}
	 			}
			}
			$data['has_add_image'] = $this->document->hasPermission('add', 'plan/plan_image_audio');
			$data['has_edit_image'] = $this->document->hasPermission('edit', 'plan/plan_image_audio');
			$data['has_del_image'] = $this->document->hasPermission('delete', 'plan/plan_image_audio');
			$data['hasEdit'] = $this->document->hasPermission('edit','plan/plan_survey');
			$data['isAdmin'] = $this->user->getGroupId();
			
			$template = 'plan/plan_survey';
			return $this->load->view($template, $data);
		}
	}

	public function save() {
		if ($this->document->hasPermission('edit','plan/plan_survey') && !empty($this->request->get['plan_id'])) {
			$plan_id = $this->request->get['plan_id'];
			$item_number = !empty($this->request->get['item_number']) ? $this->request->get['item_number'] : 0;
			$this->load->model('plan/plan');
			$plan = $this->model_plan_plan->getPlan($plan_id);
			$attrs = $this->request->post['attrs'];
			$this->load->model('catalog/attribute_data');
			$attr_data = $this->model_catalog_attribute_data->getAttrsOfPlan(array('plan_id'=>$plan_id,'item_number'=>$item_number));
			$this->load->model('history/history');
			foreach ($attrs as $attr_id => $value) {
				$value = is_array($value) ? implode(',', $value) : $value;
				if (!empty($attr_data[$attr_id])) {
					$id = $attr_data[$attr_id]['id'];
					if ($attr_data[$attr_id]['value'] != $value) {
						$history = array(
							'table_name' => 'attribute_data',
							'user_id' => $this->user->getId(),
							'user_update' => $this->user->getUserName(),
							'controller' => 'plan/plan_survey',
							'table_primary_id' => $id,
							'data' => json_encode(array('value'=>$value)),
							'old_data' => json_encode($attr_data[$attr_id])
						);
						$this->model_history_history->add($history);

						$this->model_catalog_attribute_data->update($id, array('value'=>$value));
					}
				} else {
					if ($value != '') {
						$data = array(
							'plan_id' => $plan_id,
							'attribute_id' => $attr_id,
							'value' => $value,
							'round_name' => $plan['round_name'],
							'item_number' => $item_number
						);
						$this->model_catalog_attribute_data->add($data);
					}
				}
			}
			$this->model_catalog_attribute_data->updateDisplayResult($plan_id,$item_number);
			echo 1;
		}
	}

}