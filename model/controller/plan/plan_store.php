<?php
class ControllerPlanPlanStore extends Controller {
	public function index($plan) {
		if (!empty($plan) && $this->document->hasPermission('access','plan/plan_store')) {
			$this->load->model('tool/image');
			$file_path = $plan['image_overview'];
			if (file_exists(DIR_MEDIA.$file_path) && !empty($plan['image_overview'])) {
				$plan['thumb'] = $this->model_tool_image->resize($file_path, 100, 100);
				$plan['popup'] = HTTP_SERVER . 'media/' . $file_path;
			} else {
				$plan['thumb'] = $this->model_tool_image->resize('store.jpg', 100, 100);
				$plan['popup'] = '';
			}
			$this->load->model('localisation/reason_unsuccess');
			$reasion = $this->model_localisation_reason_unsuccess->getReasons();
			foreach ($reasion as $v) {
				if ($v['reason_id'] > 19) {
					$data['problems'][] = $v;
				} elseif ($v['reason_id'] < 19) {
					if($v['parent'] == 0) {
						$v['childs'] = array();
						$data['reasons'][$v['reason_id']] = $v;
					} else {
						$data['reasons'][$v['parent']]['childs'][] = $v;
					}
				}
			}
			// p($data['reasion'],1);
			// $this->load->model('store/store_type');
			// $data['store_types'] = $this->model_store_store_type->getAllTypes();
			// $this->load->model('store/store');
			// $chanels = $this->model_store_store->getChanels();
			// $data['chanels'] = array();
			// foreach ($chanels as $value) {
			// 	$data['chanels'][$value['id']] = $value['name'];
			// }
			// $this->load->model('catalog/attribute');
			// $data['posm_attrs'] = $this->model_catalog_attribute->getAttributes(array('group_id'=>1));
			$this->load->model('project/project_user');
			$user = $this->model_project_project_user->getUser($plan['user_id']);
			$data['sup'] = $this->model_project_project_user->getUser($user['user_parent_id']);
			$data['plan'] = $plan;
			$data['hasEdit'] = $this->document->hasPermission('edit','plan/plan_store');
			$data['is_user'] = $this->user->isLogged();
			$data['pl_status'] = array(
				0 => 'Chưa thực hiện',
				2 => 'Đang thực hiện',
				1 => 'Đã thực hiện'
			);
			$template = 'plan/plan_store';
			return $this->load->view($template, $data);
		}
	}
	
	public function save() {
		$this->load->model('plan/plan');
		
		if ($this->document->hasPermission('edit','plan/plan_store') && !empty($this->request->get['plan_id'])) {
			$this->load->model('history/history');
			$history = array(
				'table_name' => 'plan',
				'user_id' => $this->user->getId(),
				'user_update' => $this->user->getUserName(),
				'controller' => 'plan/plan_store',
				'table_primary_id' => $this->request->get['plan_id'],
				'data' => json_encode($this->request->post),
				'old_data' => json_encode($this->model_plan_plan->getPlan($this->request->get['plan_id']))
			);
			$this->model_history_history->add($history);
			$this->request->post['reason_id'] = !empty($this->request->post['reason_id']) ? implode(',', $this->request->post['reason_id']) : '';
			$this->model_plan_plan->updatePlan($this->request->get['plan_id'], $this->request->post);
			echo 1;
		} elseif (isset($this->request->post['feedback'])) {
			$this->model_plan_plan->updatePlan($this->request->get['plan_id'], $this->request->post);
		}
	}
	public function verify() {
		$this->load->language('plan/plan');
			$data['user_id'] = $this->user->getId();
		
		$json = array();
		//$this->log->write($this->request->get); 
		$this->load->model('plan/plan');
		if ($this->document->hasPermission('edit', 'plan/plan_store')&&isset($this->request->get['plan_id'])) {
			
			$plan_id = $this->request->get['plan_id'];
				
			$plan_info = $this->model_plan_plan->getPlan($plan_id);
		
		$sql = "UPDATE `" . PDB_PREFIX . "store` SET ";
		$sql .= "`store_latitude` = '" . $this->pdb->escape($plan_info['latitude']) . "',";
		$sql .= "`store_longitude` = '" . $this->pdb->escape($plan_info['longitude']) . "',";
		$sql .= "`is_verify` = 1 WHERE store_id = '" . (int)$plan_info['store_id'] . "'";
		$this->pdb->query($sql);
		
			$json['success'] = $this->session->data['success'] = $this->language->get('Đã xác thực Tọa độ!!');
		}else{
			$json['error'] = $this->language->get('Bạn không có quyền sửa thông tin!');
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function info() {
		if (isset($this->request->get['plan_id'])) {
			$this->load->model('plan/plan');
			$plan_id = $this->request->get['plan_id'];
			$plan_info = $this->model_plan_plan->getPlan($plan_id);
			$this->response->setOutput($this->index($plan_info));
		}
	}
}