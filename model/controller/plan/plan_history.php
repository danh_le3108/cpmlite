<?php
class ControllerPlanPlanHistory extends Controller {	
	public function index() {
		$this->load->model('plan/plan');
		$this->load->model('history/history');

		$languages = $this->load->language('plan/plan');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		unset($data);

		if (isset($this->request->get['plan_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$data['plan_id'] = $plan_id = $this->request->get['plan_id'];	
			
			$store_info = $this->model_plan_plan->getPlanInfo($plan_id);
			$data['info'] = $store_info;

		/*Status History*/ 
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		
		$data['histories'] = array();

		$data['rstatuses'] = $st = array(
			'1'=> $this->language->get('text_rating_passed'),
			'-1'=> $this->language->get('text_rating_not_passed'),
			'-2'=> $this->language->get('text_rating_unsuccess')
		);
		
		// $results = $this->model_history_history->getHistoriesByPlanId($plan_id, ($page - 1) * 10, 10);
		$results = $this->model_plan_plan->getPlanHistories($this->request->get['plan_id'], ($page - 1) * 10, 10);

		$this->load->model('user/user');
		foreach ($results as $result) {
			$user_info = $this->model_user_user->getUser($result['user_id']);
			$data['histories'][] = array(
				'fullname'     => $user_info['fullname'],
				'route'    => ucfirst(str_replace('plan_','',$result['route'])),
				'rating_old'    => isset($st[$result['rating_old']])?$st[$result['rating_old']]:'',
				'rating_new'    => isset($st[$result['rating_new']])?$st[$result['rating_new']]:'',
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}
		
		
		$history_total = $this->model_plan_plan->getTotalPlanHistories($this->request->get['plan_id']);

		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $history_total;
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->url = $this->url->link('plan/plan_history', 'plan_id=' . $this->request->get['plan_id'] . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = $pagination->results();
		

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		/*Status History*/ 
		
			$data['user_id'] = $this->user->getId();
			$template = 'plan/plan_history';
			$this->response->setOutput($this->load->view($template, $data));
		}else{
			echo $this->language->get('text_error_permission');
		}
	}
}