<?php
class ControllerPlanPlanNote extends Controller {	
	public function index() {
		if ($this->document->hasPermission('access','plan/plan_note') && isset($this->request->get['plan_id'])) {
			$data['plan_id'] = $plan_id = $this->request->get['plan_id'];	
		   	$this->load->model('plan/plan');
		   	$data['plan_info'] = $this->model_plan_plan->getPlan($plan_id);
			$this->load->model('plan/plan');
			$notes = $this->model_plan_plan->getPlanNotesByPlanId($plan_id);
			$data['notes'] = array();
			foreach ($notes as $value) {
 				$data['notes'][$value['note_id']][] = $value;
			}
			// print_r('<pre>'); print_r($data['notes']); print_r('</pre>'); 
			
			$this->load->model('qc/note');
			$data['note_types'] = $this->model_qc_note->getAllNotes();
			$data['user_sup'] = $this->config->get('config_user_sup');
			
			$data['user_group_id'] = $this->user->getGroupId();
			$data['hasEdit'] = $this->document->hasPermission('edit','plan/plan_note');
			$data['hasDel'] = $this->document->hasPermission('delete','plan/plan_note');
			
			$user_pa = $this->config->get('config_user_pa');
			$user_pl = $this->config->get('config_user_pl');
			$user_dc = $this->config->get('config_user_dc');
			$data['group_edit'] = array(1,$user_pa,$user_pl,$user_dc);
			
			
			$template = 'plan/plan_note';
			return $this->load->view($template, $data);
		}
	}

	public function save() {
		if ($this->document->hasPermission('edit','plan/plan_note') && isset($this->request->get['plan_id'])) {
			$this->load->model('plan/plan');
			$plan = $this->model_plan_plan->getPlan($this->request->get['plan_id']);
			if ($plan) {
				if($this->request->get['note_id']==1 && $plan['plan_status'] == 0){
					echo json_encode(array('status'=>0,'msg'=>'Không thể đề nghị khắc phục cửa hàng chưa thực hiện.'));
				} else {
					$data =  array(
						'plan_id' => $this->request->get['plan_id'],
						'round_name' => $plan['round_name'],
						'note_id' => $this->request->get['note_id'],
						'comment' => $this->request->get['comment'],
						'username' => $this->user->getUserName(),
						'fullname' => $this->user->getFullName(),
						'user_id' => $this->user->getId(),
						'user_group_id' => $this->user->getGroupId()
					);
					$this->model_plan_plan->addPlanNote($data);
					
					/*Plan Fix*/ 
					if($this->request->get['note_id']==1){
						$this->model_plan_plan->updatePlan($this->request->get['plan_id'], array('is_fix'=>1));
						$plan['title'] = '#'.$plan['plan_id'].' - '.$plan['store_code'].' cần khắc phục';
						$this->log->write($plan['title']); 
						$plan['message'] = 'Yêu cầu: '.$this->request->get['comment'];
						$this->sendNotification($plan);
					}
					/*End Fix*/ 
					echo json_encode(array('status'=>1,'msg'=>''));
				}
			}
		}
	}

	public function delete() {
		if ($this->document->hasPermission('delete','plan/plan_note') && isset($this->request->get['qc_note_id'])) {
			$this->load->model('plan/plan');
			$this->model_plan_plan->deletePlanNotes($this->request->get['qc_note_id']);
			echo 1;
		}
	}
	
	public function info() {
		$this->response->setOutput($this->index());
	}
	
	private function sendNotification($data) {
		if(isset($data['user_id']) && isset($data['message'])) {
			$this->load->model('project/project_user');
			$user = $this->model_project_project_user->getUser($data['user_id']);
			if($user){
				$json = '';
				$response = '';
				// save database
				$this->load->model('fcm/fcm');
				$mess = array(
					'user_id' => $this->user->getId(),
					'nguoi_nhan' => $user['user_id'],
					'user_group_id' => 0,
					'title' => $data['title'],
					'message' => $data['message'],
					'body' => $data['message'],
					'text' => $data['message'],
					'type' => 'send_to_user'
				);
				$message_id = $this->model_fcm_fcm->addMessage($mess);

				$firebase = new Fcm\Firebase($this->config->get('config_fcm_api_key'));
				
				$mess2 = array(
					'title' => $data['title'],
					'body' => $data['message']
				);
				$response = $firebase->send($user['google_token'], $mess2,0);
				
				$this->log->write('sendNotification: '.$data['message']);
				$this->log->write($response); 
				
				$res = json_decode($response, true);
				if(!empty($response) && $res['success']==1){
					$this->model_fcm_fcm->updateStatusFcm($message_id);
				}

				//$this->response->addHeader('Content-Type: application/json');
				//$this->response->setOutput($response);
			}
		}
	}
	
	public function approved_fix() {
		$this->load->model('plan/plan');
		$json = array();
		if ($this->document->hasPermission('edit', 'plan/plan_note')&&isset($this->request->get['qc_note_id']) && ($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$is_fixed = isset($this->request->post['is_fixed'])?$this->request->post['is_fixed']:0;
			$qc_note_id = $this->request->get['qc_note_id'];
			$plan_id = $this->request->get['plan_id'];
		
			$this->load->model('plan/plan');
			$plan_info = $this->model_plan_plan->getPlan($plan_id);
			
			if($qc_note_id==0&&$is_fixed==0){
				$notes = $this->model_plan_plan->getPlanNotesByPlanId($plan_id, 1);
				foreach ($notes as $note) {
					$this->model_plan_plan->deletePlanNotes($note['qc_note_id']);
				}
				$this->model_plan_plan->updatePlan($plan_id, array('is_fix'=>0));
						
				$plan_info['title'] = 'Hủy bỏ khắc phục #'.$plan_info['plan_id'].' - '.$plan_info['store_name'];
				$plan_info['message'] = 'Hệ thống đã hủy bỏ Yêu cầu khắc phục tại CH '.$plan_info['store_name'].'. Vui lòng bỏ qua khắc phục CH này!';
				$this->sendNotification($plan_info);
				
			}else{
				$data = array(
					'is_fixed' => $is_fixed,
					'date_approved' =>  $is_fixed == 1 ? date('Y-m-d H:i:s') : '0000-00-00 00:00:00'
				);
				$notes = $this->model_plan_plan->getPlanNotesByPlanId($plan_id,1);
				foreach ($notes as $n) {
					$this->model_plan_plan->updatePlanNote($n['qc_note_id'],$data);
				}
				// $this->model_plan_plan->updatePlanNote($qc_note_id,$data);
			
				//Chuyển thành đã KP
				$total_not_fix = $this->countNotesNotFixed($plan_id);
				$data = array(
					'is_fix' => $total_not_fix == 0 ? 3 : 2
				);
				if($total_not_fix==0 && $plan_info['date_fixed']=='0000-00-00 00:00:00'){
					$data['date_fixed'] = date('Y-m-d H:i:s');
				}
				$this->model_plan_plan->updatePlan($plan_id,$data);
			}
			$this->session->data['success'] = $json['success'] = $this->language->get('Thành công!');
		}else{
			$json['error'] = $this->language->get('Lỗi');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	private function countNotesNotFixed($plan_id) {
		$query = $this->pdb->query("SELECT COUNT(*)  AS total FROM " . PDB_PREFIX . "plan_note qn
		 WHERE qn.note_id = 1 
		 AND qn.is_fixed = '0' 
		 AND qn.is_deleted = '0' 
		 AND qn.plan_id = '" . (int)$plan_id . "'");
		return $query->row['total'];
	}
}