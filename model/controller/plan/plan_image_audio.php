<?php
class ControllerPlanPlanImageAudio extends Controller {
	public function index($plan = array()) {
		if($this->document->hasPermission('access','plan/plan_image_audio') && !empty($plan)) {
			$this->load->model('localisation/image_type');
			$data['image_types'] = $this->model_localisation_image_type->getAllTypes();
			$this->load->model('plan/plan');
			$plan_id = $plan['plan_id'];
			$image_list = $this->model_plan_plan->getPlanImages($plan_id);

			$customer = $this->customer->isLogged();
			$images = array();
			if (!empty($image_list)) {
				$this->load->model('tool/image');
				$plan = $this->model_plan_plan->getPlan($this->request->get['plan_id']);
				$config_project_folder =  $this->config->get('config_project_folder');
				$file_path = 'files/'.$config_project_folder.'/'.$plan['round_name'].'/'.$plan['store_code'].'/image/'.$plan_id.'/';
				foreach ($image_list as $key => $value) {
					if (!in_array($value['image_type_id'], array(67,68))) {
						if (file_exists(DIR_MEDIA.$file_path.$value['filename'])) {
							$value['thumb'] = $this->model_tool_image->resize($file_path.$value['filename'], 250, 250);
							$value['popup'] = HTTP_SERVER . 'media/' . $file_path.$value['filename'];
						} else {
							$value['thumb'] = $this->model_tool_image->resize('no_image.jpg', 250, 250);
							$value['popup'] = '';
						}
						$images[] = $value;
					}
					
	 			}
			}
			
			$data['plan_id'] = $this->request->get['plan_id'];
			$data['plan'] = $plan;
			$data['has_edit'] = $this->document->hasPermission('edit', 'plan/plan_image_audio');
			$data['has_del'] = $this->document->hasPermission('delete', 'plan/plan_image_audio');
			$data['images'] = $images;
			$data['audios'] = $this->model_plan_plan->getAudios($this->request->get['plan_id']);
			$this->load->model('store/store_type');
			$data['store_types'] = $this->model_store_store_type->getAllTypes();
			$this->load->model('store/store');
			$chanels = $this->model_store_store->getChanels();
			$data['chanels'] = array();
			foreach ($chanels as $value) {
				$data['chanels'][$value['id']] = $value['name'];
			}
			$data['is_user'] = $this->user->isLogged();
			/*DropZone*/
	        $this->document->addStyle('assets/plugins/dropzone/dropzone.min.css'); 
	        $this->document->addStyle('assets/plugins/dropzone/basic.min.css'); 
	        $this->document->addScript('assets/plugins/dropzone/dropzone.min.js'); 
			$template = 'plan/plan_image_audio';
			return $this->load->view($template, $data);
		}
	}

	public function set_avatar() {
		if ($this->document->hasPermission('edit', 'plan/plan_image_audio') && !empty($this->request->get['plan_id']) && !empty($this->request->get['image_id'])) {
			$this->load->model('plan/plan');
			$plan_id = $this->request->get['plan_id'];
			$plan = $this->model_plan_plan->getPlan($plan_id);
			$image = $this->model_plan_plan->getImage($this->request->get['image_id']);
			if (!empty($plan) && !empty($image)) {
				$config_project_folder =  $this->config->get('config_project_folder');
				$file_path = 'files/'.$config_project_folder.'/'.$plan['round_name'].'/'.$plan['store_code'].'/image/'.$plan_id.'/';
				$image_path = $file_path.$image['filename'];
				$this->model_plan_plan->updatePlan($plan_id, array('image_overview'=>$image_path));
				
				$this->load->model('store/store');
				$this->model_store_store->updateStore($plan['store_id'], array('store_image' => $image_path));
				$this->load->model('tool/image');
				$result = array(
					'thumb' => $this->model_tool_image->resize($image_path, 100, 100),
					'popup' => HTTP_SERVER . 'media/' . $image_path
				);
				echo json_encode($result);
			}
		}
	}

	public function upload() {
		if ($this->document->hasPermission('edit', 'plan/plan_image_audio') && !empty($this->request->get['plan_id']) && !empty($this->request->files['file'])) {
			$plan_id = $this->request->get['plan_id'];
			$this->load->model('plan/plan');
			$plan = $this->model_plan_plan->getPlan($plan_id);
			$filetype = explode('/', $this->request->files['file']['type']);
			$config_project_folder =  $this->config->get('config_project_folder');
			if (!empty($plan)) {
				$username = str_replace('.', '', $this->user->getUserName());
				if ($filetype[0] == 'image' && in_array($filetype[1], array('jpg','png','gif','jpeg'))) {
					$file_path = 'files/'.$config_project_folder.'/'.$plan['round_name'].'/'.$plan['store_code'].'/image/'.$plan_id.'/';
					$sha1 = token(5);
					//
					$filename = $plan['store_code'].$plan_id.'-'.$username.'-'.$sha1.'.'.$filetype[1];
					if (!is_dir(DIR_MEDIA.$file_path)) {
		                @mkdir(DIR_MEDIA.$file_path,  0777, true);
					}
					move_uploaded_file($this->request->files['file']["tmp_name"], DIR_MEDIA.$file_path.$filename);
					$this->load->model('tool/image');
					$image_data = array(
						'user_id'=>$plan['user_id'],
						'store_id'=>$plan['store_id'],
						'store_code'=>$plan['store_code'],
						'round_name'=>$plan['round_name'],
						'user_created'=>$this->user->getId(), //,
						'filename'=>$filename,
						'manual'=>1,
						'image_type_id'=>$this->request->get['image_type_id'],
						'item_number'=>$this->request->get['item_number'],
						'sha1' => $sha1,
					);
					$image_id = $this->model_plan_plan->addPlanImage($plan['plan_id'],$image_data);
					$result = array(
						'status' => 1,
						'image_id' => $image_id,
						'type' => 'image',
						'popup' => HTTP_SERVER . 'media/' . $file_path . $filename,
						'thumb' => $this->model_tool_image->resize($file_path . $filename, 250, 250),
						'image_type_id'=>$this->request->get['image_type_id'],
						'item_number'=>$this->request->get['item_number'],
					);
					echo json_encode($result);
				} elseif ($filetype[0] == 'audio' && $filetype[1] == "mp3") {
					$file_path = 'files/'.$config_project_folder.'/'.$plan['round_name'].'/'.$plan['store_code'].'/audio/'.$plan_id.'/';
					$sha1 = token(5);
					$filename = $plan['store_code'].'-'.$plan_id.'-'.$username.'-'.$sha1.'.'.$filetype[1];
					if (!is_dir(DIR_MEDIA.$file_path)) {
		                @mkdir(DIR_MEDIA.$file_path,  0777, true);
					}
					move_uploaded_file($this->request->files['file']["tmp_name"], DIR_MEDIA.$file_path.$filename);
					$data = array(
						'plan_id' => $this->request->get['plan_id'],
						'filename' => $file_path.$filename,
						'user_id' => $this->user->getId(),
					);
					$audio_id = $this->model_plan_plan->addAudio($data);
					$result = array(
						'status' => 1,
						'audio_id' => $audio_id,
						'type' => 'audio',
						'file' => HTTP_SERVER.'media/'.$file_path.$filename
					);
					echo json_encode($result);
				}
			}
		}
	}

	public function rotate() {
		if ($this->document->hasPermission('edit','plan/plan_image_audio') && isset($this->request->get['plan_id']) && isset($this->request->get['image_id'])) {
			$this->load->model('plan/plan');
			$plan_id = $this->request->get['plan_id'];
			$plan = $this->model_plan_plan->getPlan($plan_id);
			$img = $this->model_plan_plan->getImage($this->request->get['image_id']);
			if (!empty($plan) && !empty($img)) {
				$ext = strstr($img['filename'],'.');
				$username = str_replace('.', '', $this->user->getUserName());
				$filename = $plan['store_code'].'-'.$img['image_id'].'-'.$username.'-'.token(3).$ext;
				$config_project_folder =  $this->config->get('config_project_folder');
				$image_path = 'files/'.$config_project_folder.'/'.$plan['round_name'].'/'.$plan['store_code'].'/image/'.$plan_id.'/';
				$old_path = $image_path.$img['filename'];
				$new_path = $image_path.$filename;
				$this->load->model('tool/image');
				$thumb = $this->model_tool_image->rotate($old_path, $new_path, $this->request->get['angle']);
				if(file_exists(DIR_MEDIA.$new_path)){
					$img_info = getimagesize(DIR_MEDIA.$new_path);
					$width = ((int)$img_info[0]>(int)$img_info[1])?999:562;
					$height = ((int)$img_info[0]<(int)$img_info[1])?562:999;
				}else{
					$width = 999;
					$height = 562;
				}

				if (file_exists(DIR_MEDIA.$old_path)) {
					unlink(DIR_MEDIA.$old_path);
				}
				$files = glob(DIR_MEDIA . 'cache/'.$image_path.'*.*');
				if ($files) {
					foreach ($files as $file) {
						if (file_exists($file)) {
							unlink($file);
						}
					}
				}
				
				$this->model_plan_plan->updateImage($img['image_id'], array('filename'=>$filename));
				// $this->pdb->query($sql);

				$json = array(
					'thumb' => $this->model_tool_image->resize($new_path,250,250),
					'popup' => HTTP_SERVER.'media/'.$new_path,
					'image_id' => $img['image_id'],
					'is_avatar' => 0
				);

				if ($plan['image_overview'] == $image_path.$img['filename']) {
					$this->model_plan_plan->updatePlan($this->request->get['plan_id'],array('image_overview' => $new_path));
					$json['is_avatar'] = 1;
				}elseif ($plan['image_selfie'] == $image_path.$img['filename']) {
					$this->model_plan_plan->updatePlan($this->request->get['plan_id'],array('image_selfie' => $new_path));
				}
				
				echo json_encode($json);
			}
		}
	}

	public function delete_image() {
	    if (isset($this->request->get['image_id'])&&$this->document->hasPermission('delete', 'plan/plan_image_audio')) {
	      $this->load->model('plan/plan');
	      $this->model_plan_plan->deleteImage($this->request->get['image_id']);
	      echo 1;
	    }
	}

	public function delete_audio() {
	    if (!empty($this->request->get['audio_id'])&&$this->document->hasPermission('delete', 'plan/plan_image_audio')) {
	    	$this->load->model('plan/plan');
		    $this->model_plan_plan->deleteAudio($this->request->get['audio_id']);
	      	echo 1;
	    }
	}

	public function save() {
		if (!empty($this->request->post['image_ids'])&&$this->document->hasPermission('edit', 'plan/plan_image_audio')) {
	    	$this->load->model('plan/plan');
		    foreach ($this->request->post['image_ids'] as $image_id => $value) {
		    	$this->model_plan_plan->updateImage($image_id, array('image_type_id'=>$value));
		    }
	      	echo 1;
	    }
	}

	function downloadImages() {
		if (!empty($this->request->get['plan_id']) && $this->document->hasPermission('access','plan/plan_image_audio')) {
			$plan_id = $this->request->get['plan_id'];
			$this->load->model('plan/plan');
			$plan = $this->model_plan_plan->getPlan($plan_id);
			if (!empty($plan)) {
				$file_folder = DIR_MEDIA.$plan['image_path'].'image/'.$plan_id.'/';
				$files = glob($file_folder.'*.{jpg,png,gif}', GLOB_BRACE);
				if (!empty($files)) {
					$zip = new ZipArchive(); // Load zip library   
					$zip_name = $plan_id.'-'.$plan['store_code'];
			        $zip_file = $file_folder.$zip_name.'.zip';           // Zip name  
			        if($zip->open($zip_file, ZIPARCHIVE::CREATE)!==TRUE) {   
			            // Opening zip file to load files  
				        $error .= "* Sorry ZIP creation failed at this time";  
				        echo $error;
				        exit();
			        }  
			        foreach($files as $file) {
					 	$zip->addFile($file,$zip_name.'/'.str_replace($file_folder, $file_foldere, $file));
					} 
					$zip->close();
					if(file_exists($zip_file))  
	                {  
	                     // push to download the zip  
	                     header('Content-type: application/zip');  
	                     header('Content-Disposition: attachment; filename="'.$zip_name.'.zip"');
	                     readfile($zip_file);  
	                     // remove zip file is exists in temp path  
	                     unlink($zip_file);  
	                     echo 1;
	                }
				}
			}
		}
	}
}