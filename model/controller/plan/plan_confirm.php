<?php
class ControllerPlanPlanConfirm extends Controller {	
	public function index($plan) {
		if (!empty($plan) && ($this->request->server['REQUEST_METHOD'] != 'POST') && $this->document->hasPermission('access','plan/plan_confirm')) {
			$data['plan_id'] = $plan_id = $this->request->get['plan_id'];	
			$this->load->model('plan/plan');
			$data['confirms'] = $this->model_plan_plan->getPlanConfirms($plan_id);
			$this->load->model('qc/confirm');
			$data['user_groups'] = $this->model_qc_confirm->getGroupConfirms();
			$this->load->model('project/project_user');
			$data['users'] = $this->model_project_project_user->getProjectUsers();
			$data['hasEdit'] = $this->document->hasPermission('edit', 'plan/plan_confirm');
			$data['user_log'] = $this->user->getUserName();
			$data['user_id'] = $this->user->getId();
			$template = 'plan/plan_confirm';
			return $this->load->view($template, $data);
		}
	}
	public function save() {		
		if ($this->document->hasPermission('edit', 'plan/plan_confirm')&&isset($this->request->get['plan_id'])) {
			$this->load->model('plan/plan');
			$plan = $this->model_plan_plan->getPlan($this->request->get['plan_id']);
			
			$confirm = $this->model_plan_plan->getPlanConfirm($plan['plan_id'], $this->user->getId(), $this->user->getGroupId());

			if (empty($confirm)) {
				$data = array(
					'round_name' => $plan['round_name'],
					'plan_id' => $plan['plan_id'],
					'user_id' => $this->user->getId(),
					'username' => $this->user->getUserName(),
					'user_group_id' => $this->user->getGroupId()
				);
				$this->model_plan_plan->updateConfirm($data);
				echo json_encode($data);	
			}
		}
	}
	public function info() {
		$this->response->setOutput($this->index());
	}
}