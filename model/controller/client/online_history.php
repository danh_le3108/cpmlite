<?php
class ControllerClientOnlineHistory extends Controller{
	private $error = array();
	
	private $filter_key = array(
			'filter_global',
			'filter_round',
			'filter_level_id',
			'filter_name'
		);
	private function _url(){
		$url = '';
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(trim(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8')));
			}
		}
		return $url;
	}
  public function download() {
		$url = $this->_url();
		
		$languages = $this->load->language('client/online_history');
		$this->load->model('account/online_history');
			$filter_data = array();
			foreach($this->filter_key as $key){
				if (isset($this->request->get[$key])) {
					$filter_data[$key] = $this->request->get[$key];
				}
			}
		
			
			if($this->customer->isLogged()){
				$username =$this->customer->getUsername();
			}else{
				$username =$this->user->getUsername();
			}
			$results = $this->model_account_online_history->getOnlineHistories($filter_data);

			$this->load->model('project/customer');
			$customer_info = $this->model_project_customer->getProjectCustomers();
			
			$online_histories =array();
			
			$online_histories[] =  array(
                'A' => $this->language->get('text_user'),
                'B' => $this->language->get('text_username'),
                'C' => $this->language->get('text_customer_group'),
                'D' => $this->language->get('text_ip_address'),
                'E' => $this->language->get('text_total_login'),
                'F' => $this->language->get('text_last_login'),
			);
			
			foreach ($results as $result) {
				$online_histories[] = array(
					'A'       => $customer_info[$result['customer_user_id']]['fullname'],
					'B'       => $result['username'],
					'C'       => $customer_info[$result['customer_user_id']]['level_name'],
					'D'       => $result['ip'],
					'E'       => $result['total'],
					'F'       => $result['date_modified']
				);
			}

			$export_data = array(
				'template_path'=>DIR_MEDIA . 'files/'.$this->config->get('config_project_folder').'/export/online/',
				'template_name'=>'CustomerOnline-Export-by-'.$username.'-at-'. date('Ymd-His'),
				'heading_style'=>array('colrow'=>'A1:F1','color'=>'#333333','bg'=>'#9cc3e6'),
				'template_data'=>$online_histories
			
			);
			$this->excel->export($export_data);
		
			$this->response->redirect($this->url->link('client/online_history',$url , true));
	}
	public function index() {
		$data['ajax_list'] = $this->ajax_list();
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$template = 'common/ajax_list';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
	public function getList() {
		$this->response->setOutput($this->ajax_list());
	}
	public function ajax_list() {
		$languages = $this->load->language('client/online_history');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('account/online_history');
		
		$this->load->model('plan/round');
		$data['rounds'] = $this->model_plan_round->getRounds();
		
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'cl.date_modified';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = $this->_url();
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

		$data['download'] = $this->url->link('client/online_history/download',  $url , true);
		
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '' , true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('client/online_history',  $url , true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);


		$data['online_histories'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_project'),
			'limit' => $this->config->get('config_limit_project')
		);
		
		
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$filter_data[$key] = trim($this->request->get[$key]);
			} else {
				$filter_data[$key] = NULL;
			}
		}

		$query_total = $this->model_account_online_history->getTotalOnlineHistories($filter_data);

		$results = $this->model_account_online_history->getOnlineHistories($filter_data);

		$this->load->model('project/customer');
		$customer_info = $this->model_project_customer->getProjectCustomers();
		
		foreach ($results as $result) {
			$data['online_histories'][] = array(
				'customer_login_id' => $result['customer_login_id'],
				'fullname'       => $customer_info[$result['customer_user_id']]['fullname'],
				'username'       => $result['username'],
				'total'       => $result['total'],
				'ip'       => $result['ip'],
				'customer_level'       => $customer_info[$result['customer_user_id']]['level_name'],
				'customer_level_id'       => $result['customer_level_id'],
				'last_login'       => $result['date_modified']
			);
		}


		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = $this->_url();

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('client/online_history', '&sort=name' . $url , true);

		$url = $this->_url();

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $query_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_project');
		$pagination->url = $this->url->link('client/online_history/getList', $url . '&page={page}' , true);

		$data['pagination'] = $pagination->render();

		$data['results'] = $pagination->results();

		$data['sort'] = $sort;
		$data['order'] = $order;
		
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$data[$key] = trim($this->request->get[$key]);
			} else {
				$data[$key] = NULL;
			}
		}
		/**/ 
		
		$this->load->model('project/customer_level');
		$cfilter = array();
		$data['groups'] = $this->model_project_customer_level->getCustomerLevels($cfilter,'customer_level_id');
		
		
		$template = 'client/online_history';
		
		return $this->load->view($template, $data);
	}
}