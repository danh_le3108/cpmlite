<?php
class ControllerClientPlan extends Controller{
	private $error = array();

	private $field_data = array(
			'plan_id'=>'',
			'plan_name'=>''
		);

	private $filter_key = array(
			'filter_global',
			'filter_date',
			'filter_rsm',
			'filter_asm',
			'filter_customer_parent_id',
			'filter_customer_user_id',
			'filter_distributor_id',
			'filter_province_id',
			'filter_round',
			'filter_rating_status',
			'filter_status',
			'filter_name',
			'filter_area_code',
			'filter_survey_group',
			'filter_region_code'
		);

	private function _url(){
		$url = '';
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}
	public function index() {
		$data['ajax_list'] = $this->ajax_list();
		$this->document->addStyle('assets/plugins/magnific/magnific-popup.css');
		$this->document->addScript('assets/plugins/magnific/jquery.magnific-popup.js');
		$template = 'common/ajax_list';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
	public function getList() {
		$this->response->setOutput($this->ajax_list());
	}
	public function ajax_list() {
		
		$data['config_top'] = $config_top = $this->config->get('config_customer_top');
		$data['config_rsm'] = $config_rsm = $this->config->get('config_customer_rsm');
		$data['config_asm'] = $config_asm = $this->config->get('config_customer_asm');
		$data['config_sup'] = $config_sup = $this->config->get('config_customer_sup');
		
		if(!$this->user->isLogged()&&$this->customer->isLogged()){
			$data['getLevel'] = $this->customer->getLevel(); 
		}else{
			$data['getLevel'] = 0;
		}
		
		
		$this->load->model('plan/plan');
			$data['today'] = date('Y-m-d');

		$this->load->model('catalog/distributor');
		$data['distributors'] =  $this->model_catalog_distributor->getDistributors();
		$languages = $this->load->language('plan/plan');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}



		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'plan_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = $this->_url();
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('client/plan',  $url, true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);


		$data['plans'] = array();
		
		$filter_data = array();
		
		
		foreach($this->filter_key as $key){
			if(isset($this->request->get[$key])){
				$filter_data[$key] = trim($this->request->get[$key]);
			}
		}
		$filter_data['filter_qc_status'] = 1;
		$filter_data['filter_plan_status'] = 1;
		
		
		$this->load->model('project/project_user');
		$data['staffs'] = $staffs = $this->model_project_project_user->getProjectUsersByGroupID($this->config->get('config_user_staff'),$filter_data);
		
		
		$this->load->model('localisation/area');
		$data['areas'] = $this->model_localisation_area->getAreas($filter_data);
		$this->load->model('localisation/region');
		
		$data['regions'] = $this->model_localisation_region->getRegions($filter_data);
		
		
		/*Province*/
		$this->load->model('localisation/province');
		$data['provinces'] =  $this->model_localisation_province->getProvinces($filter_data);
		$customers_managers = array();
		$rsms = array();
		$asms = array();
		$sups = array();
		
		if($this->user->isLogged()||$this->customer->getLevel()==$data['config_top']){
			$data['isNationwide'] = $isNationwide = 1;
		}else{
			$data['isNationwide'] = $isNationwide =0;
		}
		if(!$this->user->isLogged()&&$this->customer->isLogged()){
			$filter_data['customers_manager'] = $customers_managers = $this->customer->customers_manager(); 
		}
		
		$this->load->model('project/customer');
		$customers = $this->model_project_customer->getProjectCustomers($filter_data);
		
		foreach($customers as $customer){
			if($customer['customer_level_id']==$config_rsm){
				$rsms[]	= $customer;
			}
			if($customer['customer_level_id']==$config_asm){
				$asms[]	= $customer;
			}
			if($customer['customer_level_id']==$config_sup){
				$sups[]	= $customer;
			}
		}
		
		$data['rsms'] = $rsms;
		$data['asms'] = $asms;
		$data['sups'] = $sups;
		
		if(!$this->user->isLogged()&&$this->customer->isLogged()){
			$filter_data['customers_manager'] = $this->customer->customers_manager(); 
		}
		if (isset($this->request->get['filter_rsm'])) {
			$filter_rsm = $this->customer->getSupsByParentID($this->request->get['filter_rsm']);
			if($isNationwide==1){
				$filter_data['customers_manager'] = $filter_rsm;
			}else{
				$filter_data['customers_manager'] = array();
				foreach ($filter_rsm as $rsm){
					if(in_array($rsm,$customers_managers)){
				$filter_data['customers_manager'][] = $rsm;		
					}
				}
			}
		} 
		if (isset($this->request->get['filter_asm'])) {
			$filter_asm = $this->customer->getSupsByParentID($this->request->get['filter_asm']);
			if($isNationwide==1){
				$filter_data['customers_manager'] = $filter_asm;
			}else{
				$filter_data['customers_manager'] = array();
				foreach ($filter_asm as $asm){
					if(in_array($asm,$customers_managers)){
				$filter_data['customers_manager'][] = $asm;		
					}
				}
			}
		} 

		$data['round_count'] = $this->model_plan_plan->countTotal('round_name',$filter_data, array('filter_plan_status','filter_qc_status','customers_manager'));
		$data['customer_count'] = $this->model_plan_plan->countTotal('customer_user_id',$filter_data, array('filter_plan_status','filter_qc_status','customers_manager'));
		$data['area_count'] = $this->model_plan_plan->countTotal('area_code',$filter_data, array('filter_plan_status','filter_qc_status','customers_manager'));
		$data['region_count'] = $this->model_plan_plan->countTotal('region_code',$filter_data, array('filter_plan_status','filter_qc_status','customers_manager','filter_date','filter_area_code'));
		$data['province_count'] = $this->model_plan_plan->countTotal('province_id',$filter_data, array('filter_plan_status','filter_qc_status','customers_manager','filter_date','filter_region_code'));
		$data['rating_count'] = $this->model_plan_plan->countTotal('plan_rating',$filter_data, array('filter_plan_status','filter_qc_status','customers_manager','filter_date','filter_area_code','filter_region_code','filter_province_id','filter_rating_status'));
		
		$filter_data['sort'] = $sort; 
		$filter_data['order'] = $order;
		$filter_data['start'] = ($page - 1) * $this->config->get('config_limit_project');
		$filter_data['limit'] = $this->config->get('config_limit_project');
		
		
		$results = $this->model_plan_plan->getPlans($filter_data);
		
			$query_total = $this->model_plan_plan->getTotalPlans($filter_data);
	
		
		

		$this->load->model('tool/image');
		
		
			
		$this->load->model('survey/survey');
		$data['surveys'] = $survey = $this->model_survey_survey->getSurveyIndexBy('survey_id');
		
		
		
		$data['survey_audit'] = $this->config->get('config_survey_audit');
		$data['survey_adhoc'] = $this->config->get('config_survey_adhoc');
		
		
		$data['survey_groups'] = array(
				'survey_audit'=>'SkinCare',
				'survey_adhoc'=>'AdHoc'
		);	
		$data['rating_result'] = array(
				1=>'Đạt',
				0=>'',
				-1=>'Không đạt',
				-2=>'KTC',
			);
			
		$plan_surveys = $this->model_plan_plan->getPlansSurveys($filter_data);
			
			
		$data['plans'] = array();

		foreach ($results as $result) {
			//$distributor = isset($dcode[])?
			$plan_id = $result['plan_id'];
			
			$plan_status = $result['plan_status'];
			
			$plan_survey = isset($plan_surveys[$plan_id])?$plan_surveys[$plan_id]:array();
			
			
			$survey_ids = isset($result['survey_ids'])?explode(',',$result['survey_ids']):array();
			
			if($plan_status==1){
				$plan_status_text = $this->language->get('text_has_perform');
			}else{
				$plan_status_text = $this->language->get('text_no_perform');
			}
			if($plan_status==1&&$result['plan_rating']==1){
				$plan_rating_text = $this->language->get('text_rating_success');
//$this->language->get('text_rating_passed');
			}elseif($plan_status==1&&$result['plan_rating']==-1){
				$plan_rating_text = $this->language->get('text_rating_not_passed');
			}elseif($plan_status==1&&$result['plan_rating']==-2){
				$plan_rating_text = $this->language->get('text_rating_unsuccess');
			}else{
				$plan_rating_text = '';
			}
			
			
			$plan_qc_text = ($result['plan_qc']==1)?$this->language->get('text_has_qc'):$this->language->get('text_no_qc');
			

			if(!empty($result['image_overview'])&&file_exists(DIR_MEDIA.$result['image_path'].$result['image_overview'])){
					$popup = HTTP_SERVER.'media/'.$result['image_path'].$result['image_overview'];
					$store_image = $this->model_tool_image->best_fit($result['image_path'].$result['image_overview'], 150, 120);
			}else{
				if(!empty($result['store_image'])&&file_exists(DIR_MEDIA.$result['store_image'])){
					$popup = HTTP_SERVER.'media/'.$result['store_image'];
					$store_image = $this->model_tool_image->best_fit($result['store_image'], 200, 113);
				} else {
					$store_image = $this->model_tool_image->best_fit('store.jpg', 200, 113);
					$popup = false;
				}
			}

			$google_address = format_address($result['store_ward'].'|'.$result['store_district'].'|'.$result['store_province'].'|Vietnam','+');

			//?route=dev/update_rating
			$data['plans'][$result['plan_id']] = array(
				'plan_id'                => $result['plan_id'],
				'plan_survey'             => $plan_survey,
				'survey_ids'             => $survey_ids,
				'round_name'             => $result['round_name'],
				'plan_name'              => $result['plan_name'],
				'store_id'               => $result['store_id'],
				'reason_id'              => $result['reason_id'],
				'note_ktc'         => addslashes($result['note_ktc']),
				'plan_qc'                => $result['plan_qc'],
				'plan_qc_text'           => $plan_qc_text,
				'plan_rating'            => $result['plan_rating'],
				'plan_rating_text'       => $plan_rating_text,
				'point_total'            => $result['point_total'],
				'plan_status'            => $result['plan_status'],
				'upload_count'            => $result['upload_count'],
				'date_start'             => $result['date_start'],
				'date_end'               => $result['date_end'],
				'plan_status_text'       => $plan_status_text,
				'longitude'              => $result['longitude'],
				'latitude'               => $result['latitude'],
				'user_id'                => $result['user_id'],
				'usercode'               => $result['usercode'],
				'users_read'             => !empty($result['users_read'])?explode(',',$result['users_read']):array(),
				'fullname'               => isset($staffs[$result['user_id']])?$staffs[$result['user_id']]['fullname']:$result['usercode'],
				'edit'                   => $this->url->link('client/plan/info', '&plan_id=' . $result['plan_id'] . $url, true),
				/* Store info*/
				'popup'                  => $popup,
				'store_image'            => $store_image,
				'store_id'               => $result['store_id'],
				'store_name'             => $result['store_name'],
				'customer_user_id'       => $result['customer_user_id'],
				'customer_user'          => $result['customer_user'],
				'sale_name'              => $result['sale_name'],
				'distributor_code' 		 => $result['distributor_code'],
				'store_distributor'      => $result['store_distributor'],
				'store_longitude'        => $result['store_longitude'],
				'store_latitude'         => $result['store_latitude'],
				'store_code'             => $result['store_code'],
				'store_address_raw'      => str_replace('Số nhà','',$result['store_address_raw']),
				'store_address'          => $result['store_address'],
				'store_place'            => $result['store_place'],
				'google_address'         => $google_address,
				'address_raw'            => $result['store_ward'].'-'.$result['store_district'].'-'.$result['store_province'],
				'store_customer_code'    => $result['store_customer_code'],
				'store_ad_code'    => $result['store_ad_code'],
				'store_province'         => $result['store_province'],
				'version'         => $result['version'],
				'date_added'         => date('d-m H:i', strtotime($result['date_added'])),
				'date_start'         => date('d-m', strtotime($result['date_start'])),
				'date_end'         => date('d-m', strtotime($result['date_end'])),
				'time_checkin'         => ($result['time_checkin']!='0000-00-00 00:00:00')?date('d-m H:i:s', strtotime($result['time_checkin'])):'',
				'time_image'         => ($result['time_upload_image']!='0000-00-00 00:00:00')?date('d-m H:i:s', strtotime($result['time_upload_image'])):'',
				'time_survey'         => ($result['time_upload_survey']!='0000-00-00 00:00:00')?date('d-m H:i:s', strtotime($result['time_upload_survey'])):'',
				'time_coupon'         => ($result['time_upload_coupon']!='0000-00-00 00:00:00')?date('d-m H:i:s', strtotime($result['time_upload_coupon'])):'',
				'time_upload'         => ($result['time_upload']!='0000-00-00 00:00:00')?date('d-m H:i:s', strtotime($result['time_upload'])):''
			);
		}


		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = $this->_url();

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('client/plan', '&sort=name' . $url, true);

		$url = $this->_url();

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $query_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_project');
		$pagination->url = $this->url->link('client/plan/getList', $url . '&page={page}'. '' , true);

		$data['pagination'] = $pagination->render();

		$data['results'] = $pagination->results();

		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['total'] = $query_total;

		/*Comment*/

		$data['rstatuses'] = array(
			'1'=> $this->language->get('text_rating_passed'),//$this->language->get('text_rating_success')
			'-1'=> $this->language->get('text_rating_not_passed'),
			'-2'=> $this->language->get('text_rating_unsuccess')
		);
		$data['statuses'] = array(
			'0'=> $this->language->get('text_no_perform'),
			'1'=> $this->language->get('text_has_perform')
		);

		$this->document->addScript('//maps.google.com/maps/api/js?key='.$this->config->get('config_google_api_key'));
		$this->document->addScript('assets/plugins/gmaps/gmaps.js');
		
		$this->load->model('plan/round');
		$data['rounds'] =  $this->model_plan_round->getRounds();
		 
		 

		/*StaftUser*/
		$data['href'] = $this->url->link('client/plan', '', true);
		
		foreach($this->filter_key as $key){
			if(isset($this->request->get[$key])){
				$data[$key] = trim($this->request->get[$key]);
			}else{
				$data[$key] = NULL;
			}
		}
		//print_r('<pre>'); print_r($data['filter_asm']); print_r('</pre>'); 
		
			$data['text_store_customer_code'] = sprintf($this->language->get('text_store_customer_code'),$this->config->get('config_meta_title'));
			
		
		
		$data['export_plan'] = $this->load->controller('client/export_plan', $filter_data);
		$template = 'client/plan_list';
		return $this->load->view($template, $data);
	}

	public function info() {
		
		$this->load->model('plan/plan');
		$languages = $this->load->language('plan/plan');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		$this->document->setTitle($this->language->get('heading_title'));


		$error_data = array(
			'error_warning'=>'',
			'error_name'=>''
		);

		foreach($error_data as $key=>$default_value){
			if (isset($this->error[$key])) {
				$data[$key] = $this->error[$key];
			} else {
				$data[$key] = $default_value;
			}
		}

		$url = $this->_url();

				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}
				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}
				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('client/plan',  $url, true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);

		if (!isset($this->request->get['plan_id'])) {
			$data['plan_id'] = 0;
		} else {
			$data['plan_id'] = $this->request->get['plan_id'];
		}

		$data['cancel'] = $this->url->link('client/plan',  $url, true);

		if (isset($this->request->get['plan_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$plan_info = $this->model_plan_plan->getPlanInfo($this->request->get['plan_id']);
		}  else{
			$plan_info = array();
		}
		/*Province*/
		$data['href'] = $this->url->link('client/plan', '', true);

			$data['plan_surveys'] = array();
		if(isset($plan_info['plan_id'])){
			$data['plan_store'] = $this->load->controller('client/plan_store');
			$data['plan_image_store'] = $this->load->controller('client/plan_image_store'); 
			$data['plan_image_audit'] = $this->load->controller('client/plan_image_audit'); 
			$data['plan_image_coupon'] = $this->load->controller('client/plan_image_coupon'); 
			
			$survey_ids = $this->model_plan_plan->getPlanSurveys($plan_info['plan_id']);
			
			if(is_array($survey_ids)){
				foreach($survey_ids as $survey_id => $survey){
					$survey_data = array('survey_id'=>$survey_id);
					$data['plan_surveys'][$survey_id] = $this->load->controller('client/plan_survey',$survey_data); 
				}
			}
			
			$data['plan_coupon'] = $this->load->controller('client/plan_coupon');
			
		}else{
			$data['plan_store'] = '';
			$data['plan_image_store'] = '';
			$data['plan_image_audit'] = '';
			$data['plan_image_coupon'] ='';
			$data['plan_coupon'] = '';
		}
			$data['config_image_store'] = $this->config->get('config_image_store');
			$data['config_image_audit'] = $this->config->get('config_image_audit');
			$data['config_image_coupon'] = $this->config->get('config_image_coupon');

		$this->document->addStyle('assets/plugins/magnific/magnific-popup.css');
		$this->document->addScript('assets/plugins/magnific/jquery.magnific-popup.js');

		$template = 'client/plan_form';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
}