<?php
class ControllerClientDashboard extends Controller{
	private $error = array();

	private $field_data = array(
			'plan_id'=>'',
			'plan_name'=>''
		);

	private $filter_key = array(
			'filter_global',
			'filter_date',
			'filter_rsm',
			'filter_asm',
			'filter_customer_parent_id',
			'filter_customer_user_id',
			'filter_distributor_id',
			'filter_province_id',
			'filter_round',
			'filter_rating_status',
			'filter_status',
			'filter_name',
			'filter_area_code',
			'filter_survey_group',
			'filter_region_code'
		);

	private function _url(){
		$url = '';
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}
	public function index() {
		
		$data['config_top'] = $config_top = $this->config->get('config_customer_top');
		$data['config_rsm'] = $config_rsm = $this->config->get('config_customer_rsm');
		$data['config_asm'] = $config_asm = $this->config->get('config_customer_asm');
		$data['config_sup'] = $config_sup = $this->config->get('config_customer_sup');
		
		if(!$this->user->isLogged()&&$this->customer->isLogged()){
			$data['getLevel'] = $this->customer->getLevel(); 
		}else{
			$data['getLevel'] = 0;
		}
		
		
		$this->load->model('plan/plan');
			

		$languages = $this->load->language('plan/plan');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$url = $this->_url();
		
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('client/plan',  $url, true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);


		$data['plans'] = array();
		
		$filter_data = array();
		
		
		foreach($this->filter_key as $key){
			if(isset($this->request->get[$key])){
				$filter_data[$key] = trim($this->request->get[$key]);
			}
		}
		$filter_data['filter_qc_status'] = 1;
		$filter_data['filter_plan_status'] = 1;
		
		/*Customers*/
		$customers_managers = array();
		$rsms = array();
		$asms = array();
		$sups = array();
		
		if($this->user->isLogged()||$this->customer->getLevel()==$data['config_top']){
			$data['isNationwide'] = $isNationwide = 1;
		}else{
			$data['isNationwide'] = $isNationwide =0;
		}
		if(!$this->user->isLogged()&&$this->customer->isLogged()){
			$filter_data['customers_manager'] = $customers_managers = $this->customer->customers_manager(); 
		}
		
		$this->load->model('project/customer');
		$customers = $this->model_project_customer->getProjectCustomers($filter_data);
		
		foreach($customers as $customer){
			if($customer['customer_level_id']==$config_rsm){
				$rsms[]	= $customer;
			}
			if($customer['customer_level_id']==$config_asm){
				$asms[]	= $customer;
			}
			if($customer['customer_level_id']==$config_sup){
				$sups[]	= $customer;
			}
		}
		
		$data['rsms'] = $rsms;
		$data['asms'] = $asms;
		$data['sups'] = $sups;
		
		if(!$this->user->isLogged()&&$this->customer->isLogged()){
			$filter_data['customers_manager'] = $this->customer->customers_manager(); 
		}
		if (isset($this->request->get['filter_rsm'])) {
			$filter_rsm = $this->customer->getSupsByParentID($this->request->get['filter_rsm']);
			if($isNationwide==1){
				$filter_data['customers_manager'] = $filter_rsm;
			}else{
				$filter_data['customers_manager'] = array();
				foreach ($filter_rsm as $rsm){
					if(in_array($rsm,$customers_managers)){
				$filter_data['customers_manager'][] = $rsm;		
					}
				}
			}
		} 
		if (isset($this->request->get['filter_asm'])) {
			$filter_asm = $this->customer->getSupsByParentID($this->request->get['filter_asm']);
			if($isNationwide==1){
				$filter_data['customers_manager'] = $filter_asm;
			}else{
				$filter_data['customers_manager'] = array();
				foreach ($filter_asm as $asm){
					if(in_array($asm,$customers_managers)){
				$filter_data['customers_manager'][] = $asm;		
					}
				}
			}
		}
		
		$data['rating_count'] = $this->model_plan_plan->getTotalPlansByType('plan_rating',$filter_data, array('filter_plan_status','filter_qc_status','customers_manager','filter_date','filter_area_code','filter_region_code','filter_province_id','filter_rating_status'));
		

		$data['total'] = $this->model_plan_plan->getTotalPlans($filter_data);
		
		$data['survey_skin'] = $survey_skin = $this->config->get('config_survey_audit');
		$data['survey_adhoc'] = $survey_adhoc = $this->config->get('config_survey_adhoc');
		
		$filter_skin = $filter_data;
		$filter_skin['filter_survey_ids'] = $survey_skin;
		//$data['skin_not_pass'] = $this->groupTotalNotPassReasons($filter_skin);
		$data['skin_unsuccess'] = $this->groupTotalUnsuccessReasons($filter_skin);
		$data['skin_result'] = $this->groupTotalPlansBySurveyIds($filter_skin);
		$data['skin_rating'] = $this->groupTotalPlansByRating($filter_skin);
		
		
		
		$filter_adhoc = $filter_data;
		$filter_adhoc['filter_survey_ids'] = $survey_adhoc;
		//$data['adhoc_not_pass_total'] = $this->countTotalNotPassReasons($filter_adhoc);
		$data['adhoc_not_pass'] = $this->groupTotalNotPassReasons($filter_adhoc);
		//$data['adhoc_unsuccess_total'] = $this->countTotalUnsuccessReasons($filter_adhoc);
		$data['adhoc_unsuccess'] = $this->groupTotalUnsuccessReasons($filter_adhoc);
		$data['adhoc_result'] = $this->groupTotalPlansBySurveyIds($filter_adhoc);
		
		
		
		
		
		$data['survey_groups'] = array(
				'survey_audit'=>'SkinCare',
				'survey_adhoc'=>'AdHoc'
		);	
		$data['rating_result'] = array(
				'1'=>'Đạt',
				'-1'=>'Không đạt',
				'-2'=>'KTC',
			);

		/*Comment*/

		$data['rstatuses'] = array(
			'1'=> $this->language->get('text_rating_passed'),
			'-1'=> $this->language->get('text_rating_not_passed'),
			'-2'=> $this->language->get('text_rating_unsuccess')
		);
		
		$this->load->model('plan/round');
		$data['rounds'] =  $this->model_plan_round->getRounds();
		 
		//$this->load->model('survey/survey');
		//$data['surveys'] = $survey = $this->model_survey_survey->getSurveyIndexBy('survey_id');
		 

		/*StaftUser*/
		$data['href'] = $this->url->link('client/plan', '', true);
		
		foreach($this->filter_key as $key){
			if(isset($this->request->get[$key])){
				$data[$key] = trim($this->request->get[$key]);
			}else{
				$data[$key] = NULL;
			}
		}
		
		$data['text_store_customer_code'] = sprintf($this->language->get('text_store_customer_code'),$this->config->get('config_meta_title'));
			
		
		$this->document->addScript('assets/plugins/highcharts/code/highcharts.js');
		$this->document->addScript('assets/plugins/highcharts/code/modules/exporting.js');
		
		$data['colors'] = array('"#0b62a4"','"#dd4b39"','"#666"');
		
		
		$this->document->addScript('assets/plugins/morris/raphael-min.js');
		$this->document->addScript('assets/plugins/morris/morris.min.js');
		$this->document->addScript('assets/plugins/morris/mymorris.js');
		
		$template = 'client/dashboard';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
	/*KHÔNG ĐẠT*/ 
	private function countTotalNotPassReasons($filter=array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "plan_survey ps 
		LEFT JOIN " . DB_PREFIX . "plan pl ON (ps.plan_id = pl.plan_id) 
		LEFT JOIN " . DB_PREFIX . "store s ON (s.store_id = pl.store_id) 
		WHERE ps.`not_pass_id`>0 AND pl.is_deleted=0 AND pl.plan_status=1 AND pl.plan_qc=1";
		if(isset($filter['filter_round'])){
			$sql .= " AND pl.round_name = '" . $this->pdb->escape($filter['filter_round']) . "'";
		}
		if (isset($filter['filter_survey_ids'])&&!is_null($filter['filter_survey_ids'])) {
			$sql .= " AND ps.survey_id IN('".implode("','",$filter['filter_survey_ids'])."')";
		}
		if (isset($filter['customers_manager'])&&!is_null($filter['customers_manager'])) {
			$sql .= " AND s.customer_user_id IN('".implode("','",$filter['customers_manager'])."')";
		}
		if (isset($filter['filter_customer_user_id'])&&!is_null($filter['filter_customer_user_id'])) {
			$sql .= " AND s.customer_user_id ='".(int)$filter['filter_customer_user_id']."'";
		}
		
		//print_r('<pre>'); print_r($sql); print_r('</pre>'); 
		$query = $this->pdb->query($sql);
		return $query->row['total'];
	}
	private function groupTotalNotPassReasons($filter=array()) {
		$sql = "SELECT ps.`not_pass_id`, COUNT(*) AS total, (SELECT r.reason_name 
		FROM `" . DB_PREFIX . "catalog_reason_not_pass` r 
		WHERE r.not_pass_id = ps.not_pass_id) AS reason_name FROM " . DB_PREFIX . "plan_survey ps
		LEFT JOIN " . DB_PREFIX . "plan pl ON (ps.plan_id = pl.plan_id) 
		LEFT JOIN " . DB_PREFIX . "store s ON (s.store_id = pl.store_id) 
		WHERE ps.`not_pass_id`>0 AND pl.is_deleted=0 AND pl.plan_status=1 AND pl.plan_qc=1";
		if(isset($filter['filter_round'])){
			$sql .= " AND ps.round_name = '" . $this->pdb->escape($filter['filter_round']) . "'";
		}
		if (isset($filter['filter_survey_ids'])&&!is_null($filter['filter_survey_ids'])) {
			$sql .= " AND ps.survey_id IN('".implode("','",$filter['filter_survey_ids'])."')";
		}
		if (isset($filter['customers_manager'])&&!is_null($filter['customers_manager'])) {
			$sql .= " AND s.customer_user_id IN('".implode("','",$filter['customers_manager'])."')";
		}
		if (isset($filter['filter_customer_user_id'])&&!is_null($filter['filter_customer_user_id'])) {
			$sql .= " AND s.customer_user_id ='".(int)$filter['filter_customer_user_id']."'";
		}
		$sql .= " GROUP BY ps.`not_pass_id`";
		
		//print_r('<pre>'); print_r($sql); print_r('</pre>'); 
		$query = $this->pdb->query($sql);
			$plan_total = array();
			$total = 0;
		foreach($query->rows as $row){
			$total += $row['total'];
			$row['reason_name'] = empty($row['reason_name'])?'Thành công':$row['reason_name'];
			$plan_total['data'][$row['not_pass_id']] = $row;
		}
			$plan_total['total'] = $total;
		return $plan_total;
	}
	/*KTC*/ 
	private function countTotalUnsuccessReasons($filter=array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "plan_survey ps 
		LEFT JOIN " . DB_PREFIX . "plan pl ON (ps.plan_id = pl.plan_id) 
		LEFT JOIN " . DB_PREFIX . "store s ON (s.store_id = pl.store_id) 
		WHERE ps.`ktc_reason_id`>0 AND pl.is_deleted=0 AND pl.plan_status=1 AND pl.plan_qc=1";
		if(isset($filter['filter_round'])){
			$sql .= " AND pl.round_name = '" . $this->pdb->escape($filter['filter_round']) . "'";
		}
		if (isset($filter['filter_survey_ids'])&&!is_null($filter['filter_survey_ids'])) {
			$sql .= " AND ps.survey_id IN('".implode("','",$filter['filter_survey_ids'])."')";
		}
		if (isset($filter['customers_manager'])&&!is_null($filter['customers_manager'])) {
			$sql .= " AND s.customer_user_id IN('".implode("','",$filter['customers_manager'])."')";
		}
		if (isset($filter['filter_customer_user_id'])&&!is_null($filter['filter_customer_user_id'])) {
			$sql .= " AND s.customer_user_id ='".(int)$filter['filter_customer_user_id']."'";
		}
		$query = $this->pdb->query($sql);
		return $query->row['total'];
	}
	private function groupTotalUnsuccessReasons($filter=array()) {
		$sql = "SELECT ps.`ktc_reason_id`, COUNT(*) AS total, (SELECT r.reason_name 
		FROM `" . DB_PREFIX . "catalog_reason_unsuccess` r 
		WHERE r.reason_id = ps.ktc_reason_id) AS reason_name FROM " . DB_PREFIX . "plan_survey ps  
		LEFT JOIN " . DB_PREFIX . "plan pl ON (ps.plan_id = pl.plan_id) 
		LEFT JOIN " . DB_PREFIX . "store s ON (s.store_id = pl.store_id) 
		WHERE ps.`rating_manual`='-2' AND pl.is_deleted=0 AND pl.plan_status=1 AND pl.plan_qc=1";
		if(isset($filter['filter_round'])){
			$sql .= " AND ps.round_name = '" . $this->pdb->escape($filter['filter_round']) . "'";
		}
		if (isset($filter['filter_survey_ids'])&&!is_null($filter['filter_survey_ids'])) {
			$sql .= " AND ps.survey_id IN('".implode("','",$filter['filter_survey_ids'])."')";
		}
		if (isset($filter['customers_manager'])&&!is_null($filter['customers_manager'])) {
			$sql .= " AND s.customer_user_id IN('".implode("','",$filter['customers_manager'])."')";
		}
		if (isset($filter['filter_customer_user_id'])&&!is_null($filter['filter_customer_user_id'])) {
			$sql .= " AND s.customer_user_id ='".(int)$filter['filter_customer_user_id']."'";
		}
		$sql .= " GROUP BY ps.`ktc_reason_id`";
		
		
		$query = $this->pdb->query($sql);
			$plan_total = array();
			$total = 0;
		foreach($query->rows as $row){
			$total += $row['total'];
			$row['reason_name'] = empty($row['reason_name'])?'Thành công':$row['reason_name'];
			$plan_total['data'][$row['ktc_reason_id']] = $row;
		}
			$plan_total['total'] = $total;
		return $plan_total;
	}
	
	/**/ 
	
	private function groupTotalPlansByRating($filter=array()) {
		$sql = "SELECT ps.`rating_manual`, COUNT(*) AS total FROM " . DB_PREFIX . "plan_survey ps 
		LEFT JOIN " . DB_PREFIX . "plan pl ON (ps.plan_id = pl.plan_id) 
		LEFT JOIN " . DB_PREFIX . "store s ON (s.store_id = pl.store_id) 
		WHERE pl.is_deleted=0 AND pl.plan_status=1 AND pl.plan_qc=1";
		if(isset($filter['filter_round'])){
			$sql .= " AND ps.round_name = '" . $this->pdb->escape($filter['filter_round']) . "'";
		}
		if (isset($filter['filter_survey_ids'])&&!is_null($filter['filter_survey_ids'])) {
			$sql .= " AND ps.survey_id IN('".implode("','",$filter['filter_survey_ids'])."')";
		}
		if (isset($filter['customers_manager'])&&!is_null($filter['customers_manager'])) {
			$sql .= " AND s.customer_user_id IN('".implode("','",$filter['customers_manager'])."')";
		}
		if (isset($filter['filter_customer_user_id'])&&!is_null($filter['filter_customer_user_id'])) {
			$sql .= " AND s.customer_user_id ='".(int)$filter['filter_customer_user_id']."'";
		}
		$sql .= " GROUP BY ps.`rating_manual` ORDER BY ps.`rating_manual` DESC";
		$query = $this->pdb->query($sql);
			$plan_total = array();
			$plan_total['data'] = array();
			$total = 0;
			
		
		foreach($query->rows as $row){
			$total += $row['total'];
			
			$plan_total['data'][$row['rating_manual']] = $row;
			
		}
			$plan_total['total'] = $total;
		return $plan_total;
	}
	private function groupTotalPlansBySurveyIds($filter=array()) {
		$sql = "SELECT ps.`survey_id`, ps.`rating_manual`, COUNT(*) AS total, 
		(SELECT s.survey_name FROM `" . DB_PREFIX . "survey` s  WHERE s.survey_id = ps.survey_id) AS survey_name 
		FROM " . DB_PREFIX . "plan_survey ps 
		LEFT JOIN " . DB_PREFIX . "plan pl ON (ps.plan_id = pl.plan_id) 
		LEFT JOIN " . DB_PREFIX . "store s ON (s.store_id = pl.store_id) 
		WHERE pl.is_deleted=0 AND pl.plan_status=1 AND pl.plan_qc=1";
		if(isset($filter['filter_round'])){
			$sql .= " AND ps.round_name = '" . $this->pdb->escape($filter['filter_round']) . "'";
		}
		if (isset($filter['filter_survey_ids'])&&!is_null($filter['filter_survey_ids'])) {
			$sql .= " AND ps.survey_id IN('".implode("','",$filter['filter_survey_ids'])."')";
		}
		if (isset($filter['customers_manager'])&&!is_null($filter['customers_manager'])) {
			$sql .= " AND s.customer_user_id IN('".implode("','",$filter['customers_manager'])."')";
		}
		if (isset($filter['filter_customer_user_id'])&&!is_null($filter['filter_customer_user_id'])) {
			$sql .= " AND s.customer_user_id ='".(int)$filter['filter_customer_user_id']."'";
		}
		$sql .= " GROUP BY ps.`survey_id`, ps.`rating_manual`";
		
		$query = $this->pdb->query($sql);
			$plan_total = array();
			$plan_total['data'] = array();
			$total = 0;
		foreach($query->rows as $row){
			$total += $row['total'];
			
			if(!isset($plan_total['data'][$row['survey_id']]['total'])){
				$plan_total['data'][$row['survey_id']]['total'] = 0;
			}
				$plan_total['data'][$row['survey_id']]['total'] += $row['total'];
			
			$plan_total['data'][$row['survey_id']]['survey_name'] = $row['survey_name'];
			$plan_total['data'][$row['survey_id']]['rating'][$row['rating_manual']] = $row['total'];
			
		}
			$plan_total['total'] = $total;
		return $plan_total;
	}
	private function getTotalPlansBySurveyResult($filter=array()) {
		$sql = "SELECT ps.`survey_id`, ps.`rating_manual`, COUNT(*) AS total FROM " . DB_PREFIX . "plan_survey ps 
		LEFT JOIN " . DB_PREFIX . "plan pl ON (ps.plan_id = pl.plan_id) 
		LEFT JOIN " . DB_PREFIX . "store s ON (s.store_id = pl.store_id) 
		WHERE pl.is_deleted=0 AND pl.plan_status=1 AND pl.plan_qc=1";
		if(isset($filter['filter_round'])){
			$sql .= " AND ps.round_name = '" . $this->pdb->escape($filter['filter_round']) . "'";
		}
		if (isset($filter['customers_manager'])&&!is_null($filter['customers_manager'])) {
			$sql .= " AND s.customer_user_id IN('".implode("','",$filter['customers_manager'])."')";
		}
		if (isset($filter['filter_customer_user_id'])&&!is_null($filter['filter_customer_user_id'])) {
			$sql .= " AND s.customer_user_id ='".(int)$filter['filter_customer_user_id']."'";
		}
		$sql .= " GROUP BY ps.`survey_id`, ps.`rating_manual`";
		$query = $this->pdb->query($sql);
			$plan_total = array();
		foreach($query->rows as $row){
			$plan_total[$row['survey_id']] = $row;
		}
		return $plan_total;
	}
}