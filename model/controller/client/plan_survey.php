<?php
class ControllerClientPlanSurvey extends Controller {
	public function index($survey_data=array()) {
		if (isset($this->request->get['plan_id']) &&(isset($survey_data['survey_id'])||isset($this->request->get['survey_id']))) {
			
			$data['survey_id'] = $survey_id = isset($this->request->get['survey_id'])?$this->request->get['survey_id']:$survey_data['survey_id'];
		
			
			$languages= $this->load->language('plan/plan');
			foreach($languages as $key=>$value){
					$data[$key] = $value;	
			}
			$data['plan_id'] = $plan_id = $this->request->get['plan_id'];	
			
			$this->load->model('plan/plan');
			$data['plan_info'] = $plan_info = $this->model_plan_plan->getPlanInfo($plan_id);
			$data['survey'] = $this->model_plan_plan->getPlanSurveys($plan_id);
	
			$this->load->model('survey/survey');
			$data['survey_info'] = $this->model_survey_survey->getSurveyHistory($survey_id,$plan_info['round_name']);
			
			$data['survey_answer'] = $this->model_plan_plan->getPlanSurvey($plan_id,$survey_id);
			
			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];
	
				unset($this->session->data['success']);
			} else {
				$data['success'] = '';
			}
			
			
		$this->load->model('localisation/reason_unsuccess');
		$data['reasons'] = $this->model_localisation_reason_unsuccess->getReasons();
		
		
		$this->load->model('localisation/reason_not_pass');
		$data['not_pass_reasons'] = $this->model_localisation_reason_not_pass->getReasons();
		
		
			$data['user_group_id'] = $this->user->getGroupId();
			$data['user_id'] = $this->user->getId();
			 $template = 'client/plan_survey';
			return $this->load->view($template, $data);
		}
		
		
	}
	public function info() {
		$this->response->setOutput($this->index());
	}
}