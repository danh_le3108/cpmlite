<?php
class ControllerCommonHeader extends Controller {
	public function index() {
		$data['base'] = HTTP_PROJECT;
		$data['http_server'] = HTTP_SERVER;
		
		$data['config_color_mode'] = $this->config->get('config_color_mode');//'config_color_mode'
		$data['config_nav'] = $this->config->get('config_nav');//'nav_top'

		if (is_file(DIR_MEDIA . $this->config->get('config_icon'))) {
			$this->document->addLink($data['http_server'] . 'media/' . $this->config->get('config_icon'), 'icon');
		}

		$data['title'] = $this->document->getTitle();

		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$data['config_name'] = $this->config->get('config_name');
		$data['project_id'] = $this->config->get('config_project_id');
		$data['customer'] = $this->customer->getFullName();
		
		$data['user_logged'] = $this->user->isLogged();
		$data['distributor_logged'] = $this->distributor->isLogged();
		$data['customer_logged'] = $this->customer->isLogged();
		
		$data['home'] = $this->url->link('common/home', '', true);
		if (is_file(DIR_MEDIA . $this->config->get('config_logo'))) {
			$data['logo'] = $data['http_server'] . 'media/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		
		
		$languages = $this->load->language('common/header');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		if($this->user->isLogged()){
				
			$user_id = $this->user->getId();
			$username = $this->user->getUsername();
			
			$img_folder = 'files/users/' . $username.'/';
		
			$this->load->model('tool/image');
			$image =  $this->user->getImage();
			if (is_file(DIR_MEDIA . $img_folder.$image)) {
				$image= $this->model_tool_image->resize($img_folder.$image, 100, 100);
			} else {
				$image = $this->model_tool_image->resize('avatar.jpg', 100, 100);
			}
			
			$data['user'] = array(
				'fullname' =>$this->user->getFullname(),
				'username' =>$this->user->getUserName(),
				'group' =>$this->user->getGroup(),
				'image' =>$image,
				'edit' =>$this->url->link('user/user', '', true),
				'logout' =>$this->url->link('user/logout',  '', true),
			);
		}else{
			$data['user'] = array();
		}
			
			
		$data['login_action'] = $this->url->link('account/login', '', $this->request->server['HTTPS']);
		
		$data['distributor_info'] = $this->load->controller('common/distributor_info');
		$data['customer_info'] = $this->load->controller('common/customer_info');
		$data['user_info'] = $this->load->controller('common/user_info');
		$data['navigation'] = $this->load->controller('common/navigation');
		$data['language'] = $this->load->controller('common/language');
		//$data['search'] = $this->load->controller('common/search');
			
			$data['user_id'] = $this->user->getId();
			
			$data['user_group_id'] = $this->user->getGroupId();
			
			$user_pa = $this->config->get('config_user_pa');
			$user_pl = $this->config->get('config_user_pl');
			$data['group_edit'] = array(1,$user_pa,$user_pl);
			
			
		$data['information'] =  $this->url->link('information/information');
		$data['region'] =  $this->url->link('localisation/region');
		$data['province'] =  $this->url->link('localisation/province');
		$data['search_image'] =  $this->url->link('tool/search_image');
		$data['project_user'] =  $this->url->link('project/project_user');
		$data['project_customer'] =  $this->url->link('project/customer');
		$data['project_distributor'] =  $this->url->link('catalog/distributor');
		$data['search_image'] =  $this->url->link('tool/search_image');
		$data['coupon_history'] =  $this->url->link('catalog/coupon_history');
			
		$data['import_distributor'] = $this->url->link('excel/template/template',  'template_id=1', true);
		$data['import_store'] = $this->url->link('excel/template/template',  'template_id=2', true);
		$data['import_coupon'] = $this->url->link('excel/template/template',  'template_id=4', true);
		$data['import_plan'] = $this->url->link('excel/template/template',  'template_id=3', true);
			
		// For page specific css
		if (isset($this->request->get['route'])) {
			$class = str_replace('/', '-', $this->request->get['route']);
		} else if (isset($this->request->request['route'])) {
			$class = str_replace('/', '-', $this->request->request['route']);
		}else {
			$class = 'common-home';
		}
		$class = ($class=='common-forgotten'||!$this->user->isLogged())?'common-login':$class;
		
		$data['class'] = $class;
		
		
		if (isset($this->request->request['route'])) {
			$url_data = $this->request->request;

			$route = $url_data['route'];

			unset($url_data['route']);

			$url = '';

			if ($url_data) {
				if (isset($url_data['_url'])) {
					unset($url_data['_url']);
				}
				$url = '&' . urldecode(http_build_query($url_data, '', '&'));
			}

			$data['redirect'] = $this->url->link($route, $url, $this->request->server['HTTPS']);
		}else {
		
			$data['redirect'] = $this->url->link('common/home');
		}
		

		return $this->load->view('common/header', $data);
	}
}
