<?php
class ControllerCommonNavigation extends Controller {
	public function index() {
			$this->load->language('common/navigation');
			$data['menus'] = array();
			
			
 		$config_top = $this->config->get('config_customer_top');
		
 		$config_check_coupon_user_ids = $this->config->get('config_check_coupon_user_ids');
		$customer_id = 0;
		
		if($this->user->isLogged()){
			$isNationwide = 1;
		}else if($this->customer->isLogged() && $this->customer->getLevel()==$config_top){
			$isNationwide = 1;
			$customer_id = $this->customer->getId();
		}else{
			$isNationwide =0;
		}
		
			
		if($this->user->isLogged()){
			$user_group_id = $this->user->getGroupId();
		}else{
			$user_group_id = 0;
		}		
					
		$filter_round = $this->pdb->query("SELECT MAX(round_name) as round_name FROM cpm_plan WHERE is_deleted = 0")->row['round_name'];	
		if ($this->user->isLogged()||$this->customer->isLogged()||$this->distributor->isLogged()) {
			
			
			
			// Store
			$plan = array();
			
			if ($this->document->hasPermission('access', 'plan/progress')) {
				$plan[] = array(
						'name'	   => $this->language->get('text_plan_progress'),
						'href'     => $this->url->link('plan/progress','filter_round_name='.$filter_round.'&filter_date='.date('Y-m-d'), true),
						'children' => array()
					);
			}
			if ($this->document->hasPermission('access', 'project/user_route')) {
				$plan[] = array(
					'name'	   => $this->language->get('text_user_route'),
					'href'     => $this->url->link('project/user_route', 'filter_round_name='.$filter_round, true),
					'children' => array()
				);
			}
			if ($this->document->hasPermission('access', 'plan/plan')) {
				$plan[] = array(
						'name'	   => $this->language->get('text_plan'),
						'href'     => $this->url->link('plan/plan', 'filter_round_name='.$filter_round, true),
						'children' => array()
					);
			}
			if ($this->document->hasPermission('access', 'plan/map')) {
				$plan[] = array(
					'name'	   => $this->language->get('text_plan_map'),
					'href'     => $this->url->link('plan/map',  'filter_round_name='.$filter_round, true),
					'children' => array()
				);
			}
			// if ($this->document->hasPermission('access', 'catalog/distributor')) {
			// 	$plan[] = array(
			// 		'name'	   => $this->language->get('text_distributor'),
			// 		'href'     => $this->url->link('catalog/distributor', '', true),
			// 		'children' => array()
			// 	);
			// }
			if ($this->document->hasPermission('access', 'store/store')) {
				$plan[] = array(
					'name'	   => $this->language->get('text_store'),
					'href'     => $this->url->link('store/store', '', true),
					'children' => array()
				);
			}
			/*
			if ($this->document->hasPermission('access', 'catalog/coupon')) {
				$plan[] = array(
					'name'	   => $this->language->get('text_coupon'),
					'href'     => $this->url->link('catalog/coupon', '', true),
					'children' => array()
				);
			}
			if ($this->document->hasPermission('access', 'catalog/coupon_history')) {
				$plan[] = array(
					'name'	   => $this->language->get('text_coupon_history'),
					'href'     => $this->url->link('catalog/coupon_history', '', true),
					'children' => array()
				);
			}
			if ($this->document->hasPermission('access', 'catalog/coupon_prefix')) {
				$plan[] = array(
					'name'	   => $this->language->get('text_coupon_prefix'),
					'href'     => $this->url->link('catalog/coupon_prefix', '', true),
					'children' => array()
				);
			}
			*/ 
			if ($plan) {
				$data['menus'][] = array(
					'id'       => 'menu-plan',
					'icon'	   => 'fa-calendar',
					'name'	   => $this->language->get('text_plans'),
					'href'     => '',
					'children' => $plan
				);
			}
			// Store
			$project_user = array();
			if ($this->document->hasPermission('access', 'project/project_user')) {
				$project_user[] = array(
						'name'	   => $this->language->get('text_project_user'),
						'href'     => $this->url->link('project/project_user', '', true),
						'children' => array()
					);
			}

			if ($this->document->hasPermission('access', 'project/customer')) {
				$project_user[] = array(
						'name'	   => 'Nhân viên khách hàng',
						'href'     => $this->url->link('project/customer', '', true),
						'children' => array()
					);
			}
			if ($project_user) {
				$data['menus'][] = array(
					'id'       => 'menu-user',
					'icon'	   => 'fa-users',
					'name'	   => $this->language->get('text_users'),
					'href'     => '',
					'children' => $project_user
				);
			}
/*
			$report = array();
			if ($this->document->hasPermission('access', 'plan/progress')) {
				$report[] = array(
						'name'	   => $this->language->get('text_plan_progress'),
						'href'     => $this->url->link('plan/progress', '', true),
						'children' => array()
					);
			}
			if ($report) {
				$data['menus'][] = array(
					'id'       => 'menu-user',
					'icon'	   => 'fa-users',
					'name'	   => $this->language->get('text_reports'),
					'href'     => '',
					'children' => $report
				);
			}*/ 

			// Catalog
			$catalog = array();
			if ($this->document->hasPermission('access', 'catalog/group')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_group'),
					'href'     => $this->url->link('catalog/group', '', true),
					'children' => array()
				);
			}
			if ($this->document->hasPermission('access', 'catalog/attribute')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_attribute'),
					'href'     => $this->url->link('catalog/attribute', '', true),
					'children' => array()
				);
			}
			if ($this->document->hasPermission('access', 'survey/survey')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_survey'),
					'href'     => $this->url->link('survey/survey', '', true),
					'children' => array()
				);
			}
			/* 
			if ($this->document->hasPermission('access', 'catalog/coupon_status')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_coupon_status'),
					'href'     => $this->url->link('catalog/coupon_status', '', true),
					'children' => array()
				);
			}*/
			if ($this->document->hasPermission('access', 'catalog/reward_type')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_reward_type'),
					'href'     => $this->url->link('catalog/reward_type', '', true),
					'children' => array()
				);
			}
			
			if ($catalog) {
				$data['menus'][] = array(
					'id'       => 'menu-local',
					'icon'	   => 'fa-inbox',
					'name'	   => $this->language->get('text_catalog'),
					'href'     => '',
					'children' => $catalog
				);
			}
			
			// Localisation
			$localisation = array();

			if ($this->document->hasPermission('access', 'project/project_settings')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_project_settings'),
					'href'     => $this->url->link('project/project_settings', '' , true),
					'children' => array()
				);
			}
			if ($this->document->hasPermission('access', 'user/user_group')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_user_group'),
					'href'     => $this->url->link('user/user_group', '' , true),
					'children' => array()
				);
			}
			if ($user_group_id==1&&$this->document->hasPermission('access', 'excel/template')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_excel_template'),
					'href'     => $this->url->link('excel/template', '', true),
					'children' => array()
				);
			}
			if ($this->document->hasPermission('access', 'localisation/location')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_location'),
					'href'     => $this->url->link('localisation/location', '', true),
					'children' => array()
				);
			}
			if ($this->document->hasPermission('access', 'localisation/region')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_region'),
					'href'     => $this->url->link('localisation/region', '', true),
					'children' => array()
				);
			}/*
			if ($this->document->hasPermission('access', 'localisation/area')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_area'),
					'href'     => $this->url->link('localisation/area', '', true),
					'children' => array()
				);
			}*/ 
			if ($this->document->hasPermission('access', 'localisation/image_type')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_image_type'),
					'href'     => $this->url->link('localisation/image_type', '', true),
					'children' => array()
				);
			}
			/*
			if ($this->document->hasPermission('access', 'localisation/province')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_province'),
					'href'     => $this->url->link('localisation/province', '', true),
					'children' => array()
				);
			}

			if ($this->document->hasPermission('access', 'localisation/district')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_district'),
					'href'     => $this->url->link('localisation/district', '', true),
					'children' => array()
				);
			}
			if ($this->document->hasPermission('access', 'localisation/ward')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_ward'),
					'href'     => $this->url->link('localisation/ward', '', true),
					'children' => array()
				);
			}
			if ($this->document->hasPermission('access', 'localisation/place')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_place'),
					'href'     => $this->url->link('localisation/place', '', true),
					'children' => array()
				);
			}*/
			if ($this->document->hasPermission('access', 'localisation/reason_unsuccess')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_plan_reason'),
					'href'     => $this->url->link('localisation/reason_unsuccess', '', true),
					'children' => array()
				);
			}
			if ($this->document->hasPermission('access', 'localisation/reason_not_pass')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_reason_not_pass'),
					'href'     => $this->url->link('localisation/reason_not_pass', '', true),
					'children' => array()
				);
			}
			if ($this->document->hasPermission('access', 'distributor/coupon_scan')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_coupon_scan'),
					'href'     => $this->url->link('distributor/coupon_scan', '', true),
					'children' => array()
				);
			}
			if ($this->document->hasPermission('access', 'tool/log')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_log'),
					'href'     => $this->url->link('tool/log', '', true),
					'children' => array()
				);
			}
			
			
			if ($this->document->hasPermission('access', 'tool/log')) {
				$localisation[] = array(
					'name'	   => 'App '.$this->language->get('text_log'),
					'href'     => $this->url->link('api/log_error/errors', '', true),
					'children' => array()
				);
			}
			if ($this->document->hasPermission('access', 'tool/filemanager_plus')) {
				$localisation[] = array(
					'name'	   => $this->language->get('Image manager'),
					'href'     => $this->url->link('tool/filemanager_plus', '', true),
					'children' => array()
				);
			}
			if ($this->document->hasPermission('access', 'tool/search_image')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_search_image'),
					'href'     => $this->url->link('tool/search_image', '', true),
					'children' => array()
				);
			}

			if ($localisation) {
				$data['menus'][] = array(
					'id'       => 'menu-local',
					'icon'	   => 'fa-globe',
					'name'	   => $this->language->get('text_system'),
					'href'     => '',
					'children' => $localisation
				);
			}
			// Information
			// if ($this->document->hasPermission('access', 'information/information')) {
			// 		$data['menus'][] = array(
			// 			'id'       => 'menu-info',
			// 			'icon'	   => 'fa-info-circle',
			// 			'name'	   => $this->language->get('text_user_guide'),
			// 			'href'     => $this->url->link('information/information', '', true),
			// 			'children' => array()
			// 		);
			// }
// Client
			$audit = array();
			if ($this->document->hasPermission('access', 'client/plan')) {
				$audit[] = array(
						'name'	   => $this->language->get('text_plan'),
						'href'     => $this->url->link('client/plan','', true),
						'children' => array()
					);
			}
			if ($this->document->hasPermission('access', 'client/dashboard')) {
				$audit[] = array(
					'name'	   => $this->language->get('text_audit_chart'),
					'href'     => $this->url->link('client/dashboard', 'filter_round='.$filter_round, true),
					'children' => array()
				);
			}
			/*
			if ($this->document->hasPermission('access', 'chart/morris')) {
				$audit[] = array(
					'name'	   => $this->language->get('text_audit_chart'),
					'href'     => $this->url->link('chart/morris', 'filter_round='.$filter_round, true),
					'children' => array()
				);
			}*/ 
			if ($this->document->hasPermission('access', 'client/map')) {
				$audit[] = array(
					'name'	   => $this->language->get('text_plan_map'),
					'href'     => $this->url->link('client/map', 'filter_round='.$filter_round, true),
					'children' => array()
				);
			}
			/*
			//$isNationwide==1&&
			if ($this->document->hasPermission('access', 'client/coupon_history')) {
				$audit[] = array(
					'name'	   => $this->language->get('text_coupon_history'),
					'href'     => $this->url->link('client/coupon_history', '', true),
					'children' => array()
				);
			}
			
			
			
			if (($this->user->isLogged()||in_array($customer_id,$config_check_coupon_user_ids))&&$isNationwide==1&&$this->document->hasPermission('access', 'distributor/coupon_history')) {
				$audit[] = array(
					'name'	   => $this->language->get('text_check_coupon'),
					'href'     => $this->url->link('distributor/coupon_history', '', true),
					'children' => array()
				);
			}
			*/ 
			if ($isNationwide==1&&$this->document->hasPermission('access', 'client/online_history')) {
				$audit[] = array(
					'name'	   => $this->language->get('text_online_history'),
					'href'     => $this->url->link('client/online_history', '', true),
					'children' => array()
				);
			}
			
			if ($audit) {
				$data['menus'][] = array(
					'id'       => 'menu-audit',
					'icon'	   => 'fa-calendar',
					'name'	   => $this->language->get('text_audit_info'),
					'href'     => '',
					'children' => $audit
				);
			}
			/*
			// Distributor
			$distributor = array();
			if ($this->document->hasPermission('access', 'distributor/coupon_history')) {
				$distributor[] = array(
					'name'	   => $this->language->get('text_check_coupon'),
					'href'     => $this->url->link('distributor/coupon_history', '', true),
					'children' => array()
				);
			}
			if (!$this->customer->isLogged()&&$distributor) {
				$data['menus'][] = array(
					'id'       => 'menu-distributor',
					'icon'	   => 'fa-calendar',
					'name'	   => $this->language->get('text_distributor_area'),
					'href'     => '',
					'children' => $distributor
				);
			}*/ 
		}

		$template = $this->config->get('config_nav');//'nav_top'
		return $this->load->view('common/'.$template, $data);
	}
}