<?php
class ControllerCommonUserInfo extends Controller {
	public function index() {
		$languages = $this->load->language('common/user_info');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		

		$data['user_logged'] = $this->user->isLogged();
		if($this->user->isLogged()){
			$this->load->model('tool/image');
			$img_folder = 'files/users/' . $this->user->getUserName().'/';
			$image =  $img_folder.$this->user->getImage();
			if (is_file(DIR_MEDIA . $image)) {
				$image= $this->model_tool_image->resize($image, 100, 100);
			} else {
				$image = $this->model_tool_image->resize('avatar.jpg', 100, 100);
			}
			
			$data['user'] = array(
				'fullname' =>$this->user->getFullname(),
				'username' =>$this->user->getUserName(),
				'group' =>$this->user->getGroup(),
				'image' =>$image,
				'edit' =>$this->url->link('user/user', '', true),
				'logout' =>$this->url->link('user/logout',  '', true),
			);
		}else{
			$data['user'] = array();
		}
		
		return $this->load->view('common/user_info', $data);
	}
}