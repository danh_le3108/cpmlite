<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$data = array();
		$meta_title = $this->config->get('config_meta_title');
		$this->document->setTitle($meta_title);
		$this->document->setDescription($this->config->get('config_meta_title'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		// Whos Online - Chạy ở home và account nha
		if ($this->config->get('config_history_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}
			if (isset($this->request->server['HTTP_USER_AGENT'])) {
				$user_agent= $this->request->server['HTTP_USER_AGENT'];
			} else {
				$user_agent = '';
			}
				
			if($this->customer->isLogged()){
				$username = $this->customer->getUserName();
			}else{
				$username = $this->user->getUserName();
			}
			$this->model_tool_online->addOnline($ip,$this->user->getId(),$this->customer->getId(),$username, $url, $referer,$user_agent);
		}
		$template = 'common/home';
		$this->document->setBreadcrumbs($data['breadcrumbs']);
		return $this->load->controller('startup/builder',$this->load->view($template, $data));

	}
}
