<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');
		
		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');

		$data['text_footer'] = $this->language->get('text_footer');
		$data['text_version'] = sprintf($this->language->get('text_version'), VERSION);

		$data['http_server'] = HTTP_SERVER;
		
		return $this->load->view('common/footer', $data);
	}
}