<?php
class ControllerApiGetItems extends Controller {
	public function index() {
		$json = array();
		$json['api_name'] = 'get_items';
		// $json['reason_id_show_comment'] = 7;
		$this->load->model('catalog/attribute');
		$attrs = array();
		foreach ($this->model_catalog_attribute->getAttributesByCode('MERCHANDISING') as $key => $value) {
			$attrs[] = array(
				'item_id'=>$value['attribute_id'],
				'item_name'=>$value['attribute_name']
			);
		}
		$json['data'] = $attrs;
		$json['status'] = 1;
		$json['message'] = 'Load Attributes Success';

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->response->setOutput(json_encode($json));

	}
}
