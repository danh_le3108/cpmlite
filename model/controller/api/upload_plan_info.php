<?php
class ControllerApiUploadPlanInfo extends Controller {
	public function index() {
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}
		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->log->write('Upload plan info: ' . json_encode($this->request->post));
		
		$json = array(
			'api_name' => 'upload_plan_info',
			'status' => 0,
			'message' => '',
			'data' => array()
		);	

		$data = array(
			'post' => $this->request->post,
			'require' => array('api_key', 'user_id', 'plan_id', 'total_image')
		);
		$validation = $this->load->controller('api/validation', $data);
		if ($validation['status'] == 0) {
			$json['message'] = $validation['message'];
			return $this->response->setOutput(json_encode($json));
		}

		$post = $this->request->post;
		$this->load->model('plan/plan');
		$plan_id = $post['plan_id'];
		$user_id = $post['user_id'];
		$plan = $validation['plan'];	
 		$user = $validation['user'];

		$plan_rating = '';
		if (isset($post['rating']) && $post['rating'] == 0) {
			$plan_rating = 1;
		} elseif (isset($post['rating']) && $post['rating'] == 1) {
			$plan_rating = -2;
		}
		if ($plan_rating == '') {
			$json['message'] = "Upload info lỗi! (plan_id = {$plan_id})";
			return $this->response->setOutput(json_encode($json));
		}

		$this->log->write("Hình thực hiện plan_id = $plan_id : " . json_encode(array('capture_total' => $plan['capture_total'], 'total_image' => $post['total_image'], 'total_image_uploaded' => $post['total_image_uploaded'])));
		if ($post['total_image'] != $post['total_image_uploaded']) {
			$image_missed = $post['total_image'] - $post['total_image_uploaded'];
			$json['message'] = "Tải thiếu {$image_missed} hình ảnh. Vui lòng kiểm tra đường truyền và upload lại!  (plan_id = {$plan_id})";
			return $this->response->setOutput(json_encode($json));
		}
			
		$reason = array();
		if (!empty($post['ktc_l1'])) {
			$reason[] = $post['ktc_l1'];
		} 
		if (!empty($post['ktc_l2'])) {
			$reason[] = $post['ktc_l2'];
		} 
		$data = array(
			'note' => $plan_rating == 1 ? $post['note'] : '',
			'reason_id' => implode(',', $reason),
			'note_ktc' => $plan_rating == -2 ? $post['note'] : '',
			'plan_rating' => $plan_rating,
			'problem' => $post['problem'],
			'feedback' => $post['feedback']
		);

		if ($plan['plan_status'] != 1 && $plan['is_fix'] == 0) { 
 			if ($plan_rating == 1) {
				// if ($plan['time_upload_attr'] == '0000-00-00 00:00:00') {
				// 	$json['message'] = "Nhập liệu chưa được upload. Vui lòng kiểm tra đường truyền và upload lại!  (plan_id = {$plan_id})";
				// 	return $this->response->setOutput(json_encode($json));
				// }
				// if ($plan['time_upload_audio'] == '0000-00-00 00:00:00') {
				// 	$json['message'] = "Ghi âm chưa được upload. Vui lòng kiểm tra đường truyền và upload lại!  (plan_id = {$plan_id})";
				// 	return $this->response->setOutput(json_encode($json));
				// }
			}
			$data['plan_rating'] = $plan_rating;
			$data['time_upload'] = date('Y-m-d H:i:s');
		} 

		if ($plan['is_fix'] > 0) {
			$data['date_fixed'] = date('Y-m-d H:i:s');
		}

		$this->model_plan_plan->updatePlan($plan_id, $data);
		// $this->log->write($data);
		// p(1,1);
		$json['status'] = 1;
		$json['message'] = 'Success.';
		return $this->response->setOutput(json_encode($json));
	}
}
