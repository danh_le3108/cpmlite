<?php
class ControllerApiGetCallFields extends Controller {
	public function index() {
		$json = array(
			'api_name' => 'get_call_fields',
			'data' => array(
				array(
					'name' => 'Gọi ai?',
					'field' => 'call_recipient',
					'input' => 'text'
				),
				array(
					'name' => 'Số điện thoại',
					'field' => 'call_phone_number',
					'input' => 'number'
				),
				array(
					'name' => 'Tình trạng',
					'field' => 'call_note',
					'input' => 'text'
				),
				array(
					'name' => 'Số lần gọi',
					'field' => 'call_num',
					'input' => 'number'
				),
				array(
					'name' => 'Ngày gọi',
					'field' => 'call_date',
					'input' => 'date_picker'
				)
			),
 			'status' => 1,
			'message' => 'success'
		);
		
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->response->setOutput(json_encode($json));

	}
}