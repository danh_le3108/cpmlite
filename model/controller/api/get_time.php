<?php
class ControllerApiGetTime extends Controller {
	public function index() {
		$json = array();
		$json['api_name'] = 'get_time';
		
		$this->load->language('api/api');
		$json['data'] = time();
		$json['status'] = 1;
		$json['message'] = 'Get time success';
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}
}
