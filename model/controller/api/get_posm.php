<?php
class ControllerApiGetPosm extends Controller {
	public function index() {
		
		$this->load->model('catalog/attribute');
		$attr = $this->model_catalog_attribute->getAttribute(7);
		$attr['options'] = explode('|', $attr['options']);
		$posm = array();
		foreach ($attr['options'] as $key => $val) {
			$key += 1;
			$posm[] = array(
				'id' => $key,
				'value' => $val
			);
		}

		$json = array(
			'api_name' => 'get_posm',
			'data' => $posm,
			'status' => 1,
			'message' => 'Success'
		);

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->response->setOutput(json_encode($json));

	}
}