<?php
class ControllerApiLogError extends Controller {
	private $limit = 100;
	public function index() {
		$json = array();
		$json['api_name'] = 'log_error';
		
		// $this->log->write('log_error: ' . json_encode($this->request->post));
		if(!isset($this->request->post['api_key'])){
			$json['status'] = 0;
			$json['message'] = $this->language->get('text_error_key');
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}


		if (isset($this->request->post['api_key'])&&trim($this->request->post['api_key']) ==$this->config->get('config_api')&&isset($this->request->post['api_key'])&&isset($this->request->post['app_version'])&&isset($this->request->post['android_version'])&&isset($this->request->post['error'])) {
				
				
			$user_id = $this->request->post['user_id'];
			$app_version = isset($this->request->post['app_version'])?$this->request->post['app_version']:'';
			$android_version = isset($this->request->post['android_version'])?$this->request->post['android_version']:'';

			$error_message = isset($this->request->post['error'])?$this->request->post['error']:'';
			$model = isset($this->request->post['model'])?$this->request->post['model']:'';
				
				$user_ip = isset($this->request->server['REMOTE_ADDR'])?$this->request->server['REMOTE_ADDR']:'';
			
			$log_type = isset($this->request->post['log_type'])?$this->request->post['log_type']:'';
			$sql = "INSERT INTO " . DB_PREFIX . "app_log SET user_id = '" . (int)$user_id . "', user_ip = '" . $this->db->escape($user_ip) . "', app_version= '" . $this->db->escape($app_version) . "', android_version= '" . $this->db->escape($android_version) . "', error_message = '" . $this->db->escape($error_message) . "', model = '" . $this->db->escape($model) . "', log_type = '" . $this->db->escape($log_type) . "',date_added = NOW()";
											
			$this->pdb->query($sql);			
			$json['status'] = 1;
			$json['message'] = $this->language->get('Ghi lỗi thành công!');
				
		} else {
			$json['status'] = 0;
			$json['message'] = $this->language->get('text_error_key');
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->response->setOutput(json_encode($json));

	}
	
	public function errors() {
		$languages = $this->load->language('common/header');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		
		
		$meta_title = $this->config->get('config_meta_title');
		$this->document->setTitle($meta_title);
		$this->document->setDescription($this->config->get('config_meta_title'));
		
		
		
		$this->load->model('project/project_user');
		$user = $this->model_project_project_user->getProjectUsers();
		
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'date_added';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '' , true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('Error Log'),
			'href' => $this->url->link('api/log_error/errors',  $url , true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);
		
		
		$filter_data = array(
			'log_type' => isset($this->request->get['filter_log_type']) ? $this->request->get['filter_log_type'] : '',
			'sort'                    => $sort,
			'order'                   => $order,
			'start'                   => ($page - 1) * $this->limit,
			'limit'                   => $this->limit
		);
			
		$errors_total = $this->getTotalErrors($filter_data);
		$results = $this->getErrors($filter_data);
		
		$data['filter_log_type'] = isset($this->request->get['filter_log_type']) ? $this->request->get['filter_log_type'] : '';
		$data['errors'] = array();
		
		foreach ($results as $result) {
			$result['usercode'] = isset($user[$result['user_id']])?$user[$result['user_id']]['usercode']:'???';
			$data['errors'][] = $result;
			
		}
		$url = '';


		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $errors_total;
		$pagination->page = $page;
		$pagination->limit = $this->limit;
		$pagination->url = $this->url->link('api/log_error/errors', $url . '&page={page}' , true);

		$data['pagination'] = $pagination->render();

		$data['results'] = $pagination->results();

		$data['sort'] = $sort;
		$data['order'] = $order;
		
		
		
		$this_template = 'api/errors';
		return $this->load->controller('startup/builder',$this->load->view($this_template, $data));
	}
	
	public function clear_contains(){
		$languages = $this->load->language('common/header');
		$json = array();
		
		if(($this->request->server['REQUEST_METHOD'] == 'POST')&&$this->request->post['hidden_key']=='banatuan'&&isset($this->request->post['clear_contains'])&&!empty($this->request->post['clear_contains'])) {
			
			$this->pdb->query("DELETE FROM " . PDB_PREFIX . "app_log WHERE error_message LIKE '%" . $this->db->escape($this->request->post['clear_contains']) . "%'");

			$json['success'] = $this->language->get('Xóa thành công!');
			$json['redirect'] = str_replace('&amp;', '&', $this->url->link('api/log_error/errors', '', true));
		}else{
			$json['error'] =  $this->language->get('Bạn không có quyền xóa!');
			
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function clear(){
		$languages = $this->load->language('common/header');
		$json = array();
		
		if(($this->request->server['REQUEST_METHOD'] == 'POST')&&$this->request->post['hidden_key']=='banatuan') {
			$this->pdb->query("TRUNCATE " . PDB_PREFIX . "app_log");

			$json['success'] = $this->language->get('Xóa thành công!');
			$json['redirect'] = str_replace('&amp;', '&', $this->url->link('api/log_error/errors', '', true));
		}else{
			$json['error'] =  $this->language->get('Bạn không có quyền xóa!');
			
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	private function getErrors($data = array()){
		$sql = "SELECT * FROM " . PDB_PREFIX . "app_log WHERE 1";
		if (isset($data['log_type']) && $data['log_type'] != '') {
			$sql .= ' AND log_type = ' . (int)$data['log_type'];
		}
		$sql .= ' ORDER BY date_added DESC ';
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 50;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		$query = $this->pdb->query($sql);
		// p($sql,1);
		$query_data = $query->rows;

		return $query_data;
	}
	private function getTotalErrors($data = array()){
		
		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "app_log WHERE 1";
		if (isset($data['log_type']) && $data['log_type'] != '') {
			$sql .= ' AND log_type = ' . (int)$data['log_type'];
		}
		$query = $this->pdb->query($sql);
		
		return $query->row['total'];
		
	}
	
}
