<?php
class ControllerApiUpdateStatus extends Controller {
	public function index() {
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}
		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->log->write('Update status: ' . json_encode($this->request->post));
		$json = array(
			'api_name' => 'update_status',
			'status' => 0,
			'message' => '',
			'data' => array()
		);	

		$data = array(
			'post' => $this->request->post,
			'require' => array('api_key', 'user_id', 'plan_id')
		);
		$validation = $this->load->controller('api/validation', $data);
		if ($validation['status'] == 0) {
			$json['message'] = $validation['message'];
			return $this->response->setOutput(json_encode($json));
		}

		$post = $this->request->post;
		$this->load->model('plan/plan');
		$plan_id = $post['plan_id'];
		$user_id = $post['user_id'];
		$plan = $validation['plan'];	
 		$user = $validation['user'];

		if ($plan['time_upload'] == '0000-00-00 00:00:00' && $plan['plan_status'] == 0) {
			$json['message'] = "Đường truyền không ổn định, dữ liệu chưa lên hết, vui lòng kiểm tra đường truyền và upload lại! (time_upload plan_id = {$plan_id})";
			return $this->response->setOutput(json_encode($json));
		}

		$data = array(
			'plan_status' => 1,
			'upload_count' => $plan['upload_count'] + 1,
			'is_fix' => $plan['is_fix'] == 0 ? 0 : 2
		);
		$this->model_plan_plan->updatePlan($plan_id, $data);
		
		$json['status'] = 1;
		$json['message'] = 'Success.';
		return $this->response->setOutput(json_encode($json));
	}
}
