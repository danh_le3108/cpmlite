<?php
class ControllerApiFcmGetNotification extends Controller {
  public function index(){
    $json = array();
/*
    if(!isset($this->request->post['api_key'])){
      $json['status'] = 0;
			$json['message'] = $this->language->get('text_error_key');
		}
*/ 
    if(!isset($this->request->post['user_id'])){
      $json['status'] = 0;
		$json['message'] = $this->language->get('Không có user này!');
	}
/*
    if(trim($this->request->post['api_key']) != trim($this->config->get('config_api'))){
      	$json['status'] = 0;
		$json['message'] = "Lỗi api key";
    }
$this->log->write('FcmGetNotification: '.json_encode($this->request->post)); 
*/ 
    if(!$json){
      $this->load->model('fcm/fcm');
      $json['data'] = $this->model_fcm_fcm->getNotificationByUserId($this->request->post['user_id']);
      $json['status'] = 1;
      $json['message'] = "Success";
    }

//$this->log->write('Fcm Data: '.json_encode($json)); 


    if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
  }
}
?>
