<?php
class ControllerApiCheckCoupon extends Controller {
	public function index() {
		$json = array();
		$json['api_name'] = 'check_coupon';
		
		
		if(!isset($this->request->post['api_key'])){
			$json['status'] = 0;
			$json['message'] = $this->language->get('text_error_key');
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}
			
		$error = array();
		$before = array();
		$after = array();
		
				$uids = array(1);
		
			$json['config_upload_coupon_id'] = $this->config->get('config_coupon_valid');
	
		if (isset($this->request->post['api_key'])&&trim($this->request->post['api_key']) ==$this->config->get('config_api')&&isset($this->request->post['user_id'])) {
			
			//$this->log->write('CheckCoupon: '.json_encode($this->request->post)); 
			
			
			$this->load->model('plan/plan');
			$this->load->model('catalog/coupon');
			$plan_id = $this->request->post['plan_id'];
			
			$json['user_id'] = $user_id = $this->request->post['user_id'];
			$plan_info = $this->model_plan_plan->getPlanInfo($plan_id);
			
			$store_code = isset($this->request->post['store_code'])?$this->request->post['store_code']:$plan_info['store_code'];
			$store_code = str_replace(array('?',' '),array('',''),trim($store_code));  
			$json['plan_id'] = $plan_id;
			$json['store_code'] = $store_code;
			
			
			$store_code = str_replace(array('?',' '),array('',''),trim($store_code)); 
			
			$plan_coupon = $this->model_plan_plan->getPlanCoupons($plan_id,$plan_info['import_id'],'coupon_history_id');
			
			$this->load->model('catalog/coupon_prefix');
			$prefix =  $this->model_catalog_coupon_prefix->getPrefixsIndexBy('coupon_prefix');
			
			$this->load->model('catalog/coupon_status');
			$cstatus = $this->model_catalog_coupon_status->getCouponStatusesIndexBy('coupon_status_id');
			
				
				
				if(!empty($uids)&&in_array($user_id,$uids)){
					$this->log->write($plan_id.'Nhóm  AAA'.$user_id.'Check Coupon: '); 
							$json['status'] = 1;
							$json['message'] = $this->language->get('Thành công!');
				}else {
					
			if(isset($this->request->post['coupons'])&&$user_id==$plan_info['user_id']&&(int)$plan_info['is_deleted']==0){
				
				$coupon_data = array();
				$coupons = json_decode(html_entity_decode($this->request->post['coupons']),true);
				if(is_array($coupons)){
					foreach ($coupons as $coupon){
						if(isset($coupon['coupon_history_id'])){
							
							if(!empty($coupon['coupon_code'])){
								$before[] = $coupon['coupon_history_id'];
								$after[$coupon['coupon_code']] = $coupon['coupon_history_id'];
							}
							$coupon_history_id = $coupon['coupon_history_id'];
							$this->model_plan_plan->resetCoupon($coupon_history_id);
						}
					}
					foreach ($coupons as $coupon){
						if(isset($coupon['coupon_history_id'])){
							$coupon_history_id = $coupon['coupon_history_id'];
							$coupon['coupon_code'] = strtoupper($coupon['coupon_code']);
							
							$coupon['coupon_status_id'] = 0;
							
							$coupon_info = $this->model_catalog_coupon->getCouponByCode($coupon['coupon_code']);
							if(empty($coupon_info['coupon_id'])){
								$coupon['coupon_status_id'] = $this->config->get('config_coupon_not_exist');
							}else{
								
								$coupon_duplicate = $this->model_plan_plan->checkCouponDuplicate($plan_id,$coupon_history_id, $coupon['coupon_code']);
								
								
								$coupon_double = $this->model_plan_plan->checkCouponDouble($coupon_history_id, $coupon['coupon_code']);
								/*
								if(!empty($coupon_info['coupon_id'])&&$coupon_info['distributor_code']!=$plan_info['distributor_code']){
									$coupon['coupon_status_id'] = $this->config->get('config_coupon_wrong_distributor');
								}else 
								*/ 
								
								$coupon_prefix = substr($coupon['coupon_code'], 0, 2);
									
								if($coupon_prefix != $plan_coupon[$coupon_history_id]['coupon_prefix']||$coupon_prefix != $coupon_info['coupon_prefix']){
									$coupon['coupon_status_id'] = $this->config->get('config_coupon_not_exist');
								}
								
								else if(!empty($coupon_info['coupon_id'])&&$coupon_info['sup_id']!=$plan_info['sup_id']){
									$coupon['coupon_status_id'] = $this->config->get('config_coupon_wrong_sup');
								}
								else if(!empty($coupon_info['coupon_id'])&&$coupon_info['coupon_status_id']==$this->config->get('config_coupon_has_deleted')){
									$coupon['coupon_status_id'] = $this->config->get('config_coupon_has_deleted');
								}
								else if($coupon['coupon_status_id'] ==0&&$coupon_duplicate==1){
									$coupon['coupon_status_id'] = $this->config->get('config_coupon_duplicate');
									$coupon['coupon_id'] = 0;
								} 
								
								else if($coupon['coupon_status_id'] ==0&&$coupon_double==1){
									$coupon['coupon_status_id'] = $this->config->get('config_coupon_duplicate');
									$coupon['coupon_id'] = 0;
									$coupon['double_id'] = $coupon_info['plan_id'];
								}else if($coupon['coupon_status_id'] ==0&&$coupon_double==0){
									$coupon['coupon_status_id'] = $this->config->get('config_coupon_valid');
									$coupon['coupon_id'] = $coupon_info['coupon_id'];
								}
							
							}
							if(isset($cstatus[$coupon['coupon_status_id']])){	
								$coupon['coupon_status_id'] = $cstatus[$coupon['coupon_status_id']]['coupon_status_id'];
								$coupon['message'] = $cstatus[$coupon['coupon_status_id']]['coupon_status_name'];
							}else{
								$coupon['coupon_status_id'] = 0;
								$coupon['message'] = '';
							}
							
							
							$this->model_plan_plan->updatePlanCoupon($coupon_history_id,$coupon);
						}
					}
				}
					
					$count_before = count($before);
					$count_after = count($after);
					
					$error['before'] = $count_before;
					$error['after'] = $count_after;
					
			
				if($count_before==$count_after){
					$json['status'] = 1;
					$json['message'] = $this->language->get('Thành công!');
				}else{
					$json['status'] = 0;
					$json['message'] = $this->language->get('Không được nhập trùng mã!');
				}
			}else if(isset($plan_info['plan_id'])&&($plan_info['user_id']!=$user_id||(int)$plan_info['is_deleted']==1)){
					$json['status'] = 0;
					$json['message'] = 'Nội dung thực hiện của CH này đã thay đổi từ hệ thống, vui lòng upload những cửa hàng bạn đã thực hiện hôm nay sau đó tải lại danh sách hoặc liên hệ Admin!';
			}else{
				$json['status'] = 0;
				$json['message'] = $this->language->get('Plan này không phải của bạn!');
			}
		}
			
			$json['coupons'] = $this->model_plan_plan->getApiPlanCoupons($plan_id,$plan_info['import_id'],1);	
			
		} else {
			$json['status'] = 0;
			$json['message'] = $this->language->get('text_error_key');
		}
		
			
		
		
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->response->setOutput(json_encode($json));

	}
	
}
