<?php
class ControllerApiGetAttributes extends Controller {
	public function index() {
		$json = array();
		$json['api_name'] = 'get_attributes';
		// $json['reason_id_show_comment'] = 7;
		$this->load->model('catalog/attribute');
		$this->load->model('catalog/group');
		$groups = $this->model_catalog_group->getGroups(array('hide_group' => 0,'parent_group_id >' => 1,'sort'=>'sort_order'));
		foreach ($groups as $group) {
			$attributes = $this->model_catalog_attribute->getAttributes(array('group_id'=>$group['group_id'],'sort'=>'a.sort_order','a.hide'=>0));
			$attrs = array();
			foreach ($attributes as $key => $value) {
				if ($value['input'] == 'select') {
					$value['options'] = json_decode($value['options']);
				}
				$attrs[] = $value;
			}
			$group['childs'] = $attrs;
			$json['data'][] = $group;
		}
		$json['status'] = 1;
		$json['message'] = 'Load Attributes Success';

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->response->setOutput(json_encode($json));

	}
}
