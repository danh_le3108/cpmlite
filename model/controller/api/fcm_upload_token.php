<?php
class ControllerApiFcmUploadToken extends Controller {
  public function index(){
    $json = array();

//$this->log->write('ControllerApiFcmUploadToken: '.json_encode($this->request->post)); 

    if(!isset($this->request->post['user_id'])){
      $json['status'] = 0;
		$json['message'] = $this->language->get('Không có user này!');
	}
	
    if(isset($this->request->post['user_id'])&&isset($this->request->post['google_token'])){	
		$device_type = isset($this->request->post['device_type'])?$this->request->post['device_type']:1; 
		$google_token = $this->request->post['google_token']; 
		$user_id = $this->request->post['user_id']; 
		
		$sql = "UPDATE " . DB_PREFIX . "project_user SET 
		google_token = '".$this->db->escape($google_token)."', 
		device_type = '".(int)$device_type."' WHERE user_id = '" . (int)$user_id. "' AND project_id ='" . (int)$this->config->get('config_project_id'). "'";
		//$this->log->write($sql); 
		$this->db->query($sql);
		
		
      $json['status'] = 1;
		$json['message'] = $this->language->get('Success!');
	}

    if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
  }
}
?>
