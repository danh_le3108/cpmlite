<?php
class ControllerApiGetOrders extends Controller {
	public function index() {
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}
		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$json = array(
			'api_name' => 'get_orders',
			'message' => '',
			'status' => 0,
			'data' => ''
		);

		$require = array(
			'api_key', 'user_id', 'plan_id'
		);
		$post = $this->request->post;
		
		foreach ($require as $key) {
			if (empty($post[$key])) {
				$json['message'] = 'Thiếu ' . $key;
				return $this->response->setOutput(json_encode($json));
			} elseif ($key == 'api_key' && $post['api_key'] != $this->config->get('config_api')) {
				$json['message'] = 'Sai ' . $key;
				return $this->response->setOutput(json_encode($json));
			} elseif ($key == 'user_id') {
				if (!is_numeric($post['user_id'])) {
					$json['message'] = 'Nhân viên không có trong dự án!';
					return $this->response->setOutput(json_encode($json));
				} 
				$this->load->model('project/project_user');
				$user = $this->model_project_project_user->getUser($post['user_id']);
				if (!$user || $user['user_status'] != 1) {
					$json['message'] = 'Nhân viên không có trong dự án!';
					return $this->response->setOutput(json_encode($json));
				}
			} 
			// elseif ($key == 'version' && !in_array($post['version'],$this->config->get('config_app_versions'))) {
			// 	$json['message'] = 'Bạn đang sử dụng app cũ. Vui lòng cập nhật app mới!';
			// 	return $this->response->setOutput(json_encode($json));
			// }
		}
		
		$this->load->model('plan/plan');
		$user_id = $post['user_id'];
		$plan_id = $post['plan_id'];
		$filter = array(
			'plan_id' => $plan_id,
			'user_id' => $user_id
		);
		$orders = $this->model_plan_plan->getPlanOrder($filter);
		$this->load->model('catalog/attribute');
		$info = $this->model_catalog_attribute->getAttributes(array('g.group_id'=>1,'sort'=>'a.sort_order'));
		$customer = $this->model_catalog_attribute->getAttributes(array('g.group_id'=>2,'sort'=>'a.sort_order'));
		
		foreach ($orders as &$order) {
			$order['detail']['info'] = array();
			foreach ($info as $attr) {
				$attribute_code = $attr['attribute_code'];
				$order['detail']['info'][] = array(
					'attribute_id' => $attribute_code,
					'attribute_name' => $attr['attribute_name'],
					'value' => !empty($order[$attribute_code]) ? $order[$attribute_code] : '',
					'parent_name' => !empty($attr['parent_name']) ? $attr['parent_name'] : '',
					'input' => $attr['input'],
					'options' => $attr['input'] == 'select' && $attribute_code != 'district' ? json_decode($attr['options']) : $attr['options'],
					'label' => '',
					'price' => $attr['price'],
					'sort_order' => $attr['sort_order']
				);
			}

			$order['detail']['customer'] = array();
			$order_details = $this->model_plan_plan->getPlanOrderDetail(array('o.order_id'=>$order['order_id']));
			foreach ($order_details as $od) {
				$cus = array();
				$images = $this->getImages($od['detail_id']);
				foreach ($customer as $attr) {
					$attribute_code = $attr['attribute_code'];
					$cus[] = array(
						'attribute_id' => $attribute_code,
						'attribute_name' => $attr['attribute_name'],
						'value' => $attribute_code == 'image' ? $images : $od[$attribute_code],
						'parent_name' => !empty($attr['parent_name']) ? $attr['parent_name'] : '',
						'input' => $attr['input'],
						'options' => $attr['input'] != 'select' ? $attr['options'] : json_decode($attr['options']),
						'label' => '',
						'price' => $attr['price'],
						'sort_order' => $attr['sort_order']
					);
				}
				$order['detail']['customer'][] = $cus;
			}
			unset($order);
		}
		$json['data'] = $orders;
		$json['status'] = 1;
		$json['message'] = 'Success.';
		return $this->response->setOutput(json_encode($json));
	}

	public function getImages($detail_id = 0) {
		$sql = "SELECT app_file_name FROM cpm_plan_images WHERE detail_id = {$detail_id}";
		$imgs = $this->pdb->query($sql)->rows;
		$data = array();
		foreach ($imgs as $v) {
			$data[] = $v['app_file_name'];
		}
		return $data;
	}
}
