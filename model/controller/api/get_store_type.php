<?php
class ControllerApiGetStoreType extends Controller {
	public function index() {
		$json = array();
		$json['api_name'] = 'get_store_type';
		
		
		$this->load->model('store/store_type');
		$json['data'] = $this->model_store_store_type->getStoreTypes();
		
		// $json['config_checkin_id'] = $this->config->get('config_image_overview');
		
		
		$json['status'] = 1;
		$json['message'] = 'Load Store Type Success';

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->response->setOutput(json_encode($json));

	}
}