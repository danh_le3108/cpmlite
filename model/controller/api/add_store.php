<?php
class ControllerApiAddStore extends Controller {
	public function index() {
		$json = array(
			'api_name' => 'add_store',
			'status' => 0,
			'message' => '',
			'data' => array()
		);	

		$post = $this->request->post;
		$post['data'] = json_decode(html_entity_decode(str_replace(array('“','”'),array('"','"'),$post['data']), ENT_QUOTES, 'UTF-8'),true);
		foreach ($post['data'] as $key => $value) {
			$post[$key] = $value;
		}

		$this->log->write('Add_store: '.json_encode($post));
		$data = array(
			'post' => $post,
			'require' => array('api_key', 'user_id')
		);
		$validation = $this->load->controller('api/validation', $data);
		if ($validation['status'] == 0) {
			$json['message'] = $validation['message'];
			return $this->response->setOutput(json_encode($json));
		}

		
		$requires = array(
			'chanel' => 'Kênh',
			'store_type_id' => 'Loại cửa hàng',
			'province_id' => 'Tỉnh/ Thành Phố',
			'store_district' => 'Quận/ Huyện',
			'store_ward' => 'Phường/ Xã'
		);

		$address_raw = array(
			'store_address',
			'store_place',
			'store_ward',
			'store_district'
		);

		
		$store_address_raw = array();
		foreach ($requires as $key => $msg) {
			if (empty($post[$key])) {
				$json['message'] = 'Chưa nhập ' . $msg;
					return $this->response->setOutput(json_encode($json));
			} 
		}
		$this->load->model('localisation/location');
		foreach ($address_raw as $key) {
			if (!empty($post[$key])) {
				if ($key == 'store_district' && is_numeric($post[$key])) {
					$district = $this->model_localisation_location->getDistrict($post[$key]);
					if (!empty($district)) {
						$post[$key] = $district['district_name'];
					}
				} elseif ($key == 'store_ward' && is_numeric($post[$key])) {
					$ward = $this->model_localisation_location->getWard($post[$key]);
					if (!empty($ward)) {
						$post[$key] = $ward['name'];
					}
				}
				$store_address_raw[] = $post[$key];
			}
		}
		
		$this->load->model('localisation/province');
		$province = $this->model_localisation_province->getProvince($post['province_id']);
		if (empty($province)) {
			$json['message'] = "Tỉnh thành không tồn tại. (province_id = {$post['province_id']})";
			return $this->response->setOutput(json_encode($json));
		}
		$store_address_raw[] = $province['name'];
		// $store_address_raw[] = 'Hồ Chí Minh';
		$user_id = $post['user_id'];
		$user = $validation['user'];
		$store_data = array(
			'store_name' => isset($post['store_name']) ? $post['store_name'] : '',
			'store_owner' => isset($post['store_owner']) ? $post['store_owner'] : '',
			'store_address' => isset($post['store_address']) ? $post['store_address'] : '',
			'store_place' => isset($post['store_place']) ? $post['store_place'] : '',
			'store_ward' => isset($post['store_ward']) ? $post['store_ward'] : '',
			'store_district' => isset($post['store_district']) ? $post['store_district'] : '',
			'store_phone' => isset($post['store_phone']) ? $post['store_phone'] : '',
			'province_id' => $province['province_id'],
			'store_province' => $province['name'],
			'store_address_raw' => implode(', ', $store_address_raw),
			'store_type_id' => $post['store_type_id'],
			'chanel' => $post['chanel'],
			// 'region_code' => $region_code,
			'manual' => 1,
			'user_added' => $user['usercode']
		);
		$this->load->model('store/store');
		$store_id = $this->model_store_store->addStore($store_data);
		$store_code = 'PTUN_'.sprintf("%05d", $store_id);
		$this->model_store_store->updateStore($store_id, array('store_code' => $store_code));
		
		$config_project_folder = $this->config->get('config_project_folder');
		$plan_data = array(
			'round_name' => date('Y-m'),
			'plan_name' => date('Ym'),
			'date_start' => date('Y-m-d'),
			'date_end' => '2020-01-01',
			'store_id' => $store_id,
			'store_code' => $store_code,
			'user_id' => $post['user_id'],
			'usercode' => $user['usercode'] . ' - ' . $user['fullname'],
			'image_path' => 'files/'.$config_project_folder.'/'.date('Y-m').'/'. $store_code.'/',
		);
		
		$this->load->model('plan/plan');
		$plan_id = $this->model_plan_plan->addPlan($plan_data);
		$plan = $this->model_plan_plan->getPlan($plan_id);
		
		$this->load->model('store/store_type');
		$store_types = $this->model_store_store_type->getAllTypes();
		$this->load->model('tool/image');
		$json['data'] = array(
			'plan_id' => $plan['plan_id'],
			'is_fix' => 0,
			'fix_notes' => array(),
			'round_name' => $plan['round_name'],
			'plan_name' => $plan['plan_name'],
			'plan_status' => 0,
			'plan_rating' => 0,
			'upload_count' => $plan['upload_count'],
			'date_start' => $plan['date_start'],
			'date_end' => $plan['date_end'],
			'user_id' => $plan['user_id'],
			'import_id' => $plan['import_id'],
			'store_id' => $plan['store_id'],
			'store_code' => $plan['store_code'],
			'store_name' => $plan['store_name'],
			// 'pepsi_code' => $plan['pepsi_code'],
			'store_type_id' => $plan['store_type_id'], 
			'store_type' => isset($store_types[$plan['store_type_id']]) ? $store_types[$plan['store_type_id']]['type_name'] : '',
			'store_owner' => $plan['store_owner'],
			'store_address' => $plan['store_address'],
			'store_place' => $plan['store_place'],
			'store_ward' => $plan['store_ward'],
			'store_district' => $plan['store_district'],
			'store_province' => $plan['store_province'],
			'province_id' => $plan['province_id'],
			'region_code' => $plan['region_code'],
			'store_address_raw' => $plan['store_address_raw'],
			'store_phone' => $plan['store_phone'],
			'latitude' => $plan['store_latitude'],
			'longitude' => $plan['store_longitude'],
			'store_latitude' => $plan['store_latitude'],
			'store_longitude' => $plan['store_longitude'],
			'store_image' => $this->model_tool_image->best_fit('store.jpg', 300,300),
			'target_posm' => '',
			'cctb_number' => ''
		);

		$json['status'] = 1;
		$json['message'] = 'Success.';
		return $this->response->setOutput(json_encode($json));
	}

	public function getRegion($province_id) {
		$sql = "SELECT * FROM cpm_local_region WHERE province_ids LIKE '%" . $province_id . "%'";
		$region = $this->pdb->query($sql)->row;
		return $region['region_code'];
	}
}