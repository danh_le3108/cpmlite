<?php
class ControllerApiCheckPhone extends Controller {
	public function index() {
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}
		$json = array(
			'api_name' => 'check_phone',
			'message' => '',
			'status' => 1,
			'data' => ''
		);
		$phone = !empty($this->request->post['phone']) ? $this->request->post['phone'] : 0;
		$sql = "SELECT * FROM " . DB_PREFIX . "order_detail WHERE phone = '" . $this->db->escape($phone) . "'";
		$json['message'] = !empty($this->pdb->query($sql)->row) ? 'Số điện thoại này đã tồn tại trong hệ thống!' : 'Số điện thoại hợp lệ!';
		return $this->response->setOutput(json_encode($json));
	}
}
