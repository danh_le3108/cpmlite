<?php
class ControllerApiGetReasonsNotPass extends Controller {
	public function index() {
		$json = array();
		$json['api_name'] = 'get_reasons_not_pass';
		$json['not_pass_id_show_comment'] = 9;
		
		$this->load->model('localisation/reason_not_pass');
		$json['data'] = $this->model_localisation_reason_not_pass->getReasons();
		
		$json['status'] = 1;
		$json['message'] = 'Load Not Pass Reasons Success';

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->response->setOutput(json_encode($json));

	}
}
