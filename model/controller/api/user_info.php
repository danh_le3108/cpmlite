<?php
class ControllerApiUserInfo extends Controller {
	public function index() {
		$json = array();
		$json['api_name'] = 'user_info';
		
		
		$this->load->language('api/api');
		if(!isset($this->request->post['api_key'])){
			$json['message'] = $this->language->get('text_error_key');
			$json['status'] = 1;
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}
		// Login with API Key
		if (isset($this->request->post['api_key'])&&trim($this->request->post['api_key']) ==$this->config->get('config_api')&&isset($this->request->post['username'])&&isset($this->request->post['password'])) {
			$username = $this->request->post['username'];
			$password = $this->request->post['password'];

			$this->load->model('user/user');
			$user_info = $this->model_user_user->getUserByUsername($username);

			if($user_info&&isset($user_info['username'])){
				$fields = array(
						'user_id'                => '',
						'usercode'               => '',
						'username'               => '',
						'fullname'               => '',
						'email'                  => '',
						'telephone'              => '',
						'image'                  => '',
						'images'                 => '{"front_identity_card":"","back_identity_card":"","portrait":"","signature":""}',
						'active'                 => 0,
						'p_identity_number'      => '',
						'p_identity_issue_date'  => '',
						'p_identity_issue_place' => '',
						'p_tax_code'             => '',
						'p_insurance_num'        => '',
						'p_belonging_persons'    => '',
						'p_relative_telephone'   => '',
						'p_birthday'             => '',
						'p_place_of_birth'       => '',
						'p_nationality'          => '',
						'p_nation'               => '',
						'p_degree'               => '',
						'p_re_address'           => '',
						'p_re_province_id'       => '',
						'p_re_district_id'       => '',
						'p_re_ward_id'           => '',
						'p_so_address'           => '',
						'p_so_province_id'       => '',
						'p_so_district_id'       => '',
						'p_so_ward_id'           => '',
						'p_bi_address'           => '',
						'p_bi_province_id'       => '',
						'p_bi_district_id'       => '',
						'p_bi_ward_id'           => '',
						'p_protector'            => '',
						'p_bank_holder'          => '',
						'p_bank_number'          => '',
						'p_bank_name'            => '',
						'area_id'                => '',
						'region_id'              => '',
						'province_id'            => '',
						'district_id'            => '',
						'ward_id'                => '',
						'address'                => ''
				);
				if($this->user->isLogged()){
					foreach ($fields as $key =>$value) {
						if($user_info&&isset($user_info[$key])){
							$json[$key] = $user_info[$key];
						}else{
							$json[$key] = $value;
						}

					}
				}
			} else{
				$json['status'] = 0;
				$json['message'] = $this->language->get('text_error_info');
			}

				$json['success'] = $this->language->get('text_success');
		}else {
			$json['error'] = $this->language->get('text_error_key');
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
