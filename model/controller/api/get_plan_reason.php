<?php
class ControllerApiGetPlanReason extends Controller {
	public function index() {
		$json = array();
		$json['api_name'] = 'get_plan_reason';
		$json['reason_id_show_comment'] = 7;
		
		$this->load->model('localisation/reason_unsuccess');
		$reasons_data = array();
		$reasion = $this->model_localisation_reason_unsuccess->getReasons();
		$success = array();
		$unsuccess = array();
		foreach ($reasion as $v) {
			if ($v['reason_id'] > 19 && $v['type'] == 1) {
				$success[] = $v;
			} elseif ($v['type'] == 0) {
				if($v['parent'] == 0) {
					$v['childs'] = array();
					$unsuccess[$v['reason_id']] = $v;
				} else {
					$unsuccess[$v['parent']]['childs'][] = $v;
				}
			}
		}

		$json['data']['success'] = $success;
		foreach ($unsuccess as $v) {
			$json['data']['unsuccess'][] = $v;
		}
		
		
		$json['status'] = 1;
		$json['message'] = 'Load Plan Reasons Success';

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->response->setOutput(json_encode($json));

	}
}
