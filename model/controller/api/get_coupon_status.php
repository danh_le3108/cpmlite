<?php
class ControllerApigetCouponStatus extends Controller {
	public function index() {
		$json = array();
		$json['api_name'] = 'get_coupon_status';
		
		
		$this->load->model('catalog/coupon_status');
		$coupon_statuses_data = array();
		$coupon_statuses = $this->model_catalog_coupon_status->getCouponStatuses();
		foreach($coupon_statuses as $coupon_status){
			$coupon_statuses_data[] = $coupon_status;
		}
		$json['data'] =$coupon_statuses_data;
		
		$json['status'] = 1;
		$json['message'] = 'Load Coupon Statuses Success';

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->response->setOutput(json_encode($json));

	}
}
