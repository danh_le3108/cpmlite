<?php
class ControllerApiLocalDistricts extends Controller {
	private $district_fields = array(
	'district_id',
	'province_id',
	'prefix',
	'name',
	'district_name',
	'district_alias',
	'latitude',
	'longitude'
	);
	public function index() {
		$json = array();
		$json['api_name'] = 'local_districts';
		
		$this->load->language('api/api');
		$this->load->model('localisation/district');
		$district_data = array();
		$json['status'] = 1;
		$json['message'] = $this->language->get('text_success');
		$districts =  $this->model_localisation_district->getDistricts();
		foreach($districts as $district){
			$district_info = array();
			foreach($this->district_fields as $field){
				if($district[$field]){
					$district_info[$field] =  $district[$field];
				}
			}
			$district_info['district_full'] = $district['prefix'].' '.$district['name'];
			$json['data'][] = $district_info;
		}
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
