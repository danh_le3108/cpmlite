<?php
class ControllerApiUploadItem extends Controller {
	public function index() {
		$json = array();
		$json['api_name'] = 'upload_item';
		
		if(!isset($this->request->post['api_key'])){
			$json['status'] = 0;
			$json['message'] = $this->language->get('text_error_key');
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}
		
		$json['status'] = 1;
		$json['message'] = 'Success';
		if (isset($this->request->post['api_key'])&&trim($this->request->post['api_key']) ==$this->config->get('config_api')&&isset($this->request->post['user_id'])&&isset($this->request->post['plan_id'])&&isset($this->request->post['item_list'])) {
				$user_id = $this->request->post['user_id'];
				$plan_id = $this->request->post['plan_id'];
				$is_fix = isset($this->request->post['is_fix'])?$this->request->post['is_fix']:0;
					
					
				$this->load->model('plan/plan');
				$plan_info = $this->model_plan_plan->getPlan($plan_id);
				if(isset($plan_info['plan_id'])&&$user_id==$plan_info['user_id']){
					$json['status'] = 1;
					$attrs = json_decode(html_entity_decode(str_replace(array('“','”'),array('"','"'),$this->request->post['item_list']), ENT_QUOTES, 'UTF-8'),true);
					$this->load->model('catalog/attribute_data');
					if (!empty($attrs)) {
						foreach ($attrs as $attr) {
							$attribute_id = $attr['item_id'];
							if (!empty($attr['list'])) {
								foreach ($attr['list'] as $item) {
									$data = array(
										'plan_id' => $this->request->post['plan_id'],
										'attribute_id' => $attribute_id,
										'type' => $item['type'],
										'status' => $item['status'],
										'promotion' => $item['promotion'],
										'position' => $item['position'],
										'round_name' => $plan_info['round_name']
									);
									$this->model_catalog_attribute_data->add($data);
								}
							}
						}
					}
					$json['message'] = 'Success';
				}  else {
					$json['status'] = 0;
					$json['message'] = 'Không có plan này';
				}
		}else if(isset($plan_info['plan_id'])&&($plan_info['user_id']!=$user_id||(int)$plan_info['is_deleted']==1)){//||$store_code!= $plan_info['store_code']
				$json['status'] = 0;
				$json['message'] = 'Nội dung thực hiện của CH này đã thay đổi từ hệ thống, vui lòng upload những cửa hàng bạn đã thực hiện hôm nay sau đó tải lại danh sách hoặc liên hệ Admin!';
		}  else {
			$json['status'] = 0;
			$json['message'] = 'Không có Plan hoặc Nhân viên này';
		}
		$json['status'] = 1;
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->response->setOutput(json_encode($json));

	}
}
