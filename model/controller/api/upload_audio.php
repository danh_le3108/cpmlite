<?php
class ControllerApiUploadAudio extends Controller {
	public function index() {
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}
		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->log->write('Upload audio: ' . json_encode($this->request->post));
		$json = array(
			'api_name' => 'upload_audio',
			'status' => 0,
			'message' => '',
			'data' => array()
		);	
		
		$data = array(
			'post' => $this->request->post,
			'require' => array('api_key','user_id','plan_id')
		);
		$validation = $this->load->controller('api/validation', $data);
		if ($validation['status'] == 0) {
			$json['message'] = $validation['message'];
			return $this->response->setOutput(json_encode($json));
		}

		$post = $this->request->post;
		$is_fix = isset($this->request->post['is_fix'])?$this->request->post['is_fix']:0;
		$this->load->model('plan/plan');
		$plan_id = $post['plan_id'];
		$user_id = $post['user_id'];
 		$plan = $validation['plan'];	
 		$user = $validation['user'];	

		if (empty($json['message']) && empty($this->request->files['record'])) {
			$json['message'] = "Không có file ghi âm! (plan_id = {$plan_id})";
			return $this->response->setOutput(json_encode($json));
		}

		$config_project_folder =  $this->config->get('config_project_folder');
		$file_path = 'files/'.$config_project_folder.'/audio/'.$plan['round_name'].'/'.$plan['store_code'].'/'.$plan_id.'/';
		if(!is_dir(DIR_MEDIA.$file_path)){
			@mkdir(DIR_MEDIA.$file_path,  0777, true);
			@touch(DIR_MEDIA.$file_path . 'index.html');
		}
		$filename = explode('.', $this->request->files["record"]['name']);
		if (!in_array($filename[1], array('mp3'))) {
			$json['message'] = $this->language->get('error_filetype');
			return $this->response->setOutput(json_encode($json));
		}
		$ext = $filename[1];
		$username = !empty($user) ? str_replace('.', '', $user['username']) : '';
		$sha1 = $filename[0];
		$new_name = $plan['store_code'].'-'.$plan['plan_id'].'-'.$username.'-'.$sha1.'.'.$ext;

		if (!file_exists(DIR_MEDIA.$file_path.$new_name)) {
			move_uploaded_file($this->request->files['record']['tmp_name'], DIR_MEDIA.$file_path.$new_name);
			$audio = array(
				'filename' => $file_path.$new_name,
				'plan_id' => $plan['plan_id'],
				'user_id' => $post['user_id'],
				'is_fix' => $is_fix
			);
			$this->model_plan_plan->addAudio($audio);
			$plan_data = array(
				'audio_total' => $plan['audio_total']+1,
				'time_upload_audio' => date('Y-m-d H:i:s')
			);
			$this->model_plan_plan->updatePlan($plan['plan_id'], $plan_data);
		}
		
		$json['status'] = 1;
		$json['message'] = 'Success.';
		return $this->response->setOutput(json_encode($json));
	}
}
