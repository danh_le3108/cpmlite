<?php
class ModelPlanPlan extends Model {
	private $no_filter_key = array(
		'_url',
		'route',
		'page',
		'filter_global',
		'start',
		'limit',
		'sort',
		'order',
		'filter_date',
		'filter_date_checkin',
		'filter_posm_id',
		'select'
	);

	private $filter_global = array(
		's.store_name',
		's.store_code',
		's.store_phone',
		'pl.plan_id',
		'pl.store_id',
		'pl.usercode',
		'pl.plan_name',
		'pl.round_name',
		's.customer_code'
	);

	public function countTotal($data = array(), $field = '', $ignore = array()) {
		$select = !empty($field) ? $field.',' : '';
		$sql = "SELECT {$select} count(*) as total FROM " . DB_PREFIX . "plan pl
				LEFT JOIN " . DB_PREFIX . "store s ON pl.store_id = s.store_id WHERE pl.is_deleted = 0";
				
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				if (!in_array($key, $this->no_filter_key)) {
					$key = str_replace('filter_', '', $key);
					$sql .= " AND {$key}= '" . $this->pdb->escape($value) . "'";
				}	
			}

			if (!empty($data['filter_global'])) {
				$sql .= " AND (";
				foreach ($this->filter_global as $filter) {
					$implode[] = " LCASE(".$filter.") LIKE '%" . $this->pdb->escape(utf8_strtolower($data['filter_global'])) . "%'";
				}
				$sql .= " " . implode(" OR ", $implode) . "";
				$sql .= ")";
			}

			if (!empty($data['filter_date'])) {
				$sql .= " AND pl.time_upload LIKE '" . $data['filter_date'] . "%'";
			}
		}

		if ($this->user->isLogged() && $this->user->getGroupId() > $this->config->get('config_user_pa')) {
			$users_manager = $this->user->users_manager();
			if (!empty($users_manager)) {
				$sql .= " AND pl.user_id IN (" . implode(',', $this->user->users_manager()) . ")";
			} else {
				if ($this->user->getGroupId() != 4) {
					$sql .= " AND pl.user_id = " . $this->user->getId();
				}
			}
		}

		if ($this->customer->isLogged()) {
			$sql .= " AND plan_status = 1 AND pl.user_id != 3519";
			// $sql .= " AND plan_status = 1 AND pl.user_id != 3519 AND pl.plan_qc = 1";
			// if ($this->customer->getGroupId() != $this->config->get('config_customer_top')) {
			// 	$sql .= " AND pl.plan_qc = 1";
			// }
		}

		if (!empty($data['filter_posm_id'])) {
			$sql .= " AND pl.plan_id IN (SELECT plan_id FROM cpm_attribute_data WHERE attribute_id = {$data['filter_posm_id']} AND value > 0)";
		}

		if ($field) {
			$sql .= " GROUP BY " . $field;
			$data = array();
			foreach ($this->pdb->query($sql)->rows as $value) {
				$data[$value[$field]] = $value['total'];
			}
			return $data;
		} else {
			return $this->pdb->query($sql)->row['total'];
		}
	}

	public function getPlans($data) { 

		$select = !empty($data['select']) ? $data['select'] : '*';
		$sql = "SELECT " . $select . " FROM " . DB_PREFIX . "plan pl
				LEFT JOIN " . DB_PREFIX . "store s ON pl.store_id = s.store_id WHERE pl.is_deleted = 0";
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				if (!in_array($key, $this->no_filter_key)) {
					$key = str_replace('filter_', '', $key);
					$sql .= " AND {$key}= '" . $this->pdb->escape($value) . "'";
				}	
			}

			if (!empty($data['filter_global'])) {
				$sql .= " AND (";
				foreach ($this->filter_global as $filter) {
					$implode[] = " LCASE(".$filter.") LIKE '%" . $this->pdb->escape(utf8_strtolower($data['filter_global'])) . "%'";
				}
				$sql .= " " . implode(" OR ", $implode) . "";
				$sql .= ")";
			}

			if (!empty($data['filter_date'])) {
				$sql .= " AND pl.time_upload LIKE '" . $data['filter_date'] . "%'";
			}

			if (!empty($data['filter_date_checkin'])) {
				$sql .= " AND pl.time_checkin LIKE '" . $data['filter_date_checkin'] . "%'";
			}
		}

		if ($this->user->isLogged() && $this->user->getGroupId() > $this->config->get('config_user_pa')) {
			$users_manager = $this->user->users_manager();
			if (!empty($users_manager)) {
				$sql .= " AND pl.user_id IN (" . implode(',', $this->user->users_manager()) . ")";
			} else {
				if ($this->user->getGroupId() != 4) {
					$sql .= " AND pl.user_id = " . $this->user->getId();
				}
			}
		}

		if ($this->customer->isLogged()) {
			$sql .= " AND plan_status = 1 AND pl.user_id != 3519";
			// $sql .= " AND plan_status = 1 AND pl.user_id != 3519 AND pl.plan_qc = 1";
			// if ($this->customer->getGroupId() != $this->config->get('config_customer_top')) {
			// 	$sql .= " AND pl.plan_qc = 1";
			// }
		}

		if (!empty($data['filter_posm_id'])) {
			$sql .= " AND pl.plan_id IN (SELECT plan_id FROM cpm_attribute_data WHERE attribute_id = {$data['filter_posm_id']} AND value > 0)";
		}

		if (!empty($data['sort'])) {
			$sql .= ' ORDER BY ' . $data['sort'];
		} else {
			$sql .= ' ORDER BY pl.date_added DESC';
		}

		if (isset($data['start']) && isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
	
		return $this->pdb->query($sql)->rows;
	}

	public function getPlan($plan_id) {
		$sql = "SELECT * FROM " . DB_PREFIX . "plan pl
				LEFT JOIN " . DB_PREFIX . "store s ON pl.store_id = s.store_id WHERE pl.is_deleted = 0 AND plan_id = " . (int)$plan_id;

		if ($this->user->isLogged() && $this->user->getGroupId() > $this->config->get('config_user_pa')) {
			$users_manager = $this->user->users_manager();
			if (!empty($users_manager)) {
				$sql .= " AND pl.user_id IN (" . implode(',', $this->user->users_manager()) . ")";
			} else {
				if ($this->user->getGroupId() != 4) {
					$sql .= " AND pl.user_id = " . $this->user->getId();
				}
			}
		}
		
		if ($this->customer->isLogged()) {
			$sql .= " AND plan_status = 1 AND pl.user_id != 3519";
			// $sql .= " AND plan_status = 1 AND pl.user_id != 3519 AND pl.plan_qc = 1";
			// if ($this->customer->getGroupId() != $this->config->get('config_customer_top')) {
			// 	$sql .= " AND pl.plan_qc = 1";
			// }
		}
		return $this->pdb->query($sql)->row;
	}

	public function getPlansByUserId($user_id = 0, $data = array()) {
		$sql = "SELECT * FROM " . PDB_PREFIX . "plan pl 
		   LEFT JOIN " . PDB_PREFIX . "store s ON (s.store_id = pl.store_id) 
		   WHERE pl.is_deleted = '0' AND DATE(pl.`date_end`) >= DATE(NOW()) AND user_id = " .  (int)$user_id;
		
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				if (!in_array($key, $this->no_filter_key)) {
					$key = str_replace('filter_', '', $key);
					$sql .= " AND {$key}= '" . $this->pdb->escape($value) . "'";
				}	
			}
	   	}
		
		$sql .= " ORDER BY plan_id DESC";
		return $this->pdb->query($sql)->rows; 
	}
	
	public function deletePlan($plan_id) {
		$this->pdb->query("UPDATE `" . PDB_PREFIX . "plan` SET is_deleted = '1', user_deleted = '" . $this->pdb->escape($this->user->getId()) . "' WHERE plan_id = '" . (int)$plan_id . "'");
	}

	public function updatePlan($plan_id, $data) {
		$sql = "UPDATE " . PDB_PREFIX . "plan SET ";
		if(isset($data['reason_id'])){
	      	$sql .= " reason_id = '" . (int)$data['reason_id'] . "',";
	      	if((int)$data['reason_id']>0) {
	        	$sql .= " plan_rating = '-2',";
	        	// $this->updatePlanNotPass($plan_id,$data);
	      	} else {
	        	$sql .= "plan_rating = '1',";
	      	}
	      	unset($data['reason_id']);
	    }
		
		foreach ($data as $key => $value) {
			$sql .= "{$key} = '" . $this->pdb->escape($value) . "',";
		}

		$sql .= "date_modified = NOW() WHERE plan_id = '" . (int)$plan_id . "'";
		$this->pdb->query($sql);

	}

	public function addPlan($data) {
		$sql = "INSERT INTO " . PDB_PREFIX . "plan SET ";
		foreach ($data as $key => $value) {
			$sql .= "{$key} = '" . $this->pdb->escape($value) . "',";
		}
		$sql .= "date_added = NOW()";
		$this->pdb->query($sql);
		return $this->pdb->getLastId();
	}

	//PLAN NOTE
	public function getPlanNotes($data = array()) {
		$sql = "SELECT * FROM " . PDB_PREFIX . "plan_note qn WHERE qn.is_deleted = '0' ";
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				$sql .= " AND {$key} = '" . $this->pdb->escape($value) . "'";
			}
		}
		$sql .= " ORDER BY qn.date_added ASC";
		return $this->pdb->query($sql)->rows;
	}

	public function getPlanNotesByPlanId($plan_id, $note_id = 0) {
		$sql = "SELECT * FROM " . PDB_PREFIX . "plan_note qn WHERE qn.is_deleted = 0 AND qn.plan_id = " . (int)$plan_id;
		if ($note_id) {
			$sql .= ' AND note_id = ' . (int)$note_id;
		} 
		$sql .= ' ORDER BY qn.date_added ASC';
		return $this->pdb->query($sql)->rows;
	}

	public function updatePlanNote($qc_note_id, $data) {
		$sql = "UPDATE " . PDB_PREFIX . "plan_note SET ";
		foreach ($data as $key => $value) {
			$sql .= "{$key} = '" . $this->pdb->escape($value) . "',";
		}
		$sql .= "date_modified = NOW() WHERE qc_note_id = '" . (int)$qc_note_id . "'";
		$this->pdb->query($sql);
		return $this->pdb->getLastId();
	}

	public function addPlanNote($data) {
		$sql = "INSERT INTO " . PDB_PREFIX . "plan_note SET ";
		foreach ($data as $key => $value) {
			$sql .= "{$key} = '" . $this->pdb->escape($value) . "',";
		}
		$sql .= "date_added = NOW()";
		$this->pdb->query($sql);
		return $this->pdb->getLastId();
	}

	public function deletePlanNotes($qc_note_id) {
		$sql = "UPDATE " . DB_PREFIX . "plan_note SET is_deleted = 1, date_deleted = NOW(), user_deleted = '" . $this->pdb->escape($this->user->getId().' - '.$this->user->getFullName()) . "' WHERE qc_note_id = " . (int)$qc_note_id;
		$this->pdb->query($sql);
	}

	//PLAN QC
	public function getPlanCodes($plan_id) {
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "plan_code qp WHERE qp .plan_id = '" . (int)$plan_id . "'");
		return $query->rows;
	}

	public function deletePlanCode($plan_id) {
		$sql = "DELETE FROM " . DB_PREFIX . "plan_code WHERE plan_id = " . (int)$plan_id;
		$this->pdb->query($sql); 
	}

	public function updatePlanCode($data) {
		$sql = "INSERT INTO " . PDB_PREFIX . "plan_code SET ";
		foreach ($data as $key => $value) {
			$sql .= "{$key} = '" . $this->pdb->escape($value) . "', ";
		}
		$sql .= "date_added = NOW()";
		$this->pdb->query($sql);
	}

	//PLAN CONFIRM
	public function getPlanConfirms($plan_id) {
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "plan_confirm qc WHERE qc.plan_id = '" . (int)$plan_id . "'");
		return $query->rows;
	}

	public function getPlanConfirm($plan_id, $user_id = '', $user_group_id = '') {
		$sql = "SELECT * FROM " . PDB_PREFIX . "plan_confirm qc WHERE qc.plan_id = " . (int)$plan_id;
		if (!empty($user_id)) {
			$sql .= " AND user_id = " . (int)$user_id;
 		}
 		if (!empty($user_id)) {
			$sql .= " AND user_group_id = " . (int)$user_group_id;
 		}
 		
		return $this->pdb->query($sql)->row;
	}

	public function countConfirm($plan_id) {
		$query = $this->pdb->query("SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "plan_confirm
		WHERE plan_id = '" . (int)$plan_id . "' AND user_id= '" .(int)$this->user->getId(). "'");
	}

	public function updateConfirm($data) {
		$user_dc = $this->config->get('config_user_dc');
		$user_pa = $this->config->get('config_user_pa');
		$sql = "INSERT INTO " . DB_PREFIX . "plan_confirm SET ";
		foreach ($data as $key => $value) {
			$sql .= "{$key} = '" . $this->pdb->escape($value) ."',";
		}
		$sql .= "date_added = NOW()";
		$this->pdb->query($sql);
		//QC = 1 khi nhóm user == DC||PA
		$user_group_id = $this->user->getGroupId();
		if(in_array($user_group_id,array($user_dc,$user_pa))){
			$sql = "UPDATE " . PDB_PREFIX . "plan SET plan_qc = '1' WHERE plan_id = '" . (int)$data['plan_id'] . "'";
			$this->pdb->query($sql);
			
	
			//Lưu vào DB chung để check GPS - Luân
			$new_info = $this->getPlan($data['plan_id']);//***Hàm này nhớ coi lại mỗi dự án có thể thay đổi***
			$this->load->model('project/project_user');
			$user_info = $this->model_project_project_user->getUser($new_info['user_id']);
			
			$new_info['project_id'] = $user_info['project_id'];
			$new_info['user_fullname'] = $user_info['fullname'];
			$new_info['username'] = $user_info['username'];
			$new_info['usercode'] = $user_info['usercode'];
			$new_info['sup_id'] = (isset($new_info['sup_id'])&&$new_info['sup_id']>0)?$new_info['sup_id']:$user_info['user_parent_id'];
			$new_info['sup_fullname'] = isset($user_info['parent_fullname'])?$user_info['parent_fullname']:'';
			$new_info['latitude'] = $new_info['latitude'];
			$new_info['longitude'] = $new_info['longitude'];
			$this->user->checkVisit($new_info);
			//End - Lưu vào DB chung
			
			
		}
	}

	//PLAN IMAGE
	public function addPlanImage($plan_id,$data) {
		$img_fix = isset($data['img_fix'])?$data['img_fix']:0;
		$is_checkin = isset($data['is_checkin'])?$data['is_checkin']:0;
		$is_diff = isset($data['is_diff'])?$data['is_diff']:0;
		$sql = "INSERT INTO `" . PDB_PREFIX . "plan_images` SET
		`sha1` = '" . $this->db->escape($data['sha1']) . "',
		plan_id = '" . (int)$plan_id . "',
		is_checkin = '" . (int)$is_checkin . "',
		img_fix = '" . (int)$img_fix . "',
		is_diff = '" . (int)$is_diff . "',
		user_id = '" . (int)$data['user_id'] . "',
		store_id = '" . (int)$data['store_id'] . "',
		store_code = '" . $this->db->escape($data['store_code']) . "',
		round_name = '" . $this->db->escape($data['round_name']) . "',
		filename = '" . $this->db->escape($data['filename']) . "',
		manual = '" . (int)$data['manual'] . "',
		image_type_id = '" . (int)$data['image_type_id'] . "',
		user_created = '" .(int)$data['user_created']. "',
		date_added = NOW()";

		$this->pdb->query($sql);
		$image_id = $this->pdb->getLastId();
		return $image_id;
	}

	public static $static_image = array();
	public function getPlanImages($plan_id) {
		if (isset(self::$static_image[$plan_id])) {
			return self::$static_image[$plan_id];
		}
		$plan_images = array();
		$query = $this->pdb->query("SELECT * FROM `" . PDB_PREFIX . "plan_images` si WHERE si.is_deleted = '0' AND si.plan_id = '" . (int)$plan_id . "' ORDER BY si.image_id ASC");
		if($query->rows){
			$plan_images = $query->rows;
		}
	 	return self::$static_image[$plan_id] = $plan_images;
	}

	public function getImage($image_id) {
		$sql = "SELECT * FROM " . PDB_PREFIX . "plan_images WHERE image_id = '" . (int)$image_id . "'";
		$query = $this->pdb->query($sql);
		return $query->row;
	}

	public function deleteImage($image_id,$manual = 1) {
		$config_project_folder = $this->config->get('config_project_folder');
		$image_info = $this->getImage($image_id);
		
		$plan_id = $image_info['plan_id'];
		$plan_info = $this->getPlan($plan_id);
		
		$image_info['old_path'] = $old_path = DIR_MEDIA.$plan_info['image_path'].'image/'.$plan_id.'/'.$image_info['filename'];
		$new_path = DIR_MEDIA.'files/'.$config_project_folder.'/deleted/'.$image_info['store_code'].'/image/'.$plan_id.'/';
		$new_name = $image_info['filename'];
		$image_info['new_path'] = $new_path.$new_name;
		
		if (file_exists($old_path)) {
		 	@mkdir($new_path,  0777, true);
			copy($image_info['old_path'], $image_info['new_path']);
		}
		if (file_exists($old_path)&&file_exists($new_path)) {
				unlink($old_path);
		}
		if($manual==1){
			$sql = "UPDATE `" . PDB_PREFIX . "plan_images` SET is_deleted = '1', user_deleted = '" .$this->db->escape($this->user->getId()). "' WHERE image_id = '" . (int)$image_id . "'";
			$this->pdb->query($sql);
		}
	}

	public function updateImage($image_id,$data) {
		$sql = "UPDATE `" . PDB_PREFIX . "plan_images` SET ";
		foreach ($data as $key => $value) {
			$sql .= "{$key} = '" . $this->pdb->escape($value) . "',";
		}
		$sql .= "date_modified = NOW() WHERE image_id = '" . (int)$image_id . "'";
		$this->pdb->query($sql);
	}

	public function updatePlanImage($image_id,$data) {
		$sql = "UPDATE `" . PDB_PREFIX . "plan_images` SET filename = '" . $this->db->escape($data['filename']) . "' WHERE image_id = '" . (int)$image_id . "'";
		$this->pdb->query($sql);
		return $image_id;
	}

	public function getAudios($plan_id) {
		$sql = "SELECT * FROM " . DB_PREFIX . "audio WHERE is_deleted = 0 AND plan_id = " . $plan_id;
		return $this->pdb->query($sql)->rows;
	}

	public function addAudio($data) {
		$sql = "INSERT INTO " . DB_PREFIX . "audio SET ";
		foreach ($data as $key => $value) {
			$sql .= "{$key} = '" . $this->pdb->escape($value) . "',";
		}
		$sql .= "date_added = NOW()";
		$this->pdb->query($sql);
		return $this->pdb->getLastId();
	}

	public function getAudio($id) {
		$sql = "SELECT * FROM " . DB_PREFIX . "audio WHERE is_deleted = 0 AND id = " . $id;
		return $this->pdb->query($sql)->row;
	}
	
	public function deleteAudio($audio_id) {
		$config_project_folder = $this->config->get('config_project_folder');
		$audio = $this->getAudio($audio_id);
		$plan = $this->getPlan($audio['plan_id']);
		$old_path = DIR_MEDIA.$audio['filename'];
		$new_path = DIR_MEDIA.'files/'.$config_project_folder.'/deleted/'.$plan['store_code'].'/audio/'.$plan['plan_id'].'/';
		if (file_exists($old_path)) {
		 	@mkdir($new_path,  0777, true);
		 	$new_file_name = explode('/', $old_path);
		 	$new_file_name = end($new_file_name);
			copy($old_path, $new_path.$new_file_name);
		}
		if (file_exists($old_path)&&file_exists($new_path)) {
			unlink($old_path);
		}
		$sql = "UPDATE " . DB_PREFIX . "audio SET is_deleted = 1, user_deleted = " . $this->user->getId() . " WHERE id = " . (int)$audio_id;
		$this->pdb->query($sql);
	}

	public function planRating($plan_id = 0) {
		$plan = $this->getPlan($plan_id);
		$plan_rating = -1;
		$location_st = $col_num_st = $facing_st = $brand_st = 0;
		if (!empty($plan)) {
			$this->load->model('catalog/attribute');
			$store_level = $this->model_catalog_attribute->getGroupByCode($plan['region_code'].$plan['store_type_id']);
			$this->load->model('catalog/attribute_data');
			$attrs = $this->model_catalog_attribute_data->getAttrsOfPlan(array('plan_id'=>$plan_id));
			$is_sting = $is_mirinda = 0;
			$facing_num = $brand_num = $col_num = 0;
			foreach ($attrs as $attr) {
				if (!empty($attr['value'])) {
					if ($attr['attribute_id'] > 9) {
						if ($attr['attribute_id'] == 10) {                // Vi tri trung bay
							if ($attr['value'] == 1) {
								$location_st = 1;
							}
						}  elseif ($attr['attribute_id'] == 11) {
							$col_num = $attr['value'];
						} elseif ($attr['attribute_id'] != 21) {          //facing và brand
							if (in_array($attr['attribute_id'], array(14,15))) {
								if ($is_mirinda == 0) {
									$is_mirinda = 1;
									$brand_num++;
								}	
							} elseif (in_array($attr['attribute_id'], array(17,18))) {
								if ($is_sting == 0) {
									$is_sting = 1;
									$brand_num++;
								}	
							} else {
								$brand_num++;
							}
							$facing_num += $attr['value'];					
						}
					}
				}
				
			}
			
			if ($facing_num >= $store_level['facing_num']) {
				$facing_st = 1;
			}
			if ($brand_num >= $store_level['brand_num']) {
				$brand_st = 1;
			}
			if ($col_num >= $store_level['column_num']) {    // so cot trung bay
				$col_num_st = 1;
			}
			if ($location_st + $col_num_st + $facing_st + $brand_st == 4) {
				$plan_rating = 1;
			}

			return array(
				'col_num' => $col_num,
				'facing_num' => $facing_num,
				'brand_num' => $brand_num,
				'location_st' => $location_st,
				'col_num_st' => $col_num_st,
				'facing_st' => $facing_st,
				'brand_st' => $brand_st,
				'plan_rating' => $plan_rating
			);	
		}
	}

}


// class ModelPlanPlan extends Model {
// 	public function filter($data = array(), $ignore = array()) {
// 		$condition = array("pl.is_deleted = 0");
// 		$store_field = array(
// 			'region_code',
// 			'province_id'
// 		);
// 		$ignore = array_merge($ignore, array('sort','order','limit','_url','route','start','page'));
// 		if (!empty($data['filter_global'])) {
// 			$filter_global = array(
// 				's.store_name',
// 				's.store_code',
// 				's.customer_username',
// 				'pl.plan_id',
// 				'pl.store_id',
// 				'pl.usercode',
// 				'pl.plan_name',
// 				'pl.round_name'
// 			);
// 			foreach ($filter_global as $filter) {
// 				$gimplode[] = " LCASE(".$filter.") LIKE '%" . $this->pdb->escape(utf8_strtolower($data['filter_global'])) . "%'";
// 			}
// 			$condition[] = "(" . implode(" OR ", $gimplode) . ")";
// 			unset($data['filter_global']);
// 		}
// 		if (!empty($data['filter_date'])) {
// 			$condition[] = "pl.time_upload LIKE '" . $data['filter_date'] . "%'";
// 			unset($data['filter_date']);
// 		}

// 		foreach ($data as $key => $value) {
// 			if (!in_array($key, $ignore)) {
// 				$field = str_replace('filter_', '', $key);
// 				$field = in_array($field, $store_field) ? 's.'.$field : 'pl.'.$field;
// 				$condition[] = "{$field} = '" . $this->pdb->escape($value) . "'";
// 			}
// 		}
// 		return $condition;
// 	}

// 	public function getPlans($data = array()) {
// 		$sql = "SELECT * FROM " . PDB_PREFIX . "plan pl LEFT JOIN " . PDB_PREFIX . "store s ON (s.store_id = pl.store_id)";
// 		$sql .= " WHERE " . implode(' AND ', $this->filter($data));
// 		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
// 			$sql .= " ORDER BY " . $data['sort'];
// 		} else {
// 			$sql .= " ORDER BY pl.plan_status,pl.time_checkin";
// 		}

// 		if (isset($data['order']) && ($data['order'] == 'ASC')) {
// 			$sql .= " ASC";
// 		} else {
// 			$sql .= " DESC";
// 		}

// 		if (isset($data['start']) || isset($data['limit'])) {
// 			if ($data['start'] < 0) {
// 				$data['start'] = 0;
// 			}
// 			if ($data['limit'] < 1) {
// 				$data['limit'] = 20;
// 			}
// 			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
// 		}
// 		return $this->pdb->query($sql)->rows;
// 	}

// 	public function getTotalPlans($data = array()) {
// 		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "plan pl 
// 				LEFT JOIN " . PDB_PREFIX . "store s ON (s.store_id = pl.store_id)";
// 		$sql .= " WHERE " . implode(' AND ', $this->filter($data));
// 		$query = $this->pdb->query($sql);
// 		return $query->row['total'];
// 	}

// 	public function countPlanByField($field, $data = array(), $ignore = array()) {
// 		$sql = "SELECT {$field}, COUNT(*) AS total FROM " . PDB_PREFIX . "plan pl 
// 				LEFT JOIN " . PDB_PREFIX . "store s ON (s.store_id = pl.store_id)";
// 		$sql .= " WHERE " . implode(' AND ', $this->filter($data, $ignore));
// 		$sql .= " GROUP BY {$field}";
// 		$field = str_replace(array('s.','pl.'), '', $field);
// 		$data = array();
// 		foreach ($this->pdb->query($sql)->rows as $value) {
// 			$data[$value[$field]] = $value['total'];
// 		}
// 		return $data;
// 	}
	
// 	/*Plan image*/


// 	/*Qc ký tên*/
// 	public function countConfirm($plan_id) {
// 		$query = $this->pdb->query("SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "plan_confirm
// 		WHERE plan_id = '" . (int)$plan_id . "' AND user_id= '" .(int)$this->user->getId(). "'");

// 		return $query->row['total'];
// 	}
// 	public function updateConfirm($plan_id,$data) {
// 		$user_group_id =$this->user->getGroupId();
// 		$user_dc = $this->config->get('config_user_dc');
// 		$user_pa = $this->config->get('config_user_pa');
// 		//QC = 1 khi nhóm user == DC||PA
// 		if(in_array($user_group_id,array($user_dc,$user_pa))){
// 			$sql = "UPDATE " . PDB_PREFIX . "plan SET plan_qc = '1' WHERE plan_id = '" . (int)$plan_id . "'";
// 			$this->pdb->query($sql);
// 		}


// 		$query = $this->pdb->query("SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "plan_confirm
// 		WHERE plan_id = '" . (int)$plan_id . "' AND user_id= '" .(int)$this->user->getId(). "'");

// 		if($query->row['total']<1){
// 			$sql = "INSERT INTO " . PDB_PREFIX . "plan_confirm SET
// 			round_name = '" .$this->db->escape($data['round_name']). "', 
// 			plan_id = '" . (int)$plan_id . "',
// 			user_id= '" .(int)$this->user->getId(). "',
// 			user_group_id= '" .(int)$user_group_id. "',
// 			date_added = NOW()";
// 			$this->pdb->query($sql);
// 		}
// 	}

// 	public function getPlanConfirms($plan_id) {
// $query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "plan_confirm qc WHERE qc.plan_id = '" . (int)$plan_id . "'");
// 		return $query->rows;
// 	}
// 	/*Qc Codes*/




// 	/*Truy vấn Plan Notes*/


// 	/*Thêm Plan Coupon */ 
	
// 	public function deletePlanCoupon($coupon_history_id) {
// 		$sql = "UPDATE " . PDB_PREFIX . "plan_coupon_history SET plan_id = (0-plan_id), coupon_status_id = 0, user_id = 0, coupon_id = 0, double_id = 0, message = '" .$this->db->escape('Delete by '.$this->user->getUserName()). "', user_updated = '" .$this->db->escape($this->user->getUserName()). "' WHERE coupon_history_id = '" . (int)$coupon_history_id . "'";
// 		$this->pdb->query($sql);
// 	}
	
// 	public function resetCoupon($coupon_history_id) {
// 		$this->pdb->query("UPDATE " . PDB_PREFIX . "coupon SET coupon_status_id = 0, plan_id = 0 WHERE coupon_history_id = '" . (int)$coupon_history_id . "'");
// 		$this->pdb->query("UPDATE " . PDB_PREFIX . "plan_coupon_history SET coupon_status_id = 0, coupon_id = 0, double_id = 0,coupon_code = '', message = '' WHERE coupon_history_id = '" . (int)$coupon_history_id . "'");
		
		
// 	}
// 	public function addPlanCoupon($plan_id,$data) {
// 		$sql = "INSERT INTO " . PDB_PREFIX . "plan_coupon_history SET
// 		round_name = '" . $this->db->escape($data['round_name']) . "',
// 		yearmonth = '" . $this->db->escape($data['yearmonth']) . "',
// 		reward_type_id = '" . (int)$data['reward_type_id'] . "',
// 		store_id = '" . (int)$data['store_id'] . "',
// 		coupon_beta = '" . $this->db->escape($data['coupon_beta']) . "',
// 		coupon_prefix = '" . $this->db->escape($data['coupon_prefix']) . "',
// 		user_id= '" .(int)$data['user_id'] . "',
// 		plan_import_id= '" .(int)$data['plan_import_id'] . "',
// 		plan_id = '" . (int)$plan_id . "',
// 		is_manual= '1',
// 		user_updated = '" .$this->db->escape($this->user->getId()). "', date_added = NOW()";
// 		$this->pdb->query($sql);
// 	}
// 	//Cập nhật các giá trị kiểm tra xuống Bảng coupon_history
// 	public function updatePlanCoupon($coupon_history_id, $data) {
// 		$sql = "UPDATE " . PDB_PREFIX . "plan_coupon_history SET date_modify = NOW()";
// 		if(isset($data['yearmonth'])){
// 			$sql .= ",yearmonth = '" . $this->pdb->escape($data['yearmonth']) . "'";
// 		}
// 		if(isset($data['double_id'])){
// 			$sql .= ",double_id = '" . (int)$data['double_id'] . "'";
// 		}
// 		if(isset($data['coupon_status_id'])){
// 			$sql .= ",coupon_status_id = '" . (int)$data['coupon_status_id'] . "'";
// 		}
// 		if(isset($data['coupon_id'])){
// 			$sql .= ",coupon_id = '" . (int)$data['coupon_id'] . "'";
// 		}
// 		if(isset($data['coupon_code'])){
// 			$sql .= ",coupon_code = '" . $this->pdb->escape($data['coupon_code']). "'";
// 		}
// 		if(isset($data['message'])){
// 			$sql .= ",message = '" . $this->pdb->escape($data['message']). "'";
// 		}
// 		if(isset($data['message'])){
// 			$sql .= ",message = '" . $this->pdb->escape($data['message']). "'";
// 		}
// 		if(isset($data['date_upload'])){
// 			$sql .= ",`date_upload` = NOW()";
// 		}
// 		$sql .= " WHERE coupon_history_id = '" . (int)$coupon_history_id . "'";
		 
// 			$this->pdb->query($sql);
// 	}
	
// 	public function activeMissingCoupons() {
// 		/*CHÚ Ý: 3 QUERY NÀY CÙNG QUERY VÀO 1 BẢNG NHƯNG ĐIỀU KIỆN RIÊNG BIỆT - TUYỆT ĐỐI KHÔNG MERGE CHUNG VÀO 1 CÂU */ 
		
// 		$config_day_valid = $this->config->get('config_day_valid');
// 		//Set Coupon quá hạn
// 		$allow_status = $this->config->get('config_coupon_has_paid');
// 		$sql = "UPDATE `".PDB_PREFIX."plan_coupon_history` SET 
// 		`dist_check_id` = " . (int)$this->config->get('config_dist_expires') . " WHERE 
// 		(TIMESTAMPDIFF(DAY,DATE(`date_upload`),DATE(NOW()))>".$config_day_valid .") AND coupon_status_id ='".$allow_status."' AND `dist_check_id`=0 AND `plan_id`>0";
			
// 		$this->pdb->query($sql);
		
// 		//Set coupon_status_id = config_coupon_has_paid
// 		/*$this->pdb->query("UPDATE `".PDB_PREFIX."plan_coupon_history` a INNER JOIN `".PDB_PREFIX."plan` b 
// 		ON a.`plan_id` = b.`plan_id` SET a.`coupon_status_id` = '" . (int)$this->config->get('config_coupon_has_paid') . "'
// 		WHERE a.`coupon_status_id`='" . (int)$this->config->get('config_coupon_valid') . "' AND b.`plan_status`=1");	*/ 
// 		//Set message = Đã giao cửa hàng
// 		$this->pdb->query("UPDATE `".PDB_PREFIX."plan_coupon_history` a INNER JOIN `".PDB_PREFIX."coupon_status` b ON a.`coupon_status_id` = b.`coupon_status_id` SET a.`message` = b.`coupon_status_name` WHERE 1");	
			
		
// 	}
// 	public function activePlanCoupon($plan_id) {
// 			$this->pdb->query("UPDATE `".PDB_PREFIX."plan_coupon_history` a INNER JOIN `".PDB_PREFIX."coupon_status` b 
// 			ON a.`coupon_status_id` = b.`coupon_status_id` SET a.`message` = b.`coupon_status_name` WHERE a.`plan_id` = '" . (int)$plan_id . "'");	
		
// 	}
// 	//Cập nhật bảng Coupon History vào bảng Coupon và thời gian giao Coupon
// 	public function updateCouponStatus($plan_id) {
			
// 			$sql1 = "UPDATE `" . DB_PREFIX . "coupon` a 
// 			INNER JOIN `" . DB_PREFIX . "plan_coupon_history` b ON a.`coupon_id` = b.`coupon_id` SET 
// 			a.`coupon_history_id` = b.`coupon_history_id`, 
// 			a.`coupon_status_id` = b.`coupon_status_id`,  
// 			a.`plan_id` = b.`plan_id`,  
// 			a.`round_name` = b.`round_name` WHERE 
// 			b.`coupon_status_id` = '".$this->config->get('config_coupon_has_paid')."' AND b.`plan_id` = '".(int)$plan_id."'";
			
// 			$this->pdb->query($sql1);
// 		//Cập nhật thời gian giao cửa hàng là lúc này
// 		$allow_status = array($this->config->get('config_coupon_valid'),$this->config->get('config_coupon_has_paid'));
// 		$sql2 = "UPDATE `".PDB_PREFIX."plan_coupon_history` SET `date_upload` = NOW() WHERE `plan_id` = '" . (int)$plan_id . "' AND `date_upload`='0000-00-00 00:00:00' AND coupon_status_id IN('".implode("','",$allow_status)."')";	
		
// 			$this->pdb->query($sql2);
// 	}
	
// 	public function getApiPlanCoupons($plan_id,$import_id=0,$show=0) {
		
// 			$uids = array(1);
			
// 			$this->load->model('catalog/coupon_prefix');
// 			$prefix =  $this->model_catalog_coupon_prefix->getPrefixsIndexBy('coupon_prefix');
// 		$coupon_data = array();
		
		
// 		$coupons_query = $this->getPlanCoupons($plan_id,$import_id);
		
// 		foreach($coupons_query as $row){
// 			$coupon_value = isset($prefix[$row['coupon_prefix']])?$prefix[$row['coupon_prefix']]['prefix_value']:'';
		
				
// 			$coupon_data[] = array(
// 				'coupon_history_id'=>$row['coupon_history_id'],
// 				'coupon_status_id'=>$row['coupon_status_id'],
// 				'coupon_prefix'=>$row['coupon_prefix'],
// 				'plan_id'=>$row['plan_id'],
// 				'coupon_code'=>($show==1)?$row['coupon_code']:'',
// 				'coupon_beta'=>$row['coupon_beta'],
// 				'coupon_value'=>$coupon_value,
// 				'yearmonth'=>$row['yearmonth'],
// 				'reward_type_id'=>$row['reward_type_id'],
// 				'reward_type_name'=>$row['reward_type_name'],
// 				'required'=>$row['required'],
// 				'message'=>($show==1)?$row['message']:''
// 			);
// 		}
		
// 		return $coupon_data;
// 	}
	
// 	public function getPlanCoupons($plan_id,$import_id,$index_by=NULL) {
// 		$sql = "SELECT *, 
// 		(SELECT t.reward_type_name FROM `" . PDB_PREFIX . "catalog_reward_type` t WHERE ch.reward_type_id = t.reward_type_id) AS reward_type_name 
// 		FROM " . PDB_PREFIX . "plan_coupon_history ch 
// 		WHERE ch.plan_id = '" . (int)$plan_id . "' 
// 		AND ch.`reward_type_id` != '" . (int)$this->config->get('config_reward_recall') . "' ORDER BY ch.reward_type_id ASC,ch.yearmonth DESC";
		
// 		//AND (ch.plan_import_id = '" . (int)$import_id . "' OR ch.plan_import_id = '0') 
		
// 		$query = $this->pdb->query($sql);
		
// 		$return = array();
// 		if($index_by!=NULL){
// 			foreach($query->rows as $row){
// 				$return[$row[$index_by]]= $row;
// 			}
// 		}else{
// 			$return = $query->rows;
// 		}
		
// 		return $return;
// 	}
// 	public function getPlanNotes($plan_id) {
// 		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "plan_note qn WHERE qn.is_deleted = '0' AND qn.plan_id = '" . (int)$plan_id . "' ORDER BY qn.date_added ASC ");
// 		return $query->rows;
// 	}
// 	/*Truy vấn Plan History*/
// 	public function getPlanHistories($plan_id, $start = 0, $limit = 10) {
// 			$start = ($start < 0)?0:$start;
// 			$limit = ($limit < 5)?5:$limit;

// $query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "plan_history ph WHERE ph.plan_id = '" . (int)$plan_id . "' ORDER BY ph.date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

// 		return $query->rows;
// 	}
// 	/*Đếm tổng số Plan History*/
// 	public function getTotalPlanHistories($plan_id) {
// 		$query = $this->pdb->query("SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "plan_history WHERE plan_id = '" . (int)$plan_id . "'");

// 		return $query->row['total'];
// 	}
// 	/*Thêm History*/
	
// 	public function checkPlanHistory($plan_id,$data) {
// 		$user_id = $this->user->getId();
		
// 		if(isset($data['rating_old'])){		
// 			$info_new = $this->getPlanInfo($plan_id);
// 			$data['rating_new'] = $info_new['plan_rating'];
// 		}
		
// 		if(!isset($this->session->data['plan_history'][$plan_id][$data['route']])) {
// 			$this->session->data['plan_history'][$plan_id][$data['route']] = time();
// 			$this->addPlanHistory($plan_id, $data); 
// 		} else{
// 			$total = $this->countPlanHistory($plan_id, $data);
// 			$last_time = $this->session->data['plan_history'][$plan_id][$data['route']];
// 			$different = time() - $last_time;
// 			if($total>0){
// 				$this->updatePlanHistory($plan_id, $data); 
// 			}else{
// 				$this->addPlanHistory($plan_id, $data); 
// 			}
// 			$this->session->data['plan_history'][$plan_id][$data['route']] = time();
// 		}
		
// 	}
	
// 	public function countPlanHistory($plan_id, $data) {
// 		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "plan_history WHERE plan_id = '" . (int)$plan_id . "' AND route = '" . $this->pdb->escape($data['route']) . "' AND user_id = '" . (int)$this->user->getId() . "'";
// 		$query = $this->pdb->query($sql);
// 		return isset($query->row['total'])?$query->row['total']:0;
			
// 	}
// 	public function updatePlanHistory($plan_id, $data) {
// 		$sql = "UPDATE " . PDB_PREFIX . "plan_history SET data_new = '" . $this->pdb->escape(json_encode($data['data_new'])) . "',";
// 		if(isset($data['rating_old'])){
// 			$sql .= "rating_old = '" . $this->pdb->escape($data['rating_old']) . "',";
// 		}
// 		if(isset($data['rating_new'])){
// 			$sql .= "rating_new = '" . $this->pdb->escape($data['rating_new']) . "',";
// 		}
// 		$sql .= "date_modified = NOW() WHERE plan_id = '" . (int)$plan_id . "' AND route = '" . $this->pdb->escape($data['route']) . "' AND user_id = '" . (int)$this->user->getId() . "'";
// 		$this->pdb->query($sql);
			
// 	}
// 	public function addPlanHistory($plan_id, $data) {
// 		$sql = "INSERT INTO " . PDB_PREFIX . "plan_history SET
// 				plan_id = '" . (int)$plan_id . "',
// 				user_id = '" . (int)$this->user->getId() . "',
// 				route = '" . $this->pdb->escape($data['route']) . "',
// 				data_old = '" . $this->pdb->escape($data['data_old']) . "',
// 				data_new = '" . $this->pdb->escape(json_encode($data['data_new'])) . "',";
		
// 		if(isset($data['rating_old'])){
// 			$sql .= "rating_old = '" . $this->pdb->escape($data['rating_old']) . "',";
// 		}
// 		if(isset($data['rating_new'])){
// 			$sql .= "rating_new = '" . $this->pdb->escape($data['rating_new']) . "',";
// 		}
				
// 			$sql .= "date_added = NOW()";
// 			$this->pdb->query($sql);
// 	}
// 	/*Cập nhật Plan*/
// 	public function updatePlanNotPass($plan_id,$data){
// 		$sql = "UPDATE `" . PDB_PREFIX . "plan_survey` SET rating_manual = '-2'";
			
// 			$sql .= ", `ktc_reason_id` = '" . (int)$data['reason_id'] . "'";
			
// 			$sql .= ", `note_ktc` = '" . $this->db->escape($data['note_ktc']) . "'";
			
// 			$sql .= " WHERE plan_id = '" . (int)$plan_id . "'";
// 			$this->pdb->query($sql);
// 	}
			
			
// 	public function updatePlan($plan_id, $data) {
// 		$sql = "UPDATE " . PDB_PREFIX . "plan SET ";
// 		if(isset($data['reason_id'])){
// 			$sql .= " `reason_id` = '" . (int)$data['reason_id'] . "',";
// 			if((int)$data['reason_id']>0) {
// 				$sql .= " `plan_rating` = '-2',";
// 				$this->updatePlanNotPass($plan_id,$data);
// 			}else{
// 				$sql .= "`plan_rating` = '1',";
// 			}
// 			unset($data['reason_id']);
// 		}
// 		foreach ($data as $key => $value) {
// 			$sql .= "{$key} = '" . $this->pdb->escape($value) . "',";
// 		}
// 		$sql .= "date_modified = NOW() WHERE plan_id = '" . (int)$plan_id . "'";
// 		$this->pdb->query($sql);

// 	}
// 	/*Plan Images*/
	
	
// 	public function bulkDeleteImages($filter){
// 		$sql = "SELECT * FROM " . PDB_PREFIX . "plan_images WHERE is_deleted = '1'";

// 		if (isset($filter['filter_round'])&&!empty($filter['filter_round'])) {
// 			$sql .= " AND round_name = '" . $this->pdb->escape($filter['filter_round']) . "'";
// 		}
// 		$query = $this->pdb->query($sql);
// 		if($query->rows){
// 			foreach($query->rows as $row){
// 				$this->deleteImage($row['image_id'],0);
// 			}
// 		}
// 	}

// 	public function deleteNote($qc_note_id) {
// 		$sql = "UPDATE `" . PDB_PREFIX . "plan_note` SET is_deleted = '1' WHERE qc_note_id = '" . (int)$qc_note_id . "'";
// 		$this->pdb->query($sql);
// 	}
	


// 	public static $count_images = array();
// 	public function countPlanImages($plan_id) {
// 		if (isset(self::$count_images[$plan_id])) {
// 			return self::$count_images[$plan_id];
// 		}
// 		$row_count = array();
// 		$query = $this->pdb->query("SELECT COUNT(*) as total, `image_type_id` FROM `" . DB_PREFIX . "plan_images` si WHERE si.is_deleted = '0' AND si.plan_id = '" . (int)$plan_id . "' GROUP BY si.image_type_id");
// 		if($query->num_rows){
// 			foreach($query->rows as $row){
// 			 $row_count[$row['image_type_id']] = $row['total'];
	
// 			}
// 		}
// 	 	return self::$count_images[$plan_id] = $row_count;
// 	}
// 	public static $static_image = array();
	
// 	public static $static_info = array();
// 	public function getPlanInfo($plan_id) {
// 		if (isset(self::$static_info[$plan_id])) {
// 			return self::$static_info[$plan_id];
// 		}
// 		$plan_info = array();
// 		$query = $this->pdb->query("SELECT *, pl.is_deleted as is_deleted, 
// 			(SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "plan_images i WHERE i.plan_id = pl.plan_id AND i.is_deleted = '0' GROUP BY i.plan_id) AS total_image, 
// 		(SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "plan_survey_data sv WHERE sv.plan_id = pl.plan_id GROUP BY sv.plan_id) AS total_answer, 
// 			(SELECT COUNT(*) FROM " . PDB_PREFIX . "plan_code pc WHERE pc.plan_id = pl.plan_id) AS total_code FROM " . PDB_PREFIX . "plan pl LEFT JOIN " . PDB_PREFIX . "store s ON (s.store_id = pl.store_id) WHERE pl.plan_id = '" . (int)$plan_id . "'");
// 			if($query->row){
// 				$plan_info = $query->row;
// 		  }
// 	 	return self::$static_info[$plan_id] = $plan_info;
// 	}
// 	public static $static_plan = array();
// 	public function getPlan($plan_id) {
// 		if (isset(self::$static_plan[$plan_id])) {
// 			return self::$static_plan[$plan_id];
// 		}
// 		$plan_info = array();
// 		$sql = "SELECT DISTINCT *, 
// 		(SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "plan_images i WHERE i.plan_id = p.plan_id AND i.is_deleted = '0' GROUP BY i.plan_id) AS total_image, 
// 		(SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "plan_survey_data s WHERE s.plan_id = p.plan_id GROUP BY s.plan_id) AS total_answer, 
// 		(SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "plan_coupon_history c WHERE c.plan_id = p.plan_id AND c.user_id >0 AND c.coupon_code != '' GROUP BY c.plan_id) AS total_coupon FROM " . PDB_PREFIX . "plan p WHERE p.plan_id = '" . (int)$plan_id . "'";
		
// 		$query = $this->pdb->query($sql);
// 		if($query->row){
// 				$plan_info = $query->row;
// 		}
// 	 	return self::$static_plan[$plan_id] = $plan_info;
// 	}

// 	public function getPlansByUserId($user_id) {
// 	   $plan_data = array();
// 	   if($user_id>0){
// 		   $sql = "SELECT * FROM " . PDB_PREFIX . "plan pl LEFT JOIN " . PDB_PREFIX . "store s ON (s.store_id = pl.store_id) WHERE pl.is_deleted = '0' AND DATE(pl.`date_end`) >= DATE(NOW())";
// 		   if($user_id!=0&&$user_id!=''){
// 			$sql .= " AND pl.user_id = '" . (int)$user_id . "'";
// 		   }
// 		   $query = $this->pdb->query($sql);
// 		   if($query->num_rows){
// 			foreach($query->rows as $plan){
// 			 $plan_data[] = $plan;
// 			}
// 		   }
// 	  }
// 	  return $plan_data;
// 	}
// 	public function getPlansByStoreId($store_id, $plan_id=0) {
// 		$sql = "SELECT *,pl.image_overview as image_overview FROM " . DB_PREFIX . "plan pl 
// 		   LEFT JOIN " . DB_PREFIX . "store s ON (s.store_id = pl.store_id) 
// 		   WHERE pl.store_id = '" . (int)$store_id . "' AND pl.plan_id != '" . (int)$plan_id . "' AND pl.is_deleted = '0' AND pl.plan_status = '1'";
// 		   $query = $this->pdb->query($sql);
// 	  return $query->rows;
// 	}

// 	/* Planogram survey */
// 	public function getPlanSurvey($plan_id,$survey_id) {
// 		$survey_answer = array();
// 		$query = $this->pdb->query("SELECT * FROM `" . PDB_PREFIX . "plan_survey_data` WHERE `plan_id` = '" . (int)$plan_id . "' AND  `survey_id` = '" . (int)$survey_id . "' ORDER BY `question_id` ASC");
// 		if($query->num_rows){
// 			foreach($query->rows as $answer){
// 				$survey_answer[$answer['question_id']][$answer['answer_id']] = array(
// 					'input_type'=> $answer['input_type'],
// 					'question_id'=> $answer['question_id'],
// 					'answer_id'=> $answer['answer_id'],
// 					'answer_value'=> $answer['answer_value']
// 				);
// 				foreach($query->rows as $an){
// 					if($answer['question_id']==$an['question_id']&&$answer['survey_data_id']!==$an['survey_data_id']){
// 						$survey_answer[$answer['question_id']][$an['answer_id']] = array(
// 							'input_type'=> $an['input_type'],
// 							'question_id'=> $an['question_id'],
// 							'answer_id'=> $an['answer_id'],
// 							'answer_value'=> $an['answer_value']
// 						);
// 					}
// 				}
// 			}

// 		}
// 		return $survey_answer;
// 	}
// 	/***************************************************************************************************************/

// 	public function deletePlan($plan_id) {
// 		$this->pdb->query("UPDATE `" . PDB_PREFIX . "plan` SET is_deleted = '1', user_deleted = '" . $this->pdb->escape($this->user->getId()) . "' WHERE plan_id = '" . (int)$plan_id . "'");
// 	}

// 	public function getStoreByStoreCode($store_code) {
// 		$query = $this->pdb->query("SELECT * FROM `" . DB_PREFIX . "store` WHERE store_code = '" . $this->pdb->escape($store_code) . "'");

// 		return $query->row;
// 	}
	

// 	public function getTotalPlansByType($type='plan_rating',$data = array(),$allow = array()) {
// 		$sql = "SELECT `".$type."`, COUNT(*) AS total FROM " . PDB_PREFIX . "plan pl WHERE  pl.is_deleted = '0'";

// 			/*Filter start*/
// 		$implode = array();

// 		if (in_array('filter_round',$allow)&&!empty($data['filter_round'])) {
// 			$implode[] = "pl.round_name LIKE '%" . $this->pdb->escape($data['filter_round']) . "%'";
// 		}
// 		if (in_array('filter_name',$allow)&&!empty($data['filter_name'])) {
// 			$implode[] = "pl.plan_name LIKE '%" . $this->pdb->escape($data['filter_name']) . "%'";
// 		}
// 		if (in_array('filter_area_code',$allow)&&!empty($data['filter_area_code'])) {
// 			$implode[] = "pl.area_code = '" .$this->pdb->escape($data['filter_area_code']) . "'";
// 		}
// 		if (in_array('filter_region_codes',$allow)&&!empty($data['filter_region_codes'])){
// 			$sql .= " AND pl.region_code IN (" .$data['filter_region_codes'] . ")";
// 		}
// 		if (in_array('filter_region_code',$allow)&&!empty($data['filter_region_code'])) {
// 			$implode[] = "pl.region_code = '" .$this->pdb->escape($data['filter_region_code']) . "'";
// 		}
// 		if (in_array('filter_province_ids',$allow)&&!empty($data['filter_province_ids'])) {
// 			//$implode[] = "pl.province_id IN (" . $this->db->escape($data['filter_province_ids']) . ")";
// 		}
// 		if (in_array('filter_province_id',$allow)&&!empty($data['filter_province_id'])) {
// 			$implode[] = "pl.province_id = '" . (int)$data['filter_province_id'] . "'";
// 		}
// 		if (in_array('filter_rating_status',$allow)&&isset($data['filter_rating_status'])&& !is_null($data['filter_rating_status'])) {
// 			//$implode[] = "pl.plan_rating = '" . (int)$data['filter_rating_status'] . "'";// AND pl.reason_id = 0
// 		}
// 		if (in_array('filter_plan_status',$allow)&&isset($data['filter_plan_status'])&& !is_null($data['filter_plan_status'])) {
// 			$implode[] = "pl.plan_status = '" . (int)$data['filter_plan_status'] . "'";
// 		}
// 		if (in_array('filter_qc_status',$allow)&&isset($data['filter_qc_status'])&& !is_null($data['filter_qc_status'])) {
// 			$implode[] = "pl.plan_qc = '" . (int)$data['filter_qc_status'] . "'";
// 		}
// 		if (in_array('filter_user_ids',$allow)&&!empty($data['filter_user_ids'])) {
// 			$implode[] = "pl.user_id IN('".implode("','",$data['filter_user_ids'])."')";
// 		}
// 		if (in_array('filter_user',$allow)&&!empty($data['filter_user'])) {
// 			$implode[] = "pl.user_id = '" . (int)$data['filter_user'] . "'";
// 		}
		
// 		if (in_array('customers_manager',$allow)&&!empty($data['customers_manager'])) {
// 			$implode[] = "pl.`customer_user_id` IN('".implode("','",$data['customers_manager'])."')";
// 		}
// 		if (in_array('filter_customer_user_id',$allow)&&!empty($data['filter_customer_user_id'])) {
// 			$implode[] = "pl.customer_user_id = '" . (int)$data['filter_customer_user_id'] . "'";
// 		}
// 		if (in_array('filter_date_checkin',$allow)&&!empty($data['filter_date_checkin'])) {
// 		   $implode[] = "pl.time_checkin LIKE '" . $data['filter_date_checkin'] . "%'";
// 			$sql .= " AND pl.time_checkin !='0000-00-00 00:00:00'";
// 		}
// 		if (in_array('filter_date',$allow)&&!empty($data['filter_date'])) {
// 		   $implode[] = "pl.time_upload LIKE '" . $data['filter_date'] . "%'";
// 		}
// 		if (in_array('filter_upload_count',$allow)&&!empty($data['filter_upload_count'])) {
// 		   $implode[] = "pl.upload_count = '" . $data['filter_upload_count'] . "'";
// 		}
// 		if (in_array('filter_version',$allow)&&!empty($data['filter_version'])) {
// 		   $implode[] = "pl.version = '" . $data['filter_version'] . "'";
// 		}

// 		if ($implode) {
// 			$sql .= " AND " . implode(" AND ", $implode);
// 		}

// 		$sql .= " GROUP BY pl.`".$type."`";
// 			/*Filter end*/
// 		$query = $this->pdb->query($sql);
// 			$plan_total = array();
// 		foreach($query->rows as $row){
// 				$plan_total[$row[$type]] = $row['total'];
// 		}
// 		return $plan_total;
// 	}
// 	public function getUserByProvinceFilter($data = array()) {
// 		$sql = "SELECT `province_id`,`user_id` FROM " . PDB_PREFIX . "plan pl WHERE 1";
// 		$sql .= " GROUP BY pl.`province_id`,`user_id`";

// 		$query = $this->pdb->query($sql);
// 		$province_user = array();
// 		foreach($query->rows as $row){
// 				$province_user[$row['province_id']][] = $row['user_id'];
// 		}
// 		return $province_user;

// 	}
	
// 	public function checkCouponDuplicate($plan_id, $coupon_history_id,$coupon_code) {
// 		$sql = "SELECT COUNT(*) AS total FROM `" . PDB_PREFIX . "plan_coupon_history` WHERE `plan_id`>0 AND `plan_id` = '" . (int)$plan_id . "' AND `coupon_history_id` != '" . (int)$coupon_history_id . "' AND `coupon_code` = '" . $this->pdb->escape(trim($coupon_code)) . "'";
// 		$query = $this->pdb->query($sql);
// 		return $query->row['total'];
// 	}
// 	public function checkCouponDouble($coupon_history_id,$coupon_code) {
// 		$sql = "SELECT COUNT(*) AS total FROM `" . PDB_PREFIX . "plan_coupon_history` WHERE `plan_id`>0 AND `coupon_history_id` != '" . (int)$coupon_history_id . "' AND `coupon_id` > 0 AND `coupon_code` = '" . $this->db->escape(trim($coupon_code)) . "' AND `coupon_status_id` = '" . (int)$this->config->get('config_coupon_has_paid') . "'";
// 		$query = $this->pdb->query($sql);
		
// 		return $query->row['total'];
// 	}
// 	public function updateStoreImage($store_id,$store_image,$is_null=0){
// 		$sql = "UPDATE `" . DB_PREFIX . "store` SET store_image = '" . $this->pdb->escape($store_image). "' WHERE `store_id` = '" . (int)$store_id . "'";
// 		if($is_null==1){
// 			$sql .= " AND `store_image` = ''";
// 		}
// 		$this->pdb->query($sql);
// 	}
// 	public function updateRead($plan_id,$data) {
// 		$sql = "UPDATE " . PDB_PREFIX . "plan SET";
// 		if(is_array($data['users_read'])){
// 			$sql .= " users_read = '" . implode(',',$data['users_read']). "',";
// 		}
// 		$sql .= " date_modified = NOW() WHERE plan_id = '" . (int)$plan_id . "'";
// 		$this->pdb->query($sql);
// 	}
// 	public function getPlanSurveyIDs($plan_id) {
// 		$survey_ids = array();
// 		$query = $this->pdb->query("SELECT * FROM `" . PDB_PREFIX . "plan_survey` WHERE plan_id = '" . (int)$plan_id . "' ORDER BY `plan_survey_id` ASC");
// 		if($query->num_rows){
// 			foreach($query->rows as $row){
// 				$survey_ids[] = 	$row['survey_id'];
// 			}
// 		}
		
// 		return $survey_ids;
// 	}
// 	public static $survey_ids = array();
	
// 	public function getPlanSurveys($plan_id) {
// 		if ( ! empty(self::$survey_ids[$plan_id])) {
// 			return self::$survey_ids[$plan_id];
// 		}
// 		$plan_surveys = array();
// 		$query = $this->pdb->query("SELECT * FROM `" . PDB_PREFIX . "plan_survey` WHERE plan_id = '" . (int)$plan_id . "' ORDER BY `plan_survey_id` ASC");
// 		if($query->num_rows){
// 			foreach($query->rows as $row){
// 				$plan_surveys[$row['survey_id']] = 	$row;
// 			}
// 		}
		
// 		return self::$survey_ids[$plan_id] = $plan_surveys;
		
// 	}
	
// 	public function getPlansSurveys($filter){
		
// 		$this->load->model('survey/survey');
// 		$survey = $this->model_survey_survey->getSurveyIndexBy('survey_id');
		
		
// 		$rating_result = array(
// 				'0'=>'KTC',
// 				'1'=>'Đạt',
// 				'-1'=>'Không đạt',
// 				'-2'=>'KTC',
// 			);
			
// 		$sql = "SELECT * FROM " . PDB_PREFIX . "plan_survey WHERE survey_id>0";

// 		if (isset($filter['filter_round'])&&!empty($filter['filter_round'])) {
// 			$sql .= " AND round_name = '" . $this->pdb->escape($filter['filter_round']) . "'";
// 		}
// 			$sql .= " ORDER BY `rating_manual` ASC";
// 		$query = $this->pdb->query($sql);

// 		$plan_survey = array();
// 		if($query->rows){
// 			foreach($query->rows as $row){
// 				$row['rating_result'] = $rating_result[(int)$row['rating_manual']];
// 				$row['survey_name'] = $survey[$row['survey_id']]['survey_name'];
// 				$plan_survey[$row['plan_id']][$row['survey_id']] = $row;
// 			}
// 		}
// 		return $plan_survey;
// 	}

// 	public function getAudios($plan_id) {
// 		$sql = "SELECT * FROM " . DB_PREFIX . "audio WHERE is_deleted = 0 AND plan_id = " . $plan_id;
// 		return $this->pdb->query($sql)->rows;
// 	}

// 	public function addAudio($data) {
// 		$sql = "INSERT INTO " . DB_PREFIX . "audio SET ";
// 		foreach ($data as $key => $value) {
// 			$sql .= "{$key} = '" . $this->pdb->escape($value) . "',";
// 		}
// 		$sql .= "date_added = NOW()";
// 		$this->pdb->query($sql);
// 		return $this->pdb->getLastId();
// 	}

// 	public function getAudio($id) {
// 		$sql = "SELECT * FROM " . DB_PREFIX . "audio WHERE is_deleted = 0 AND id = " . $id;
// 		return $this->pdb->query($sql)->row;
// 	}
	
// 	public function deleteAudio($audio_id) {
// 		$config_project_folder = $this->config->get('config_project_folder');
// 		$audio = $this->getAudio($audio_id);
// 		$plan = $this->getPlan($audio['plan_id']);
// 		$old_path = DIR_MEDIA.$audio['filename'];
// 		$new_path = DIR_MEDIA.'files/'.$config_project_folder.'/deleted/'.$plan['store_code'].'/audio/';
// 		if (file_exists($old_path)) {
// 		 	@mkdir($new_path,  0777, true);
// 			copy($old_path, $new_path.$audio['filename']);
// 		}
// 		if (file_exists($old_path)&&file_exists($new_path)) {
// 			unlink($old_path);
// 		}
// 		$sql = "UPDATE " . DB_PREFIX . "audio SET is_deleted = 1 WHERE id = " . (int)$audio_id;
// 		$this->pdb->query($sql);
// 	}
// }