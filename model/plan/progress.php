<?php

class ModelPlanProgress extends Model {

	public function getAllSupPlanConfirms($plan_id) {
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "plan_confirm qc WHERE qc.plan_id = '" . (int)$plan_id . "'");
		return $query->rows;
	}
	/* SON */
 	public function getTotalPlansMadeGroupByUserId($data = array()) {
	  $results = array();
	  $sql = "SELECT COUNT(*) AS total, user_id FROM `" . PDB_PREFIX . "plan` WHERE `plan_status` = '1' AND is_deleted = '0'";

	  if (!empty($data['filter_date'])&& !is_null($data['filter_date'])) {
			$sql .= "  AND DATE(`time_upload`) <= DATE('" . $data['filter_date'] . "')";
		}

		if ((!empty($data['filter_date_start'])&& !is_null($data['filter_date_start'])) && (!empty($data['filter_date_end'])&& !is_null($data['filter_date_end']))) {
			$sql .= " AND DATE(`time_upload`) BETWEEN '" . $data['filter_date_start'] . "' AND '". $data['filter_date_end'] ."'";
		}

		if(isset($data['filter_group_id']) && !empty($data['filter_group_id'])){
			$sql .= " AND `group_id` = '" . $this->pdb->escape($data['filter_group_id']) . "'";
		}
		if(isset($data['filter_group_id']) && !empty($data['filter_group_id'])){
			$sql .= " AND `group_id` = '" . $this->pdb->escape($data['filter_group_id']) . "'";
		}
		if(isset($data['filter_round_name']) && !empty($data['filter_round_name'])){
			$sql .= " AND `round_name` = '" . $this->pdb->escape($data['filter_round_name']). "'";
		}

	  $sql .= " GROUP BY `user_id`";

	  $query = $this->pdb->query($sql);
	  if($query->rows){
	  	foreach($query->rows as $row){
	  		$results[$row['user_id']] = $row['total'];
	  	}
	  }

	  return $results;
 	}
 	public function getTotalPlanNotesGroupByUserId($data = array()) {
	  $results = array();
	  
		$sql = "SELECT a.note_id, COUNT(a.note_id) as total, b.user_id as user_id 
		FROM `" . PDB_PREFIX . "plan_note` a LEFT JOIN `" . PDB_PREFIX . "plan` b ON(a.plan_id=b.plan_id) 
		WHERE b.`plan_status` = '1' AND b.`plan_qc` = '1' AND b.is_deleted = '0' AND a.is_deleted = '0' ";
	
		if(isset($data['filter_group_id']) && !empty($data['filter_group_id'])){
			$sql .= " AND b.`group_id` = '" . $this->pdb->escape($data['filter_group_id']) . "'";
		}
		if(isset($data['filter_round_name']) && !empty($data['filter_round_name'])){
			$sql .= " AND b.`round_name` = '" . $this->pdb->escape($data['filter_round_name']) . "'";
		}
		
		$sql .= " GROUP BY b.user_id,a.note_id";
		
		$query = $this->pdb->query($sql);
		
	  if($query->rows){
	  	foreach($query->rows as $row){
	  		$results[$row['user_id']][$row['note_id']] = $row['total'];
	  	}
	  }

	  return $results;
 	}
 	public function getTotalProblemsGroupByUserId($data = array()) {
	  $results = array();
	  
		$sql = "SELECT COUNT(DISTINCT(a.plan_id)) as total, b.user_id as user_id 
		FROM `" . PDB_PREFIX . "plan_code` a LEFT JOIN `" . PDB_PREFIX . "plan` b ON(a.plan_id=b.plan_id) 
		WHERE b.`plan_status` = '1' AND b.`plan_qc` = '1' AND b.is_deleted = '0'";
	

		if(isset($data['filter_round_name']) && !empty($data['filter_round_name'])){
			$sql .= " AND b.`round_name` = '" . $this->pdb->escape($data['filter_round_name']) . "'";
		}
		if(isset($data['filter_group_id']) && !empty($data['filter_group_id'])){
			$sql .= " AND b.`group_id` = '" . $this->pdb->escape($data['filter_group_id']) . "'";
		}
		
		$list_ignore = $this->config->get('config_ignore_code');
		
		if(!empty($list_ignore)){
			$sql .= " AND a.`problem_id` NOT IN('".implode("','",$list_ignore)."')";
		}
		$sql .= " GROUP BY b.user_id";
			
		$query = $this->pdb->query($sql);
		
	  if($query->rows){
	  	foreach($query->rows as $row){
	  		$results[$row['user_id']] = $row['total'];
	  	}
	  }

	  return $results;
 	}
	
 	public function getTotalPlansQcGroupByUserId($data = array()) {
	  $results = array();
	  $sql = "SELECT COUNT(*) AS total, user_id FROM `" . PDB_PREFIX . "plan` WHERE `plan_status` = '1' AND `plan_qc` = '1'  AND is_deleted = '0'";

	  if (!empty($data['filter_date'])&& !is_null($data['filter_date'])) {
			$sql .= "  AND DATE(`time_upload`) <= DATE('" . $data['filter_date'] . "')";
		}

		if ((!empty($data['filter_date_start'])&& !is_null($data['filter_date_start'])) && (!empty($data['filter_date_end'])&& !is_null($data['filter_date_end']))) {
			$sql .= " AND DATE(`time_upload`) BETWEEN '" . $data['filter_date_start'] . "' AND '". $data['filter_date_end'] ."'";
		}
		if(isset($data['filter_group_id']) && !empty($data['filter_group_id'])){
			$sql .= " AND `group_id` = '" . $this->pdb->escape($data['filter_group_id']) . "'";
		}
		if(isset($data['filter_round_name']) && !empty($data['filter_round_name'])){
			$sql .= " AND `round_name` = '" . $this->pdb->escape($data['filter_round_name']) . "'";
		}

	  $sql .= " GROUP BY `user_id`";

	  $query = $this->pdb->query($sql);
	  if($query->rows){
	  	foreach($query->rows as $row){
	  		$results[$row['user_id']] = $row['total'];
	  	}
	  }

	  return $results;
 	}

 	public function getTotalPlansQcByGroupId($user_group_id, $data = array()) {
	  $results = array();
	  $sql = "SELECT pl.user_id, qc.date_added, qc.user_group_id, qc.user_id AS qc_user_id 
	  FROM `" . PDB_PREFIX . "plan_confirm` qc INNER JOIN `".PDB_PREFIX."plan` pl ON(qc.plan_id=pl.plan_id) 
	  WHERE pl.`plan_status` = '1' AND qc.`user_group_id` = '".(int)$user_group_id."' AND pl.is_deleted = '0'";

	  if (!empty($data['filter_date'])&& !is_null($data['filter_date'])) {
			$sql .= " AND DATE(`time_upload`) <= DATE('" . $data['filter_date'] . "')";
		}

		if ((!empty($data['filter_date_start'])&& !is_null($data['filter_date_start'])) && (!empty($data['filter_date_end'])&& !is_null($data['filter_date_end']))) {
			$sql .= " AND DATE(pl.`time_upload`) BETWEEN '" . $data['filter_date_start'] . "' AND '". $data['filter_date_end'] ."'";
		}
		if(isset($data['filter_group_id']) && !empty($data['filter_group_id'])){
			$sql .= " AND `group_id` = '" . $this->pdb->escape($data['filter_group_id']) . "'";
		}
		if(isset($data['filter_round_name']) && !empty($data['filter_round_name'])){
			$sql .= " AND pl.`round_name` = '" . $this->pdb->escape($data['filter_round_name']) . "'";
		}

	  $query = $this->pdb->query($sql);
	  if($query->rows){
	  	foreach($query->rows as $row){
 				$results[$row['user_id']][] = array('date_added' => $row['date_added']);
 			}
	  }

	  return $results;
 	}

 	public function getTotalPlansTodayGroupByUserId($data = array()) {
	  $results = array();
	  if (isset($data['filter_date']) && !empty($data['filter_date'])) {
	  	$today = $data['filter_date'];
	  } else {
	  	$today = date('Y-m-d');
	  }
	  $sql = "SELECT COUNT(*) AS total, user_id FROM `" . PDB_PREFIX . "plan` pl WHERE pl.plan_status = 1 AND pl.is_deleted = 0 AND pl.`time_upload` LIKE '".$today."%' ";
	  if(isset($data['filter_group_id']) && !empty($data['filter_group_id'])){
			$sql .= " AND `group_id` = '" . $this->pdb->escape($data['filter_group_id']) . "'";
		}
	  if(isset($data['filter_round_name']) && !empty($data['filter_round_name'])){
	  	$sql .= " AND pl.`round_name` = '".$this->pdb->escape($data['filter_round_name'])."'";
	  }

	  $sql .= " GROUP BY pl.`user_id`";
	  
	  $query = $this->pdb->query($sql);
	  if($query->rows){
	  	foreach($query->rows as $row){
	  		$results[$row['user_id']] = $row['total'];
	  	}
	  }

	  return $results;
 	}

 	public function getTotalPlansReasonGroupByUserId($data = array()) {
	  $results = array();
	  $sql = "SELECT COUNT(*) as total, user_id FROM `" . PDB_PREFIX . "plan` WHERE `plan_status` = '1' AND is_deleted = '0'";
	  $sql2 = "SELECT COUNT(*) as ktc, user_id FROM `" . PDB_PREFIX . "plan` WHERE `plan_status` = '1' AND reason_id > 0 AND is_deleted = '0'";

	  if (!empty($data['filter_date'])&& !is_null($data['filter_date'])) {
			$sql .= "  AND DATE(`time_upload`) <= DATE('" . $data['filter_date'] . "')";
		}

		if ((!empty($data['filter_date_start'])&& !is_null($data['filter_date_start'])) && (!empty($data['filter_date_end'])&& !is_null($data['filter_date_end']))) {
			$sql .= " AND DATE(`time_upload`) BETWEEN '" . $data['filter_date_start'] . "' AND '". $data['filter_date_end'] ."'";
			$sql2 .= " AND DATE(`time_upload`) BETWEEN '" . $data['filter_date_start'] . "' AND '". $data['filter_date_end'] ."'";
		}

		if(isset($data['filter_group_id']) && !empty($data['filter_group_id'])){
			$sql .= " AND `group_id` = '" . $this->pdb->escape($data['filter_group_id']) . "'";
			$sql2 .= " AND `group_id` = '" . $this->pdb->escape($data['filter_group_id']) . "'";
		}

		if(isset($data['filter_round_name']) && !empty($data['filter_round_name'])){
			$sql .= " AND `round_name` = '" . $this->pdb->escape($data['filter_round_name']) . "'";
			$sql2 .= " AND `round_name` = '" . $this->pdb->escape($data['filter_round_name']) . "'";
		}

	  $sql .= " GROUP BY `user_id`";
	  $sql2 .= " GROUP BY `user_id`";

	  $query = $this->pdb->query($sql);
	  if($query->rows){
	  	foreach($query->rows as $row){
	  		$results[$row['user_id']]['total'] = $row['total'];
	  	}
	  }

	  $query2 = $this->pdb->query($sql2);
	  if($query2->rows){
	  	foreach($query2->rows as $row){
	  		if(isset($row['ktc'])){
	  			$results[$row['user_id']]['ktc'] = $row['ktc'];
	  		} else {
	  			$results[$row['user_id']]['ktc'] = 0;
	  		}
	  	}
	  }

	  foreach($results as $user_id => $result){
		  
	  	if(isset($result['total'])){
			if(isset($result['ktc'])){
				$results[$user_id]['tc'] = $result['total'] - $result['ktc'];
			} else {
				$results[$user_id]['tc'] = $result['total'];
			}
		} else {
				$results[$user_id]['tc'] = 0;
		}

	  	if(!isset($result['ktc'])){
	  		$results[$user_id]['ktc'] = 0;
	  	}
	  }

	  return $results;
 	}

 	public function getTotalChanelGroupByUser() {
 		$sql = "SELECT user_id, chanel, count(*) as total FROM " . PDB_PREFIX . "plan pl
 				LEFT JOIN " . PDB_PREFIX . "store s ON s.store_id = pl.store_id
 				WHERE pl.is_deleted = 0 AND plan_status = 1
 				GROUP BY user_id, chanel";
 		$datas = $this->pdb->query($sql)->rows;
 		$results = array();
 		foreach ($datas as $data) {
 			$results[$data['user_id']][$data['chanel']] = $data['total'];
 		}
 		return $results;
 	}

 	public function getTotalPosmGroupByUser() {
 		$sql = "SELECT user_id, attribute_id, sum(a.value) as total FROM " . PDB_PREFIX . "plan pl
 				LEFT JOIN " . PDB_PREFIX . "attribute_data a ON a.plan_id = pl.plan_id
 				WHERE pl.is_deleted = 0 AND plan_status = 1 AND value != '' AND value != 0
 				GROUP BY user_id, attribute_id";
 		$datas = $this->pdb->query($sql)->rows;
 		$results = array();
 		foreach ($datas as $data) {
 			$results[$data['user_id']][$data['attribute_id']] = $data['total'];
 		}
 		return $results;
 	}

 	public function getCheckInGroupByUser($data) {
 		$sql = "SELECT user_id, count(*) as total FROM cpm_plan WHERE is_deleted = 0 AND time_checkin LIKE '" . $data['filter_date'] . "%'";

 		if(isset($data['filter_group_id']) && !empty($data['filter_group_id'])){
			$sql .= " AND `group_id` = '" . $this->pdb->escape($data['filter_group_id']) . "'";
		}

		if(isset($data['filter_round_name']) && !empty($data['filter_round_name'])){
			$sql .= " AND `round_name` = '" . $this->pdb->escape($data['filter_round_name']) . "'";
		}
 		$sql .= " GROUP BY user_id";
 		$datas = $this->pdb->query($sql)->rows;
 		$results = array();
 		foreach ($datas as $data) {
 			$results[$data['user_id']] = $data['total'];
 		}
 		return $results;
 	}
}

?>