<?php
class ModelStoreStore extends Model {
	private $no_filter_key = array(
		'_url',
		'route',
		'filter_global',
		'start',
		'limit',
		'sort',
		'order',
		'page'
	);

	private $operator = array();
	public function countTotal($data = array()) {
		$sql = "SELECT count(*) as total FROM " . DB_PREFIX . "store s WHERE is_deleted = 0";
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				if (!in_array($key, $this->no_filter_key)) {
					$key = str_replace('filter_', '', $key);
					$sql .= " AND {$key} = '" . $this->pdb->escape($value) . "'";
				}	
			}

			if (!empty($data['filter_global'])) {
				$filter_global = array(
					's.store_name',
					's.store_code',
					's.store_phone',
					's.store_address_raw',
					'customer_code'
				);
				$sql .= " AND (";
				foreach ($filter_global as $filter) {
					$implode[] = " LCASE(".$filter.") LIKE '%" . $this->pdb->escape(utf8_strtolower($data['filter_global'])) . "%'";
				}
				$sql .= " " . implode(" OR ", $implode) . "";
				$sql .= ")";
			}
		}

		return $this->pdb->query($sql)->row['total'];
	}

	public function getStores($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "store s WHERE is_deleted = 0";
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				if (!in_array($key, $this->no_filter_key)) {
					$key = str_replace('filter_', '', $key);
					$sql .= " AND {$key} = '" . $this->pdb->escape($value) . "'";
				}	
			}

			if (!empty($data['filter_global'])) {
				$filter_global = array(
					's.store_name',
					's.store_code',
					's.store_phone',
					's.store_address_raw',
					'customer_code'
				);
				$sql .= " AND (";
				foreach ($filter_global as $filter) {
					$implode[] = " LCASE(".$filter.") LIKE '%" . $this->pdb->escape(utf8_strtolower($data['filter_global'])) . "%'";
				}
				$sql .= " " . implode(" OR ", $implode) . "";
				$sql .= ")";
			}

			if (isset($data['start']) && isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}
				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}
		}
		
		return $this->pdb->query($sql)->rows;
	}

	public function getStore($store_id) {
		$sql = "SELECT * FROM " . DB_PREFIX . "store WHERE is_deleted = 0 AND store_id = " . (int)$store_id;
		return $this->pdb->query($sql)->row;
	}
	
	public function addStore($data = array()) {
		$sql = "INSERT INTO `" . PDB_PREFIX . "store` SET ";
		foreach ($data as $field => $value) {
			$sql .= " $field = '" . $this->pdb->escape($value) . "', ";
		}
		$sql .= "date_added = NOW()";
		$this->pdb->query($sql);
		return $this->pdb->getLastId();
	}

	public function updateStore($store_id, $data) {
		$sql = "UPDATE `" . PDB_PREFIX . "store` SET ";
		foreach ($data as $field => $value) {
			$sql .= " $field = '" . $this->pdb->escape($value) . "', ";
		}
		$sql .= "date_modified = NOW() WHERE store_id = " . (int)$store_id;
		$this->pdb->query($sql);
	}

	public function deleteStore($store_id) {
		$sql = "UPDATE " . DB_PREFIX . "store SET is_deleted = 1, user_deleted = " . $this->user->getId() . " WHERE store_id = " . (int)$store_id;
		$this->pdb->query($sql);
	}

	public function getStoreByStoreCode($store_code) {
		$query = $this->pdb->query("SELECT * FROM `" . PDB_PREFIX . "store` WHERE store_code = '" . $this->pdb->escape($store_code) . "'");
		return $query->row;
	}

	public function getChanels() {
		$sql = "SELECT * FROM " . PDB_PREFIX . "chanel";
		return $this->pdb->query($sql)->rows;
	}
}