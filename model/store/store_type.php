<?php
class ModelStoreStoreType extends Model {
	public function addStoreType($data) {
		$this->pdb->query("INSERT INTO " . PDB_PREFIX . "store_type SET type_name = '" . $this->pdb->escape($data['type_name']) . "', type_description = '" . $this->pdb->escape($data['type_description']) . "'");
	
		return $this->pdb->getLastId();
	}

	public function editStoreType($store_type_id, $data) {
		$this->pdb->query("UPDATE " . PDB_PREFIX . "store_type SET type_name = '" . $this->pdb->escape($data['type_name']) . "',type_description = '" . $this->pdb->escape($data['type_description']) . "' WHERE store_type_id = '" . (int)$store_type_id . "'");
	}

	public function deleteStoreType($store_type_id) {
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "store_type WHERE store_type_id = '" . (int)$store_type_id . "'");
	}

	public function getStoreType($store_type_id) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "store_type WHERE store_type_id = '" . (int)$store_type_id . "'");

		$type_name = array(
			'store_type_id'       => $query->row['store_type_id'],
			'type_name'       => $query->row['type_name'],
			'type_description'       => $query->row['type_description']
		);

		return $type_name;
	}

	public function getAllTypes() {
		$type_data = array();
		$sql = "SELECT * FROM " . PDB_PREFIX . "store_type";
		$query = $this->pdb->query($sql);
		foreach($query->rows as $row){
			$type_data[$row['store_type_id']] = $row;
		}
		return $type_data;
	}
	public function getStoreTypes($data = array()) {
		$sql = "SELECT * FROM " . PDB_PREFIX . "store_type";

		$sql .= " ORDER BY store_type_id";

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->pdb->query($sql);

		return $query->rows;
	}

	public function getTotalStoreTypes() {
		$query = $this->pdb->query("SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "store_type");

		return $query->row['total'];
	}
}