<?php

$_['heading_title']    = 'Import/Export Store';

$_['text_import']      = 'Import';

$_['text_error']       = 'Error';

$_['text_filename']    = 'Filename';

$_['text_action']      = 'Action';


$_['text_success_row'] = 'Success Data';

$_['text_error_row']   = 'Error Data';

$_['text_importing']   = 'Importing.....';

$_['text_error_upload']     = 'File could not be uploaded!';

$_['text_error_filetype']   = 'Invalid file type!';

?>