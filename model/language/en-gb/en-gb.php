<?php
// Locale
$_['code']                  = 'en';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd/m/Y';
$_['date_format_long']      = 'l dS F Y';
$_['time_format']           = 'h:i:s A';
$_['datetime_format']       = 'd/m/Y H:i:s';
$_['decimal_point']         = '.';
$_['thousand_point']        = ',';
$_['text_years']               = 'years';
$_['text_months']               = 'months';
$_['text_days']               = 'days';
$_['text_hours']               = 'hours';
$_['text_minutes']               = 'minutes';
$_['text_seconds']               = 'seconds';
$_['text_checkin']              = 'Checkin:';
$_['text_time_diff']              = 'Time diff';



$_['text_action']               = 'Action';
$_['text_code']           = 'Code';
$_['text_point']            = 'Point';
$_['text_point_total']            = 'Point total';
$_['text_planogram']        = 'Planogram';
$_['text_staff_user'] = 'Nhân viên';
$_['text_reward'] = 'Reward';
$_['text_rewarded'] = 'Rewarded';
$_['text_search_image'] = 'Search image';
$_['text_manager'] = 'Manager';
$_['text_staff'] = 'Staff';
$_['text_store'] = 'Store';
$_['text_reward_type'] = 'Reward type';
$_['text_canceled'] = 'Canceled';
$_['text_unused'] = 'Unused';
$_['text_store_type']              = 'Store type';
$_['text_coupon_num']              = 'Coupon num';
$_['text_coupon_value']              = 'Coupon value';
$_['text_pass_point']              = 'Pass point';
$_['text_passratio']              = 'Pass ratio';
$_['text_coupon_check']              = 'Use coupon';
$_['text_all_store_code']              = 'CPM/DEM/AD code';
$_['text_distributor_check']              = 'Distributor check';
$_['text_not_recovered']              = 'Not yet recovered';
$_['text_store_customer_code']        = 'Mã DEM %s';
$_['text_store_ad_code']        = 'AD code';
$_['text_project_progress']   	= 'Project progress';
$_['text_qc_status']        = 'QC status';
$_['text_has_qc']        = 'Has QC';
$_['text_no_qc']        = 'No QC';
$_['text_checkin']              = 'Checkin:';


$_['text_reward_month'] = 'Reward month';
$_['text_round_month'] = 'Plan month';
$_['text_coupon_month']             = 'Month give coupon';
$_['text_is_rating']              = 'Is rating ?';
$_['text_coupon_prefix']              = 'Coupon prefix';
$_['text_sup_user']        = 'SUP';
$_['text_username']           = 'Username';
$_['text_plan_name']     = 'Plan name';
$_['text_server_rating']  = 'Server rating';
$_['text_staff_rating']  = 'Staff rating';
$_['text_password']           = 'Password';
$_['text_project_user']     = 'CPM project user';
$_['text_total_row']           = 'Total rows';
$_['text_date_added']       = 'Date added';
$_['text_approved']              = 'Approved';
$_['text_store_code']              = 'CPM Store code';
$_['text_user_route']              = 'User route';
$_['text_online_history']              = 'Lịch sử đăng nhập';




$_['text_description']              = 'Description';

$_['text_date_start']           = 'Date start';
$_['text_date_end']           = 'Date end';
$_['text_from_date']           = 'Từ ngày';
$_['text_to_date']           = 'Đến ngày';

$_['text_country']          = 'Country';
$_['text_longitude']       = 'Longitude';
$_['text_latitude']       = 'Latitude';
$_['text_province']           = 'Province';
$_['text_district']           = 'District';
$_['text_ward']               = 'Ward';
$_['text_place']            = 'Place';
$_['text_address']           = 'Address';
$_['text_address_update']           = 'Address Update';
$_['text_telephone']                   = 'Phone';
$_['text_telephone_update']                   = 'Phone Update';
$_['text_phone']                   = 'Phone';
$_['text_coupon_scan']           = 'Coupon Scan Status';


$_['text_customer_user']     = 'User of Nivea';

$_['text_distributor_role']           = 'Distributor role';

$_['text_customer_level']     = 'Customer user group';
$_['text_customer_role']     = 'Customer user role';
$_['text_date'] = 'Date';
$_['text_sort_order'] = 'Sort order';
$_['text_point'] = 'Point';
$_['text_ratio'] = 'Ratio';
$_['text_import'] = 'Import';
$_['text_download_results'] = 'Download results';
$_['text_download_report'] = 'Download report';
$_['text_reward_type'] = 'Reward type';
$_['text_coupon_audit'] = 'Coupon audit';
$_['text_coupon_sales'] = 'Coupon sales';
$_['text_month'] = 'Month';
$_['text_coupon_code'] = 'Coupon code';
$_['text_user_guide'] = 'User guide';
$_['text_round'] = 'Month';
$_['text_rounds']           = 'Rounds';
$_['text_belong_to'] = 'Belong to';
$_['text_project_settings']              = 'Project settings';
$_['text_group']              = 'Group';
$_['text_attribute']              = 'Attribute';


$_['text_total']        = 'Total';
$_['text_details']        = 'Details';

$_['text_rating_passed']        = 'Passed';
$_['text_rating_not_passed']        = 'Not Pass';
$_['text_rating_success']        = 'Success';
$_['text_rating_unsuccess']        = 'Unsuccess';
$_['text_note_unsuccess']        	= 'Note KTC';
$_['text_plan_reason']      = 'Unsuccess reason';


$_['text_export_history']   = 'Export history';
$_['text_import_history']   = 'Import history';

// Text
$_['text_has_perform']        = 'Has_performn';
$_['text_no_perform']        = 'Not perform';
$_['text_catalog']        	= 'Product Management';
$_['text_product']        	= 'Product';
$_['text_category']        	= 'Category';
$_['text_distributor_area']        = 'Distributor area';
$_['text_distributor']        = 'Distributor';
$_['text_distributor_code']        = 'Distributor Code';
$_['text_capacity']        = 'Capacity';
$_['text_coupon']        = 'Coupon';
$_['text_coupon_status']        = 'Coupon status';
$_['text_check_coupon']        = 'Check coupon';
$_['text_coupon_history']        = 'Coupon history';
 
$_['text_user']             = 'User';
$_['text_add_user']             = 'Add user';
$_['text_none_group_user']             = 'User in project';

$_['text_all']             = ' -- All -- ';
$_['text_nationwide']             = 'Nationwide';
$_['text_rsm']              = 'RSM';
$_['text_asm']              = 'ASM';
$_['text_ssup']              = 'SUP sale';
$_['text_plan_result']           = 'Plan result';
$_['text_today']            = 'Today';


$_['text_general'] = 'General';
$_['text_yes']                      = 'Yes';
$_['text_no']                       = 'No';
$_['text_enabled']                  = 'Enabled';
$_['text_disabled']                 = 'Disabled';
$_['text_none']                     = ' --- None --- ';
$_['text_select']                   = ' --- Please Select --- ';
$_['text_select_all']               = 'Select All';
$_['text_unselect_all']             = 'Unselect All';
$_['text_all_zones']                = 'All Zones';
$_['text_default']                  = ' <b>(Default)</b>';
$_['text_close']                    = 'Close';
$_['text_loading']                  = 'Loading...';
$_['text_no_results']               = 'No results!';
$_['text_no_changes']         = 'No changes!';
$_['text_home']                     = 'Home';
$_['text_confirm']                  = 'Are you sure?';
$_['button_add']                    = 'Add New';
$_['text_search']                   = 'Search';
$_['text_action']                   = 'Action';
$_['entry_prefix']                   = 'Prefix';
$_['text_title']            = 'Title';
$_['text_area']               = 'Area';
$_['text_region']                  = 'Region';
$_['text_address']            = 'Address';
 
$_['text_status']     = 'Status';
$_['text_active']     = 'Active';
$_['text_deactive']   = 'Deactive';
$_['text_reactive']   = 'Re-active';
$_['text_log']              = 'Log';



// Button
$_['button_check']            = 'Check';
$_['text_confirm']                  = 'Are you sure?';
$_['button_add']                    = 'Add New';
$_['button_delete']                 = 'Delete';
$_['button_save']                   = 'Save';
$_['button_cancel']                 = 'Cancel';
$_['button_cancel_recurring']       = 'Cancel Recurring Payments';
$_['button_continue']               = 'Continue';
$_['button_clear']                  = 'Clear';
$_['button_close']                  = 'Close';
$_['button_enable']                 = 'Enable';
$_['button_disable']                = 'Disable';
$_['button_filter']                 = 'Filter';
$_['text_filter']                 = 'Filter';
$_['button_send']                   = 'Send';
$_['button_edit']                   = 'Edit';
$_['button_copy']                   = 'Copy';
$_['button_back']                   = 'Back';
$_['button_remove']                 = 'Remove';
$_['button_refresh']                = 'Refresh';
$_['button_export']                 = 'Export';
$_['button_import']                 = 'Import';
$_['button_download']               = 'Download';
$_['button_rebuild']                = 'Rebuild';
$_['button_upload']                 = 'Upload';
$_['button_submit']                 = 'Submit';
$_['button_image_add']              = 'Add Image';
$_['button_history_add']            = 'Add History';
$_['button_route_add']              = 'Add Route';
$_['button_rule_add']               = 'Add Rule';
$_['button_module_add']             = 'Add Module';
$_['button_link_add']               = 'Add Link';
$_['button_approve']                = 'Approve';
$_['button_approve_update']   		= 'Approve update';
$_['button_reset']                  = 'Reset';
$_['button_generate']               = 'Generate';
$_['button_ip_add']                 = 'Add IP';
$_['button_parent']                 = 'Parent';
$_['button_folder']                 = 'New Folder';
$_['text_project_folder']                 = 'Project Folder';
$_['button_search']                 = 'Search';
$_['button_view']                   = 'View';
$_['button_link']                   = 'Link';
$_['button_apply']                  = 'Apply';
$_['button_category_add']           = 'Add Category';

// Text
$_['text_home']             = '<i class="fa fa-home"></i>';
$_['text_yes']              = 'Yes';
$_['text_no']               = 'No';
$_['text_none']             = ' --- None --- ';
$_['text_select']           = ' --- Please Select --- ';
$_['text_all_zones']        = 'All Zones';
$_['text_pagination']       = 'Showing %d &raquo; %d of <span class="badge alert-success">%d</span> (%d Pages)';


// Buttons
$_['button_address_add']    = 'Add Address';
$_['button_back']           = 'Back';
$_['button_continue']       = 'Continue';
$_['button_cart']           = 'Add to Cart';
$_['button_cancel']         = 'Cancel';
$_['button_compare']        = 'Compare this Product';
$_['button_wishlist']       = 'Add to Wish List';
$_['button_checkout']       = 'Checkout';
$_['button_confirm']        = 'Confirm Order';
$_['button_coupon']         = 'Apply Coupon';
$_['button_delete']         = 'Delete';
$_['button_download']       = 'Download';
$_['button_edit']           = 'Edit';
$_['button_view']           = 'View';
$_['button_filter']         = 'Refine Search';
$_['button_new_address']    = 'New Address';
$_['button_change_address'] = 'Change Address';
$_['button_reviews']        = 'Reviews';
$_['button_write']          = 'Write Review';
$_['button_login']          = 'Login';
$_['button_update']         = 'Update';
$_['button_remove']         = 'Remove';
$_['button_reorder']        = 'Reorder';
$_['button_return']         = 'Return';
$_['button_shopping']       = 'Continue Shopping';
$_['button_search']         = 'Search';
$_['button_shipping']       = 'Apply Shipping';
$_['button_submit']         = 'Submit';
$_['button_guest']          = 'Guest Checkout';
$_['button_view']           = 'View';
$_['button_voucher']        = 'Apply Gift Certificate';
$_['button_upload']         = 'Upload File';
$_['button_reward']         = 'Apply Points';
$_['button_quote']          = 'Get Quotes';
$_['button_list']           = 'List';
$_['button_grid']           = 'Grid';
$_['button_map']            = 'View Google Map';
$_['button_clear_data']       = 'Clear all sample data?';
$_['text_reason_not_pass']       = 'Reason not pass';
$_['text_list_of']           = 'List ';


// Text
$_['text_register']    = 'Register';
$_['text_login']       = 'Login';
$_['text_logout']      = 'Logout';
$_['text_forgotten']   = 'Forgotten Password';
$_['text_account']     = 'My Account';
$_['text_edit']        = 'Edit Account';
$_['text_password']    = 'Password';


// Error
$_['text_error_qc']        	= 'Warning: Please check QC code first!';
$_['text_error_empty']        = 'Error: Data is empty!';
$_['text_error_warning']        = 'Warning: Please check the form carefully for errors!';
$_['text_error_exception']       = 'Error Code(%s): %s in %s on line %s';
$_['text_error_upload_1']        = 'Warning: The uploaded file exceeds the upload_max_filesize directive in php.ini!';
$_['text_error_upload_2']        = 'Warning: The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form!';
$_['text_error_upload_3']        = 'Warning: The uploaded file was only partially uploaded!';
$_['text_error_upload_4']        = 'Warning: No file was uploaded!';
$_['text_error_upload_6']        = 'Warning: Missing a temporary folder!';
$_['text_error_upload_7']        = 'Warning: Failed to write file to disk!';
$_['text_error_upload_8']        = 'Warning: File upload stopped by extension!';
$_['text_error_upload_999']      = 'Warning: No error code available!';
$_['text_error_curl']            = 'CURL: Error Code(%s): %s';
$_['text_error_password']  = 'Password must be between 4 and 20 characters!';
