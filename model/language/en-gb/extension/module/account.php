<?php
// Heading
$_['heading_title']    = 'Account';



$_['text_heading']   = 'Administration';
$_['text_login']     = 'Account - Login details.';
$_['text_forgotten'] = 'Forgotten Password';

// Entry
$_['entry_username'] = 'Username';
$_['entry_password'] = 'Password';

// Button
$_['button_login']   = 'Login';

// Error
$_['text_error_login']    = 'No match for Username and/or Password.';
$_['text_error_token']    = 'Invalid token session. Please login again.';