<?php
// Heading
$_['heading_title']           = 'Provinces';
$_['group_title']           = 'System';

// Text
$_['text_success']            = 'Success: You have modified Provinces!';
$_['text_list']               = 'Province List';
$_['text_add']                = 'Add Province';
$_['text_edit']               = 'Edit Province';
$_['form_add'] = 'Add new';
$_['form_edit'] = 'Edit ID# %s';

// Column
$_['text_name']             = 'Province Name';
$_['text_prefix']       = 'Level';
$_['text_action']           = 'Action';

// Entry
$_['text_name']              = 'Province Name';
$_['text_code']        = 'Code';
$_['text_address_format']    = 'Address Format';
$_['text_postcode_required'] = 'Postcode Required';


// Help
$_['help_address_format']     = 'Full Name = {fullname}<br />Last Name = {lastname}<br />Company = {company}<br />Address 1 = {address_1}<br />Address 2 = {address_2}<br />City = {city}<br />Postcode = {postcode}<br />Zone = {zone}<br />Zone Code = {zone_code}<br />Province = {country}';

// Error
$_['text_error_permission']        = 'Warning: You do not have permission to modify Provinces!';
$_['text_error_name']              = 'Province Name must be between 3 and 128 characters!';
$_['text_error_default']           = 'Warning: This country cannot be deleted as it is currently assigned as the default store country!';
$_['text_error_store']             = 'Warning: This country cannot be deleted as it is currently assigned to %s stores!';
$_['text_error_address']           = 'Warning: This country cannot be deleted as it is currently assigned to %s address book entries!';
$_['text_error_affiliate']         = 'Warning: This country cannot be deleted as it is currently assigned to %s affiliates!';
$_['text_error_zone']              = 'Warning: This country cannot be deleted as it is currently assigned to %s zones!';
$_['text_error_zone_to_geo_zone']  = 'Warning: This country cannot be deleted as it is currently assigned to %s zones to geo zones!';