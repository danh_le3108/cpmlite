<?php
// Heading
$_['heading_title']           = 'Place localitys';
$_['group_title']           = 'System';

// Text
$_['text_success']            = 'Success: You have modified places!';
$_['text_list']               = 'Place locality List';
$_['text_add']                = 'Add Place locality';
$_['text_edit']               = 'Edit Place locality';


// Column
$_['text_name']             = 'Place locality Name';
$_['text_prefix']       = 'Level';
$_['text_action']           = 'Action';

// Entry
$_['text_name']              = 'Name';
$_['text_code']        = 'Code';


// Error
$_['text_error_permission']        = 'Warning: You do not have permission to modify places!';
$_['text_error_name']              = 'Place locality Name must be between 3 and 128 characters!';
$_['text_error_default']           = 'Warning: This country cannot be deleted as it is currently assigned as the default store country!';
$_['text_error_store']             = 'Warning: This country cannot be deleted as it is currently assigned to %s stores!';
$_['text_error_address']           = 'Warning: This country cannot be deleted as it is currently assigned to %s address book entries!';
$_['text_error_affiliate']         = 'Warning: This country cannot be deleted as it is currently assigned to %s affiliates!';
$_['text_error_zone']              = 'Warning: This country cannot be deleted as it is currently assigned to %s zones!';
$_['text_error_zone_to_geo_zone']  = 'Warning: This country cannot be deleted as it is currently assigned to %s zones to geo zones!';