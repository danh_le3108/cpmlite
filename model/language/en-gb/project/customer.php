<?php
// Heading
$_['heading_title']       = 'User of Customer';

// Text
$_['text_success']        = 'Thành công: Đã cập nhật Project Users!';
$_['text_list']           = 'Project Users List';
$_['text_add']            = 'Add Project Users';
$_['text_edit']           = 'Edit Project Users';
$_['text_remove']         = 'Remove';
$_['text_route']          = 'Choose the store and routes to be used with this Project Users';
$_['text_module']         = 'Choose the position of the modules';
$_['text_default']        = 'Default';
$_['text_user']           = 'User';
$_['text_none_group_user']   = 'None group users';
$_['text_user_group']     = 'Role';
$_['text_add']     		= 'Thêm';
$_['text_add_user']     = 'Thêm nhân viên';
$_['text_add_group']     = 'Add group';
$_['text_child_group']     = 'Child group';
$_['text_add_staff']     = 'Thêm staff';
$_['text_staff']     = '&raquo;';
$_['text_user']     = 'Nhân viên';
$_['text_group_name']     = 'Group name';
$_['text_description']     = 'Description';

// Quickadd
$_['text_fullname']     = 'Họ tên';
$_['text_identity_number']     = 'Số CMND';
$_['text_email']     = 'Email';
$_['text_phone']     = 'Điện thoại';
$_['text_workplace']     = 'Nơi làm việc';
$_['text_user_group']     = 'Nhóm';

// Column
$_['column_name']         = 'Dự án';
$_['column_action']       = 'Hành động';

// Entry
$_['entry_name']          = 'Dự án';
$_['entry_project']       = 'Dự án';
$_['entry_route']         = 'Route';
$_['entry_module']        = 'Module';

// Text
$_['text_success']               = 'Success: You have modified customer users!';
$_['text_group']                  = 'User group';
$_['text_username']              = 'Username';
$_['text_fullname']              = 'Fullname';
$_['text_email']              = 'Email';
$_['text_password']              = 'Password';
$_['text_password_confirm']              = 'Confirm Password';
$_['text_telephone']              = 'Telephone';

$_['text_ip']              = 'IP';
$_['text_action']              = 'Action';


$_['text_filter']                = 'Search';
$_['help_filter']                = 'Support search: Username, Fullname, Email, Telephone, Code...';
 
// Error
$_['text_error_permission']      = 'Warning: You don\'t have permission!';
$_['text_error_exists']          = 'Warning: E-Mail Address is already registered!';
$_['text_error_username_exists'] = 'Warning: Username is already registered!';
$_['text_error_name']        = 'Name  must be between 2 and 32 characters!';
$_['text_error_fullname']        = 'Full Name must be between 1 and 32 characters!';
$_['text_error_lastname']        = 'Last Name must be between 1 and 32 characters!';
$_['text_error_email']           = 'E-Mail Address does not appear to be valid!';
$_['text_error_telephone']       = 'Telephone must be between 3 and 32 characters!';
$_['text_error_password']        = 'Password must be between 4 and 20 characters!';
$_['text_error_confirm']         = 'Password and password confirmation do not match!';