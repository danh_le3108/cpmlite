<?php
// Heading
$_['heading_title']       = 'Project Users';

// Text
$_['text_success']        = 'Success: You have modified Project Users!';
$_['text_list']           = 'Project Users List';
$_['text_add']            = 'Add Project Users';
$_['text_edit']           = 'Edit Project Users';
$_['text_remove']         = 'Remove';
$_['text_route']          = 'Choose the store and routes to be used with this Project Users';
$_['text_module']         = 'Choose the position of the modules';
$_['text_default']        = 'Default';
$_['text_user']           = 'User';
$_['text_none_group_user']   = 'None group user';
$_['text_user_group']     = 'Role';
$_['text_add']     = 'Add';
$_['text_add_user']     = 'Select user';
$_['text_add_staff']     = 'Add staff';
$_['text_staff']     = '&raquo;';

// Quickadd
$_['text_fullname']     = 'Họ tên';
$_['text_identity_number']     = 'Số CMND';
$_['text_email']     = 'Email';
$_['text_phone']     = 'Điện thoại';
$_['text_workplace']     = 'Nơi làm việc';
$_['text_user_group']     = 'Nhóm';


// Column
$_['column_name']         = 'Project';
$_['column_action']       = 'Action';

// Entry
$_['entry_name']          = 'Project';
$_['entry_project']         = 'Project';
$_['entry_route']         = 'Route';
$_['entry_module']        = 'Module';

// Error
$_['text_error_permission']    = 'Warning: You do not have permission to modify Project Users!';
$_['text_error_name']          = 'Layout Name must be between 3 and 64 characters!';
$_['text_error_default']       = 'Warning: This layout cannot be deleted as it is currently assigned as the default store Project Users!';
$_['text_error_store']         = 'Warning: This layout cannot be deleted as it is currently assigned to %s stores!';
$_['text_error_product']       = 'Warning: This layout cannot be deleted as it is currently assigned to %s products!';
$_['text_error_category']      = 'Warning: This layout cannot be deleted as it is currently assigned to %s categories!';
$_['text_error_information']   = 'Warning: This layout cannot be deleted as it is currently assigned to %s information pages!';