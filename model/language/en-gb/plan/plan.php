<?php
// Heading
$_['heading_title']           = 'Plan';
$_['group_title']           = 'System';
$_['heading_store_info']           = 'Store info';
$_['heading_plan_info']           = 'Plan info';
$_['heading_image_store']           = 'Store Images';
$_['heading_image_audit']           = 'Audit Images';
$_['heading_image_coupon']          = 'Coupon Images';
$_['heading_plan_survey']           = 'Survey form';
$_['heading_audit_result']           = 'Audit result';
$_['heading_confirm']           = 'Customer confirm';
$_['heading_note']           = 'Note';
$_['heading_qc']           = 'QC code';
$_['heading_qc_confirm']  = 'Qc confirm';
$_['heading_reward']  = 'Reward';
$_['heading_plan_note']  = 'Plan Note';
$_['heading_plan_history']  = 'Plan history';
$_['text_point_total'] = 'Point total';
$_['text_result_before'] = 'Old result';
$_['text_result_after'] = 'New result';
$_['text_route'] = 'Route';
 
// Text
$_['text_success']            = 'Success: You have modified plans!';
$_['text_list']               = 'Plan List';
$_['text_add']                = 'Add Plan';
$_['text_edit']               = 'Edit Plan';


$_['text_province']               = 'Province';
$_['text_ward']               = 'Ward';
$_['text_comment']            = 'Comment';
$_['text_plan_status']        = 'Plan status';
$_['text_history_add']        = 'Add Plan history';
$_['button_history_add']        = 'Add history';
$_['text_related_planogram']        = 'Related Planogram';
$_['text_has_perform']        = 'Has perform';
$_['text_no_perform']        = 'No perform';
$_['help_unsuccess']        = 'Survey have Unsuccess reason, please unselect Un success reason to survey again.';
$_['text_unsuccess_reason']        = 'Un success reason';
$_['text_unsuccess_comment']        = 'Orther comment';
$_['text_upload_manual']        = 'Upload manual';
$_['text_upload_app']        = 'Upload through app';
$_['button_confirm_sign']        = 'Confirm as signed';
$_['button_confirm_passed']        = 'Confirm data passed';

$_['text_has_confirm']        = 'Đã xác nhận';
$_['text_time_frame']        = 'Time frame';
$_['text_from']        = 'From';
$_['text_to']        = 'To';
$_['text_store_distributor']        = 'Nhà CC';

$_['text_audit_result']        = 'Audit result';
$_['text_s']        = 'Success';
$_['text_u']        = 'Unsuccess';
$_['text_formula']       = 'Formula';
$_['text_value']        = 'Value';
$_['text_result']        = 'Result';
$_['text_end_result']        = 'End result';
$_['text_sum_total']        = 'Average total';


$_['text_update_info']               = 'Update info';
$_['text_call_confirm']               = 'Confirm call';
$_['text_call_recipient']               = 'Caller';
$_['text_call_phone_number']               = 'Call phone number';
$_['text_call_date']               = 'Call_date';
$_['text_call_num']               = 'Number of calls';
$_['text_call_note']               = 'Call note';


// Column
$_['text_plan']             = 'Plan';
$_['text_map']             = 'Map';
$_['text_checkin_c']         = 'Checkin coordinates';
$_['text_checkin']         = 'Checkin';
$_['text_plan_survey']   = 'Store type';
$_['text_prefix']       = 'Level';
$_['text_action']           = 'Action';
$_['text_plan_status']           = 'Plan status';
$_['text_staff_user']        = 'Staff user';
$_['text_time_checkin']           = 'Checkin';
$_['text_time_upload']           = 'Upload time';
$_['text_coordinates']           = 'Checkin Coordinates';
$_['text_rating']           = 'Rating';

// Entry
$_['entry_store']              = 'Store: ';
$_['text_store_name']              = 'Store Name';
$_['text_user']              = 'User';
$_['text_staff_user']        = 'Audit staff';
$_['text_code']        = 'Code';
$_['text_address_format']    = 'Address Format';
$_['text_postcode_required'] = 'Postcode Required';


// Error
$_['text_error_coupon']        = 'Warning: Passed %s coupon(s), please check again!';
$_['text_success_coupon']        = 'Success: Passed %s coupon(s), now please click Save!';


$_['text_error_permission']        = 'Warning: You do not have permission to modify plans!';
$_['text_error_note_type']         = 'Please select note type!';
$_['text_error_name']              = 'Plan Name must be between 3 and 128 characters!';
$_['text_error_default']           = 'Warning: This country cannot be deleted as it is currently assigned as the default store country!';
$_['text_error_store']             = 'Warning: This country cannot be deleted as it is currently assigned to %s stores!';
$_['text_error_address']           = 'Warning: This country cannot be deleted as it is currently assigned to %s address book entries!';
$_['text_error_affiliate']         = 'Warning: This country cannot be deleted as it is currently assigned to %s affiliates!';
$_['text_error_zone']              = 'Warning: This country cannot be deleted as it is currently assigned to %s zones!';
$_['text_error_zone_to_geo_zone']  = 'Warning: This country cannot be deleted as it is currently assigned to %s zones to geo zones!';