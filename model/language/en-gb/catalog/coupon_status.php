<?php

// Heading

$_['heading_title']           = 'CouponStatus';

$_['group_title']           = 'System';



// Text

$_['text_success']            = 'Success: You have modified CouponStatus!';

$_['text_list']               = 'CouponStatus List';

$_['text_add']                = 'Add CouponStatus';

$_['text_edit']               = 'Edit CouponStatus';



// Column

$_['text_action']           = 'Action';

// Entry

$_['text_name']              = 'CouponStatus Name';

$_['text_code']        = 'CouponStatus';




// Error

$_['text_error_permission']        = 'Warning: You do not have permission to modify Codes!';

$_['text_error_name']              = 'CouponStatus Name must be between 3 and 128 characters!';