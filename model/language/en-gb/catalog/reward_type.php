<?php

// Heading

$_['heading_title']           = 'Reward Type';

$_['group_title']           = 'System';



// Text

$_['text_success']            = 'Success: You have modified Reward Type!';

$_['text_list']               = 'Reward Type List';

$_['text_add']                = 'Add Reward Type';

$_['text_edit']               = 'Edit Reward Type';



// Column

$_['text_action']           = 'Action';

// Entry

$_['text_name']              = 'Reward Type Name';

$_['text_code']        = 'Reward Type';




// Error

$_['text_error_permission']        = 'Warning: You do not have permission to modify Codes!';

$_['text_error_name']              = 'Reward Type Name must be between 3 and 128 characters!';