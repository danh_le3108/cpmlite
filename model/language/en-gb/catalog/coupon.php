<?php

// Heading

$_['heading_title']           = 'Coupons';

$_['group_title']           = 'System';



// Text

$_['text_success']            = 'Success: You have modified Coupons!';

$_['text_list']               = 'Coupon List';

$_['text_add']                = 'Add Coupon';

$_['text_edit']               = 'Edit Coupon';



// Column

$_['text_name']             = 'Coupon Name';

$_['text_action']           = 'Action';

$_['text_code']           = 'Code';



// Entry

$_['text_name']              = 'Coupon Name';

$_['text_code']           = 'Code';





// Error

$_['text_error_permission']        = 'Warning: You do not have permission to modify Coupons!';

$_['text_error_name']              = 'Coupon Name must be between 3 and 128 characters!';