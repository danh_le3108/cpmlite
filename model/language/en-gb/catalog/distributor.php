<?php

// Heading

$_['heading_title']           = 'Distributors';

$_['group_title']           = 'System';



// Text

$_['text_success']            = 'Success: You have modified Distributors!';

$_['text_list']               = 'Distributor List';

$_['text_add']                = 'Add Distributor';

$_['text_edit']               = 'Edit Distributor';



// Column

$_['text_name']             = 'Distributor Name';

$_['text_action']           = 'Action';

$_['text_code']           = 'Code';



// Entry

$_['text_name']              = 'Distributor Name';





// Error

$_['text_error_permission']        = 'Warning: You do not have permission to modify Distributors!';

$_['text_error_name']              = 'Distributor Name must be between 3 and 128 characters!';