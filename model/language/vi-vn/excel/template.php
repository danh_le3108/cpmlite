<?php
// Heading
$_['heading_title']    = 'Excel Template';

// Text
$_['text_success']        = 'Thành công: Đã cập nhật templates!';
$_['text_list']           = 'Template List';
$_['text_add']                = 'Thêm';
$_['text_edit']           = 'Edit Template';
$_['text_remove']         = 'Remove';
$_['text_table']          = 'Choose the table to be used with this template';
$_['text_module']         = 'Choose the position of the modules';
$_['text_default']        = 'Default';
$_['text_upload']        = 'Upload';
$_['text_download']        = 'Download';
$_['text_export_history']   = 'Export history';
$_['text_import_history']   = 'Import history';

// Column
$_['column_name']         = 'Template Name';
$_['column_action']       = 'Action';
$_['text_filename']       = 'Filename';
$_['text_success_row']       = 'Tạo mới';
$_['text_update_row']       = 'Cập nhật';
$_['text_error_row']       = 'Lỗi';

// Entry
$_['entry_name']          = 'Template Name';
$_['entry_table']          = 'Table';
$_['entry_field']         = 'Table > Column';
$_['entry_label']         = 'As Title';
$_['entry_based']         = 'Based on';
$_['entry_required']        = 'Required';
$_['entry_overwrite']        = 'Overwrite';
$_['entry_hbg']        = 'Heading Bg';
$_['entry_hcolor']        = 'Heading Color';
$_['entry_ptable']        = 'Primary Table';
$_['entry_start_row']        = 'Start row';
$_['entry_unique']        = 'Unique';
$_['entry_unique_update']        = 'Unique & Update';
$_['entry_file']          = 'File';


$_['text_import']      = 'Import';

$_['text_error']       = 'Error';

$_['text_filename']    = 'Filename';

$_['text_action']      = 'Action';


$_['text_success_row'] = 'Success Data';

$_['text_error_row']   = 'Error Data';

$_['text_importing']   = 'Importing.....';

$_['text_success']   = 'Import success!!!';

$_['text_error_upload']     = 'File could not be uploaded!';

$_['text_error_filetype']   = 'Invalid file type!';

$_['button_download_plan']   = 'Download Plan';


// Error
$_['text_error_template']          = 'Unknown template!';
$_['text_error_permission']    = 'Warning: Bạn không có quyền sửa templates!';
$_['text_error_name']          = 'Template Name must be between 3 and 64 characters!';
$_['text_error_table']          = 'Database table required!';
$_['text_error_default']       = 'Warning: This template cannot be deleted as it is currently assigned as the default store template!';
$_['text_error_store']         = 'Warning: This template cannot be deleted as it is currently assigned to %s stores!';
$_['text_error_product']       = 'Warning: This template cannot be deleted as it is currently assigned to %s products!';
$_['text_error_category']      = 'Warning: This template cannot be deleted as it is currently assigned to %s categories!';
$_['text_error_information']   = 'Warning: This template cannot be deleted as it is currently assigned to %s information pages!';