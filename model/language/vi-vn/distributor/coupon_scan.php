<?php

// Heading

$_['heading_title']           = 'Trạng thái thu hồi';

$_['group_title']           = 'System';



// Text

$_['text_success']            = 'Success: You have modified Trạng thái thu hồi!';

$_['text_list']               = 'Trạng thái thu hồi List';

$_['text_add']                = 'Thêm';

$_['text_edit']               = 'Edit Trạng thái thu hồi';



// Column

$_['text_action']           = 'Thao tác';

// Entry

$_['text_name']              = 'Trạng thái thu hồi Name';

$_['text_code']        = 'Trạng thái thu hồi';

$_['text_status']            = 'Trạng thái';


// Error

$_['text_error_permission']        = 'Warning: You do not have permission to modify Codes!';

$_['text_error_name']              = 'Trạng thái thu hồi Name must be between 3 and 128 characters!';

