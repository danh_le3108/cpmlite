<?php
// Text
$_['text_success'] = 'Success!';
$_['text_success_changepass'] = 'Success: Password changed!';

// Error
$_['text_error_key']  = 'Warning: Incorrect API Key!';
$_['text_error_ip']   = 'Warning: Your IP %s is not allowed to access this API!';
$_['text_error_permission']   = 'Warning: You do not have permission to access the API!';
$_['text_error_fullname']    = 'Full Name must be between 1 and 32 characters!';
$_['text_error_email']        = 'E-Mail Address does not appear to be valid!';
$_['text_error_telephone']    = 'Telephone must be between 3 and 32 characters!';


// Error
$_['text_error_info'] = 'Warning: Not found user information on the system!';
$_['text_error_login']  = 'Warning: No match for Username and/or Password.';
$_['text_error_key']  = 'Warning: Incorrect API Key!';
$_['text_error_login']  = 'Warning: Please login!';