<?php
// Heading
$_['heading_title']	   = 'Error Log';

// Text
$_['text_success']	   = 'Thành công: Đã xóa lịch sử lỗi!';
$_['text_list']        = 'Errors List';

// Error
$_['text_error_Cảnh báo']	   = 'Cảnh báo: Your error log file %s is %s!';
$_['text_error_permission'] = 'Cảnh báo: You do not have permission to clear error log!';