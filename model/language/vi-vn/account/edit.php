<?php
// Heading 
$_['heading_title']     = 'Thông Tin Tài Khoản';

// Text
$_['text_account']      = 'Tài khoản';
$_['text_edit']         = 'Sửa thông tin';
$_['text_your_details'] = 'Thông tin cá nhân';
$_['text_success']      = 'Thành công: Tài khoản của bạn đã được cập nhật.';

// Entry
$_['entry_firstname']  = 'Tên đầy đủ:';
$_['entry_email']      = 'E-Mail:';
$_['entry_telephone']  = 'Điện thoại:';
$_['entry_fax']        = 'Số Fax:';

// Error
$_['text_error_exists']     = 'Lỗi: E-Mail đã có người sử dụng!';
$_['text_error_firstname']  = 'Tên phải từ 1 đến 64 kí tự!';
$_['text_error_email']      = 'E-Mail không hợp lệ!';
$_['text_error_telephone']  = 'Điện thoại phải từ 3 đến 32 kí tự!';
?>