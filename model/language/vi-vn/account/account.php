<?php
// Heading 
$_['heading_title']      = 'Thông tin tài khoản';

// Text
$_['text_account']       = 'Tài khoản';
$_['text_my_account']    = 'Tài khoản của tôi';
$_['text_edit']          = 'Thay đổi thông tin tài khoản';
$_['text_password']      = 'Thay đổi mật khẩu';
?>