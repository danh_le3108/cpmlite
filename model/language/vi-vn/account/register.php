<?php
// Heading 
$_['heading_title']        = 'Đăng Kí Tài Khoản';

// Text
$_['text_account']         = 'Tài khoản';
$_['text_register']        = 'Đăng Kí';
$_['text_account_already'] = 'Nếu bạn đã đăng kí tài khoản, vui lòng đăng nhập tại <a href="%s">đây</a>.';
$_['text_your_details']    = 'Thông tin cá nhân';
$_['text_your_password']   = 'Mật khẩu';
$_['text_agree']           = 'Tôi đã đọc và đồng ý với điều khoản <a class="fancybox" href="%s" alt="%s"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'Nhóm khách hàng';
$_['entry_firstname']           = 'Tên đầy đủ:';
$_['entry_email']               = 'Địa chỉ E-Mail:';
$_['entry_email_confirm']       = 'Xác nhận E-Mail:';
$_['entry_telephone']           = 'Điện Thoại:';
$_['entry_password']            = 'Mật Khẩu:';
$_['entry_confirm']             = 'Nhập lại Mật Khẩu:';

// Error
$_['text_error_exists']      = 'Lỗi: E-Mail đã có người sử dụng!';
$_['text_error_firstname']   = 'Tên phải từ 1 đến 32 kí tự!';
$_['text_error_email']       = 'E-Mail không hợp lệ!';
$_['text_error_telephone']   = 'Điện thoại phải từ 3 đến 32 kí tự!';
$_['text_error_password']    = 'Mật khẩu phải từ 4 đến 20 kí tự!';
$_['text_error_confirm']     = 'Nhập lại mật khẩu không chính xác!';
?>