<?php
// Heading
$_['heading_title']    = 'Tài khoản';

// Text
$_['text_heading']   = 'Administration';
$_['text_login']     = 'Account - Đăng nhập';
$_['text_forgotten'] = 'Quên mật khẩu?';

// Entry
$_['entry_username'] = 'Tên đăng nhập';
$_['entry_password'] = 'Mật khẩu';

// Button
$_['button_login']   = 'Đăng nhập';

// Error
$_['text_error_login']    = 'Tên đăng nhập hoặc mật khẩu không chính xác.';
$_['text_error_token']    = 'Phiên đăng nhập hết hạn. Hãy đăng nhập lại.';