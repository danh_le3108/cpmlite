<?php
// Heading
$_['heading_title']              = 'Users';
$_['group_title']                = 'Users';

// Text
$_['text_general']               = 'General';
$_['text_user_info']             = 'CPM info';
$_['text_personal_info']         = 'Personal info';
$_['text_user_level']            = 'CPM Level';
$_['text_none']                  = ' -- None -- ';
$_['text_clear']                 = 'Clear';
$_['text_filter']                = 'Search';
$_['help_filter']                = 'Support search: Username, Fullname, Email, Telephone, Code...';


// Text
$_['text_workplace']                 = 'Workplace';


$_['text_user_image']     				= 'Image';
$_['text_front_identity_card']      = 'Front identity card';
$_['text_back_identity_card']       = 'Back identity card';
$_['text_portrait']                 = 'Image Portrait';
$_['text_signature']                = 'Image Signature';



// Text
$_['text_success']               = 'Thành công: Đã cập nhật users!';
$_['text_list']                  = 'User List';
$_['text_add']                = 'Thêm';
$_['text_edit']                  = 'Edit User';
$_['text_avatar']                = 'Avatar';
$_['text_phone']                 = 'Phone number';
$_['text_tax_code']              = 'Tax code';
$_['text_identity_number']       = 'Identity number';
$_['text_identity_issue_date']   = 'Identity issuedate';
$_['text_identity_issue_place']  = 'Identity issued place';
$_['text_insurance_num']         = 'Insurance number';
$_['text_social_insurance_num']  = 'Social insurance NUM';
$_['text_relative_telephone']    = 'Relative telephone';
$_['text_belonging_persons']     = 'Belonging persons';

// Column
$_['text_username']              = 'Username';
$_['text_usercode']              = 'CPM code';
$_['text_fullname']              = 'Fullname';
$_['text_action']                = 'Action';
$_['text_user_register']         = 'Register by';
$_['text_delete_selected']       = 'Delete selected';

// Entry
$_['text_user_department']       = 'Department';
$_['text_user_group']            = 'User Group';
$_['text_password']              = 'Password';
$_['text_password_confirm']      = 'Confirm';
$_['text_phone_email']           = 'Phone/Email';
$_['text_fullname']              = 'Full Name';
$_['text_lastname']              = 'Last Name';
$_['text_email']                 = 'E-Mail';
$_['text_image']                 = 'Image';
$_['text_other_info']            = 'Other Info';
$_['text_birthday']              = 'Birthday';
$_['text_place_of_birth']        = 'Place of birth';
$_['text_gender']                = 'Gender';
$_['text_male']                  = 'Male';
$_['text_female']                = 'Female';
$_['text_nationality']           = 'Nationality';
$_['text_nation']                = 'Nation';
$_['text_tax_code']              = 'Tax code';
$_['text_degree']                = 'Degree';
$_['text_residence_information'] = 'Residence Information';
$_['text_sojourn_information']   = 'Sojourn Information';
$_['text_birth_information']     = 'Birth information';
$_['text_protector']             = 'Protector';
$_['text_bank_account']          = 'Bank Account';
$_['text_bank_holder']           = 'Account holder';
$_['text_bank_number']           = 'Bank account number';
$_['text_bank_name']             = 'Bank name';
// Error
$_['text_error_permission']      = 'Warning: Bạn không có quyền sửa users!';
$_['text_error_account']         = 'Warning: You can not delete your own account!';
$_['text_error_exists_usercode'] = 'Warning: CPM code is already in use!';
$_['text_error_exists_username'] = 'Warning: Username is already in use!';
$_['text_error_username']        = 'Username must be between 2 and 20 characters!';
$_['text_error_password']        = 'Password must be between 4 and 20 characters!';
$_['text_error_confirm']         = 'Password and password confirmation do not match!';
$_['text_error_fullname']        = 'Full Name must be between 1 and 32 characters!';
$_['text_error_lastname']        = 'Last Name must be between 1 and 32 characters!';
$_['text_error_email']           = 'E-Mail Address does not appear to be valid!';
$_['text_error_exists_email']    = 'Warning: E-Mail Address is already registered!';
$_['error_filename']    = 'Error: Filename must be between 3 and 64 characters!';
$_['error_filetype']    = 'Error: Invalid file type! File type allow: .jpg';
$_['error_upload']      = 'Error: Upload required!';
$_['error_upload_file_size']      = 'Error: File size upload limit maximum 40KB!';
$_['error_upload_file_dimension'] = 'Error: Image dimension must not exceed 480px x 480px!';
$_['text_success_upload']     		= 'Success upload your avatar';
$_['text_error_province_id']     	= 'Workplace required';
$_['text_error_identity_number']     		= 'Identity number required';
$_['text_error_telephone']     		= 'Telephone required';

