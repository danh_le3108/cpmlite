<?php
// Text
$_['text_information']  = 'Thông tin';
$_['text_service']      = 'Chăm sóc khách hàng';
$_['text_extra']        = 'Chức năng khác';
$_['text_contact']      = 'Liên hệ';
$_['text_sitemap']      = 'Sơ đồ trang';
$_['text_account']      = 'Tài khoản của tôi';
$_['text_powered']      = 'Bản quyền của <a href="http://www.opencart.com"> Opencart</a><br /> %s &copy; %s';