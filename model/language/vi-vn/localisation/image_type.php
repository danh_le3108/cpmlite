<?php

// Heading

$_['heading_title']           = 'Loại hình ảnh';

$_['group_title']           = 'System';



// Text

$_['text_success']            = 'Success: You have modified Loại hình ảnh!';

$_['text_list']               = 'Danh sách';

$_['text_add']                = 'Thêm';

$_['text_edit']               = 'Sửa';



// Column

$_['text_action']           = 'Thao tác';

// Entry

$_['text_name']              = 'Tên';

$_['text_code']        = 'ImageType';

$_['text_status']            = 'Trạng thái';


// Error

$_['text_error_permission']        = 'Warning: You do not have permission to modify Loại hình ảnh!';

$_['text_error_name']              = 'ImageType Name must be between 3 and 128 characters!';

