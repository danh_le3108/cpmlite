<?php
// Heading
$_['heading_title']           = 'Places';
$_['group_title']           = 'System';

// Text
$_['text_success']            = 'Thành công: Đã cập nhật places!';
$_['text_list']               = 'Place List';
$_['text_add']                = 'Thêm';
$_['text_edit']               = 'Edit Place';
$_['text_province']               = 'Province';


// Column
$_['text_name']             = 'Place Name';
$_['text_prefix']       = 'Level';
$_['text_longitude']       = 'Longitude';
$_['text_latitude']       = 'Latitude';
$_['text_action']           = 'Action';
$_['text_district']           = 'District';
$_['text_ward']           = 'Ward';

// Entry
$_['text_name']              = 'Place Name';
$_['text_code']        = 'Code';
$_['text_address_format']    = 'Address Format';
$_['text_postcode_required'] = 'Postcode Required';

// Error
$_['text_error_permission']        = 'Warning: Bạn không có quyền sửa places!';
$_['text_error_name']              = 'Place Name must be between 3 and 128 characters!';
$_['text_error_default']           = 'Warning: This country cannot be deleted as it is currently assigned as the default store country!';
$_['text_error_store']             = 'Warning: This country cannot be deleted as it is currently assigned to %s stores!';
$_['text_error_address']           = 'Warning: This country cannot be deleted as it is currently assigned to %s address book entries!';
$_['text_error_affiliate']         = 'Warning: This country cannot be deleted as it is currently assigned to %s affiliates!';
$_['text_error_zone']              = 'Warning: This country cannot be deleted as it is currently assigned to %s zones!';
$_['text_error_zone_to_geo_zone']  = 'Warning: This country cannot be deleted as it is currently assigned to %s zones to geo zones!';