<?php
// Heading
$_['heading_title']           = 'Khu vực';
$_['group_title']           = 'Hệ thống';

// Text
$_['text_Thành công']            = 'Thành công: Sửa Vùng thành công!';
$_['text_list']               = 'Danh sách vùng';
$_['text_add']                = 'Thêm';
$_['text_edit']               = 'Sửa';

$_['form_add'] = 'Thêm';
$_['form_edit'] = 'Sửa ID# %s';

// Column
$_['text_name']             = 'Tên vùng';
$_['text_level']       = 'Level';
$_['text_action']           = 'Thao tác';

// Entry
$_['text_code']        			= 'Code';
$_['text_status']            = 'Trạng thái';

// Error
$_['text_error_permission']        = 'Cảnh báo: Bạn không có quyền sửa!';
$_['text_error_name']              = 'Region Name must be between 3 and 128 characters!';
$_['text_error_default']           = 'Cảnh báo: This country cannot be deleted as it is currently assigned as the default store country!';
$_['text_error_store']             = 'Cảnh báo: This country cannot be deleted as it is currently assigned to %s stores!';
$_['text_error_address']           = 'Cảnh báo: This country cannot be deleted as it is currently assigned to %s address book entries!';
$_['text_error_affiliate']         = 'Cảnh báo: This country cannot be deleted as it is currently assigned to %s affiliates!';
$_['text_error_zone']              = 'Cảnh báo: This country cannot be deleted as it is currently assigned to %s zones!';
$_['text_error_zone_to_geo_zone']  = 'Cảnh báo: This country cannot be deleted as it is currently assigned to %s zones to geo zones!';