<?php
// Heading
$_['heading_title']           = 'Lý do KTC';
$_['group_title']           = 'System';

// Text
$_['text_success']            = 'Thành công: Đã cập nhật Lý do KTC!';
$_['text_list']               = 'Danh sách';
$_['text_add']                = 'Thêm';
$_['text_edit']               = 'Edit Lý do KTC';

$_['form_add'] = 'Add new';
$_['form_edit'] = 'Edit ID# %s';


// Column
$_['text_name']             = 'Lý do';
$_['text_level']       = 'Level';
$_['text_action']           = 'Thao tác';

// Entry
$_['text_name']              = 'Lý do KTC';
$_['text_code']        			= 'Code';

// Error
$_['text_error_permission']        = 'Warning: Bạn không có quyền sửa Reason!';
$_['text_error_name']              = 'Reason Name must be between 3 and 128 characters!';
$_['text_error_default']           = 'Warning: This country cannot be deleted as it is currently assigned as the default store country!';
$_['text_error_store']             = 'Warning: This country cannot be deleted as it is currently assigned to %s stores!';
$_['text_error_address']           = 'Warning: This country cannot be deleted as it is currently assigned to %s address book entries!';
$_['text_error_affiliate']         = 'Warning: This country cannot be deleted as it is currently assigned to %s affiliates!';
$_['text_error_zone']              = 'Warning: This country cannot be deleted as it is currently assigned to %s zones!';
$_['text_error_zone_to_geo_zone']  = 'Warning: This country cannot be deleted as it is currently assigned to %s zones to geo zones!';