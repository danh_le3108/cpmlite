<?php
// Heading
$_['heading_title']           = 'Tỉnh/Thành phố';
$_['group_title']           = 'Hệ thống';

// Text
$_['text_Thành công']            = 'Thành công: Sửa Tỉnh/Thành phố thành công!';
$_['text_list']               = 'Danh sách Tỉnh/Thành phố';
$_['text_add']                = 'Thêm';
$_['text_edit']               = 'Sửa';


$_['form_add'] = 'Thêm';
$_['form_edit'] = 'Sửa ID# %s';

// Column
$_['text_area']           = 'Area';
$_['text_name']             = 'Tên Tỉnh/Thành phố';
$_['text_prefix']       = 'Level';
$_['text_action']           = 'Thao tác';

// Entry
$_['text_code']        = 'Code';
$_['text_address_format']    = 'Address Format';
$_['text_postcode_required'] = 'Postcode Required';

// Help
$_['help_address_format']     = 'Full Name = {fullname}<br />Last Name = {lastname}<br />Company = {company}<br />Address 1 = {address_1}<br />Address 2 = {address_2}<br />City = {city}<br />Postcode = {postcode}<br />Zone = {zone}<br />Zone Code = {zone_code}<br />Province = {country}';

// Error
$_['text_error_permission']        = 'Cảnh báo: Bạn không có quyền sửa!';
$_['text_error_name']              = 'Province Name must be between 3 and 128 characters!';
$_['text_error_default']           = 'Cảnh báo: This country cannot be deleted as it is currently assigned as the default store country!';
$_['text_error_store']             = 'Cảnh báo: This country cannot be deleted as it is currently assigned to %s stores!';
$_['text_error_address']           = 'Cảnh báo: This country cannot be deleted as it is currently assigned to %s address book entries!';
$_['text_error_affiliate']         = 'Cảnh báo: This country cannot be deleted as it is currently assigned to %s affiliates!';
$_['text_error_zone']              = 'Cảnh báo: This country cannot be deleted as it is currently assigned to %s zones!';
$_['text_error_zone_to_geo_zone']  = 'Cảnh báo: This country cannot be deleted as it is currently assigned to %s zones to geo zones!';