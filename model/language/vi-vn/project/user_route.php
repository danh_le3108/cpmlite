<?php
// Heading
$_['heading_title']         = 'Lộ trình thực hiện';

$_['text_sup_user']         = 'Sup';
$_['text_staff_user']       = 'Nhân viên khảo sát';
$_['text_province']         = 'Tỉnh thành';
$_['text_plan_total']       = 'Tổng số plan';
$_['text_progress']         = 'Tiến độ';
$_['text_progress_percent'] = '% Tiến độ';
$_['text_has_qc']           = 'Đã QC';
$_['text_sup_staff']        = 'Sup - NV khảo sát';
$_['text_qc_by_sup']        = 'Sup QC';
$_['text_qc_by_pl']         = 'PL QC';
$_['text_qc_by_dc']         = 'DC QC';
$_['text_qc_by_pa']         = 'PA QC';
$_['text_total_qc']         = 'Tổng đã QC';
$_['text_plan_made']         = 'Đã thực hiện';
