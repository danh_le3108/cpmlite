<?php
// Heading
$_['heading_title']           = 'Plan';
$_['group_title']           = 'System';
$_['heading_store_info']           = 'Thông tin cửa hàng';
$_['heading_plan_info']           = 'Thông tin Plan';
$_['heading_image_store']           = 'Hình ảnh cửa hàng';
$_['heading_image_audit']           = 'Hình ảnh khảo sát';
$_['heading_image_coupon']           = 'Hình ảnh coupon';
$_['heading_plan_survey']       = 'Form khảo sát';
$_['heading_audit_result']           = 'Kết quả khảo sát';
$_['heading_confirm']           = 'Khách hàng xác nhận';
$_['heading_note']           = 'Note';
$_['heading_qc']           = 'QC code';
$_['heading_plan_note']  = 'Plan Note';
$_['heading_qc_confirm']  = 'Qc xác nhận';
$_['heading_reward']  = 'Coupon trả thưởng';

$_['button_confirm_sign']        = 'Xác nhận đã ký';
$_['button_confirm_passed']        = 'Xác nhận data đã đạt chất lượng';

$_['text_has_confirm']        = 'Đã xác nhận';
$_['text_audit_result']        = 'Kết quả khảo sát';
$_['text_store_distributor']        = 'Nhà PP';
$_['text_point_total'] = 'Điểm';
$_['text_result_before'] = 'Old result';
$_['text_result_after'] = 'New result';
$_['text_route'] = 'Route';


$_['text_s']        = 'Thành công';
$_['text_u']        = 'Không thuyết phục được';
$_['text_formula']       = 'Công thức';
$_['text_value']        = 'Giá trị';
$_['text_result']        = 'Kết quả';
$_['text_end_result']        = 'Kết quả cuối';

$_['text_sum_total']        = 'Tổng số lượng TB';

$_['text_has_perform']        = 'Đã thực hiện';
$_['text_no_perform']        = 'Chưa thực hiện';

// Text
$_['text_success']            = 'Thành công: Đã cập nhật plans!';
$_['text_list']               = 'Danh sách Plan';
$_['text_add']                = 'Thêm Plan';
$_['text_edit']               = 'Sửa Plan';


$_['text_update_info']               = 'Thông tin cập nhật';
$_['text_call_confirm']               = 'Cuộc gọi xác nhận';
$_['text_call_recipient']               = 'Tên sale ';
$_['text_call_phone_number']               = 'Số ĐT sale';
$_['text_call_date']               = 'Ngày gọi';
$_['text_call_num']               = 'Số lần gọi';
$_['text_call_note']               = 'Ghi chú';


$_['text_province']           = 'Tỉnh';
$_['text_district']           = 'Quận huyện';
$_['text_ward']               = 'Phường/xã';
$_['text_comment']            = 'Ghi chú';
$_['text_plan_status']        = 'Trạng thái plan';
$_['button_update']        = 'Cập nhật';
$_['button_note_add']        = 'Thêm lịch sử';
$_['text_related_planogram']        = 'Hàng hóa liên quan';
$_['help_unsuccess']        = 'Khảo sát chứa lý do KTC, vui lòng bỏ chọn lý do KTC để tính toán lại.';
$_['text_unsuccess_reason']        = 'Lý do KTC';
$_['text_unsuccess_comment']        = 'Ghi chú KTC';
$_['text_upload_manual']        = 'Upload thủ công';
$_['text_upload_app']        = 'Upload qua app';
$_['text_time_frame']        = 'Khung thời gian';
$_['text_from']        = 'Từ';
$_['text_to']        = 'Đến';


// Column

$_['text_plan']             = 'Plan';
$_['text_map']             = 'Map';
$_['text_checkin_c']         = 'Toạ độ checkin';
$_['text_checkin']         = 'Checkin';
$_['text_plan_survey']   = 'Mức Đăng ký';
$_['text_prefix']       = 'Prefix';
$_['text_action']           = 'Thao tác';
$_['text_plan_status']           = 'Thực hiện';
$_['text_staff_user']        = 'NV thực hiện';
$_['text_time_checkin']           = 'Checkin';
$_['text_time_upload']           = 'Ngày upload';
$_['text_coordinates']           = 'Tọa độ Checkin';
$_['text_rating']           = 'Đánh giá';

// Entry
$_['entry_store']              = 'Cửa hàng: ';
$_['text_store_name']              = 'Tên CH';
$_['text_user']              = 'User';
$_['text_code']        			= 'Code';
$_['text_status']            = 'Trạng thái';

// Error

$_['text_error_coupon']        = 'Lỗi: %s coupon(s) hợp lệ, vui lòng kiểm tra lại!';
$_['text_success_coupon']        = 'Thành công: %s coupon(s) hợp lệ!';

$_['text_error_permission']        = 'Warning: Bạn không có quyền sửa plans!';
$_['text_error_note_type']         = 'Vui lòng chọn note type!';
$_['text_error_name']              = 'Plan Name must be between 3 and 128 characters!';
$_['text_error_default']           = 'Warning: This country cannot be deleted as it is currently assigned as the default store country!';
$_['text_error_store']             = 'Warning: This country cannot be deleted as it is currently assigned to %s stores!';
$_['text_error_address']           = 'Warning: This country cannot be deleted as it is currently assigned to %s address book entries!';
$_['text_error_affiliate']         = 'Warning: This country cannot be deleted as it is currently assigned to %s affiliates!';
$_['text_error_zone']              = 'Warning: This country cannot be deleted as it is currently assigned to %s zones!';
$_['text_error_zone_to_geo_zone']  = 'Warning: This country cannot be deleted as it is currently assigned to %s zones to geo zones!';