<?php
class ModelUserUser extends Model {
	private $field_list = array(
		'telephone',
		'gender',
			  'p_identity_number',
			  'p_identity_issue_date',
			  'p_identity_issue_place',
			  'p_tax_code',
			  'p_insurance_num',
			  'p_belonging_persons',
			  'p_relative_telephone',
			  'p_birthday',
			  'p_place_of_birth',
			  'p_nationality',
			  'p_nation',
			  'p_degree',
			  'p_re_address',
			  'p_re_province_id',
			  'p_re_district_id',
				'p_re_ward_id',
				'p_so_address',
				'p_so_province_id',
				'p_so_district_id',
				'p_so_ward_id',
				'p_bi_address',
				'p_bi_province_id',
				'p_bi_district_id',
				'p_bi_ward_id',
				'p_protector',
			  'p_bank_holder',
			  'p_bank_number',
			  'p_bank_name',
			  'province_id',
			  'district_id',
			  'ward_id',
			  'address',
			'front_identity_card',
			'back_identity_card',
			'signature',
			'portrait'
		);

	public function addUser($data) {
		$field_list = $this->field_list;
		$user_register = isset($data['user_register'])?$data['user_register']:$this->user->getUserName();

		$sql = "INSERT INTO `" . DB_PREFIX . "user` SET ";

		foreach ($data as $field => $value) {
			if (in_array($field,$field_list)&&!empty($value)) {
			$sql .= " $field = '" . $this->db->escape($value) . "', ";
			}
		}
		$this->load->model('user/user_group');
		$group_name = $this->model_user_user_group->getGroupNameById($data['user_group_id']);

		$sql .= "fullname = '" . $this->db->escape($data['fullname']) . "',
		email = '" . $this->db->escape($data['email']) . "',
		user_group_id = '" . (int)$data['user_group_id'] . "',
		group_name = '" . $this->db->escape($group_name) . "',
		user_register = '" . $this->db->escape($user_register) . "',
		active = '1',";

		if ($data['password']&&!empty($data['password'])) {
			$sql .= "salt = '" . $this->db->escape($salt = token(9)) . "',
			password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', ";
		}
		$sql .= " date_added = NOW()";


		$this->db->query($sql);
		$user_id = $this->db->getLastId();
		$data['user_id'] = $user_id;
		$this->load->model('project/project_user');
		$this->model_project_project_user->addUserToProject($data);
		return $user_id;
	}

	public function updateUsername($user_id, $username){
		$this->db->query("UPDATE `".DB_PREFIX."user` SET `username` = '" .$this->db->escape($username). "', `usercode` = '".$this->db->escape($username)."' WHERE `user_id` = '" . (int)$user_id . "'");
	}

	public function changePassword($password) {
		if ($password&&!empty($password)) {
			$user_id = $this->user->getId();
			$sql = "UPDATE `" . DB_PREFIX . "user` SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "', code = '' WHERE user_id = '" . (int)$user_id . "'";

			$query = $this->db->query($sql);
			if ($query->num_rows) {
				return 1;
			}else{
				return 1;
			}
		}
	}
	public function editUser($user_id, $data = array()) {
		$field_list = $this->field_list;

		$sql = "UPDATE `" . DB_PREFIX . "user` SET ";

		foreach ($data as $field => $value) {
			if (in_array($field,$field_list)&&!empty($value)) {
			$sql .= " $field = '" . $this->db->escape($value) . "', ";
			}
		}
		if(isset($data['image'])&&!empty($data['image'])){
			$sql .= "image = '" . $this->db->escape($data['image']) ."'";
		}
		$sql .= " date_modified = NOW() WHERE user_id = '" . (int)$user_id . "'";

		$this->db->query($sql);

		if (isset($data['password'])&&!empty($data['password'])) {
			$this->db->query("UPDATE `" . DB_PREFIX . "user` SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "' WHERE user_id = '" . (int)$user_id . "'");
		}
	}
	public function getUserByUsername($username) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "user` u WHERE u.username = '" . $this->db->escape($username) . "'";
		$query = $this->db->query($sql);

		return $query->row;
	}
	public function getUser($user_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "user` u WHERE u.user_id = '" . (int)$user_id . "'");

		return $query->row;
	}
	public function getUsers($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "user` u LEFT JOIN `".DB_PREFIX."user_group` ug ON(u.user_group_id=ug.user_group_id)";

		$user_group_id = $this->user->getGroupId();
		$access_group = array(1, $this->config->get('config_user_qc'), $this->config->get('config_user_pa'), $this->config->get('config_user_pl'));

		if(in_array($user_group_id, $access_group)){
			$sql .= "WHERE 1 ";
		} else {
			$user = $this->getUser($this->user->getId());
			$province_manager = !empty($user['province_manager'])?$user['province_manager']:'-1';
			$sql .= "WHERE `province_id` IN(".$province_manager.")";
		}

		if(!in_array($user_group_id, $access_group)){
			$sql .= " AND ug.group_level>'" . (int)$this->user->getLevel() . "'";
		}

		$filter_global = array(
			'u.usercode',
			'u.username',
			'u.fullname',
			'u.email',
			'u.telephone',
			'u.address'
		);

		if (!empty($data['filter_global'])) {
			$sql .= " AND (";
			foreach ($filter_global as $filter) {
				$fimplode[] = " LCASE(".$filter.") LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_global'])) . "%'";
			}
			$sql .= " " . implode(" OR ", $fimplode) . "";
			$sql .= ")";
		}

		$sort_data = array(
			'u.username',
			'u.fullname',
			'u.status',
			'u.date_added'
		);
		if (!empty($data['filter_usercode'])) {
			$sql .= " AND u.usercode LIKE '%" . $this->db->escape($data['filter_usercode']) . "%'";
		}
		if (!empty($data['filter_username'])) {
			$sql .= " AND u.username LIKE '%" . $this->db->escape($data['filter_username']) . "%'";
		}
		if (!empty($data['filter_fullname'])) {
			$sql .= " AND u.fullname LIKE '%" . $this->db->escape($data['filter_fullname']) . "%'";
		}
		if (!empty($data['filter_email'])) {
			$sql .= " AND u.email LIKE '%" . $this->db->escape($data['filter_email']) . "%'";
		}
		if (!empty($data['filter_telephone'])) {
			$sql .= " AND u.telephone LIKE '%" . $this->db->escape($data['filter_telephone']) . "%'";
		}

		if (isset($data['filter_status'])) {
			$sql .= " AND u.status ='" . $this->db->escape($data['filter_status']) . "'";
		}

		if (!empty($data['filter_user_group_id'])) {
			$sql .= " AND u.user_group_id ='" . (int)$data['filter_user_group_id'] . "'";
		}
		if (!empty($data['filter_region_id'])) {
			$sql .= " AND u.region_id ='" . (int)$data['filter_region_id'] . "'";
		}
		if (!empty($data['filter_province_id'])) {
			$sql .= " AND u.province_id ='" . (int)$data['filter_province_id'] . "'";
		}
		if (!empty($data['filter_district_id'])) {
			$sql .= " AND u.district_id ='" . (int)$data['filter_district_id'] . "'";
		}


		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY u.username";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalUsers($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "user u ";

		$user_group_id = $this->user->getGroupId();
		$access_group = array(1, $this->config->get('config_user_qc'));

		if(in_array($user_group_id, $access_group)){
			$sql .= "WHERE 1";
		} else {
			$user = $this->getUser($this->user->getId());
			$sql .= "WHERE `province_id` IN(".$user['province_manager'].")";
		}

		$filter_global = array(
			'u.usercode',
			'u.username',
			'u.fullname',
			'u.email',
			'u.telephone',
			'u.address'
		);

		if (!empty($data['filter_global'])) {
			$sql .= " AND (";
			foreach ($filter_global as $filter) {
				$fimplode[] = " LCASE(".$filter.") LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_global'])) . "%'";
			}
			$sql .= " " . implode(" OR ", $fimplode) . "";
			$sql .= ")";
		}

		if (!empty($data['filter_usercode'])) {
			$sql .= " AND u.usercode LIKE '%" . $this->db->escape($data['filter_usercode']) . "%'";
		}
		if (!empty($data['filter_username'])) {
			$sql .= " AND u.username LIKE '%" . $this->db->escape($data['filter_username']) . "%'";
		}
		if (!empty($data['filter_fullname'])) {
			$sql .= " AND u.fullname LIKE '%" . $this->db->escape($data['filter_fullname']) . "%'";
		}
		if (!empty($data['filter_email'])) {
			$sql .= " AND u.email LIKE '%" . $this->db->escape($data['filter_email']) . "%'";
		}
		if (!empty($data['filter_telephone'])) {
			$sql .= " AND u.telephone LIKE '%" . $this->db->escape($data['filter_telephone']) . "%'";
		}

		if (isset($data['filter_status'])) {
			$sql .= " AND u.status ='" . $this->db->escape($data['filter_status']) . "'";
		}

		if (!empty($data['filter_user_group_id'])) {
			$sql .= " AND u.user_group_id ='" . (int)$data['filter_user_group_id'] . "'";
		}
		if (!empty($data['filter_region_id'])) {
			$sql .= " AND u.region_id ='" . (int)$data['filter_region_id'] . "'";
		}
		if (!empty($data['filter_province_id'])) {
			$sql .= " AND u.province_id ='" . (int)$data['filter_province_id'] . "'";
		}
		if (!empty($data['filter_district_id'])) {
			$sql .= " AND u.district_id ='" . (int)$data['filter_district_id'] . "'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getUserByEmail($email) {
		$query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "user` WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

	public function getUserByIdentityNumber($p_identity_number) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "user` WHERE p_identity_number = '" . $this->db->escape($p_identity_number) . "'");

		return $query->row;
	}

}