<?php
class ModelUserGroupPermission extends Model {
	public function editUserGroup($user_group_id, $data) {
		
		$project_group_query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "project_group_permission WHERE user_group_id = '" . (int)$user_group_id . "' AND project_id = '".(int)$this->config->get('config_project_id')."'");
		if ($project_group_query->row['total']>0) {
			$this->db->query("UPDATE " . DB_PREFIX . "project_group_permission SET permission = '" . (isset($data['permission']) ? $this->db->escape(json_encode($data['permission'])) : '') . "' WHERE user_group_id = '" . (int)$user_group_id . "' AND project_id='" . (int)$this->config->get('config_project_id') . "'");
		}else{
			$this->db->query("INSERT INTO " . DB_PREFIX . "project_group_permission SET 
			project_id='" . (int)$this->config->get('config_project_id') . "', 
			user_group_id='" . (int)$user_group_id . "',
			permission = '" . (isset($data['permission']) ? $this->db->escape(json_encode($data['permission'])) : '') . "'");
		}
		
	}
	
	public function getUserGroup($user_group_id) {
		$user_group = array();
		$query = $this->db->query("SELECT DISTINCT p.*, (SELECT ug.group_name FROM `" . DB_PREFIX . "user_group` ug WHERE ug.user_group_id = p.user_group_id) AS group_name FROM " . DB_PREFIX . "project_group_permission p WHERE p.user_group_id = '" . (int)$user_group_id . "' AND p.project_id='" . (int)$this->config->get('config_project_id') . "'");
		if ($query->num_rows) {
			$user_group = array(
				'user_group_id'       => $query->row['user_group_id'],
				'group_name'       => $query->row['group_name'],
				'permission' => json_decode($query->row['permission'], true)
			);
		}

		return $user_group;
	}

	public function getUserGroups($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "user_group p WHERE 1";

		$sql .= " ORDER BY p.user_group_id";

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalUserGroups() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "project_group_permission WHERE project_id='" . (int)$this->config->get('config_project_id') . "'");

		return $query->row['total'];
	}
}