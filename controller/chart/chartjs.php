<?php
class ControllerChartChartjs extends Controller{
	private $error = array();

	private $field_data = array(
			'plan_id'=>'',
			'plan_name'=>''
		);

	private $filter_key = array(
			'filter_global',
			'filter_user_provinces',
			'filter_province_id',
			'filter_round',
			'filter_rating_status',
			'filter_plan_status',
			'filter_status',
			'filter_name',
			'filter_area_code',
			'filter_region_code'
		);

	private function _url(){
		$url = '';
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}
	public function index() {
		$data['ajax_list'] = $this->ajax_list();
		$template = 'common/ajax_list';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
	public function getList() {
		$this->response->setOutput($this->ajax_list());
	}
	public function ajax_list() {
		$this->load->model('project/customer');
		$this->load->model('chart/morris');
		$this->load->model('localisation/area');

		$languages = $this->load->language('chart/morris');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		
		$data['ykeys'] = $ykeys = array('"success"','"unsuccess"','"fail"');
		$data['label'] = $label = array('"Đạt"','"Không đạt"','"KTC"');
		$data['colors'] = $colors = array('"#00a65a"','"#dd4b39"','"#898e92"');
		
		
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}



		$url = $this->_url();

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('client/plan',  $url, true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);


		
		$filter_data = array();

		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$data[$key] = trim($this->request->get[$key]);
				$filter_data[$key] = trim($this->request->get[$key]);
			} else{
				$data[$key] = null;
			}
		}	
		$xsuccess = 0;
		$xunsuccess = 0;
		$xfail =  0;
		$xtotal =  0;
			
		if(!$this->user->isLogged()&&$this->customer->isLogged()){
			$filter_data['customers_manager'] = $this->customer->customers_manager();
		}	
				
				
		$sup_group = $this->config->get('config_customer_sup');
		
		$sups = $this->customer->getAllSups($filter_data);
		$sups_data =array();
		$sups_total =array();
		
		$data['isLogged'] = $isLogged =  $this->customer->isLogged();
		$data['sup_area'] = $this->customer->area_code();
		$data['sup_chart'] = array();
		
		
		foreach($sups as $sup){
			if($sup['customer_level_id']==$sup_group){
				$rating = $this->model_chart_morris->countTotalPlansBySup($sup['customer_user_id']);
				$success =  isset($rating['success'])?$rating['success']:0;
				$unsuccess =  isset($rating['unsuccess'])?$rating['unsuccess']:0;
				$fail =  isset($rating['fail'])?$rating['fail']:0;
				
				$sups_total[$sup['customer_parent_id']]['success'][]= $rating['success'];
				$sups_total[$sup['customer_parent_id']]['unsuccess'][]= $rating['unsuccess'];
				$sups_total[$sup['customer_parent_id']]['fail'][]= $rating['fail'];
				
				
				$sups_data[$sup['customer_parent_id']]['sups'][$sup['customer_user_id']]= array(
					'customer_user_id'=>$sup['customer_user_id'],
					'username'=>$sup['username'],
					'fullname'=>$sup['fullname'],
					'chart' =>array("success:$success","unsuccess:$unsuccess","fail:$fail"),
					'success'=>$rating['success'],
					'unsuccess'=>$rating['unsuccess'],
					'fail'=>$rating['fail'],
					'total'=>array_sum($rating),
					'url_success'   => $this->url->link('client/plan','filter_rating_status=1&filter_customer_user_id='.$sup['customer_user_id'],true),
					'url_unsuccess' => $this->url->link('client/plan','filter_rating_status=-2&filter_customer_user_id='.$sup['customer_user_id'],true),
					'url_fail'      => $this->url->link('client/plan','filter_rating_status=-1&filter_customer_user_id='.$sup['customer_user_id'],true),
					'url_total'     => $this->url->link('client/plan','filter_customer_user_id='.$sup['customer_user_id'],true),
					'down_success'   => $this->url->link('client/export_plan/export','filter_rating_status=1&filter_customer_user_id='.$sup['customer_user_id'],true),
					'down_unsuccess' => $this->url->link('client/export_plan/export','filter_rating_status=-2&filter_customer_user_id='.$sup['customer_user_id'],true),
					'down_fail'      => $this->url->link('client/export_plan/export','filter_rating_status=-1&filter_customer_user_id='.$sup['customer_user_id'],true),
					'down_total'     => $this->url->link('client/export_plan/export','filter_customer_user_id='.$sup['customer_user_id'],true),
				);
				if($isLogged==$sup['customer_user_id']){
					$data['sup_chart'] = $sups_data[$sup['customer_parent_id']]['sups'][$sup['customer_user_id']];	
				}
				unset($rating);
			}
		}
			//print_r('<pre>'); print_r($sups_total); print_r('</pre>'); 
		$data['areas'] = array();
		$areas = $this->model_localisation_area->getAreas();
		$data['all_cats'] = array();
		foreach($areas as $area){
			$data['areas'][] = array(
				'href'      => $this->url->link('chart/chartjs', 'filter_area_code='.$area['area_code'], true),
				'area_id'   => $area['area_id'],
				'area_name' => $area['area_name'],
				'area_code' => $area['area_code']
			);
			
			$filter_area = array('filter_area_code'=>$area['area_code']);
			$cat_customers = $this->model_project_customer->getCustomers($filter_area);
			foreach($cat_customers as $cat){
				
				$success= isset($sups_total[$cat['customer_user_id']]['success'])?array_sum($sups_total[$cat['customer_user_id']]['success']):0;
				$unsuccess= isset($sups_total[$cat['customer_user_id']]['unsuccess'])?array_sum($sups_total[$cat['customer_user_id']]['unsuccess']):0;
				$fail = isset($sups_total[$cat['customer_user_id']]['fail'])?array_sum($sups_total[$cat['customer_user_id']]['fail']):0;
				$total = $success+$unsuccess+$fail;
				$xsuccess += $success;
				$xunsuccess += $unsuccess;
				$xfail += $fail;
				$xtotal += $total;
				$data['all_cats'][$area['area_code']][$cat['customer_user_id']]= array(
					'customer_user_id'=>$cat['customer_user_id'],
					'username'=>$cat['username'],
					'fullname'=>$cat['fullname'],
					'url_success'   => $this->url->link('client/plan','filter_rating_status=1&filter_customer_parent_id='.$cat['customer_user_id'],true),
					'url_unsuccess' => $this->url->link('client/plan','filter_rating_status=-2&filter_customer_parent_id='.$cat['customer_user_id'],true),
					'url_fail'      => $this->url->link('client/plan','filter_rating_status=-1&filter_customer_parent_id='.$cat['customer_user_id'],true),
					'url_total'     => $this->url->link('client/plan','filter_customer_parent_id='.$cat['customer_user_id'],true),
					'down_success'   => $this->url->link('client/export_plan/export','filter_rating_status=1&filter_customer_parent_id='.$cat['customer_user_id'],true),
					'down_unsuccess' => $this->url->link('client/export_plan/export','filter_rating_status=-2&filter_customer_parent_id='.$cat['customer_user_id'],true),
					'down_fail'      => $this->url->link('client/export_plan/export','filter_rating_status=-1&filter_customer_parent_id='.$cat['customer_user_id'],true),
					'down_total'     => $this->url->link('client/export_plan/export','filter_customer_parent_id='.$cat['customer_user_id'],true),
					'chart' =>array("success:$success","unsuccess:$unsuccess","fail:$fail"),
					'success'=>(int)$success,
					'unsuccess'=>(int)$unsuccess,
					'fail'=>(int)$fail,
					'total'=>(int)$total,
					'sups'=>isset($sups_data[$cat['customer_user_id']]['sups'])?$sups_data[$cat['customer_user_id']]['sups']:array()
				); 
				unset($success);
				unset($unsuccess);
				unset($fail);
				unset($total);
			}
		}
		$data['nationwide'] = array(
			'success'=>$xsuccess,
			'unsuccess'=>$xunsuccess,
			'fail'=>$xfail,
			'total'=>$xtotal
		); 
		
		$data['nationwide']['url_success'] = $this->url->link('client/plan','filter_rating_status=1',true);
		$data['nationwide']['url_unsuccess'] = $this->url->link('client/plan','filter_rating_status=-2',true);
		$data['nationwide']['url_fail'] = $this->url->link('client/plan','filter_rating_status=-1',true);
		$data['nationwide']['url_total'] = $this->url->link('client/plan','',true);
		
		$data['nationwide']['down_success'] = $this->url->link('client/export_plan/export','filter_rating_status=1',true);
		$data['nationwide']['down_unsuccess'] = $this->url->link('client/export_plan/export','filter_rating_status=-2',true);
		$data['nationwide']['down_fail'] = $this->url->link('client/export_plan/export','filter_rating_status=-1',true);
		$data['nationwide']['down_total'] = $this->url->link('client/export_plan/export','',true);
		
		
		
		if($this->user->isLogged()||$this->customer->getLevel()==$this->config->get('config_customer_top')){
			$data['isNationwide'] = 1;
		}else{
			$data['isNationwide'] = 0;
		}
		/*StaftUser*/
		$data['href'] = $this->url->link('chart/chartjs', 'filter_area_code=1', true);


		$this->document->addScript('assets/plugins/chartjs/Chart.bundle.min.js');
		$data['export_plan'] = $this->load->controller('client/export_plan', $filter_data);
		$template = 'chart/chartjs';
		return $this->load->view($template, $data);
	}

}