<?php
class ControllerPlanPlanHistory extends Controller {	

	public function index() {
		if (isset($this->request->get['plan_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {

			$data['histories'] = $filter_data = $history = array();

			$data['plan_id'] = $plan_id = $this->request->get['plan_id'];	

			$this->load->model('user/user');
			$this->load->model('plan/plan');
			$this->load->model('history/history');
			$this->load->model('localisation/image_type');

			$image_types = $this->model_localisation_image_type->getAllTypes();
	
			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else {
				$page = 1;
			}
		
			$filter_data = [
				'plan_id' 	=> $plan_id,
				'start' 	=> ($page - 1) * 6,
				'limit' 	=> 6
			];

			$results = $this->model_history_history->getHistories($filter_data);

			foreach ($results as $result) {

				$user_info = $this->model_user_user->getUser($result['user_id']);

				$data_old = json_decode($result['old_data'], true);

				$data_new =	json_decode($result['data'], true);

				foreach ($data_new as $k => $v) {
					if(array_key_exists($k, $data_old)) {
						$dup[$k] = $data_old[$k];
					}
				}

				$diff = array_diff($dup, $data_new);
		
				if(!empty($diff)){
					foreach ($diff as $k => $v) {
						if(array_key_exists($k, $data_new)){
							$new[$k] = $data_new[$k];
						}
					}
					if (array_merge_recursive($diff, $new)) {
						$history = array_merge_recursive($diff, $new);
					}
				}

				if ($result['table_name'] == 'plan') {
					unset($history['store_image'], $history['is_deleted'], $history['value']);

				}elseif ($result['table_name'] == 'attribute_data') {
					$history['group_code'][] 	= $data_old['group_code'];
					$history['group_code'][] 	= $data_old['group_code'];
					$history['attribute_name'][] = $data_old['attribute_name'];
					$history['attribute_name'][] = $data_old['attribute_name'];
					unset($history['store_image'], $history['image_type_id'], $history['is_deleted']);

				}elseif ($result['table_name'] == 'plan_image') {

					$history['image_type'][] = $this->model_localisation_image_type->getImageType($data_old['image_type_id'])['image_type'];
						
					$history['image_type'][] = $this->model_localisation_image_type->getImageType($data_new['image_type_id'])['image_type'];

					if($data_old['image_type_id'] == $data_new['image_type_id']){
						unset($history['image_type'], $history['image_type_id']);
		 			}
					unset($history['note_ktc'], $history['store_image'], $history['value']);	

				}elseif ($result['table_name'] == 'delete_image') {
					$history['filename'][] = $data_old['filename'];
					$history['filename'][] = $data_old['filename'];
					unset($history['note_ktc'], $history['store_image'], $history['value']);	

				}elseif ($result['table_name'] == 'set_avatar') {
					unset($history['note_ktc'], $history['is_deleted'], $history['value']);	

				}else{
					$history = [];		
				}

				if(!empty($history)){
					$histories['id'] 		= $result['id'];
					$histories['fullname'] 	= $user_info['fullname'];
					$histories['history'] 	= $history;
					$histories['date_added'] = date('d-m-Y', strtotime($result['date_added']));
					$data['histories'][] = $histories;
				}
				
			}	

			$total 				= $this->model_history_history->getTotalHistories($plan_id);
			$pagination 		= new Pagination($this->language->get('text_pagination'));
			$pagination->total 	= $total;
			$pagination->page 	= $page;
			$pagination->limit 	= 6;
			$pagination->url 	= $this->url->link('plan/plan_history', 'plan_id='.$plan_id.'&page={page}' , true);
			$data['pagination'] = $pagination->render();
			$data['results'] 	= $pagination->results();

			$template = 'plan/plan_history';
			$this->response->setOutput($this->load->view($template, $data));
		}else{
			echo $this->language->get('text_error_permission');
		}
	}
}