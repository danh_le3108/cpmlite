<?php
class ControllerPlanChangePlan extends Controller{
	private $no_filter_key = array('_url','route','page');
	public function index() {
		$this->load->model('plan/plan');
		$this->load->model('localisation/province');
		$this->load->model('localisation/region');
		$this->load->model('project/project_user');
		$this->load->model('catalog/attribute');

		$data['plan_checked'] = array();
		if (!empty($this->request->post)) {
			if (empty($this->request->post['selected']) || empty($this->request->get['filter_user_id'])) {
				$data['error_warning'] = 'Vui lòng chọn plan cần chuyển ở bước 2!';
			}elseif (empty($this->request->post['user_id_to'])) {
				$data['plan_checked'] = $this->request->post['selected'];
				$data['error_warning'] = 'Vui lòng chọn nhân viên cần chuyển plan ở bước 3!';
			} else {
				$user = $this->model_project_project_user->getUser($this->request->post['user_id_to']);
				if (!$user) {
					$data['error_warning'] = 'Nhân viên cần chuyển không tham gia dự án!';
				} else {
					$plan_ids = $this->request->post['selected'];
					$sql = 'UPDATE ' . DB_PREFIX . 'plan SET user_id = ' . $user['user_id'] . ', usercode = "' . $this->pdb->escape($user['usercode'].' - '.$user['fullname']) . '" WHERE plan_id IN (' . implode(',', $plan_ids) . ')';
					$this->pdb->query($sql);
					
					$this->load->model('history/history');
					$user_from = $this->model_project_project_user->getUser($this->request->get['filter_user_id']);
					$history = array(
						'table_name' => 'plan',
						'old_data' => 'plan_ids: ' . implode(',', $plan_ids),
						'user_id' => $this->user->getId(),
						'user_update' => $this->user->getUserName(),
						'controller' => 'plan/change_plan',
						'data' => '['.date('d-m-Y H:i:s').'] ' . '<strong>'.$this->user->getFullName().'</strong> chuyển ' . count($plan_ids) . ' plan từ <span class="text-danger">' . $user_from['fullname'] . '</span> => <span class="text-danger">' . $user['fullname'] . '</span>'
					);
					$this->model_history_history->add($history);
					$this->session->data['success'] = 'Chuyển plan thành công';
					$this->response->redirect($this->url->link('plan/change_plan&filter_user_id='.$this->request->post['user_id_to']));
				}
			}
		}
		
		$data['plan_total'] = $this->model_plan_plan->countTotal();
		$filter_data = array('filter_plan_status'=>0);
		if (isset($this->request->get['filter_global'])) {
			$filter_data['filter_global'] = $this->request->get['filter_global'];
		}
		$data['rounds'] = $this->model_plan_plan->countTotal(array(), 'round_name');
		if (isset($this->request->get['filter_round_name'])) {
			$filter_data['filter_round_name'] = $this->request->get['filter_round_name'];
		}
		// $data['count_region'] = $this->model_plan_plan->countTotal($filter_data, 'region_code');
		// if (isset($this->request->get['filter_region_code'])) {
		// 	$filter_data['filter_region_code'] = $this->request->get['filter_region_code'];
		// }
		$data['count_province'] = $this->model_plan_plan->countTotal($filter_data, 'province_id');
		if (isset($this->request->get['filter_province_id'])) {
			$filter_data['filter_province_id'] = $this->request->get['filter_province_id'];
		}
		$data['count_staff'] = $this->model_plan_plan->countTotal($filter_data, 'user_id');
		if (isset($this->request->get['filter_user_id'])) {
			$filter_data['filter_user_id'] = $this->request->get['filter_user_id'];
		}
		// $data['count_status'] = $this->model_plan_plan->countTotal($filter_data, 'plan_status');
		// if (isset($this->request->get['filter_plan_status'])) {
		// 	$filter_data['filter_plan_status'] = $this->request->get['filter_plan_status'];
		// }
		// $data['count_qc'] = $this->model_plan_plan->countTotal($filter_data, 'plan_qc');
		// $data['versions'] = $this->model_plan_plan->countTotal($filter_data, 'version');
		// $data['upload_count'] = $this->model_plan_plan->countTotal($filter_data, 'upload_count');
		$data['provinces'] = $this->model_localisation_province->getProvinces();
		// $data['regions'] = $this->model_localisation_region->getRegions();
		$data['staffs'] = $this->model_project_project_user->getProjectUsersByGroupID($this->config->get('config_user_staff'));
		
		$languages = $this->load->language('plan/plan');
	    foreach($languages as $key=>$value){
	        $data[$key] = $value; 
	    }
	  	$data['breadcrumbs'] = array();
	    $data['breadcrumbs'][] = array(
	      'text' => $this->language->get('text_home'),
	      'href' => $this->url->link('common/home', '', true)
	    );
	    $data['breadcrumbs'][] = array(
	      'text' => 'Chuyển plan',
	      'href' => $this->url->link('plan/change_plan')
	    );
	    $this->document->setBreadcrumbs($data['breadcrumbs']);
	   	foreach($this->request->get as $key => $value){
			$data[$key] = $value;
		}
		// if (!empty($this->session->data['error_warning'])) {
		// 	$data['error_warning'] = $this->session->data['error_warning'];
		// 	unset($this->session->data['error_warning']);
		// }
		if (!empty($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}

		$data['pl_status'] = array(
			1 => 'Đã thực hiện',
			2 => 'Đang thực hiện',
			0 => 'Chưa thực hiện'
		);
		
		$filter_data = array();
		foreach ($this->request->get as $key => $value) {
			if (!in_array($key, $this->no_filter_key)) {
				$filter_data[$key] = $this->request->get[$key];
			}
		}
		$filter_data['filter_plan_status'] = 0;
 		$data['plans'] = count($filter_data) > 1 ? $this->model_plan_plan->getPlans($filter_data) : array();
 		$data['history'] = $this->pdb->query("SELECT * FROM cpm_log_history WHERE controller = 'plan/change_plan' ORDER BY id DESC LIMIT 10")->rows;
		$this->document->setTitle('Chuyển plan nhân viên');
		$template = 'plan/change_plan';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}

	private function _url(){
		$url = '';
		foreach ($this->request->get as $key => $value) {
			if (!in_array($key, $this->no_filter_key)) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($value, ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}
}