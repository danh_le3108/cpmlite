<?php
class ControllerPlanPlanStore extends Controller {
	public function index($plan) {
		if (!empty($plan) && $this->document->hasPermission('access','plan/plan_store')) {
			$this->load->model('tool/image');
			$file_path = $plan['image_overview'];
			if (file_exists(DIR_MEDIA.$file_path) && !empty($plan['image_overview'])) {
				$plan['thumb'] = $this->model_tool_image->resize($file_path, 100, 100);
				$plan['popup'] = HTTP_SERVER . 'media/' . $file_path;
			} else {
				$plan['thumb'] = $this->model_tool_image->resize('store.jpg', 100, 100);
				$plan['popup'] = '';
			}
			$this->load->model('localisation/reason_unsuccess');
			$data['reasons'] = $this->model_localisation_reason_unsuccess->getReasons();
			$this->load->model('store/store_type');
			$data['store_types'] = $this->model_store_store_type->getAllTypes();
			$this->load->model('catalog/group');
			$data['groups'] = $this->model_catalog_group->getGroups(array('parent_group_id' => 0));
			$this->load->model('catalog/attribute');
			$data['posm_attrs'] = $this->model_catalog_attribute->getAttributes(array('group_id'=>1));
			$data['plan'] = $plan;
			$data['hasEdit'] = $this->document->hasPermission('edit','plan/plan_store');
			$data['isUser'] = $this->user->isLogged();
			$data['pl_status'] = array(
				0 => 'Chưa thực hiện',
				2 => 'Đang thực hiện',
				1 => 'Đã thực hiện'
			);
			$template = 'plan/plan_store';
			return $this->load->view($template, $data);
		}
	}
	
	public function save() {
		$this->load->model('plan/plan');
		$this->load->model('history/history');

		if ($this->document->hasPermission('edit','plan/plan_store') && !empty($this->request->get['plan_id'])) {

			$data = $this->request->post;
			$old_data = $this->model_plan_plan->getPlan($this->request->get['plan_id']);

			foreach($data as $k => $v) {
	    		if(array_key_exists($k, $old_data))
	    			$dup[$k] = $old_data[$k];
	    	}

	    	if(array_diff($dup, $data)){
				$diff = array_diff($dup, $data);
			}

	    	if($diff) { 

				$history = array(
					'table_name' => 'plan',
					'user_id' => $this->user->getId(),
					'user_update' => $this->user->getUserName(),
					'controller' => 'plan/plan_store',
					'table_primary_id' => $this->request->get['plan_id'],
					'data' => json_encode($data),
					'old_data' => json_encode($old_data)
				);
				$this->model_history_history->add($history);
			}

			$this->model_plan_plan->updatePlan($this->request->get['plan_id'], $this->request->post);
			echo 1;
		} elseif (isset($this->request->post['feedback'])) {
			$this->model_plan_plan->updatePlan($this->request->get['plan_id'], $this->request->post);
		}
	}
	public function verify() {
		$this->load->language('plan/plan');
			$data['user_id'] = $this->user->getId();
		
		$json = array();
		//$this->log->write($this->request->get); 
		$this->load->model('plan/plan');
		if ($this->document->hasPermission('edit', 'plan/plan_store')&&isset($this->request->get['plan_id'])) {
			
			$plan_id = $this->request->get['plan_id'];
				
			$plan_info = $this->model_plan_plan->getPlan($plan_id);
		
		$sql = "UPDATE `" . PDB_PREFIX . "store` SET ";
		$sql .= "`store_latitude` = '" . $this->pdb->escape($plan_info['latitude']) . "',";
		$sql .= "`store_longitude` = '" . $this->pdb->escape($plan_info['longitude']) . "',";
		$sql .= "`is_verify` = 1 WHERE store_id = '" . (int)$plan_info['store_id'] . "'";
		$this->pdb->query($sql);
		
			$json['success'] = $this->session->data['success'] = $this->language->get('Đã xác thực Tọa độ!!');
		}else{
			$json['error'] = $this->language->get('Bạn không có quyền sửa thông tin!');
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function info() {
		if (isset($this->request->get['plan_id'])) {
			$this->load->model('plan/plan');
			$plan_id = $this->request->get['plan_id'];
			$plan_info = $this->model_plan_plan->getPlan($plan_id);
			$this->response->setOutput($this->index($plan_info));
		}
	}
}