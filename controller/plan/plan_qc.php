<?php
class ControllerPlanPlanQc extends Controller {	
	public function index() {
		if (isset($this->request->get['plan_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST') && $this->document->hasPermission('access','plan/plan_qc')) {
			$data['plan_id'] = $plan_id = $this->request->get['plan_id'];	
			$codes = $this->model_plan_plan->getPlanCodes($plan_id);
			$data['plan_codes'] = array();
			foreach($codes as $code){
				$data['plan_codes'][] = $code['problem_id'];
			}
			/*Status History*/ 
			$this->load->model('qc/code');
			$data['code_groups'] = $this->model_qc_code->getGroupCodes();
			$data['hasEdit'] = $this->document->hasPermission('edit', 'plan/plan_qc');
			$template = 'plan/plan_qc';
			return $this->load->view($template, $data);
		}
	}
	public function save() {
		if ($this->document->hasPermission('edit', 'plan/plan_qc')&&isset($this->request->get['plan_id']) && isset($this->request->post['codes'])) {
			$plan_id = $this->request->get['plan_id'];
			$this->load->model('plan/plan');
			$plan = $this->model_plan_plan->getPlan($plan_id);
			$this->model_plan_plan->deletePlanCode($plan_id);
			foreach ($this->request->post['codes'] as $code) {
				$data = array(
					'plan_id' => $plan_id,
					'round_name' => $plan['round_name'],
					'user_id' => $this->user->getId(),
					'user_group_id' => $this->user->getGroupId(),
					'problem_id' => $code
				);
				$this->model_plan_plan->updatePlanCode($data);
			}
			echo 1;
		}
	}
}