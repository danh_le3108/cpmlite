﻿<?php
class ControllerPlanPlan extends Controller{
	private $no_filter_key = array('_url','route','page');

	private function _url(){
		$url = '';
		foreach ($this->request->get as $key => $value) {
			if (!in_array($key, $this->no_filter_key)) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($value, ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}

	public function index() {
		$this->load->model('plan/plan');
		$this->load->model('localisation/province');
		$this->load->model('localisation/region');
		$this->load->model('project/project_user');
		$this->load->model('catalog/attribute');
		$this->load->model('catalog/group');
		$this->load->model('store/store_type');

		$filter_data = array();
		$data['plan_total'] = $this->model_plan_plan->countTotal();

		if (isset($this->request->get['filter_global'])) {
			$filter_data['filter_global'] = $this->request->get['filter_global'];
		}

		$data['count_project'] = $this->model_plan_plan->countTotal($filter_data, 'group_id');
		if (isset($this->request->get['filter_group_id'])) {
			$filter_data['filter_group_id'] = $this->request->get['filter_group_id'];
		}

		$data['rounds'] = $this->model_plan_plan->countTotal($filter_data, 'round_name');
		if (isset($this->request->get['filter_round_name'])) {
			$filter_data['filter_round_name'] = $this->request->get['filter_round_name'];
		}

		$data['count_province'] = $this->model_plan_plan->countTotal($filter_data, 'province_id');
		if (isset($this->request->get['filter_province_id'])) {
			$filter_data['filter_province_id'] = $this->request->get['filter_province_id'];
		}

		$data['count_staff'] = $this->model_plan_plan->countTotal($filter_data, 'user_id');
		if (isset($this->request->get['filter_user_id'])) {
			$filter_data['filter_user_id'] = $this->request->get['filter_user_id'];
		}

		$data['count_status'] = $this->model_plan_plan->countTotal($filter_data, 'plan_status');
		if (isset($this->request->get['filter_plan_status'])) {
			$filter_data['filter_plan_status'] = $this->request->get['filter_plan_status'];
		}

		$data['count_qc'] = $this->model_plan_plan->countTotal($filter_data, 'plan_qc');
		$data['versions'] = $this->model_plan_plan->countTotal($filter_data, 'version');
		$data['upload_count'] = $this->model_plan_plan->countTotal($filter_data, 'upload_count');
		$data['provinces'] = $this->model_localisation_province->getProvinces();
		$data['staffs'] = $this->model_project_project_user->getProjectUsersByGroupID($this->config->get('config_user_staff'));
		$data['projects'] = $this->model_catalog_group->getGroups(array('parent_group_id' => 0));
		$data['posm'] = $this->model_catalog_attribute->getAttributes(array('group_id'=>8,'sort'=>'attribute_id'));

		$languages = $this->load->language('plan/plan');
	    foreach($languages as $key => $value){
	        $data[$key] = $value; 
	    }

	  	$data['breadcrumbs'] = array();
	    $data['breadcrumbs'][] = array(
	      'text' => $this->language->get('text_home'),
	      'href' => $this->url->link('common/home', '', true)
	    );

	    $data['breadcrumbs'][] = array(
	      'text' => $this->language->get('heading_title'),
	      'href' => $this->url->link('plan/plan')
	    );

	    $this->document->setBreadcrumbs($data['breadcrumbs']);

	   	foreach($this->request->get as $key => $value){
			$data[$key] = $value;
		}

		if (!empty($this->session->data['error_warning'])) {
			$data['error_warning'] = $this->session->data['error_warning'];
			unset($this->session->data['error_warning']);
		}

		if (!empty($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}

		$data['fix_statuses'] = array(
			'0'=> $this->language->get('Không'),
			'1'=> $this->language->get('Có - chưa KP'),
			'2'=> $this->language->get('Có - đã KP chưa xác nhận'),
			'3'=> $this->language->get('Có - đã xác nhận KP')
		);

		$data['store_types'] 	= $this->model_store_store_type->getStoreTypes();
		$data['projects'] 		= $this->model_catalog_group->getGroups(array('parent_group_id' => 0));
		$data['hasDel'] 		= $this->document->hasPermission('delete','plan/plan');
		$data['isAdmin'] 		= $this->user->getGroupId() == 1 ? 1 : 0;
		$data['isUser'] 		= $this->user->isLogged();
		$data['url'] 			= $this->url->link('plan/plan/getPlans', $this->_url());
		$data['import'] 		= $this->load->controller('excel/plan_import');

		$this->document->setTitle('Plan');
		$this->document->addStyle('assets/plugins/magnific/magnific-popup.css'); 
		$this->document->addScript('assets/plugins/magnific/jquery.magnific-popup.js');
		return $this->load->controller('startup/builder',$this->load->view('plan/index', $data));
	}

	public function getPlans() {
		$this->load->model('plan/plan');
		$this->load->model('tool/image');
		$this->load->model('store/store_type');
		
		$languages= $this->load->language('plan/plan');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}

		$filter_data = array();
		foreach ($this->request->get as $key => $value) {
			if (!in_array($key, $this->no_filter_key)) {
				$filter_data[$key] = $this->request->get[$key];
			}
		}

		$page = !empty($this->request->get['page']) ? $this->request->get['page'] : 1;

		$plan_total = $this->model_plan_plan->countTotal($filter_data);

		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $plan_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_project');
		$pagination->url = $this->url->link('plan/plan/getPlans', $this->_url() . '&page={page}', true);
		$data['results'] = $pagination->results();
		$data['pagination'] = $pagination->render();

		$filter_data['start'] = ($page - 1) * $this->config->get('config_limit_project');
		$filter_data['limit'] = $this->config->get('config_limit_project');

		$data['plans'] = $this->model_plan_plan->getPlans($filter_data);
		$data['model_tool_image'] = $this->model_tool_image;
		$data['thumb'] = $this->model_tool_image->resize('store.jpg', 60, 60);
		$data['hasDel'] = $this->document->hasPermission('delete', 'plan/plan');
		$data['store_types'] = $this->model_store_store_type->getAllTypes();
	
		//data for plan progress
		if (!empty($this->request->get['filter_user_id'])) {
			$data['plan_total'] = $plan_total;
			$this->load->model('project/project_user');
			$data['staff'] = $this->model_project_project_user->getUser($this->request->get['filter_user_id']);
		}

		$data['user_id'] = $this->user->isLogged() ? $this->user->getId() : 0;
		$data['pl_status'] = array(
			1 => 'Đã thực hiện',
			2 => 'Đang thực hiện',
			0 => 'Chưa thực hiện'
		);

		$data['isUser'] = $this->user->isLogged();
		echo $this->load->view('plan/plan_list', $data);
	} 

	public function delete() {
		if ($this->document->hasPermission('delete', 'plan/plan')) {
			$this->load->model('plan/plan');
			$this->model_plan_plan->deletePlan($this->request->get['plan_id']);
		}
	}

	public function edit() {
		$languages = $this->load->language('plan/plan');
	    foreach($languages as $key=>$value){
	        $data[$key] = $value; 
	    }

	  	$data['breadcrumbs'] = array();
	    $data['breadcrumbs'][] = array(
	      	'text' => $this->language->get('text_home'),
	      	'href' => $this->url->link('common/home', '', true)
	    );

	    $data['breadcrumbs'][] = array(
	      	'text' => $this->language->get('heading_title'),
	      	'href' => $this->url->link('plan/plan')
	    );

	    $this->document->setBreadcrumbs($data['breadcrumbs']);

	   	$this->load->model('plan/plan');
	   	$plan = $this->model_plan_plan->getPlan($this->request->get['plan_id']);
	   	
		$data['plan_id'] = $this->request->get['plan_id'];
		
	   	if ($this->user->isLogged()) {
	   		$users_read = !empty($plan['users_read']) ? explode(',', $plan['users_read']) : array();
	   		if (!in_array($this->user->getId(), $users_read)) {
	   			$users_read[] = $this->user->getId();
	   			$this->model_plan_plan->updatePlan($this->request->get['plan_id'], array('users_read'=>implode(',', $users_read)));
	   		}
	   	}

		$data['plan_store'] 		= $this->load->controller('plan/plan_store', $plan);
		$data['plan_survey'] 		= $this->load->controller('plan/plan_survey', $plan);
		$data['plan_image_audio'] 	= $this->load->controller('plan/plan_image_audio', $plan);
		$data['plan_note'] 			= $this->load->controller('plan/plan_note');
		$data['plan_qc'] 			= $this->load->controller('plan/plan_qc');
		$data['plan_confirm'] 		= $this->load->controller('plan/plan_confirm', $plan);
		$data['is_user'] 			= $this->user->isLogged() ? 1 : 0;
		$data['is_admin'] 			= $this->user->getGroupId() == 1 ? 1 : 0;
		$data['back']				= $this->url->link('plan/plan', '', true);

		$this->document->addStyle('assets/plugins/magnific/magnific-popup.css'); 
		$this->document->addScript('assets/plugins/magnific/jquery.magnific-popup.js');
		$this->document->setTitle($this->language->get('heading_title').' #'.$plan['plan_id']);

		$template = 'plan/plan_form';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}

	public function autocomplete() {
		$filter_data = array(
			'start'       => 0,
			'limit'       => 20
		);
		if (!empty($this->request->get['filter_global'])) {
			$filter_data['filter_global'] = $this->request->get['filter_global'];
			$this->load->model('plan/plan');
			$plans = $this->model_plan_plan->getPlans($filter_data);
			echo json_encode($plans);
		}
	}

	public function test() {
		// $this->load->model('store/store');
		$this->load->model('plan/plan');
		// $filter = array(
		// 	'filter_global' => 'MDHCM',
		// 	'filter_group_id' => 7,
		// 	'filter_round_name' => '2019-04',
		// 	'filter_plan_status' => 0
		// );
		// $plans = $this->model_plan_plan->getPlans($filter);
		// foreach ($plans as $p) {
		// 	$sql = "delete from cpm_plan where plan_id = " . $p['plan_id'];
		// 	$this->pdb->query($sql);
		// }
		// p($sql,1);
		$sql = "SELECT * FROM cpm_plan pl
				LEFT JOIN cpm_store s ON pl.store_id = s.store_id WHERE pl.is_deleted = 0 AND plan_status= '1' AND user_id != '3519' AND round_name='2019-04' AND hinh = 0 AND group_id = 5 LIMIT 500";
		$plans = $this->pdb->query($sql)->rows;
		// p($plans,1);
		// p(count($plans),1);
		$count = 0;
		foreach ($plans as $p) {
			@mkdir(DIR_MEDIA.'files/CPM_NESTPOSM/milo_dangler/'.$p['region_code'].'/'.$p['distributor'].'/'.$p['store_code'],  0777, true);
			$path = DIR_MEDIA.$p['image_path'].'image/'.$p['plan_id'].'/';
			$new_path = DIR_MEDIA.'files/CPM_NESTPOSM/milo_dangler/'.$p['region_code'].'/'.$p['distributor'].'/'.$p['store_code'].'/';
			// if (is_dir($new_path)) {
			// 	$count++;
			// 	echo $count.' - ';
			// }
			$images = $this->model_plan_plan->getPlanImages($p['plan_id']);
			// p($p,1);
			foreach ($images as $img) {
				if (!file_exists($path.$img['filename'])) {
					// copy($path.$img['filename'], $new_path.$img['filename']);
					p($img);
				}
				copy($path.$img['filename'], $new_path.$img['filename']);
				// p($new_path,1);
			}
			$this->model_plan_plan->updatePlan($p['plan_id'], array('hinh'=>1));
			// p(1,1);
			// p($images);
			// p($new_path,1);
		}
		// p($count,1);
		p(count($plans),1);
		// $a = array(4805 => 'PPT7_05560');
		// foreach ($a as $plan_id => $store_code) {
		// 	$s = $this->pdb->query('SELECT * FROM cpm_store WHERE store_code = "'. $store_code . '"')->row;
		// 	$p = $this->model_plan_plan->getPlan($plan_id);
		// 	$old_store_code = $p['store_code'];
		// 	$data = array(
		// 		'store_id' => $s['store_id'],
		// 		'store_code' => $s['store_code'],
		// 		'image_path' => str_replace($old_store_code, $s['store_code'], $p['image_path']),
		// 		'image_overview' => str_replace($old_store_code, $s['store_code'], $p['image_overview']),
		// 		'image_selfie' => str_replace($old_store_code, $s['store_code'], $p['image_selfie']),
		// 	);
		      // $this->model_plan_plan->updatePlan($plan_id,$data);
		// }
		// p(DIR_MEDIA.'files/CPM_POSMT7',1);
		// p(scandir(DIR_MEDIA.'files/CPM_POSMT7', 1),1);
		// p(array_filter(glob(DIR_MEDIA.'files/CPM_POSMT7/2018-07/*'), "is_dir"));
		echo count(glob(DIR_MEDIA.'files/CPM_POSMT7/2018-07/*', GLOB_ONLYDIR) );
	}	
}