<?php
class ControllerPlanPlanSurvey extends Controller {
	public function index($plan = array()) {
		if (!empty($plan) && $this->document->hasPermission('access','plan/plan_survey')) {
			$data['plan_id'] = $plan_id = $plan['plan_id'];
			$data['plan'] = $plan;
			$this->load->model('catalog/attribute');
			$attrs = $this->model_catalog_attribute->getAttributes(array('sort'=>'g.sort_order','g.parent_group_id'=>$plan['group_id']));
			$data['attrs'] = array();
			foreach ($attrs as $key => $value) {
				$data['groups'][$value['group_name']][] = $value;
			}
			$this->load->model('catalog/attribute_data');
			$data['attr_data'] = $this->model_catalog_attribute_data->getAttrsOfPlan(array('plan_id'=>$plan_id));
			$data['hasEdit'] = $this->document->hasPermission('edit','plan/plan_survey');
			$template = 'plan/plan_survey';
			return $this->load->view($template, $data);
		}
	}

	public function save() {
		if ($this->document->hasPermission('edit','plan/plan_survey') && !empty($this->request->get['plan_id'])) {

			$this->load->model('plan/plan');
			$this->load->model('history/history');
			$this->load->model('catalog/attribute_data');

			$plan_id = $this->request->get['plan_id'];

			$attrs 	= $this->request->post['attrs'];

			$plan = $this->model_plan_plan->getPlan($plan_id);

			$attr_data = $this->model_catalog_attribute_data->getAttrsOfPlan(array('plan_id'=>$plan_id));

			foreach ($attrs as $attr_id => $value) {
				if (!empty($attr_data[$attr_id])) {
					$id = $attr_data[$attr_id]['id'];
					if ($attr_data[$attr_id]['value'] != $value) {
						$data = array('value' => $value);

						$old_data = $attr_data[$attr_id];

						foreach($data as $k => $v) {
				    		if(array_key_exists($k, $old_data))
				    			$dup[$k] = $old_data[$k];
				    	}

						if(array_diff($dup, $data)) { 
							$history = array(
								'table_name' 		=> 'attribute_data',
								'user_id' 			=> $this->user->getId(),
								'user_update' 		=> $this->user->getUserName(),
								'controller' 		=> 'plan/plan_survey',
								'table_primary_id' 	=> $plan_id,
								'data' 				=> json_encode($data),
								'old_data' 			=> json_encode($old_data)	
							);
							$this->model_history_history->add($history);
						}
						
						$this->model_catalog_attribute_data->update($id, array('value'=>$value));
					}
				} else {
					if ($value != '') {
						$data = array(
							'plan_id' => $plan_id,
							'attribute_id' => $attr_id,
							'value' => $value,
							'round_name' => $plan['round_name']
						);
						$this->model_catalog_attribute_data->add($data);
					}
				}
			}

			echo 1;
		}
	}
}