<?php
class ControllerPlanProgress extends Controller{
	private $error = array();

	private $field_data = array(
			'plan_id'=>'',
			'plan_name'=>''
		);

	private $filter_key = array(
		'filter_round_name',
		'filter_date',
		'filter_group_id'
	);

	private function _url(){
		$url = '';
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}
	
	private function validateDate($date, $format = 'Y-m-d H:i:s') {
			$d = DateTime::createFromFormat($format, $date);
			$return =  $d && $d->format($format) == $date;
			return ($return==true)?1:0;
	}
	
	private function countPlansNotMade($filter=array()){
		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "plan WHERE `plan_status` = '0'";
		if($filter['is_checkin']==1){
			$sql .= " AND `time_checkin`!='0000-00-00 00:00:00'";
		}
		if($filter['is_checkin']==0){
			$sql .= " AND `time_checkin`='0000-00-00 00:00:00'";
		}
		if(isset($filter['import_time'])){
			$sql .= " AND `import_time` = '" . (int)$filter['import_time'] . "' ";
		}
		if(isset($filter['user_id'])){
			$sql .= " AND `user_id` = '" . (int)$filter['user_id'] . "' ";
		}
		if(isset($filter['user_ids'])){
			$sql .= " AND `user_id` IN('".implode("','",$filter['user_ids'])."')";
		}
		$query = $this->pdb->query($sql);
		return isset($query->row['total'])?$query->row['total']:0;
			
	}
	public function change_date(){
		
		$languages = $this->load->language('common/header');
		$json = array();
			$user_id = $this->user->getId();
			$user_group_id = $this->user->getGroupId();
			$user_pa = $this->config->get('config_user_pa');
			$user_pl = $this->config->get('config_user_pl');
			$group_edit = array(1,$user_pa,$user_pl);
			
			
		if($this->user->isLogged()&&in_array($user_group_id,$group_edit)&&($this->request->server['REQUEST_METHOD'] == 'POST')&&isset($this->request->post['date_end'])) {
			$import_time = time();
			$date_end = $this->request->post['date_end'];
				/*1: Cập nhận date_endcho plan*/ 
				$filter = array();
				
				//$filter['is_checkin'] = 1;
				if(isset($this->request->post['user_ids'])&&!empty($this->request->post['user_ids'])){
					$filter['user_ids'] = $this->request->post['user_ids'];
				}
				$before = $this->countPlansNotMade($filter);
				
				//$filter['is_checkin'] = 0;
				$filter['import_time'] = $import_time;
				
				if($this->validateDate($date_end,'Y-m-d')==1&&$user_id>0){
					
					$sql = "UPDATE " . PDB_PREFIX . "plan SET `import_time` = '" . (int)$import_time . "',`import_user` = '" . (int)$user_id . "'";
							
					$sql .= ",`date_end` = '" . $this->pdb->escape($date_end ) . "'";
					
					$sql .= " WHERE `plan_status` = '0'";
					
					if(isset($this->request->post['user_ids'])&&!empty($this->request->post['user_ids'])){
						$sql .= " AND `user_id` IN('".implode("','",$this->request->post['user_ids'])."')";
					}
					
					$this->pdb->query($sql);
					$after = $this->countPlansNotMade($filter);
					$json['success'] = $this->language->get('Thay đổi '.$after.' data, '.$before.' data đã checkin!');
			
				}else{
					$json['error'] =  $this->language->get('Chưa chọn ngày!');
				}
		}else{
			$json['error'] =  $this->language->get('Bạn không có quyền xem!');
			
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}
	public function change_plan(){
		
		$languages = $this->load->language('common/header');
		$json = array();
			$user_id = $this->user->getId();
			$user_group_id = $this->user->getGroupId();
			$user_pa = $this->config->get('config_user_pa');
			$user_pl = $this->config->get('config_user_pl');
			$group_edit = array(1,$user_pa,$user_pl);
			
			
		if($this->user->isLogged()&&in_array($user_group_id,$group_edit)&&($this->request->server['REQUEST_METHOD'] == 'POST')) {
			if(isset($this->request->post['plan_ids'])&&!empty($this->request->post['plan_ids'])&&isset($this->request->post['to_user_id'])&&isset($this->request->post['filter_user'])) {
				$import_time = time();
				$plan_ids = $this->request->post['plan_ids'];
				$from_user = $this->request->post['filter_user'];
				$to_user_id = $this->request->post['to_user_id'];
				
			$this->load->model('project/project_user');
			$to_user = $this->model_project_project_user->getUser($to_user_id);
			
				$filter = array();
				
				//filter user cũ
				$filter['is_checkin'] = 1;
				$filter['user_id'] = $from_user;
				
				$before = $this->countPlansNotMade($filter);
				
				$filter['is_checkin'] = 0;
				
				if(isset($to_user['user_id'])&&$user_id>0){
					/*1: Cập nhận user id cho plan*/ 
					$sql = "UPDATE " . PDB_PREFIX . "plan SET  `import_id` = '" . (int)$import_time . "',`import_time` = '" . (int)$import_time . "'";
					$sql .= ",`import_user` = '" . (int)$user_id . "'";
					$sql .= ",`user_id` = '" . (int)$to_user['user_id']  . "'";
					$sql .= ",`sup_id` = '" . (int)$to_user['user_parent_id'] . "'";
					$sql .= ",`usercode` = '" . $this->pdb->escape($to_user['usercode']) . "'";
					
					$sql .= " WHERE `plan_status` = '0' AND `time_checkin`='0000-00-00 00:00:00'";
					
					$sql .= " AND `user_id` = '" . (int)$from_user . "' AND `plan_id` IN('".implode("','",$this->request->post['plan_ids'])."')";
						
						
					$this->pdb->query($sql);
					
					/*2: Cập nhận user id cho plan_coupon_history */ 
					
					$sql2 = "UPDATE  `" . PDB_PREFIX . "plan_coupon_history` SET `plan_import_id` = '" . (int)$import_time . "',`sup_id` = '" . (int)$to_user['user_parent_id'] . "',";
					$sql2 .= " `user_id` = '" . (int)$to_user['user_id']  . "'";
					$sql2 .= " WHERE `user_id` = '" . (int)$from_user . "' AND `plan_id` IN('".implode("','",$this->request->post['plan_ids'])."')";
					
					$this->pdb->query($sql2);
					//filter user mới
					$filter['user_id'] = $to_user['user_id'];
					$filter['import_time'] = $import_time;
					
					$after = $this->countPlansNotMade($filter);
					
					$json['success'] = $this->language->get('Thay đổi '.$after.' data, '.$before.' data đã checkin!');
				}else {
					$json['error'] = 'Nhân viên này ko tham gia dự án!';
				}
				
			}else {
				$json['error'] = 'Tham số chưa đầy đủ';
			}
		}else{
			$json['error'] =  $this->language->get('Bạn không có quyền thao tác!');
			
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}
	public function approveDevice() {
		
		$user_group_id = $this->user->getGroupId();
		$user_pa = $this->config->get('config_user_pa');
		$user_pl = $this->config->get('config_user_pl');
		$group_edit = array(1,$user_pa,$user_pl);
			
		$json = array();
		if (in_array($user_group_id,$group_edit)&&isset($this->request->post['user_id']) &&isset($this->request->post['status']) && $this->request->server['REQUEST_METHOD'] == 'POST' &&$this->document->hasPermission('edit', 'project/project_user')) {
				$this->load->model('project/project_user');
				if ($this->request->post['status']==1) {
					$this->model_project_project_user->approveDevice($this->request->post['user_id']);
				}else{
					$this->model_project_project_user->rejectDevice($this->request->post['user_id']);
				}
				
				$json['success'] = 'Success!';
		}else {
			$json['error'] = 'Nhân viên không tồn tại';
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function index() {
		$note_fix = 1;
		$note_fixed = 2;
		$data['user_id'] = $this->user->getId();
		$this->load->model('localisation/region');
		$data['regions'] = array();
		$data['regions']['DEFAULT'] = array(
		 'region_id' => 0,
          'region_name' => 'DEFAULT',
          'region_code' => 'DEFAULT'
		);
		
		$regions = $this->model_localisation_region->getRegions();
		foreach ($regions as $region){
			$data['regions'][$region['region_code']] = $region;
		}
		
			$data['today'] = date('Y-m-d');
			
			$this->document->addStyle('assets/plugins/magnific/magnific-popup.css');
			$this->document->addScript('assets/plugins/magnific/jquery.magnific-popup.js');
		$this->load->model('plan/progress');
		$data['user_id'] = $this->user->getId();

		/*StaftUser*/
		$languages = $this->load->language('plan/progress');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$url = $this->_url();

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('plan/progress',  $url, true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);

		
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$filter_data[$key] = trim($this->request->get[$key]);
			} else {
				$filter_data[$key] = NULL;
			}
		}

		if (empty($filter_data['filter_date'])) {
			$filter_data['filter_date'] = date('Y-m-d');
		}

		$managers = array(
			1,
			$this->config->get('config_user_qc'),
			$this->config->get('config_user_pl'),
			$this->config->get('config_user_pa'),
			$this->config->get('config_user_qa'),
			$this->config->get('config_user_dc'),
			$this->config->get('config_user_fl')
		);
		
		
		$data['group_id'] =  $this->user->getGroupId();
		
		$data['managers'] =  array(
			1,
			$this->config->get('config_user_qc'),
			$this->config->get('config_user_pl'),
			$this->config->get('config_user_pa'),
			$this->config->get('config_user_qa')
		);
		
		

		$this->load->model('plan/progress');
		$this->load->model('project/project_user');
		$this->load->model('user/user');

		$users_manager = $this->user->users_manager();
		
		if(in_array($this->user->getGroupId(), $managers)){
			$filter_data['users_manager'] = $users_manager;
			$sups =  $this->model_project_project_user->getProjectUsersByGroupID($this->config->get('config_user_sup'), $filter_data);
			unset($filter_data['users_manager']);
		} elseif($this->user->getGroupId()==$this->config->get('config_user_sup')){
			$filter_data['users_manager'] = $users_manager;
			$sups = $this->model_project_project_user->getProjectUser($this->user->getId(), $filter_data);
			unset($filter_data['users_manager']);
		} else {
			$sups = array();
		}
		
		// $this->load->model('plan/round');
		// $data['rounds'] =  $this->model_plan_round->getRounds();
		$this->load->model('plan/plan');
		$data['rounds'] = $this->model_plan_plan->countTotal('', 'round_name');
		// p($data,1);
		$this->load->model('catalog/group');
		$data['projects'] = $this->model_catalog_group->getGroups(array('parent_group_id' => 0));

		$users =  $this->model_project_project_user->getProjectUsers($filter_data);
		
		$all_filter = array();
		if (isset($this->request->get['filter_group_id'])) { 
			$all_filter['filter_group_id'] = trim($this->request->get['filter_group_id']);
		}
		if (isset($this->request->get['filter_round_name'])) {
			$all_filter['filter_round_name'] = trim($this->request->get['filter_round_name']);
		}else{
			$all_filter['filter_round_name'] = date('Y-m');
		}
		
		$data['all_total'] = $all_total =  $this->model_plan_plan->countTotal($all_filter);
		$all_filter['filter_plan_status'] = 1;
		$data['all_made'] = $all_made = $this->model_plan_plan->countTotal($all_filter);
		$data['all_percent'] = ($all_total> 0) ? round(($all_made/$all_total)*100, 1) : 0;
		
		
		$filter_data['filter_plan_status'] = 1;
		$filter_total = $this->model_plan_plan->countTotal($filter_data);
		
		$data['filter_made'] = $filter_made = $this->model_plan_plan->countTotal($filter_data);
		
		
		$data['filter_percent'] = ($all_total> 0) ? round(($filter_made/$all_total)*100, 1) : 0;
		$data['sups'] = array();
		$problem_total = $this->model_plan_progress->getTotalProblemsGroupByUserId($filter_data);
		
		$note_total = $this->model_plan_progress->getTotalPlanNotesGroupByUserId($filter_data);
		
		//print_r('<pre>'); print_r($note_total); print_r('</pre>'); 
		
		$qc_total = $this->model_plan_progress->getTotalPlansQcGroupByUserId($filter_data);
		$plan_made_total = $this->model_plan_progress->getTotalPlansMadeGroupByUserId($filter_data);
		
		if(!isset($this->request->get['filter_date_start']) && !isset($this->request->get['filter_date_end'])){
			$plan_today = $this->model_plan_progress->getTotalPlansTodayGroupByUserId($filter_data);
		} else {
			$plan_today = $this->model_plan_progress->getTotalPlansMadeGroupByUserId($filter_data);
		}

		$qc_by_sup_total = $this->model_plan_progress->getTotalPlansQcByGroupId($this->config->get('config_user_sup'), $filter_data);
		$qc_by_dc_total = $this->model_plan_progress->getTotalPlansQcByGroupId($this->config->get('config_user_dc'), $filter_data);
		$qc_by_pa_total = $this->model_plan_progress->getTotalPlansQcByGroupId($this->config->get('config_user_pa'), $filter_data);

		// $chanels = $this->model_plan_progress->getTotalChanelGroupByUser();
		$posms = $this->model_plan_progress->getTotalPosmGroupByUser();

		$reason = $this->model_plan_progress->getTotalPlansReasonGroupByUserId($filter_data);

		$date = isset($filter_date) ? $filter_date : date('Y-m-d');
		$checkins = $this->model_plan_progress->getCheckInGroupByUser($filter_data);

		foreach($sups as $sup){
			if ($sup['user_status']==1) {
				$sup_plan_total    = 0;
				$sup_qc        = 0;
				$sup_fix_total    = 0;
				$sup_fix_percent    = 0;
				$sup_code_total    = 0;
				$sup_code_percent    = 0;
				$sup_plan_made = 0;
				$sup_today     = 0;
				$sup_tc            = 0;
				$sup_ktc           = 0;
				$sup_total_qc_pa = 0;
				$sup_total_qc_dc = 0;
				$sup_total_qc_sup = 0;
				// $on_chanel = 0;
				// $off_chanel = 0;
				$picture_pepsi_7up = 0;
				$sticker_pushcart = 0;
				$checkin_count = 0;
				$staffs[$sup['user_id']] = array();
				foreach($users as $user){
					if ($user['user_parent_id']==$sup['user_id']&&$user['plan_total']>0) {
						
						$staff_qc        = isset($qc_total[$user['user_id']])?$qc_total[$user['user_id']]:0;
						$staff_plan_made = isset($plan_made_total[$user['user_id']])?$plan_made_total[$user['user_id']]:0;
						$staff_plan_rest = $user['plan_total']-$staff_plan_made;
						$staff_today     = isset($plan_today[$user['user_id']])?$plan_today[$user['user_id']]:0;
						
						$qc_by_sup = isset($qc_by_sup_total[$user['user_id']])?count($qc_by_sup_total[$user['user_id']]):0;
						$qc_by_dc = isset($qc_by_dc_total[$user['user_id']])?count($qc_by_dc_total[$user['user_id']]):0;
						$qc_by_pa = isset($qc_by_pa_total[$user['user_id']])?count($qc_by_pa_total[$user['user_id']]):0;
						
	
						$sup_plan_total    += $user['plan_total'];
						
						
						$sup_plan_made += $staff_plan_made;
						
						/*Code*/ 
						$code_total  = 0;
						$code_percent  = 0;
						if(isset($problem_total[$user['user_id']])){
							$code_total  = $problem_total[$user['user_id']];
							
							$sup_code_total    +=$code_total;
							$code_percent  = ($code_total>0&&$staff_plan_made>0) ? round(($code_total/$staff_plan_made)*100, 1) : 0;
						}
						
						/*Fix*/ 
						$fix_total  = 0;
						$fix_percent  = 0;
						if(isset($note_total[$user['user_id']])&&isset($note_total[$user['user_id']][1])){
							$fix_total  = $note_total[$user['user_id']][1];
							
							$sup_fix_total    +=$fix_total;
							$fix_percent  = ($fix_total>0&&$staff_plan_made>0) ? round(($fix_total/$staff_plan_made)*100, 1) : 0;
						}
						
						
						$sup_qc        += $staff_qc;
						$sup_today     += $staff_today;
						$sup_total_qc_pa += $qc_by_pa;
						$sup_total_qc_dc += $qc_by_dc;
						$sup_total_qc_sup += $qc_by_sup;
	
						$tc            = 0;
						$ktc           = 0;
						if(isset($reason[$user['user_id']])){
							$tc = $reason[$user['user_id']]['tc'];
							$ktc = $reason[$user['user_id']]['ktc'];
							$sup_tc += $reason[$user['user_id']]['tc'];
							$sup_ktc += $reason[$user['user_id']]['ktc'];
						} 
						
						// $staff_on_chanel = isset($chanels[$user['user_id']][1]) ? $chanels[$user['user_id']][1] : 0;
						// $staff_off_chanel = isset($chanels[$user['user_id']][2]) ? $chanels[$user['user_id']][2] : 0;
						$staff_picture_pepsi_7up = isset($posms[$user['user_id']][1]) ? $posms[$user['user_id']][1] : 0;
						$staff_sticker_pushcart = isset($posms[$user['user_id']][2]) ? $posms[$user['user_id']][2] : 0;
						$staff_checkin_count = isset($checkins[$user['user_id']]) ? $checkins[$user['user_id']] : 0;

						// $on_chanel += $staff_on_chanel;
						// $off_chanel += $staff_off_chanel;
						$picture_pepsi_7up += $staff_picture_pepsi_7up;
						$sticker_pushcart += $staff_sticker_pushcart;
						// $picture_frame += $staff_picture_frame;
						// $sticker_juicy += $staff_sticker_juicy;
						// $sticker_twister += $staff_sticker_twister;
						$checkin_count += $staff_checkin_count;

						$staff[$sup['user_id']][] = array(
							'user_id'            => $user['user_id'],
							'usercode'           => $user['usercode'],
							'request_count'           => $user['request_count'],
							'device_model'           => $user['device_model'],
							'device_imei'           => $user['device_imei'],
							'app_version'           => $user['app_version'],
							'new_imei'           => $user['new_imei'],
							'new_model'           => $user['new_model'],
							'email'              => $user['email'],
							'telephone'          => $user['telephone'],
							'user_parent_id'     => $user['user_parent_id'],
							'project_user_group' => $user['project_user_group'],
							'user_status'        => $user['user_status'],
							'user_update'        => $user['user_update'],
							'province_ids'       => $user['province_ids'],
							'fullname'           => $user['fullname'],
							'group_name'         => $user['group_name'],
							'plan_total'         => $user['plan_total'],
							'plan_rest'         => $staff_plan_rest,
							'staff_plan_made'    => $staff_plan_made,
							'staff_qc'           => $staff_qc,
							'staff_today'        => $staff_today,
							'code_total'        => $code_total,
							'code_percent'        => $code_percent,
							'fix_total'        => $fix_total,
							'fix_percent'        => $fix_percent,
							'percent'            => ($user['plan_total']!=0) ? round(($staff_plan_made/$user['plan_total'])*100, 1) : 0,
							'href'               => $this->url->link('plan/plan','filter_date='.$date.'&filter_plan_status=1&filter_user_id='.$user['user_id']),
							'href2'               => $this->url->link('plan/plan',$url.'&filter_plan_status=1&filter_user_id='.$user['user_id']),
							'qc_by_sup'          => $qc_by_sup,
							'qc_by_dc'           => $qc_by_dc,
							'qc_by_pa'           => $qc_by_pa,
							'tc'                 => $tc,
							'ktc'                => $ktc,
							// 'on_chanel' => $staff_on_chanel,
							// 'off_chanel' => $staff_off_chanel,
							'picture_pepsi_7up' => $staff_picture_pepsi_7up,
							'sticker_pushcart' => $staff_sticker_pushcart,
							// 'picture_frame' => $staff_picture_frame,
							// 'sticker_juicy' => $staff_sticker_juicy,
							// 'sticker_twister' => $staff_sticker_twister,
							'checkin_count' => $staff_checkin_count
						);
					}
				}
				$region_code = !empty($sup['region_code'])?$sup['region_code']:'DEFAULT';
				
				
							$sup_code_percent    =($sup_code_total>0&&$sup_plan_made>0) ? round(($sup_code_total/$sup_plan_made)*100, 1) : 0;
							$sup_fix_percent    =($sup_fix_total>0&&$sup_plan_made>0) ? round(($sup_fix_total/$sup_plan_made)*100, 1) : 0;
							$sup_percent    = ($sup_plan_made!=0) ? round(($sup_plan_made/$sup_plan_total)*100, 1) : 0;
							
							
					$data['regions'][$region_code]['sup_ids'][] = $sup['user_id']; 
				$data['sups'][$sup['user_id']] = array(
					'user_id'            => $sup['user_id'],
					'region_code'           => $region_code,
					'usercode'           => $sup['usercode'],
					'email'              => $sup['email'],
					'telephone'          => $sup['telephone'],
					'user_parent_id'     => $sup['user_parent_id'],
					'parent_fullname'     => $sup['parent_fullname'],
					'project_user_group' => $sup['project_user_group'],
					'user_status'        => $sup['user_status'],
					'user_update'        => $sup['user_update'],
					'province_ids'       => $sup['province_ids'],
					'fullname'           => $sup['fullname'],
					'group_name'         => $sup['group_name'],
					'plan_total'         => $sup_plan_total,
					'sup_code_total'      => $sup_code_total,
					'sup_code_percent'      => $sup_code_percent,
					'sup_fix_total'      => $sup_fix_total,
					'sup_fix_percent'      => $sup_fix_percent,
					'sup_plan_made'      => $sup_plan_made,
					'percent'            => $sup_percent,
					'sup_qc'             => $sup_qc,
					'sup_today'          => $sup_today,
					'staff'              => isset($staff[$sup['user_id']])?$staff[$sup['user_id']]:array(),
					'tc'                 => $sup_tc,
					'ktc'                => $sup_ktc,
					'total_qc_pa'        => $sup_total_qc_pa,
					'total_qc_dc'        => $sup_total_qc_dc,
					'total_qc_sup'        => $sup_total_qc_sup,
					// 'on_chanel' => $on_chanel,
					// 'off_chanel' => $off_chanel,
					'picture_pepsi_7up' => $picture_pepsi_7up,
					'sticker_pushcart' => $sticker_pushcart,
					// 'picture_frame' => $picture_frame,
					// 'sticker_juicy' => $sticker_juicy,
					// 'sticker_twister' => $sticker_twister,
					'checkin_count' => $checkin_count
				);
			}
		}

		// p($data['sups'],1);
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$data[$key] = trim($this->request->get[$key]);
			} else {
				$data[$key] = NULL;
			}
		}

		if (isset($this->request->get['filter_round'])) {
				$data['filter_round'] = trim($this->request->get['filter_round']);
		} else{
				$data['filter_round'] = NULL;
		}
		
		/*Comment*/

		$data['statuses'] = array(
			'0'=> $this->language->get('text_no_perform'),
			'1'=> $this->language->get('text_has_perform')
		);

		
			$data['user_group_id'] = $this->user->getGroupId();
			$user_pa = $this->config->get('config_user_pa');
			$user_pl = $this->config->get('config_user_pl');
			$data['group_edit'] = array(1,$user_pa,$user_pl);
			
			
		$template = 'plan/progress';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
}