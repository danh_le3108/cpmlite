<?php
class ControllerAccountLogout extends Controller {
	public function index() {
		if ($this->user->isLogged()) {
			$this->user->logout();
			$this->response->redirect($this->url->link('common/home', '', true));
		}
		if ($this->customer->isLogged()) {
			$this->customer->logout();
			$this->response->redirect($this->url->link('common/home', '', true));
		}
		if ($this->distributor->isLogged()) {
			$this->distributor->logout();
			$this->response->redirect($this->url->link('common/home', '', true));
		}
		$languages= $this->load->language('account/logout');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}

		$this->load->language('account/logout');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_logout'),
			'href' => $this->url->link('account/logout', '', true)
		);

		$data['continue'] = $this->url->link('common/home');
		
		$template = 'common/success';
		$this->document->setBreadcrumbs($data['breadcrumbs']);
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
}
