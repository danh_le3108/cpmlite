<?php
class ControllerAccountLogin extends Controller {
	private $error = array();
	public function index() {
		$languages= $this->load->language('account/login');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('account/customer');
		$this->load->model('user/user');
		$this->load->model('catalog/distributor');
		// Login override for admin users
		if (!empty($this->request->get['token'])) {
			$this->customer->logout();
			$customer_info = $this->model_account_customer->getCustomerByToken($this->request->get['token']);
			if ($customer_info && $this->customer->login($customer_info['username'], '', true)) {
				$this->response->redirect($this->url->link('account/account', '', true));
			}
		}
		
		

		if ($this->customer->isLogged()&&!$this->user->isLogged()&&!$this->distributor->isLogged()) {
			//$this->response->redirect($this->url->link('chart/morris', '', true));
		}else if($this->user->isLogged()&&!$this->customer->isLogged()&&!$this->distributor->isLogged()){
			//$this->response->redirect($this->url->link('plan/progress', '', true));
		}else if($this->distributor->isLogged()&&!$this->user->isLogged()&&!$this->customer->isLogged()){
			//$this->response->redirect($this->url->link('distributor/coupon_history', '', true));
		}

		$this->load->language('account/login');


		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()&&isset($this->request->post['username'])&&isset($this->request->post['password'])) {
			// Unset guest
			unset($this->session->data['guest']);

			// Add to activity log
			if ($this->config->get('config_history_activity')) {
				$this->load->model('account/activity');

				$activity_data = array(
					'customer_user_id' => $this->customer->getId(),
					'name'        => $this->customer->getFullName() 
				);

				$this->model_account_activity->addActivity('login', $activity_data);
			}

			// Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
			if (isset($this->request->post['redirect']) && $this->request->post['redirect'] != $this->url->link('account/logout', '', true) && (strpos($this->request->post['redirect'], $this->config->get('config_url')) !== false || strpos($this->request->post['redirect'], $this->config->get('config_ssl')) !== false)) {
				$this->response->redirect(str_replace('&amp;', '&', $this->request->post['redirect']));
			} else {
				$this->response->redirect($this->url->link('common/home', '', true));
			}
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_login'),
			'href' => $this->url->link('account/login', '', true)
		);
		$error_data = array(
		'error_warning'=>''
		);
		foreach($error_data as $key=>$default_value){
			if (isset($this->error[$key])) {
				$data[$key] = $this->error[$key];
			} else {
				$data[$key] = $default_value;
			}
		}

		if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];
			unset($this->session->data['error']);
		}

		$data['action'] = $this->url->link('account/login', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['forgotten'] = $this->url->link('account/forgotten', '', true);

		// Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
		if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], $this->config->get('config_url')) !== false || strpos($this->request->post['redirect'], $this->config->get('config_ssl')) !== false)) {
			$data['redirect'] = $this->request->post['redirect'];
		} elseif (isset($this->session->data['redirect'])) {
			$data['redirect'] = $this->session->data['redirect'];

			unset($this->session->data['redirect']);
		}else if (isset($this->request->get['route'])) {
			$route = $this->request->get['route'];

			unset($this->request->get['route']);
			unset($this->request->get['token']);

			$url = '';

			if ($this->request->get) {
				if (isset($this->request->get['_url'])) {
					unset($this->request->get['_url']);
				}
				$url .= http_build_query($this->request->get);
			}

			$data['redirect'] = $this->url->link($route, $url, true);
		} else {
			$data['redirect'] = $this->url->link('common/home');
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['username'])) {
			$data['username'] = trim($this->request->post['username']);
		} else {
			$data['username'] = '';
		}

		if (isset($this->request->post['password'])) {
			$data['password'] =  trim($this->request->post['password']);
		} else {
			$data['password'] = '';
		}
		
		$data['user_logged'] = $user_logged = $this->user->isLogged();
		$data['customer_logged'] = $customer_logged = $this->user->isLogged();
		$data['distributor_logged'] = $distributor_logged = $this->distributor->isLogged();
		
			
			$data['register'] = $this->url->link('account/register', '', true);
			$data['login'] = $this->url->link('account/login', '', true);
			$data['logout'] = $this->url->link('account/logout', '', true);
			$data['forgotten'] = $this->url->link('account/forgotten', '', true);
			$data['account'] = $this->url->link('account/account', '', true);
			$data['edit'] = $this->url->link('account/edit', '', true);
			$data['link_password'] = $this->url->link('account/password', '', true);	
			
		if($user_logged ){
			$data['edit'] = $this->url->link('user/user', '', true);
			$data['logout'] = $this->url->link('user/logout', '', true);
		}
		
		if($distributor_logged ){
			$data['edit'] = $this->url->link('distributor/distributor', '', true);
			$data['logout'] = $this->url->link('distributor/logout', '', true);
		}
		
		// Whos Online - Chạy ở home và account nha
		if ($this->config->get('config_history_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}
			if (isset($this->request->server['HTTP_USER_AGENT'])) {
				$user_agent= $this->request->server['HTTP_USER_AGENT'];
			} else {
				$user_agent = '';
			}
				
			if($this->customer->isLogged()){
				$username = $this->customer->getUserName();
			}else{
				$username = $this->user->getUserName();
			}
			$this->model_tool_online->addOnline($ip,$this->user->getId(),$this->customer->getId(),$username, $url, $referer,$user_agent);
		}
		

		$template = 'account/login';
		$this->document->setBreadcrumbs($data['breadcrumbs']);
		
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}

	protected function validate() {
		if(isset($this->request->post['username'])&&isset($this->request->post['password'])){
			$username = trim($this->request->post['username']);
			$password = trim($this->request->post['password']);
			// Check if customer has been approved.
			$user_info = $this->model_user_user->getUserByUsername($username);
			// Check if customer has been approved.
			$customer_info = $this->model_account_customer->getCustomerByUsername($username);
			// Check if distributor has been approved.
			$distributor_info = $this->model_catalog_distributor->getDistributorByCode($username);
			if ($user_info&&!$customer_info&&!$distributor_info) {
				if (!$this->user->login($username, $password)) {
					$this->error['error_warning'] = $this->language->get('text_error_login');
				} 
			}
			if ($distributor_info&&!$customer_info&&!$user_info) {
				//$this->log->write($distributor_info); 
				if (!$this->distributor->login($username, $password)) {
					$this->error['error_warning'] = $this->language->get('text_error_login');
				} 
				//$this->log->write($this->distributor->isLogged());
			}
			if ($customer_info&&!$user_info&&!$distributor_info) {
				if (!$this->customer->login($username, $password)) {
					$this->error['error_warning'] = $this->language->get('text_error_login');
				}
				$this->load->model('account/customer');
				$this->model_account_customer->addLoginAttempt($username);
			}
	
			if (!$user_info&&!$customer_info&&!$distributor_info) {
				$this->error['error_warning'] = $this->language->get('text_error_login');
			}
		}else{
			$this->error['error_warning'] = $this->language->get('text_error_login');
		}
		return !$this->error;
	}
}
