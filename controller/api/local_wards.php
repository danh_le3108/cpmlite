<?php
class ControllerApiLocalWards extends Controller {
	private $ward_fields = array(
	'ward_id',
	'district_id',
	'province_id',
	'prefix',
	'name',
	'ward_name',
	'ward_alias',
	'latitude',
	'longitude'
	);
	public function index() {
		$json = array();
		$json['api_name'] = 'local_wards';
		
		
		$this->load->language('api/api');

		$this->load->model('localisation/ward');
		$ward_data = array();
		$json['status'] = 1;
		$json['message'] = $this->language->get('text_success');
		$wards =  $this->model_localisation_ward->getWards();
		foreach($wards as $ward){
			$ward_info = array(
				'ward_id'=>$ward['ward_id'],
				'name'=>$ward['name'],
				'ward_name'=>$ward['ward_name'],
				'district_id'=>$ward['district_id'],
				'province_id'=>$ward['province_id'],
				'latitude'=>$ward['latitude'],
				'longitude'=>$ward['longitude']
			);
			$json['data'][] = $ward_info;
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
