<?php
class ControllerApiGetPromotions extends Controller {
	public function index() {
		$json = array();
		$json['api_name'] = 'get_promotions';
		// $json['reason_id_show_comment'] = 7;
		$this->load->model('catalog/attribute');
		$attrs = array();
		$json['data'] = $this->model_catalog_attribute->getAttributesByCode('PROMOTION');
		$json['status'] = 1;
		$json['message'] = 'Load Attributes Success';

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->response->setOutput(json_encode($json));

	}
}
