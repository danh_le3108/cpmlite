<?php
class ControllerApiUploadSurvey extends Controller {
	public function index() {
		$json = array();
		$json['api_name'] = 'upload_survey';
		
		
		if(!isset($this->request->post['api_key'])){
			$json['status'] = 0;
			$json['message'] = $this->language->get('text_error_key');
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}
		if(isset($this->request->post['user_id'])&&$this->request->post['user_id']==3519){
			$this->log->write('updatePlanSurvey 1'); 
		}
		$this->log->write('upload survey : ' .json_encode( $this->request->post)); 
		$json['status'] = 1;
		$json['message'] = 'Success';
		$is_fix = isset($this->request->post['is_fix'])?$this->request->post['is_fix']:0;
		// if (isset($this->request->post['api_key'])&&trim($this->request->post['api_key']) ==$this->config->get('config_api')&&isset($this->request->post['user_id'])&&isset($this->request->post['plan_id'])) {
		// 		$user_id = $this->request->post['user_id'];
		// 		$plan_id = $this->request->post['plan_id'];
		// 		$this->load->model('plan/plan');
		// 		$plan_info = $this->model_plan_plan->getPlanInfo($plan_id);
		// 		if(isset($plan_info['plan_id'])&&$user_id==$plan_info['user_id']){
		// 			$json['status'] = 1;
					
		// 			$json['message'] = 'Có '.(int)$count_total .' câu trả lời.';
		// 		}  else {
		// 			$json['status'] = 0;
		// 			$json['message'] = 'Không có plan này';
		// 		}
		// }else if(isset($plan_info['plan_id'])&&($plan_info['user_id']!=$user_id||(int)$plan_info['is_deleted']==1)){//||$store_code!= $plan_info['store_code']
		// 		$json['status'] = 0;
		// 		$json['message'] = 'Nội dung thực hiện của CH này đã thay đổi từ hệ thống, vui lòng upload những cửa hàng bạn đã thực hiện hôm nay sau đó tải lại danh sách hoặc liên hệ Admin!';
		// }  else {
		// 	$json['status'] = 0;
		// 	$json['message'] = 'Không có Plan hoặc Nhân viên này';
		// }

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->response->setOutput(json_encode($json));

	}
	private function updatePlanSurvey($plan_id, $sdata) {
			$this->load->model('plan/plan');
			
			$plan_info = $this->model_plan_plan->getPlanInfo($plan_id);
		
			$decode_data = json_decode(html_entity_decode(str_replace(array('“','”'),array('"','"'),$this->request->post['planogram_data']), ENT_QUOTES, 'UTF-8'),true);
			
			if(is_array($decode_data)&&!empty($decode_data)){
				foreach($decode_data as $value){
					if(isset($value['survey_id'])){
						$survey_id = $value['survey_id'];	
						$survey_data = $value;	
						$this->model_plan_plan->updatePlanSurveyData($plan_id,$survey_id,$survey_data);
		 
					}
				}
			}
	}
	private function countTotal($plan_id){
		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "plan_survey_data s WHERE s.plan_id =  '" . (int)$plan_id . "' GROUP BY s.plan_id";
	
		$query = $this->pdb->query($sql);
		return isset($query->row['total'])?$query->row['total']:0;
	}
	
}
