<?php
class ControllerApiUploadCoupon extends Controller {
	
	private function updateBeta($coupon_history_id, $data) {
		$sql = "UPDATE " . PDB_PREFIX . "plan_coupon_history SET date_modify=NOW()";
		if (isset($data['coupon_confirm'])) {
			$sql .= ",coupon_confirm = '" . $this->db->escape(strtoupper($data['coupon_confirm'])) . "'";
		}
		
		$sql .= " WHERE coupon_history_id = '" . (int)$coupon_history_id . "'";
		
		$this->pdb->query($sql);
	}
	
	public function index() {
		$json = array();
		$json['api_name'] = 'upload_coupon';
		
		
		if(!isset($this->request->post['api_key'])){
			$json['status'] = 0;
			$json['message'] = $this->language->get('text_error_key');
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}
		
				$uids = array(1);

		$is_fix = isset($this->request->post['is_fix'])?$this->request->post['is_fix']:0;
		if (isset($this->request->post['api_key'])&&trim($this->request->post['api_key']) ==$this->config->get('config_api')&&isset($this->request->post['user_id']) && isset($this->request->post['plan_id'])) {
				
			$this->load->model('plan/plan');
			$this->load->model('catalog/coupon');
			
			$plan_id = $this->request->post['plan_id'];
				
				
			$plan_info = $this->model_plan_plan->getPlanInfo($plan_id);
				$store_code = isset($this->request->post['store_code'])?$this->request->post['store_code']:$plan_info['store_code'];
				$store_code = str_replace(array('?',' '),array('',''),trim($store_code));  
				$json['plan_id'] = $plan_id;
				$json['store_code'] = $store_code;
				
				
				
			$json['user_id']  = $user_id = $this->request->post['user_id'];
			
				
			
			$json['config_upload_coupon_id'] = $this->config->get('config_coupon_valid');
			if(isset($this->request->post['coupons_beta'])){
				//$this->log->write('coupons_beta'); 
				//$this->log->write($this->request->post['coupons_beta']); 
				$coupons_beta = json_decode(html_entity_decode($this->request->post['coupons_beta']),true);
				
				if(is_array($coupons_beta)){
					foreach ($coupons_beta as $beta){
							$coupon_history_id = $beta['coupon_history_id'];
							$this->updateBeta($coupon_history_id,$beta);
					}
				}
			}
			
			$plan_coupon = $this->model_plan_plan->getPlanCoupons($plan_id,$plan_info['import_id'],'coupon_history_id');
			
			$this->load->model('catalog/coupon_status');
			$cstatus = $this->model_catalog_coupon_status->getCouponStatusesIndexBy('coupon_status_id');
			
			
			
			$this->load->model('catalog/coupon_prefix');
			$prefix =  $this->model_catalog_coupon_prefix->getPrefixsIndexBy('coupon_prefix');
			
				
				/* if(!empty($uids)&&in_array($user_id,$uids)){
					$this->log->write($plan_id.'Nhóm AAA '.$user_id.' UploadCoupon: '); 
							$json['status'] = 1;
							$json['message'] = $this->language->get('Thành công!');
				}else  { */
				
				if($user_id==$plan_info['user_id']&&(int)$plan_info['is_deleted']==0){//&&$store_code== $plan_info['store_code']
					
				
				if(isset($this->request->post['coupons']) && !empty($this->request->post['coupons'])){
							$this->pdb->query("UPDATE " . DB_PREFIX . "plan SET `time_upload_coupon` = NOW() WHERE plan_id = '" . (int)$plan_id . "'");
						
							$upload_valid = array();
							$config_coupon_has_paid = $this->config->get('config_coupon_has_paid');
							$config_coupon_duplicate = $this->config->get('config_coupon_duplicate');
							$config_coupon_valid = $this->config->get('config_coupon_valid');
							
							$paid_info =  $cstatus[$config_coupon_has_paid];
						
						
							$coupon_data = array();
							$coupons = json_decode(html_entity_decode($this->request->post['coupons']),true);
							
							$total_error = 0;
					
							if(is_array($coupons)){
								foreach ($coupons as $coupon){
										$coupon_history_id = $coupon['coupon_history_id'];
										$this->model_plan_plan->resetCoupon($coupon_history_id);
								}
								$total = 0;
									foreach ($coupons as $coupon){
										$coupon['coupon_code'] = strtoupper($coupon['coupon_code']);
										
										$coupon['plan_id'] = $plan_id;
										$coupon['round_name'] = $plan_info['round_name'];
										$coupon_history_id = $coupon['coupon_history_id'];
										
										$coupon['coupon_status_id'] = 0;
										
										$coupon_info = $this->model_catalog_coupon->getCouponByCode($coupon['coupon_code']);
										if(empty($coupon_info['coupon_id'])){
											$coupon['coupon_status_id'] = $this->config->get('config_coupon_not_exist');
										}else{
											$coupon['coupon_id'] = $coupon_info['coupon_id'];
											$coupon['is_upload'] = 1;
											
											$coupon_double = $this->model_plan_plan->checkCouponDouble($coupon_history_id, $coupon['coupon_code']);
											
											
											$coupon_prefix = substr($coupon['coupon_code'], 0, 2);
						
											if($coupon_prefix != $plan_coupon[$coupon_history_id]['coupon_prefix']||$coupon_prefix != $coupon_info['coupon_prefix']){
												$coupon['coupon_status_id'] = $this->config->get('config_coupon_not_exist');
												$this->log->write($plan_id.'- coupon not_exis 1- '.$coupon['coupon_code']);  
											}
											else if(!empty($coupon_info['coupon_id'])&&$coupon_info['sup_id']!=$plan_info['sup_id']){
												$coupon['coupon_status_id'] = $this->config->get('config_coupon_wrong_sup');
												$this->log->write($plan_id.'- coupon wrong_sup 2- '.$coupon['coupon_code']);  
											}
											elseif(!empty($coupon_info['coupon_id'])&&$coupon_info['coupon_status_id']==$this->config->get('config_coupon_has_deleted')){
												$coupon['coupon_status_id'] = $this->config->get('config_coupon_has_deleted');
												$this->log->write($plan_id.'- coupon has_deleted 3- '.$coupon['coupon_code']); 
											}elseif($coupon['coupon_status_id'] ==0&&$coupon_double==1){
												$coupon['coupon_status_id'] =  $this->config->get('config_coupon_duplicate');
												$coupon['coupon_id'] = 0;
												$coupon['double_id'] = $coupon_info['plan_id'];
												$upload_valid[] = 1;
												$this->log->write($plan_id.'- coupon duplicate 4- '.$coupon['coupon_code']); 
											}elseif($coupon['coupon_status_id'] ==0&&$coupon_double==0){
												$coupon['coupon_status_id'] = $config_coupon_has_paid;
												$coupon['coupon_id'] = $coupon_info['coupon_id'];
												$coupon['date_upload'] = date('Y-m-d H:i:s');
												$upload_valid[] = 1;
												//$this->log->write($plan_id.'- coupon coupon_valid 5- '.$coupon['coupon_code']); 
											}
										
										}
										if(isset($cstatus[$coupon['coupon_status_id']])){	
											$coupon['coupon_status_id'] = $cstatus[$coupon['coupon_status_id']]['coupon_status_id'];
											$coupon['message'] = $cstatus[$coupon['coupon_status_id']]['coupon_status_name'];
										}else{
											$coupon['coupon_status_id'] = 0;
											$coupon['message'] = '';
										}
											
										$this->model_plan_plan->updatePlanCoupon($coupon['coupon_history_id'],$coupon);
										
										if(!in_array($coupon['coupon_status_id'],array($config_coupon_duplicate,$config_coupon_has_paid))){
											$upload_valid[] = 0;
											$total_error++;
										}
										
										$total++;
									}
									$this->model_plan_plan->updateCouponStatus($plan_id);
							}
							$count_total = $this->countTotal($plan_id);
							
							if($count_total>0){//&&!empty($upload_valid)&&!in_array(0,$upload_valid)
								$json['status'] = 1;
								$json['message'] = $this->language->get('text_success');
								
					
							}else{
								$total_text = $total_error.'/'.$total;
								$json['status'] = 0;
								$json['message'] = $total_text.' coupon chưa hợp lệ, vui lòng kiểm tra lại';
							}
								
							$json['coupons'] = $this->model_plan_plan->getApiPlanCoupons($plan_id,$plan_info['import_id'],1);	
							
						} else {
							$json['status'] = 0;
							$json['message'] = 'Danh sách coupon rỗng';
						}
					}else if(isset($plan_info['plan_id'])&&($plan_info['user_id']!=$user_id||(int)$plan_info['is_deleted']==1)){//||$store_code!= $plan_info['store_code']
							$json['status'] = 0;
							$json['message'] = 'Nội dung thực hiện của CH này đã thay đổi từ hệ thống, vui lòng upload những cửa hàng bạn đã thực hiện hôm nay sau đó tải lại danh sách hoặc liên hệ Admin!';
					}else {
						$json['status'] = 0;
						$json['message'] = $this->language->get('Plan này không phải của bạn!');
					}
				//}
			
		} else {
			$json['status'] = 0;
			$json['message'] = $this->language->get('text_error_key');
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}
		
		$json_encode = json_encode($json);
		
		//$this->log->write('Upload Coupon: '.$json_encode); 
		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->response->setOutput($json_encode);

	}
	private function countTotal($plan_id){
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "plan_coupon_history c WHERE c.plan_id =  '" . (int)$plan_id . "' AND c.user_id >0 AND c.coupon_code != '' GROUP BY c.plan_id";
		$query = $this->pdb->query($sql);
		return isset($query->row['total'])?$query->row['total']:0;
	}
	
}