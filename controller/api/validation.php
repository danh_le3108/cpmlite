<?php
class ControllerApiValidation extends Controller {
	public function index($data = array()) {
		$result = array(
			'message' => '',
			'status' => 0,
			'user' => array(),
			'plan' => array(),
		);
		
		$post = $data['post'];
		$require = $data['require'];
		$user = array();
		foreach ($require as $key) {
			if (empty($post[$key])) {
				$result['message'] = 'Thiếu ' . $key;
				return $result;
			} elseif ($key == 'api_key' && $post['api_key'] != $this->config->get('config_api')) {
				$result['message'] = 'Sai ' . $key;
				return $result;
			} elseif ($key == 'user_id') {
				if (!is_numeric($post['user_id'])) {
					$result['message'] = 'Nhân viên không có trong dự án!';
					return $result;
				} 
				$this->load->model('project/project_user');
				$user = $this->model_project_project_user->getUser($post['user_id']);
				if (!$user || $user['user_status'] != 1) {
					$result['message'] = 'Nhân viên không có trong dự án!';
					return $result;
				}
			} elseif ($key == 'version' && !in_array($post['version'],$this->config->get('config_app_versions'))) {
				$result['message'] = 'Bạn đang sử dụng app cũ. Vui lòng cập nhật app mới!';
				return $result;
			}
		}
		// p($post,1);
		$result['user'] = $user;
		if (isset($post['plan_id'])) {
			$this->load->model('plan/plan');
			$plan_id = (int)$post['plan_id'];
			$plan = $this->model_plan_plan->getPlans(array('plan_id' => $plan_id, 'user_id' => $user['user_id']));
			
			if (!$plan) {
	 			$result['message'] = "Nhân viên không có cửa hàng này! (plan_id = {$plan_id})";
				return $result;
	 		}
	 		$plan = $plan[0];
			if ($plan['date_end'] < date('Y-m-d')) {
				$result['message'] = "Đã hết hạn thực hiện CH này! (plan_id = {$plan_id})";
				return $result;
			}
			if ($plan['plan_status'] == 1 &&  $plan['is_fix'] != 1) {
				$result['message'] = "Cửa hàng này đã thực hiện! (plan_id = {$plan_id})";
				return $result;
			}
			$result['plan'] = $plan;
		}
		$result['status'] = 1;
		return $result;
	}	
}
