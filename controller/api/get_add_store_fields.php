<?php
class ControllerApiGetAddStoreFields extends Controller {
	public function index() {
		$this->load->model('catalog/attribute');
		$store_attrs = $this->model_catalog_attribute->getAttributes(array('group_id'=>3,'sort'=>'a.sort_order'));
		
		$this->load->model('store/store');
		// $chanels = $this->model_store_store->getChanels();
		// $chanel_list = array();
		// foreach ($chanels as $c) {
		// 	$chanel_list[] = array('id'=>$c['id'],'value'=>$c['name']);
		// }
		$this->load->model('store/store_type');
		$store_type_list = $this->model_store_store_type->getStoreTypes();
		$store_types = array();
		foreach ($store_type_list as $val) {
			$store_types[] = array(
				'id' => $val['store_type_id'],
				'value' => $val['type_name'],
				'chanel' => 0
			);
		}
		$this->load->model('localisation/location');
		// $provinces = $this->model_localisation_location->getProvinces();
		// $province_list = array();
		// foreach ($provinces as $p) {
		// 	$province_list[] = array(
		// 		'id' => $p['province_id'],
		// 		'value' => $p['name']
		// 	);
		// }
		$districts = $this->model_localisation_location->getDistricts();
		$district_list = array();
		foreach ($districts as $d) {
			$district_list[] = array(
				'id' => $d['district_id'],
				'value' => $d['name'],
				'province_id' => $d['province_id']
			);
		}
		$wards = $this->model_localisation_location->getWards();
		$ward_list = array();
		foreach ($wards as $d) {
			$ward_list[] = array(
				'id' => $d['ward_id'],
				'value' => $d['name'],
				'store_district' => $d['district_id']
			);
		}
		$places = $this->model_localisation_location->getPlaces();
		$place_list = array();
		foreach ($places as $d) {
			$place_list[] = array(
				'id' => $d['name'],
				'value' => $d['name'],
				'store_ward' => $d['ward_id']
			);
		}
		// p($place_list,1);
		$json = array(
			'api_name' => 'get_add_store_fields',
			// 'data' => $data,
			'data' => array(
				array(
					'name' => 'Tên cửa hiệu',
					'field' => 'store_name',
					'input' => 'text',
					'option' => '',
					'default_value' => ''
				),
				array(
					'name' => 'Người kinh doanh',
					'field' => 'store_owner',
					'input' => 'text',
					'option' => '',
					'default_value' => ''
				),
				array(
					'name' => 'Số điện thoại',
					'field' => 'store_phone',
					'input' => 'number',
					'option' => '',
					'default_value' => ''
				),
				// array(
				// 	'name' => 'Kênh',
				// 	'field' => 'chanel',
				// 	'input' => 'select',
				// 	'option' => $chanel_list,
				// 	'default_value' => ''
				// ),
				array(
					'name' => 'Loại cửa hàng',
					'field' => 'store_type_id',
					'input' => 'select',
					'option' => $store_types,
					'default_value' => ''
				),
				// array(
				// 	'name' => 'Tỉnh/ Thành Phố',
				// 	'field' => 'province_id',
				// 	'input' => 'select',
				// 	'option' => $province_list,
				// 	'default_value' => 79
				// ),
				array(
					'name' => 'Quận/ Huyện',
					'field' => 'store_district',
					'input' => 'select',
					'option' => $district_list,
					'default_value' => ''
				),
				array(
					'name' => 'Phường/ Xã',
					'field' => 'store_ward',
					'input' => 'select',
					'option' => $ward_list,
					'default_value' => ''
				),
				array(
					'name' => 'Đường/ Ấp',
					'field' => 'store_place',
					'input' => 'select',
					'option' => $place_list,
					'default_value' => ''
				),
				array(
					'name' => 'Số nhà',
					'field' => 'store_address',
					'input' => 'text',
					'option' => '',
					'default_value' => ''
				),
			),
 			'status' => 1,
			'message' => 'success'
		);
		
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}
        // $this->log->write(json_encode($json));
		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->response->setOutput(json_encode($json));

	}
}
