<?php
class ControllerApiGetSurveys extends Controller {
	public function index() {
		$json = array();
		$json['api_name'] = 'get_surveys';

		$json['data'] = array();
		$attrs = array();
		$this->load->model('catalog/attribute');
		foreach ($this->model_catalog_attribute->getAttributesByCode('MARKET') as $key => $value) {
			$attrs['MARKET_INFO'][] = array(
				'attribute_id'=>$value['attribute_id'],
				'attribute_name'=>$value['attribute_name']
			);
		}
		foreach ($this->model_catalog_attribute->getAttributesByCode('PROMOTION') as $key => $value) {
			$attrs['PROMOTION'][] = array(
				'attribute_id'=>$value['attribute_id'],
				'attribute_name'=>$value['attribute_name']
			);
		}
		$json['data'] = $attrs;
		
	

		$json['status'] = 1;
		$json['message'] = 'Tải surveys thành công';
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->response->setOutput(json_encode($json));

	}
}
