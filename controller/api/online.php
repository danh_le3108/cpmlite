<?php
class ControllerApiOnline extends Controller {
	public function index($param=array()) {
		if(isset($param['user_id'])){
			$user_id = $param['user_id'];
			$user_info = $this->user->getUser($user_id);
			$username = $user_info['username'];
		}else{
			$user_id = $this->user->getId();
			$username = $this->user->getUserName();
		}
		// Whos Online
		if ($this->config->get('config_history_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}
			if (isset($this->request->server['HTTP_USER_AGENT'])) {
				$user_agent= $this->request->server['HTTP_USER_AGENT'];
			} else {
				$user_agent = '';
			}
				
			if($this->customer->isLogged()){
				$username = $this->customer->getUserName();
			}
			$this->model_tool_online->addOnline($ip,$user_id,$this->customer->getId(),$username, $url, $referer,$user_agent);
		}
	}
}
