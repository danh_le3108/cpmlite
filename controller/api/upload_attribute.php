<?php
class ControllerApiUploadAttribute extends Controller {
	public function index() {
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}
		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->log->write('Upload attribute: '.json_encode($this->request->post));
		$json = array(
			'api_name' => 'upload_attribute',
			'status' => 0,
			'message' => '',
			'data' => array()
		);	

		if($this->request->post['is_fix'] == 0) {

			$data = array(
				'post' => $this->request->post,
				'require' => array('api_key', 'user_id', 'plan_id', 'attribute_list')
			);
			$validation = $this->load->controller('api/validation', $data);
			if ($validation['status'] == 0) {
				$json['message'] = $validation['message'];
				return $this->response->setOutput(json_encode($json));
			}

			$post = $this->request->post;
			$this->load->model('plan/plan');
			$plan_id = $post['plan_id'];
			$user_id = $post['user_id'];
	 		$plan = $validation['plan'];
	 		$user = $validation['user'];
			$attrs = json_decode(html_entity_decode(str_replace(array('“','”'),array('"','"'),$post['attribute_list']), ENT_QUOTES, 'UTF-8'),true);
			
			$this->load->model('catalog/attribute_data');
			$old_data = $this->model_catalog_attribute_data->getAttrsOfPlan(array('plan_id'=>$plan_id),0);
			if (!empty($old_data)) {
				$this->model_catalog_attribute_data->delete(array('plan_id'=>$plan_id));
			}

			$datas = array();
			foreach ($attrs as $attr) {
				$data = array(
					'plan_id' => $plan_id,
					'round_name' => $plan['round_name'],
					'attribute_id' => $attr['attribute_id'],
					'value' => $attr['value']
				);
				$this->model_catalog_attribute_data->add($data);
			}

			$plan_update = array(
				'time_upload_attr'=>date('Y-m-d H:i:s')
			);
			$this->model_plan_plan->updatePlan($plan_id, $plan_update);
			
			if (!empty($old_data)) {
				$new_data = $this->model_catalog_attribute_data->getAttrsOfPlan(array('plan_id'=>$plan_id),0);
				$this->load->model('history/history');
				$history = array(
					'table_name' => 'plan',
					'user_id' => $user_id,
					'user_update' => $user['username'],
					'controller' => 'api/upload_attribute',
					'table_primary_id' => $plan_id,
					'data' => json_encode($new_data),
					'old_data' => json_encode($old_data)
				);
				
				$this->model_history_history->add($history);
			}

		}

		// p(1,1);
		$json['status'] = 1;
		$json['message'] = 'Success.';
		return $this->response->setOutput(json_encode($json));
	}
}
