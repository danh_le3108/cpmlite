<?php
class ControllerApiUploadImages extends Controller {
	public function index() {
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}
		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->log->write('Upload Image: ' . json_encode($this->request->post));
		$json = array(
			'api_name' => 'upload_images',
			'status' => 0,
			'message' => '',
			'data' => array()
		);	

		$data = array(
			'post' => $this->request->post,
			'require' => array('api_key', 'user_id', 'plan_id', 'image_type', 'sha1')
		);
		$validation = $this->load->controller('api/validation', $data);
		if ($validation['status'] == 0) {
			$json['message'] = $validation['message'];
			return $this->response->setOutput(json_encode($json));
		}

		$post = $this->request->post;
		$this->load->model('plan/plan');
		$plan_id = $post['plan_id'];
		$user_id = $post['user_id'];
 		$plan = $validation['plan'];	
 		$user = $validation['user'];	
		
		if (empty($this->request->files["image"])) {
			$json['message'] = "Không có file hình ảnh! (plan_id = {$plan_id})";
			return $this->response->setOutput(json_encode($json));
		}

		$config_project_folder =  $this->config->get('config_project_folder');
		$file_path = 'files/'.$config_project_folder.'/'.$plan['round_name'].'/'.$plan['store_code'].'/image/'.$plan_id.'/';
		if(!is_dir(DIR_MEDIA.$file_path)){
			@mkdir(DIR_MEDIA.$file_path,  0777, true);
			@touch(DIR_MEDIA.$file_path . 'index.html');
		}
		$filename = explode('.', $this->request->files["image"]['name']);
		if (!in_array($filename[1], array('jpg','png','gif','jpeg'))) {
			$json['message'] = $this->language->get('error_filetype');
			return $this->response->setOutput(json_encode($json));
		}
		$ext = $filename[1];
		$sha1 = !empty($this->request->post['sha1']) ? substr($this->request->post['sha1'],0,9) : token(3);
		$username = !empty($user) ? str_replace('.', '', $user['username']) : '';
		$new_name = $plan['store_code'].'-'.$plan_id.'-'.$username.'-'.$sha1.'.'.$ext;
		
		if (!file_exists(DIR_MEDIA.$file_path.$new_name)) {
			move_uploaded_file($this->request->files['image']['tmp_name'], DIR_MEDIA.$file_path.$new_name);
			$img = array(
				'filename' => $new_name,
				'plan_id' => $plan_id,
				'store_id' => $plan['store_id'],
				'user_id' => $user_id,
				'store_code' => $plan['store_code'],
				'sha1' => $sha1,
				'img_fix' => isset($post['is_fix']) ? $post['is_fix'] : 0,
				'is_diff' => isset($post['image_difference'])?$post['image_difference']:0,
				'is_checkin' => isset($post['is_checkin'])?$post['is_checkin']:0,
				'image_type_id' => $post['image_type'],
				'round_name' => $plan['round_name'],
				'user_created' => $user_id,
				'manual' => 0
			);
			$this->model_plan_plan->addPlanImage($plan_id, $img);

			if (empty($plan['store_image'])) {
				$this->load->model('store/store');
				$this->model_store_store->updateStore($plan['store_id'], array('store_image'=>$file_path.$new_name));
			}

			$plan_data = array(
				'capture_total' => $plan['capture_total']+1,
				'time_upload_image' => date('Y-m-d H:i:s')
			);
			if (empty($plan['image_overview']) && $post['image_type'] == $this->config->get('config_image_overview')) {
				$plan_data['image_overview'] = $file_path.$new_name;
			}
			if (empty($plan['image_selfie']) && $post['image_type'] == $this->config->get('config_image_selfie')) {
				$plan_data['image_selfie'] = $file_path.$new_name;
			}
			$this->model_plan_plan->updatePlan($plan_id, $plan_data);
		}
		
		$json['status'] = 1;
		$json['message'] = 'Success.';
		return $this->response->setOutput(json_encode($json));
	}
}