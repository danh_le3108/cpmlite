<?php
class ControllerApiGetTimeFull extends Controller {
	public function index() {
		$json = array();
		$json['api_name'] = 'get_time_full';
		
		$this->load->language('api/api');
			$json['data'] = date($this->language->get('datetime_format'), time());
			$json['status'] = 1;
			$json['message'] = 'Get full time success';
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));

	}
}
