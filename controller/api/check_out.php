<?php
class ControllerApiCheckOut extends Controller {
	public function index() {
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}
		$this->response->addHeader('Content-Type: application/json; charset=utf-8');

		$json = array(
			'api_name' => 'Checkout',
			'message' => '',
			'status' => 0,
			'data' => ''
		);

		$require = array(
			'api_key', 'user_id', 'plan_id', 'latitude', 'longitude'
		);
		$post = $this->request->post;
		$this->log->write("Checkout:".json_encode($post));
		foreach ($require as $key) {
			if (empty($post[$key])) {
				$json['message'] = 'Thiếu ' . $key;
				return $this->response->setOutput(json_encode($json));
			} elseif ($key == 'api_key' && $post['api_key'] != $this->config->get('config_api')) {
				$json['message'] = 'Sai ' . $key;
				return $this->response->setOutput(json_encode($json));
			} elseif ($key == 'user_id') {
				if (!is_numeric($post['user_id'])) {
					$json['message'] = 'Nhân viên không có trong dự án!';
					return $this->response->setOutput(json_encode($json));
				} 
				$this->load->model('project/project_user');
				$user = $this->model_project_project_user->getUser($post['user_id']);
				if (!$user || $user['user_status'] != 1) {
					$json['message'] = 'Nhân viên không có trong dự án!';
					return $this->response->setOutput(json_encode($json));
				}
			}
		}

 		$this->load->model('plan/plan');
		$user_id = $post['user_id'];
		$plan_id = $post['plan_id'];
		
 		$plan = $this->model_plan_plan->getPlans(array('plan_id'=>$plan_id));
 		if (!$plan) {
 			$json['message'] = "Không có cửa hàng này! (plan_id = {$plan_id})";
			return $this->response->setOutput(json_encode($json));
 		}
 		$plan = $plan[0];
 		// if ($plan['date'] != date('Y-m-d')) {
		// 	$json['message'] = "Đã hết hạn thực hiện CH này! (plan_id = {$plan_id})";
		// 	return $this->response->setOutput(json_encode($json));
		// }
 		if ($plan['time_checkout'] == '0000-00-00 00:00:00') {
 			$data = array(
	 			'checkout_latitude' => $post['latitude'],
	 			'checkout_longitude' => $post['longitude'],
	 			'time_checkout' => date('Y-m-d H:i:s')
	 		);
	 		$this->model_plan_plan->updatePlan($plan_id, $data);
 		}
 		
		$json['status'] = 1;
		$json['message'] = 'Success.';
		return $this->response->setOutput(json_encode($json));
	}
}
