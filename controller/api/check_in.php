<?php
class ControllerApiCheckIn extends Controller {
	public function index() {
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}
		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->log->write('Check in: ' . json_encode($this->request->post));
		$json = array(
			'api_name' => 'check_in',
			'status' => 0,
			'message' => '',
			'data' => array()
		);	
		if($this->request->post['is_fix'] == 0 || empty($this->request->post['is_fix'])) {
			$data = array(
				'post' => $this->request->post,
				'require' => array('api_key', 'user_id', 'plan_id','version','imei','model')
			);
			$validation = $this->load->controller('api/validation', $data);
			if ($validation['status'] == 0) {
				$json['message'] = $validation['message'];
				return $this->response->setOutput(json_encode($json));
			}
			
			$post = $this->request->post;
			$this->load->model('plan/plan');
			$plan_id = $post['plan_id'];
			$user_id = $post['user_id'];
			$plan = $validation['plan'];
			$user = $validation['user'];
			$test_ids = array(3519);
			if ($user['device_imei'] != $post['imei'] && !in_array($user_id, $test_ids)) {
				$json['message'] = "Thiết bị {$post['model']}({$post['imei']}) chưa được duyệt!";
				return $this->response->setOutput(json_encode($json));
			}

			$data = array();
			if($plan['plan_status'] == 0){
				$data = array(
					'latitude' => isset($post['latitude']) ? $post['latitude'] : '',
					'longitude' => isset($post['longitude']) ? $post['longitude'] : '',
					'imei' => isset($post['imei']) ? $post['imei'] : '',
					'model' => isset($post['model']) ? $post['model'] : '',
					'version' => isset($post['version']) ? $post['version'] : '',
					'time_checkin' => date('Y-m-d H:i:s')
				);
			}
			$data['plan_status'] = 2;
			$this->model_plan_plan->updatePlan($plan_id, $data);
			
			
			if (empty($plan['store_latitude']) ||$plan['is_verify']==0) {
				$this->load->model('store/store');
				$store = array(
					'store_latitude' => isset($post['latitude']) ? $post['latitude'] : '',
					'store_longitude' => isset($post['longitude']) ? $post['longitude'] : ''
				);
				$this->model_store_store->updateStore($plan['store_id'], $store);
			}
		}
		$json['status'] = 1;
		$json['message'] = 'Success.';
		return $this->response->setOutput(json_encode($json));
	}
}
