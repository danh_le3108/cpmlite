<?php
class ControllerApiGetStoreFields extends Controller {
	public function index() {
		$this->load->model('localisation/province');
		$province_list = $this->model_localisation_province->getProvinces();
		$provinces = array();
		foreach ($province_list as $value) {
			$provinces[] = array(
				'id' => $value['province_id'],
				'value' => $value['province_name']
			);
		}
		p($provinces,1);
		$json = array(
			'api_name' => 'get_store_fields',
			'data' => array(
				array(
					'name' => 'Tên cửa hiệu',
					'field' => 'store_name',
					'input' => 'text'
				),
				array(
					'name' => 'Người kinh doanh',
					'field' => 'store_owner',
					'input' => 'text'
				),
				array(
					'name' => 'Số điện thoại',
					'field' => 'store_phone',
					'input' => 'number'
				),
				array(
					'name' => 'Số nhà',
					'field' => 'store_address',
					'input' => 'text'
				),
				array(
					'name' => 'Đường/ Ấp',
					'field' => 'store_place',
					'input' => 'text'
				),
				array(
					'name' => 'Phường/ Xã',
					'field' => 'store_ward',
					'input' => 'text'
				),
				array(
					'name' => 'Quận/ Huyện',
					'field' => 'store_district',
					'input' => 'text'
				),
				array(
					'name' => 'Tỉnh/ Thành Phố',
					'field' => 'province_id',
					'input' => 'select',
					'option' => $provinces
				),
				array(
					'name' => 'Tỉnh/ Thành Phố',
					'field' => 'posm',
					'input' => 'select',
					'option' => array(
						array('id' => 1, 'value' => 'Khung Tranh Nhựa'),
						array('id' => 2, 'value' => 'Sticker Pushcart')
					)
				),
			),
 			'status' => 1,
			'message' => 'success'
		);
		
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->response->setOutput(json_encode($json));

	}
}