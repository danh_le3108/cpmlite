<?php

class ControllerPlanMap extends Controller {

	private $filter_key = array(
		'filter_date',
		'filter_global',
		'filter_province_id',
		'filter_round_name',
		'filter_user_id',
	);

	private function _url(){
		$url = '';
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}	
	public function index() {
		$languages= $this->load->language('plan/map');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}

		$filter_data['filter_plan_qc'] = 1;
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$data[$key] = $this->request->get[$key];
				$filter_data[$key] = trim($this->request->get[$key]);
			} else{
				$data[$key] = null;
			}
		}

		$this->load->model('plan/plan');
		$data['rounds'] = $this->model_plan_plan->countTotal('', 'round_name');
		// $this->load->model('localisation/region');
		// $data['regions'] =  $this->model_localisation_region->getRegions($filter_data);
		$this->load->model('localisation/province');
		$data['provinces'] = $this->model_localisation_province->getProvinces();
		$this->load->model('project/project_user');
		$data['staffs'] = $this->model_project_project_user->getProjectUsersByGroupID($this->config->get('config_user_staff'));
		$data['count_staff'] = $this->model_plan_plan->countTotal($filter_data, 'user_id');
		$plans = $this->model_plan_plan->getPlans($filter_data);
		$data['plan_total'] = count($plans);

		$this->document->setTitle($this->language->get('heading_title'));
		$data['latitude'] = '10.824725';
		$data['longitude'] = '106.640540';
		$data['http_server'] = HTTP_SERVER;
		$data['marker_url'] =  HTTP_SERVER . 'assets/image/marker/';
		$data['plan_edit'] = str_replace('&amp;','&',$this->url->link('plan/plan/edit', '&plan_id=' , true));
		if ($data['plan_total'] < 3000) {
			$this->load->model('tool/image');
			$store_image = $this->model_tool_image->best_fit('store.jpg', 120, 80);
			foreach ($plans as &$plan) {
				if (is_file(DIR_MEDIA.$plan['store_image'])) {
					$plan['store_image'] = $this->model_tool_image->best_fit($plan['store_image'], 120, 80);
				} else {
					$plan['store_image'] = $store_image;
				}
				unset($plan);
			}
		}
		$data['plans'] = $plans;
		$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');

		$this_template = 'plan/map';
		$this->response->setOutput($this->load->view($this_template, $data));
	}

	public function getStores(){

		$json = array();

	
		$filter_data = array();
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$filter_data[$key] = $this->request->get[$key];
			}
		}
		
		$filter_data['filter_qc_status'] = 1;
		
		
		if(!$this->user->isLogged()&&$this->customer->isLogged()){
			$filter_data['customers_manager'] = $this->customer->customers_manager();
		} 
		
		if (isset($this->request->get['filter_customer_parent_id'])) {
			$filter_data['customers_manager'] = $this->customer->getSupsByParentID($this->request->get['filter_customer_parent_id']);
		}
		
		
		
		if(!$this->user->isLogged()&&$this->customer->isLogged()){
			$filter_data['filter_area_codes'] = $this->customer->area_codes();
			$filter_data['filter_region_codes'] = $this->customer->region_codes();
			//$filter_data['filter_province_ids'] = $this->customer->province_ids();
		} 
		
		$json['latitude'] = '10.824725';
		$json['longitude'] = '106.640540';
		

		if (isset($this->request->get['filter_area_code'])) {
			$this->load->model('localisation/area');
			$area_info = $this->model_localisation_area->getAreaByCode($this->request->get['filter_area_code']);
			$json['latitude'] = $latitude = $area_info['latitude'];
			$json['longitude'] = $longitude = $area_info['longitude'];
		}
		if (isset($this->request->get['filter_region_code'])) {
			$this->load->model('localisation/region');
			$region_info = $this->model_localisation_region->getRegionByCode($this->request->get['filter_region_code']);
			$json['latitude'] = $latitude = $region_info['latitude'];
			$json['longitude'] = $longitude = $region_info['longitude'];
		}
		if (isset($this->request->get['filter_province_id'])) {
			$this->load->model('localisation/province');
			$province_info = $this->model_localisation_province->getProvince($this->request->get['filter_province_id']);
			$json['latitude'] = $latitude = $province_info['latitude'];
			$json['longitude'] = $longitude = $province_info['longitude'];
		}
		$count_rot = $count_dat = $count_ktc = 0;
		
		$this->load->model('plan/plan');
		$results = $this->model_plan_plan->getPlans($filter_data);
		
		$this->load->model('project/project_user');
		$data['staffs'] = $staffs = $this->model_project_project_user->getProjectUsersByGroupID($this->config->get('config_user_staff'),$filter_data);
		
		$this->load->model('tool/image');
		
		$this->load->model('survey/survey');
		$surveys = $this->model_survey_survey->getSurveyIndexBy('survey_id');
		
		$plan_surveys = $this->model_plan_plan->getPlansSurveys($filter_data);
		
		
		$stores= array();
		foreach ($results as $result) {
			$plan_id = $result['plan_id'];
			$survey_data = isset($plan_surveys[$plan_id])?$plan_surveys[$plan_id]:array();
	
				
			foreach ($survey_data as $sd) {
				if($sd['survey_id']!=1){
					$icon_marker = 'grey.png';
					if($sd['rating_manual']==1){
						$count_dat++;
						$icon_marker = 'green.png';
					}elseif($sd['rating_manual']==-1){
						$count_rot++;
						$icon_marker = 'grey.png';
					} elseif($sd['rating_manual']==-2){
						$count_ktc++;
						$icon_marker = 'red.png';
					} 
					$stores[$result['store_id']]['maker'] = $icon_marker;
				
					$sd['staff'] = isset($staffs[$result['user_id']])?$staffs[$result['user_id']]['fullname']:$result['username'];
					$stores[$result['store_id']]['plans'][$sd['plan_survey_id']] = $sd;
				}
			}
		}
		
		$json['stores'] = array();
		$total = 0;
		
		$http_server = HTTP_SERVER;
			
		
		foreach ($results as $result) {
			if(isset($stores[$result['store_id']]['plans'])){
				$total++;
				$plans  = $stores[$result['store_id']]['plans'];
				if(!empty($result['store_image'])&&file_exists(DIR_MEDIA.$result['store_image'])){
					$store_image = $this->model_tool_image->best_fit($result['store_image'], 120, 80);
				} else {
					$store_image = $this->model_tool_image->best_fit('store.jpg', 120, 80);
				}
				$store_image = str_replace($http_server,'',$store_image);
				$maker = 'grey.png';
				$json['stores'][] = array(
					'id'       => $result['store_id'],
					'marker'=>isset($stores[$result['store_id']]['maker'])?$stores[$result['store_id']]['maker']:$maker,
					'plans' => $plans,
					'img' => $store_image,
					'long'       => $result['store_longitude'],
					'lat'       => $result['store_latitude'],
					'name'       => addslashes($result['store_name']),
					'sup'       => $result['customer_user'],
					'code'       => $result['store_code'],
					'add'       => $result['store_address_raw']
				);
			}
		}
		 
		$json['count'] = array(
			'dat' => $count_dat,
			'rot' => $count_rot,
			'ktc' => $count_ktc,
		);
			
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}
}