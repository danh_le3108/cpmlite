<?php
class ControllerApiGetRounds extends Controller {
	public function index() {
		$json = array();
		$json['api_name'] = 'get_rounds';
		
		
		$this->load->language('api/api');
		
		$this->load->model('plan/round');
		$rounds = $this->model_plan_round->getRounds();
		

		$json['total'] = count($rounds);
		$json['data'] = $rounds;

		$json['status'] = 1;
		$json['message'] = $this->language->get('text_success');

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->response->setOutput(json_encode($json));

	}
}