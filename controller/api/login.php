<?php
class ControllerApiLogin extends Controller {
	private $field_list = array(
		'user_id',
		'username',
		'usercode',
		'fullname',
		'email',
		'telephone',
		'device_model',
		'status',
		'project_user_group'
	);

	public function index() {
		$json = array();
		$json['api_name'] = 'login';
		
		
		$this->log->write('Login: '.json_encode($this->request->post)); 
			
			
		$this->load->language('api/api');
		
		$json['data'] = array();
		if(!isset($this->request->post['api_key'])){
			$json['message'] = $this->language->get('text_error_key');
			$json['status'] = 0;
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}
		
		
		if(!isset($this->request->post['version'])||isset($this->request->post['version']) && !in_array($this->request->post['version'],$this->config->get('config_app_versions'))){
			$json['status'] = 0;
			$json['message'] = 'Bạn đang sử dụng app cũ. Vui lòng cập nhật app mới!';
			$this->response->addHeader('Content-Type: application/json; charset=utf-8');
			return $this->response->setOutput(json_encode($json));
		}
				
		
		$test_ids = array(3519,4884,3450);
		// Login with API Key
		if (isset($this->request->post['api_key'])&&trim($this->request->post['api_key']) ==$this->config->get('config_api')&&isset($this->request->post['username'])&&isset($this->request->post['password'])&&isset($this->request->post['imei'])) {
			$this->request->post['ip_address'] = $this->request->server['REMOTE_ADDR'];
			$json['postdata'] = json_encode($this->request->post);
			
			 
			
			
			$username = $this->request->post['username'];
			$password = $this->request->post['password'];
			$new_imei = $this->request->post['imei'];
			$this->request->post['version'] = isset($this->request->post['version'])?$this->request->post['version']:'';
			
			$new_model = isset($this->request->post['device_model'])?$this->request->post['device_model']:$new_imei;

			$this->load->model('localisation/province');
			$this->load->model('localisation/district');
			$this->load->model('localisation/ward');

			$this->load->model('project/project_user');
			$user_info = $this->model_project_project_user->getUserByUsername($username);
			if($user_info&&isset($user_info['user_status'])&&$user_info['user_status']==1){
				
				if($user_info['status']==$this->config->get('config_status_unactive')){
					$this->updateStatus($user_info['user_id']);
				}
				$this->user->logout();
				$login_status = $this->user->login($username, $password);
				$user_id = $json['logged'] = $this->user->isLogged();
				
				$param = array('user_id'=>$user_id);
				if (!empty($user_info)) {
					$this->load->controller('api/online',$param);
				}
				
				if($login_status ==1 && empty($user_info['device_imei'])){
					$this->addImei($user_id,$this->request->post);
				}
				if($login_status ==1 && (empty($user_info['device_imei'])||$user_info['device_imei']==$new_imei||in_array($user_id,$test_ids))&&$user_id>0){
					
					$this->updateImei($user_id,$this->request->post);
					$this->log->write('Login success: '.$json['postdata']);
					 
					foreach($this->field_list as $field){
						if(isset($user_info[$field])){
							$json['data'][$field] = $user_info[$field];
						}

					}
					$json['data']['user_group_id'] = $user_info['project_user_group'];
					$json['status'] = 1;
					$json['message'] = $this->language->get('text_success');
					$this->user->logout();
				}else if($login_status ==1 && !in_array($user_id,$test_ids)&&!empty($user_info['device_imei'])&&$user_info['device_imei']!=$new_imei){
			
					$this->updateImei($user_id,$this->request->post);
					$json['status'] = 0;
					$json['message'] = $this->language->get('Chào '.$user_info['usercode'] .' - Bạn đã đăng nhập trên 1 thiết bị ('.$user_info['device_model'].') trước đó. Thiết bị thứ 2 này ('.$new_model.') chưa được duyệt, vui lòng liên hệ Admin!');
				}else{
					$json['status'] = 0;
					$json['message'] = $this->language->get('Sai username hoặc mật khẩu!');
				}
			}else if($user_info&&isset($user_info['username'])&&isset($user_info['user_status'])&&$user_info['user_status']==0){
				$json['status'] = 0;
				$json['message'] = $this->language->get('Bạn không còn hoạt động trong dự án này!');
			}else{
				$json['status'] = 0;
				$json['message'] = $this->language->get('Bạn không tham gia dự án này!');
			}
		}else {
			$json['status'] = 0;
			$json['message'] = $this->language->get('text_error_key');
		}
		
		$json['config_checkin_id'] = $this->config->get('config_image_overview');

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	private function addImei($user_id,$data){
		$device_model = isset($data['device_model'])?$data['device_model']:$data['imei'];
		
		$this->db->query("UPDATE " . DB_PREFIX . "project_user SET 
		user_token = '".$this->db->escape(base64_encode(base64_encode($data['password'])))."',
		app_version = '".$this->db->escape($data['version'])."', 
		device_imei = '".$this->db->escape($data['imei'])."',
		device_model = '".$this->db->escape($device_model)."', 
		new_imei = '".$this->db->escape($data['imei'])."',
		new_model = '".$this->db->escape($device_model)."' WHERE user_id = '" . (int)$user_id. "' AND project_id ='" . (int)$this->config->get('config_project_id'). "'");
	}
	private function updateImei($user_id,$data){
		$device_model = isset($data['device_model'])?$data['device_model']:$data['imei']; 
		$google_token = isset($data['google_token'])?$data['google_token']:''; 
		$this->db->query("UPDATE " . DB_PREFIX . "project_user SET 
		google_token = '".$this->db->escape($google_token)."', 
		user_token = '".$this->db->escape(base64_encode(base64_encode($data['password'])))."', 
		app_version = '".$this->db->escape($data['version'])."', 
		new_imei = '".$this->db->escape($data['imei'])."',
		new_model = '".$this->db->escape($device_model)."' WHERE user_id = '" . (int)$user_id. "' AND project_id ='" . (int)$this->config->get('config_project_id'). "'");
	}
	private function updateStatus($user_id){
		$this->db->query("UPDATE " . DB_PREFIX . "user SET status = '".$this->config->get('config_status_start')."' WHERE user_id = '" . (int)$user_id. "'");
	}

}
