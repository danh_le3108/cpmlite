<?php
class ControllerApiProgress extends Controller{
	private $error = array();

	private $filter_key = array(
			'filter_global',
			'filter_user',
			'filter_plan_status',
			'filter_province_id',
			'filter_round',
			'filter_name',
			'filter_date'
		);

	private function _url(){
		$url = '';
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}
	public function index() {
		
			$note_fix = 1;
			$note_fixed = 2;
			
		//$this->log->write($this->request->get); 	
		$data['project_id'] = $this->config->get('config_project_id');
		
			
		$this->load->model('plan/progress');

		/*StaftUser*/
		$this->load->language('plan/progress');
		
		$url = $this->_url();
		
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$filter_data[$key] = trim($this->request->get[$key]);
			} else {
				$filter_data[$key] = NULL;
			}
		}

		$this->load->model('plan/progress');
		$this->load->model('project/project_user');
		
		
		
		$data['posts'] =  $this->request->post;
		
		$users =  $this->model_project_project_user->getProjectUsers($filter_data);
		
		$all_filter = array();
		if (isset($this->request->get['filter_round'])) {
			$all_filter['filter_round'] = trim($this->request->get['filter_round']);
		}else{
			$all_filter['filter_round'] = date('Y-m');
		}
		
		$this->load->model('plan/plan');
		$data['all_total'] = $all_total =  $this->getTotalPlans($all_filter);
		$all_filter['filter_plan_status'] = 1;
		$data['all_made'] = $all_made = $this->getTotalPlans($all_filter);
		$data['all_percent'] = ($all_total> 0) ? round(($all_made/$all_total)*100, 1) : 0;
		
		
		$filter_data['filter_plan_status'] = 1;
		$filter_total = $this->getTotalPlans($filter_data);
		
		$data['filter_made'] = $filter_made = $this->getTotalPlans($filter_data);
		
		
		$data['filter_percent'] = ($all_total> 0) ? round(($filter_made/$all_total)*100, 1) : 0;
		
		$problem_total = $this->model_plan_progress->getTotalProblemsGroupByUserId($filter_data);
		
		$note_total = $this->model_plan_progress->getTotalPlanNotesGroupByUserId($filter_data);
		
		$qc_total = $this->model_plan_progress->getTotalPlansQcGroupByUserId($filter_data);
		$plan_made_total = $this->model_plan_progress->getTotalPlansMadeGroupByUserId($filter_data);
		
		if(!isset($this->request->get['filter_date_start']) && !isset($this->request->get['filter_date_end'])){
			$plan_today = $this->model_plan_progress->getTotalPlansTodayGroupByUserId($filter_data);
		} else {
			$plan_today = $this->model_plan_progress->getTotalPlansMadeGroupByUserId($filter_data);
		}
		
		$qc_by_fl_total = $this->model_plan_progress->getTotalPlansQcByGroupId($this->config->get('config_user_fl'), $filter_data);
		$qc_by_sup_total = $this->model_plan_progress->getTotalPlansQcByGroupId($this->config->get('config_user_sup'), $filter_data);
		$qc_by_dc_total = $this->model_plan_progress->getTotalPlansQcByGroupId($this->config->get('config_user_dc'), $filter_data);
		$qc_by_pa_total = $this->model_plan_progress->getTotalPlansQcByGroupId($this->config->get('config_user_pa'), $filter_data);

		$reason = $this->model_plan_progress->getTotalPlansReasonGroupByUserId($filter_data);
		$data['staff'] = array();
		
		$plan_total    = 0;
				
		$date = isset($filter_date) ? $filter_date : date('Y-m-d');
		foreach($users as $user){
					if ($user['plan_total']>0) {
						$staff_qc        = isset($qc_total[$user['user_id']])?$qc_total[$user['user_id']]:0;
						$staff_plan_made = isset($plan_made_total[$user['user_id']])?$plan_made_total[$user['user_id']]:0;
						$staff_plan_rest = $user['plan_total']-$staff_plan_made;
						$staff_today     = isset($plan_today[$user['user_id']])?$plan_today[$user['user_id']]:0;
						
						$qc_by_fl = isset($qc_by_fl_total[$user['user_id']])?count($qc_by_fl_total[$user['user_id']]):0;
						$qc_by_sup = isset($qc_by_sup_total[$user['user_id']])?count($qc_by_sup_total[$user['user_id']]):0;
						$qc_by_dc = isset($qc_by_dc_total[$user['user_id']])?count($qc_by_dc_total[$user['user_id']]):0;
						$qc_by_pa = isset($qc_by_pa_total[$user['user_id']])?count($qc_by_pa_total[$user['user_id']]):0;
						
	
						$plan_total    += $user['plan_total'];
						
						
						/*Code*/ 
						$code_total  = 0;
						$code_percent  = 0;
						if(isset($problem_total[$user['user_id']])){
							$code_total  = $problem_total[$user['user_id']];
							$code_percent  = ($code_total>0&&$staff_plan_made>0) ? round(($code_total/$staff_plan_made)*100, 1) : 0;
						}
						
						/*Fix*/ 
						$fix_total  = 0;
						$fix_percent  = 0;
						if(isset($note_total[$user['user_id']])&&isset($note_total[$user['user_id']][1])){
							$fix_total  = $note_total[$user['user_id']][1];
							$fix_percent  = ($fix_total>0&&$staff_plan_made>0) ? round(($fix_total/$staff_plan_made)*100, 1) : 0;
						}
	
						
						$tc = isset($reason[$user['user_id']])?$reason[$user['user_id']]['tc']:0;
						$ktc = isset($reason[$user['user_id']])?$reason[$user['user_id']]['ktc']:0;
						
	
						$data['staff'][$user['user_id']] = array(
							'user_id'            => $user['user_id'],
							'fullname'           => $user['fullname'],
							'usercode'           => $user['usercode'],
							'telephone'          => $user['telephone'],
							'user_parent_id'     => $user['user_parent_id'],
							'user_group_id' => $user['project_user_group'],
							'user_status'        => $user['user_status'],
							'plan_total'         => $user['plan_total'],
							'plan_rest'         => $staff_plan_rest,
							'plan_made'    => $staff_plan_made,
							'plan_qc'           => $staff_qc,
							'plan_today'        => $staff_today,
							'code_total'        => $code_total,
							'code_percent'        => $code_percent,
							'fix_total'        => $fix_total,
							'fix_percent'        => $fix_percent,
							'percent'            => ($user['plan_total']!=0) ? round(($staff_plan_made/$user['plan_total'])*100, 1) : 0,
							'href'               => $this->url->link('plan/plan','filter_date='.$date.'&filter_plan_status=1&filter_user='.$user['user_id']),
							'href2'               => $this->url->link('plan/plan',$url.'&filter_plan_status=1&filter_user='.$user['user_id']),
							'qc_by_fl'          => $qc_by_fl,
							'qc_by_sup'          => $qc_by_sup,
							'qc_by_dc'           => $qc_by_dc,
							'qc_by_pa'           => $qc_by_pa,
							'tc'                 => $tc,
							'ktc'                => $ktc
						);
					}
		}
		
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}
		//$this->response->setOutput($this->load->view('common/home', $data));
		
		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		return $this->response->setOutput(json_encode($data)); 
	}
 	public function getTotalPlansQcByGroupIdAndUserID($data = array()) {
	  $results = array();
	  $sql = "SELECT pl.user_id, qc.date_added, qc.user_group_id, qc.user_id AS qc_user_id 
	  FROM `" . PDB_PREFIX . "plan_confirm` qc INNER JOIN `".PDB_PREFIX."plan` pl ON(qc.plan_id=pl.plan_id) 
	  WHERE `plan_status` = '1' AND pl.is_deleted = '0'";

	  if (!empty($data['filter_date'])&& !is_null($data['filter_date'])) {
			$sql .= " AND DATE(`time_upload`) <= DATE('" . $data['filter_date'] . "')";
		}

		if ((!empty($data['filter_date_start'])&& !is_null($data['filter_date_start'])) && (!empty($data['filter_date_end'])&& !is_null($data['filter_date_end']))) {
			$sql .= " AND DATE(pl.`time_upload`) BETWEEN '" . $data['filter_date_start'] . "' AND '". $data['filter_date_end'] ."'";
		}

		if(isset($data['filter_round']) && !empty($data['filter_round'])){
			$sql .= " AND pl.`round_name` = '" . $this->pdb->escape($data['filter_round']) . "'";
		}

	  $query = $this->pdb->query($sql);
	  if($query->rows){
	  	foreach($query->rows as $row){
 				$results[$row['user_group_id']][$row['user_id']][] = array('date_added' => $row['date_added']);
 			}
	  }

	  return $results;
 	}
	public function getTotalPlans($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "plan pl LEFT JOIN " . PDB_PREFIX . "store s ON (s.store_id = pl.store_id)";
		
	
		$sql .= " WHERE pl.is_deleted = '0'";
		//Plan không KP or Đã xác nhận KP
		if($this->customer->isLogged()){
			$sql .= " AND pl.`is_fix` IN (0,3)";
		}
		
		//	$sql .= " AND pl.user_id != '3519'";
		$filter_global = array(
			's.store_name',
			's.store_owner',
			's.store_code',
			's.distributor',
			's.store_customer_code',
			's.store_ad_code',
			'pl.plan_id',
			'pl.user_id',
			'pl.usercode',
			'pl.plan_name',
			'pl.round_name'

		);

		if (!empty($data['filter_global'])) {
			$sql .= " AND (";
			foreach ($filter_global as $filter) {
				$gimplode[] = " LCASE(".$filter.") LIKE '%" . $this->pdb->escape(utf8_strtolower($data['filter_global'])) . "%'";
			}
			$sql .= " " . implode(" OR ", $gimplode) . "";
			$sql .= ")";
		}
			/*Filter start*/
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "pl.plan_name LIKE '%" . $this->pdb->escape($data['filter_name']) . "%'";
		}
		if (!empty($data['filter_round'])) {
		   $implode[] = "pl.round_name = '" . $this->pdb->escape($data['filter_round']). "'";
		}
		if (!empty($data['filter_outlet_code'])) {
			$implode[] = "pl.outlet_code = '" .$this->pdb->escape($data['filter_outlet_code']) . "'";
		}
		if (!empty($data['filter_region_codes'])){
			$sql .= " AND s.region_code IN (" .$data['filter_region_codes'] . ")";
		}
		if (!empty($data['filter_region_code'])) {
			$implode[] = "s.region_code = '" .$this->pdb->escape($data['filter_region_code']) . "'";
		}
		/*if (!empty($data['filter_province_ids'])) {
			//$implode[] = "s.province_id IN (" . $this->db->escape($data['filter_province_ids']) . ")";
		}*/
		if (!empty($data['filter_province_id'])) {
			$implode[] = "s.province_id = '" . (int)$data['filter_province_id'] . "'";
		}
		if (!empty($data['filter_distributor_id'])) {
			$implode[] = "s.distributor_id = '" .(int)$data['filter_distributor_id'] . "'";
		}
		if (isset($data['filter_distributor_ids'])&&!empty($data['filter_distributor_ids'])) {
			$implode[] = "s.distributor_id IN('".implode("','",$data['filter_distributor_ids'])."')";
		}
		if (isset($data['filter_rating_status'])&& !is_null($data['filter_rating_status'])) {
			$implode[] = "pl.plan_rating = '" . (int)$data['filter_rating_status'] . "'";
		}
		if (isset($data['filter_plan_status'])&& !is_null($data['filter_plan_status'])) {
			$implode[] = "pl.plan_status = '" . (int)$data['filter_plan_status'] . "'";
		}
		if (isset($data['filter_is_fix'])&&!empty($data['filter_is_fix'])){
				$sql .= " AND pl.is_fix = '" . (int)$data['filter_is_fix'] . "'";
		}
		if (isset($data['filter_qc_status'])&& !is_null($data['filter_qc_status'])) {
			$implode[] = "pl.plan_qc = '" . (int)$data['filter_qc_status'] . "'";
		}
		if (isset($data['filter_user_ids'])&&!empty($data['filter_user_ids'])) {
			$implode[] = "pl.user_id IN('".implode("','",$data['filter_user_ids'])."')";
		}
		if (isset($data['filter_user'])&&!empty($data['filter_user'])) {
			$implode[] = "pl.user_id = '" . (int)$data['filter_user'] . "'";
		}
		
		if (!empty($data['filter_customer_user_id'])) {
			$implode[] = "s.customer_user_id = '" . (int)$data['filter_customer_user_id'] . "'";
		}
		if (isset($data['filter_customer_user_ids'])&&!empty($data['filter_customer_user_ids'])) {
			$implode[] = "s.customer_user_id IN('".implode("','",$data['filter_customer_user_ids'])."')";
		}
		if (!empty($data['filter_date_checkin'])) {
		   $implode[] = "pl.time_checkin LIKE '" . $data['filter_date_checkin'] . "%'";
			$sql .= " AND pl.time_checkin !='0000-00-00 00:00:00'";
		}
		if (!empty($data['filter_date'])) {
		   $implode[] = "pl.time_upload LIKE '" . $data['filter_date'] . "%'";
		}
		if (!empty($data['filter_upload_count'])) {
		   $implode[] = "pl.upload_count = '" . $data['filter_upload_count'] . "'";
		}
		if (!empty($data['filter_version'])) {
		   $implode[] = "pl.version = '" . $data['filter_version'] . "'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
		
		if (isset($data['customers_manager'])&& is_array($data['customers_manager'])) {
			$sql .= " AND pl.`customer_user_id` IN('".implode("','",$data['customers_manager'])."')";
		} 
		
			/*Filter end*/
		$query = $this->pdb->query($sql);
		return $query->row['total'];
	}
}