<?php
class ControllerApiUploadOrder extends Controller {
	public function index() {
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}
		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		if(isset($this->request->post['order_code'])){
			// $this->log->write('Upload-order: Plan #'. $this->request->post['plan_id'].' -Order: #'. $this->request->post['order_code'].' -User: #'. $this->request->post['user_id']);
			$this->log->write('Upload-order: '. json_encode($this->request->post));
		}
		
		$json = array(
			'api_name' => 'upload_order',
			'message' => '',
			'status' => 0,
			'data' => ''
		);

		$require = array(
			'api_key', 'user_id', 'plan_id', 'attribute_list', 'time_order'
		);
		$post = $this->request->post;
		
		foreach ($require as $key) {
			if (empty($post[$key])) {
				$json['message'] = 'Thiếu ' . $key;
				return $this->response->setOutput(json_encode($json));
			} elseif ($key == 'api_key' && $post['api_key'] != $this->config->get('config_api')) {
				$json['message'] = 'Sai ' . $key;
				return $this->response->setOutput(json_encode($json));
			} elseif ($key == 'user_id') {
				if (!is_numeric($post['user_id'])) {
					$json['message'] = 'Nhân viên không có trong dự án!';
					return $this->response->setOutput(json_encode($json));
				} 
				$this->load->model('project/project_user');
				$user = $this->model_project_project_user->getUser($post['user_id']);
				if (!$user || $user['user_status'] != 1) {
					$json['message'] = 'Nhân viên không có trong dự án!';
					return $this->response->setOutput(json_encode($json));
				}
			}
		}

 		$this->load->model('plan/plan');
		$user_id = $post['user_id'];
		$plan_id = $post['plan_id'];
		
 		$plan = $this->model_plan_plan->getPlans(array('plan_id'=>$plan_id));
 		if (!$plan) {
 			$json['message'] = "Không có cửa hàng này! (plan_id = {$plan_id})";
			return $this->response->setOutput(json_encode($json));
 		}
 		$plan = $plan[0];
 		if ($plan['date'] != date('Y-m-d')) {
			$json['message'] = "Đã hết hạn thực hiện CH này! (plan_id = {$plan_id})";
			return $this->response->setOutput(json_encode($json));
		}
		if ($plan['plan_status'] == 1) {
			$json['message'] = "Ca làm đã checkout, vui lòng liên hệ admin! (plan_id = {$plan_id})";
			return $this->response->setOutput(json_encode($json));
		}
		if ($plan['plan_status'] == 0) {
			$json['message'] = "Ca làm chưa mở, vui lòng liên hệ SUP checkin! (plan_id = {$plan_id})";
			return $this->response->setOutput(json_encode($json));
		}
		
		$attr_data = json_decode(html_entity_decode(str_replace(array('“','”'),array('"','"'),$post['attribute_list']), ENT_QUOTES, 'UTF-8'),true);
 		// $this->log->write($attr_data);
 		// ADD ORDER
 		$filter = array(
 			'plan_id' => $plan_id,
 			'user_id' => $user_id,
 			'order_code' => $post['order_code']
 		);
 		$orders = $this->model_plan_plan->getPlanOrder($filter);
 		
 		if (!empty($orders)) {
 			$old_data = $order = $orders[0];
 			$order_id = $order['order_id'];
 		} else {
 			$order = array(
				'order_code' => $post['order_code'],
				'user_id' => $user_id,
				'plan_id' => $plan_id,
				'date' => $plan['date'],
				'time_order' => date("Y-m-d H:i:s",$post['time_order'])
			);
			$order_id = $this->model_plan_plan->addOrder($order);
 		}
 		$sql = "UPDATE cpm_plan_images SET order_id = " . $order_id . " WHERE plan_id = " . $plan_id . " AND order_code = '" . $this->pdb->escape($order['order_code']) . "'";
		$this->pdb->query($sql);
		
		if (!empty($attr_data['info'])) { 
			$info = $attr_data['info'];
			foreach ($info as $value) {
				$order[$value['attribute_id']] =  $value['value'];
			}
		}
		$this->model_plan_plan->updateOrder($order_id,$order);
		// ADD ORDER DETAIL
		if (!empty($attr_data['customer'])) { 
			$customers = $attr_data['customer'];
			$order_detail = array(
				'order_id' => $order_id,
				'plan_id' => $plan_id
			);
			foreach ($customers as $customer) {
				$images = array();
				foreach ($customer as $value) {
					if ($value['attribute_id'] != 'image') {
						$order_detail[$value['attribute_id']] =  $value['value'];
					} else {
						$images = $value['value'];
					}
				}

				$detail = $this->checkOrderDetail($order_detail);
				if (!empty($detail)) {
					$detail_id = $detail['detail_id'];
					$this->model_plan_plan->updateOrderDetail($detail_id, $order_detail);
				} else {
					$detail_id = $this->model_plan_plan->addOrderDetail($order_detail);
				}
				
				if (!empty($images)) {
					foreach ($images as $img) {
						$image_data = array( 
							'order_id' => $order_id,
							'detail_id' => $detail_id,
							'plan_id' => $plan_id,
							'user_id' => $user_id
						);
						$image = $this->model_plan_plan->getImageByAppFileName($img);
						if (!empty($image)) {
							$this->model_plan_plan->updateImage($image['image_id'], $image_data);
						} else {
							$image_data['app_file_name'] = $img;
							// $this->log->write($image_data);
							// exit();
							
							$image_id = $this->model_plan_plan->addPlanImage($image_data);
						}
					}
				}
			}
		}

		if (!empty($orders)) {
			$this->load->model('history/history');
			$history = array(
				'table_name' => 'plan',
				'user_id' => $user_id,
				'user_update' => $user['username'],
				'controller' => 'api/upload_order',
				'table_primary_id' => $order_id,
				'data' => json_encode($order),
				'old_data' => json_encode($old_data)
			);
			$this->model_history_history->add($history);
		}
		
 		$json['data'] = array();
		$json['status'] = 1;
		$json['message'] = 'Success.';
		
 		$this->logOrder($this->request->post);
		
		
		return $this->response->setOutput(json_encode($json));
	}

	public function logOrder($data) {
		
		$user_id = isset($data['user_id'])?$data['user_id']:0; 
		$plan_id = isset($data['plan_id'])?$data['plan_id']:0; 
		$order_code = isset($data['order_code'])?$data['order_code']:0; 
		$api_name = 'upload_order'; 
		
		$sql = "INSERT INTO " . PDB_PREFIX . "alog_order SET 
		plan_id = '".(int)$plan_id."', 
		user_id = '".(int)$user_id."', 
		order_code = '".$this->pdb->escape($order_code)."', 
		api_name = '".$this->pdb->escape($api_name)."', 
		post_data = '".$this->pdb->escape(json_encode($data))."', 
		date_added = NOW()";
		//$this->log->write($sql); 
		$this->pdb->query($sql);
	}

	public function checkOrderDetail($data) {
		$sql = "SELECT * FROM cpm_order_detail WHERE 1";
		foreach ($data as $key => $value) {
			$sql .= " AND {$key} = '{$value}'";
		}
		return $this->pdb->query($sql)->row;
	}
}
