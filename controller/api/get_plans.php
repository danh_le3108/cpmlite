<?php
class ControllerApiGetPlans extends Controller {
	public function index() {
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}
		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->log->write('Get Plans: ' . json_encode($this->request->post));
		$json = array(
			'api_name' => 'get_plans',
			'status' => 0,
			'message' => '',
			'data' => array()
		);	

		$data = array(
			'post' => $this->request->post,
			'require' => array('api_key', 'user_id')
		);
		$validation = $this->load->controller('api/validation', $data);
		if ($validation['status'] == 0) {
			$json['message'] = $validation['message'];
			return $this->response->setOutput(json_encode($json));
		}
		$user_id = $validation['user']['user_id'];
		
		$this->load->model('tool/image');
		$img = $this->model_tool_image->best_fit('store.jpg', 300,300);
		$this->load->model('store/store_type');
		$store_types = $this->model_store_store_type->getAllTypes();
		$this->load->model('catalog/group');
		$groups = $this->model_catalog_group->getGroups(array('parent_group_id'=>0));
		$group_mapping = array();
		foreach ($groups as $value) {
			$group_mapping[$value['group_id']] = $value['group_name'];
		}
		
		$this->load->model('plan/plan');
		$filter = array(
			'user_id' => $user_id,
			'date_end >' => date('Y-m-d'),
		);
		$plans = $this->model_plan_plan->getPlans($filter);
		$plan_data = array();
		foreach ($plans as $plan) {
			$project_name = !empty($group_mapping[$plan['group_id']]) ? $group_mapping[$plan['group_id']] : '';
			if ($plan['posm_milo_t4_can']) {
				$project_name .= ' - Poster Milo Can';
			} 
			if ($plan['posm_milo_t4_sugar']) {
				$project_name .= ' - Poster Less Surgar';
			} 
			if ($plan['posm_milo_t4_wobler']) {
				$project_name .= ' - Wobler Milo can';
			} 
			if ($plan['posm_milo_t4_signboad']) {
				$project_name .= ' - Signalboard';
			} 
			$plan_data[] = array(
				'plan_id' => $plan['plan_id'],
				'is_fix' => $plan['is_fix'] == 1 ? 1 : 0,
				'fix_notes' => $plan['is_fix'] == 1 ? $this->model_plan_plan->getPlanNotes(array('plan_id'=>$plan['plan_id'],'note_id'=>1,'is_fixed'=>0)) : [],
				'round_name' => $plan['round_name'],
				'plan_name' => $plan['plan_name'],
				'plan_status' => $plan['plan_status'] == 1 ? 1 : 0,
				'plan_rating' => $plan['plan_rating'],
				'upload_count' => $plan['upload_count'],
				'date_start' => $plan['date_start'],
				'date_end' => $plan['date_end'],
				'user_id' => $plan['user_id'],
				'import_id' => $plan['import_id'],
				'store_id' => $plan['store_id'],
				'store_code' => $plan['store_code'],
				'store_name' => $plan['store_name'],
				// 'pepsi_code' => $plan['pepsi_code'],
				'store_type_id' => $plan['store_type_id'], 
				'store_type' => isset($store_types[$plan['store_type_id']]) ? $store_types[$plan['store_type_id']]['type_name'] : '',
				'store_owner' => $plan['store_owner'],
				'store_address' => $plan['store_address'],
				'store_place' => $plan['store_place'],
				'store_ward' => $plan['store_ward'],
				'store_district' => $plan['store_district'],
				'store_province' => $plan['store_province'],
				'province_id' => $plan['province_id'],
				'region_code' => $plan['region_code'],
				'store_address_raw' => $plan['store_address_raw'],
				'store_phone' => $plan['store_phone'],
				'latitude' => $plan['store_latitude'],
				'longitude' => $plan['store_longitude'],
				'store_latitude' => $plan['store_latitude'],
				'store_longitude' => $plan['store_longitude'],
				'store_image' => !empty($plan['store_image'])&&file_exists(DIR_MEDIA.$plan['store_image']) ? $this->model_tool_image->best_fit($plan['store_image'],300,300) : $img,
				'group_id' => $plan['group_id'],
				'project_name' => $project_name,
				'distributor' => $plan['distributor'],
				'note_admin' => $plan['note_admin']
			);
		}	

		$json['data'] = $plan_data;
		$json['status'] = 1;
		$json['message'] = 'Success.';
		$json['total'] = count($json['data']);
		return $this->response->setOutput(json_encode($json));
	}	

}
