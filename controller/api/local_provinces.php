<?php
class ControllerApiLocalProvinces extends Controller {
	private $province_fields = array(
	'province_id',
	'prefix',
	'name',
	'province_name',
	'province_alias',
	'latitude',
	'longitude'
	);
	public function index() {
		$json = array();
		$json['api_name'] = 'local_provinces';
		
		
		$this->load->language('api/api');

		$this->load->model('localisation/province');
		$province_data = array();
		$json['status'] = 1;
		$json['message'] = $this->language->get('text_success');
		$provinces =  $this->model_localisation_province->getProvinces();
		foreach($provinces as $province){
			$province_info = array();
			foreach($this->province_fields as $field){
				if($province[$field]){
					$province_info[$field] = $province[$field];
				}
			}
			$province_info['province_full'] = $province['prefix'].' '.$province['name'];
			$json['data'][] = $province_info;
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
