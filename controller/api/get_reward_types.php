<?php
class ControllerApigetRewardTypes extends Controller {
	public function index() {
		$json = array();
		$json['api_name'] = 'get_reward_types';
		
		
		$this->load->model('catalog/reward_type');
		$reward_types_data = array();
		$reward_types = $this->model_catalog_reward_type->getRewardTypes();
		foreach($reward_types as $reward_type){
			$reward_types_data[] = $reward_type;
		}
		$json['data'] =$reward_types_data;
		
		$json['status'] = 1;
		$json['message'] = 'Load Reward Types Success';

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json; charset=utf-8');
		$this->response->setOutput(json_encode($json));

	}
}
