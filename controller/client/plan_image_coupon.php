<?php
class ControllerClientPlanImageCoupon extends Controller {
	public function index() {
		if (isset($this->request->get['plan_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$languages= $this->load->language('plan/plan');
			foreach($languages as $key=>$value){
					$data[$key] = $value;
			}
			$data['plan_id'] = $plan_id = $this->request->get['plan_id'];

			$this->load->model('plan/plan');
			$plan_info = $this->model_plan_plan->getPlanInfo($plan_id);
			$data['plan_info'] = $plan_info;
		/*Status History*/

		$this->load->model('localisation/image_type');
		$data['image_types'] = $this->model_localisation_image_type->getAllTypes();


		$data['route_image'] = 'image_coupon';
		$data['config_image'] = $this->config->get('config_image_coupon');
		$data['image_ignore'] = $this->config->get('config_image_ignore');


		$data['images'] = array();


		$results = $this->model_plan_plan->getPlanImages($this->request->get['plan_id']);

		$this->load->model('tool/image');
		
		foreach ($results as $image) {
				$filename = $plan_info['image_path']. $image['filename'];
			if (file_exists(DIR_MEDIA.$filename)) {

				$img_info = getimagesize(DIR_MEDIA.$filename);
				if(isset($img_info[0])){
	
					$popup = HTTP_SERVER.'media/'.$filename;
	
					$thumb = $this->model_tool_image->best_fit($plan_info['image_path']. $image['filename'], 250,250);
	
					$data['images'][] = array(
						'image_id'=>$image['image_id'],
						'filename'=>$image['filename'],
						'popup'=>$popup,
						'thumb'=>$thumb,
						'type'=>($image['manual']==1)?$this->language->get('text_upload_manual'):$this->language->get('text_upload_app'),
						'image_type_id'=>$image['image_type_id']
					);
				}
			}
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->session->data['error_warning'])) {
			$data['error_warning'] = $this->session->data['error_warning'];

			unset($this->session->data['error_warning']);
		} else {
			$data['error_warning'] = '';
		}

		/*Status History*/
		$data['heading_title'] = $this->language->get('heading_image_coupon');
		
		$data['config_customer_top'] = $config_top = $this->config->get('config_customer_top');
		
		if($this->user->isLogged()||$this->customer->getLevel()==$data['config_customer_top']){
			$data['isNationwide'] = $isNationwide = 1;
		}else{
			$data['isNationwide'] = $isNationwide =0;
		}
			$data['user_qa'] = $this->config->get('config_user_qa');
			$data['group_id'] = $this->user->getGroupId();
			$data['user_id'] = $this->user->getId();
			
			$template = 'client/plan_image';
			return $this->load->view($template, $data);
		}
	}
	public function info() {
		$this->response->setOutput($this->index());
	}
}