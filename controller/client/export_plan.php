<?php

class ControllerClientExportPlan extends Controller {
	private $export_id = 6;
	private $round;

	private $filter_key = array(
		'filter_round',
		'filter_global',
		'filter_user',
		'filter_province_id',
		'filter_qc_status',
		'filter_region_code',
		'filter_area_code',
		'filter_date',
		'filter_rsm',
		'filter_asm',
		'filter_survey_group',
		'filter_rating_status',
		'filter_customer_user_id'
	);


	private function _url(){
		$url = '';
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}	
	public function index($filter_data=array()){

		if (($this->document->hasPermission('access', 'client/export_plan')&&$this->customer->isLogged()) || $this->user->isLogged()) {
				$languages= $this->load->language('excel/template');

				foreach($languages as $key=>$value){
						$data[$key] = $value;
				}

				$url = '';

				foreach($this->filter_key as $key){
					if (!empty($filter_data[$key])) {
						$url .= '&'.$key.'=' . urlencode(html_entity_decode($filter_data[$key], ENT_QUOTES, 'UTF-8'));
					}
				}

				/* Requied */
				$data['token'] ='';

				$data['folder'] = 'plan';

				$data['template_id'] = 0;//$this->config->get('config_import_user');

				$data['export'] = $this->url->link('client/export_plan/export',  $url , true);

			$this->load->model('plan/round');
			$data['rounds'] =  $this->model_plan_round->getRounds();
			
			foreach($this->filter_key as $key){
				if (isset($filter_data[$key])) {
					$data[$key] = $filter_data[$key];
				}else{
					$data[$key] = NULL;
				}
			}
			
				$template = 'excel/audit_export';

				return $this->load->view($template, $data);
			}
	}

	public function export() {

		$this->load->language('plan/plan');
		$this->load->model('excel/handle');
		$this->load->model('plan/plan');

		$filter_data = array();
		
		$filter_data['filter_plan_status'] = 1;
		$filter_data['filter_qc_status'] = 1;

		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$filter_data[$key] = $this->request->get[$key];
			}
		}
		
		$config_top = $this->config->get('config_customer_top');
		
		if($this->user->isLogged()||$this->customer->getLevel()==$config_top){
			$data['isNationwide'] = $isNationwide = 1;
		}else{
			$data['isNationwide'] = $isNationwide =0;
		}
		if(!$this->user->isLogged()&&$this->customer->isLogged()){
			$filter_data['customers_manager'] = $customers_managers = $this->customer->customers_manager(); 
		}
		
		if (isset($this->request->get['filter_rsm'])) {
			$filter_rsm = $this->customer->getSupsByParentID($this->request->get['filter_rsm']);
			if($isNationwide==1){
				$filter_data['customers_manager'] = $filter_rsm;
			}else{
				$filter_data['customers_manager'] = array();
				foreach ($filter_rsm as $rsm){
					if(in_array($rsm,$customers_managers)){
				$filter_data['customers_manager'][] = $rsm;		
					}
				}
			}
		} 
		if (isset($this->request->get['filter_asm'])) {
			$filter_asm = $this->customer->getSupsByParentID($this->request->get['filter_asm']);
			if($isNationwide==1){
				$filter_data['customers_manager'] = $filter_asm;
			}else{
				$filter_data['customers_manager'] = array();
				foreach ($filter_asm as $asm){
					if(in_array($asm,$customers_managers)){
				$filter_data['customers_manager'][] = $asm;		
					}
				}
			}
		} 
		/*End filter*/ 
		
		
		$template_id = $this->export_id;//6


					$starttime = explode(' ',microtime());
					$start_time =  $starttime['1'] + $starttime['0'];
			$results = $this->model_plan_plan->getPlans($filter_data);
			
					/**/
					$endtime                     = microtime();
					$endtime                     = explode(' ', $endtime);
					$endtime                     = $endtime['1'] + $endtime['0'];
				$loadingtime = number_format($endtime - $start_time, 4);
				
			$this->log->write('Customer Export: #'.$this->user->isLogged().' #'.$this->user->getUserName().' GetPlans: '.$loadingtime); 
			
		
			//Lý do KTC
			$this->load->model('localisation/reason_unsuccess');
			$reason_unsuccess = $this->model_localisation_reason_unsuccess->getReasons(1);
			
			
			//Lý do Không đạt
			$this->load->model('localisation/reason_not_pass');
			$reason_not_pass = $this->model_localisation_reason_not_pass->getReasons(1);
	
			$config_reward_audit = $this->config->get('config_reward_audit');
			$config_reward_sales = $this->config->get('config_reward_sales');
			$config_reward_old = $this->config->get('config_reward_old');
			
			//User group id
			$pa_id = $this->config->get('config_user_pa');
			$dc_id = $this->config->get('config_user_dc');
			$sup_id = $this->config->get('config_user_sup');
			$pl_id = $this->config->get('config_user_pl');
			$qc_id = $this->config->get('config_user_qc');
			
			$coupons = $this->getThisPlanCoupons($filter_data,$isNationwide);
			
			//Tất cả câu trả lời
			$survey_answer = $this->getThisSurveys($filter_data);
			
			$survey_audit = $this->config->get('config_survey_audit');
			$survey_adhoc = $this->config->get('config_survey_adhoc');
			
			$rating_result = array(
				1=>'Đạt',
				0=>'',
				-1=>'Không đạt',
				-2=>'KTC',
			);
			
			$this->load->model('survey/survey');
			$surveys = $this->model_survey_survey->getSurveyIndexBy('survey_id');
			
			$plan_surveys = $this->model_plan_plan->getPlansSurveys($filter_data);
			
			$s_arr = array(2,3,4,5,6,7,8,9,10,11);
			$adhoc_arr = array(12,13,14,15);
			
			$qc_users = $this->getAllUserQc($filter_data);
			
			$this->load->model('project/project_user');
			$user = $this->model_project_project_user->getProjectUsers();
		
			$qc_note = $this->getQcNote($filter_data);
		
			$i=1;
			
			$plans = array();
				
			foreach($results as $plan){
					$plan_id = $plan['plan_id'];
					$answer = isset($survey_answer[$plan_id])?$survey_answer[$plan_id]:array();
					
					$plan_survey = isset($plan_surveys[$plan_id])?$plan_surveys[$plan_id]:array();
					$plan_reward_audit = isset($coupons[$plan_id][$config_reward_audit])?$coupons[$plan_id][$config_reward_audit]:array();
					$plan_reward_old = isset($coupons[$plan_id][$config_reward_old])?$coupons[$plan_id][$config_reward_old]:array();
					
			
					$plan_reason = '';
					if(isset($reason_unsuccess[$plan['reason_id']])) {
						$plan_reason .= $reason_unsuccess[$plan['reason_id']]['reason_name'];
						if(!empty($plan['note_ktc'])){
							$plan_reason .= ' - '.$plan['note_ktc'];
						}
					}
	
			
			
					$survey_audit_name = '';
					$plan_survey_audit = '';
					
					$audit_reason = '';
					
					
					
					$adhoc_result = array();
					$adhoc_reason = array();
					
					
					$survey_total=0;
					
					if($plan['reason_id']>0){
						$plan_survey_audit[] = $rating_result[-2];
					}
					//$reason_unsuccess KTC => ktc_reason_id
					//$reason_not_pass  Không đạt => not_pass_id
				
					foreach($plan_survey as $sid => $sdata){
						if(isset($sdata['survey_id'])){
							
							if(in_array($sdata['survey_id'],$survey_audit)){
								$survey_audit_name = 	$surveys[$sdata['survey_id']]['survey_name'];
								if($plan['reason_id']>0){
									$plan_survey_audit = $rating_result[-2];
									$audit_reason = $plan_reason;
								}else{
									$plan_survey_audit = 	$rating_result[$sdata['rating_manual']];
									if($sdata['not_pass_id']>0){
										$audit_reason = $reason_not_pass[$sdata['not_pass_id']]['reason_name'].' - '. html_entity_decode($sdata['note']);
									}
									if($sdata['ktc_reason_id']>0){
										$audit_reason = $reason_unsuccess[$sdata['ktc_reason_id']]['reason_name'].' - '. html_entity_decode($sdata['note_ktc']);
									}
								}
							}
							if(in_array($sdata['survey_id'],$survey_adhoc)){
									if($plan['reason_id']>0){
										$adhoc_result[$sdata['survey_id']] = $rating_result[-2];
										$adhoc_reason[$sdata['survey_id']] = $plan_reason;
									}else{
										$adhoc_result[$sdata['survey_id']] = $rating_result[$sdata['rating_manual']];
										
										if($sdata['not_pass_id']>0){
											$adhoc_reason[$sdata['survey_id']] = $reason_not_pass[$sdata['not_pass_id']]['reason_name'].' - '. html_entity_decode($sdata['note']);
										}
										if($sdata['ktc_reason_id']>0){
											$adhoc_reason[$sdata['survey_id']] = $reason_unsuccess[$sdata['ktc_reason_id']]['reason_name'].' - '. html_entity_decode($sdata['note_ktc']);
										}
									
									}
							}
							if(in_array($sid,$s_arr)){
								$survey_total+=	$sdata['survey_total'];
							}
						}
					}
					
					
					$last_round = $this->getLastRound($plan['round_name']);
					
	
					// 7- Note POSM
					$note_posm = array();
					if(isset($qc_note[$plan_id]) && isset($qc_note[$plan_id][7])&&!empty($qc_note[$plan_id][7]['notes'])){
						$note_posm['comment'] = implode(' + ', $qc_note[$plan_id][7]['notes']);
						$note_posm['date_added'] = $qc_note[$plan_id][7]['date_added'];
					}
					
					$plan_link = '=HYPERLINK("'.str_replace('&amp;', '&', $this->url->link('client/plan/info', 'plan_id='.$plan_id, true)).'", "Click here")';
					
					$trong_cho = isset($answer[1][10])?$answer[1][10]['answer_value']:'';//TRONG CHỢ
					$ngoai_cho = isset($answer[1][18])?$answer[1][18]['answer_value']:'';//NGOÀI CHỢ
					$round_month = datetimeConvert($plan['round_name'], 'm');
					$plans[] = array(
						'A' => $i,
						'B' => $plan['store_distributor'],//NPP
						'C' => $plan['distributor_code'],//CODE NPP
						'D' => $plan['store_customer_code'],//DEM CODE 
						'E' => $plan['store_ad_code'],//MÃ HỆ THỐNG NIVEA - AD CODE
						'F' => $plan['sale_name'],//SALE
						'G' => $plan['customer_user'],//SUP
						'H' => $plan['store_code'],//MÃ SỐ  TRƯNG BÀY CPM
						'I' => $plan['store_name'],//TÊN CỬA HÀNG
						'J' => $plan['store_address'],//SỐ NHÀ
						'K' => $plan['store_place'],//ĐỊA CHỈ
						'L' => $plan['store_ward'],//PHƯỜNG
						'M' => $plan['store_district'],//QUẬN
						'N' => $plan['store_province'],//TỈNH/ THÀNH PHỐ
						'O' => $plan['region_code'],//KHU VỰC
						'P' => !empty($survey_audit_name)?$survey_audit_name:'',// MỨC TB SKIN CARE ,//KIỂU CỬA HÀNG
						'Q' => $plan['store_phone'],//ĐiỆN THOẠI
						'R' => $plan['update_name'],//Update -TÊN CỬA HÀNG 
						'S' => $plan['update_address'],//Update -ĐỊA CHỈ
						'T' => $plan['update_phone'],//Update -ĐIỆN THOẠI
						'U' => $user[$plan['user_id']]['fullname'],//NHÂN VIÊN CHẤM ĐiỂM
						'V' => $plan['time_checkin'],//NGÀY CHẤM ĐiỂM
						'W' => $survey_total,//TỒNG SỐ MẶT
						'X' => isset($answer[1][26])?$answer[1][26]['answer_value']:'',//Có khay xanh
						'Y' => isset($answer[1][37])?$answer[1][37]['answer_value']:'',//Có counter top Serum Deo
						'Z' => isset($answer[1][45])?$answer[1][45]['answer_value']:'',//Hiện trạng TB counter top khi NV đến chấm điểm
						'AA' => isset($answer[1][34])?$answer[1][34]['answer_value']:'',//TẬP TRUNG/KHÔNG TẬP TRUNG
						'AB' => isset($answer[1][19])?$answer[1][19]['answer_value']:'',//Vị trí trưng bày ngang tầm mắt
						'AC' => isset($answer[1][27])?$answer[1][27]['answer_value']:'',// VỊ TRÍ  TRƯNG BÀY DO AUDITOR CẬP NHẬT 
						'AD' => $trong_cho.' - '.$ngoai_cho,// TRONG CHỢ/NGOÀI CHỢ
						'AE' => !empty($plan_survey_audit)?$plan_survey_audit:'',// KẾT QUẢ SKIN CARE
						'AF' => (isset($plan_reward_audit[$round_month])&&!empty($plan_reward_audit[$round_month]))?implode(',',$plan_reward_audit[$round_month]):'',// SỐ SERIES COUPON SKIN CARE $plan_reward_audit
						
						'AG' => isset($adhoc_result[12])?$adhoc_result[12]:'',//KẾT QUẢ ADHOC REAL MADRID #12
						'AH' => isset($adhoc_result[13])?$adhoc_result[13]:'',//KẾT QUẢ ADHOC SHAMPOO #13
						'AI' => isset($adhoc_result[14])?$adhoc_result[14]:'',//KẾT QUẢ ADHOC VISAGE #14
						'AJ' => isset($adhoc_result[15])?$adhoc_result[15]:'',//KẾT QUẢ ADHOC COOL POWDER #15
						'AK' => (isset($audit_reason)&&!empty($audit_reason))?$audit_reason:'',//SKIN CARE LÝ DO : RỚT/KTC
						
						'AL' => isset($adhoc_reason[12])?$adhoc_reason[12]:'',//REAL MADRID LÝ DO :RỚT/KTC #12
						'AM' => isset($adhoc_reason[13])?$adhoc_reason[13]:'',//SHAMPOO LÝ DO :RỚT/KTC #13
						'AN' => isset($adhoc_reason[14])?$adhoc_reason[14]:'',//VISAGE LÝ DO :RỚT/KTC #14
						'AO' => isset($adhoc_reason[15])?$adhoc_reason[15]:'',//COOL POWDER LÝ DO :RỚT/KTC #15
						
						'AP' => ($plan['reason_id']==0)?$plan['note']:$plan['note_ktc'],//NVCĐ NOTE 
						'AQ' => $plan['note_admin'],//ADMIN NOTE
						'AR' => isset($note_posm['comment']) ? html_entity_decode($note_posm['comment']): '',//NOTE POSM
						'AS' => $plan_link,
						'AT' => (isset($plan_reward_old['01'])&&!empty($plan_reward_old['01']))?implode(',',$plan_reward_old['01']):'',//01
						'AU' => (isset($plan_reward_old['02'])&&!empty($plan_reward_old['02']))?implode(',',$plan_reward_old['02']):'',//2 
						'AV' => (isset($plan_reward_old['03'])&&!empty($plan_reward_old['03']))?implode(',',$plan_reward_old['03']):'',//3
						'AW' => (isset($plan_reward_old['04'])&&!empty($plan_reward_old['04']))?implode(',',$plan_reward_old['04']):'',//4
						'AX' => (isset($plan_reward_old['05'])&&!empty($plan_reward_old['05']))?implode(',',$plan_reward_old['05']):'',//5
						'AY' => (isset($plan_reward_old['06'])&&!empty($plan_reward_old['06']))?implode(',',$plan_reward_old['06']):'',//6
						'AZ' => (isset($plan_reward_old['07'])&&!empty($plan_reward_old['07']))?implode(',',$plan_reward_old['07']):'',//7
						'BA' => (isset($plan_reward_old['08'])&&!empty($plan_reward_old['08']))?implode(',',$plan_reward_old['08']):'',//8
						'BB' => (isset($plan_reward_old['09'])&&!empty($plan_reward_old['09']))?implode(',',$plan_reward_old['09']):'',//9
						'BC' => (isset($plan_reward_old['10'])&&!empty($plan_reward_old['10']))?implode(',',$plan_reward_old['10']):'',//10
						'BD' => (isset($plan_reward_old['11'])&&!empty($plan_reward_old['11']))?implode(',',$plan_reward_old['11']):'',//11
						'BE' => (isset($plan_reward_old['12'])&&!empty($plan_reward_old['12']))?implode(',',$plan_reward_old['12']):'',//12
					
					);
					if(isset($survey_total)){
						unset($survey_total);
					}
					
				$i++;
			}
				$endtime2                     = microtime();
					$endtime2                     = explode(' ', $endtime2);
					$endtime2                     = $endtime2['1'] + $endtime2['0'];
				$loadingtime2 = number_format($endtime2 - $start_time, 4); 
					
				$this->log->write('Handle: '.$loadingtime2); 	

		$export_data = array(
			'excel_data' => $plans,
			'template_id' => $template_id,
			'template_name' => 'NiveaReport-by-'.$this->customer->getUsername().'-at-'. date('Ymd-His') .'.xlsx'
		);

		if(!empty($plans)){
			$file = $this->excel->exportWithTemplate($export_data);
			
				$file =  $this->excel->exportWithTemplate($export_data);
				$endtime3                     = microtime();
				$endtime3                     = explode(' ', $endtime3);
				$endtime3                     = $endtime3['1'] + $endtime3['0'];
				$loadingtime3 = number_format($endtime3 - $start_time, 4); 
				$this->log->write('Export: '.$loadingtime3); 	
				
				
			$this->response->setOutput(file_get_contents($file, FILE_USE_INCLUDE_PATH, null));
		}

		$url = $this->_url();

		$this->response->redirect($this->url->link('client/plan',  $url, true));
	}
	
	
	private function getLastRound($round_name){
		
				$explode = explode('-',$round_name);
				$curr_year= (int)$explode[0];
				$curr_month = (int)end($explode);
				if($curr_month==1){
					$last_month = 12;
					$last_year = $curr_year-1;
				}else{
					$last_month = $curr_month-1;
					$last_year = $curr_year;	
				}
				$last_round = $last_year.'-'.sprintf('%02d',$last_month);
				
				
		return $last_round;
	}
	
	//report
  private function getAllUserQc($filter=array()){
		$this->load->model('project/project_user');
		$user = $this->model_project_project_user->getProjectUsers();
			
		$results = array();
		$sql = "SELECT * FROM " . PDB_PREFIX . "plan_confirm qc WHERE 1";
		
		if (isset($filter['filter_round'])&&!empty($filter['filter_round'])) {
			$sql .= " AND round_name = '" . $this->pdb->escape($filter['filter_round']) . "'";
		}
		
		$query = $this->pdb->query($sql);
		foreach($query->rows as $row){
			
			$results[$row['plan_id']][$row['user_group_id']] = isset($user[$row['user_id']])?$user[$row['user_id']]['username']:''.' - '.isset($user[$row['user_id']])?$user[$row['user_id']]['fullname']:'';
		}
		return $results;
  }
	//report
  private function getThisSurveys($filter=array()){
  	$sql = "SELECT * FROM `".PDB_PREFIX."plan_survey_data` WHERE 1";
	
	if (isset($filter['filter_round'])&&!empty($filter['filter_round'])) {
		$sql .= " AND round_name = '" . $this->pdb->escape($filter['filter_round']) . "'";
	}
	
  	$query = $this->pdb->query($sql);

  	$survey = array();
  	if($query->rows){
  		foreach($query->rows as $row){
  			$survey[$row['plan_id']][$row['survey_id']][$row['question_id']] = $row;
  		}
  	}

  	return $survey;
  }
	//report
  private function getThisPlanCoupons($filter=array(),$isNationwide=0){

	$this->load->model('catalog/coupon_prefix');
	$prefix =  $this->model_catalog_coupon_prefix->getPrefixsIndexBy('coupon_prefix');
			
			
  	$sql = "SELECT plan_id,reward_type_id,coupon_prefix,coupon_code,yearmonth FROM ".PDB_PREFIX."plan_coupon_history WHERE `coupon_code`!=''";
	
	if (isset($filter['filter_round'])&&!empty($filter['filter_round'])) {
		$sql .= " AND round_name = '" . $this->pdb->escape($filter['filter_round']) . "'";
	}
	
  	$query = $this->pdb->query($sql);

  	$coupon = array();
  	if($query->rows){
  		foreach($query->rows as $row){//[$row['reward_type_id']]
  			if(!empty($row['coupon_code'])){
				$yearmonth = datetimeConvert($row['yearmonth'], 'm');
  				//$coupon[$row['plan_id']][$row['reward_type_id']][$yearmonth][] = ($isNationwide==1)?$row['coupon_code']:number_format($prefix[$row['coupon_prefix']]['prefix_value'],0,".",",");
  				$coupon[$row['plan_id']][$row['reward_type_id']][$yearmonth][] = number_format($prefix[$row['coupon_prefix']]['prefix_value'],0,".",",");
  			}
  		}
  	}

  	return $coupon;
  }
	//report + qc_report
	private function getQcNote($filter=array()){
		$sql = "SELECT * FROM `".PDB_PREFIX."plan_note` WHERE is_deleted = '0' ";
	
		if (isset($filter['filter_round'])&&!empty($filter['filter_round'])) {
			$sql .= " AND round_name = '" . $this->pdb->escape($filter['filter_round']) . "'";
		}
			$sql .= " ORDER BY date_added ASC";
	
		$query = $this->pdb->query($sql);

		$plan_note = array();
		if($query->rows){
			foreach($query->rows as $row){
				$plan_note[$row['plan_id']][$row['note_id']]['date_added'] = $row['date_added'];
				$plan_note[$row['plan_id']][$row['note_id']]['notes'][] = html_entity_decode($row['comment']);
			}
		}
		return $plan_note;
	}

	  
}
?>