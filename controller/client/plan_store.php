<?php
class ControllerClientPlanStore extends Controller {
	public function index() {
		if (isset($this->request->get['plan_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$languages= $this->load->language('plan/plan');
			foreach($languages as $key=>$value){
					$data[$key] = $value;	
			}
			
			
			$data['text_store_customer_code'] = sprintf($this->language->get('text_store_customer_code'),$this->config->get('config_meta_title'));
			
			
			$data['plan_id'] = $plan_id = $this->request->get['plan_id'];	
			
			$this->load->model('plan/plan');
			$plan_info = $this->model_plan_plan->getPlanInfo($plan_id);
			
			$data['plan_info'] =$plan_info;
			
			$this->load->model('tool/image');
			if(!empty($plan_info['store_image'])){
				$data['store_image'] =	$this->model_tool_image->best_fit($plan_info['store_image'], 250,250);
			}else{
				$data['store_image'] =	$this->model_tool_image->best_fit('store.jpg', 250,250);
			}
			$data['google_address'] = format_address($plan_info['store_ward'].'|'.$plan_info['store_district'].'|'.$plan_info['store_province'].'|Vietnam','+');
			
			$data['store_edit'] = $this->url->link('store/store/edit', '&store_id=' . $plan_info['store_id']  , true);
			
			/*StaftUser*/ 
			$this->load->model('project/project_user');
			$data['staffs'] =  $this->model_project_project_user->getProjectUsersByGroupID($this->config->get('config_user_staff'));


			$data['survey_ids'] =   isset($plan_info['survey_ids'])?explode(',',$plan_info['survey_ids']):array();
			
			$this->load->model('survey/survey');
			$data['surveys'] = $survey = $this->model_survey_survey->getSurveyIndexBy('survey_id');

	

		$data['statuses'] = array(
			'0'=> $this->language->get('text_no_perform'),
			'1'=> $this->language->get('text_has_perform')
		);
		
		
		$this->load->model('localisation/reason_unsuccess');
		$data['reasons'] = $this->model_localisation_reason_unsuccess->getReasons();
		
			$data['user_id'] = $this->user->getId();
			$template = 'client/plan_store';
			return $this->load->view($template, $data);
		}
	}
	public function info() {
		$this->response->setOutput($this->index());
	}
}