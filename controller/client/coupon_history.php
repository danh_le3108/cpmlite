<?php
class ControllerClientCouponHistory extends Controller {
	private $error = array();
	
  
	private $field_data = array(
			'coupon_id'=>'',
			'coupon_code'=>'',
			'distributor_code'=>''
		);
		
		
	/*Excel*/ 
	private $template_id = 8;
	
	private $filter_key = array(
		'filter_coupon_code',
		'filter_coupon_scan',
		'filter_yearmonth',
		'filter_round',
		'filter_sup_id',
		'filter_distributor_id',
		'filter_reward_type_id',
		'filter_coupon_status',
		'filter_date_start',
		'filter_date_end'
	);
	private function _url(){
		$url = '';
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}
	public function index() {
		$data['ajax_list'] = $this->ajax_list();
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$template = 'common/ajax_list';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
	public function getList() {
		$this->response->setOutput($this->ajax_list());
	}
	public function ajax_list() {
		
			$this->load->model('plan/plan');
			$this->model_plan_plan->activeMissingCoupons();
				
				
		$languages = $this->load->language('catalog/coupon');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		
		
		$data['qc_statuses'] = array(
			'0'=> $this->language->get('text_no_qc'),
			'1'=> $this->language->get('text_has_qc')
		);
		
		$this->document->setTitle($this->language->get('text_coupon_history'));
		
		$this->load->model('catalog/coupon_history');
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'coupon_code';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = $this->_url();
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '' , true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/coupon',  $url , true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);


		$data['coupons'] = array();

		$filter_data = array(
			'sort'                    => $sort,
			'order'                   => $order,
			'start'                   => ($page - 1) * $this->config->get('config_limit_project'),
			'limit'                   => $this->config->get('config_limit_project')
		);
		
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$filter_data[$key] = $this->request->get[$key];
			}
		}
				$filter_data['filter_qc_status'] = 1;

		if(!$this->user->isLogged()&&$this->customer->isLogged()){
			$filter_data['customers_manager'] = $customers_managers = $this->customer->customers_manager(); 
		}
		
		
		$this->load->model('catalog/coupon_status');
		$this->load->model('plan/plan');
		
		$this->load->model('catalog/reward_type');
		$reward_type = array();
		$reward_types = $this->model_catalog_reward_type->getRewardTypes();
		foreach($reward_types as $type){
			$reward_type[$type['reward_type_id']] = $type;
		}
		$data['reward_types'] = $reward_type;
		
	
		$data['coupon_status'] = $coupon_status = $this->model_catalog_coupon_status->getCouponStatuses();


		$query_total = $this->model_catalog_coupon_history->getTotalCouponsHistory($filter_data);

		$results = $this->model_catalog_coupon_history->getCouponsHistory($filter_data);

		$this->load->model('project/project_user');
		$user = $this->model_project_project_user->getProjectUsers();
		
		$this->load->model('store/store');
		$store_fields = array(
			'store_id',
			'store_name',
			'store_code',
			'distributor_code',
			'store_distributor'
		);
		$store = $this->model_store_store->getStoreByFields($store_fields,'store_id');
		
		
		$this->load->model('catalog/coupon_prefix');
		$prefix =  $this->model_catalog_coupon_prefix->getPrefixsIndexBy('coupon_prefix');
		
		
		$this->load->model('catalog/distributor');
		$distributor = $this->model_catalog_distributor->getDistributorIndexBy('distributor_id');
			
		$this->load->model('distributor/coupon_scan');
		$data['coupon_scans'] = $scans =  $this->model_distributor_coupon_scan->getCouponScanesByIndex();
		
		
		$config_top = $this->config->get('config_customer_top');
		$config_dist_valid = $this->config->get('config_dist_valid');
		
		if($this->user->isLogged()||($this->customer->isLogged() && $this->customer->getLevel()==$config_top)){
			$isNationwide = 1;
		}else{
			$isNationwide =0;
		}	
		$data['isNationwide'] = $isNationwide;
		
		$config_coupon_has_paid = $this->config->get('config_coupon_has_paid');
		
		$config_dist_valid = $this->config->get('config_dist_valid');	
		
		foreach ($results as $result) {
			if(isset($store[$result['store_id']])){
				$store_info = $store[$result['store_id']];
				$store_name = $store_info['store_name'];
				$store_code = $store_info['store_code'];
			} else {
				$store_code = '';
				$store_name = '';
			}
			
			if(isset($distributor[$result['distributor_id']])){
				$dinfo = $distributor[$result['distributor_id']];
				$distributor_code = $dinfo['distributor_code'];
				$distributor_name = $dinfo['distributor_name'];
			}else{
				$distributor_name = $distributor_code = '';
			}

			$plan_href = ($result['plan_id']>0)?$this->url->link('client/plan/info', '&plan_id=' . $result['plan_id']  , true):'';
			
			
			if(isset($scans[$result['dist_check_id']])){
				$scan_status = $scans[$result['dist_check_id']]['coupon_scan_name'];
			}else{
				$scan_status = '';
			}
				
			
			$username = isset($user[$result['user_id']])?$user[$result['user_id']]['username'].' <br/> '.$user[$result['user_id']]['fullname']:'';
			$coupon_value = isset($prefix[$result['coupon_prefix']])?$prefix[$result['coupon_prefix']]['prefix_value']:$result['coupon_prefix'];
			$prefix_title = isset($prefix[$result['coupon_prefix']])?$prefix[$result['coupon_prefix']]['prefix_title']:$result['coupon_prefix'];
			//$isNationwide==1||
			$data['coupons'][] = array(
				'coupon_id'        => $result['coupon_id'],
				'username'        => $username,
				'time_remaining' =>($result['dist_check_id']!=$config_dist_valid)? $this->model_catalog_coupon_history->time_remaining($result['date_upload']):'<i class="fa fa-check text-green"></i>',
				'coupon_code'      => ($result['dist_check_id']==$config_dist_valid)?$result['coupon_code']:'',
				'coupon_prefix'      => $result['coupon_prefix'],
				'prefix_title'      => $prefix_title,
				'coupon_value'      => number_format($coupon_value,0,",","."),
				'scan_status'      => $scan_status,
				'yearmonth'      => $result['yearmonth'],
				'date_upload'      => ($result['coupon_status_id']==$config_coupon_has_paid)?date('Y-m-d',strtotime($result['date_upload'])):'',
				'distributor_code' => $distributor_code,
				'distributor_name' => $distributor_name,
				'reward_type' => isset($reward_type[$result['reward_type_id']]) ? $reward_type[$result['reward_type_id']]['reward_type_name'] : '',
				'store_code' => $store_code,
				'store_name' => $store_name,
				'plan_href' => $plan_href,
				'coupon_status'    => isset($coupon_status[$result['coupon_status_id']]) ? $coupon_status[$result['coupon_status_id']]['coupon_status_name'] : ''
			);
		}


		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = $this->_url();

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/coupon', '&sort=name' . $url , true);

		$url = $this->_url();

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $query_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_project');
		$pagination->url = $this->url->link('client/coupon_history/getList', $url . '&page={page}' , true);

		$data['pagination'] = $pagination->render();

		$data['results'] = $pagination->results();
		$data['total'] = $query_total;

		$data['sort'] = $sort;
		$data['order'] = $order;
		
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$data[$key] = $this->request->get[$key];
			}else{
				$data[$key] = NULL;
			}
		}
		/**/ 
		
		$this->load->model('plan/round');
		$data['rounds'] =  $this->model_plan_round->getRounds();
		
		
		$data['yearmonths'] =  $this->model_catalog_coupon_history->getYearMonths();
		$data['distributors'] =  $this->model_catalog_distributor->getDistributors();
		
		$this->load->model('project/project_user');
		$data['users'] =  $this->model_project_project_user->getProjectUsersByGroupID($this->config->get('config_user_sup'));	
		
		$data['allow_filter'] = array($this->config->get('config_reward_audit'),$this->config->get('config_reward_old'));
				
		/**/ 	
		$data['template_id'] = $this->template_id;
		$data['export'] = $this->url->link('client/coupon_history/export', $url , true);
		$data['has_edit'] = $this->document->hasPermission('access', 'client/coupon_history');
		$data['report'] = $this->url->link('client/coupon_history/report', $url , true);


		$template = 'client/coupon_history';
		return $this->load->view($template, $data);
	}
	
	  

 	public function export() {
			$export_name = '';
				
			$this->load->model('catalog/coupon_history');
			$json = array();
			$filter_data = array();
			foreach($this->filter_key as $key){
				if (isset($this->request->get[$key])) {
					$filter_data[$key] = $this->request->get[$key];
				}
			}
			if(!$this->user->isLogged()&&$this->customer->isLogged()){
				$filter_data['customers_manager'] = $customers_managers = $this->customer->customers_manager(); 
			}

		
			$template_id = 8;
			$template_info = $this->excel->getTemplate($template_id);
			$setting = $template_info['settings'];
			$rstart = $rend = $setting['start_row'];
			$rend = $rstart-1;
			 
			if(isset($template_info['template_id'])){
				
					/*Loading Start*/
					$starttime = explode(' ',microtime());
					$start_time =  $starttime['1'] + $starttime['0'];
					
					/*Loading Start*/
					
			
			
				$this->load->model('store/store');
				$store_fields = array(
					'store_id',
					'store_code',
					'store_ad_code',
					'store_name',
					'store_phone',
					'store_address',
					'store_place',
					'store_ward',
					'store_district',
					'store_province'
				);
				$store = $this->model_store_store->getStoreByFields($store_fields,'store_id');
				
				$color = array(
				'0'=>'92D050',
				'1'=>'FFFF00',
				'2'=>'FF0000',
				);
			
				$this->load->model('catalog/distributor');
				$distributor = $this->model_catalog_distributor->getDistributorIndexBy('distributor_id');
	
				if (isset($this->request->get['filter_distributor_id'])) {
					$did = $this->request->get['filter_distributor_id'];
					$export_name = $distributor[$did]['distributor_code'].'-'.$distributor[$did]['distributor_name'];
					$export_name = alias($export_name,'-');
				}
				
				
				$this->load->model('catalog/coupon_prefix');
				$prefix =  $this->model_catalog_coupon_prefix->getPrefixsIndexBy('coupon_prefix');
				
				$coupon_histories = $this->model_catalog_coupon_history->getCouponsHistory($filter_data);
				if($this->user->isLogged()){
					$logged_id = $this->user->isLogged();
					$logged_name = $this->user->getUserName();
				}else{
					$logged_id = $this->customer->isLogged();
					$logged_name = $this->customer->getUserName();
				}
				$results = $this->model_catalog_coupon_history->mappingCouponsByStoreID($coupon_histories);
				//print_r('<pre>'); print_r($results); print_r('</pre>'); die();
				
					/*Loading Start*/
					$endtime                     = microtime();
					$endtime                     = explode(' ', $endtime);
					$endtime                     = $endtime['1'] + $endtime['0'];
					$loadingtime = number_format($endtime - $start_time, 4);
					$this->log->write('Coupon: #'.$logged_id.' #'.$logged_name.' GetCoupons: '.$loadingtime); 
					/*Loading End*/
					
				$check_valid = $this->config->get('config_dist_valid');
		
		
				$coupons = array();
				$i = 1;
				foreach($results as $store_id => $val){
					//print_r('<pre>'); print_r($val); print_r('</pre>'); die();
					if(isset($store[$store_id])){
						$coupon = $val['coupons'];
						
						$store_info = $store[$store_id];
						$store_code = $store_info['store_code'];
						$store_ad_code = $store_info['store_ad_code'];
						$store_name = $store_info['store_name'];
						$store_phone = $store_info['store_phone'];
						$store_address = $store_info['store_address'];
						$store_place = $store_info['store_place'];
						$store_ward = $store_info['store_ward'];
						$store_district = $store_info['store_district'];
						$store_province = $store_info['store_province'];
					
				
						if(isset($distributor[$val['distributor_id']])){
							$dinfo = $distributor[$val['distributor_id']];
							$distributor_code = $dinfo['distributor_code'];
							$distributor_name = $dinfo['distributor_name'];
						}else{
							$distributor_name = $distributor_code = '';
						}
						
	
					$coupons[]['style'] = array(
						'A'=>'',
						'B'=>'',
						'C'=>'',
						'D'=>'',
						'E'=>'',
						'F'=>'',
						'G'=>'',
						'H'=>'',
						'I'=>'',
						'J'=>'',
						'K'=>'',
						'L'=>isset($coupon['01']['dist_check_id'])?$color[$coupon['01']['dist_check_id']]:'',
						'M'=>'',
						'N'=>'',
						'O'=>isset($coupon['02']['dist_check_id'])?$color[$coupon['02']['dist_check_id']]:'',
						'P'=>'',
						'Q'=>'',
						'R'=>isset($coupon['03']['dist_check_id'])?$color[$coupon['03']['dist_check_id']]:'',
						'S'=>'',
						'T'=>'',
						'U'=>isset($coupon['04']['dist_check_id'])?$color[$coupon['04']['dist_check_id']]:'',
						'V'=>'',
						'W'=>'',
						'X'=>isset($coupon['05']['dist_check_id'])?$color[$coupon['05']['dist_check_id']]:'',
						'Y'=>'',
						'Z'=>'',
						'AA'=>isset($coupon['06']['dist_check_id'])?$color[$coupon['06']['dist_check_id']]:'',
						'AB'=>'',
						'AC'=>'',
						'AD'=>isset($coupon['07']['dist_check_id'])?$color[$coupon['07']['dist_check_id']]:'',
						'AE'=>'',
						'AF'=>'',
						'AG'=>isset($coupon['08']['dist_check_id'])?$color[$coupon['08']['dist_check_id']]:'',
						'AH'=>'',
						'AI'=>'',
						'AJ'=>isset($coupon['09']['dist_check_id'])?$color[$coupon['09']['dist_check_id']]:'',
						'AK'=>'',
						'AL'=>'',
						'AM'=>isset($coupon['10']['dist_check_id'])?$color[$coupon['10']['dist_check_id']]:'',
						'AN'=>'',
						'AO'=>'',
						'AP'=>isset($coupon['11']['dist_check_id'])?$color[$coupon['11']['dist_check_id']]:'',
						'AQ'=>'',
						'AR'=>'',
						'AS'=>isset($coupon['12']['dist_check_id'])?$color[$coupon['12']['dist_check_id']]:'',
						'AT'=>'',
						'AU'=>''
					);
	
					$coupons[]['value'] = array(
						'A'=>$i,
						'B'=>$distributor_name,
						'C'=>$distributor_code,
						'D'=>$store_ad_code,
						'E'=>$store_code,
						'F'=>$store_name,
						'G'=>$store_address,
						'H'=>$store_place,
						'I'=>$store_ward,
						'J'=>$store_district,
						'K'=>$store_province,
						'L'=>isset($coupon['01']['coupon_prefix'])?$prefix[$coupon['01']['coupon_prefix']]['prefix_value']:'',//1
						'M'=>(isset($coupon['01']['coupon_code'])&&$coupon['01']['dist_check_id']==$check_valid)?$coupon['01']['coupon_code']:'',//1
						'N'=>(isset($coupon['01']['time_remaining'])&&$coupon['01']['dist_check_id']!=$check_valid)?$coupon['01']['time_remaining']:'',
						'O'=>isset($coupon['02']['coupon_prefix'])?$prefix[$coupon['02']['coupon_prefix']]['prefix_value']:'',//2
						'P'=>(isset($coupon['02']['coupon_code'])&&$coupon['02']['dist_check_id']==$check_valid)?$coupon['02']['coupon_code']:'',//2
						'Q'=>(isset($coupon['02']['time_remaining'])&&$coupon['02']['dist_check_id']!=$check_valid)?$coupon['02']['time_remaining']:'',
						'R'=>isset($coupon['03']['coupon_prefix'])?$prefix[$coupon['03']['coupon_prefix']]['prefix_value']:'',//3
						'S'=>(isset($coupon['03']['coupon_code'])&&$coupon['03']['dist_check_id']==$check_valid)?$coupon['03']['coupon_code']:'',//3
						'T'=>(isset($coupon['03']['time_remaining'])&&$coupon['03']['dist_check_id']!=$check_valid)?$coupon['03']['time_remaining']:'',
						'U'=>isset($coupon['04']['coupon_prefix'])?$prefix[$coupon['04']['coupon_prefix']]['prefix_value']:'',//4
						'V'=>(isset($coupon['04']['coupon_code'])&&$coupon['04']['dist_check_id']==$check_valid)?$coupon['04']['coupon_code']:'',//4
						'W'=>(isset($coupon['04']['time_remaining'])&&$coupon['04']['dist_check_id']!=$check_valid)?$coupon['04']['time_remaining']:'',
						'X'=>isset($coupon['05']['coupon_prefix'])?$prefix[$coupon['05']['coupon_prefix']]['prefix_value']:'',//5
						'Y'=>(isset($coupon['05']['coupon_code'])&&$coupon['05']['dist_check_id']==$check_valid)?$coupon['05']['coupon_code']:'',//5
						'Z'=>(isset($coupon['05']['time_remaining'])&&$coupon['05']['dist_check_id']!=$check_valid)?$coupon['05']['time_remaining']:'',
						'AA'=>isset($coupon['06']['coupon_prefix'])?$prefix[$coupon['06']['coupon_prefix']]['prefix_value']:'',//6
						'AB'=>(isset($coupon['06']['coupon_code'])&&$coupon['06']['dist_check_id']==$check_valid)?$coupon['06']['coupon_code']:'',//6
						'AC'=>(isset($coupon['06']['time_remaining'])&&$coupon['06']['dist_check_id']!=$check_valid)?$coupon['06']['time_remaining']:'',
						'AD'=>isset($coupon['07']['coupon_prefix'])?$prefix[$coupon['07']['coupon_prefix']]['prefix_value']:'',//7
						'AE'=>(isset($coupon['07']['coupon_code'])&&$coupon['07']['dist_check_id']==$check_valid)?$coupon['07']['coupon_code']:'',//7
						'AF'=>(isset($coupon['07']['time_remaining'])&&$coupon['07']['dist_check_id']!=$check_valid)?$coupon['07']['time_remaining']:'',
						'AG'=>isset($coupon['08']['coupon_prefix'])?$prefix[$coupon['08']['coupon_prefix']]['prefix_value']:'',//8
						'AH'=>(isset($coupon['08']['coupon_code'])&&$coupon['08']['dist_check_id']==$check_valid)?$coupon['08']['coupon_code']:'',//8
						'AI'=>(isset($coupon['08']['time_remaining'])&&$coupon['08']['dist_check_id']!=$check_valid)?$coupon['08']['time_remaining']:'',
						'AJ'=>isset($coupon['09']['coupon_prefix'])?$prefix[$coupon['09']['coupon_prefix']]['prefix_value']:'',//9
						'AK'=>(isset($coupon['09']['coupon_code'])&&$coupon['09']['dist_check_id']==$check_valid)?$coupon['09']['coupon_code']:'',//9
						'AL'=>(isset($coupon['09']['time_remaining'])&&$coupon['09']['dist_check_id']!=$check_valid)?$coupon['09']['time_remaining']:'',
						'AM'=>isset($coupon['10']['coupon_prefix'])?$prefix[$coupon['10']['coupon_prefix']]['prefix_value']:'',//10
						'AN'=>(isset($coupon['10']['coupon_code'])&&$coupon['10']['dist_check_id']==$check_valid)?$coupon['10']['coupon_code']:'',//10
						'AO'=>(isset($coupon['10']['time_remaining'])&&$coupon['10']['dist_check_id']!=$check_valid)?$coupon['10']['time_remaining']:'',
						'AP'=>isset($coupon['11']['coupon_prefix'])?$prefix[$coupon['11']['coupon_prefix']]['prefix_value']:'',//11
						'AQ'=>(isset($coupon['11']['coupon_code'])&&$coupon['11']['dist_check_id']==$check_valid)?$coupon['11']['coupon_code']:'',//11
						'AR'=>(isset($coupon['11']['time_remaining'])&&$coupon['11']['dist_check_id']!=$check_valid)?$coupon['11']['time_remaining']:'',
						'AS'=>isset($coupon['12']['coupon_prefix'])?$prefix[$coupon['12']['coupon_prefix']]['prefix_value']:'',//12
						'AT'=>(isset($coupon['12']['coupon_code'])&&$coupon['12']['dist_check_id']==$check_valid)?$coupon['12']['coupon_code']:'',//12
						'AU'=>(isset($coupon['12']['time_remaining'])&&$coupon['12']['dist_check_id']!=$check_valid)?$coupon['12']['time_remaining']:'',
						);
						$i++;
						$rend++;
					} 
				}
					$coupons[]['value'] = array(
						'A'=>'',
						'B'=>'',
						'C'=>'',
						'D'=>'',
						'E'=>'',
						'F'=>'',
						'G'=>'',
						'H'=>'',
						'I'=>'',
						'J'=>'',
						'K'=>'Tổng cộng',
						'L'=>'=SUM(L'.$rstart.':L'.$rend.')',//1
						'M'=>'',
						'N'=>'',
						'O'=>'=SUM(O'.$rstart.':O'.$rend.')',//2
						'P'=>'',
						'Q'=>'',
						'R'=>'=SUM(R'.$rstart.':R'.$rend.')',//3
						'S'=>'',
						'T'=>'',
						'U'=>'=SUM(U'.$rstart.':U'.$rend.')',//4
						'V'=>'',
						'W'=>'',
						'X'=>'=SUM(X'.$rstart.':X'.$rend.')',//5
						'Y'=>'',
						'Z'=>'',
						'AA'=>'=SUM(AA'.$rstart.':AA'.$rend.')',//6
						'AB'=>'',
						'AC'=>'',
						'AD'=>'=SUM(AD'.$rstart.':AD'.$rend.')',//7
						'AE'=>'',
						'AF'=>'',
						'AG'=>'=SUM(AG'.$rstart.':AG'.$rend.')',//8
						'AH'=>'',
						'AI'=>'',
						'AJ'=>'=SUM(AJ'.$rstart.':AJ'.$rend.')',//9
						'AK'=>'',
						'AL'=>'',
						'AM'=>'=SUM(AM'.$rstart.':AM'.$rend.')',//10
						'AN'=>'',
						'AO'=>'',
						'AP'=>'=SUM(AP'.$rstart.':AP'.$rend.')',//11
						'AQ'=>'',
						'AR'=>'',
						'AS'=>'=SUM(AS'.$rstart.':AS'.$rend.')',//12
						'AT'=>'',
						'AU'=>''
					);
	
				
				//print_r('<pre>'); print_r($coupons); print_r('</pre>'); 
				/*Mapping Data*/ 
				$endtime2                     = microtime();
					$endtime2                     = explode(' ', $endtime2);
					$endtime2                     = $endtime2['1'] + $endtime2['0'];
				$loadingtime2 = number_format($endtime2 - $start_time, 4); 
					
				$this->log->write('Mapping Data: '.$loadingtime2); 
				//print_r('<pre>'); print_r($loadingtime2); print_r('</pre>');die();
				/*Mapping Data End*/
				
				$export_data = array(
					'excel_data' => $coupons,
					'template_id' => $template_id,
					'template_name' => 'Bao-cao-Thanh-toan-cho-NPP-'.$export_name.'-'. date('Y-m-d-His') .'.xlsx'
				);
	
	
				$file = $this->excel->exportData($export_data);
			
				$endtime3                     = microtime();
				$endtime3                     = explode(' ', $endtime3);
				$endtime3                     = $endtime3['1'] + $endtime3['0'];
				$loadingtime3 = number_format($endtime3 - $start_time, 4); 
				$this->log->write('Write Excel: '.$loadingtime3); 	
				$this->response->setOutput(file_get_contents($file, FILE_USE_INCLUDE_PATH, null));
			}
				
			$url = $this->_url();
		
			$this->response->redirect($this->url->link('client/coupon_history', $url , true));
	}
	public function report() {
			$export_name = '';
				
			$this->load->model('catalog/coupon_history');
			$json = array();
			$filter_data = array();
			foreach($this->filter_key as $key){
				if (isset($this->request->get[$key])) {
					$filter_data[$key] = $this->request->get[$key];
					$$key = $this->request->get[$key];
				}else{
					$$key = NULL;
				}
			}
			
		
			if(!$this->user->isLogged()&&$this->customer->isLogged()){
				$filter_data['customers_manager'] = $customers_managers = $this->customer->customers_manager(); 
			}
		
			$template_id = 11;
			$template_info = $this->excel->getTemplate($template_id);
			$setting = $template_info['settings'];
			$rstart = $rend = $setting['start_row'];
			$rend = $rstart-1;
			 
			if(isset($template_info['template_id'])){
				
					/*Loading Start*/
					$starttime = explode(' ',microtime());
					$start_time =  $starttime['1'] + $starttime['0'];
					
					/*Loading Start*/
			
			
				$this->load->model('catalog/distributor');
				$distributor = $this->model_catalog_distributor->getDistributorIndexBy('distributor_id');
	
				if (isset($this->request->get['filter_distributor_id'])) {
					$did = $this->request->get['filter_distributor_id'];
					$export_name = $distributor[$did]['distributor_code'].'-'.$distributor[$did]['distributor_name'];
					$export_name = alias($export_name,'-');
					
					$distributor_code = $distributor[$did]['distributor_code'];
					$distributor_name = $distributor[$did]['distributor_name'];
					
					
				}else{
					$distributor_code = '';
					$distributor_name = '';
				}
				
				
				if($this->user->isLogged()){
					$logged_id = $this->user->isLogged();
					$logged_name = $this->user->getUserName();
				}else{
					$logged_id = $this->customer->isLogged();
					$logged_name = $this->customer->getUserName();
				}
				
				$report = $this->model_catalog_coupon_history->getTotalCouponsByMonth($filter_data);
				
				//print_r('<pre>'); print_r($report); print_r('</pre>'); die();
					/*Loading Start*/
					$endtime                     = microtime();
					$endtime                     = explode(' ', $endtime);
					$endtime                     = $endtime['1'] + $endtime['0'];
					$loadingtime = number_format($endtime - $start_time, 4);
					$this->log->write('CouponReport: #'.$logged_id.' #'.$logged_name.' GetReport: '.$loadingtime); 
					/*Loading End*/
					
		
		
				$coupons = array();
				
					$coupons[] = array(
						'A'=>'',
						'B'=>$filter_date_start,
						'C'=>'','D'=>'','E'=>'','F'=>'','G'=>'','H'=>'','I'=>'',
						'J'=>$filter_date_end,
						'K'=>'','L'=>'',
						'M'=>'','N'=>'',
						'O'=>date('Y-m-d H:i:s')
					);
					
					$coupons[] = array(
						'A'=>'','B'=>$distributor_code
					);
					
					$coupons[] = array(
						'A'=>'','B'=>$distributor_name
					);
					
					$coupons[] = array('A'=>'');
					
					$coupons[] = array('A'=>'Tổng hợp thanh toán coupon trưng bày NIVEA');
					
					$coupons[] = array('A'=>'');
					
					$coupons[] = array('A'=>'Loại Coupon');
					
	
					//Blue
					$coupons[] = array(
						'A'=>'BLUE',
						'B'=>'1000000',
						'C'=>isset($report['01'])&&isset($report['01']['BS'])?$report['01']['BS']:'',//01
						'D'=>'=IF(C8>0,C8*B8,"")',
						'E'=>isset($report['02'])&&isset($report['02']['BS'])?$report['02']['BS']:'',//02
						'F'=>'',
						'G'=>isset($report['03'])&&isset($report['03']['BS'])?$report['03']['BS']:'',//03
						'H'=>'',
						'I'=>isset($report['04'])&&isset($report['04']['BS'])?$report['04']['BS']:'',//04
						'J'=>'',
						'K'=>isset($report['05'])&&isset($report['05']['BS'])?$report['05']['BS']:'',//05
						'L'=>'',
						'M'=>isset($report['06'])&&isset($report['06']['BS'])?$report['06']['BS']:'',//06
						'N'=>'',
						'O'=>isset($report['07'])&&isset($report['07']['BS'])?$report['07']['BS']:'',//07
						'P'=>'',
						'Q'=>isset($report['08'])&&isset($report['08']['BS'])?$report['08']['BS']:'',//08
						'R'=>'',
						'S'=>isset($report['09'])&&isset($report['09']['BS'])?$report['09']['BS']:'',//09
						'T'=>'',
						'U'=>isset($report['10'])&&isset($report['10']['BS'])?$report['10']['BS']:'',//10
						'V'=>'',
						'W'=>isset($report['11'])&&isset($report['11']['BS'])?$report['11']['BS']:'',//11
						'X'=>'',
						'Y'=>isset($report['12'])&&isset($report['12']['BS'])?$report['12']['BS']:''//12
					);
	
					//Diamond
					$coupons[] = array(
						'A'=>'DIAMOND',
						'B'=>'400000',
						'C'=>isset($report['01'])&&isset($report['01']['DS'])?$report['01']['DS']:'',//01
						'D'=>'',
						'E'=>isset($report['02'])&&isset($report['02']['DS'])?$report['02']['DS']:'',//02
						'F'=>'',
						'G'=>isset($report['03'])&&isset($report['03']['DS'])?$report['03']['DS']:'',//03
						'H'=>'',
						'I'=>isset($report['04'])&&isset($report['04']['DS'])?$report['04']['DS']:'',//04
						'J'=>'',
						'K'=>isset($report['05'])&&isset($report['05']['DS'])?$report['05']['DS']:'',//05
						'L'=>'',
						'M'=>isset($report['06'])&&isset($report['06']['DS'])?$report['06']['DS']:'',//06
						'N'=>'',
						'O'=>isset($report['07'])&&isset($report['07']['DS'])?$report['07']['DS']:'',//07
						'P'=>'',
						'Q'=>isset($report['08'])&&isset($report['08']['DS'])?$report['08']['DS']:'',//08
						'R'=>'',
						'S'=>isset($report['09'])&&isset($report['09']['DS'])?$report['09']['DS']:'',//09
						'T'=>'',
						'U'=>isset($report['10'])&&isset($report['10']['DS'])?$report['10']['DS']:'',//10
						'V'=>'',
						'W'=>isset($report['11'])&&isset($report['11']['DS'])?$report['11']['DS']:'',//11
						'X'=>'',
						'Y'=>isset($report['12'])&&isset($report['12']['DS'])?$report['12']['DS']:''//12
					);
					
					//Gold
					$coupons[] = array(
						'A'=>'GOLD',
						'B'=>'200000',
						'C'=>isset($report['01'])&&isset($report['01']['GS'])?$report['01']['GS']:'',//01
						'D'=>'',
						'E'=>isset($report['02'])&&isset($report['02']['GS'])?$report['02']['GS']:'',//02
						'F'=>'',
						'G'=>isset($report['03'])&&isset($report['03']['GS'])?$report['03']['GS']:'',//03
						'H'=>'',
						'I'=>isset($report['04'])&&isset($report['04']['GS'])?$report['04']['GS']:'',//04
						'J'=>'',
						'K'=>isset($report['05'])&&isset($report['05']['GS'])?$report['05']['GS']:'',//05
						'L'=>'',
						'M'=>isset($report['06'])&&isset($report['06']['GS'])?$report['06']['GS']:'',//06
						'N'=>'',
						'O'=>isset($report['07'])&&isset($report['07']['GS'])?$report['07']['GS']:'',//07
						'P'=>'',
						'Q'=>isset($report['08'])&&isset($report['08']['GS'])?$report['08']['GS']:'',//08
						'R'=>'',
						'S'=>isset($report['09'])&&isset($report['09']['GS'])?$report['09']['GS']:'',//09
						'T'=>'',
						'U'=>isset($report['10'])&&isset($report['10']['GS'])?$report['10']['GS']:'',//10
						'V'=>'',
						'W'=>isset($report['11'])&&isset($report['11']['GS'])?$report['11']['GS']:'',//11
						'X'=>'',
						'Y'=>isset($report['12'])&&isset($report['12']['GS'])?$report['12']['GS']:''//12
					);
					
					//Silver
					$coupons[] = array(
						'A'=>'SILVER',
						'B'=>'120000',
						'C'=>isset($report['01'])&&isset($report['01']['SS'])?$report['01']['SS']:'',//01
						'D'=>'',
						'E'=>isset($report['02'])&&isset($report['02']['SS'])?$report['02']['SS']:'',//02
						'F'=>'',
						'G'=>isset($report['03'])&&isset($report['03']['SS'])?$report['03']['SS']:'',//03
						'H'=>'',
						'I'=>isset($report['04'])&&isset($report['04']['SS'])?$report['04']['SS']:'',//04
						'J'=>'',
						'K'=>isset($report['05'])&&isset($report['05']['SS'])?$report['05']['SS']:'',//05
						'L'=>'',
						'M'=>isset($report['06'])&&isset($report['06']['SS'])?$report['06']['SS']:'',//06
						'N'=>'',
						'O'=>isset($report['07'])&&isset($report['07']['SS'])?$report['07']['SS']:'',//07
						'P'=>'',
						'Q'=>isset($report['08'])&&isset($report['08']['SS'])?$report['08']['SS']:'',//08
						'R'=>'',
						'S'=>isset($report['09'])&&isset($report['09']['SS'])?$report['09']['SS']:'',//09
						'T'=>'',
						'U'=>isset($report['10'])&&isset($report['10']['SS'])?$report['10']['SS']:'',//10
						'V'=>'',
						'W'=>isset($report['11'])&&isset($report['11']['SS'])?$report['11']['SS']:'',//11
						'X'=>'',
						'Y'=>isset($report['12'])&&isset($report['12']['SS'])?$report['12']['SS']:''//12
					);
					
				//print_r('<pre>'); print_r($coupons); print_r('</pre>'); die();
				/*Mapping Data*/ 
				$endtime2                     = microtime();
					$endtime2                     = explode(' ', $endtime2);
					$endtime2                     = $endtime2['1'] + $endtime2['0'];
				$loadingtime2 = number_format($endtime2 - $start_time, 4); 
					
				$this->log->write('Mapping Data: '.$loadingtime2); 
				/*Mapping Data End*/
				
				$export_data = array(
					'excel_data' => $coupons,
					'template_id' => $template_id,
					'template_name' => 'Baocao-Thanh-toan-cho-NPP-'.$export_name.'-'. date('Y-m-d-His') .'.xlsx'
				);
	
	
				$file = $this->excel->exportWithTemplate($export_data);
			
				$endtime3                     = microtime();
				$endtime3                     = explode(' ', $endtime3);
				$endtime3                     = $endtime3['1'] + $endtime3['0'];
				$loadingtime3 = number_format($endtime3 - $start_time, 4); 
				$this->log->write('Write Excel: '.$loadingtime3); 	
				$this->response->setOutput(file_get_contents($file, FILE_USE_INCLUDE_PATH, null));
			}
				
			$url = $this->_url();
		
			$this->response->redirect($this->url->link('client/coupon_history', $url , true));
	}
}