<?php
class ControllerClientPlanCoupon extends Controller {	
	public function index() {
		if (isset($this->request->get['plan_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$languages= $this->load->language('plan/plan');
			foreach($languages as $key=>$value){
					$data[$key] = $value;	
			}
			$data['plan_id'] = $plan_id = $this->request->get['plan_id'];	
			
			$this->load->model('plan/plan');
			$data['plan_info'] =  $plan_info = $this->model_plan_plan->getPlanInfo($plan_id);
			
			$data['plan_coupons'] = $this->model_plan_plan->getPlanCoupons($plan_id,$plan_info['import_id'],'coupon_history_id');

			$this->load->model('catalog/reward_type');
			$data['reward_types'] = $this->model_catalog_reward_type->getRewardTypes();
			
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
			
		if (isset($this->session->data['error_warning'])) {
			$data['error_warning'] = $this->session->data['error_warning'];

			unset($this->session->data['error_warning']);
		} else {
			$data['error_warning'] = '';
		}
			
		$this->load->model('catalog/coupon_prefix');
		$data['prefix'] = $this->model_catalog_coupon_prefix->getPrefixsIndexBy('coupon_prefix');
		
			$data['config_reward_recall'] = $config_reward_recall = $this->config->get('config_reward_recall');
		
			$data['config_coupon_valid'] = $config_coupon_valid =  $this->config->get('config_coupon_valid');
			
			$data['config_coupon_has_paid'] = $config_coupon_has_paid = $this->config->get('config_coupon_has_paid');
			
			$data['config_reward_audit'] = $config_reward_audit = $this->config->get('config_reward_audit');
			
			$data['config_reward_old'] = $config_reward_old = $this->config->get('config_reward_old');
			
			
			$data['valid'] = array($config_coupon_valid,$config_coupon_has_paid);
			$data['allow_view'] = array($config_reward_audit,$config_reward_old);
			
		$data['config_customer_top'] = $config_top = $this->config->get('config_customer_top');
		
		if($this->user->isLogged()||$this->customer->getLevel()==$data['config_customer_top']){
			$data['isNationwide'] = $isNationwide = 1;
		}else{
			$data['isNationwide'] = $isNationwide =0;
		}
			
			$data['group_name'] = $this->user->getGroup();
			$data['user_id'] = $this->user->getId();
			$data['user_group_id'] = $this->user->getGroupId();
			$data['username'] = $this->user->getUsername();
			$template = 'client/plan_coupon';
			return $this->load->view($template, $data);
		}
	}
	public function info() {
		$this->response->setOutput($this->index());
	}
}