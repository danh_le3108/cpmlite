<?php
class ControllerClientMap extends Controller {

	private $filter_key = array(
			'filter_date',
			'filter_global',
			'filter_user_provinces',
			'filter_province_id',
			'filter_round',
			'filter_rating_status',
			'filter_status',
			'filter_user',
			'filter_name',
			'filter_area_code',
			'filter_region_code',
			'filter_customer_parent_id',
			'filter_customer_user_id',
			'filter_survey_group',
			'filter_plan_status'
		);
	private function _url(){
		$url = '';
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}	
	public function index() {
		$languages= $this->load->language('plan/map');

		foreach($languages as $key=>$value){

				$data[$key] = $value;

		}
		$data['rstatuses'] = array(
			'1'=> $this->language->get('text_rating_passed'),//$this->language->get('text_rating_success')
			'-1'=> $this->language->get('text_rating_not_passed'),
			'-2'=> $this->language->get('text_rating_unsuccess')
		);

		$this->document->setTitle($this->language->get('heading_title'));
		
		
		$filter_data = array();

		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$data[$key] = $this->request->get[$key];
				$filter_data[$key] = trim($this->request->get[$key]);
			} else{
				$data[$key] = null;
			}
		}	
		
		$filter_data['filter_qc_status'] = 1;
		
		
		if(!$this->user->isLogged()&&$this->customer->isLogged()){
			$filter_data['customers_manager'] = $this->customer->customers_manager();
		}
		if (isset($this->request->get['filter_customer_parent_id'])) {
			$filter_data['customers_manager'] = $this->customer->getSupsByParentID($this->request->get['filter_customer_parent_id']);
		}
		
		 
		$data['customers'] =  $this->customer->getAllSups($filter_data);
		
		$this->load->model('plan/round');
		$data['rounds'] = $this->model_plan_round->getRounds();
		
		$this->load->model('localisation/area');
		$data['areas'] =  $this->model_localisation_area->getAreas($filter_data);


		$this->load->model('localisation/region');
		
		$data['regions'] = $this->model_localisation_region->getRegions($filter_data);
		
		$this->load->model('localisation/province');

		$data['provinces'] = $this->model_localisation_province->getProvinces($filter_data);



		/*DATA START HERE*/ 
		$this->load->model('plan/plan');
		$data['total'] =  $this->model_plan_plan->getTotalPlans($filter_data);
		$data['latitude'] = '10.824725';
		$data['longitude'] = '106.640540';

		if (isset($this->request->get['filter_area_code'])) {
			$this->load->model('localisation/area');
			$area_info = $this->model_localisation_area->getAreaByCode($this->request->get['filter_area_code']);
			$data['latitude'] = $latitude = $area_info['latitude'];
			$data['longitude'] = $longitude = $area_info['longitude'];
		}
		if (isset($this->request->get['filter_region_code'])) {
			$this->load->model('localisation/region');
			$region_info = $this->model_localisation_region->getRegionByCode($this->request->get['filter_region_code']);
			$data['latitude'] = $latitude = $region_info['latitude'];
			$data['longitude'] = $longitude = $region_info['longitude'];
		}
		if (isset($this->request->get['filter_province_id'])) {
			$this->load->model('localisation/province');
			$province_info = $this->model_localisation_province->getProvince($this->request->get['filter_province_id']);
			$data['latitude'] = $latitude = $province_info['latitude'];
			$data['longitude'] = $longitude = $province_info['longitude'];
		}
		$data['http_server'] = HTTP_SERVER;
		
		
		$data['marker_url'] =  HTTP_SERVER . 'assets/image/marker/';
		$data['plan_edit'] = str_replace('&amp;','&',$this->url->link('client/plan/info', '&plan_id=' , true));
		
		
		/*DATA START HERE*/ 
		

		$data['survey_groups'] = array(
				'survey_audit'=>'SkinCare',
				'survey_adhoc'=>'AdHoc'
		);	
		$count_rot = $count_dat = $count_ktc = 0;
		
		$this->load->model('plan/plan');
		$results = $this->model_plan_plan->getPlans($filter_data);
		
		$this->load->model('project/project_user');
		$data['staffs'] = $staffs = $this->model_project_project_user->getProjectUsersByGroupID($this->config->get('config_user_staff'),$filter_data);
		
		$this->load->model('tool/image');
		
		$this->load->model('survey/survey');
		$surveys = $this->model_survey_survey->getSurveyIndexBy('survey_id');
		
		$plan_surveys = $this->model_plan_plan->getPlansSurveys($filter_data);
		
		
		$stores= array();
		foreach ($results as $result) {
			$plan_id = $result['plan_id'];
			$survey_data = isset($plan_surveys[$plan_id])?$plan_surveys[$plan_id]:array();
	
				
			foreach ($survey_data as $sd) {
				if($sd['survey_id']!=1){
					$icon_marker = 'grey.png';
					if($sd['rating_manual']==1){
						$count_dat++;
						$icon_marker = 'green.png';
					}elseif($sd['rating_manual']==-1){
						$count_rot++;
						$icon_marker = 'red.png';
					} elseif($sd['rating_manual']==-2){
						$count_ktc++;
						$icon_marker = 'grey.png';
					} 
					$stores[$result['store_id']]['maker'] = $icon_marker;
				
					//$ssd['staff'] = isset($staffs[$result['user_id']])?$staffs[$result['user_id']]['fullname']:$result['username'];
					$ssd['rating_manual'] = $sd['rating_manual'];
					$ssd['survey_name'] = $sd['survey_name'];
					$ssd['plan_id'] = $sd['plan_id'];
					$ssd['round_name'] = $sd['round_name'];
					$ssd['rating_result'] = $sd['rating_result'];
					$ssd['survey_total'] = $sd['survey_total'];
					
					$stores[$result['store_id']]['plans'][$sd['plan_survey_id']] = $ssd;
				}
			}
		}
		
		$coordinates = array();
		$locations = array();
		$total = 0;
		
		$http_server = HTTP_SERVER;
			
		
		foreach ($results as $result) {
			if($total<=3000&&isset($stores[$result['store_id']]['plans'])){
				if ($total==0) {
					$data['latitude'] = $result['store_latitude'];
					$data['longitude'] = $result['store_longitude'];
				}
				$total++;
				$plans  = $stores[$result['store_id']]['plans'];
				if(!empty($result['store_image'])&&file_exists(DIR_MEDIA.$result['store_image'])){
					$store_image = $this->model_tool_image->best_fit($result['store_image'], 120, 80);
				} else {
					$store_image = $this->model_tool_image->best_fit('store.jpg', 120, 80);
				}
				$store_image = str_replace($http_server,'',$store_image);
				$maker = 'grey.png';
				
				$coordinates[] = array(
					'marker'=>isset($stores[$result['store_id']]['maker'])?$stores[$result['store_id']]['maker']:$maker,
					'long'       => $result['store_longitude'],
					'lat'       => $result['store_latitude']
				);
				
				$locations[] = array(
					'id'       => $result['store_id'],
					'plans' => $plans,
					'img' => $store_image,
					'name'       => addslashes($result['store_name']),
					'sup'       => $result['customer_user'],
					'code'       => $result['store_code'],
					'add'       => $result['store_address_raw']
				);
			}
		}
		 
		$count = array(
			'dat' => $count_dat,
			'rot' => $count_rot,
			'ktc' => $count_ktc,
		);
		$data['count'] = json_encode($count);	
		$data['locations'] = json_encode($locations);
		$data['coordinates'] = json_encode($coordinates);

		/*DATA END HERE*/ 
		

		$url = $this->_url();
		$data['stores_url'] = str_replace('&amp;','&',$this->url->link('client/map/getStores', $url, true));
		
		
		
		$data['http_server'] = HTTP_SERVER;
		
		$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');

		$this_template = 'client/client_map';
		$this->response->setOutput($this->load->view($this_template, $data));
	}
	
	public function getStores(){

		$json = array();

	
		$filter_data = array();
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$filter_data[$key] = $this->request->get[$key];
			}
		}
		
		$filter_data['filter_qc_status'] = 1;
		
		
		if(!$this->user->isLogged()&&$this->customer->isLogged()){
			$filter_data['customers_manager'] = $this->customer->customers_manager();
		} 
		
		if (isset($this->request->get['filter_customer_parent_id'])) {
			$filter_data['customers_manager'] = $this->customer->getSupsByParentID($this->request->get['filter_customer_parent_id']);
		}
		
		
		
		if(!$this->user->isLogged()&&$this->customer->isLogged()){
			$filter_data['filter_area_codes'] = $this->customer->area_codes();
			$filter_data['filter_region_codes'] = $this->customer->region_codes();
			//$filter_data['filter_province_ids'] = $this->customer->province_ids();
		} 
		
		$json['latitude'] = '10.824725';
		$json['longitude'] = '106.640540';
		

		if (isset($this->request->get['filter_area_code'])) {
			$this->load->model('localisation/area');
			$area_info = $this->model_localisation_area->getAreaByCode($this->request->get['filter_area_code']);
			$json['latitude'] = $latitude = $area_info['latitude'];
			$json['longitude'] = $longitude = $area_info['longitude'];
		}
		if (isset($this->request->get['filter_region_code'])) {
			$this->load->model('localisation/region');
			$region_info = $this->model_localisation_region->getRegionByCode($this->request->get['filter_region_code']);
			$json['latitude'] = $latitude = $region_info['latitude'];
			$json['longitude'] = $longitude = $region_info['longitude'];
		}
		if (isset($this->request->get['filter_province_id'])) {
			$this->load->model('localisation/province');
			$province_info = $this->model_localisation_province->getProvince($this->request->get['filter_province_id']);
			$json['latitude'] = $latitude = $province_info['latitude'];
			$json['longitude'] = $longitude = $province_info['longitude'];
		}
		$count_rot = $count_dat = $count_ktc = 0;
		
		$this->load->model('plan/plan');
		$results = $this->model_plan_plan->getPlans($filter_data);
		
		$this->load->model('project/project_user');
		$data['staffs'] = $staffs = $this->model_project_project_user->getProjectUsersByGroupID($this->config->get('config_user_staff'),$filter_data);
		
		$this->load->model('tool/image');
		
		$this->load->model('survey/survey');
		$surveys = $this->model_survey_survey->getSurveyIndexBy('survey_id');
		
		
		
		$stores= array();
		foreach ($results as $result) {
			$icon_marker = 'grey.png';
			if($result['plan_rating']==1){
				$count_dat++;
				$icon_marker = 'green.png';
			}elseif($result['plan_rating']==-1){
				$count_rot++;
				$icon_marker = 'grey.png';
			} elseif($result['plan_rating']==-2){
				$count_ktc++;
				$icon_marker = 'red.png';
			} 
			$stores[$result['store_id']]['maker'] = $icon_marker;
				
			$survey_ids = isset($result['survey_ids'])?explode(',',$result['survey_ids']):array();
			$survey_name = '';
            foreach ($surveys as $survey) {
            		if(in_array($survey['survey_id'],$survey_ids)&&$survey['survey_id']!=1){
                  		 $survey_name =  $survey['survey_name'];
                 	} 
            } 
                    
			
			if($result['plan_rating']==1){
				$plan_status_text = $this->language->get('text_rating_passed');
			}elseif($result['plan_rating']==-1){
				$plan_status_text = $this->language->get('text_rating_unsuccess');
			}elseif($result['plan_rating']==-2){
				$plan_status_text = $this->language->get('text_rating_not_passed');
			}else{
				$plan_status_text = $this->language->get('text_no_perform');
			}
				
				
			$stores[$result['store_id']]['plans'][$result['plan_id']] = array(
				'id' => $result['plan_id'],
				'name'       => $result['plan_name'],
				'survey'       => $survey_name,
				'status' => $plan_status_text,
				'point' => $result['point_total'],
				'staff'       => isset($staffs[$result['user_id']])?$staffs[$result['user_id']]['fullname']:$result['username']
			);
		}
		
		$json['stores'] = array();
		$total = 0;
		
		$http_server = HTTP_SERVER;
		
		foreach ($results as $result) {
			if(isset($stores[$result['store_id']]['plans'])){
				$total++;
				$plans  = $stores[$result['store_id']]['plans'];
				if(!empty($result['store_image'])&&file_exists(DIR_MEDIA.$result['store_image'])){
					$store_image = $this->model_tool_image->best_fit($result['store_image'], 120, 80);
				} else {
					$store_image = $this->model_tool_image->best_fit('store.jpg', 120, 80);
				}
				$store_image = str_replace($http_server,'',$store_image);
				$maker = 'grey.png';
				$json['stores'][] = array(
					'id'       => $result['store_id'],
					'marker'=>isset($stores[$result['store_id']]['maker'])?$stores[$result['store_id']]['maker']:$maker,
					'plans' => $plans,
					'img' => $store_image,
					'long'       => $result['store_longitude'],
					'lat'       => $result['store_latitude'],
					'name'       => addslashes($result['store_name']),
					'sup'       => $result['customer_user'],
					'code'       => $result['store_code'],
					'add'       => $result['store_address_raw']
				);
			}
		}
		 
		$json['count'] = array(
			'dat' => $count_dat,
			'rot' => $count_rot,
			'ktc' => $count_ktc,
		);
			
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}
}