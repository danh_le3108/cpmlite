<?php
class ControllerToolSearchImage extends Controller {
	
	private $filter_key = array(
			'filter_global',
			'filter_store_id',
			'filter_store_code',
			'filter_user_id',
			'filter_round',
			'filter_is_diff',
			'filter_manual',
			'filter_limit',
			'filter_image_type'
		);
	
	private function _url(){
		$url = '';
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(trim(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8')));
			}
		}
		return $url;
	}
	
	public function index() {
		$this->document->addStyle('assets/plugins/magnific/magnific-popup.css');
		$this->document->addScript('assets/plugins/magnific/jquery.magnific-popup.js');

		$languages= $this->load->language('tool/search_image');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('tool/search_image', '', true)
		);


		$this->load->model('localisation/image_type');
		$data['image_types'] = $this->model_localisation_image_type->getImageTypes();

		$url = $this->_url();

		$data['config_project_images'] = $this->config->get('config_project_images');
		$data['results'] = $data['pagination'] = $data['images'] = array();
		
		$data['is_diffs'] = array(
			'0'=>$this->language->get('text_no'),
			'1'=>$this->language->get('text_yes')
		);
		$data['is_manuals'] = array(
			'0'=>$this->language->get('App'),
			'1'=>$this->language->get('Thủ công')
		);
		
		
		
		$data_images = $images = array();
		
		if (isset($this->request->get['filter_limit'])) {
			$limit = (int)$this->request->get['filter_limit'];
		} else {
			$limit = 50;
		}


		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$filter_data = array(
			'start' => ($page - 1) * $limit,
			'limit' => $limit
		);

		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$filter_data[$key] = trim($this->request->get[$key]);
			} else {
				$filter_data[$key] = NULL;
			}
		}
		if($this->user->isLogged()&&$this->user->getGroupId()==$this->config->get('config_user_sup')){
			$filter_data['filter_user_ids'] = $this->user->user_ids();
		}

		if($this->user->isLogged()&&$this->user->getGroupId()==$this->config->get('config_user_sup')){
			$data['users_manager'] = $filter_data['users_manager'] = $this->user->user_ids();
		}elseif($this->user->isLogged()&&$this->user->getGroupId()!=$this->config->get('config_user_sup')){
			$data['users_manager'] =  $filter_data['users_manager'] = $this->user->users_manager();
		}
			
			
		$this->load->model('project/project_user');

		$data['users'] = $user = $this->model_project_project_user->getProjectUsersByGroupID($this->config->get('config_user_staff'),$filter_data);
		
		$total_images = $this->getTotalImages($filter_data);

		$images = $this->getImages($filter_data);

		$this->load->model('tool/image');
		$popup_path = HTTP_SERVER.'media/';
		
		foreach($images as $img){
			$filename = $img['image_path'].'image/'.$img['plan_id'].'/'.$img['filename'];
			// p($filename,1);
			if(file_exists(DIR_MEDIA.$filename)){
				$img_info = getimagesize(DIR_MEDIA.$filename);
				if(isset($img_info[0])){
					$img['thumb'] = $this->model_tool_image->best_fit($filename, 250, 250);
					$img['popup'] = $popup_path.$filename;
					$img['fullname'] = isset($user[$img['user_id']])?$user[$img['user_id']]['usercode'].' - '.$user[$img['user_id']]['fullname']:'';
					$img['href'] = $this->url->link('plan/plan/edit', 'plan_id='.$img['plan_id'], true);
					$data['images'][] = $img; 
				}
			}
		}
		// p($data['images'],1);
		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $total_images;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('tool/search_image', $url . '&page={page}'. '' , true);

		$data['pagination'] = $pagination->render();
		$data['results'] = $pagination->results();
		
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$data[$key] = trim($this->request->get[$key]);
			} else {
				$data[$key] = NULL;
			}
		}
		
		if (isset($this->request->get['filter_limit'])) {
			$data['filter_limit'] = (int)$this->request->get['filter_limit'];
		} else {
			$data['filter_limit'] = 50;
		}
			
		$data['limits'] =  array(20,50,100,200,500);
		$this->load->model('plan/round');
		$data['rounds'] =  $this->model_plan_round->getRounds();

		$template = 'tool/search_image';
		$this->document->setBreadcrumbs($data['breadcrumbs']);
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
	public function compare() {
		
		$this->document->addStyle('assets/plugins/magnific/magnific-popup.css');
		$this->document->addScript('assets/plugins/magnific/jquery.magnific-popup.js');

		$languages= $this->load->language('tool/search_image');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('tool/search_image', '', true)
		);


		$this->load->model('localisation/image_type');
		$data['image_types'] = $this->model_localisation_image_type->getImageTypes();

		$url = $this->_url();

		$data['config_project_images'] = $this->config->get('config_project_images');
		$data['results'] = $data['pagination'] = $data['images'] = array();
		
		$data['is_diffs'] = array(
			'0'=>$this->language->get('text_no'),
			'1'=>$this->language->get('text_yes')
		);
		$data['is_manuals'] = array(
			'0'=>$this->language->get('App'),
			'1'=>$this->language->get('Thủ công')
		);
		
		
		
		$data_images = $images = array();
		
		
			if (isset($this->request->get['filter_limit'])) {
				$limit = (int)$this->request->get['filter_limit'];
			} else {
				$limit = 200;
			}



			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else {
				$page = 1;
			}

			$filter_data = array(
				'start' => ($page - 1) * $limit,
				'limit' => $limit
			);

			foreach($this->filter_key as $key){
				if (isset($this->request->get[$key])) {
					$filter_data[$key] = trim($this->request->get[$key]);
				} else {
					$filter_data[$key] = NULL;
				}
			}
			if($this->user->isLogged()&&$this->user->getGroupId()==$this->config->get('config_user_sup')){
				$filter_data['filter_user_ids'] = $this->user->user_ids();
			}

			if($this->user->isLogged()&&$this->user->getGroupId()==$this->config->get('config_user_sup')){
				$data['users_manager'] = $filter_data['users_manager'] = $this->user->user_ids();
			}elseif($this->user->isLogged()&&$this->user->getGroupId()!=$this->config->get('config_user_sup')){
				$data['users_manager'] =  $filter_data['users_manager'] = $this->user->users_manager();
			}
			
			
			$this->load->model('project/project_user');
	
			$data['users'] = $user = $this->model_project_project_user->getProjectUsersByGroupID($this->config->get('config_user_staff'),$filter_data);
			
			

			$total_images = $this->getTotalImages($filter_data);

			$images = $this->getImages($filter_data);

			$this->load->model('tool/image');
			$popup_path = HTTP_SERVER.'media/';
			
			$data['rounds'] =  array();
		
			foreach($images as $img){
				$filename = $img['image_path'].'image/'.$img['filename'];
				if(file_exists(DIR_MEDIA.$filename)){
					$data['rounds'][$img['round_name']] = $img['round_name'];
					$img_info = getimagesize(DIR_MEDIA.$filename);
					if(isset($img_info[0])){
						$img['thumb'] = $this->model_tool_image->best_fit($filename, 250, 250);
						$img['popup'] = $popup_path.$filename;
						$img['fullname'] = isset($user[$img['user_id']])?$user[$img['user_id']]['usercode'].' - '.$user[$img['user_id']]['fullname']:'';
						$img['href'] = $this->url->link('plan/plan/edit', 'plan_id='.$img['plan_id'], true);
						$data['images'][] = $img; 
					}
				}
			}
			$pagination = new Pagination($this->language->get('text_pagination'));
			$pagination->total = $total_images;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->url = $this->url->link('tool/search_image/compare', $url . '&page={page}'. '' , true);

			$data['pagination'] = $pagination->render();
			$data['results'] = $pagination->results();
		
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$data[$key] = trim($this->request->get[$key]);
			} else {
				$data[$key] = NULL;
			}
		}
		
		if (isset($this->request->get['filter_limit'])) {
			$data['filter_limit'] = (int)$this->request->get['filter_limit'];
		} else {
			$data['filter_limit'] = 200;
		}
			
		$data['limits'] =  array(20,50,100,200,500);
		
		$this->load->controller('api/online');

		$template = 'tool/compare_image';
		$this->document->setBreadcrumbs($data['breadcrumbs']);
		$this->response->setOutput($this->load->view($template, $data));
	}

	private function getImages($data = array()){
		$sql = "SELECT si.*, pl.image_path, pl.time_checkin FROM ".PDB_PREFIX."plan_images si LEFT JOIN `".PDB_PREFIX."plan` pl ON(pl.plan_id=si.plan_id) WHERE 1";
		
		$filter_global = array(
			'si.store_code',
			'si.filename',
			'si.round_name'
		);
		if (!empty($data['filter_global'])) {
			$sql .= " AND (";
			foreach ($filter_global as $filter) {
				$gimplode[] = " LCASE(".$filter.") LIKE '%" . $this->pdb->escape(utf8_strtolower($data['filter_global'])) . "%'";
			}
			$sql .= " " . implode(" OR ", $gimplode) . "";
			$sql .= ")";
		}
		
		if(isset($data['filter_user_id']) && !empty($data['filter_user_id'])){
			$sql .= " AND si.user_id = '".(int)$data['filter_user_id']."'";
		}
		if (isset($data['filter_user_ids'])&&!empty($data['filter_user_ids'])) {
			$implode[] = "si.user_id IN('".implode("','",$data['filter_user_ids'])."')";
		}
		if(isset($data['filter_round']) && !empty($data['filter_round'])){
			$sql .= " AND si.round_name = '".$this->pdb->escape($data['filter_round'])."'";
		}
		if(isset($data['filter_store_code']) && !empty($data['filter_store_code'])){
			$sql .= " AND si.store_code = '".$this->pdb->escape($data['filter_store_code'])."'";
		}
		if(isset($data['filter_image_type']) && !empty($data['filter_image_type'])){
			$sql .= " AND si.image_type_id = '".(int)$data['filter_image_type']."'";
		}
		if(isset($data['filter_store_id']) && !empty($data['filter_store_id'])){
			$sql .= " AND si.store_id = '".(int)$data['filter_store_id']."'";
		}
		if(isset($data['filter_is_diff'])){
			$sql .= " AND si.is_diff = '".(int)$data['filter_is_diff']."'";
		}
		if(isset($data['filter_manual']) && !empty($data['filter_manual'])){
			$sql .= " AND si.manual = '".(int)$data['filter_manual']."'";
		}
 
		if (isset($data['users_manager'])&& !empty($data['users_manager'])) {
			$sql .= " AND si.`user_id` IN('".implode("','",$data['users_manager'])."')";
		}
		$sql .= " ORDER BY si.date_added DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->pdb->query($sql);
		// p($query->rows,1);
		return $query->rows;
	}

	public function getTotalImages($data = array()){
		$sql = "SELECT COUNT(*) AS total FROM ".PDB_PREFIX."plan_images si WHERE 1";

		$filter_global = array(
			'si.store_code',
			'si.filename',
			'si.round_name'
		);
		if (!empty($data['filter_global'])) {
			$sql .= " AND (";
			foreach ($filter_global as $filter) {
				$gimplode[] = " LCASE(".$filter.") LIKE '%" . $this->pdb->escape(utf8_strtolower($data['filter_global'])) . "%'";
			}
			$sql .= " " . implode(" OR ", $gimplode) . "";
			$sql .= ")";
		}
		if(isset($data['filter_user_id']) && !empty($data['filter_user_id'])){
			$sql .= " AND si.user_id = '".(int)$data['filter_user_id']."'";
		}
		if (isset($data['filter_user_ids'])&&!empty($data['filter_user_ids'])) {
			$implode[] = "si.user_id IN('".implode("','",$data['filter_user_ids'])."')";
		}
		if(isset($data['filter_round']) && !empty($data['filter_round'])){
			$sql .= " AND si.round_name = '".$this->pdb->escape($data['filter_round'])."'";
		}
		if(isset($data['filter_store_code']) && !empty($data['filter_store_code'])){
			$sql .= " AND si.store_code = '".$this->pdb->escape($data['filter_store_code'])."'";
		}
		if(isset($data['filter_image_type']) && !empty($data['filter_image_type'])){
			$sql .= " AND si.image_type_id = '".(int)$data['filter_image_type']."'";
		}
		if(isset($data['filter_store_id']) && !empty($data['filter_store_id'])){
			$sql .= " AND si.store_id = '".(int)$data['filter_store_id']."'";
		}
		if(isset($data['filter_is_diff'])){
			$sql .= " AND si.is_diff = '".(int)$data['filter_is_diff']."'";
		}
		if(isset($data['filter_manual']) && !empty($data['filter_manual'])){
			$sql .= " AND si.manual = '".(int)$data['filter_manual']."'";
		}

 
		if (isset($data['users_manager'])&& !empty($data['users_manager'])) {
			$sql .= " AND si.`user_id` IN('".implode("','",$data['users_manager'])."')";
		}
		$query = $this->pdb->query($sql);

		return $query->row['total'];
	}
}