<?php
class ControllerStoreStore extends Controller {
	private $no_filter_key = array('_url','route','page');

	private $error = array();

	public function index() {
		$this->load->model('store/store_type');
		$this->load->model('localisation/province');
		$this->load->model('localisation/region');
		$this->load->model('store/store');

		$data['store_types'] = $this->model_store_store_type->getStoreTypes();
		$data['provinces'] = $this->model_localisation_province->getProvinces();
		$data['regions'] = $this->model_localisation_region->getRegions();
		// $data['chanels'] = $this->model_store_store->getChanels();
		$languages= $this->load->language('store/store');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'icon' => 'fa-dashboard',
			'href' => $this->url->link('common/home')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('store/store')
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);
		$this->document->setTitle($this->language->get('heading_title'));

		if (!empty($this->session->data['error_warning'])) {
			$data['error_warning'] = $this->session->data['error_warning'];
			unset($this->session->data['error_warning']);
		}
		if (!empty($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}

		$url = $this->_url();
		$data['url'] = $this->url->link('store/store/getStores', $url);
		$data['hasAdd'] = $this->document->hasPermission('add', 'store/store');
		$data['import_store'] = $this->load->controller('excel/store_import', $url);
		$this->document->addStyle('assets/plugins/magnific/magnific-popup.css'); 
		$this->document->addScript('assets/plugins/magnific/jquery.magnific-popup.js');
		$template = 'store/index';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}

	public function getStores() {
		$languages= $this->load->language('store/store');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		
		$page = !empty($this->request->get['page']) ? $this->request->get['page'] : 1;
		$this->load->model('store/store');
		$filter_data = $this->request->get;
		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $this->model_store_store->countTotal($filter_data);
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_project');
		$pagination->url = $this->url->link('store/store/getStores', $this->_url() . '&page={page}');

		$data['results'] = $pagination->results();
		$data['pagination'] = $pagination->render();
		
		$filter_data['start'] = ($page - 1) * $this->config->get('config_limit_project');
		$filter_data['limit'] = $this->config->get('config_limit_project');
		$data['stores'] = $this->model_store_store->getStores($filter_data);
		$this->load->model('tool/image');
		$data['model_tool_image'] = $this->model_tool_image;
		$this->load->model('store/store_type');
		$data['store_types'] = $this->model_store_store_type->getAllTypes();
		$data['thumb'] = $this->model_tool_image->resize('store.jpg', 60, 60);
		$data['hasDel'] = $this->document->hasPermission('delete', 'store/store');
		$template = 'store/store_list';
		echo $this->load->view($template, $data);
	}

	public function edit() {
		if ($this->document->hasPermission('edit','store/store') && !empty($this->request->post) && $this->validateForm()) {
			$store = $this->request->post;
			$store_address_raw = array();
			if (!empty($this->request->post['store_address'])) {
				$store_address_raw[] = $this->request->post['store_address'];
			}
			if (!empty($this->request->post['store_place'])) {
				$store_address_raw[] = $this->request->post['store_place'];
			}
			if (!empty($this->request->post['store_ward'])) {
				$store_address_raw[] = $this->request->post['store_ward'];
			}	
			if (!empty($this->request->post['store_district'])) {
				$store_address_raw[] = $this->request->post['store_district'];
			}
			$store_address_raw[] = $this->request->post['store_province'];
			$store['store_address_raw'] = implode(', ', $store_address_raw);
			// p($store,1);
			$this->load->model('store/store');
			$this->load->model('history/history');
			$history = array(
				'table_name' => 'store',
				'user_id' => $this->user->getId(),
				'user_update' => $this->user->getUserName(),
				'controller' => 'store/store/edit',
				'table_primary_id' => $this->request->get['store_id'],
				'data' => json_encode($store),
				'old_data' => json_encode($this->model_store_store->getStore($this->request->get['store_id']))
			);
			$this->model_history_history->add($history);
			$this->model_store_store->updateStore($this->request->get['store_id'], $store);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('store/store'));
		} else {
			$this->getForm();
		}
	}

	public function add() {
		if ($this->document->hasPermission('edit','store/store') && !empty($this->request->post) && $this->validateForm()) {
			$store = $this->request->post;
			$store_address_raw = array();
			if (!empty($this->request->post['store_address'])) {
				$store_address_raw[] = $this->request->post['store_address'];
			}
			if (!empty($this->request->post['store_place'])) {
				$store_address_raw[] = $this->request->post['store_place'];
			}
			if (!empty($this->request->post['store_ward'])) {
				$store_address_raw[] = $this->request->post['store_ward'];
			}	
			if (!empty($this->request->post['store_district'])) {
				$store_address_raw[] = $this->request->post['store_district'];
			}
			$store_address_raw[] = $this->request->post['store_province'];
			$store['store_address_raw'] = implode(', ', $store_address_raw);
			$this->load->model('store/store');
			$this->model_store_store->addStore($store);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('store/store'));
		} else {
			$this->getForm();
		}
	}

	public function getForm() {
		$this->load->model('store/store');
		$this->load->model('store/store_type');
		$this->load->model('localisation/province');
		$this->load->model('localisation/region');
		$data['store_types'] = $this->model_store_store_type->getStoreTypes();
		$data['store_chanels'] = $this->model_store_store->getChanels();
		$data['provinces'] = $this->model_localisation_province->getProvinces();
		$data['regions'] = $this->model_localisation_region->getRegions();
		$languages= $this->load->language('store/store');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'icon' => 'fa-dashboard',
			'href' => $this->url->link('common/home')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('store/store')
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);
		$this->document->setTitle($this->language->get('heading_title'));

		if (!empty($this->session->data['error_warning'])) {
			$data['error_warning'] = $this->session->data['error_warning'];
			unset($this->session->data['error_warning']);
		}
		if (!empty($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}

		if (!empty($this->request->post)) {
			$data['store'] = $this->request->post;
		} elseif (!empty($this->request->get['store_id'])) {
			$data['store'] = $this->model_store_store->getStore($this->request->get['store_id']);
		}
		$this->load->model('tool/image');
		$data['model_tool_image'] = $this->model_tool_image;
		$data['hasAdd'] = $this->document->hasPermission('add','store/store');
		$data['hasEdit'] = $this->document->hasPermission('edit','store/store');
		$data['error'] = $this->error;

		$this->document->addStyle('assets/plugins/magnific/magnific-popup.css'); 
		$this->document->addScript('assets/plugins/magnific/jquery.magnific-popup.js');
        $this->document->addStyle('assets/plugins/storelocator/admin/storelocator.css');
        $this->document->addScript('assets/plugins/storelocator/admin/storelocator.js');
		$locator_protocol = $this->config->get('locator_protocol');
        $this->document->addScript('https://maps.googleapis.com/maps/api/js?key=' . $this->config->get('config_google_api_key'));
        $this->document->addScript('https://www.google.com/jsapi');
		$template = 'store/store_form';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}

	public function delete() {
		if ($this->document->hasPermission('delete', 'store/store')) {
			$this->load->model('store/store');
			$this->model_store_store->deleteStore($this->request->get['store_id']);
		}
	}

	private function _url(){
		$url = '';
		foreach ($this->request->get as $key => $value) {
			if (!in_array($key, $this->no_filter_key)) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($value, ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}

	private function validateForm() {
		$this->load->language('store/store');
		if (empty($this->request->post['store_name'])) {
			$this->error['error_store_name'] = $this->language->get('text_error_store_name');
		}
		if (empty($this->request->post['store_code'])) {
			$this->error['error_store_code'] = $this->language->get('text_error_store_code');
		} else {
			$this->load->model('store/store');
			$store_info = $this->model_store_store->getStoreByStoreCode($this->request->post['store_code']);
			$store_id = isset($this->request->get['store_id']) ? $this->request->get['store_id'] : '';
			if(!empty($store_info) && $store_info['store_id'] != $store_id && $store_info['store_code'] == $this->request->post['store_code']) {
				$this->error['error_store_code'] = $this->language->get('text_error_exists_store_code');
			}
		}
		if ($this->error) {
			return false;
		} else {
			return true;
		}
	}

	public function autocomplete() {
		$filter_data = array(
			'start'       => 0,
			'limit'       => 20
		);
		if (!empty($this->request->get['filter_global'])) {
			$filter_data['filter_global'] = $this->request->get['filter_global'];
			$this->load->model('store/store');
			$stores = $this->model_store_store->getStores($filter_data);
			echo json_encode($stores);
		}
	}

}