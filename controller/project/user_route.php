<?php
class ControllerProjectUserRoute extends Controller{
	private $error = array();

	private $field_data = array(
			'plan_id'=>'',
			'plan_name'=>''
		);

	private $filter_key = array(
		'filter_user_id',
		'filter_date_checkin',
		'filter_round_name'
	);

	private function _url(){
		$url = '';
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}
	
	public function index() {
		$languages = $this->load->language('project/user_route');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		
		foreach ($this->filter_key as $key) {
			if (!empty($this->request->get[$key])) {
				$data[$key] = $this->request->get[$key];
 			} else {
				$data[$key] = '';
			}
		}

		$this->load->model('project/project_user');
		$data['staffs'] = $this->model_project_project_user->getProjectUsersByGroupID($this->config->get('config_user_staff'), array('users_manager' => $this->user->users_manager()));

		$this->load->model('plan/plan');
		$data['rounds'] = $this->model_plan_plan->countTotal('', 'round_name');
		$filter_data = array();
		// $filter_data['plan_status'] = 1;
		$plans = array();
		if (!empty($this->request->get['filter_round_name'])) {
			$filter_data['filter_round_name'] = $this->request->get['filter_round_name'];
		}
		$data['count_staff'] = $this->model_plan_plan->countTotal($filter_data, 'user_id');
		if (!empty($this->request->get['filter_user_id'])) {
			$filter_data['filter_user_id'] = $this->request->get['filter_user_id'];
			$data['dates'] = $this->model_plan_plan->countTotal($filter_data, 'DATE(time_checkin)');
			unset($data['dates']['0000-00-00']);
			if (!empty($this->request->get['filter_date_checkin'])) {
				$filter_data['filter_date_checkin'] = $this->request->get['filter_date_checkin'];
			}
			$filter_data['sort'] = 'time_checkin ASC';
			$plans = $this->model_plan_plan->getPlans($filter_data);
		}
		$data['plans'] = array();
		if (!empty($plans)) {
			$this->load->model('tool/image');
			$avatar = $this->model_tool_image->resize('avatar.jpg', 80, 80);
			$dStart = !empty($plans[0]) ? new DateTime($plans[0]['time_checkin']) : '';
			
			foreach ($plans as $p) {
				if ($p['time_checkin'] != '0000-00-00 00:00:00') {
					$plan = $p;
					if (is_file(DIR_MEDIA.$plan['store_image'])) {
						$store_image =$this->model_tool_image->resize($plan['store_image'], 80, 80);
						$popup = HTTP_SERVER.'media/'.$plan['store_image'];
					} else {
						$store_image = $avatar;
						$popup = '';
					}
					$plan['store_image'] =  $store_image;
					$plan['popup'] =  $popup;

					$dEnd  = new DateTime($plan['time_checkin']);
		           	$dDiff = $dStart->diff($dEnd);
		           	$day = $dDiff->d > 0 ? $dDiff->d . ' ngày ' : '';
		           	$hour = $dDiff->h > 0 ? $dDiff->h . ' giờ ' : '';
		           	$minute = $dDiff->i > 0 ? $dDiff->i . ' phút ' : '';
		           	$second = $dDiff->s > 0 ? $dDiff->s : 0;
		           	$dStart = new DateTime($plan['time_checkin']);
		           	$plan['time_diff'] = $day . $hour . $minute . $second . ' giây sau';
		           	$data['plans'][] = $plan;
				}
			}
		}
		$this->document->addStyle('assets/plugins/magnific/magnific-popup.css');
		$this->document->addScript('assets/plugins/magnific/jquery.magnific-popup.js');
		$this->document->setTitle($this->language->get('heading_title'));
		$template = 'project/user_route';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
}