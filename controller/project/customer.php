<?php
class ControllerProjectCustomer extends Controller {
	private $error = array();
	private function _url(){
		$url = '';
		return $url;
	}
	
  public function download() {
		$url = $this->_url();
		
		$languages = $this->load->language('project/customer');
			$this->load->model('project/customer');
			
			if($this->customer->isLogged()){
				$username =$this->customer->getUsername();
			}else{
				$username =$this->user->getUsername();
			}
			$results = $this->staff_customers();

			
			$all_customers =array();
			
			$all_customers[] =  array(
                'A' => $this->language->get('text_user'),
                'B' => $this->language->get('text_username'),
                'C' => $this->language->get('text_customer_group'),
                'D' => $this->language->get('text_email'),
                'E' => $this->language->get('text_telephone')
			);
			
			foreach ($results as $result) {
				$all_customers[] = array(
					'A'       => $result['fullname'],
					'B'       => $result['username'],
					'C'       => $result['level_name'],
					'D'       => $result['email'],
					'E'       => $result['telephone']
				);
			}

			$export_data = array(
				'template_path'=>DIR_MEDIA . 'files/'.$this->config->get('config_project_folder').'/export/online/',
				'template_name'=>'Customers-Export-by-'.$username.'-at-'. date('Ymd-His'),
				'heading_style'=>array('colrow'=>'A1:E1','color'=>'#333333','bg'=>'#9cc3e6'),
				'template_data'=>$all_customers
			
			);
			$this->excel->export($export_data);
		
			$this->response->redirect($this->url->link('client/online_history',$url , true));
	}
	private function staff_customers() {
		$this->load->model('tool/image');
		$thumb = $this->model_tool_image->resize('avatar.jpg', 100, 100);
		$this->load->model('project/customer');
		$results = $this->model_project_customer->getProjectCustomers();
		$users = array();
		 
		foreach($results as $result){
			$imag_path = 'files/users/' . $result['username'].'/';
			$img_href = HTTP_SERVER.'media/files/users/' . $result['username'].'/';
			if (!empty($result) && $result['image'] && is_file(DIR_MEDIA .$imag_path. $result['image'])) {
				$avatar[$result['customer_user_id']] = $this->model_tool_image->resize($imag_path.$result['image'], 100, 100);
			}else{
				$avatar[$result['customer_user_id']] = $thumb;
			}
			$users[$result['customer_user_id']] = array(
				'customer_user_id' => $result['customer_user_id'],
				'thumb' => $avatar[$result['customer_user_id']],
				'real_token' => $result['real_token'],
				'username' => $result['username'],
				'fullname' => $result['fullname'],
				'email' => $result['email'],
				'telephone' => $result['telephone'],
				'customer_level_id' => $result['customer_level_id'],
				'customer_parent_id' => $result['customer_parent_id'],
				'parent_fullname' => $result['parent_fullname'],
				'status' => $result['status'],
				'user_update' =>  $result['user_update'],
				'status' =>  $result['status'],
				'date_added' =>  $result['date_added'],
				'text_status' => $this->language->get(($result['status']==1)?'text_active':'text_deactive'),
				'level_name' => $result['level_name'],
				'region_code' => $result['region_code']
			);
		}

		return $users;
	}
	public function index() {
		
		$data['config_customer_top'] = $this->config->get('config_customer_top');
		$data['config_customer_rsm'] = $this->config->get('config_customer_rsm');
		$data['config_customer_asm'] = $this->config->get('config_customer_asm');
		$data['config_customer_sup'] = $this->config->get('config_customer_sup');

		$languages = $this->load->language('project/customer');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('project/customer');


		$data['download'] = $this->url->link('project/customer/download', '', true);
		
		$url = $this->_url();

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('project/customer',  $url, true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);

		$this->load->model('localisation/region');
		$data['regions'] = $this->model_localisation_region->getRegions();
		$data['staff_customers'] = $this->staff_customers();
		$this->load->model('project/customer_level');
		$data['customer_groups'] = $this->model_project_customer_level->getCustomerLevels();
		$template = 'project/customer';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));

	}
	public function customer_list() {
		$languages = $this->load->language('project/customer');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}

		$data['staff_customers'] = $this->staff_customers();
		$template = 'project/customer_list';
		$this->response->setOutput($this->load->view($template, $data));

	}
	public function load_group() {
		
		if (isset($this->request->get['customer_level_id'])) {
			$data['filter_group_id'] = $this->request->get['customer_level_id'];
			$data['config_customer_sup'] = $this->config->get('config_customer_sup');
			$data['config_customer_top'] = $this->config->get('config_customer_top');
	
			$languages = $this->load->language('project/customer');
			foreach($languages as $key=>$value){
					$data[$key] = $value;
			}
	
			$this->load->model('tool/image');
			$data['thumb'] = $thumb = $this->model_tool_image->resize('avatar.jpg', 100, 100);
	
		$data['staff_customers'] = $this->staff_customers();
	
		$this->load->model('project/customer_level');
		$data['customer_groups'] = $this->model_project_customer_level->getCustomerLevels();
	
	
			$template = 'project/customer_ajax';
			$this->response->setOutput($this->load->view($template, $data));
		}else{
			echo 'Không có tham số yêu cầu!';
		}
	}
	
	public function remove_customer() {
		$json = array();
		if (isset($this->request->get['customer_user_id'])&&$this->document->hasPermission('edit', 'project/customer')) {
			$this->load->model('project/customer');
			$this->model_project_customer->removeCustomer($this->request->get['customer_user_id']);
			$json['success'] = 'Success!';
		}else {
			$json['error'] = 'Nhân viên không tồn tại';
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function add_customer() {
		$json = array();
		if (isset($this->request->post['customer_user_id']) && $this->request->server['REQUEST_METHOD'] == 'POST' &&$this->document->hasPermission('edit', 'project/customer')) {
			$this->load->model('project/customer');
			$this->model_project_customer->updateCustomer($this->request->post['customer_user_id'], $this->request->post);
			$json['success'] = 'Success!';
		}else {
			$json['error'] = 'Nhân viên không tồn tại';
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function edit_customer_user() {
		if (isset($this->request->get['customer_user_id'])) {
			$data['customer_user_id'] = $customer_user_id = $this->request->get['customer_user_id'];
			
			
			$languages = $this->load->language('project/customer');
			foreach($languages as $key=>$value){
					$data[$key] = $value;
			}
			$this->load->model('project/customer');
			
			$data['info'] = $this->model_project_customer->getCustomer($customer_user_id);
			
		$this->load->model('project/customer_level');
		$data['customer_groups'] = $this->model_project_customer_level->getCustomerLevels();
			
			$template = 'project/customer_form';
			$this->response->setOutput($this->load->view($template, $data));
		}else{
			echo '<div class="alert alert-info">Không có tham số yêu cầu!</div>';
		}
	}
	
	/*LEVEL*/ 
	
	public function new_level(){
		$languages = $this->load->language('project/customer');
		$json = array();
		
		if($this->user->hasPermission('add', 'project/customer')&&($this->request->server['REQUEST_METHOD'] == 'POST')) {
			if ((utf8_strlen($this->request->post['level_name']) < 2) || (utf8_strlen($this->request->post['level_name']) > 64)) {
				$json['error'] = $this->language->get('text_error_name');
			}else{
				$this->load->model('project/customer_level');
				$this->model_project_customer_level->addUserGroup( $this->request->post);
	
				$json['success'] = $this->language->get('text_success');
				$json['redirect'] = str_replace('&amp;', '&', $this->url->link('project/customer', '', true));
			}
		}else{
				$json['error'] =  $this->language->get('text_error_permission');
			
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function edit_customer_level() {
		if (isset($this->request->get['customer_level_id'])) {
			$data['customer_level_id'] = $customer_level_id = $this->request->get['customer_level_id'];
			
			
			$languages = $this->load->language('project/customer');
			foreach($languages as $key=>$value){
					$data[$key] = $value;
			}
			$this->load->model('project/customer_level');
			
			$data['info'] = $this->model_project_customer_level->getCustomerLevel($customer_level_id);
			
			$data['customer_levels'] = $this->model_project_customer_level->getCustomerLevels();
			
			$template = 'project/customer_level_form';
			$this->response->setOutput($this->load->view($template, $data));
		}else{
			echo '<div class="alert alert-info">Không có tham số yêu cầu!</div>';
		}
	}
	public function save_customer_level() {
		
		$languages = $this->load->language('project/customer');
		$json = array();
		
		if($this->user->hasPermission('add', 'project/customer')&&($this->request->server['REQUEST_METHOD'] == 'POST')&&isset($this->request->get['customer_level_id'])) {
			if ((utf8_strlen($this->request->post['level_name']) < 2) || (utf8_strlen($this->request->post['level_name']) > 64)) {
				$json['error'] = $this->language->get('text_error_name');
			}else{
				$this->load->model('project/customer_level');
				$this->model_project_customer_level->editUserGroup($this->request->get['customer_level_id'], $this->request->post);
	
				$json['success'] = $this->language->get('text_success');
			}
		}else{
			$json['error'] =  $this->language->get('text_error_permission');
			
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}
	/*CUSTOMER*/ 
	public function save_customer_user() {
		
		$languages = $this->load->language('project/customer');
		$json = array();

				$this->load->model('project/customer');
		if(($this->request->server['REQUEST_METHOD'] == 'POST')&&isset($this->request->get['customer_user_id'])) {
			
			$customer_user_id = $this->request->get['customer_user_id'];
			
			if (isset($this->request->post['password'])&&!empty($this->request->post['password'])) {
				if ((utf8_strlen($this->request->post['password']) < 3) || (utf8_strlen($this->request->post['password']) > 20)) {
					$json['error'] =  $this->language->get('text_error_password');
				}
				if (!isset($this->request->post['confirm'])) {
					$json['error'] =  $this->language->get('text_error_confirm');
				}
				if ($this->request->post['password'] != $this->request->post['confirm']) {
					$json['error'] =  $this->language->get('text_error_confirm');
				}
				
			}
			
			if ((utf8_strlen($this->request->post['fullname']) < 3) || (utf8_strlen(trim($this->request->post['fullname'])) > 64)) {
				$json['error'] = $this->language->get('text_error_fullname');
			}
	/*
			if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
				$json['error'] = $this->language->get('text_error_email');
			}*/ 
	
			$username_info = $this->model_project_customer->getCustomerByUsername($this->request->post['username']);
	
			if ($username_info && ($customer_user_id  != $username_info['customer_user_id'])) {
				$json['error'] = $this->language->get('text_error_username_exists');
			}
			
			/*$email_info = $this->model_project_customer->getCustomerByEmail($this->request->post['email']);
	
			if ($email_info && ($customer_user_id  != $email_info['customer_user_id'])) {
				$json['error'] =  $this->language->get('text_error_exists');
			}*/ 
			
			if(empty($json['error'])){
				$this->model_project_customer->editCustomer($customer_user_id,$this->request->post);
				$json['success'] =  $this->language->get('text_success');
			}

		}else{
			$json['error'] =  $this->language->get('text_error_permission');
			
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function new_customer(){
		$languages = $this->load->language('project/customer');
		$json = array();

				$this->load->model('project/customer');
		if(($this->request->server['REQUEST_METHOD'] == 'POST')) {
			
			if (isset($this->request->post['password'])) {
				if ((utf8_strlen($this->request->post['password']) < 3) || (utf8_strlen($this->request->post['password']) > 20)) {
					$json['error'] =  $this->language->get('text_error_password');
				}
	
				if ($this->request->post['password'] != $this->request->post['confirm']) {
					$json['error'] =  $this->language->get('text_error_confirm');
				}
			}else{
				$json['error'] =  $this->language->get('text_error_password');
			}
			
			if ((utf8_strlen($this->request->post['fullname']) < 3) || (utf8_strlen(trim($this->request->post['fullname'])) > 64)) {
				$json['error'] = $this->language->get('text_error_fullname');
			}
	
			//if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
				//$json['error'] = $this->language->get('text_error_email');
			//}
	
			$username_info = $this->model_project_customer->getCustomerByUsername($this->request->post['username']);
	
			if ($username_info) {
				$json['error'] = $this->language->get('text_error_username_exists');
			}
			//$email_info = $this->model_project_customer->getCustomerByEmail($this->request->post['email']);
	
			//if ($email_info) {
				//$json['error'] =  $this->language->get('text_error_exists');
			//}
			if(empty($json['error'])){
				$this->model_project_customer->addCustomer($this->request->post);
					$json['success'] =  $this->language->get('text_success');
					$json['redirect'] = str_replace('&amp;', '&', $this->url->link('project/customer', '', true));
			}

		}else{
				$json['error'] =  $this->language->get('text_error_permission');
			
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function autocomplete() {
		$json = array();

			$filter_data = array(
			'sort'        => 'fullname',
			'order'       => 'ASC',
			'start'       => 0,
			'limit'       => 10
			);
			$filter_data['filter_global'] = isset($this->request->get['filter_global'])?$this->request->get['filter_global']:'';
			$filter_data['filter_fullname'] = isset($this->request->get['filter_fullname'])?$this->request->get['filter_fullname']:'';
			$filter_data['filter_username'] = isset($this->request->get['filter_username'])?$this->request->get['filter_username']:'';
			$filter_data['filter_email'] = isset($this->request->get['filter_email'])?$this->request->get['filter_email']:'';

			$this->load->model('project/customer');


			$results = $this->model_project_customer->getCustomers($filter_data);
			
			foreach ($results as $result) {
				$json[] = array(
					'customer_user_id'       => $result['customer_user_id'],
					'username'         => $result['username'],
					'fullname'              => strip_tags(html_entity_decode($result['fullname'], ENT_QUOTES, 'UTF-8')),
					'email'             => $result['email'],
					'telephone'         => $result['telephone']
				);
			}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['fullname'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function updateRegion() {
		if (!empty($this->request->get) && $this->document->hasPermission('edit','project/customer')) {
			$sql = "UPDATE " . DB_PREFIX . "customer_user SET region_code = '" . $this->pdb->escape($this->request->get['region_code']) . "' WHERE customer_user_id = " . (int)$this->request->get['customer_user_id'];

			$this->pdb->query($sql);
		}
	}
}
