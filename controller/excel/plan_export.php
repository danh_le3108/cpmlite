<?php

class ControllerExcelPlanExport extends Controller {
	private $milo_export_id = 3;
	private $no_filter_key = array('_url','route');
	private $round = array();

	private function _url(){
		$url = '';
		foreach ($this->request->get as $key => $value) {
			if (!in_array($key, $this->no_filter_key)) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($value, ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}

	public function export_detail() {
		if ($this->document->hasPermission('add','excel/plan_export')) {
			if (empty($this->request->get['filter_group_id'])) {
				$this->session->data['error_warning'] = "Vui lòng mức trưng bày!";
				$this->response->redirect($this->url->link('plan/plan'));
			}

			$this->load->model('plan/plan');
			$filter = $this->request->get;
			$plans = $this->model_plan_plan->getPlans($filter);
			if (!empty($plans)) {
				set_time_limit(1800);

				$this->load->model('catalog/group');
				$group_list = $this->model_catalog_group->getGroups();
				$project_name = '';
				$group_codes = array();
				foreach ($group_list as $g) {
					if ($g['group_id'] == $this->request->get['filter_group_id']) {
						$project_name = $g['group_name'];
					}
					if ($g['parent_group_id'] == $this->request->get['filter_group_id']) {
						$group_codes[] = $g['group_code'];
					}
				}
				
				if ($this->request->get['filter_group_id'] == 1) {
					$template_id = $this->milo_export_id;
				} else {
					$this->session->data['error_warning'] = "Không có file excel export!";
					$this->response->redirect($this->url->link('plan/plan'));
				}
				
				$template_info = $this->excel->getTemplate($template_id,0);

				$template_file = DIR_MEDIA.$template_info['file_path'];

				$settings = $template_info['settings'];

				if(!file_exists($template_file)){
					return;
				}

				$template_name = 'Export_detail_-'.$project_name.'-by-'.$this->user->getUsername().'-at-'. date('Ymd-His').'.xlsx';

				$file_name = ucwords(str_replace(' ', '-', $template_name));

				$folder = isset($data['folder']) ? $data['folder'] .'/' : '';
				
				$template_path = DIR_MEDIA.'files/'. $this->config->get('config_project_folder') .'/export/'.$folder;

				if(!is_dir($template_path)){
					@mkdir($template_path, 0777, true);
				}

				$export_file = $template_path.$file_name;

				if(file_exists($export_file)){
					@unlink($export_file);
				}
			
				if(copy($template_file, $export_file)) {
					try {
						$inputFileType = PHPExcel_IOFactory::identify($export_file);
						$objReader = PHPExcel_IOFactory::createReader($inputFileType);
						$workbook = $objReader->load($export_file);
						$worksheet  = $workbook->setActiveSheetIndex($settings['sheet_index']);
						$row = isset($settings['start_row']) ? $settings['start_row'] : 2;
				
						$this->load->model('project/project_user');
						$users = $this->model_project_project_user->getProjectUsers();
			
						$this->load->model('localisation/reason_unsuccess');
						$reasons = $this->model_localisation_reason_unsuccess->getReasons(1);

						$this->load->model('catalog/attribute');
						$this->load->model('catalog/attribute_data');

						$attrs = $this->model_catalog_attribute->getAttributes(array('sort'=>'g.sort_order','g.parent_group_id'=>$this->request->get['filter_group_id']));

						foreach ($attrs as &$attr) {
							if ($attr['input'] == 'select') {
								$options = json_decode($attr['options']);
								$attr['options'] = array();
								foreach ($options as $op) {
									$attr['options'][$op->id] = $op->name;
								}
							}
							unset($attr);
						}
						
						$filter['select'] = 'DISTINCT (round_name)';
						$rounds = $this->model_plan_plan->getPlans($filter);
						$round_names = array();
						foreach ($rounds as $r) {
							$round_names[] = $r['round_name'];
						}

						$plan_data = $this->model_catalog_attribute_data->getAttrsOfPlan(array('round_names' => $round_names,'group_codes'=>$group_codes), false);
						$plandata = array();
						foreach ($plan_data as $value) {
							$val = $value['value'];
							$plandata[$value['plan_id']][$value['attribute_id']] = $value['value'];
						}

						$stt = 1;
						foreach ($plans as $key => $p) {
							$data = array();
				            $data[] = $stt; //stt
				            $data[] = isset($p['customer_area']) ? $p['customer_area'] : ''; //area
				            $data[] = isset($p['distributor']) ? $p['distributor'] : ''; //NPP
				            $data[] = isset($p['sales_nestle']) ? $p['sales_nestle'] : ''; //Sales

				            $data[] = isset($p['store_code']) ? $p['store_code'] : ''; // Mã CPM
				            $data[] = isset($p['customer_code']) ? $p['customer_code'] : ''; // Mã CH 
				            $data[] = isset($p['store_name']) ? $p['store_name'] : ''; // Tên CH 

				            $data[] = isset($p['store_address']) ? $p['store_address'] : ''; // số nhà
				            $data[] = isset($p['store_place']) ? $p['store_place'] : ''; // đường
				            $data[] = isset($p['store_ward']) ? $p['store_ward'] : ''; // Phường/ Xã
				            $data[] = isset($p['store_district']) ? $p['store_district'] : ''; // Quận/ Huyện
				            $data[] = isset($p['store_province']) ? $p['store_province'] : ''; // Tỉnh/ Thành phố

				            $data[] = isset($p['store_type']) ? $p['store_type'] : ''; // Loại CH 
				            $data[] = isset($p['note_admin']) ? $p['note_admin'] : ''; // Số lượng dù TC

				            if (isset($users[$p['user_id']])) {
								$data[] = $users[$p['user_id']]['usercode'] . '-' . $users[$p['user_id']]['fullname'];
							} else {
								$data[] = '';
							}

				            $data[] = isset($p['longitude']) ? $p['longitude'] : ''; // Kinh độ
							$data[] = isset($p['latitude']) ? $p['latitude'] : ''; // Vĩ độ
				            $data[] = (strtotime($p['time_checkin']) > 0) ? date('Y-m-d', strtotime($p['time_checkin'])) : ''; // Ngày viếng thăm
				        
	            			$plan_rating = $note = '';
							if ($p['plan_status'] == 1) {
								if ($p['plan_rating'] == 1) {
									$plan_rating = 'Thành công';
									$note = $p['note'];
								} elseif ($p['plan_rating'] == -2) {
									$plan_rating = 'Không thành công';
									$note = $p['note_ktc'];
								}
							}

							$data[] = $plan_rating;
							$data[] = !empty($reasons[$p['reason_id']]) && $p['plan_rating'] ==-2 ? $reasons[$p['reason_id']]['reason_name'] : '';
							$data[] = $note;

							$data[] = $p['plan_qc']; // QC

				            foreach ($attrs as $attr) {
								if (!empty($plandata[$p['plan_id']][$attr['attribute_id']])) {
									$pd = $plandata[$p['plan_id']][$attr['attribute_id']];
									if ($attr['input'] == 'select' && isset($attr['options'][$pd])) {
										$val = $attr['options'][$pd];
									} else {
										$val = $pd;
									}
								} else {
									$val = '';
								}
								$data[] = $val;
							}
							
							$worksheet->fromArray($data, NULL, 'A'.$row, false);
							$row++;
							$stt++;
							
						}

						$objWriter = PHPExcel_IOFactory::createWriter($workbook, 'Excel2007');
				        ob_end_clean();
				        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				        header('Content-Disposition: attachment;filename="'.$file_name.'"');
				        header('Cache-Control: max-age=0');
				        header("Content-Transfer-Encoding: binary");
				        // $objWriter->setPreCalculateFormulas(false);
				        $objWriter->save($export_file);
				        $objWriter->save('php://output');
				        // $this->clearSpreadsheetCache();
					} catch(Exception $e){
						echo $e->__toString();
					}
				}
			} else {
				$this->session->data['error_warning'] = "Không có dữ liệu!";
				$this->response->redirect($this->url->link('plan/plan'));
			}
 		}
	}
}