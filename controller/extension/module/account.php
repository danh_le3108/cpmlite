<?php
class ControllerExtensionModuleAccount extends Controller {
	public function index() {
		$languages = $this->load->language('extension/module/account');
		foreach($languages as $k=>$v){
			$data[$k] = $this->language->get($v);
		}
		
		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		if (isset($this->request->post['username'])) {
			$data['username'] = $this->request->post['username'];
		} else {
			$data['username'] = '';
		}

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}
		
		if (isset($this->request->get['route'])) {
			$route = $this->request->get['route'];

			unset($this->request->get['route']);
			unset($this->request->get['token']);

			$url = '';

			if ($this->request->get) {
				
				if (isset($this->request->get['_url'])) {
					unset($this->request->get['_url']);
				}
				$url .= http_build_query($this->request->get);
			}

			$data['redirect'] = $this->url->link($route, $url, true);
		} else {
			$data['redirect'] = $this->url->link('common/home');
		}
		
		

		$data['user_logged'] = $user_logged = $this->user->isLogged();
		$data['customer_logged'] = $customer_logged = $this->customer->isLogged();
		$data['distributor_logged'] = $distributor_logged = $this->distributor->isLogged();
		
			
			$data['register'] = $this->url->link('account/register', '', true);
			$data['login'] = $this->url->link('account/login', '', true);
			$data['logout'] = $this->url->link('account/logout', '', true);
			$data['forgotten'] = $this->url->link('account/forgotten', '', true);
			$data['account'] = $this->url->link('account/account', '', true);
			$data['edit'] = $this->url->link('account/edit', '', true);
			$data['link_password'] = $this->url->link('account/password', '', true);	
			
		if($user_logged ){
			$data['edit'] = $this->url->link('user/user', '', true);
			$data['logout'] = $this->url->link('user/logout', '', true);
		}
		
		if($distributor_logged ){
			$data['edit'] = $this->url->link('distributor/distributor', '', true);
			$data['logout'] = $this->url->link('distributor/logout', '', true);
		}

		return $this->load->view('extension/module/account', $data);
	}
}