<?php
class ControllerCatalogDistributor extends Controller{
	private $error = array();
	/*Excel*/ 
	private $template_id = 1;
	private $filter_key = array(
			'filter_global',
			'filter_region_code',
			'filter_coupon_check'
		);

	private $field_data = array(
			'distributor_id'=>'',
			'distributor_name'=>'',
			'distributor_code'=>'',
			'distributor_address'=>'',
			'coupon_check'=>'',
			'region_code'=>'',
			'status'=>1
		);
		
	
	public function import(){


		$this->load->language('excel/template');

		$this->load->model('catalog/distributor');
		
			$data['callback'] = str_replace('&amp;','&',$this->url->link('catalog/distributor',   '', true));
					

			if (!empty($this->request->files['file_import']['name'])&&substr($this->request->files['file_import']['name'], -5) == '.xlsx') {
				

					$template_id = isset($this->request->post['template_id']) ? $this->request->post['template_id'] : 0;

					$file_name = 'Distributor-Import-by-' . $this->user->getUserName(). '-at-'. date('Ymd-His') . '.xlsx';

					$dir = DIR_MEDIA . 'files/'.$this->config->get('config_project_folder').'/import/distributor/';

		      if(!is_dir($dir)){
		      	@mkdir($dir, 0777, true);
				@touch($dir. 'index.html');
		      }

		      $file = $dir . $file_name;

		      move_uploaded_file($this->request->files['file_import']['tmp_name'], $file);
		      if(file_exists($file)){
		      	$template_info = $this->excel->getTemplate($template_id);
			      $filesize = $this->request->files['file_import']['size'];
			      $import_info = $this->excel->getColRow($file);
			      $import_id = $this->excel->addImportHistory($template_info['template_id'], $file, $import_info);
					
					$data['callback'] = str_replace('&amp;','&',$this->url->link('catalog/distributor/import_data',   '&import_id='.$import_id , true));
				
				} else {
					$data['error'] = $this->language->get('text_error_filetype');
				}
			}  else {
				$data['error'] = $this->language->get('text_error_upload');
			}

			
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
			
			$this->response->setOutput($this->load->view('catalog/redirect', $data));
  }
  public function export() {
			$this->load->model('catalog/distributor');
			$json = array();
			$filter_data = array();
			foreach($this->filter_key as $key){
				if (isset($this->request->get[$key])) {
					$filter_data[$key] = $this->request->get[$key];
				}
			}
		$heading_fields = $this->model_catalog_distributor->distributor_fields();
		$template_data = $this->model_catalog_distributor->getExportDistributors($filter_data);
		
		$template_data = array(
			'template_path'=>DIR_MEDIA . 'files/'.$this->config->get('config_project_folder').'/export/distributor/',
			'template_name'=>'Distributor-Export-by-'.$this->user->getUsername().'-at-'. date('Ymd-His'),
			'heading_style'=>array('colrow'=>'A1:E1','color'=>'#333333','bg'=>'#9cc3e6'),
			'heading_fields'=>$heading_fields,
			'template_data'=>$template_data
		
		);
		$this->excel->export($template_data);
		
		$this->response->redirect($this->url->link('catalog/distributor', '' , true));
	}
	
   public function import_data(){
	
		$json = array();

		if($this->request->get['import_id']){
	
	    $this->load->model('localisation/region');
		$region = $this->model_localisation_region->getRegionsIndexBy('region_code');

			$this->load->model('catalog/distributor');
			$distributor_by_code = $this->model_catalog_distributor->getDistributorIndexBy('distributor_code');
	
			$import_id = $this->request->get['import_id'];
			$this->session->data['history_id'] = $import_id;
	
			$history_info = $this->excel->getImportHistory($import_id);

	  	$error = array();
	  	$data_row = array();
	  	$unique_update = array();

	     $update_row = $error_row = $success_row = 0;

	    $template_info = $this->excel->getTemplate($history_info['template_id']);

	    $setting = $template_info['settings'];

			$header_check = array();
	    $result = $this->excel->readFileCellKey($history_info['template_path'], $setting['start_row'],array(),$setting['sheet_index']);
		
		//	print_r('<pre>'); print_r($result); print_r('</pre>'); 	die();
			$distributors = array();
			$error_data = array();
	  		foreach($result['data'] as $row){
				if(!empty($row['B'])){
					$distributor['distributor_name'] =  $row['B'];
				}else{
					$error[] =  'Tên NPP không được trống';
				}
				if(!empty($row['C'])){
					$distributor['distributor_code'] =  $row['C'];
				}else{
					$error[] =  'Mã NPP không được trống';
				}
				if(!empty($row['D'])){
					$distributor['coupon_check'] =  ($row['D']=='x')?1:0;
				}
				
				if(!empty($row['E'])){
					$distributor['region_code'] = $CODE =  strtoupper($row['E']);
					if(isset($region[$CODE])){
						$distributor['region_id'] =  $region[$CODE]['region_id'];
						$distributor['region_code'] =  $region[$CODE]['region_code'];
					}
				}
				
				if(!empty($row['F'])){
					$distributor['distributor_address'] =  $row['F'];
				}
				
				if(empty($error)){
					$check_total = $this->model_catalog_distributor->checkDistributor($distributor);
					if($check_total==0){
						$success_row++;
					}elseif($check_total==2){
						$update_row++;
					}
				} else {
					$row['message'] = implode(' | ', $error);
					$error_data[] = $row;	
					$error_row++;
				}
		    }

		$history_info['import_info']['error'] = $error_data;
	    $history_info['import_info']['error_row'] = $error_row;
    	$history_info['import_info']['update_row'] = $update_row;
	    $history_info['import_info']['success_row'] = $success_row;
	  
	  	$this->excel->updateHistoryProgress($import_id, $history_info['import_info']);

			$this->response->redirect($this->url->link('catalog/distributor', '' , true));

	  }

  }
  /*End Excel*/ 
	private function _url(){
		$url = '';
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}
	public function delete() {
		$languages = $this->load->language('catalog/distributor');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/distributor');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $distributor_id) {
				$this->model_catalog_distributor->deleteDistributor($distributor_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = $this->_url();
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/distributor',  $url , true));
		}

		return $this->index();
	}
	public function index() {
		
		$data['ajax_list'] = $this->ajax_list();
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$template = 'common/ajax_list';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
	public function getList() {
		$this->response->setOutput($this->ajax_list());
	}
	public function ajax_list() {
		
		$languages = $this->load->language('catalog/distributor');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/distributor');
		
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'distributor_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = $this->_url();
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

		$data['add'] = $this->url->link('catalog/distributor/add',  '' , true);
		$data['delete'] = $this->url->link('catalog/distributor/delete',  $url , true);
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '' , true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/distributor',  $url , true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);


		$data['problems'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_project'),
			'limit' => $this->config->get('config_limit_project')
		);
		
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$filter_data[$key] = trim($this->request->get[$key]);
			} 
		}

		$query_total = $this->model_catalog_distributor->getTotalDistributors($filter_data);

		$results = $this->model_catalog_distributor->getDistributors($filter_data);

		foreach ($results as $result) {
			$data['problems'][] = array(
				'distributor_id' => $result['distributor_id'],
				'real_token'       => $result['real_token'],
				'distributor_name'       => $result['distributor_name'],
				'distributor_code'       => $result['distributor_code'],
				'distributor_address'    => $result['distributor_address'],
				'coupon_check'       => $result['coupon_check'],
				'region_code'       => $result['region_code'],
				'status'       => $result['status'],
				'edit'       => $this->url->link('catalog/distributor/edit', '&distributor_id=' . $result['distributor_id'] . $url , true)
			);
		}


		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = $this->_url();

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/distributor', '&sort=name' . $url , true);

		$url = $this->_url();

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $query_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_project');
		$pagination->url = $this->url->link('catalog/distributor/getList', $url . '&page={page}' , true);

		$data['pagination'] = $pagination->render();

		$data['results'] = $pagination->results();

		$data['sort'] = $sort;
		$data['order'] = $order;
		
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$data[$key] = $this->request->get[$key];
			}else{
				$data[$key] = null;
			}
		}

		/**/ 
		$this->load->model('localisation/region');
		$data['regions'] =  $this->model_localisation_region->getRegions();
		
			/* Excel */
			$data['form'] = $this->url->link('catalog/distributor/import', '' , true);
			
		if (isset($this->session->data['history_id'])) {
			$data['history_id'] = $this->session->data['history_id'];
		} else {
			$data['history_id'] = null;
		}
		
				
			$data['has_edit'] = $this->document->hasPermission('edit', 'catalog/distributor');
			$data['has_add'] = $this->document->hasPermission('add', 'catalog/distributor');
			$data['has_delete'] = $this->document->hasPermission('delete', 'catalog/distributor');
			$data['template_url'] = $this->url->link('excel/template/template',  'template_id='.$this->template_id, true);
			
			$data['template_id'] = $this->template_id;
			$data['export'] = $this->url->link('catalog/distributor/export', $url , true);
			
			
		$template = 'catalog/distributor_list';
		return $this->load->view($template, $data);
	}

	public function add() {
		$languages = $this->load->language('catalog/distributor');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/distributor');

		if ($this->user->hasPermission('add', 'catalog/distributor')) {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$this->model_catalog_distributor->addDistributor($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = $this->_url();
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/distributor',  $url , true));
			
			}
		}else{
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return $this->getForm();
	}

	public function edit() {
		$languages = $this->load->language('catalog/distributor');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/distributor');
		if ($this->user->hasPermission('add', 'catalog/distributor')) {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
					
				$this->model_catalog_distributor->editDistributor($this->request->get['distributor_id'], $this->request->post);
	
				$this->session->data['success'] = $this->language->get('text_success');
	
				$url = $this->_url();
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
	
				$this->response->redirect($this->url->link('catalog/distributor',  $url , true));
		
			}
		}else{
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return $this->getForm();
	}
	protected function getForm() {
		$languages = $this->load->language('catalog/distributor');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));
		
		
		$error_data = array(
			'error_warning'=>'',
			'error_name'=>'',
			'error_password'=>''
		);

		foreach($error_data as $key=>$default_value){
			if (isset($this->error[$key])) {
				$data[$key] = $this->error[$key];
			} else {
				$data[$key] = $default_value;
			}
		}

		$url = $this->_url();
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '' , true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/distributor',  $url , true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);

		if (!isset($this->request->get['distributor_id'])) {
			$data['action'] = $this->url->link('catalog/distributor/add',  $url , true);
		} else {
			$data['action'] = $this->url->link('catalog/distributor/edit', '&distributor_id=' . $this->request->get['distributor_id'] . $url , true);
		}

		$data['cancel'] = $this->url->link('catalog/distributor',  $url , true);

		if (isset($this->request->get['distributor_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$query_info = $this->model_catalog_distributor->getDistributor($this->request->get['distributor_id']);
		}
		foreach($this->field_data as $key=>$default_value){
			if (isset($this->request->post[$key])) {
				$data[$key] = $this->request->post[$key];
			} elseif (!empty($query_info)) {
				$data[$key] = $query_info[$key];
			} else {
				$data[$key] = $default_value;
			}
		}
		$this->load->model('localisation/region');
		$data['regions'] =  $this->model_localisation_region->getRegions();

		$template = 'catalog/distributor_form';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}

	protected function validateForm() {

		if ((utf8_strlen($this->request->post['distributor_name']) < 3) || (utf8_strlen($this->request->post['distributor_name']) > 128)) {
			$this->error['error_name'] = $this->language->get('text_error_name');
		}
		if (isset($this->request->post['password'])&&!empty($this->request->post['password'])) {
			if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
				$this->error['error_password'] =  $this->language->get('text_error_password');
			}
			if (!isset($this->request->post['confirm'])) {
				$this->error['error_password'] =  $this->language->get('text_error_confirm');
			}
			if ($this->request->post['password'] != $this->request->post['confirm']) {
				$this->error['error_password'] =  $this->language->get('text_error_confirm');
			}
		}

		return !$this->error;
	}
	protected function validateDelete() {
		if (!$this->user->hasPermission('delete', 'catalog/distributor')) {
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return !$this->error;
	}
}