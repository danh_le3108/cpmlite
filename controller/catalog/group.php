<?php
class ControllerCatalogGroup extends Controller{
	private $error = array();
	
	private $field_data = array(
			'group_id'=>'',
			'group_name'=>'',
			'group_code'=>'',
			'sort_order'=>''
		);

	private function _url(){
		$url = '';
		$url_key = array(
			'filter_prefix',
			'filter_name'
		);
		foreach($url_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}

	public function index() {
		$data['ajax_list'] = $this->ajax_list();
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$template = 'common/ajax_list';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
	public function getList() {
		$this->response->setOutput($this->ajax_list());
	}
	public function ajax_list() {
		$languages = $this->load->language('catalog/group');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'icon' => 'fa-dashboard',
			'href' => $this->url->link('common/home')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/group')
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);
		
		if (!empty($this->session->data['error_warning'])) {
			$data['error_warning'] = $this->session->data['error_warning'];
			unset($this->session->data['error_warning']);
		}
		if (!empty($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}
		$this->load->model('catalog/group');
		$data['groups'] = $this->model_catalog_group->getGroups($this->request->get);

		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$template = 'catalog/group_list';
		return $this->load->view($template, $data);
	}

	public function save() {
		$this->load->model('catalog/group');
		if (!empty($this->request->get['group_id'])) {
			$group_id = $this->request->get['group_id'];
			$this->model_catalog_group->edit($group_id, $this->request->post);
		} else {
			$this->model_catalog_group->add($this->request->post);
		}	
	}

	public function delete() {
		if (!empty($this->request->post['group_ids'])) {
			$sql = "DELETE FROM cpm_attribute_group WHERE group_id IN (" . implode(',', $this->request->post['group_ids']) . ")";
			$this->pdb->query($sql);
		}
	}
}