<?php
class ControllerCatalogAttribute extends Controller{
	private $error = array();
	
	private $field_data = array(
			'attribute_id'=>'',
			'attribute_name'=>'',
			'attribute_code'=>'',
			'capacity'=>'',
			'group_code'=>'',
			'sort_order'=>''
		);
	private $filter_key = array(
			'filter_global',
			'filter_group_code',
			'filter_name'
		);
	private function _url(){
		$url = '';
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(trim(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8')));
			}
		}
		return $url;
	}
	
	public function index() {

		$sql = "SELECT * FROM `cpm_local_streets` WHERE `province_id` = 79 order by street_name";
		$streets = $this->pdb->query($sql)->rows;
		$ds = array();
		foreach ($streets as $key => $d) {
			$ds[] = array(
				'id' => $d['name'],
				'value' => $d['name'],
				'store_ward' => $d['ward_name']
			);
		}

		$data['ajax_list'] = $this->ajax_list();
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$template = 'common/ajax_list';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}

	public function getList() {
		$this->response->setOutput($this->ajax_list());
	}
	
	public function ajax_list() {
		$languages = $this->load->language('catalog/attribute');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'icon' => 'fa-dashboard',
			'href' => $this->url->link('common/home')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/attribute')
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);
		
		if (!empty($this->session->data['error_warning'])) {
			$data['error_warning'] = $this->session->data['error_warning'];
			unset($this->session->data['error_warning']);
		}
		if (!empty($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}
		$this->load->model('catalog/attribute');
		$data['attributes'] = $this->model_catalog_attribute->getAttributes($this->request->get);
		$this->load->model('catalog/group');
		$data['groups'] = $this->model_catalog_group->getGroups();
		
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$template = 'catalog/attribute_list';
		return $this->load->view($template, $data);
	}

	public function save() {
		$this->load->model('catalog/attribute');
		if (!empty($this->request->get['attribute_id'])) {
			$this->request->post['options'] = str_replace('&quot;', '"', $this->request->post['options']);
			$attribute_id = $this->request->get['attribute_id'];
			$this->model_catalog_attribute->edit($attribute_id, $this->request->post);
		} else {
			$this->request->post['options'] = str_replace('&quot;', '"', $this->request->post['options']);
			$this->model_catalog_attribute->add($this->request->post);
		}	
	}

	public function delete() {
		if (!empty($this->request->post['attribute_ids'])) {
			$sql = "DELETE FROM cpm_attribute WHERE attribute_id IN (" . implode(',', $this->request->post['attribute_ids']) . ")";
			$this->pdb->query($sql);
		}
	}
}