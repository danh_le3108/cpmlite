<?php
class ControllerCatalogCoupon extends Controller{
	private $error = array();
	
  
	private $field_data = array(
			'coupon_id'=>'',
			'coupon_code'=>'',
			'sup_code'=>''
		);
		
		
	/*Excel*/ 
	private $template_id = 4;
	private $filter_key = array(
			'filter_name',
			'filter_sup_id',
			'filter_coupon_status'
		);
	public function import(){

		$json = array();

		$this->load->language('excel/template');

		$this->load->model('catalog/coupon');

			if (!empty($this->request->files['file_import']['name'])&&substr($this->request->files['file_import']['name'], -5) == '.xlsx') {
					

					$template_id = isset($this->request->post['template_id']) ? $this->request->post['template_id'] : 0;



					$file_name = 'Coupon-Import-by-'.$this->user->getUsername().'-at-'. date('Ymd-His') .'.xlsx';

					$dir = DIR_MEDIA . 'files/'. $this->config->get('config_project_folder') .'/import/coupon/';

		      if(!is_dir($dir)){
		      	@mkdir($dir, 0777, true);
				@touch($dir. 'index.html');
		      }

		      $file = $dir . $file_name;

		      move_uploaded_file($this->request->files['file_import']['tmp_name'], $file);
		      if(file_exists($file)){
		      	$template_info = $this->excel->getTemplate($template_id);
			      $filesize = $this->request->files['file_import']['size'];
			      $import_info = $this->excel->getColRow($file);
			      $import_id = $this->excel->addImportHistory($template_info['template_id'], $file, $import_info);
						
				} else {
					$json['error'] = $this->language->get('text_error_filetype');
				}
			}  else {
				$json['error'] = $this->language->get('text_error_upload');
			}

			$data['callback'] = str_replace('&amp;','&',$this->url->link('catalog/coupon/import_data',   '&import_id='.$import_id , true));
			
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
			
			$this->response->setOutput($this->load->view('catalog/redirect', $data));
  }
  public function export() {
			$this->load->model('catalog/coupon');
			$json = array();
			$filter_data = array();
			foreach($this->filter_key as $key){
				if (isset($this->request->get[$key])) {
					$filter_data[$key] = $this->request->get[$key];
				}
			}
		
			$heading_fields = $this->model_catalog_coupon->coupon_fields();
			$coupons_data = $this->model_catalog_coupon->getExportCoupons($filter_data);
			
			$export_data = array(
				'template_path'=>DIR_MEDIA . 'files/'.$this->config->get('config_project_folder').'/export/coupon/',
				'template_name'=>'Coupon-Export-by-'.$this->user->getUsername().'-at-'. date('Ymd-His'),
				'heading_style'=>array('colrow'=>'A1:D1','color'=>'#333333','bg'=>'#9cc3e6'),
				'heading_fields'=>$heading_fields,
				'template_data'=>$coupons_data
			
			);
			$this->excel->export($export_data);
		
			$this->response->redirect($this->url->link('catalog/coupon', '' , true));
	}
	/*Unlock Coupon*/ 
	
	public function unlock_coupon(){
		if ($this->user->hasPermission('delete', 'catalog/coupon')&&!empty($this->request->files['file_import']['name'])&&substr($this->request->files['file_import']['name'], -5) == '.xlsx') {
			
			$template_id = isset($this->request->post['template_id']) ? $this->request->post['template_id'] : 4;


			$file_name = 'Coupon_Unlock-by-' . $this->user->getUserName(). '-at-'. date('Ymd-His') .'.xlsx';

			$dir = DIR_MEDIA . 'files/'. $this->config->get('config_project_folder') .'/import/coupon/';

      if(!is_dir($dir)){
      	@mkdir($dir, 0777, true);
		@touch($dir. 'index.html');
      }

      $file = $dir . $file_name;

      move_uploaded_file($this->request->files['file_import']['tmp_name'], $file);
      if(file_exists($file)){
      	$template_info = $this->excel->getTemplate($template_id);
	      $filesize = $this->request->files['file_import']['size'];
	      $import_info = $this->excel->getColRow($file);
	      $import_id = $this->excel->addImportHistory($template_info['template_id'], $file, $import_info);
	      $history_info = $this->excel->getImportHistory($import_id);
				$template_info = $this->excel->getTemplate($history_info['template_id']);

	    $settings = $template_info['settings'];
		
				$this->load->model('catalog/coupon');
				$this->load->model('catalog/distributor');
				
				$coupon_duplicate = $this->config->get('config_coupon_duplicate');
				$coupon_has_paid = $this->config->get('config_coupon_has_paid');
				$coupon_has_deleted = $this->config->get('config_coupon_has_deleted');
				
				$in_delete = array($coupon_duplicate,$coupon_has_paid);
				
				$distributor_code = $this->model_catalog_distributor->getDistributorIndexBy('distributor_code');

			$header_check = array();
		    $result = $this->excel->readFileCellKey($history_info['template_path'], $settings['start_row'], $header_check, $settings['sheet_index']);
			
			$error_data = array();
					
			$error_row = $success_row = 0;	
					
		    foreach($result['data'] as $row => $excel_row) {
					$error_message = array();
					$coupon_code = strtoupper($excel_row['B']); 
					$coupon_info = $this->model_catalog_coupon->getCouponByCode($coupon_code);
					
					if(isset($coupon_info['coupon_code'])){
						if($coupon_info['coupon_status_id']==$coupon_has_deleted){
							$this->pdb->query("UPDATE ".PDB_PREFIX."coupon SET coupon_status_id = '0', import_id ='".(int)$import_id."' WHERE coupon_code  = '". $this->db->escape($coupon_info['coupon_code'])."' AND coupon_status_id = '".(int)$coupon_has_deleted."'");	
							$success_row++;
						} else {
							$error_message[] = 'Mã đã sử dụng';
						}
					} else {
						$error_message[] = 'Mã không tồn tại';
					}

					if(!empty($error_message)){
							$error_data[$row] = $excel_row;
							$error_data[$row]['message'] = implode(' | ', $error_message);
							$error_row++;
					}
			}//for
				
				$history_info['import_info']['error'] = $error_data;
				$history_info['import_info']['error_row'] = $error_row;
				$history_info['import_info']['success_row'] = $success_row;
				
				$this->excel->updateHistoryProgress($import_id, $history_info['import_info']);
		
				
			} else {
				$json['error'] = $this->language->get('text_error_filetype');
			}
		}  else {
			$json['error'] = $this->language->get('text_error_upload');
		}

		$this->response->redirect($this->url->link('catalog/coupon', '' , true));
	}
	
	/*Huy Coupon*/ 
	public function huy_coupon(){
		if ($this->user->hasPermission('delete', 'catalog/coupon')&&!empty($this->request->files['file_import']['name'])&&substr($this->request->files['file_import']['name'], -5) == '.xlsx') {
			

			$template_id = isset($this->request->post['template_id']) ? $this->request->post['template_id'] : 4;



			$file_name = 'Coupon-Huy-by-' . $this->user->getUserName(). '-at-'. date('Ymd-His') .'.xlsx';

			$dir = DIR_MEDIA . 'files/'. $this->config->get('config_project_folder') .'/import/coupon/';

      if(!is_dir($dir)){
      	@mkdir($dir, 0777, true);
		@touch($dir. 'index.html');
      }

      $file = $dir . $file_name;

      move_uploaded_file($this->request->files['file_import']['tmp_name'], $file);
      if(file_exists($file)){
      	$template_info = $this->excel->getTemplate($template_id);
	      $filesize = $this->request->files['file_import']['size'];
	      $import_info = $this->excel->getColRow($file);
	      $import_id = $this->excel->addImportHistory($template_info['template_id'], $file, $import_info);
	      $history_info = $this->excel->getImportHistory($import_id);
				$template_info = $this->excel->getTemplate($history_info['template_id']);

	    $settings = $template_info['settings'];
		
		$this->load->model('catalog/coupon');
				
				$coupon_duplicate = $this->config->get('config_coupon_duplicate');
				$coupon_has_paid = $this->config->get('config_coupon_has_paid');
				$coupon_has_deleted = $this->config->get('config_coupon_has_deleted');
				
				$in_delete = array($coupon_duplicate,$coupon_has_paid);
				
		$this->load->model('project/project_user');
		$sup_code = $this->model_project_project_user->getProjectUsersIndexBy('usercode');

		
		
			$header_check = array();
		    $result = $this->excel->readFileCellKey($history_info['template_path'], $settings['start_row'], $header_check, $settings['sheet_index']);
			
			$error_data = array();
					
			$error_row = $success_row = 0;	
					
		    foreach($result['data'] as $row => $excel_row) {
					$error_message = array();
					$coupon_code = strtoupper($excel_row['B']); 
					$coupon_info = $this->model_catalog_coupon->getCouponByCode($coupon_code);
					
					
					
					if(isset($coupon_info['coupon_code'])){
						if(!in_array($coupon_info['coupon_status_id'],$in_delete)){
							$this->pdb->query("UPDATE ".PDB_PREFIX."coupon SET coupon_status_id = '".(int)$coupon_has_deleted."' WHERE coupon_code  = '". $this->db->escape($coupon_info['coupon_code'])."' AND coupon_status_id NOT IN(".implode(',',$in_delete).")");	
						} else {
							$error_message[] = 'Mã coupon đã sử dụng';
						}
						
					} else {
						$error_message[] = 'Mã coupon không tồn tại';
					}

					if(!empty($error_message)){
							$error_data[$row] = $excel_row;
							$error_data[$row]['message'] = implode(' | ', $error_message);
							$error_row++;
					}else{
						$success_row++;
					}
			}//for
				
				$history_info['import_info']['error'] = $error_data;
				$history_info['import_info']['error_row'] = $error_row;
				$history_info['import_info']['success_row'] = $success_row;
				
				$this->excel->updateHistoryProgress($import_id, $history_info['import_info']);
		
				
			} else {
				$json['error'] = $this->language->get('text_error_filetype');
			}
		}  else {
			$json['error'] = $this->language->get('text_error_upload');
		}

		$this->response->redirect($this->url->link('catalog/coupon', 'filter_coupon_status='.$this->config->get('config_coupon_has_deleted') , true));
	}
   public function import_data(){
	
		$json = array();
	
		$this->load->model('catalog/coupon');
		

	
		if($this->request->get['import_id']){
	
			$import_id = $this->request->get['import_id'];
			$this->session->data['history_id'] = $import_id;
	
			$history_info = $this->excel->getImportHistory($import_id);

	  	$error = array();
	  	$data_row = array();

	    $update_row = $error_row = $success_row = 0;

	    $template_info = $this->excel->getTemplate($history_info['template_id']);

	    $setting = $template_info['settings'];

	    $result = $this->excel->readFileCellKey($history_info['template_path'], $setting['start_row'],array(),$setting['sheet_index']);
		//print_r('<pre>'); print_r($result); print_r('</pre>'); die();
		$this->load->model('project/project_user');
		$sup_group =$this->config->get('config_user_sup');
		$sup= $this->model_project_project_user->getProjectUsersIndexBy('usercode');
		
		$this->load->model('catalog/coupon_prefix');
		$prefix =  $this->model_catalog_coupon_prefix->getPrefixsIndexBy('coupon_prefix');
				
		$error_data = array();
  		foreach($result['data'] as $row){
			$error = array();
				$coupon['import_id'] = $import_id;
				
			$coupon_prefix = substr($row['B'], 0, 2);
			if(isset($prefix[$coupon_prefix])){
				$coupon['coupon_code'] = $row['B'];
				$coupon['coupon_prefix'] = $coupon_prefix;
			}else{
				$error[] = 'Không có mệnh giá này!';
			}
			if(isset($sup[$row['C']])&&$sup[$row['C']]['project_user_group']==$sup_group){
				$coupon['sup_code'] = $row['C'];
				$coupon['sup_id'] = $sup[$row['C']]['user_id'];
			}else{
				$error[] = 'NV này không phải SUP!';
			}
			if(empty($error)){
				$check_total = $this->model_catalog_coupon->checkCoupon($coupon);
				if($check_total==1){
					$error[] = 'Mã Coupon đã tồn tại!';
				}elseif($check_total==2){
					$update_row++;
				}
			}
			
			if(!empty($error)){
				$row['message'] = implode("|",$error);
				$error_data[] = $row;
				$error_row++;
			}else{
				$success_row++;
			}
			unset($error);
	    }
		$history_info['import_info']['error'] = $error_data;
		$history_info['import_info']['error_row'] = $error_row;
    	$history_info['import_info']['update_row'] = $update_row;
		$history_info['import_info']['success_row'] = $success_row;
		
		$this->excel->updateHistoryProgress($import_id, $history_info['import_info']);
		
		$this->response->redirect($this->url->link('catalog/coupon', '' , true));

	  }

  }
  /*End Excel*/
	private function _url(){
		$url = '';
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}
	public function index() {
		$data['ajax_list'] = $this->ajax_list();
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$template = 'common/ajax_list';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
	public function getList() {
		$this->response->setOutput($this->ajax_list());
	}
	public function ajax_list() {
		
		
		
		
		$languages = $this->load->language('catalog/coupon');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/coupon');
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'coupon_code';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = $this->_url();
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

		$data['add'] = $this->url->link('catalog/coupon/add',  '' , true);
		$data['delete'] = $this->url->link('catalog/coupon/delete',  $url , true);
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '' , true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/coupon',  $url , true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);


		$data['coupons'] = array();
		$filter_data = array(
			'sort'                    => $sort,
			'order'                   => $order,
			'start'                   => ($page - 1) * $this->config->get('config_limit_project'),
			'limit'                   => $this->config->get('config_limit_project')
		);
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$filter_data[$key] = $this->request->get[$key];
			} 
		}

		$this->load->model('catalog/coupon_status');
		$this->load->model('plan/plan');
		
		$data['coupon_status'] = array();
		$status_allow = array(4,6);
		$coupon_statuses = $this->model_catalog_coupon_status->getCouponStatuses();
	
		
		foreach($coupon_statuses as $coupon_status){
			if(in_array($coupon_status['coupon_status_id'],$status_allow )){
				$data['coupon_status'][$coupon_status['coupon_status_id']] = $coupon_status;
			}
			
		}


		$query_total = $this->model_catalog_coupon->getTotalCoupons($filter_data);
		$results = $this->model_catalog_coupon->getCoupons($filter_data);

		foreach ($results as $result) {
			if($result['plan_id']>0){
				$plan_href = $this->url->link('plan/plan/edit', '&plan_id=' . $result['plan_id']  , true);
			} else {
				$plan_href = '';
			}

			$data['coupons'][] = array(
				'coupon_id'        => $result['coupon_id'],
				'plan_id'        => $result['plan_id'],
				'coupon_code'      => $result['coupon_code'],
				'round_name'        => $result['round_name'],
				'sup_code' => $result['sup_code'],
				'coupon_prefix' => $result['coupon_prefix'],
				'plan_href' => $plan_href,
				'coupon_status'    => isset($coupon_statuses[$result['coupon_status_id']]) ? $coupon_statuses[$result['coupon_status_id']]['coupon_status_name'] :$this->language->get('text_unused'),
				'edit'             => $this->url->link('catalog/coupon/edit', '&coupon_id=' . $result['coupon_id'] . $url , true)
			);
		}


		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = $this->_url();

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/coupon', '&sort=name' . $url , true);

		$url = $this->_url();

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $query_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_project');
		$pagination->url = $this->url->link('catalog/coupon/getList', $url . '&page={page}' , true);

		$data['pagination'] = $pagination->render();

		$data['results'] = $pagination->results();

		$data['sort'] = $sort;
		$data['order'] = $order;
		
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$data[$key] = $this->request->get[$key];
			}else{
				$data[$key] = null;
			}
		}
		/**/ 
		$this->load->model('project/project_user');
		$data['users'] =  $this->model_project_project_user->getProjectUsersByGroupID($this->config->get('config_user_sup'));
		/* Excel */
			$data['form'] = $this->url->link('catalog/coupon/import', '' , true);
		
			if (isset($this->session->data['history_id'])) {
				$data['history_id'] = $this->session->data['history_id'];
			} else {
				$data['history_id'] = null;
			}
				
			$data['has_edit'] = $this->document->hasPermission('edit', 'catalog/coupon');
			$data['has_add'] = $this->document->hasPermission('add', 'catalog/coupon');
			$data['has_delete'] = $this->document->hasPermission('delete', 'catalog/coupon');
			
			$data['template_url'] = $this->url->link('excel/template/template',  'template_id='.$this->template_id, true);
			
			$data['template_id'] = $this->template_id;
			$data['export'] = $this->url->link('catalog/coupon/export', $url , true);

		$template = 'catalog/coupon_list';
		return $this->load->view($template, $data);
	}

}