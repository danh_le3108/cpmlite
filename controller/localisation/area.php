<?php
class ControllerLocalisationArea extends Controller{
	private $error = array();
	
	private $field_data = array(
			'area_id'=>'',
			'area_name'=>'',
			'area_code'=>'',
			'sort_order'=>''
		);

	private function _url(){
		$url = '';
		$url_key = array(
			'filter_prefix',
			'filter_name'
		);
		foreach($url_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}

	public function add() {
		$languages = $this->load->language('localisation/area');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('localisation/area');

		if ($this->document->hasPermission('add', 'localisation/area')) {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
				
				$this->model_localisation_area->addArea($this->request->post);
	
				$this->session->data['success'] = $this->language->get('text_success');
	
				$url = $this->_url();
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
	
				$this->response->redirect($this->url->link('localisation/area',  $url, true));
			}
		}else{
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return $this->index();
	}

	public function edit() {
		$languages = $this->load->language('localisation/area');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('localisation/area');

		if ($this->document->hasPermission('edit', 'localisation/area')) {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
				$this->model_localisation_area->editArea($this->request->get['area_id'], $this->request->post);
	
				$this->session->data['success'] = $this->language->get('text_success');
	
				$url = $this->_url();
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
	
				$this->response->redirect($this->url->link('localisation/area',  $url, true));
			}
		}else{
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return $this->index();
	}

	public function delete() {
		$languages = $this->load->language('localisation/area');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('localisation/area');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $area_id) {
				$this->model_localisation_area->deleteArea($area_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = $this->_url();
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

			$this->response->redirect($this->url->link('localisation/area',  $url, true));
		}

		return $this->index();
	}
	public function index() {		
		$data['ajax_list'] = $this->ajax_list();
		$template = 'common/ajax_list';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
	public function getList() {
		$this->response->setOutput($this->ajax_list());
	}
	public function ajax_list() {
		$languages = $this->load->language('localisation/area');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('localisation/area');
		$filter_key = array(
			'filter_prefix'=>null,
			'filter_name'=>null
		);
		foreach($filter_key as $key=>$value){
			if (isset($this->request->get[$key])) {
				$$key = $this->request->get[$key];
			} else {
				$$key = $value;
			}
		}
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'area_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = $this->_url();
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('localisation/area',  $url, true)
		);

		$data['add'] = $this->url->link('localisation/area/add',  $url.'&form=show', true);
		$data['delete'] = $this->url->link('localisation/area/delete',  $url, true);
		$data['user_delete'] = $this->document->hasPermission('delete', 'localisation/area');
		$data['user_add'] = $this->document->hasPermission('add', 'localisation/area');

		$data['areas'] = array();

		$filter_data = array(
			'filter_prefix'=>$filter_prefix,
			'filter_name'=>$filter_name,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_project'),
			'limit' => $this->config->get('config_limit_project')
		);

		$query_total = $this->model_localisation_area->getTotalAreas($filter_data);

		$results = $this->model_localisation_area->getAreas($filter_data);
		
		$user_edit =  $this->document->hasPermission('edit', 'localisation/area');
		foreach ($results as $result) {
			$action = array();
			if($user_edit){
				$action[] = array(
					'text'		=>$this->language->get('button_edit'),
					'icon'		=>'fa-edit',
					'href'       => $this->url->link('localisation/area/edit', '&area_id=' . $result['area_id'] . $url.'&form=show', true)
				);
			}
			
			$data['areas'][] = array(
				'area_id' => $result['area_id'],
				'area_name'       => $result['area_name'],
				'area_code'       => $result['area_code'],
				'sort_order'       => $result['sort_order'],
				'action'    => $action
			);
		}


		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = $this->_url();

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('localisation/area', '&sort=name' . $url, true);

		$url = $this->_url();

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $query_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_project');
		$pagination->url = $this->url->link('localisation/area/getList', $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = $pagination->results();

		$data['sort'] = $sort;
		$data['order'] = $order;
		
		/*Form start*/ 
		
		$error_data = array(
			'error_warning'=>'',
			'error_name'=>''
		);

		foreach($error_data as $key=>$default_value){
			if (isset($this->error[$key])) {
				$data[$key] = $this->error[$key];
			} else {
				$data[$key] = $default_value;
			}
		}
		if (!isset($this->request->get['area_id'])) {
			$data['form_heading'] = $this->language->get('form_add');
			$data['action'] = $this->url->link('localisation/area/add',  $url.'&form=show', true);
		} else {
			$data['form_heading'] = sprintf($this->language->get('form_edit'), $this->request->get['area_id']);
			$data['action'] = $this->url->link('localisation/area/edit', '&area_id=' . $this->request->get['area_id'] . $url.'&form=show', true);
		}

		$data['form_class'] = isset($this->request->get['form'])?'show':'hide';
		$data['cancel'] = $this->url->link('localisation/area',  $url, true);

		if (isset($this->request->get['area_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$query_info = $this->model_localisation_area->getArea($this->request->get['area_id']);
		}
		foreach($this->field_data as $key=>$default_value){
			if (isset($this->request->post[$key])) {
				$data[$key] = $this->request->post[$key];
			} elseif (!empty($query_info)) {
				$data[$key] = $query_info[$key];
			} else {
				$data[$key] = $default_value;
			}
		}
		$this->load->model('localisation/district');
		$data['districts'] =  $this->model_localisation_district->getDistricts();
		
		/*Form end*/ 
		
		$data['filter_prefix'] = $filter_prefix;
		$data['filter_name'] = $filter_name;
		
		
		$template = 'localisation/area';
		$this->document->setBreadcrumbs($data['breadcrumbs']);
		return $this->load->view($template, $data);
	}

	protected function validateForm() {

		if ((utf8_strlen($this->request->post['area_name']) < 2) || (utf8_strlen($this->request->post['area_name']) > 128)) {
			$this->error['error_name'] = $this->language->get('text_error_name');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->document->hasPermission('delete', 'localisation/area')) {
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}
		return !$this->error;
	}
	
	public function area() {
		$json = array();

		$this->load->model('localisation/area');

		$query_info = $this->model_localisation_area->getArea($this->request->get['area_id']);

		if ($query_info) {
			foreach($this->field_data as $key=>$default_value){
				if (!empty($query_info)) {
					$json[$key] = $query_info[$key];
				} else {
					$json[$key] = $default_value;
				}
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}