<?php
class ControllerLocalisationProvince extends Controller{
	
	private $limit = 100;
	
	private $field_data = array(
			'province_id'=>'',
			'name'=>'',
			'province_name'=>'',
			'prefix'=>'',
			'code'=>'',
			'area_id'=>0,
			'region_id'=>0,
			'longitude'=>'',
			'latitude'=>''
		);
		
	private function _url(){
		$url = '';
		$url_key = array(
			'filter_prefix',
			'filter_name'
		);
		foreach($url_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}

	public function index() {
	
		$languages = $this->load->language('localisation/province');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('localisation/province');
		
		$filter_key = array(
			'filter_prefix'=>null,
			'filter_name'=>null
		);
		foreach($filter_key as $key=>$value){
			if (isset($this->request->get[$key])) {
				$$key = $this->request->get[$key];
			} else {
				$$key = $value;
			}
		}
		
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'province_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = $this->_url();
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['add'] = $this->url->link('localisation/province/add',  $url , true);
		$data['delete'] = $this->url->link('localisation/province/delete',  $url , true);

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '' , true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('localisation/province',  $url , true)
		);

		$this->document->setBreadcrumbs($data['breadcrumbs']);

		$data['provinces'] = array();

		$filter_data = array(
			'filter_prefix'=>$filter_prefix,
			'filter_name'=>$filter_name,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->limit,
			'limit' => $this->limit
		);
		
		$query_total = $this->model_localisation_province->getTotalProvinces($filter_data);

		$results = $this->model_localisation_province->getProvinces($filter_data);
		
		$district_ids = array();
		$data['central'] = array();
		$this->load->model('localisation/district');
		
		if(!empty($results)){
			foreach ($results as $result) {
				$district_ids[] = $result['central_district_id'];
				$action = array();
					$action[] = array(
						'text'		=>$this->language->get('button_view'),
						'icon'		=>'fa-edit',
						'href'       => $this->url->link('localisation/district', '&filter_province_id=' . $result['province_id'] , true)
					);
					$action[] = array(
						'text'		=>$this->language->get('button_edit'),
						'icon'		=>'fa-edit',
						'href'       => $this->url->link('localisation/province/edit', '&province_id=' . $result['province_id'] . $url.'&form=show', true)
					);
				
				
				$data['provinces'][] = array(
					'province_id' => $result['province_id'],
					'central_district_id' => $result['central_district_id'],
					'prefix' => $result['prefix'],
					'name'       => $result['name'],
					'province_name'       => $result['province_name'],
					'code'       => $result['code'],
					'region_name'       => $result['region_name'],
					'area_name'       => $result['area_name'],
					'latitude'       => $result['latitude'],
					'longitude'       => $result['longitude'],
					'action'    => $action
				);
			}
		}

			

		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = $this->_url();

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('localisation/province', '&sort=name' . $url , true);

		$url = $this->_url();

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $query_total;
		$pagination->page = $page;
		$pagination->limit = $this->limit;
		$pagination->url = $this->url->link('localisation/province', $url . '&page={page}' , true);

		$data['pagination'] = $pagination->render();

		$data['results'] = $pagination->results();

		$data['sort'] = $sort;
		$data['order'] = $order;

		
		$data['delete'] = $this->url->link('localisation/province/delete',  $url , true);

		$data['filter_prefix'] = $filter_prefix;
		$data['filter_name'] = $filter_name;
		$data['token'] = '';
		
		
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$template = 'localisation/province_list';
		
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$this->response->setOutput($this->load->view($template, $data));
		
	}
	
	
	
	public function provinces() {
		$json = array();

		$this->load->model('localisation/province');

		$results = $this->model_localisation_province->getProvinces();

		foreach ($results as $result) {
			$json['province'][] = array(
				'province_id' => $result['province_id'],
				'province_name'       => $result['province_name']
			);
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function province() {
		$json = array();

		$this->load->model('localisation/province');

		$query_info = $this->model_localisation_province->getProvince($this->request->get['province_id']);

		if ($query_info) {
			$this->load->model('localisation/district');
			foreach($this->field_data as $key=>$default_value){
				if (!empty($query_info)) {
					$json[$key] = $query_info[$key];
				} else {
					$json[$key] = $default_value;
				}
			}
			$json['district'] = $this->model_localisation_district->getDistrictsByProvinceId($this->request->get['province_id']);
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}