<?php
class ControllerLocalisationWard extends Controller{
	private $field_data = array(
			'ward_id'=>'',
			'name'=>'',
			'ward_name'=>'',
			'district_id'=>'',
			'province_id'=>''
		);
	public function ward() {
		$json = array();

		$this->load->model('localisation/ward');

		$query_info = $this->model_localisation_ward->getWard($this->request->get['ward_id']);

		if ($query_info) {
			foreach($this->field_data as $key=>$default_value){
				if (!empty($query_info)) {
					$json[$key] = $query_info[$key];
				} else {
					$json[$key] = $default_value;
				}
			}
		
			$this->load->model('localisation/place');
			$json['place'] = $this->model_localisation_place->getPlacesByWardId($this->request->get['ward_id']);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}