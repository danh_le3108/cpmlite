<?php
class ControllerLocalisationRegion extends Controller{
	private $error = array();

	private $field_data = array(
			'region_id'=>'',
			'region_name'=>'',
			'region_code'=>'',
			'sort_order'=>1
		);

	private function _url(){
		$url = '';
		return $url;
	}

	public function add() {
		$languages = $this->load->language('localisation/region');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('localisation/region');

		if ($this->document->hasPermission('add', 'localisation/region')) {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
				$this->model_localisation_region->addRegion($this->request->post);

				$this->session->data['success'] = $this->language->get('text_success');

				$url = $this->_url();
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

				$this->response->redirect($this->url->link('localisation/region',  $url, true));
			}
		}else{
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return $this->index();
	}

	public function edit() {
		$languages = $this->load->language('localisation/region');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('localisation/region');
		if ($this->document->hasPermission('edit', 'localisation/region')) {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

				$this->model_localisation_region->editRegion($this->request->get['region_id'], $this->request->post);

				$this->session->data['success'] = $this->language->get('text_success');

				$url = $this->_url();
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

				$this->response->redirect($this->url->link('localisation/region',  $url, true));
			}
		}else{
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return $this->index();
	}

	public function delete() {
		$languages = $this->load->language('localisation/region');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('localisation/region');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $region_id) {
				$this->model_localisation_region->deleteRegion($region_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = $this->_url();
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

			$this->response->redirect($this->url->link('localisation/region',  $url, true));
		}

		return $this->index();
	}
	public function index() {
		$data['ajax_list'] = $this->ajax_list();
		$template = 'common/ajax_list';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
	public function getList() {
		$this->response->setOutput($this->ajax_list());
	}
	public function ajax_list() {
		$languages = $this->load->language('localisation/region');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('localisation/region');

		$filter_key = array(
			'filter_prefix'=>null,
			'filter_name'=>null
		);
		foreach($filter_key as $key=>$value){
			if (isset($this->request->get[$key])) {
				$$key = $this->request->get[$key];
			} else {
				$$key = $value;
			}
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'region_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = $this->_url();
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('localisation/region',  $url, true)
		);

		$data['add'] = $this->url->link('localisation/region/add',  $url.'&form=show', true);
		$data['delete'] = $this->url->link('localisation/region/delete',  $url, true);
		$data['user_delete'] = $this->document->hasPermission('delete', 'localisation/region');
		$data['user_add'] = $this->document->hasPermission('add', 'localisation/region');
		$data['regions'] = array();

		$filter_data = array(
			'filter_prefix'=>$filter_prefix,
			'filter_name'=>$filter_name,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_project'),
			'limit' => $this->config->get('config_limit_project')
		);

		$query_total = $this->model_localisation_region->getTotalRegions($filter_data);

		$results = $this->model_localisation_region->getRegions($filter_data);

		$user_edit =  $this->document->hasPermission('edit', 'localisation/region');
		foreach ($results as $result) {
			$action = array();
			if($user_edit){
				$action[] = array(
					'text'		=>$this->language->get('button_edit'),
					'icon'		=>'fa-edit',
					'href'       => $this->url->link('localisation/region/edit', '&region_id=' . $result['region_id'] . $url.'&form=show', true)
				);
			}
			$data['regions'][] = array(
				'region_id' => $result['region_id'],
				'region_name'       => $result['region_name'],
				'region_code'       => $result['region_code'],
				'sort_order'     => $result['sort_order'],
				'action'    => $action
			);
		}


		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = $this->_url();

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('localisation/region', '&sort=region_name' . $url, true);

		$url = $this->_url();

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $query_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_project');
		$pagination->url = $this->url->link('localisation/region/getList', $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = $pagination->results();

		$data['sort'] = $sort;
		$data['order'] = $order;


		/*Form start*/

		$error_data = array(
			'error_warning'=>'',
			'error_name'=>''
		);

		foreach($error_data as $key=>$default_value){
			if (isset($this->error[$key])) {
				$data[$key] = $this->error[$key];
			} else {
				$data[$key] = $default_value;
			}
		}

		if (!isset($this->request->get['region_id'])) {
			$data['form_heading'] = $this->language->get('form_add');
			$data['action'] = $this->url->link('localisation/region/add',  $url.'&form=show', true);
		} else {
			$data['form_heading'] = sprintf($this->language->get('form_edit'), $this->request->get['region_id']);
			$data['action'] = $this->url->link('localisation/region/edit', '&region_id=' . $this->request->get['region_id'] . $url.'&form=show', true);
		}

		$data['form_class'] = isset($this->request->get['form'])?'show':'hide';
		$data['cancel'] = $this->url->link('localisation/region',  $url, true);

		if (isset($this->request->get['region_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$query_info = $this->model_localisation_region->getRegion($this->request->get['region_id']);
		}
		foreach($this->field_data as $key=>$default_value){
			if (isset($this->request->post[$key])) {
				$data[$key] = $this->request->post[$key];
			} elseif (!empty($query_info)) {
				$data[$key] = $query_info[$key];
			} else {
				$data[$key] = $default_value;
			}
		}
		/*Form end*/
		$data['filter_prefix'] = $filter_prefix;
		$data['filter_name'] = $filter_name;


		$template = 'localisation/region';
		$this->document->setBreadcrumbs($data['breadcrumbs']);
		return $this->load->view($template, $data);
	}

	protected function validateForm() {

		if ((utf8_strlen($this->request->post['region_name']) < 2) || (utf8_strlen($this->request->post['region_name']) > 128)) {
			$this->error['error_name'] = $this->language->get('text_error_name');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->document->hasPermission('delete', 'localisation/region')) {
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}
		return !$this->error;
	}

	public function region() {
		$json = array();

		$this->load->model('localisation/region');

		$query_info = $this->model_localisation_region->getRegion($this->request->get['region_id']);

		if ($query_info) {
			foreach($this->field_data as $key=>$default_value){
				if (!empty($query_info)) {
					$json[$key] = $query_info[$key];
				} else {
					$json[$key] = $default_value;
				}
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}