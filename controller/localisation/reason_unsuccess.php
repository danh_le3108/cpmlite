<?php
class ControllerLocalisationReasonUnsuccess extends Controller{
	private $error = array();
	
	private $field_data = array(
			'reason_id'=>'',
			'reason_name'=>'',
			'sort_order'=>''
		);

	private function _url(){
		$url = '';
		$url_key = array(
			'filter_prefix',
			'filter_name'
		);
		foreach($url_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}

	public function add() {
		$languages = $this->load->language('localisation/reason_unsuccess');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('localisation/reason_unsuccess');

		if ($this->document->hasPermission('add', 'localisation/reason_unsuccess')) {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
				
				$this->model_localisation_reason_unsuccess->addReason($this->request->post);
	
				$this->session->data['success'] = $this->language->get('text_success');
	
				$url = $this->_url();
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
	
				$this->response->redirect($this->url->link('localisation/reason_unsuccess',  $url, true));
			}
		}else{
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return $this->index();
	}

	public function edit() {
		$languages = $this->load->language('localisation/reason_unsuccess');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('localisation/reason_unsuccess');

		if ($this->document->hasPermission('edit', 'localisation/reason_unsuccess')) {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
				$this->model_localisation_reason_unsuccess->editReason($this->request->get['reason_id'], $this->request->post);
	
				$this->session->data['success'] = $this->language->get('text_success');
	
				$url = $this->_url();
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
	
				$this->response->redirect($this->url->link('localisation/reason_unsuccess',  $url, true));
			}
		}else{
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return $this->index();
	}

	public function delete() {
		$languages = $this->load->language('localisation/reason_unsuccess');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('localisation/reason_unsuccess');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $reason_id) {
				$this->model_localisation_reason_unsuccess->deleteReason($reason_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = $this->_url();
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

			$this->response->redirect($this->url->link('localisation/reason_unsuccess',  $url, true));
		}

		return $this->index();
	}
	public function index() {		
		$data['ajax_list'] = $this->ajax_list();
		$template = 'common/ajax_list';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
	public function getList() {
		$this->response->setOutput($this->ajax_list());
	}
	public function ajax_list() {
		$languages = $this->load->language('localisation/reason_unsuccess');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('localisation/reason_unsuccess');
		$filter_key = array(
			'filter_prefix'=>null,
			'filter_name'=>null
		);
		foreach($filter_key as $key=>$value){
			if (isset($this->request->get[$key])) {
				$$key = $this->request->get[$key];
			} else {
				$$key = $value;
			}
		}
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'reason_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = $this->_url();
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('localisation/reason_unsuccess',  $url, true)
		);

		$data['add'] = $this->url->link('localisation/reason_unsuccess/add',  $url.'&form=show', true);
		$data['delete'] = $this->url->link('localisation/reason_unsuccess/delete',  $url, true);
		$data['user_delete'] = $this->document->hasPermission('delete', 'localisation/reason_unsuccess');
		$data['user_add'] = $this->document->hasPermission('add', 'localisation/reason_unsuccess');

		$data['reasons'] = array();

		$filter_data = array(
			'filter_prefix'=>$filter_prefix,
			'filter_name'=>$filter_name,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_project'),
			'limit' => $this->config->get('config_limit_project')
		);

		$query_total = $this->model_localisation_reason_unsuccess->getTotalReason($filter_data);

		$results = $this->model_localisation_reason_unsuccess->getReasons(0,$filter_data);
		
		$user_edit =  $this->document->hasPermission('edit', 'localisation/reason_unsuccess');
		foreach ($results as $result) {
			$action = array();
			if($user_edit){
				$action[] = array(
					'text'		=>$this->language->get('button_edit'),
					'icon'		=>'fa-edit',
					'href'       => $this->url->link('localisation/reason_unsuccess/edit', '&reason_id=' . $result['reason_id'] . $url.'&form=show', true)
				);
			}
			
			$data['reasons'][] = array(
				'reason_id' => $result['reason_id'],
				'sort_order' => $result['sort_order'],
				'reason_name'       => $result['reason_name'],
				'action'    => $action
			);
		}


		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = $this->_url();

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('localisation/reason_unsuccess', '&sort=name' . $url, true);

		$url = $this->_url();

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $query_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_project');
		$pagination->url = $this->url->link('localisation/reason_unsuccess/getList', $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = $pagination->results();

		$data['sort'] = $sort;
		$data['order'] = $order;
		
		/*Form start*/ 
		
		$error_data = array(
			'error_warning'=>'',
			'error_name'=>''
		);

		foreach($error_data as $key=>$default_value){
			if (isset($this->error[$key])) {
				$data[$key] = $this->error[$key];
			} else {
				$data[$key] = $default_value;
			}
		}
		if (!isset($this->request->get['reason_id'])) {
			$data['form_heading'] = $this->language->get('form_add');
			$data['action'] = $this->url->link('localisation/reason_unsuccess/add',  $url.'&form=show', true);
		} else {
			$data['form_heading'] = sprintf($this->language->get('form_edit'), $this->request->get['reason_id']);
			$data['action'] = $this->url->link('localisation/reason_unsuccess/edit', '&reason_id=' . $this->request->get['reason_id'] . $url.'&form=show', true);
		}

		$data['form_class'] = isset($this->request->get['form'])?'show':'hide';
		$data['cancel'] = $this->url->link('localisation/reason_unsuccess',  $url, true);

		if (isset($this->request->get['reason_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$query_info = $this->model_localisation_reason_unsuccess->getReason($this->request->get['reason_id']);
		}
		foreach($this->field_data as $key=>$default_value){
			if (isset($this->request->post[$key])) {
				$data[$key] = $this->request->post[$key];
			} elseif (!empty($query_info)) {
				$data[$key] = $query_info[$key];
			} else {
				$data[$key] = $default_value;
			}
		}
		
		/*Form end*/ 
		
		$data['filter_prefix'] = $filter_prefix;
		$data['filter_name'] = $filter_name;
		
		
		$template = 'localisation/reason_unsuccess';
		$this->document->setBreadcrumbs($data['breadcrumbs']);
		return $this->load->view($template, $data);
	}

	protected function validateForm() {

		if ((utf8_strlen($this->request->post['reason_name']) < 3) || (utf8_strlen($this->request->post['reason_name']) > 128)) {
			$this->error['error_name'] = $this->language->get('text_error_name');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->document->hasPermission('delete', 'localisation/reason_unsuccess')) {
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return !$this->error;
	}
}