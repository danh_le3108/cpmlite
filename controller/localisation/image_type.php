<?php
class ControllerLocalisationImageType extends Controller{
	private $error = array();
	
	private $field_data = array(
			'image_type_id'=>'',
			'project_id'=>0,
			'upload_limit'=>'',
			'image_type'=>'',
			'sort_order'=>0,
			'required'=>0
		);

	private function _url(){
		$url = '';
		$url_key = array(
			'filter_prefix',
			'filter_name'
		);
		foreach($url_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}
	public function delete() {
		$languages = $this->load->language('localisation/image_type');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('localisation/image_type');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $image_type_id) {
				$this->model_localisation_image_type->deleteImageType($image_type_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = $this->_url();

			$this->response->redirect($this->url->link('localisation/image_type',  $url , true));
		}

		return $this->index();
	}

	public function index() {
		$languages = $this->load->language('localisation/image_type');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('localisation/image_type');
		
		$filter_key = array(
			'filter_name'=>null
		);
		foreach($filter_key as $key=>$value){
			if (isset($this->request->get[$key])) {
				$$key = $this->request->get[$key];
			} else {
				$$key = $value;
			}
		}
		
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'image_type';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = $this->_url();
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['add'] = $this->url->link('localisation/image_type/add',  $url , true);
		$data['delete'] = $this->url->link('localisation/image_type/delete',  $url , true);

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '' , true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('localisation/image_type',  $url , true)
		);

		$this->document->setBreadcrumbs($data['breadcrumbs']);

		$data['codes'] = array();

		$filter_data = array(
			'filter_name'=>$filter_name,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_project'),
			'limit' => $this->config->get('config_limit_project')
		);

		$query_total = $this->model_localisation_image_type->getTotalImageType($filter_data);

		$results = $this->model_localisation_image_type->getImageTypes($filter_data);
		$config_project_images = $this->config->get('config_project_images');
		
		foreach ($results as $result) {
			if(in_array($result['image_type_id'],$config_project_images)){
				$data['codes'][] = array(
					'image_type_id' => $result['image_type_id'],
					'project_id' => $result['project_id'],
					'sort_order' => $result['sort_order'],
					'image_type'       => $result['image_type'],
					'required'       => $result['required'],
					'upload_limit'       => $result['upload_limit'],
					'edit'       => $this->url->link('localisation/image_type/edit', '&image_type_id=' . $result['image_type_id'] . $url , true)
				);
			}
		}
		$data['config_project_id'] = $this->config->get('config_project_id');

		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = $this->_url();

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('localisation/image_type', '&sort=name' . $url , true);

		$url = $this->_url();

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $query_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_project');
		$pagination->url = $this->url->link('localisation/image_type', $url . '&page={page}' , true);

		$data['pagination'] = $pagination->render();

		$data['results'] = $pagination->results();

		$data['sort'] = $sort;
		$data['order'] = $order;

		
		$data['delete'] = $this->url->link('localisation/image_type/delete',  $url , true);

		$data['filter_name'] = $filter_name;
		$data['token'] = '';
		
		
		$template = 'localisation/image_type_list';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
		
	}

	public function add() {
		$languages = $this->load->language('localisation/image_type');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('localisation/image_type');

		if ($this->user->hasPermission('add', 'localisation/image_type')) {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
				
				$this->model_localisation_image_type->addImageType($this->request->post);
	
				$this->session->data['success'] = $this->language->get('text_success');
	
				$url = $this->_url();
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
	
				$this->response->redirect($this->url->link('localisation/image_type',  $url , true));
			}
		}else{
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return $this->getForm();
	}

	public function edit() {
		$languages = $this->load->language('localisation/image_type');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('localisation/image_type');

		if ($this->user->hasPermission('edit', 'localisation/image_type')) {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
				$this->model_localisation_image_type->editImageType($this->request->get['image_type_id'], $this->request->post);
				$this->session->data['success'] = $this->language->get('text_success');
	
				$url = $this->_url();
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
	
				$this->response->redirect($this->url->link('localisation/image_type',  $url , true));
			}
		}else{
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return $this->getForm();
	}

	protected function getForm() {
		$languages = $this->load->language('localisation/image_type');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));
		
		
		$error_data = array(
			'error_warning'=>'',
			'error_name'=>''
		);

		foreach($error_data as $key=>$default_value){
			if (isset($this->error[$key])) {
				$data[$key] = $this->error[$key];
			} else {
				$data[$key] = $default_value;
			}
		}

		$url = $this->_url();
		
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '' , true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('localisation/image_type',  $url , true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);

		if (!isset($this->request->get['image_type_id'])) {
			$data['action'] = $this->url->link('localisation/image_type/add',  $url , true);
		} else {
			$data['action'] = $this->url->link('localisation/image_type/edit', '&image_type_id=' . $this->request->get['image_type_id'] . $url , true);
		}

		$data['cancel'] = $this->url->link('localisation/image_type',  $url , true);

		if (isset($this->request->get['image_type_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$query_info = $this->model_localisation_image_type->getImageType($this->request->get['image_type_id']);
		}
		foreach($this->field_data as $key=>$default_value){
			if (isset($this->request->post[$key])) {
				$data[$key] = $this->request->post[$key];
			} elseif (!empty($query_info)) {
				$data[$key] = $query_info[$key];
			} else {
				$data[$key] = $default_value;
			}
		}
		
		
		$this->load->model('project/project');
		$data['projects'] = $this->model_project_project->getProjects();
		


		$template = 'localisation/image_type_form';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}

	protected function validateAdd() {
		if (!$this->user->hasPermission('add', 'localisation/image_type')) {
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		if ((utf8_strlen($this->request->post['image_type']) < 3) || (utf8_strlen($this->request->post['image_type']) > 128)) {
			$this->error['error_name'] = $this->language->get('text_error_name');
		}

		return !$this->error;
	}
	protected function validateForm() {

		if (!$this->user->hasPermission('add', 'localisation/image_type')) {
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return !$this->error;
	}
	public function codes() {
		$json = array();

		$this->load->model('localisation/image_type');

		$results = $this->model_localisation_image_type->getAllTypes();

		foreach ($results as $result) {
			$json['code'][] = array(
				'image_type_id' => $result['image_type_id'],
				'image_type'       => $result['image_type']
			);
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}