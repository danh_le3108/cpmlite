<?php
class ControllerSurveyQuestion extends Controller{
	public function index() {
			echo '<div class="alert alert-info">Không có tham số yêu cầu!</div>';
	}
	public function form() {
		if (isset($this->request->get['survey_id'])&&isset($this->request->get['question_id'])) {
			$data['survey_id'] = $survey_id = $this->request->get['survey_id'];
			$data['question_id'] = $question_id = $this->request->get['question_id'];
			$languages = $this->load->language('survey/survey');
			foreach($languages as $key=>$value){
					$data[$key] = $value;
			}
			$this->load->model('survey/survey');
			$data['info'] = $this->model_survey_survey->getQuestion($question_id);
			
			
			$data['inputs'] = $this->model_survey_survey->inputs();
			$data['groups'] = $this->model_survey_survey->getGroupsBySurveyID($this->request->get['survey_id']);
			
			$this->load->model('catalog/attribute');
			$data['attr_groups'] = $this->model_catalog_attribute->getAttributesGroups();
			
			$template = 'survey/form_question';
			$this->response->setOutput($this->load->view($template, $data));
		}else{
			echo '<div class="alert alert-info">Không có tham số yêu cầu!</div>';
		}
	}
	public function delete() {
		$this->load->language('plan/plan');
			$data['user_id'] = $this->user->getId();
		
		$json = array();
		
		$this->load->model('survey/survey');
			
		if ($this->document->hasPermission('edit', 'survey/survey')&&isset($this->request->post)&&isset($this->request->get['question_id'])) {
			$data['survey_id'] = $survey_id = $this->request->get['survey_id'];
			$this->model_survey_survey->deleteQuestion($this->request->get['question_id']);
			$this->session->data['success'] = $json['success'] = $this->language->get('text_success');
		}else{
			$this->session->data['error_warning'] = $json['error'] = $this->language->get('text_error_permission');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function save() {
		$this->load->language('plan/plan');
		$data['user_id'] = $this->user->getId();
		
		$json = array();
		
			$this->load->model('catalog/attribute');
			$attr = $this->model_catalog_attribute->getAttributesIndexBy('attribute_id');
			
		$this->load->model('survey/survey');
		if ($this->document->hasPermission('edit', 'survey/survey')&&isset($this->request->post)) {
			
			$data['survey_id'] = $survey_id = $this->request->get['survey_id'];
			
			$this->request->post['survey_id'] = $survey_id;
			
			
			if(isset($this->request->post['attribute_id'])){
				$attribute_id = $this->request->post['attribute_id'];
				if(isset($attr[$attribute_id])){
					$this->request->post['attribute_code'] = $attr[$attribute_id]['attribute_code'];
				}else{
					$this->request->post['attribute_code'] = 0;
				}
			}
			if(isset($this->request->get['question_id'])){
				$data['question_id'] = $question_id = $this->request->get['question_id'];
				$this->model_survey_survey->editQuestion($question_id,$this->request->post);
			}else{
				$this->model_survey_survey->addQuestion($this->request->post);
			}
			$this->session->data['success'] = $json['success'] = $this->language->get('text_success');
		}else{
			$this->session->data['error_warning'] = $json['error'] = $this->language->get('text_error_permission');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function info() {
		$this->response->setOutput($this->index());
	}
	
}