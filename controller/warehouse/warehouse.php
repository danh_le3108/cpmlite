<?php
class ControllerWarehouseWarehouse extends Controller {
	private $no_filter_key = array('_url','route','page');

	private $error = array();

	public function index() {
		$this->load->model('catalog/attribute');
		$this->load->model('localisation/province');
		// $this->load->model('localisation/region');
		// $this->load->model('project/customer');

		$data['gifts'] = $gifts = $this->model_catalog_attribute->getAttributes(array('a.group_code'=>'GIFT','parent_attr_id !' => 0,'sort'=>'a.sort_order'));
		$data['provinces'] = $this->model_localisation_province->getProvinces();
		$data['province'] = isset($this->request->get['filter_province_id']) ? $this->model_localisation_province->getProvince($this->request->get['filter_province_id']) : '';
		$languages= $this->load->language('store/store');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'icon' => 'fa-dashboard',
			'href' => $this->url->link('common/home')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('Kho'),
			'href' => $this->url->link('warehouse/warehouse')
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);
		$this->document->setTitle('Kho');

		if (!empty($this->session->data['error_warning'])) {
			$data['error_warning'] = $this->session->data['error_warning'];
			unset($this->session->data['error_warning']);
		}
		if (!empty($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}

		$this->load->model('warehouse/warehouse');
		$whs = $this->model_warehouse_warehouse->getWarehouses($this->request->get);
		$gift_ids = array(11,12,13,14,36);
		$data['wh_gift'] = array();
		foreach ($whs as $wh) {
			foreach ($gift_ids as $gift_id) {
				$f = 'gift_'.$gift_id;
				if (empty($data['wh_gift'][$f])) {
					$data['wh_gift'][$f] = array(
						'input' => 0,
						'output' => 0
					);
				}
				$data['wh_gift'][$f]['input'] += $wh[$f] > 0 ? $wh[$f] : 0;
				$data['wh_gift'][$f]['output'] += $wh[$f] < 0 ? $wh[$f] : 0;
			}
		}

		$this->load->model('plan/plan');
		$filter = $this->request->get;
		// $filter['plan_status'] = 1;
		$plan_output = $this->model_plan_plan->countGift($filter);
		foreach ($gift_ids as $gift_id) {
			$f = 'gift_'.$gift_id;
			if (empty($data['wh_gift'][$f])) {
				$data['wh_gift'][$f]['output'] = 0;
			}
			$data['wh_gift'][$f]['output'] -= $plan_output[$f];
		}
		
		$url = $this->_url();
		$data['url'] = $this->url->link('warehouse/warehouse/getWarehouses', $url);
		$data['hasAdd'] = $this->document->hasPermission('add', 'warehouse/warehouse');
		// $data['import_store'] = $this->load->controller('excel/store_import', $url);
		$template = 'warehouse/index';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}

	public function getWarehouses() {
		$languages= $this->load->language('store/store');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		
		$page = !empty($this->request->get['page']) ? $this->request->get['page'] : 1;
		$this->load->model('warehouse/warehouse');
		$filter_data = $this->request->get;
		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $this->model_warehouse_warehouse->countTotal($filter_data);
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->url = $this->url->link('warehouse/warehouse/getWarehouses', $this->_url() . '&page={page}');

		$data['results'] = $pagination->results();
		$data['pagination'] = $pagination->render();
		
		$filter_data['start'] = ($page - 1) * 10;
		$filter_data['limit'] = 10;
		$data['warehouses'] = $this->model_warehouse_warehouse->getWarehouses($filter_data);

		$data['hasDel'] = $this->document->hasPermission('delete', 'store/store');
		$template = 'warehouse/warehouse_list';
		echo $this->load->view($template, $data);
	}

	public function edit() {
		if ($this->document->hasPermission('edit','warehouse/warehouse') && !empty($this->request->post) && $this->validateForm()) {
			$this->load->model('warehouse/warehouse');
			$warehouse_id = $this->request->get['warehouse_id'];
			$old_wh = $this->model_warehouse_warehouse->getWarehouse(array('warehouse_id'=>$warehouse_id));
			if (!empty($old_wh)) {
				$new_wh = $this->request->post;
				$new_wh['user'] = $this->user->getFullName();
				$new_wh['user_id'] = $this->user->getId();
				$this->model_warehouse_warehouse->updateWarehouse($this->request->get['warehouse_id'],$new_wh);
				$this->load->model('history/history');
				$history = array(
					'table_name' => 'warehouse',
					'user_id' => $this->user->getId(),
					'user_update' => $this->user->getFullName(),
					'controller' => 'warehouse/warehouse/edit',
					'table_primary_id' => $this->request->get['warehouse_id'],
					'data' => json_encode($new_wh),
					'old_data' => json_encode($old_wh)
				);
				$this->model_history_history->add($history);
			}
			$this->session->data['success'] = $this->language->get('Cập nhật thành công');
			$this->response->redirect($this->url->link('warehouse/warehouse'));
		} else {
			$this->getForm();
		}
	}

	public function add() {
		if ($this->document->hasPermission('add','warehouse/warehouse') && !empty($this->request->post) && $this->validateForm()) {
			$wh = $this->request->post;
			$wh['user'] = $this->user->getFullName();
			$wh['user_id'] = $this->user->getId();
			$this->load->model('warehouse/warehouse');
			$this->model_warehouse_warehouse->addWarehouse($wh);
			$this->session->data['success'] = 'Thêm thành công';
			$this->response->redirect($this->url->link('warehouse/warehouse'));
		} else {
			$this->getForm();
		}
	}

	public function getForm() {
		$this->load->model('store/store_type');
		$this->load->model('localisation/province');
		$this->load->model('localisation/region');
		$this->load->model('catalog/attribute');

		$this->load->model('catalog/attribute');
		$this->load->model('localisation/province');
		// $this->load->model('localisation/region');
		// $this->load->model('project/customer');

		$data['gifts'] = $this->model_catalog_attribute->getAttributes(array('a.group_code'=>'GIFT','parent_attr_id !' => 0,'sort'=>'a.sort_order'));
		$data['provinces'] = $this->model_localisation_province->getProvinces();

		$languages= $this->load->language('store/store');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'icon' => 'fa-dashboard',
			'href' => $this->url->link('common/home')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('warehouse/warehouse')
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);
		$this->document->setTitle('Kho');

		if (!empty($this->session->data['error_warning'])) {
			$data['error_warning'] = $this->session->data['error_warning'];
			unset($this->session->data['error_warning']);
		}
		if (!empty($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}

		if (!empty($this->request->post)) {
			$data['warehouse'] = $this->request->post;
		} elseif (!empty($this->request->get['warehouse_id'])) {
			$this->load->model('warehouse/warehouse');
			$data['warehouse'] = $this->model_warehouse_warehouse->getWarehouse(array('warehouse_id'=>$this->request->get['warehouse_id']));
		}
		$data['hasAdd'] = $this->document->hasPermission('add','warehouse/warehouse');
		$data['hasEdit'] = $this->document->hasPermission('edit','warehouse/warehouse');
		$data['error'] = $this->error;
		$template = 'warehouse/warehouse_form';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}

	public function delete() {
		if ($this->document->hasPermission('delete', 'warehouse/warehouse')) {
			$this->load->model('warehouse/warehouse');
			$this->model_warehouse_warehouse->deleteWarehouse($this->request->get['warehouse_id']);
		}
	}

	private function _url(){
		$url = '';
		foreach ($this->request->get as $key => $value) {
			if (!in_array($key, $this->no_filter_key)) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($value, ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}

	private function validateForm() {
		if (empty($this->request->post['province_id'])) {
			$this->session->data['error_warning'] = 'Chưa chọn tỉnh thành!';
			return false;
		} else {
			return true;
		}
	}

	public function autocomplete() {
		$filter_data = array(
			'start'       => 0,
			'limit'       => 20
		);
		if (!empty($this->request->get['filter_global'])) {
			$filter_data['filter_global'] = $this->request->get['filter_global'];
			$this->load->model('store/store');
			$stores = $this->model_store_store->getStores($filter_data);
			echo json_encode($stores);
		}
	}

}