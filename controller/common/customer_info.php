<?php
class ControllerCommonCustomerInfo extends Controller {
	public function index() {
		$languages = $this->load->language('common/customer_info');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		
		$data['action'] = $this->url->link('account/login', '', $this->request->server['HTTPS']);
		$data['customer_logged'] = $this->customer->isLogged();
		if($this->customer->isLogged()){
			$this->load->model('tool/image');
			$image =  $this->customer->getImage();
			if (is_file(DIR_MEDIA . $image)) {
				$image= $this->model_tool_image->resize($image, 100, 100);
			} else {
				$image = $this->model_tool_image->resize('avatar.jpg', 100, 100);
			}
			
			$data['customer'] = array(
				'fullname' =>$this->customer->getFullname(),
				'username' =>$this->customer->getUserName(),
				'group' =>$this->customer->getGroup(),
				'image' =>$image,
				'edit' =>$this->url->link('account/edit', '', true),
				'logout' =>$this->url->link('account/logout',  '', true),
			);
		}else{
			$data['customer'] = array();
		}
		
		if (!isset($this->request->get['route'])) {
			$data['redirect'] = $this->url->link('common/home');
		} else {
			$url_data = $this->request->get;

			$route = $url_data['route'];

			unset($url_data['route']);

			$url = '';

			if ($url_data) {
				$url = '&' . urldecode(http_build_query($url_data, '', '&'));
			}

			$data['redirect'] = $this->url->link($route, $url, $this->request->server['HTTPS']);
		}

		return $this->load->view('common/customer_info', $data);
	}
}