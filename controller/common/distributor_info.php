<?php
class ControllerCommonDistributorInfo extends Controller {
	public function index() {
		$languages = $this->load->language('common/customer_info');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		
		$data['action'] = $this->url->link('account/login', '', $this->request->server['HTTPS']);
		$data['distributor_logged'] = $this->distributor->isLogged();
		if($this->distributor->isLogged()){
			$this->load->model('tool/image');
			$image =  $this->distributor->getImage();
			if (is_file(DIR_MEDIA . $image)) {
				$image= $this->model_tool_image->resize($image, 100, 100);
			} else {
				$image = $this->model_tool_image->resize('avatar.jpg', 100, 100);
			}
			
			$data['distributor'] = array(
				'fullname' =>$this->distributor->getName(),
				'username' =>$this->distributor->getCode(),
				'group' =>$this->language->get('text_distributor'),
				'image' =>$image,
				'edit' =>'#',
				'logout' =>$this->url->link('account/logout',  '', true),
			);
		}else{
			$data['distributor'] = array();
		}
		
		if (!isset($this->request->get['route'])) {
			$data['redirect'] = $this->url->link('common/home');
		} else {
			$url_data = $this->request->get;

			$route = $url_data['route'];

			unset($url_data['route']);

			$url = '';

			if ($url_data) {
				$url = '&' . urldecode(http_build_query($url_data, '', '&'));
			}

			$data['redirect'] = $this->url->link($route, $url, $this->request->server['HTTPS']);
		}

		return $this->load->view('common/distributor_info', $data);
	}
}