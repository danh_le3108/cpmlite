<?php
class ControllerUserUserGroup extends Controller {
	private $error = array();


	private function _url(){
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		return $url;
	}
	public function index() {
			$this->load->model('user/group_permission');
		$languages = $this->load->language('user/user_group');
		$this->document->setTitle($this->language->get('heading_title'));
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'group_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = $this->_url();

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '' , true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('user/user_group',  $url , true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);

		$data['add'] = $this->url->link('user/user_group/add',  $url , true);
		$data['delete'] = $this->url->link('user/user_group/delete',  $url , true);

		$data['user_groups'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_project'),
			'limit' => $this->config->get('config_limit_project')
		);

		$user_group_total = $this->model_user_group_permission->getTotalUserGroups();

		$results = $this->model_user_group_permission->getUserGroups($filter_data);

		foreach ($results as $result) {
			$data['user_groups'][] = array(
				'user_group_id' => $result['user_group_id'],
				'group_name'          => $result['group_name'],
				'group_level'          => $result['group_level'],
				'group_description'          => $result['group_description'],
				'edit'          => $this->url->link('user/user_group/edit', '&user_group_id=' . $result['user_group_id'] . $url , true)
			);
		}


		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('user/user_group', '&sort=name' . $url , true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $user_group_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_project');
		$pagination->url = $this->url->link('user/user_group/getList', $url . '&page={page}' , true);

		$data['results'] = $pagination->results();
		$data['pagination'] = $pagination->render();

		

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$template = 'user/user_group_list';
		
   	 	return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}


	public function edit() {
		$this->load->language('user/user_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/group_permission');

		if ($this->user->hasPermission('edit', 'user/user_group')) {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
				
				$this->model_user_group_permission->editUserGroup($this->request->get['user_group_id'], $this->request->post);
	
				$this->session->data['success'] = $this->language->get('text_success');
	
				$url = $this->_url();
	
				// $this->response->redirect($this->url->link('user/user_group',  $url , true));
			
			}
		}else{
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return $this->getForm();
	}

	protected function getForm() {
		$languages = $this->load->language('user/user_group');
		$this->document->setTitle($this->language->get('heading_title'));
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$error_data = array(
			'error_warning'=>'',
			'error_name'=>''
		);

		foreach($error_data as $key=>$default_value){
			if (isset($this->error[$key])) {
				$data[$key] = $this->error[$key];
			} else {
				$data[$key] = $default_value;
			}
		}

		$url = $this->_url();

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '' , true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('user/user_group',  $url , true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);

		if (!isset($this->request->get['user_group_id'])) {
			$data['action'] = $this->url->link('user/user_group/add',  $url , true);
		} else {
			$data['action'] = $this->url->link('user/user_group/edit', '&user_group_id=' . $this->request->get['user_group_id'] . $url , true);
		}

		$data['cancel'] = $this->url->link('user/user_group',  $url , true);

		if (isset($this->request->get['user_group_id']) && $this->request->server['REQUEST_METHOD'] != 'POST') {
			$user_group_info = $this->model_user_group_permission->getUserGroup($this->request->get['user_group_id']);
		}
		$form_data = array('group_name'=>'');

		foreach($form_data as $key=>$default_value){
			if (isset($this->request->post[$key])) {
				$data[$key] = $this->request->post[$key];
			} elseif (!empty($user_group_info)) {
				$data[$key] = $user_group_info[$key];
			} else {
				$data[$key] = '';
			}
		}
		
		$folder_admin_ignore = array(
			'api',
			'account',
			'common',
			'startup',
			'event',
			'error');
		$route_admin_ignore = array('common/login','account/login','user/login');

		$files = array();

		// Make path into an array
		$path = array(DIR_APPLICATION . 'controller/*');

		// While the path array is still populated keep looping through
		while (count($path) != 0) {
			$next = array_shift($path);

			foreach (glob($next) as $file) {
				// If directory add to path array
				if (is_dir($file)) {
					$path[] = $file . '/*';
				}

				// Add the file to the files to be deleted array
				if (is_file($file)) {
					$files[] = $file;
				}
			}
		}
		// Sort the file array
		sort($files);
		
		$data['areas']['admin']['label'] = 'Project';
		foreach ($files as $file) {
			$controller = substr($file, strlen(DIR_APPLICATION . 'controller/'));

			$permission = substr($controller, 0, strrpos($controller, '.'));
			$part = explode('/', $permission);
			$label = ucwords(str_replace(array('/','_'),array(' &raquo; ',' '),$permission));
			if (!in_array($permission, $route_admin_ignore)&&!in_array($part[0], $folder_admin_ignore)) {
				$data['areas']['admin']['folders'][$part[0]]['label'] = ucwords($part[0]);
				$route = array('value'=>$permission, 'label'=>$label);
				$data['areas']['admin']['folders'][$part[0]]['permissions'][] = $route;
			}
		}
		if (isset($this->request->post['permission']['access'])) {
			$data['access'] = $this->request->post['permission']['access'];
		} elseif (isset($user_group_info['permission']['access'])) {
			$data['access'] = $user_group_info['permission']['access'];
		} else {
			$data['access'] = array();
		}
		if (isset($this->request->post['permission']['add'])) {
			$data['add'] = $this->request->post['permission']['add'];
		} elseif (isset($user_group_info['permission']['add'])) {
			$data['add'] = $user_group_info['permission']['add'];
		} else {
			$data['add'] = array();
		}

		if (isset($this->request->post['permission']['edit'])) {
			$data['edit'] = $this->request->post['permission']['edit'];
		} elseif (isset($user_group_info['permission']['edit'])) {
			$data['edit'] = $user_group_info['permission']['edit'];
		} else {
			$data['edit'] = array();
		}
		if (isset($this->request->post['permission']['delete'])) {
			$data['delete'] = $this->request->post['permission']['delete'];
		} elseif (isset($user_group_info['permission']['delete'])) {
			$data['delete'] = $user_group_info['permission']['delete'];
		} else {
			$data['delete'] = array();
		}

		$template = 'user/user_group_form';
   	 	return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}

	protected function validateForm() {

		if (!$this->user->hasPermission('edit','user/user_group')) {
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return !$this->error;
	}
}