<?php
class ControllerUserUser extends Controller {
	private $error = array();
	private $field_data = array(
				'user_id'                => '',
				'usercode'               => '',
				'username'               => '',
				'fullname'               => '',
				'email'                  => '',
				'telephone'              => '',
				'image'                  => '',
				'images'                  => '{"front_identity_card":"","back_identity_card":"","portrait":"","signature":""}',
				'active'                 => 0,
				'p_identity_number'      => '',
				'p_identity_issue_date'  => '',
				'p_identity_issue_place' => '',
				'p_tax_code'             => '',
				'p_insurance_num'        => '',
				'p_belonging_persons'    => '',
				'p_relative_telephone'   => '',
				'p_birthday'             => '',
				'p_place_of_birth'       => '',
				'p_nationality'          => '',
				'p_nation'               => '',
				'p_degree'               => '',
				'p_re_address'           => '',
				'p_re_province_id'       => '',
				'p_re_district_id'       => '',
				'p_re_ward_id'           => '',
				'p_so_address'           => '',
				'p_so_province_id'       => '',
				'p_so_district_id'       => '',
				'p_so_ward_id'           => '',
				'p_bi_address'           => '',
				'p_bi_province_id'       => '',
				'p_bi_district_id'       => '',
				'p_bi_ward_id'           => '',
				'p_protector'            => '',
				'p_bank_holder'          => '',
				'p_bank_number'          => '',
				'p_bank_name'            => '',
				'area_id'                => '',
				'region_id'              => '',
				'province_id'            => '',
				'district_id'            => '',
				'ward_id'                => '',
				'address'                => '',
				'user_group_id'          => '',
				'user_department_id'     => '',
				'user_level_id'          => '',
				'user_register'          => '',
	);
	protected function validateForm() {
		$this->load->language('user/user');

		if ($this->request->post['password']) {
			if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
				$this->error['error_password'] = $this->language->get('text_error_password');
			}

			if ($this->request->post['password'] != $this->request->post['confirm']) {
				$this->error['error_confirm'] = $this->language->get('text_error_confirm');
			}
		}

		return !$this->error;
	}
	public function index() {
		$this->load->model('user/user');
		if (!$this->user->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('user/user', '', true);
			$this->response->redirect($this->url->link('user/login', '', true));
		}
		$this->load->model('tool/image');
		$data['placeholder'] = $placeholder =  $this->model_tool_image->resize('no_image.png', 100, 100);

		$this->document->addStyle('assets/plugins/magnific/magnific-popup.css');
		$this->document->addScript('assets/plugins/magnific/jquery.magnific-popup.js');

		$this->document->addScript('assets/editor/plugins/ajaxupload.js');
		$languages= $this->load->language('user/user');
		$this->document->setTitle($this->language->get('heading_title'));
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

				$this->model_user_user->editUser($this->user->getId(), $this->request->post);

				$this->session->data['success'] = $this->language->get('text_success');


				$this->response->redirect($this->url->link('user/user', '', true));

		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$error_data = array(
			'error_warning'=>'',
			'error_usercode'=>'',
			'error_username'=>'',
			'error_password'=>'',
			'error_confirm'=>'',
			'error_fullname'=>'',
			'error_email'=>''
		);

		foreach($error_data as $key=>$default_value){
			if (isset($this->error[$key])) {
				$data[$key] = $this->error[$key];
			} else {
				$data[$key] = $default_value;
			}
		}

		$url = '';;

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'icon' => 'fa-dashboard',
			'href' => $this->url->link('common/home', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('user/user',  $url, true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);

		$data['action'] = $this->url->link('user/user', '', true);

		$data['cancel'] = $this->url->link('common/home', '', true);


		if ($this->user->getId() && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$query_info = $this->model_user_user->getUser($this->user->getId());
		}
		foreach($this->field_data as $key=>$default_value){
			if (isset($this->request->post[$key])) {
				$data[$key] = $this->request->post[$key];
			} elseif (!empty($query_info)) {
				$data[$key] = $query_info[$key];
			} else {
				$data[$key] = $default_value;
			}
		}
		$data['user_images'] = array();
		$data['group_name'] = $this->user->getGroup();
		$user_id = $this->user->getId();
		$username = $this->user->getUsername();
		$data['group_name'] = $this->user->getGroup();

		$img_href = HTTP_SERVER.'media/files/users/' .$username.'/';
		$img_folder = 'files/users/' . $username.'/';

		$user_images = array('portrait'=>'','front_identity_card'=>'','back_identity_card'=>'','signature'=>'');
		foreach($user_images as $key=>$value){
			if(!empty($query_info[$key])&&file_exists(DIR_MEDIA.$img_folder.$query_info[$key])){
				$thumb = $this->model_tool_image->resize($img_folder.$query_info[$key], 200, 200);
				$popup = $img_href.$query_info[$key];
			}else{
				$thumb = $this->model_tool_image->resize('no_image.png', 200, 200);
				$popup = $thumb;
			}
			$data['user_images'][$key] = array(
				'key'=>$key,
				'label'=>$this->language->get('text_'.$key),
				'value'=>$value,
				'thumb'=>$thumb,
				'popup'=>$popup,
			);
		}

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}

		if (isset($this->request->post['confirm'])) {
			$data['confirm'] = $this->request->post['confirm'];
		} else {
			$data['confirm'] = '';
		}


		$this->load->model('localisation/province');
		$data['provinces'] = $this->model_localisation_province->getProvinces();

		$this->load->model('localisation/district');
		$data['districts'] = $this->model_localisation_district->getDistrictsByProvinceId($data['province_id']);

		$this->load->model('localisation/ward');
		$data['wards'] = $this->model_localisation_ward->getWardsByDistrictId($data['district_id']);

		if (isset($this->request->post['image']) && is_file(DIR_MEDIA .$img_folder. $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($img_folder.$this->request->post['image'], 100, 100);
		} elseif (!empty($query_info) && $query_info['image'] && is_file(DIR_MEDIA . $img_folder.$query_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($img_folder.$query_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('avatar.jpg', 100, 100);
		}

			$data['http_server'] = HTTP_SERVER;
			
		$template = 'user/user_form';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}

	public function image() {

		if (!$this->user->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('user/user', '', true);
			$this->response->redirect($this->url->link('user/login', '', true));
		}


		$this->load->model('user/user');
		$language_data = $this->load->language('user/user');
		$this->load->model('tool/image');
		$json = array();
			$user_info = $this->model_user_user->getUser($this->user->getId());

		if (!empty($user_info['username'])&&!empty($this->request->files['file']['name'])) {
			$full_filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
			if ((utf8_strlen($full_filename) < 3) || (utf8_strlen($full_filename) > 255)) {
				$json['error'] = $this->language->get('error_filename');
			}
			$filename=(string)$full_filename;
			$full_name = explode(".",$filename);

			$filename_prefix =$user_info['user_id'].'-'.$user_info['username'].'-'.substr(md5(mt_rand()),0,8);

			$filename_ext =strtolower(substr(strrchr($filename, '.'), 1));
			$filename = $filename_prefix.'.'.$filename_ext;
			$allowed = array('jpg','png','gif','jpeg');
			if (!in_array(strtolower($filename_ext), $allowed)) {
				$json['error'] = $this->language->get('error_filetype');
       		}
			if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
				$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
			}/*
			if ($this->request->files['file']['size']>500000) {
				//$json['error'] = $this->language->get('error_upload_file_size');
			}
			$img_info = getimagesize($this->request->files["file"]["tmp_name"]);
			$img_width = $img_info[0];
			$img_height = $img_info[1];*/ 
		} else {
			$json['error'] = $this->language->get('error_upload');
		}

		if (!isset($json['error'])) {
				if (is_uploaded_file($this->request->files['file']['tmp_name']) && file_exists($this->request->files['file']['tmp_name'])) {
					$folder = DIR_MEDIA.'files/users/' .  $user_info['username'];
					if (!is_dir($folder)){
						@mkdir($folder, 0777, true);
						@touch($folder. 'index.html');
					}
					if(file_exists($folder .'/'.$filename)){
						@unlink($folder.'/'.$filename);
					}
					move_uploaded_file($this->request->files['file']['tmp_name'], $folder.'/'.$filename);
					$json['filename'] = $filename;

					$json['thumb']= $this->model_tool_image->resize('files/users/' .  $user_info['username'].'/'.$filename, 100, 100);
			}

			$json['success'] = $this->language->get('text_success_upload');
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete() {
		$json = array();
		$filter_data = array(
			'sort'        => 'fullname',
			'order'       => 'ASC',
			'start'       => 0,
			'limit'       => 20
		);
		if (isset($this->request->get['filter_global'])) {
			$filter_data['filter_global'] = $this->request->get['filter_global'];
		}
		if (isset($this->request->get['filter_username'])) {
			$filter_data['filter_username'] = $this->request->get['filter_username'];
		}
		if (isset($this->request->get['filter_usercode'])) {
			$filter_data['filter_usercode'] = $this->request->get['filter_usercode'];
		}
		if (isset($this->request->get['filter_fullname'])) {
			$filter_data['filter_fullname'] = $this->request->get['filter_fullname'];
		}
		if (isset($this->request->get['filter_user_group_id'])) {
			$filter_data['filter_user_group_id'] = $this->request->get['filter_user_group_id'];
		}
			$this->load->model('user/user');

			$results = $this->model_user_user->getUsers($filter_data);
			
		$this->load->model('tool/image');
		$thumb = $this->model_tool_image->resize('avatar.jpg', 100, 100);
		

			foreach ($results as $result) {
				$imag_path = 'files/users/' . $result['username'].'/';
				$img_href = HTTP_SERVER.'media/files/users/' . $result['username'].'/';
				if (!empty($result) && $result['image'] && is_file(DIR_MEDIA .$imag_path. $result['image'])) {
					$avatar[$result['user_id']] = $this->model_tool_image->resize($imag_path.$result['image'], 100, 100);
				}else{
					$avatar[$result['user_id']] = $thumb;
				}
				$json[] = array(
					'user_id' => $result['user_id'],
					'user_group_id' => $result['user_group_id'],
					'group_name' => $result['group_name'],
					'username' => $result['username'],
					'usercode' => $result['usercode'],
					'email' => $result['email'],
					'telephone' => $result['telephone'],
					'thumb' => $avatar[$result['user_id']],
					'fullname'        => html_entity_decode($result['fullname'], ENT_QUOTES, 'UTF-8')
				);
			}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['fullname'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function quickAddUser(){
		$language_data = $this->load->language('user/user');
		foreach($language_data as $key=>$value){
			$data[$key] = $value;
		}

		if(($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$json = array();
			$json['error'] = array();
			$this->load->model('user/user');

			if ($this->request->post['province_id'] == '*') {
				$json['error']['province_id'] = $this->language->get('text_error_province_id');
			}

			if ((utf8_strlen($this->request->post['fullname']) < 2) || (utf8_strlen($this->request->post['fullname']) > 20) || empty($this->request->post['fullname'])) {
				$json['error']['fullname'] = $this->language->get('text_error_fullname');
			}

			if (!empty($this->request->post['email'])) {
				$user_info = $this->model_user_user->getUserByEmail($this->request->post['email']);

				if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
					$json['error']['email'] = $this->language->get('text_error_email');
				} elseif ($user_info) {
					$json['error']['email'] = $this->language->get('text_error_email_exist');
				}
			} elseif(empty($this->request->post['email'])){
				$json['error']['email'] = $this->language->get('text_error_email');
			}

			if (!empty($this->request->post['p_identity_number'])) {
				$user_info = $this->model_user_user->getUserByIdentityNumber($this->request->post['p_identity_number']);

				if ($user_info) {
					$json['error']['identity_number'] = $this->language->get('text_error_identity_number_exist');
				}
			} elseif(empty($this->request->post['p_identity_number'])){
				$json['error']['identity_number'] = $this->language->get('text_error_identity_number');
			}

			if ((utf8_strlen($this->request->post['telephone']) < 2) || (utf8_strlen($this->request->post['telephone']) > 20)) {
				$json['error']['telephone'] = $this->language->get('text_error_telephone');
			}

			if(empty($json['error'])){
				$this->load->model('localisation/province');
				$province = $this->model_localisation_province->getProvince($this->request->post['province_id']);

				$this->request->post['password'] = $data['password'] = 'cpmvietnam'.rand(0,9999);

				$user_id = $this->model_user_user->addUser($this->request->post);

				$username = $province['code'] . sprintf('%05d', $user_id);

				$this->model_user_user->updateUsername($user_id, $username);

				$user = $this->model_user_user->getUser($user_id);

				$data['fullname'] = $user['fullname'];
				$data['username'] = $user['username'];

				$languages = $this->load->language('mail/welcome');
				foreach($languages as $key => $value){
					$data[$key] = $value;
				}

				$data['store_name'] = $this->config->get('config_name');
				$data['logo'] = $this->config->get('config_logo');

				$subject = sprintf($this->language->get('text_subject'), html_entity_decode($user['fullname'], ENT_QUOTES, 'UTF-8'));

				$text  = sprintf($this->language->get('text_greeting'), html_entity_decode($user['fullname'], ENT_QUOTES, 'UTF-8')) . "\n\n";
				$_email = $this->config->get('config_email') ? $this->config->get('config_email') : 'qc@cpm-vietnam.com.vn';
				$data['link_app'] = '';
				$data['link_web'] = 'http://retailbuild.vn/admin/';

				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
				$mail->smtp_username = $this->config->get('config_mail_smtp_username');
				$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
				$mail->smtp_port = $this->config->get('config_mail_smtp_port');
				$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

				$mail->setTo($this->request->post['email']);
				$mail->setFrom($_email);
				$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
				$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
				$mail->setHtml($this->load->view('mail/welcome', $data));
				$mail->setText($text);
				$mail->send();

				$json['success'] = $this->language->get('text_success');
			}



			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}
	}
}