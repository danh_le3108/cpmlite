<?php
class ControllerDistributorCouponScan extends Controller{
	private $error = array();
	
	private $field_data = array(
			'coupon_scan_id'=>'',
			'coupon_scan_name'=>''
		);

	private function _url(){
		$url = '';
		$url_key = array(
			'filter_prefix',
			'filter_name'
		);
		foreach($url_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}
	public function delete() {
		$languages = $this->load->language('distributor/coupon_scan');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('distributor/coupon_scan');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $coupon_scan_id) {
				$this->model_distributor_coupon_scan->deleteCouponScan($coupon_scan_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = $this->_url();

			$this->response->redirect($this->url->link('distributor/coupon_scan',  $url , true));
		}

		return $this->index();
	}

	public function index() {
		$data['ajax_list'] = $this->ajax_list();
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$template = 'common/ajax_list';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
	public function getList() {
		$this->response->setOutput($this->ajax_list());
	}
	public function ajax_list() {
		$languages = $this->load->language('distributor/coupon_scan');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('distributor/coupon_scan');
		
		$filter_key = array(
			'filter_name'=>null
		);
		foreach($filter_key as $key=>$value){
			if (isset($this->request->get[$key])) {
				$$key = $this->request->get[$key];
			} else {
				$$key = $value;
			}
		}
		
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'coupon_scan_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = $this->_url();
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['add'] = $this->url->link('distributor/coupon_scan/add',  $url , true);
		$data['delete'] = $this->url->link('distributor/coupon_scan/delete',  $url , true);

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '' , true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('distributor/coupon_scan',  $url , true)
		);

		$this->document->setBreadcrumbs($data['breadcrumbs']);

		$data['categories'] = array();

		$filter_data = array(
			'filter_name'=>$filter_name,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_project'),
			'limit' => $this->config->get('config_limit_project')
		);

		$query_total = $this->model_distributor_coupon_scan->getTotalCouponScanes($filter_data);

		$results = $this->model_distributor_coupon_scan->getCouponScanes($filter_data);

		foreach ($results as $result) {
			$data['categories'][] = array(
				'coupon_scan_id' => $result['coupon_scan_id'],
				'coupon_scan_name'       => $result['coupon_scan_name'],
				'edit'       => $this->url->link('distributor/coupon_scan/edit', '&coupon_scan_id=' . $result['coupon_scan_id'] . $url , true)
			);
		}


		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = $this->_url();

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('distributor/coupon_scan', '&sort=name' . $url , true);

		$url = $this->_url();

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $query_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_project');
		$pagination->url = $this->url->link('distributor/coupon_scan/getList', $url . '&page={page}' , true);

		$data['pagination'] = $pagination->render();

		$data['results'] = $pagination->results();

		$data['sort'] = $sort;
		$data['order'] = $order;

		
		$data['delete'] = $this->url->link('distributor/coupon_scan/delete',  $url , true);

		$data['filter_name'] = $filter_name;
		

		$template = 'distributor/coupon_scan_list';
		return $this->load->view($template, $data);
	}

	public function add() {
		$languages = $this->load->language('distributor/coupon_scan');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('distributor/coupon_scan');

		if ($this->user->hasPermission('add', 'distributor/coupon_scan')) {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
				
				$this->model_distributor_coupon_scan->addCouponScan($this->request->post);
	
				$this->session->data['success'] = $this->language->get('text_success');
	
				$url = $this->_url();
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
	
				$this->response->redirect($this->url->link('distributor/coupon_scan',  $url , true));
			}
		}else{
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return $this->getForm();
	}

	public function edit() {
		$languages = $this->load->language('distributor/coupon_scan');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('distributor/coupon_scan');

		if ($this->user->hasPermission('edit', 'distributor/coupon_scan')) {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
				$this->model_distributor_coupon_scan->editCouponScan($this->request->get['coupon_scan_id'], $this->request->post);
				$this->session->data['success'] = $this->language->get('text_success');
	
				$url = $this->_url();
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
	
				$this->response->redirect($this->url->link('distributor/coupon_scan',  $url , true));
			}
		}else{
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return $this->getForm();
	}

	protected function getForm() {
		$languages = $this->load->language('distributor/coupon_scan');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('heading_title'));
		
		
		$error_data = array(
			'error_warning'=>'',
			'error_name'=>''
		);

		foreach($error_data as $key=>$default_value){
			if (isset($this->error[$key])) {
				$data[$key] = $this->error[$key];
			} else {
				$data[$key] = $default_value;
			}
		}

		$url = $this->_url();
		
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '' , true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('distributor/coupon_scan',  $url , true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);

		if (!isset($this->request->get['coupon_scan_id'])) {
			$data['action'] = $this->url->link('distributor/coupon_scan/add',  $url , true);
		} else {
			$data['action'] = $this->url->link('distributor/coupon_scan/edit', '&coupon_scan_id=' . $this->request->get['coupon_scan_id'] . $url , true);
		}

		$data['cancel'] = $this->url->link('distributor/coupon_scan',  $url , true);

		if (isset($this->request->get['coupon_scan_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$query_info = $this->model_distributor_coupon_scan->getCouponScan($this->request->get['coupon_scan_id']);
		}
		foreach($this->field_data as $key=>$default_value){
			if (isset($this->request->post[$key])) {
				$data[$key] = $this->request->post[$key];
			} elseif (!empty($query_info)) {
				$data[$key] = $query_info[$key];
			} else {
				$data[$key] = $default_value;
			}
		}
		

		$template = 'distributor/coupon_scan_form';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('delete', 'distributor/coupon_scan')) {
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return !$this->error;
	}
	protected function validateAdd() {
		if (!$this->user->hasPermission('add', 'distributor/coupon_scan')) {
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		if ((utf8_strlen($this->request->post['coupon_scan_name']) < 3) || (utf8_strlen($this->request->post['coupon_scan_name']) > 128)) {
			$this->error['error_name'] = $this->language->get('text_error_name');
		}

		return !$this->error;
	}
	protected function validateForm() {

		if (!$this->user->hasPermission('add', 'distributor/coupon_scan')) {
			$this->error['error_warning'] = $this->language->get('text_error_permission');
		}

		return !$this->error;
	}
	public function coupon_scan() {
		$json = array();

		$this->load->model('distributor/coupon_scan');

		$results = $this->model_distributor_coupon_scan->getCouponScanes();

		foreach ($results as $result) {
			$json['category'][] = array(
				'coupon_scan_id' => $result['coupon_scan_id'],
				'coupon_scan_name'       => $result['coupon_scan_name']
			);
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
}