<?php
class ControllerDistributorCouponHistory extends Controller {
	private $error = array();
	
  
	private $field_data = array(
			'coupon_id'=>'',
			'coupon_code'=>'',
			'distributor_code'=>''
		);
		
		
	/*Excel*/ 
	private $template_id = 8;
	private $import_check_id = 9;
	
	private $filter_key = array(
		'filter_coupon_code',
		'filter_round',
		'filter_yearmonth',
		'filter_store_code',
		'filter_reward_type_id',
		'filter_coupon_scan',
		'filter_date_start',
		'filter_date_end',
		'filter_distributor_id'
	);
	private function _url(){
		$url = '';
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}
	
	/*Huy Coupon*/ 
	public function import_check(){
		$customer_id = 0;
		if($this->customer->isLogged()){
			$customer_id = $this->customer->getId();
		}
 		$config_check_coupon_user_ids = $this->config->get('config_check_coupon_user_ids');
		
		
		$languages = $this->load->language('catalog/coupon');
		
		//Nếu có quyền && (NPP đăng nhập || Customer có quyền || User CPM )
		
		if (($this->distributor->isLogged()||($this->customer->isLogged()&&in_array($customer_id,$config_check_coupon_user_ids))||$this->user->isLogged())&&$this->document->hasPermission('access', 'distributor/coupon_history')) {
			
			if (($this->request->server['REQUEST_METHOD'] == 'POST') &&!empty($this->request->files['file_import']['name'])&&substr($this->request->files['file_import']['name'], -5) == '.xlsx') {
				
	
				$template_id = $this->import_check_id;
	
				if($this->distributor->isLogged()){
					$export_name = $this->distributor->getCode();
				}else if($this->customer->isLogged()){
					$export_name = $this->customer->getUserName();
				}else{
					$export_name = $this->user->getUserName();
				}
	
				$file_name = 'Coupon-Check-by-' . $export_name. '-at-'. date('Ymd-His') .'.xlsx';
	
				$dir = DIR_MEDIA . 'files/'. $this->config->get('config_project_folder') .'/import/coupon_check/';
		
			  if(!is_dir($dir)){
				@mkdir($dir, 0777, true);
				@touch($dir. 'index.html');
			  }
	
		  $file = $dir . $file_name;
	
		  move_uploaded_file($this->request->files['file_import']['tmp_name'], $file);
		  if(file_exists($file)){
			$template_info = $this->excel->getTemplate($template_id);
			  $filesize = $this->request->files['file_import']['size'];
			  $import_info = $this->excel->getColRow($file);
			  $dist_import_id = $this->excel->addImportHistory($template_info['template_id'], $file, $import_info);
			  $history_info = $this->excel->getImportHistory($dist_import_id);
					$template_info = $this->excel->getTemplate($history_info['template_id']);
	
			$settings = $template_info['settings'];
			
			$this->load->model('catalog/coupon_history');
					
			$filter_data['filter_coupon_status'] = $this->config->get('config_coupon_has_paid');
					
			if($this->distributor->isLogged()){
				$filter_data['filter_distributor_id'] = $this->distributor->isLogged();
			}
		
			$results = $this->model_catalog_coupon_history->getCouponsHistory($filter_data);
				
			$coupon = $this->mapping_coupon($results,'coupon_code');
	
			
				//print_r('<pre>'); print_r($coupon); print_r('</pre>'); die();
	
	
					$header_check = array();
					$result = $this->excel->readFileCellKey($history_info['template_path'], $settings['start_row'], $header_check, $settings['sheet_index']);
					
					$error_data = array();
							
					$error_row = $success_row = 0;	
							
					foreach($result['data'] as $row => $excel_row) {
							$message = array();
							$coupon_code = strtoupper(trim($excel_row['A'])); 
							$store_code = strtoupper(trim($excel_row['B'])); 
							if(isset($coupon[$coupon_code])&&($coupon[$coupon_code]['store_code']==$store_code||$coupon[$coupon_code]['store_customer_code']==$store_code||$coupon[$coupon_code]['store_ad_code']==$store_code)){
								$coupon[$coupon_code]['dist_import_id'] =$dist_import_id; 
								$this->model_catalog_coupon_history->updateDistributorCheck($coupon[$coupon_code]);
								
								if($coupon[$coupon_code]['dist_check']>0){
									$message[] = 'Trùng mã!';
								}else{
									$message[] = 'Đúng mã Coupon và Đúng mã Cửa hàng!';
								}
								
								$success_row++;
							}else{
								$message[] = 'Không hợp lệ!';
								$error_row++;
							}
							$error_data[$row] = $excel_row;
							$error_data[$row]['message'] = implode(' | ', $message);
					}//for
					
					$history_info['import_info']['error'] = $error_data;
					$history_info['import_info']['error_row'] = $error_row;
					$history_info['import_info']['success_row'] = $success_row;
					
					$this->excel->updateHistoryProgress($dist_import_id, $history_info['import_info']);
			
			
					$download_result = $this->url->link('distributor/coupon_history/download_history', '&import_id=' .$dist_import_id , true);
			
					$this->session->data['success'] = $this->language->get('Import thành công, click vào đây để tải về file chi tiết! <a class="btn btn-sm btn-primary" href="'.$download_result.'">Tải về</a>');
					
				} else {
					 $this->session->data['error_warning'] = $this->language->get('File không đúng chuẩn!');
				}
			}  else {
				 $this->session->data['error_warning'] = $this->language->get('Không có file gửi lên!');
			}
		}  else {
			 $this->session->data['error_warning'] = $this->language->get('Bạn không có quyền truy cập khu vực này!');
		}

		$this->response->redirect($this->url->link('distributor/coupon_history',  '' , true));
	}
	public function download_history(){

		if(isset($this->request->get['import_id'])){
			$import_id = $this->request->get['import_id'];
		} else {
			$import_id = 0;
		}

		if($import_id > 0){

			$this->load->model('excel/handle');

			$history_data = $this->excel->getImportHistory($import_id);

			$error = $history_data['import_info']['error'];

			$template_id = $history_data['template_id'];

			$template_info = $this->excel->getTemplate($template_id);

			if(!empty($template_info['file_path'])){
				
				$export_data = array(
					'excel_data' => $error,
					'template_id' => $template_id,
					'template_name' => 'CouponCheck-Report-'.$import_id.'-'. time() .'.xlsx'
				);


				$this->excel->exportWithTemplate($export_data);
				
			} else { 
				$this->model_excel_handle->download($template_id, $error);
			}

		}

		$this->response->redirect($this->url->link('excel/template',   '' , true));
	}
	
	public function check() {
		$languages = $this->load->language('catalog/coupon');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$url = '';//$this->_url();
			
		
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/coupon_history');
		
		$filter_data = array();
		
		$filter_data['filter_coupon_status'] = $this->config->get('config_coupon_has_paid');
				
		if($this->distributor->isLogged()){
			$filter_data['filter_distributor_id'] = $this->distributor->isLogged();
		}
	
			


		
		if ($this->document->hasPermission('access', 'distributor/coupon_history')&&($this->request->server['REQUEST_METHOD'] == 'POST') ) {
			
			$coupon_code = NULL;
			if (!isset($this->request->post['coupon_code'])||empty($this->request->post['coupon_code'])) {
				$this->session->data['error_coupon_code'] = $this->language->get('Mã coupon là bắt buộc');
			}else{
				$coupon_code = trim($this->request->post['coupon_code']);
				$url .= '&filter_coupon_code=' . $coupon_code;
				$this->session->data['coupon_code'] = $coupon_code;
			}
			$store_code = NULL;
			if (!isset($this->request->post['store_code'])||empty($this->request->post['store_code'])) {
				$this->session->data['error_store_code'] = $this->language->get('Mã Cửa hàng là bắt buộc');
			}else{
				$store_code = trim($this->request->post['store_code']);
				$this->session->data['store_code'] = $store_code;
			}
			if(!empty($coupon_code)&&!empty($store_code)){
				
				$coupon_info = $this->model_catalog_coupon_history->getCouponsHistoryByCode($coupon_code,$filter_data);
		
				if(!empty($store_code)&&($coupon_info['store_code']==$store_code||$coupon_info['store_customer_code']==$store_code||$coupon_info['store_ad_code']==$store_code)){
					
					$query= $this->model_catalog_coupon_history->updateDistributorCheck($coupon_info);
					
					
					if($coupon_info['dist_check']==1){
						$this->session->data['success'] = $this->language->get('Xác nhận coupon đã thu hồi về NPP!');
					}else{
						$this->session->data['error_warning'] = $this->language->get('Trùng mã - Mã này đã thu hồi!');	
					}
					
				}else{
					 $this->session->data['error_warning'] = $this->language->get('Không hợp lệ!');	
				}

			}
		
			
		}else{
			$this->session->data['error_warning'] = $this->language->get('text_error_permission');
		}

		$this->response->redirect($this->url->link('distributor/coupon_history',  $url , true));
	}
	
	public function index() {
		$data['ajax_list'] = $this->ajax_list();
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$template = 'common/ajax_list';
		return $this->load->controller('startup/builder',$this->load->view($template, $data));
	}
	public function getList() {
		$this->response->setOutput($this->ajax_list());
	}
	public function ajax_list() {
		
			$this->load->model('plan/plan');
			$this->model_plan_plan->activeMissingCoupons();
			
			
		$languages = $this->load->language('catalog/coupon');
		foreach($languages as $key=>$value){
				$data[$key] = $value;	
		}
		$this->document->setTitle($this->language->get('text_coupon_history'));
		
		$this->load->model('catalog/coupon_history');
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'coupon_code';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = $this->_url();
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '' , true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/coupon',  $url , true)
		);
		$this->document->setBreadcrumbs($data['breadcrumbs']);



		$filter_data = array(
			'sort'                    => $sort,
			'order'                   => $order,
			'start'                   => ($page - 1) * $this->config->get('config_limit_project'),
			'limit'                   => $this->config->get('config_limit_project')
		);
		
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$filter_data[$key] = $this->request->get[$key];
			}
		}
		
		
			$filter_data['filter_coupon_status'] = $this->config->get('config_coupon_has_paid');
				
		if($this->distributor->isLogged()){
			$filter_data['filter_distributor_id'] = $this->distributor->isLogged();
		}
		

		$this->load->model('catalog/coupon_status');
		$this->load->model('plan/plan');
		
		$this->load->model('catalog/reward_type');
		$data['reward_types'] = $this->model_catalog_reward_type->getRewardTypes();

		$data['coupon_status'] = $this->model_catalog_coupon_status->getCouponStatuses();

		$filter_paid = $filter_data;
		$filter_paid['filter_coupon_status'] = $this->config->get('config_coupon_has_paid');
		$filter_paid['filter_coupon_scan'] = $this->config->get('config_dist_valid');
		$data['paid_total'] = $this->model_catalog_coupon_history->getTotalCouponsHistory($filter_paid);
		
		
		$query_total = $this->model_catalog_coupon_history->getTotalCouponsHistory($filter_data);

		$results = $this->model_catalog_coupon_history->getCouponsHistory($filter_data);

		
		$data['coupons'] = $this->mapping_coupon($results);
		
		$data_sessions = array(
			'error_coupon_code',
			'error_store_code',
			'error_warning',
			'coupon_code',
			'store_code',
			'success'
		);
		foreach($data_sessions as $ss){
			if (isset($this->session->data[$ss])) {
				$data[$ss] = $this->session->data[$ss];
				unset($this->session->data[$ss]);
			} else {
				$data[$ss] = '';
			}
		}
		

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = $this->_url();

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/coupon', '&sort=name' . $url , true);

		$url = $this->_url();

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination($this->language->get('text_pagination'));
		$pagination->total = $query_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_project');
		$pagination->url = $this->url->link('distributor/coupon_history', $url . '&page={page}' , true);

		$data['pagination'] = $pagination->render();

		$data['results'] = $pagination->results();

		$data['total'] = $query_total;
		$data['sort'] = $sort;
		$data['order'] = $order;
		
		foreach($this->filter_key as $key){
			if (isset($this->request->get[$key])) {
				$data[$key] = $this->request->get[$key];
			}else{
				$data[$key] = NULL;
			}
		}
		/**/ 
		$data['yearmonths'] =  $this->model_catalog_coupon_history->getYearMonths();
		
	
		$this->load->model('plan/round');
		$data['rounds'] =  $this->model_plan_round->getRounds();
	
		$this->load->model('catalog/distributor');
		$data['distributors'] =  $this->model_catalog_distributor->getDistributors();
		
		$this->load->model('project/project_user');
		$data['users'] =  $this->model_project_project_user->getProjectUsersByGroupID($this->config->get('config_user_sup'));
		
		
		$this->load->model('distributor/coupon_scan');
		$data['coupon_scans'] = $this->model_distributor_coupon_scan->getCouponScanesByIndex();
		
		$data['allow_filter'] = array($this->config->get('config_reward_audit'),$this->config->get('config_reward_old'));
		
			if($this->distributor->isLogged()){
				$data['filter_distributor_id'] = $this->distributor->isLogged();
				$data['isLogged'] = true;
			}else{
				$data['isLogged'] = false;
			}
		/**/ 	
		 
		$data['template_id'] = $this->template_id;
		$import_check_id = $this->import_check_id;
		$data['template_url'] = $this->url->link('excel/template/template',  'template_id='.$this->import_check_id, true);
		$data['import_check'] = $this->url->link('distributor/coupon_history/import_check', '' , true);
		$data['check_form'] = $this->url->link('distributor/coupon_history/check', '' , true);
		$data['export'] = $this->url->link('distributor/coupon_history/export', $url , true);

		$data['has_edit'] = $this->document->hasPermission('access', 'distributor/coupon_history');
		$data['report'] = $this->url->link('distributor/coupon_history/report', $url , true);
		
		
		$template = 'distributor/coupon_history';
		return $this->load->view($template, $data);
	}
	private function mapping_coupon($coupons,$index_by=NULL){
		$coupons_data = array();
			$url = $this->_url();
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
		if(!empty($coupons)){
			$this->load->model('catalog/coupon_history');
			
			$this->load->model('catalog/reward_type');
			$reward_type = $this->model_catalog_reward_type->getRewardTypesIndexBy('reward_type_id');
		
			$this->load->model('project/project_user');
			$user = $this->model_project_project_user->getProjectUsers();
			
			$this->load->model('store/store');
			$store_fields = array(
				'store_id',
				'store_name',
				'store_code',
				'store_customer_code',
				'store_ad_code',
				'distributor_code',
				'store_distributor',
				'store_address_raw'
			);
			$store = $this->model_store_store->getStoreByFields($store_fields,'store_id');
			
			$this->load->model('catalog/coupon_prefix');
			$prefix =  $this->model_catalog_coupon_prefix->getPrefixsIndexBy('coupon_prefix');
			
			
			$this->load->model('catalog/distributor');
			$distributor = $this->model_catalog_distributor->getDistributorIndexBy('distributor_id');
			
			$this->load->model('catalog/coupon_status');
			$coupon_status = $this->model_catalog_coupon_status->getCouponStatuses();
		
			$this->load->model('distributor/coupon_scan');
			$scans =$this->model_distributor_coupon_scan->getCouponScanesByIndex();
			
				
			$config_coupon_has_paid = $this->config->get('config_coupon_has_paid');
			
			$config_dist_valid = $this->config->get('config_dist_valid');	
				
			foreach ($coupons as $result) {
				if($result['plan_id']>0){
					$year_month = explode('-',$result['round_name']);
					$year  = $year_month[0];
					$month  = $year_month[1];
					if(isset($store[$result['store_id']])){
						$store_info = $store[$result['store_id']];
						$store_name = $store_info['store_name'];
						$store_code = $store_info['store_code'];
						$store_customer_code = $store_info['store_customer_code'];
						$store_ad_code = $store_info['store_ad_code'];
						$store_address_raw = $store_info['store_address_raw'];
					} else {
						$store_name = '';
						$store_code = '';
						$store_customer_code = '';
						$store_ad_code = '';
						$store_address_raw = '';
					}
					
					
					if(isset($scans[$result['dist_check_id']])){
						$coupon_scan_name = $scans[$result['dist_check_id']]['coupon_scan_name'];
					}else{
						$coupon_scan_name = '';
					}
					
					if(isset($distributor[$result['distributor_id']])){
						$dinfo = $distributor[$result['distributor_id']];
						$distributor_code = $dinfo['distributor_code'];
						$distributor_name = $dinfo['distributor_name'];
					}else{
						$distributor_name = $distributor_code = '';
					}
		
					$plan_href = ($result['plan_id']>0)?$this->url->link('plan/plan/edit', '&plan_id=' . $result['plan_id']  , true):'';
					
					
					$username = isset($user[$result['user_id']])?$user[$result['user_id']]['username']:'';//.' - '.$user[$result['user_id']]['fullname']
					$coupon_value = isset($prefix[$result['coupon_prefix']])?$prefix[$result['coupon_prefix']]['prefix_value']:$result['coupon_prefix'];
					$prefix_title = isset($prefix[$result['coupon_prefix']])?$prefix[$result['coupon_prefix']]['prefix_title']:$result['coupon_prefix'];
					
					$coupons_success = array(
						'coupon_history_id'        => $result['coupon_history_id'],
						'coupon_id'        => $result['coupon_id'],
						'username'        => $username,
						'dist_check'      => $result['dist_check'],
						'coupon_code'      => $result['coupon_code'],
						'time_remaining' =>($result['dist_check_id']!=$config_dist_valid)? $this->model_catalog_coupon_history->time_remaining($result['date_upload']):'<i class="fa fa-check text-green"></i>',
						'coupon_prefix'      => $result['coupon_prefix'],
						'prefix_title'      => $prefix_title,
						'coupon_value'      => number_format($coupon_value,0,",","."),
						'year'      => $year,
						'month'      => $month,
						'yearmonth'      => $result['yearmonth'],
						'round_name'      => $result['round_name'],
						'date_upload'      => ($result['coupon_status_id']==$config_coupon_has_paid)?date('Y-m-d',strtotime($result['date_upload'])):'',
						'distributor_code' => $distributor_code,
						'distributor_name' => $distributor_name,
						'dist_check_date' => ($result['dist_check_date']!='0000-00-00')?$result['dist_check_date']:'',
						'coupon_scan_name' => $coupon_scan_name,
						'reward_type' => isset($reward_type[$result['reward_type_id']]) ? $reward_type[$result['reward_type_id']]['reward_type_name'] : '',
						'store_name' => $store_name,
						'store_code' => $store_code,
						'store_customer_code' => $store_customer_code,
						'store_ad_code' => $store_ad_code,
						'store_address_raw' => $store_address_raw,
						'plan_href' => $plan_href,
						'coupon_status'    => isset($coupon_status[$result['coupon_status_id']]) ? $coupon_status[$result['coupon_status_id']]['coupon_status_name'] : ''
					);
					if($index_by!=NULL){
						$coupons_data[$result[$index_by]] = $coupons_success;
					}else{
						$coupons_data[] = $coupons_success;
					}
				}
			}
		}
		
		return $coupons_data;
	}
 	
	
	  
 	public function export() {
			if($this->user->isLogged()){
				$logged_id = $this->user->isLogged();
				$logged_name = $this->user->getUserName();
			}else{
				$logged_id = $this->customer->isLogged();
				$logged_name = $this->customer->getUserName();
			}
				
				
			$this->load->model('catalog/coupon_history');
			$json = array();
			$filter_data = array();
			foreach($this->filter_key as $key){
				if (isset($this->request->get[$key])) {
					$filter_data[$key] = $this->request->get[$key];
				}
			}
			$filter_data['filter_coupon_status'] = $this->config->get('config_coupon_has_paid');
				
			if($this->distributor->isLogged()){
				$filter_data['filter_distributor_id'] = $this->distributor->isLogged();
			}
		
			$template_id = 8;
			$template_info = $this->excel->getTemplate($template_id);
			$setting = $template_info['settings'];
			$rstart = $rend = $setting['start_row'];
			$rend = $rstart-1;
			 
			if(isset($template_info['template_id'])){
				
					/*Loading Start*/
					$starttime = explode(' ',microtime());
					$start_time =  $starttime['1'] + $starttime['0'];
					
					/*Loading Start*/
					
			
			
				$this->load->model('store/store');
				$store_fields = array(
					'store_id',
					'store_code',
					'store_ad_code',
					'store_name',
					'store_phone',
					'store_address',
					'store_place',
					'store_ward',
					'store_district',
					'store_province'
				);
				$store = $this->model_store_store->getStoreByFields($store_fields,'store_id');
				
				$color = array(
				'0'=>'92D050',
				'1'=>'FFFF00',
				'2'=>'FF0000',
				);
			
				$this->load->model('catalog/distributor');
				$distributor = $this->model_catalog_distributor->getDistributorIndexBy('distributor_id');
	
				$this->load->model('catalog/coupon_prefix');
				$prefix =  $this->model_catalog_coupon_prefix->getPrefixsIndexBy('coupon_prefix');
				
				$coupon_histories = $this->model_catalog_coupon_history->getCouponsHistory($filter_data);
				
				$results = $this->model_catalog_coupon_history->mappingCouponsByStoreID($coupon_histories);
				//print_r('<pre>'); print_r($results); print_r('</pre>'); die();
				
					/*Loading Start*/
					$endtime                     = microtime();
					$endtime                     = explode(' ', $endtime);
					$endtime                     = $endtime['1'] + $endtime['0'];
					$loadingtime = number_format($endtime - $start_time, 4);
					$this->log->write('Coupon: #'.$logged_id.' #'.$logged_name.' GetCoupons: '.$loadingtime); 
					/*Loading End*/
					
				$check_valid = $this->config->get('config_dist_valid');
				
				$coupons = array();
				$i = 1;
				foreach($results as $store_id => $val){
					if(isset($store[$store_id])){
						$coupon = $val['coupons'];
						
						$store_info = $store[$store_id];
						$store_code = $store_info['store_code'];
						$store_ad_code = $store_info['store_ad_code'];
						$store_name = $store_info['store_name'];
						$store_phone = $store_info['store_phone'];
						$store_address = $store_info['store_address'];
						$store_place = $store_info['store_place'];
						$store_ward = $store_info['store_ward'];
						$store_district = $store_info['store_district'];
						$store_province = $store_info['store_province'];
					
				
						if(isset($distributor[$val['distributor_id']])){
							$dinfo = $distributor[$val['distributor_id']];
							$distributor_code = $dinfo['distributor_code'];
							$distributor_name = $dinfo['distributor_name'];
						}else{
							$distributor_name = $distributor_code = '';
						}
						
	
					$coupons[]['style'] = array(
						'A'=>'',
						'B'=>'',
						'C'=>'',
						'D'=>'',
						'E'=>'',
						'F'=>'',
						'G'=>'',
						'H'=>'',
						'I'=>'',
						'J'=>'',
						'K'=>'',
						'L'=>isset($coupon['01']['dist_check_id'])?$color[$coupon['01']['dist_check_id']]:'',
						'M'=>'',
						'N'=>'',
						'O'=>isset($coupon['02']['dist_check_id'])?$color[$coupon['02']['dist_check_id']]:'',
						'P'=>'',
						'Q'=>'',
						'R'=>isset($coupon['03']['dist_check_id'])?$color[$coupon['03']['dist_check_id']]:'',
						'S'=>'',
						'T'=>'',
						'U'=>isset($coupon['04']['dist_check_id'])?$color[$coupon['04']['dist_check_id']]:'',
						'V'=>'',
						'W'=>'',
						'X'=>isset($coupon['05']['dist_check_id'])?$color[$coupon['05']['dist_check_id']]:'',
						'Y'=>'',
						'Z'=>'',
						'AA'=>isset($coupon['06']['dist_check_id'])?$color[$coupon['06']['dist_check_id']]:'',
						'AB'=>'',
						'AC'=>'',
						'AD'=>isset($coupon['07']['dist_check_id'])?$color[$coupon['07']['dist_check_id']]:'',
						'AE'=>'',
						'AF'=>'',
						'AG'=>isset($coupon['08']['dist_check_id'])?$color[$coupon['08']['dist_check_id']]:'',
						'AH'=>'',
						'AI'=>'',
						'AJ'=>isset($coupon['09']['dist_check_id'])?$color[$coupon['09']['dist_check_id']]:'',
						'AK'=>'',
						'AL'=>'',
						'AM'=>isset($coupon['10']['dist_check_id'])?$color[$coupon['10']['dist_check_id']]:'',
						'AN'=>'',
						'AO'=>'',
						'AP'=>isset($coupon['11']['dist_check_id'])?$color[$coupon['11']['dist_check_id']]:'',
						'AQ'=>'',
						'AR'=>'',
						'AS'=>isset($coupon['12']['dist_check_id'])?$color[$coupon['12']['dist_check_id']]:'',
						'AT'=>'',
						'AU'=>''
					);
	
					$coupons[]['value'] = array(
						'A'=>$i,
						'B'=>$distributor_name,
						'C'=>$distributor_code,
						'D'=>$store_ad_code,
						'E'=>$store_code,
						'F'=>$store_name,
						'G'=>$store_address,
						'H'=>$store_place,
						'I'=>$store_ward,
						'J'=>$store_district,
						'K'=>$store_province,
						'L'=>isset($coupon['01']['coupon_prefix'])?$prefix[$coupon['01']['coupon_prefix']]['prefix_value']:'',//1
						'M'=>(isset($coupon['01']['coupon_code'])&&$coupon['01']['dist_check_id']==$check_valid)?$coupon['01']['coupon_code']:'',//1
						'N'=>(isset($coupon['01']['time_remaining'])&&$coupon['01']['dist_check_id']!=$check_valid)?$coupon['01']['time_remaining']:'',
						'O'=>isset($coupon['02']['coupon_prefix'])?$prefix[$coupon['02']['coupon_prefix']]['prefix_value']:'',//2
						'P'=>(isset($coupon['02']['coupon_code'])&&$coupon['02']['dist_check_id']==$check_valid)?$coupon['02']['coupon_code']:'',//2
						'Q'=>(isset($coupon['02']['time_remaining'])&&$coupon['02']['dist_check_id']!=$check_valid)?$coupon['02']['time_remaining']:'',
						'R'=>isset($coupon['03']['coupon_prefix'])?$prefix[$coupon['03']['coupon_prefix']]['prefix_value']:'',//3
						'S'=>(isset($coupon['03']['coupon_code'])&&$coupon['03']['dist_check_id']==$check_valid)?$coupon['03']['coupon_code']:'',//3
						'T'=>(isset($coupon['03']['time_remaining'])&&$coupon['03']['dist_check_id']!=$check_valid)?$coupon['03']['time_remaining']:'',
						'U'=>isset($coupon['04']['coupon_prefix'])?$prefix[$coupon['04']['coupon_prefix']]['prefix_value']:'',//4
						'V'=>(isset($coupon['04']['coupon_code'])&&$coupon['04']['dist_check_id']==$check_valid)?$coupon['04']['coupon_code']:'',//4
						'W'=>(isset($coupon['04']['time_remaining'])&&$coupon['04']['dist_check_id']!=$check_valid)?$coupon['04']['time_remaining']:'',
						'X'=>isset($coupon['05']['coupon_prefix'])?$prefix[$coupon['05']['coupon_prefix']]['prefix_value']:'',//5
						'Y'=>(isset($coupon['05']['coupon_code'])&&$coupon['05']['dist_check_id']==$check_valid)?$coupon['05']['coupon_code']:'',//5
						'Z'=>(isset($coupon['05']['time_remaining'])&&$coupon['05']['dist_check_id']!=$check_valid)?$coupon['05']['time_remaining']:'',
						'AA'=>isset($coupon['06']['coupon_prefix'])?$prefix[$coupon['06']['coupon_prefix']]['prefix_value']:'',//6
						'AB'=>(isset($coupon['06']['coupon_code'])&&$coupon['06']['dist_check_id']==$check_valid)?$coupon['06']['coupon_code']:'',//6
						'AC'=>(isset($coupon['06']['time_remaining'])&&$coupon['06']['dist_check_id']!=$check_valid)?$coupon['06']['time_remaining']:'',
						'AD'=>isset($coupon['07']['coupon_prefix'])?$prefix[$coupon['07']['coupon_prefix']]['prefix_value']:'',//7
						'AE'=>(isset($coupon['07']['coupon_code'])&&$coupon['07']['dist_check_id']==$check_valid)?$coupon['07']['coupon_code']:'',//7
						'AF'=>(isset($coupon['07']['time_remaining'])&&$coupon['07']['dist_check_id']!=$check_valid)?$coupon['07']['time_remaining']:'',
						'AG'=>isset($coupon['08']['coupon_prefix'])?$prefix[$coupon['08']['coupon_prefix']]['prefix_value']:'',//8
						'AH'=>(isset($coupon['08']['coupon_code'])&&$coupon['08']['dist_check_id']==$check_valid)?$coupon['08']['coupon_code']:'',//8
						'AI'=>(isset($coupon['08']['time_remaining'])&&$coupon['08']['dist_check_id']!=$check_valid)?$coupon['08']['time_remaining']:'',
						'AJ'=>isset($coupon['09']['coupon_prefix'])?$prefix[$coupon['09']['coupon_prefix']]['prefix_value']:'',//9
						'AK'=>(isset($coupon['09']['coupon_code'])&&$coupon['09']['dist_check_id']==$check_valid)?$coupon['09']['coupon_code']:'',//9
						'AL'=>(isset($coupon['09']['time_remaining'])&&$coupon['09']['dist_check_id']!=$check_valid)?$coupon['09']['time_remaining']:'',
						'AM'=>isset($coupon['10']['coupon_prefix'])?$prefix[$coupon['10']['coupon_prefix']]['prefix_value']:'',//10
						'AN'=>(isset($coupon['10']['coupon_code'])&&$coupon['10']['dist_check_id']==$check_valid)?$coupon['10']['coupon_code']:'',//10
						'AO'=>(isset($coupon['10']['time_remaining'])&&$coupon['10']['dist_check_id']!=$check_valid)?$coupon['10']['time_remaining']:'',
						'AP'=>isset($coupon['11']['coupon_prefix'])?$prefix[$coupon['11']['coupon_prefix']]['prefix_value']:'',//11
						'AQ'=>(isset($coupon['11']['coupon_code'])&&$coupon['11']['dist_check_id']==$check_valid)?$coupon['11']['coupon_code']:'',//11
						'AR'=>(isset($coupon['11']['time_remaining'])&&$coupon['11']['dist_check_id']!=$check_valid)?$coupon['11']['time_remaining']:'',
						'AS'=>isset($coupon['12']['coupon_prefix'])?$prefix[$coupon['12']['coupon_prefix']]['prefix_value']:'',//12
						'AT'=>(isset($coupon['12']['coupon_code'])&&$coupon['12']['dist_check_id']==$check_valid)?$coupon['12']['coupon_code']:'',//12
						'AU'=>(isset($coupon['12']['time_remaining'])&&$coupon['12']['dist_check_id']!=$check_valid)?$coupon['12']['time_remaining']:'',
						);
						$i++;
						$rend++;
					} 
				}
					$coupons[]['value'] = array(
						'A'=>'',
						'B'=>'',
						'C'=>'',
						'D'=>'',
						'E'=>'',
						'F'=>'',
						'G'=>'',
						'H'=>'',
						'I'=>'',
						'J'=>'',
						'K'=>'Tổng cộng',
						'L'=>'=SUM(L'.$rstart.':L'.$rend.')',//1
						'M'=>'',
						'N'=>'',
						'O'=>'=SUM(O'.$rstart.':O'.$rend.')',//2
						'P'=>'',
						'Q'=>'',
						'R'=>'=SUM(R'.$rstart.':R'.$rend.')',//3
						'S'=>'',
						'T'=>'',
						'U'=>'=SUM(U'.$rstart.':U'.$rend.')',//4
						'V'=>'',
						'W'=>'',
						'X'=>'=SUM(X'.$rstart.':X'.$rend.')',//5
						'Y'=>'',
						'Z'=>'',
						'AA'=>'=SUM(AA'.$rstart.':AA'.$rend.')',//6
						'AB'=>'',
						'AC'=>'',
						'AD'=>'=SUM(AD'.$rstart.':AD'.$rend.')',//7
						'AE'=>'',
						'AF'=>'',
						'AG'=>'=SUM(AG'.$rstart.':AG'.$rend.')',//8
						'AH'=>'',
						'AI'=>'',
						'AJ'=>'=SUM(AJ'.$rstart.':AJ'.$rend.')',//9
						'AK'=>'',
						'AL'=>'',
						'AM'=>'=SUM(AM'.$rstart.':AM'.$rend.')',//10
						'AN'=>'',
						'AO'=>'',
						'AP'=>'=SUM(AP'.$rstart.':AP'.$rend.')',//11
						'AQ'=>'',
						'AR'=>'',
						'AS'=>'=SUM(AS'.$rstart.':AS'.$rend.')',//12
						'AT'=>'',
						'AU'=>''
					);
	
				
				//print_r('<pre>'); print_r($coupons); print_r('</pre>'); 
				/*Mapping Data*/ 
				$endtime2                     = microtime();
					$endtime2                     = explode(' ', $endtime2);
					$endtime2                     = $endtime2['1'] + $endtime2['0'];
				$loadingtime2 = number_format($endtime2 - $start_time, 4); 
					
				$this->log->write('Mapping Data: '.$loadingtime2); 
				//print_r('<pre>'); print_r($loadingtime2); print_r('</pre>');die();
				/*Mapping Data End*/
				
				$export_data = array(
					'excel_data' => $coupons,
					'template_id' => $template_id,
					'template_name' => 'ExportCoupon-by-'.$logged_name.'-at-'. date('Ymd-His') .'.xlsx'
				);
	
	
				$file = $this->excel->exportData($export_data);
			
				$endtime3                     = microtime();
				$endtime3                     = explode(' ', $endtime3);
				$endtime3                     = $endtime3['1'] + $endtime3['0'];
				$loadingtime3 = number_format($endtime3 - $start_time, 4); 
				$this->log->write('Write Excel: '.$loadingtime3); 	
				$this->response->setOutput(file_get_contents($file, FILE_USE_INCLUDE_PATH, null));
			}
				
			$url = $this->_url();
		
			$this->response->redirect($this->url->link('distributor/coupon_history', $url , true));
	}

	public function report() {
		
			if($this->user->isLogged()){
				$logged_id = $this->user->isLogged();
				$logged_name = $this->user->getUserName();
			}elseif($this->customer->isLogged()){
				$logged_id = $this->customer->isLogged();
				$logged_name = $this->customer->getUserName();
			}else{
				$logged_id = $this->distributor->isLogged();
				$logged_name = $this->distributor->getCode();
			}
				
			$export_name = '';
				
			$this->load->model('catalog/coupon_history');
			$json = array();
			$filter_data = array();
			foreach($this->filter_key as $key){
				if (isset($this->request->get[$key])) {
					$filter_data[$key] = $this->request->get[$key];
					$$key = $this->request->get[$key];
				}else{
					$$key = NULL;
				}
			}
			
			$filter_data['filter_coupon_status'] = $this->config->get('config_coupon_has_paid');
				
			if($this->distributor->isLogged()){
				$filter_data['filter_distributor_id'] = $this->distributor->isLogged();
			}
		
		
			$template_id = 11;
			$template_info = $this->excel->getTemplate($template_id);
			$setting = $template_info['settings'];
			$rstart = $rend = $setting['start_row'];
			$rend = $rstart-1;
			 
			
				$this->load->model('catalog/distributor');
				$distributor = $this->model_catalog_distributor->getDistributorIndexBy('distributor_id');
				
				
			if(isset($template_info['template_id'])&&isset($filter_data['filter_distributor_id'])){
				
					$did = $filter_data['filter_distributor_id'];
					$export_name = $distributor[$did]['distributor_code'].'-'.$distributor[$did]['distributor_name'];
					$export_name = alias($export_name,'-');
					
					$distributor_code = $distributor[$did]['distributor_code'];
					$distributor_name = $distributor[$did]['distributor_name'];
					
					
					
					/*Loading Start*/
					$starttime = explode(' ',microtime());
					$start_time =  $starttime['1'] + $starttime['0'];
					
					/*Loading Start*/
			
	
				
				$report = $this->model_catalog_coupon_history->getTotalCouponsByMonth($filter_data);
				
					/*Loading Start*/
					$endtime                     = microtime();
					$endtime                     = explode(' ', $endtime);
					$endtime                     = $endtime['1'] + $endtime['0'];
					$loadingtime = number_format($endtime - $start_time, 4);
					$this->log->write('CouponReport: #'.$logged_id.' #'.$logged_name.' GetReport: '.$loadingtime); 
					/*Loading End*/
					
		
		
				$coupons = array();
				
					$coupons[] = array(
						'A'=>'',
						'B'=>$filter_date_start,
						'C'=>'','D'=>'','E'=>'','F'=>'','G'=>'','H'=>'','I'=>'',
						'J'=>$filter_date_end,
						'K'=>'','L'=>'',
						'M'=>'','N'=>'',
						'O'=>date('Y-m-d H:i:s')
					);
					
					$coupons[] = array(
						'A'=>'','B'=>$distributor_code
					);
					
					$coupons[] = array(
						'A'=>'','B'=>$distributor_name
					);
					
					$coupons[] = array('A'=>'');
					
					$coupons[] = array('A'=>'Tổng hợp thanh toán coupon trưng bày NIVEA');
					
					$coupons[] = array('A'=>'');
					
					$coupons[] = array('A'=>'Loại Coupon');
					
	
					//Blue
					$coupons[] = array(
						'A'=>'BLUE',
						'B'=>'1000000',
						'C'=>isset($report['01'])&&isset($report['01']['BS'])?$report['01']['BS']:'',//01
						'D'=>'=IF(C8>0,C8*B8,"")',
						'E'=>isset($report['02'])&&isset($report['02']['BS'])?$report['02']['BS']:'',//02
						'F'=>'',
						'G'=>isset($report['03'])&&isset($report['03']['BS'])?$report['03']['BS']:'',//03
						'H'=>'',
						'I'=>isset($report['04'])&&isset($report['04']['BS'])?$report['04']['BS']:'',//04
						'J'=>'',
						'K'=>isset($report['05'])&&isset($report['05']['BS'])?$report['05']['BS']:'',//05
						'L'=>'',
						'M'=>isset($report['06'])&&isset($report['06']['BS'])?$report['06']['BS']:'',//06
						'N'=>'',
						'O'=>isset($report['07'])&&isset($report['07']['BS'])?$report['07']['BS']:'',//07
						'P'=>'',
						'Q'=>isset($report['08'])&&isset($report['08']['BS'])?$report['08']['BS']:'',//08
						'R'=>'',
						'S'=>isset($report['09'])&&isset($report['09']['BS'])?$report['09']['BS']:'',//09
						'T'=>'',
						'U'=>isset($report['10'])&&isset($report['10']['BS'])?$report['10']['BS']:'',//10
						'V'=>'',
						'W'=>isset($report['11'])&&isset($report['11']['BS'])?$report['11']['BS']:'',//11
						'X'=>'',
						'Y'=>isset($report['12'])&&isset($report['12']['BS'])?$report['12']['BS']:''//12
					);
	
					//Diamond
					$coupons[] = array(
						'A'=>'DIAMOND',
						'B'=>'400000',
						'C'=>isset($report['01'])&&isset($report['01']['DS'])?$report['01']['DS']:'',//01
						'D'=>'',
						'E'=>isset($report['02'])&&isset($report['02']['DS'])?$report['02']['DS']:'',//02
						'F'=>'',
						'G'=>isset($report['03'])&&isset($report['03']['DS'])?$report['03']['DS']:'',//03
						'H'=>'',
						'I'=>isset($report['04'])&&isset($report['04']['DS'])?$report['04']['DS']:'',//04
						'J'=>'',
						'K'=>isset($report['05'])&&isset($report['05']['DS'])?$report['05']['DS']:'',//05
						'L'=>'',
						'M'=>isset($report['06'])&&isset($report['06']['DS'])?$report['06']['DS']:'',//06
						'N'=>'',
						'O'=>isset($report['07'])&&isset($report['07']['DS'])?$report['07']['DS']:'',//07
						'P'=>'',
						'Q'=>isset($report['08'])&&isset($report['08']['DS'])?$report['08']['DS']:'',//08
						'R'=>'',
						'S'=>isset($report['09'])&&isset($report['09']['DS'])?$report['09']['DS']:'',//09
						'T'=>'',
						'U'=>isset($report['10'])&&isset($report['10']['DS'])?$report['10']['DS']:'',//10
						'V'=>'',
						'W'=>isset($report['11'])&&isset($report['11']['DS'])?$report['11']['DS']:'',//11
						'X'=>'',
						'Y'=>isset($report['12'])&&isset($report['12']['DS'])?$report['12']['DS']:''//12
					);
					
					//Gold
					$coupons[] = array(
						'A'=>'GOLD',
						'B'=>'200000',
						'C'=>isset($report['01'])&&isset($report['01']['GS'])?$report['01']['GS']:'',//01
						'D'=>'',
						'E'=>isset($report['02'])&&isset($report['02']['GS'])?$report['02']['GS']:'',//02
						'F'=>'',
						'G'=>isset($report['03'])&&isset($report['03']['GS'])?$report['03']['GS']:'',//03
						'H'=>'',
						'I'=>isset($report['04'])&&isset($report['04']['GS'])?$report['04']['GS']:'',//04
						'J'=>'',
						'K'=>isset($report['05'])&&isset($report['05']['GS'])?$report['05']['GS']:'',//05
						'L'=>'',
						'M'=>isset($report['06'])&&isset($report['06']['GS'])?$report['06']['GS']:'',//06
						'N'=>'',
						'O'=>isset($report['07'])&&isset($report['07']['GS'])?$report['07']['GS']:'',//07
						'P'=>'',
						'Q'=>isset($report['08'])&&isset($report['08']['GS'])?$report['08']['GS']:'',//08
						'R'=>'',
						'S'=>isset($report['09'])&&isset($report['09']['GS'])?$report['09']['GS']:'',//09
						'T'=>'',
						'U'=>isset($report['10'])&&isset($report['10']['GS'])?$report['10']['GS']:'',//10
						'V'=>'',
						'W'=>isset($report['11'])&&isset($report['11']['GS'])?$report['11']['GS']:'',//11
						'X'=>'',
						'Y'=>isset($report['12'])&&isset($report['12']['GS'])?$report['12']['GS']:''//12
					);
					
					//Silver
					$coupons[] = array(
						'A'=>'SILVER',
						'B'=>'120000',
						'C'=>isset($report['01'])&&isset($report['01']['SS'])?$report['01']['SS']:'',//01
						'D'=>'',
						'E'=>isset($report['02'])&&isset($report['02']['SS'])?$report['02']['SS']:'',//02
						'F'=>'',
						'G'=>isset($report['03'])&&isset($report['03']['SS'])?$report['03']['SS']:'',//03
						'H'=>'',
						'I'=>isset($report['04'])&&isset($report['04']['SS'])?$report['04']['SS']:'',//04
						'J'=>'',
						'K'=>isset($report['05'])&&isset($report['05']['SS'])?$report['05']['SS']:'',//05
						'L'=>'',
						'M'=>isset($report['06'])&&isset($report['06']['SS'])?$report['06']['SS']:'',//06
						'N'=>'',
						'O'=>isset($report['07'])&&isset($report['07']['SS'])?$report['07']['SS']:'',//07
						'P'=>'',
						'Q'=>isset($report['08'])&&isset($report['08']['SS'])?$report['08']['SS']:'',//08
						'R'=>'',
						'S'=>isset($report['09'])&&isset($report['09']['SS'])?$report['09']['SS']:'',//09
						'T'=>'',
						'U'=>isset($report['10'])&&isset($report['10']['SS'])?$report['10']['SS']:'',//10
						'V'=>'',
						'W'=>isset($report['11'])&&isset($report['11']['SS'])?$report['11']['SS']:'',//11
						'X'=>'',
						'Y'=>isset($report['12'])&&isset($report['12']['SS'])?$report['12']['SS']:''//12
					);
					
				//print_r('<pre>'); print_r($coupons); print_r('</pre>'); die();
				/*Mapping Data*/ 
				$endtime2                     = microtime();
					$endtime2                     = explode(' ', $endtime2);
					$endtime2                     = $endtime2['1'] + $endtime2['0'];
				$loadingtime2 = number_format($endtime2 - $start_time, 4); 
					
				$this->log->write('Mapping Data: '.$loadingtime2); 
				/*Mapping Data End*/
				
				$export_data = array(
					'excel_data' => $coupons,
					'template_id' => $template_id,
					'template_name' => 'Baocao-Thanh-toan-cho-NPP-'.$export_name.'-'. date('Y-m-d-His') .'.xlsx'
				);
	
	
				$file = $this->excel->exportWithTemplate($export_data);
			
				$endtime3                     = microtime();
				$endtime3                     = explode(' ', $endtime3);
				$endtime3                     = $endtime3['1'] + $endtime3['0'];
				$loadingtime3 = number_format($endtime3 - $start_time, 4); 
				$this->log->write('Write Excel: '.$loadingtime3); 	
				$this->response->setOutput(file_get_contents($file, FILE_USE_INCLUDE_PATH, null));
			}
				
			$url = $this->_url();
		
			$this->response->redirect($this->url->link('distributor/coupon_history', $url , true));
	}

}