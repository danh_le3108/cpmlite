<?php

class ControllerFcmFcm extends Controller {
	private function _url(){
		$url = '';
		$url_key = array(
			'filter_global'
		);
		foreach($url_key as $key){
			if (isset($this->request->get[$key])) {
				$url .= '&'.$key.'=' . urlencode(html_entity_decode($this->request->get[$key], ENT_QUOTES, 'UTF-8'));
			}
		}
		return $url;
	}

  public function index(){
		$this->load->library('fcm/firebase');
		$this->load->library('fcm/push');

    $languages= $this->load->language('fcm/fcm');
		foreach($languages as $key=>$value){
				$data[$key] = $value;
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('fcm/fcm');

		$data['users_google_id'] = $this->model_fcm_fcm->getAllUsers();

		$this->document->addStyle('assets/fcm/fcm.css');
		//$this->document->addScript('assets/gcm/gcm.js');
		$this->document->addScript('assets/plugins/ckeditor/ckeditor.js');

		$data['logged_id'] = $this->user->isLogged();

		$url = $this->_url();

		$data['href_history_to_all'] = str_replace('&amp;', '&', $this->url->link('fcm/fcm/history', 'type=send_to_all'.$url, true));
		$data['href_history_to_user'] = str_replace('&amp;', '&', $this->url->link('fcm/fcm/history', 'type=send_to_user'.$url, true));
		$data['href_history_to_multiple'] = str_replace('&amp;', '&', $this->url->link('fcm/fcm/history', 'type=send_to_multiple'.$url, true));


		if(isset($this->request->get['filter_global'])){
			$data['filter_global'] = $this->request->get['filter_global'];
		} else {
			$data['filter_global'] = '';
		}

		$data['has_add'] = $this->user->hasPermission('add', 'fcm/fcm');

		$data['url'] = $this->url->link('fcm/fcm', '', true);

		$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');

		$template = 'fcm/notification';
		$this->response->setOutput($this->load->view($template, $data));
  }

	public function send_to_all(){
		if(isset($this->request->post['user_id']) && isset($this->request->post['message']) && isset($this->request->post['title'])){
			$json = '';
			$response = '';

			// save database
			$this->load->model('fcm/fcm');
			$mess = array(
				'user_id' => $this->request->post['user_id'],
				'user_group_id' => 0,
				'title' => $this->request->post['title'],
				'message' => $this->request->post['message'],
					'body' => $this->request->post['message'],
					'text' => $this->request->post['message'],
				'type' => 'send_to_all'
			);
			$message_id = $this->model_fcm_fcm->addMessage($mess);

			$firebase = new Fcm\Firebase($this->config->get('config_fcm_api_key'));
	    
				$mess2 = array(
					'title' => $this->request->post['title'],
					'body' => $this->request->post['message']
				);
			$response = $firebase->sendToTopic('mycpm', $mess2);

			$res = json_decode($response, true);

			if(!empty($response)){
				$this->model_fcm_fcm->updateStatusFcm($message_id);
			}

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput($response);
		}
	}

	public function send_to_user() {
		if(isset($this->request->post['user_id']) && isset($this->request->post['message']) && isset($this->request->post['to_user']) && isset($this->request->post['title'])) {
			$this->load->model('project/project_user');
			$user = $this->model_project_project_user->getUser($this->request->post['to_user']);
			if($user){
				$json = '';
				$response = '';
				// save database
				$this->load->model('fcm/fcm');
				$mess = array(
					'user_id' => $this->request->post['user_id'],
					'nguoi_nhan' => $user['user_id'],
					'user_group_id' => 0,
					'title' => $this->request->post['title'],
					'message' => $this->request->post['message'],
					'body' => $this->request->post['message'],
					'text' => $this->request->post['message'],
					'type' => 'send_to_user'
				);
				$message_id = $this->model_fcm_fcm->addMessage($mess);

				$firebase = new Fcm\Firebase($this->config->get('config_fcm_api_key'));
				
				$mess2 = array(
					'title' => $this->request->post['title'],
					'body' => $this->request->post['message']
				);
				$response = $firebase->send($user['google_token'], $mess2, 0);

				$res = json_decode($response, true);
				if(!empty($response) && $res['success']==1){
					$this->model_fcm_fcm->updateStatusFcm($message_id);
				}

				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput($response);
			}
		}
	}

  public function send_to_multiple(){
    if(isset($this->request->post['user_id']) && isset($this->request->post['message']) && isset($this->request->post['to_user'])) {
			$json = '';
			$response = '';

			$users_id = explode(',', $this->request->post['to_user']);
		
		$this->load->model('fcm/fcm');
		$users = $this->model_fcm_fcm->getAllUsers();

			$list_google_token = array();
			$list_id = array();

			foreach($users_id as $user_id) {
				if(isset($users[$user_id]) && !empty($users[$user_id]['google_token'])){
					$user = $users[$user_id];
					$list_google_token[] = $user['google_token'];
					$list_id[] = $user['user_id'];
				}
			}

			$this->load->model('fcm/fcm');

			if(!empty($list_id)) {
				$mess = array(
					'user_id' => $this->request->post['user_id'],
					'nguoi_nhan' => implode(',', $list_id),
					'user_group_id' => 0,
					'message' => $this->request->post['message'],
					'body' => $this->request->post['message'],
					'text' => $this->request->post['message'],
					'type' => 'send_to_multiple'
				);
				$this->model_fcm_fcm->addMessage($mess);
			}

			if(!empty($list_google_token)){
				$firebase = new Fcm\Firebase($this->config->get('config_fcm_api_key'));

				$mess2 = array(
					'title' => $this->request->post['title'],
					'body' => $this->request->post['message']
				);

				$list = implode(',', $list_google_token);
				$response = $firebase->sendMultiple($list, $mess2, 0);
				
				$res = json_decode($response, true);
				if(!empty($response) && $res['success']==1){
					$this->model_fcm_fcm->updateStatusFcm($message_id);
				}

				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput($response);
			}
		}
  }

	public function history(){
		if(isset($this->request->get['type'])){

			$this->load->model('fcm/fcm');

			$filter_data = array(
				'filter_global' => isset($this->request->get['filter_global'])?$this->request->get['filter_global']:''
			);

			$data['histories'] = $this->model_fcm_fcm->getHistory($this->user->isLogged(), $this->request->get['type'], $filter_data);
			$data['logged_id'] = $this->user->isLogged();

			$template = 'fcm/history';
			$this->response->setOutput($this->load->view($template, $data));
		}
	}

	public function autocomplete(){
		$json = array();
		$filter_data = array(
			'sort'        => 'fullname',
			'order'       => 'ASC',
			'start'       => 0,
			'limit'       => 20
		);

		$filter_data = array(
			'filter_global' => isset($this->request->get['filter_global'])?$this->request->get['filter_global']:''
		);

		$this->load->model('user/user');
		$results = $this->model_user_user->getUsers($filter_data);

		foreach ($results as $result) {
			$json[] = array(
				'user_id'       => $result['user_id'],
				'user_group_id' => $result['user_group_id'],
				'username'      => $result['username'],
				'usercode'      => $result['usercode'],
				'email'         => $result['email'],
				'telephone'     => $result['telephone'],
				'fullname'      => html_entity_decode($result['fullname'], ENT_QUOTES, 'UTF-8')
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
?>
