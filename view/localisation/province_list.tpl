<?php echo $header;?>
<div id="content">
  <div class="container-fluid">

    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>

     <div class="box" id="filter-area">
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="filter_name"><?php echo $text_search; ?></label>
            <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $text_search; ?>" id="filter_name" class="form-control" onchange="filter()"/>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="filter_prefix"><?php echo $entry_prefix; ?></label>
            <select name="filter_prefix" id="filter_prefix" class="form-control" onchange="filter()">
              <option value="*"><?php echo $text_select; ?></option>
              <option value="Thành phố" <?php if ($filter_prefix=='Thành phố') { ?>selected="selected" <?php } ?>>Thành phố</option>
              <option value="Tỉnh" <?php if ($filter_prefix=='Tỉnh') { ?>selected="selected" <?php } ?>>Tỉnh</option>
            </select>
          </div>
        </div>
        <div class="col-sm-4">
          <button type="button" id="button-clear" class="btn btn-primary pull-right"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>
        </div>
      </div>
    </div>
    <!--panel-body -->
  </div>
  <!--box -->



    <div class="box">
      <div class="box-header">
        <h2 class="box-title"> <?php echo $heading_title; ?></h2>
      </div>
      <div class="panel-body"><div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-province">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td style="width: 1px;" class="text-left">ID</td>
                  <td class="text-left">Level</td>
                  <td class="text-left"><?php if ($sort == 'name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $text_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $text_name; ?></a>
                    <?php } ?></td>

                  <td class="text-left"><?php echo $text_code; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($provinces) { ?>
                <?php foreach ($provinces as $province) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($province['province_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $province['province_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $province['province_id']; ?>" />
                    <?php } ?></td>
                  <td class="text-left"><?php echo $province['province_id']; ?></td>
                  <td class="text-left"><?php echo $province['prefix']; ?></td>
                  <td class="text-left"><?php echo $province['name']; ?></td>
                  <td class="text-left"><?php echo $province['code']; ?></td>
                 
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="5"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form><div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      </div>
    </div>

  </div>
  </div>



  <script type="text/javascript"><!--
  function filter() {
  	url = 'index.php?route=localisation/province';

	var filter_name = $('input[name=\'filter_name\']').val();

  	if (filter_name) {
  		url += '&filter_name=' + encodeURIComponent(filter_name);
  	}
  	var filter_prefix = $('select[name=\'filter_prefix\']').val();

  	if (filter_prefix != '*') {
  		url += '&filter_prefix=' + encodeURIComponent(filter_prefix);
  	}
  	location = url;
  }
  //-->
</script>
<?php echo $footer;?>