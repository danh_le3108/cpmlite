<div class="container-fluid">
  <div class="row">
    <div class="col-sm-7"><?php echo $pagination;?> </div>
    <div class="col-sm-5 text-right"><?php echo $results;?></div>
  </div>
  <div class="box box-info">
    <div class="box-header">
      <h3 class="box-title">THÔNG TIN</h3>
    </div>
    <div class="box-body table-responsive no-padding">
      <div class="panel-body">
        <table class="table table-hover table table-bordered">
          <thead>
            <tr>
              <th class="text-center" width="1">ID</th>
              <th class="text-center" width="12%">Cinema voucher</th>
              <th class="text-center" width="12%">Beverage voucher</th>
              <th class="text-center" width="12%">10K phone card</th>
              <th class="text-center" width="10%">Coffee cup</th>
              <th class="text-center" width="10%">Coffee Joy 45g</th>
              <th class="text-center">Ghi chú</th>
              <th class="text-center" width="15%">Ngày tạo</th>
              <?php if($hasDel) {?>
              <th width="5%" class="text-center"> Xóa </th>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <?php 
            foreach($warehouses as $wh) { ?>
            <tr id="warehouse_id_<?=$wh['warehouse_id']?>">
              <td class="text-center">
                <a href="index.php?route=warehouse/warehouse/edit&warehouse_id=<?=$wh['warehouse_id']?>" target="_blank">
                <?=$wh['warehouse_id']?>
                </a>
              </td>
              <td class="text-center"><?=$wh['gift_11']?></td>
              <td class="text-center"><?=$wh['gift_12']?></td>
              <td class="text-center"><?=$wh['gift_13']?></td>
              <td class="text-center"><?=$wh['gift_14']?></td>
              <td class="text-center"><?=$wh['gift_36']?></td>
              <td><?=$wh['comment']?></td>
              <td class="text-center"><?=$wh['date_added']?></td>
              <td class="text-center">
                <a class="btn" style="color:#F00;" onclick="deleteStore(<?=$wh['warehouse_id']?>)"><i class="fa fa-trash"></i></a>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-7"><?php echo $pagination;?> </div>
    <div class="col-sm-5 text-right"><?php echo $results;?></div>
  </div>
</div>