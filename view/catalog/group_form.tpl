
<!-- Your Page Content Here -->

  <div class="container-fluid">

    <?php if ($error_warning) { ?>

    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>

      <button type="button" class="close" data-dismiss="alert">&times;</button>

    </div>

    <?php } ?>

    <div class="box">

      <div class="box-header">

        <h2 class="box-title"> <?php echo $heading_title; ?></h3>
        
        <div class="pull-right box-tools text-right">
        <button type="submit" form="form-code" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>

        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_back; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
        </div>

      </div>

      <div class="panel-body">

        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-code" class="form-horizontal">

        

          <div class="form-group required">

            <label class="col-sm-2 control-label" for="input-name"><?php echo $text_name; ?></label>

            <div class="col-sm-10">

              <input type="text" name="group_name" value="<?php echo $group_name; ?>" placeholder="<?php echo $text_name; ?>" id="input-name" class="form-control" />

              <?php if ($error_name) { ?>

              <div class="text-danger"><?php echo $error_name; ?></div>

              <?php } ?>

            </div>

          </div>

          

          <div class="form-group required">

            <label class="col-sm-2 control-label" for="input-code"><?php echo $text_code; ?></label>

            <div class="col-sm-10">

              <input type="text" name="group_code" value="<?php echo $group_code; ?>" placeholder="<?php echo $text_code; ?>" id="input-code" class="form-control" />

            </div>

          </div>

          <div class="form-group required">

            <label class="col-sm-2 control-label" for="sort_order"><?php echo $text_sort_order; ?></label>

            <div class="col-sm-10">

              <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $text_sort_order; ?>" id="sort_order" class="form-control" />

            </div>

          </div>

          

        </form>

      </div>

    </div>

  </div>

</div>

</section>

    <!-- /.content -->