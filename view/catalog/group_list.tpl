<div class="container-fluid">
   <?php if (!empty($error_warning)) { ?>
   <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
   </div>
   <?php } ?>
   <?php if (!empty($success)) { ?>
   <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
   </div>
   <?php } ?>
   <div class="box" id="filter-area">
      <div class="panel-body">
         <div class="row">
            <div class="col-sm-6">
               <div class="form-group">
                  <label class="control-label" for="filter_name"><?php echo $text_search; ?></label>
                  <input type="text" name="filter_global" value="" placeholder="<?php echo $text_search; ?>" id="filter_global" class="form-control filter" onchange="filter()"/>
               </div>
            </div>
            <div class="col-sm-3">
               <button type="button" id="button-clear" class="btn btn-primary pull-right"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>
            </div>
            <div class="col-sm-3 text-right">
              <button class="btn btn-primary" onclick="add()"><i class="fa fa-plus"></i></button>
              <button class="btn btn-danger" type="button" onclick="delete_group()"><i class="fa fa-trash-o"></i></button>
            </div>
         </div>
      </div>
      <!--panel-body -->
   </div>
   <!--box -->
   <div class="box">
      <div class="box-header">
         <h2 class="box-title">
         <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
           <table class="table table-bordered table-hover">
              <thead>
                 <tr>
                    <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                    <td style="width: 1px;" class="text-center">#</td>
                    <td class="text-left"><?php echo $text_name; ?>
                    </td>
                    <td class="text-left"><?php echo $text_code; ?></td>
                    <td class="text-left">Parent Group</td>
                    <td class="text-left"><?php echo $text_sort_order; ?></td>
                    <td class="text-right"><?php echo $text_action; ?></td>
                 </tr>
              </thead>
              <tbody>
                <tr id="group-0" style="display: none;">
                  <td class="text-center">
                     <input type="checkbox" name="selected[]" value=""/>
                  </td>
                  <td style="width: 1px;" class="text-center"></td>
                  <td class="text-left">
                    <input class="form-control group_name" type="text" value="">
                  </td>
                  <td class="text-left">
                    <input class="form-control group_code" type="text" value="">
                  </td>
                  <td class="text-left">
                    <select class="form-control parent_group_id">
                      <option value="0">--- Chọn ---</option>
                      <?php foreach($groups as $g) { ?>
                      <option value="<?=$g['group_id']?>"><?=$g['group_name']?></option>
                      <?php } ?>
                    </select>  
                  </td>
                  <td class="text-left">
                  <input type="text" class="form-control sort_order" value=""></td>
                  <td class="text-right">
                    <button class="btn btn-primary" onclick="edit(0)"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                  </td>
                </tr>
                 <?php if ($groups) { ?>
                 <?php foreach ($groups as $group) { ?>
                 <tr id="group-<?=$group['group_id']?>">
                    <td class="text-center">
                       <input type="checkbox" name="selected[]" value="<?php echo $group['group_id']; ?>"/>
                    </td>
                    <td style="width: 1px;" class="text-center"><?php echo $group['group_id']; ?></td>
                    <td class="text-left">
                      <input class="form-control group_name" type="text" value="<?php echo $group['group_name']; ?>">
                    </td>
                    <td class="text-left">
                      <input class="form-control group_code" type="text" value="<?php echo $group['group_code']; ?>">
                    </td>
                    <td class="text-left">
                      <select class="form-control parent_group_id">
                        <option value="0">--- Chọn ---</option>
                        <?php foreach($groups as $g) { ?>
                        <option value="<?=$g['group_id']?>" <?=$g['group_id'] == $group['parent_group_id'] ? 'selected' : ''?>><?=$g['group_name']?></option>
                        <?php } ?>
                      </select>  
                    </td>
                    <td class="text-left">
                    <input type="text" class="form-control sort_order" value="<?php echo $group['sort_order']; ?>"></td>
                    <td class="text-right">
                      <button class="btn btn-primary" onclick="edit(<?=$group['group_id']?>)"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                    </td>
                 </tr>
                 <?php } ?>
                 <?php } else { ?>
                 <tr>
                    <td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
                 </tr>
                 <?php } ?>
              </tbody>
           </table>
        </div>
      </div>
   </div>
</div>


<script type="text/javascript"><!--
  function edit(group_id) {
    if (group_id) {
      var tr = $('#group-'+group_id);
      var data = {
        'group_name' : tr.find('.group_name').val(),
        'group_code' : tr.find('.group_code').val(),
        'parent_group_id' : tr.find('.parent_group_id').val(),
        'sort_order' : tr.find('.sort_order').val(),
      }
      $.post('index.php?route=catalog/group/save&group_id='+group_id,data);
    } else {
      var tr = $("tbody tr").first()
      var data = {
        'group_name' : tr.find('.group_name').val(),
        'group_code' : tr.find('.group_code').val(),
        'parent_group_id' : tr.find('.parent_group_id').val(),
        'sort_order' : tr.find('.sort_order').val(),
      }
      $.post('index.php?route=catalog/group/save',data, function() {
         location.reload();
      });
     
    }
  }

  function add() {
    var tr = $('#group-0').clone();
    tr.attr('id','');
    tr.css('display','table-row');
    tr.prependTo('tbody');
  }

  function delete_group() {
    var checkboxs = $('input[name="selected[]"]:checked');
    console.log(checkboxs.eq(0).val());
    var group_ids = [];
    for (var i = 0; i < checkboxs.length; i++) {
      group_ids.push(checkboxs.eq(i).val());
    }
    
    $.post('index.php?route=catalog/group/delete',{'group_ids' : group_ids}, function() {
      location.reload();
    });
  }
</script>