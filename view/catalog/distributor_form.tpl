
          <!-- Your Page Content Here -->
          
             

  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-code" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="box">
      <div class="box-header">
        <h2 class="box-title"> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-code" class="form-horizontal">
        
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $text_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="distributor_name" value="<?php echo $distributor_name; ?>" placeholder="<?php echo $text_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="distributor_code"><?php echo $text_code; ?></label>
            <div class="col-sm-10">
              <input type="text" name="distributor_code" value="<?php echo $distributor_code; ?>" placeholder="<?php echo $text_distributor_code; ?>" id="distributor_code" class="form-control" />
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="distributor_address"><?php echo $text_description; ?></label>
            <div class="col-sm-10">
              <input type="text" name="distributor_address" value="<?php echo $distributor_address; ?>" placeholder="<?php echo $text_description; ?>" id="distributor_address" class="form-control" />
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-region_code"><?php echo $text_region; ?></label>
            <div class="col-sm-10">
            
            <select name="region_code" id="input-region_code" class="form-control chosen">
                <?php foreach ($regions as $region) { ?>
                <option value="<?php echo $region['region_code'];?>" <?php if ($region['region_code']==$region_code) { ?>selected="selected"<?php } ?>><?php echo $region['region_name'];?></option>
              <?php } ?>
              </select>
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-coupon_check"><?php echo $text_coupon_check; ?></label>
            <div class="col-sm-10">
            <select name="coupon_check" id="input-coupon_check" class="form-control chosen">
                <option value="1" <?php if ($coupon_check==1) { ?>selected="selected"<?php } ?>><?php echo $text_yes;?></option>
                <option value="0" <?php if ($coupon_check==0) { ?>selected="selected"<?php } ?>><?php echo $text_no;?></option>
              </select>
            </div>
          </div>
          
          
          <div class="form-group hide">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $text_status; ?></label>
            <div class="col-sm-10">
              <select name="status" id="input-status" class="form-control chosen"
                  <option value=""></option>
                  <option value="1" <?php echo ($status==1)?'selected="selected"':'';?>><?php echo $text_enabled; ?></option>
                  <option value="0" <?php echo ($status==0)?'selected="selected"':''; ?>><?php echo $text_disabled; ?></option>
                </select>
            </div>
          </div>
          
          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-password">Password</label>
            <div class="col-sm-10">
              <input type="password" name="password" value="" placeholder="Password" id="input-password" class="form-control" autocomplete="false" />
                 
                     <?php if ($error_password) { ?>
                      <div class="text-danger"><?php echo $error_password; ?></div>
                      <?php } ?>  
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="password-confirm">Password confirm</label>
            <div class="col-sm-10">
                <input type="password" name="confirm" value="" placeholder="Password confirm" autocomplete="false" id="input-confirm" class="form-control" />
                     
            </div>
          </div> 
          
          
          
          
        </form>
      </div>
    </div>
  </div>
</div>
</section>
    <!-- /.content -->
