
<div class="container-fluid">
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  
  <div class="box" id="filter-area">
  
    <div class="box-header">
    
          <button type="button" id="button-clear" class="btn btn-primary pull-right"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label" for="filter_coupon_code"><?php echo $text_search; ?></label>
            <input type="text" name="filter_coupon_code" value="<?php echo $filter_coupon_code; ?>" placeholder="<?php echo $text_search; ?>" id="filter_coupon_code" class="form-control" onchange="filter()"/>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label" for="filter_yearmonth"><?php echo $text_reward_month; ?></label>
            <select name="filter_yearmonth" id="filter_yearmonth" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_all; ?></option>
              <?php foreach ($yearmonths as $yearmonth) { ?>
              <option value="<?php echo $yearmonth; ?>" <?php if ($yearmonth == $filter_yearmonth) { ?>selected="selected"<?php } ?>><?php echo $yearmonth; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label" for="filter_round"><?php echo $text_coupon_month; ?></label>
            <select name="filter_round" id="filter_round" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_all; ?></option>
              <?php foreach ($rounds as $round) { ?>
              <option value="<?php echo $round['round_name']; ?>" <?php if ($round['round_name'] == $filter_round) { ?>selected="selected"<?php } ?>><?php echo $round['round_name']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div> 
      
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label" for="filter_coupon_status"><?php echo $text_reward_type; ?></label>
            <select name="filter_reward_type_id" id="filter_reward_type_id" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_all; ?></option>
              <?php foreach ($reward_types as $type) { ?>
              <option value="<?php echo $type['reward_type_id']; ?>" <?php if ($type['reward_type_id'] == $filter_reward_type_id) { ?>selected="selected"<?php } ?>><?php echo $type['reward_type_name']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label" for="filter_coupon_status"><?php echo $text_coupon_status; ?></label>
            <select name="filter_coupon_status" id="filter_coupon_status" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_all; ?></option>
              <?php foreach ($coupon_status as $status) { ?>
              <option value="<?php echo $status['coupon_status_id']; ?>" <?php if ($status['coupon_status_id'] == $filter_coupon_status) { ?>selected="selected"<?php } ?>><?php echo $status['coupon_status_name']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label" for="filter_sup_id"><?php echo $text_sup_user; ?></label>
            <select name="filter_sup_id" id="filter_sup_id" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_all; ?></option>
              <?php foreach ($users as $user) { ?>
              <option value="<?php echo $user['user_id']; ?>" <?php if ($user['user_id'] == $filter_sup_id) { ?>selected="selected"<?php } ?>><?php echo $user['usercode'].' - '.$user['fullname']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        
      </div><!--//row 2--> 
      
      <div class="row">
        <div class="col-sm-3">
        
           <div class="form-group">
            <label class="control-label" for="filter_distributor_id"><?php echo $text_distributor; ?></label>
            <select name="filter_distributor_id" id="filter_distributor_id" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_all; ?></option>
              <?php foreach ($distributors as $distributor) { ?>
              <option value="<?php echo $distributor['distributor_id']; ?>" <?php if ($distributor['distributor_id'] == $filter_distributor_id) { ?>selected="selected"<?php } ?>><?php echo $distributor['distributor_code']; ?> - <?php echo $distributor['distributor_name']; ?></option>
              <?php } ?>
            </select>
          </div>
          
        </div><!--//col--> 
        <div class="col-sm-3">
        
          <div class="form-group">
            <label class="control-label" for="filter_coupon_scan"><?php echo $text_coupon_scan; ?></label>
            <select name="filter_coupon_scan" id="filter_coupon_scan" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_all; ?></option>
              <?php foreach ($coupon_scans as $status) { ?>
              <option value="<?php echo $status['coupon_scan_id']; ?>" <?php if (!is_null($filter_coupon_scan)&&$status['coupon_scan_id'] == $filter_coupon_scan) { ?>selected="selected"<?php } ?>><?php echo $status['coupon_scan_name']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div><!--//col--> 
        
        <div class="col-sm-3">
         <div class="form-group">
                      <label class="control-label" for="filter_qc_status"><?php echo $text_qc_status; ?></label>
                      <select name="filter_qc_status" id="filter_qc_status" class="form-control" onchange="filter()">
                        <option value="*"><?php echo $text_select; ?></option>
                        <?php foreach($qc_statuses as $value=>$label){?>
                        <option value="<?php echo $value;?>" <?php echo ($value==$filter_qc_status&& !is_null($filter_qc_status))?'selected="selected"':'';?>><?php echo $label;?> <?php echo isset($qc_count[$value])?'('.$qc_count[$value].')':'(0)';?></option>
                        <?php } ?>  
                      </select>
                    </div>
            
                    </div><!--//col--> 
        <div class="col-sm-3">
        
            <?php if($has_edit && isset($export)) { ?>
          <div class="form-group"><br/> 
            <a href="<?php echo $export;?>" class="btn btn-primary"><span class="badge alert-error"><?php echo $total;?></span> &nbsp; <i class="fa fa-download"></i> <?php echo $text_download_results; ?></a> 
            </div>
        
              <?php } ?>
              
        </div><!--//col--> 
              
              
      </div><!--//row 2--> 
    </div>
    <!--panel-body -->
  </div>
  <!--box -->
  
 

</div>
<!-- /.container -->


<div class="container-fluid">
  <div class="box">
    <div class="box-header">
      <h2 class="box-title">
      <?php echo $heading_title; ?></h3>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $results; ?></div>
        <div class="col-sm-6 text-right">
          <nav><?php echo $pagination; ?></nav>
        </div>
      </div>
      <form action="" method="post" enctype="multipart/form-data" id="form-problem">
        <div class="table-responsive">
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <th style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></th>
                
                
                <th class="text-left"><?php if ($sort == 'coupon_code') { ?>
                  <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $text_code; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_name; ?>"><?php echo $text_code; ?></a>
                  <?php } ?>
                </th>
                <th class="text-left"><?php echo $text_coupon_value;?></th>
                <th class="text-left"><?php echo $text_reward_type;?></th>
                <th class="text-left"><?php echo $text_staff;?></th>
                <th class="text-left"><?php echo $text_store;?></th>
                <th class="text-left"><?php echo $text_distributor; ?></th>
                <th class="text-left"><?php echo $text_reward_month; ?></th>
                <th class="text-left"><?php echo $text_coupon_status; ?></th>
                <th class="text-left"><?php echo $text_coupon_scan; ?></th>
                <th class="text-right">Day Remaining</th>
              </tr>
            </thead>
            <tbody>
              <?php if ($coupons) { ?>
              <?php foreach ($coupons as $coupon) { ?>
              <tr>
                <td class="text-center">
                  <input type="checkbox" name="selected[]" value="<?php echo $coupon['coupon_id']; ?>" <?php if (in_array($coupon['coupon_id'], $selected)) { ?>checked="checked"<?php } ?> id="pr<?php echo $coupon['coupon_id']; ?>"/><label for="pr<?php echo $coupon['coupon_id']; ?>">&nbsp;</label>
                </td>
                <td class="text-left"><?php echo $coupon['coupon_code']; ?></td>
                <td class="text-left"><?php echo $coupon['coupon_value']; ?></td>
                <td class="text-left"><?php echo $coupon['reward_type']; ?> <br/> <?php echo $coupon['coupon_prefix']; ?> -  <?php echo $coupon['prefix_title']; ?></td>

                <td class="text-left"><?php echo $coupon['username']; ?></td>
                <td class="text-left">
                
                <?php echo $coupon['store_code']; ?><br/> 
                <a href="<?php echo $coupon['plan_href']; ?>" target="_blank">
                <?php echo $coupon['store_name']; ?>
                </a></td>
                <td class="text-left">
               <?php echo $coupon['distributor_code']; ?> <br/> - <?php echo $coupon['distributor_name']; ?>
                </td>
                <td class="text-left"><?php echo $coupon['yearmonth']; ?></td>
                
                <td class="text-left"><?php echo $coupon['date_upload']; ?> - <?php echo $coupon['coupon_status']; ?><br/> 
                </td>
                <td class="text-left"><?php echo $coupon['scan_status']; ?></td>
                <td class="text-right"><?php echo $coupon['time_remaining']; ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="text-center" colspan="11"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </form>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $results; ?></div>
        <div class="col-sm-6 text-right">
          <nav><?php echo $pagination; ?></nav>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript"><!--
  function filter() {
    url = 'index.php?route=catalog/coupon_history';
  
    var filter_coupon_code = $('input[name=\'filter_coupon_code\']').val();
  
    if (filter_coupon_code) {
      url += '&filter_coupon_code=' + encodeURIComponent(filter_coupon_code);
    }

    var filter_coupon_status = $('select[name=\'filter_coupon_status\']').val();
  
    if (filter_coupon_status != '*') {
      url += '&filter_coupon_status=' + encodeURIComponent(filter_coupon_status);
    }

    var filter_coupon_scan = $('select[name=\'filter_coupon_scan\']').val();
  
    if (filter_coupon_scan != '*') {
      url += '&filter_coupon_scan=' + encodeURIComponent(filter_coupon_scan);
    }

    var filter_reward_type_id = $('select[name=\'filter_reward_type_id\']').val();
  
    if (filter_reward_type_id != '*') {
      url += '&filter_reward_type_id=' + encodeURIComponent(filter_reward_type_id);
    }
    
    var filter_yearmonth = $('select[name=\'filter_yearmonth\']').val();
  
    if (filter_yearmonth != '*') {
      url += '&filter_yearmonth=' + encodeURIComponent(filter_yearmonth);
    }
	
    var filter_distributor_id = $('select[name=\'filter_distributor_id\']').val();
  
    if (filter_distributor_id != '*') {
      url += '&filter_distributor_id=' + encodeURIComponent(filter_distributor_id);
    }
    var filter_sup_id = $('select[name=\'filter_sup_id\']').val();
  
    if (filter_sup_id != '*') {
      url += '&filter_sup_id=' + encodeURIComponent(filter_sup_id);
    }

    var filter_round = $('select[name=\'filter_round\']').val();
  
    if (filter_round != '*') {
      url += '&filter_round=' + encodeURIComponent(filter_round);
    }
    var filter_qc_status = $('select[name=\'filter_qc_status\']').val();
  
    if (filter_qc_status != '*') {
      url += '&filter_qc_status=' + encodeURIComponent(filter_qc_status);
    }
	

    location = url;
  }
  //-->
</script>

