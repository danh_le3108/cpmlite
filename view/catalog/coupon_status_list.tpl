  <div class="container-fluid">
  
  
      <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
     
     <div class="box" id="filter-area">
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label class="control-label" for="filter_name"><?php echo $text_search; ?></label>
            <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $text_search; ?>" id="filter_name" class="form-control" onchange="filter()"/>
          </div>
        </div>
        
        <div class="col-sm-3">
          <button type="button" id="button-clear" class="btn btn-primary pull-right"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>
        </div>
         <div class="col-sm-3 text-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-code').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      
      
      </div>
    </div>
    <!--panel-body -->
  </div>
  <!--box -->
  
		
    <div class="box">
      <div class="box-header">
        <h2 class="box-title"> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
	 <div class="row">
          <div class="col-sm-6 text-left"><?php echo $results; ?></div>
          <div class="col-sm-6 text-right"><nav><?php echo $pagination; ?></nav></div>
        </div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-code">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td style="width: 1px;" class="text-center">#</td>
                  <td class="text-left"><?php if ($sort == 'coupon_status_name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $text_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $text_name; ?></a>
                    <?php } ?></td>
                
                  <td class="text-right"><?php echo $text_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($categories) { ?>
                <?php foreach ($categories as $category) { ?>
                <tr>
                  <td class="text-center">
                    <input type="checkbox" name="selected[]" value="<?php echo $category['coupon_status_id']; ?>" <?php if (in_array($category['coupon_status_id'], $selected)) { ?>checked="checked"<?php } ?> id="pr<?php echo $category['coupon_status_id']; ?>"/><label for="pr<?php echo $category['coupon_status_id']; ?>">&nbsp;</label>
                    </td>
                    <td class="text-center"><?php echo $category['coupon_status_id']; ?></td>
                  <td class="text-left"><?php echo $category['coupon_status_name']; ?></td>
                  <td class="text-right"><a href="<?php echo $category['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="3"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $results; ?></div>
          <div class="col-sm-6 text-right"><nav><?php echo $pagination; ?></nav></div>
        </div>
      </div>
    </div>
	
  </div>
  
  
  <script type="text/javascript"><!--
  function filter() {
  	url = 'index.php?route=catalog/coupon_status';

	var filter_name = $('input[name=\'filter_name\']').val();

  	if (filter_name) {
  		url += '&filter_name=' + encodeURIComponent(filter_name);
  	}
  	location = url;
  }
  //-->
</script>