<div class="panel panel-default">
  <div class="panel-heading"><?php echo $heading_title; ?></div>
  <p style="text-align: center;"><?php echo $text_project; ?></p>
  <?php foreach ($projects as $project) { ?>
  <?php if ($project['project_id'] == $project_id) { ?>
  <a href="<?php echo $project['url']; ?>"><b><?php echo $project['project_name']; ?></b></a><br />
  <?php } else { ?>
  <a href="<?php echo $project['url']; ?>"><?php echo $project['project_name']; ?></a><br />
  <?php } ?>
  <?php } ?>
  <br />
</div>
