<form method="post" action="index.php?route=plan/plan_user/save&plan_id=<?=$plan['plan_id']?>" id="plan_users">
  <div class="box box-info">
    <div class="box-header">
      <h3 class="box-title">Nhân viên</h3>
    </div>
    <div class="box-body">
      <?php if ($hasAdd) { ?>
      <div class="input-group">
        <input type="text" id="search_user" value="" placeholder="Họ tên, usercode, username" class="form-control" autocomplete="off">
          <a type="button" class="input-group-addon"><i class="fa fa-plus"></i> Thêm</a>
      </div>
      <br>
      <?php } ?>
      <table class="table table-bordered" id="user_list">
        <tr>
          <th width="30%">Nhân viên</th>
          <th>Checkin</th>
          <th>Upload</th>
          <th>Note</th>
          <?php if ($hasDel) { ?>
          <th class="text-center"><i class="fa fa-trash" aria-hidden="true" style="color: red"></i></th>
          <?php } ?>
        </tr>
        <?php foreach($plan_users as $pu) {?>
        <tr id="pu_<?=$pu['id']?>">
          <td title="<?=$pu['user_id']?> - Last update: <?=$pu['user_updated']?>" style="color: <?=$pu['checkin'] > $plan['time_checkin'] ? 'red' : ''?>">
            <?=$pu['usercode']?><br>
            <?=$pu['fullname']?>
          </td>
          <td>
            <input type="text" name="users_checkin[<?=$pu['id']?>]" value="<?=$pu['checkin'] == '0000-00-00 00:00:00' ? '' : $pu['checkin']?>" class="form-control datetime" data-date-format="YYYY-MM-DD HH:mm:ss" autocomplete="off">
          </td>
          <td>
            <input type="text" name="users_checkout[<?=$pu['id']?>]" value="<?=$pu['checkout'] == '0000-00-00 00:00:00' ? '' : $pu['checkout']?>" class="form-control datetime" data-date-format="YYYY-MM-DD HH:mm:ss" autocomplete="off">
          </td>
          <td>
            <textarea class="form-control" name="users_note[<?=$pu['id']?>]" rows="1"><?=$pu['user_note']?></textarea>
          </td>
          <td>
            <?php if ($hasDel) { ?>
            <a class="btn" style="color:#F00;" onclick="delete_pu(<?=$pu['id']?>)"><i class="fa fa-trash"></i></a>
            <?php } ?>
          </td>
        </tr>
        <?php } ?>
      </table>
    </div>
    <?php if($hasEdit) { ?>
    <div class="box-footer clearfix">
      <div class="col-sm-8  message">
        
      </div>
      <div class="col-sm-4">
        <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Lưu</button>
      </div>
    </div>
    <?php } ?>
  </div>
</form>
<script type="text/javascript">
   $('#search_user').autocomplete({
   
     'source': function(request, response) {
        if (encodeURIComponent(request) != '') {
          $.ajax({
   
             url: 'index.php?route=plan/plan_user/search_user&plan_id=<?=$plan['plan_id']?>&filter_global=' +  encodeURIComponent(request),
       
             dataType: 'json',
       
             success: function(json) {
       
               response($.map(json, function(item) {
                 return {
                    label: item.usercode + ' - ' + item.fullname,
                    value: item.user_id,
                    user_id: item.user_id
                 }
       
               }));
       
             },
       
           });
        }
     },
   
     'select': function(item) {
        $.get('index.php?route=plan/plan_user/add_plan_user&plan_id=<?=$plan['plan_id']?>&user_id='+item.user_id,function(data) {
          if (data) {
            var tr = '<tr id="pu_'+data.id+'"><td title="'+item.user_id+'" style="color: ">'+data.usercode+'<br>'+data.fullname+'</td><td><input type="text" name="users_checkin['+item.user_id+']" value="" class="form-control datetime" data-date-format="YYYY-MM-DD HH:mm:ss" autocomplete="off"></td><td><textarea class="form-control" name="users_note['+item.user_id+']" rows="1" autocomplete="off"></textarea></td>';
            <?php if ($hasDel) { ?>
            tr += '<td><a class="btn" style="color:#F00;" onclick="delete_pu('+data.id+')"><i class="fa fa-trash"></i></a></td></tr>';
            <?php } ?>
            $('#user_list').append(tr);
            $('#plan_users').find('.message').html('<div class="alert alert-success alert-sm" style="padding: 5px"><i class="fa fa-exclamation-circle"></i> Thành công: Đã thêm nhân viên!<button type="button" class="close" data-dismiss="alert">×</button></div>');
         
          }
        },'JSON')
     }
   
   });

  function delete_pu(pu_id) {
    $('#btn-confirm-delete').unbind('click');
    $('#modal-confirm-delete').modal('show');
    $('#btn-confirm-delete').click(function(event) {
        event.preventDefault();
        $.get("index.php?route=plan/plan_user/delete_plan_user&id="+pu_id, function(data) {
          $('#pu_'+pu_id).remove();
          $('#modal-confirm-delete').modal('hide');
        })
    });
  }
</script>