<div id="modal-confirm" class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-body">
            <div class="mpadding message"> Bạn chắc chắn muốn xóa ? </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-hidden="true">Close</button>
            <a id="btn-modal-confirm" class="btn btn-primary pull-right" style="margin-right:15px;">Ok</a>
         </div>
      </div>
   </div>
</div>
<!--//module-modal -->

<script type="text/javascript"><!--
   function loadPlanRoute(route_id, param_name, param_value) {
      plan_id = getURLVar('plan_id');
      route_url = 'index.php?route=plan/' + route_id + '/info&plan_id=' + plan_id;
      form_id = route_id;
      if (param_value) {
         route_url += '&' + param_name + '=' + param_value;
         form_id += param_value;
      }
      $('#' + form_id).load(route_url);
   }

   function updateFixed(qc_note_id, is_fixed) {
      $.ajax({
         url: 'index.php?route=plan/plan_note/approved_fix&plan_id=<?php echo $plan_id;?>&qc_note_id=' + qc_note_id,
         type: 'POST',
         dataType: 'json',
         data: 'is_fixed=' + is_fixed,
         success: function(json) {
            if (json['success']) {
               loadPlanRoute('plan_note');
            }
            if (json['error']) {
               $('#plan_note .message').html('<div class = "alert alert-danger alert-sm" > < i class = "fa fa-check-circle" > < /i> '+json['error']+'<button type="button" class="close" data-dismiss="alert">×</button > < /div>');
            }
         }
      });
   }

   function confirmRemoveFix() {
      $('#modal-confirm .message').html('Bạn có chắc muốn hủy bỏ yêu cầu khắc phục? <br/><span class="text-red"> Đề nghị xóa hủy bỏ tick QC code lỗi này.</span>');
      $('#modal-confirm').modal('show');
      $('#btn-modal-confirm').click(function(event) {
         event.preventDefault();
         $('#modal-confirm').modal('hide');
         updateFixed('0', '0');
      });
   }
</script>

<div class="container-fluid" id="plan">
    <div class="box box-widget">
      <div class="panel-body">
         <div class="row">
            <div class="col-sm-4">
            </div>
            <div class="col-sm-8 text-right body-btn">
               <a onclick="loadHistory('<?php echo $plan_id;?>')" data-toggle="tooltip" title="<?php echo $heading_plan_history; ?>" class="btn btn-primary">Log Edit</a>
               <a href="<?php echo $back ;?>" data-toggle="tooltip" title="<?php echo $button_back; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-6">
         <?= $plan_store?>
         
      </div>
      <div class="col-md-6"><?= $plan_image_audio?></div>
   </div>
   <div class="row">
      <?= $plan_survey?>
   </div>
   <div class="row">
      <div class="col-md-12"><?= $plan_confirm?></div>
   </div>
   <div class="row">
      <div class="col-md-12"><?= $plan_qc?></div>
   </div>
   <div class="row">
      <div class="col-md-12"  id="plan_note"><?= $plan_note?></div>
   </div>
</div>

<style type="text/css">
   .sticky-container {
      position:fixed;
      right:-152px;
      top:150px;
      width:180px;
      z-index: 999;
   }
   .sticky-container li {
      list-style-type:none;
      background-color:#222d32;
      color:#efefef;
      line-height:33px;
      height:33px;
      -webkit-transition:all 0.25s ease-in-out;
      -moz-transition:all 0.25s ease-in-out;
      -o-transition:all 0.25s ease-in-out;
      transition:all 0.25s ease-in-out;
      cursor:pointer;
   }
   .sticky-container li:hover {
      margin-left:-120px;
   }
   .sticky-container li i {
      margin: 0 8px;
   }
</style>

<?php if($is_user) { ?>
<div class="sticky-container">
   <!--  <li onclick="compareImage()">
      <i class="fa fa-image"></i>Lọc hình ảnh</li> -->
   <li onclick="scrollToElem('plan_store')">
      <i class="fa fa-info-circle"></i>Thông tin
   </li>
   <li onclick="scrollToElem('posm')">
      <i class="fa fa-check-square-o"></i>Nhập liệu
   </li>
   <li onclick="scrollToElem('plan_image')">
      <i class="fa fa-image"></i>Ghi âm & Hình ảnh
   </li>
   <li onclick="scrollToElem('plan_confirm')">
      <i class="fa fa-pencil"></i>Qc xác nhận
   </li>
   <li onclick="scrollToElem('plan_qc')">
      <i class="fa fa-code"></i>Stick qc
   </li>
   <li onclick="scrollToElem('plan_note')">
      <i class="fa fa-comments"></i>Note
   </li>
   <li onclick="download_images(<?=$plan_id?>)">
      <i class="fa fa-download"></i>Tải hình ảnh
   </li>
</div>
<?php } ?>

<script type="text/javascript">
	$('.thumbnails').magnificPopup({
      type:'image',
      delegate: 'a.img',
      gallery: {enabled:true},
      image: {
         verticalFit: true,
         titleSrc: function (item) {
            var image_id = item.el.closest('.col-sm-6').attr('id');
            caption = '';
            if (typeof image_id !== "undefined") {
               image_id = image_id.replace('img_','');
               var caption = '<div class="popup"><a onclick="rotate(\''+image_id+'\',\'-90\');" class="btn btn-success" title="-90"><i class="fa fa-rotate-left"></i></a> <a onclick="rotate(\''+image_id+'\',\'180\');" class="btn btn-success" title="180"><i class="fa fa-arrow-up"></i></a> <a onclick="rotate(\''+image_id+'\',\'90\');" class="btn btn-success" title="+90"><i class="fa fa-rotate-right"></i></a></div><div style="min-width:200px">'+item.src+'</div>';
            }
            return caption;
         }
      }
   })

   $('#plan').on('submit', 'form', function(e){
      e.preventDefault();
      var form = $(this);
      var box_info = form.find('.box-info').attr('id');
      <?php if(!$is_admin) { ?>
      if ($('input[name^="codes"]:checked').length == 0 && box_info != 'plan_qc') {
         form.find('.message').html('<div class="alert alert-danger alert-sm" style="padding: 5px"><i class="fa fa-exclamation-circle"></i> Cảnh báo: Vui lòng check vào lỗi QC!<button type="button" class="close" data-dismiss="alert">×</button></div>');
         return;
      }
      <?php } ?>
      $.post(
         form.attr('action'),
         form.serialize(),
         function (data){
           if (data == 1) {
             form.find('.message').html('<div class="alert alert-success alert-sm" style="padding: 5px"><i class="fa fa-exclamation-circle"></i> Thành công: Đã cập nhật plans!<button type="button" class="close" data-dismiss="alert">×</button></div>');
           }
      })
   })

   function scrollToElem(elem){
      $('html,body').animate({
         scrollTop: $('#'+elem).offset().top -10
      }, 500);
   }

   function download_images(plan_id) {
      if (plan_id) {
         window.location = 'index.php?route=plan/plan_image_audio/downloadImages&plan_id=<?=$plan_id?>';
      }
   } 

   $('#plan_note').on('click', '#button-note', function(){
      var note_id = $('#note-select').val();
      var comment = $('#note-comment').val();
      if (note_id == 0) {
         alert('Chưa chọn loại note!');
         return;
      } else if (comment == '') {
         alert('Chưa nhập nội dung note!');
         return;
      }
      $.get('index.php?route=plan/plan_note/save&plan_id=<?=$plan_id?>&note_id='+note_id+'&comment='+comment, function(data){
         if (data.status == 1) {
           loadPlanRoute('plan_note');
         } else {
           $('#plan_note').find('.message').html('<div class="alert alert-danger alert-sm" style="padding: 5px"><i class="fa fa-exclamation-circle"></i> '+data.msg+'<button type="button" class="close" data-dismiss="alert">×</button></div>');
         }
      },'JSON')
   })

   $('#plan_note').on('click', '.del-note', function(){
      var btn = $(this).closest('.form-group');
      var qc_note_id = btn.attr('id').replace('qc_note_id_','');
      $.get('index.php?route=plan/plan_note/delete&qc_note_id='+qc_note_id, function(data){
         if(data == 1){
           btn.remove();
         }
      })
   })
</script>

<script type="text/javascript">
   function loadHistory(plan_id){
      $.ajax({
         url: 'index.php?route=plan/plan_history&plan_id='+plan_id,
         dataType: 'html',
         success: function(html) {
            $('#import_history .modal-body').html(html);
            $('#import_history').modal('show');
         }
      });
   }    
</script>