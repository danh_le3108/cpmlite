<style>

   .tr_info.is_read .store_title{

   color:#800000;

   }

   .text-grey{color:#666;}

   .red{color:#f00;}

   .blur{color:#999;}

   .orange{color:#F60;}

   .table-list thead tr td{ font-weight:600;}

   .t_map td{position:relative;}

   .t_map .btn{

   z-index:999;

   position:absolute;

   right:10px;

   top:10px;

   }

   .box-map #bodymap .btn{display:none;}

   #bodymap{ }

   #bodymap.mopen{}

   .table .table>thead>tr>th,

   .table .table>tbody>tr>th,

   .table .table>tfoot>tr>th,

   .table .table>thead>tr>td,

   .table .table>tbody>tr>td,

   .table .table>tfoot>tr>td {

   border-top: none !important;

   font-weight: 500;

   }

   .table-responsive table  table{

   margin-bottom: 20px;

   }

   .btn-xs {

   padding: 1px 2px;

   font-size: 8px;

   line-height: 1;

   border-radius: 3px;

   }

   .color-red{color:#F00;}

   .breadcrumb { margin: 0; }

   .popover_td {

    width: 25px;

    float: left;

    text-align: center;

    padding: 1px 0;

  }

  .status_0 {

    color: #F00;

  }

  .status_2 {

    color: #3c8dbc;

  }

  .status_1 {

    color: #00a65a;

  }

</style>

<div class="container-fluid">

  <form method="POST">

    <div class="row">

      <div class="col-md-3">

        <div class="box box-info" style="padding:0px 0 20px 0">

          <div class="box-header">

            <h3 class="box-title">Bước 1: Lọc plan cần chuyển</h3>

          </div>

          <div class="box-body no-padding">

            <div class="col-md-12 col-sm-3">

              <div class="form-group">

                <label class="control-label" for="input-filter_global">Tìm kiếm</label>

                <input type="text" name="filter_global" value="" placeholder="Tìm kiếm" id="input-filter_global" class="form-control filter" onchange="javascript:filter()"/>

              </div>

            </div>

            <!-- round name-->

            <!-- <div class="col-md-12 col-sm-3">

              <div class="form-group">

                <label class="control-label">Tháng</label>

                <select name="filter_round_name" class="form-control filter" onchange="filter()">

                   <option value="*"> --- Chọn --- </option>

                   <?php foreach($rounds as $r => $total) { ?>

                   <option value="<?=$r?>"><?=$r?> (<?=$total?>)</option>

                   <?php } ?>

                </select>

              </div>

            </div> -->

            

            <!-- province -->

            <!-- <div class="col-md-12 col-sm-3">

              <div class="form-group">

                <label class="control-label">Tỉnh thành</label>

                <select name="filter_province_id" class="form-control chosen filter" onchange="filter()">

                   <option value="*"> --- Chọn --- </option>

                   <?php foreach($provinces as $p) { 

                      $count = !empty($count_province[$p['province_id']]) ? $count_province[$p['province_id']] : 0;

                      

                      ?>

                   <option  value="<?=$p['province_id']?>"><?=$p['name']?> (<?=$count?>)</option>

                   <?php } ?>

                </select>

              </div>

            </div> -->

            <div class="col-md-12 col-sm-3">

              <div class="form-group">

                <label class="control-label">Nhân viên</label>

                <select name="filter_user_id" class="form-control chosen filter" onchange="filter()">

                   <option value="*"> --- Chọn --- </option>

                   <?php foreach($staffs as $s) { 

                      $count = !empty($count_staff[$s['user_id']]) ? $count_staff[$s['user_id']] : 0;

                      

                      ?>

                   <option value="<?=$s['user_id']?>"><?=$s['username'].' - '.$s['fullname']?> (<?=$count?>)</option>

                   <?php } ?>

                </select>

              </div>

            <!-- staff -->

            </div>

            <!-- <div class="col-md-12 col-sm-3">

               <div class="form-group">

                  <label class="control-label">Trạng thái</label>

                  <select name="filter_plan_status" class="form-control filter" onchange="filter()">

                     <option value="*"> --- Chọn --- </option>

                     <option value="0">Chưa thực hiện</option>

                     <option value="2">Đang thực hiện</option>

                     <option value="1">Đã thực hiện</option>

                  </select>

               </div>

            </div> -->

            <!-- <div class="col-sm-12">

              <a class="btn-primary btn-sm pull-right" href="index.php?route=plan/plan"><i class="fa fa-eraser"></i>Xóa bộ lọc</a>

            </div> -->

          </div>

          <!-- /.box-body -->

        </div>

        <div class="box box-info" style="padding:0px 0 20px 0">

          <div class="box-header">

            <h3 class="box-title">Bước 3: Chọn nhân viên cần chuyển plan</h3>

          </div>

          <div class="box-body no-padding">

            <div class="col-md-12 col-sm-3">

              <div class="form-group">

                <select class="form-control chosen" name="user_id_to"> 

                   <option value=""> --- Chọn --- </option>

                   <?php foreach($staffs as $s) { 

                      $count = !empty($count_staff[$s['user_id']]) ? $count_staff[$s['user_id']] : 0;

                      

                      ?>

                   <option value="<?=$s['user_id']?>"><?=$s['username'].' - '.$s['fullname']?> (<?=$count?>)</option>

                   <?php } ?>

                </select>

              </div>

            </div>

            <div class="col-sm-12">

              <button type="submit" class="btn btn-primary btn-sm pull-right"><i class="fa fa-eraser"></i>Chuyển plan</button>

            </div>

          </div>

          <!-- /.box-body -->

        </div>

        <div class="box box-info" style="padding:0px 0 20px 0">

          <div class="box-header">

            <h3 class="box-title">Lịch sử chuyển plan</h3>

          </div>

          <div class="box-body no-padding">

            <div class="col-md-12 col-sm-3">

              <?php foreach($history as $h) {

                echo $h['data'].'<br><br>';

              } ?>

            </div>

          </div>

        </div>

      </div>

      <div class="col-md-9" style="padding-left: 0;padding-right: 0">

        <div class="container-fluid">

          <?php if (!empty($success)) { ?>

          <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>

            <button type="button" class="close" data-dismiss="alert">&times;</button>

          </div>

          <?php } ?>

          <?php if (!empty($error_warning)) { ?>

          <div class="alert alert-danger"><i class="fa fa-check-circle"></i> <?php echo $error_warning; ?>

            <button type="button" class="close" data-dismiss="alert">&times;</button>

          </div>

          <?php } ?>

        </div>

        <div class="container-fluid">

          <div class="box box-info">

            <div class="box-header">

              <h3 class="box-title">Bước 2: Chọn plan cần chuyển</h3>

            </div>

            <div class="box-body table-responsive no-padding">

              <div class="panel-body">

                <table class="table table-hover table table-bordered">

                  <thead>

                    <tr>

                      <th class="text-center" width="1">#</th>

                      <th class="text-center" width="1"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></th>

                      <th width="40%">Cửa hàng</th>

                      <th class="text-center" >Plan name</th> 

                      <th class="text-center" >Nhân viên</th>  

                      <th class="text-center" >Trạng thái</th> 

                    </tr>

                  </thead>

                  <tbody>

                    <?php  foreach($plans as $i => $plan){ ?>

                    <tr>

                      <td class="text-center"><?=$i+1?></td>

                      <td>

                        <input type="checkbox" name="selected[]" value="<?=$plan['plan_id']?>" <?=in_array($plan['plan_id'],$plan_checked) ? 'checked' : ''?>/>

                      </td>

                      <td>

                        <a target="_blank" href="index.php?route=plan/plan/edit&plan_id=<?=$plan['plan_id']?>"><?=$plan['store_code']?> - <?=$plan['store_name']?></a>

                        <span class="blur">

                          <br>

                          <i class="fa fa-map-marker"></i> &nbsp<?php echo $plan['store_address_raw'];?>

                        </span>

                      </td>

                      <td class="text-center">

                        <?=$plan['plan_name']?>

                      </td>

                      <td class="text-center"><?=$plan['usercode']?></td>

                      <td class="text-center">

                        <span class="status_<?=$plan['plan_status']?>">

                          <?=isset($pl_status[$plan['plan_status']]) ? $pl_status[$plan['plan_status']] : 'Chưa thực hiện';?>

                        </span>

                      </td>

                    </tr>

                    <?php } ?>

                  </tbody>

                </table>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </form>

</div>





<script type="text/javascript">



   

   $('input[name=\'filter_global\']').autocomplete({

   

     'source': function(request, response) {

   

       $.ajax({

   

         url: 'index.php?route=plan/plan/autocomplete&filter_global=' +  encodeURIComponent(request),

   

         dataType: 'json',

   

         success: function(json) {

   

           response($.map(json, function(item) {

   

             return {

   

               label: item['store_code'] + ' - ' + item['store_name'],

               value: item['plan_id'],

                store_code: item['store_code']

             }

   

           }));

   

         },

   

       });

   

     },

   

     'select': function(item) {

        console.log(item);

       $('input[name=\'filter_global\']').val(item['store_code']);

        filter();

     }

   

   });

   

</script>