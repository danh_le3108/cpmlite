<div class="box box-info" id="plan_distribution_check">
  <div class="box-header">
    <h3 class="box-title">Nhập liệu</h3>
  </div>
  <div class="box-body">
    <?php 
    foreach($groups as $group_code => $group) { ?>
    <form method="post" action="index.php?route=plan/plan_distribution_check/save&plan_id=<?=$plan_id?>&group_code=<?=$group_code?>">
      <div class="box box-success collapsed-box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title"><?=$group['group_name'] ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table">
            <thead>
              <th width="70%">Name</th>
              <th class="text-center" width="10%">Code</th>
              <th class="text-center" width="20%">Status</th>
            </thead>
            <tbody>
            <?php foreach($group['attrs'] as $attr) {?>
              <tr>
                <td><?=$attr['attribute_name']?></td>
                <td class="text-center"><?=$attr['attribute_code']?></td>
                <td class="text-center"><input type="number" class="form-control" name="attrs[<?=$attr['attribute_id']?>]" value="<?=!empty($attrs_data[$attr['attribute_id']]) ? $attrs_data[$attr['attribute_id']]['value'] : 2?>"></td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          <?php if($hasEdit) { ?>
          <div class="col-sm-8 message">
          </div>
          <div class="col-sm-4">
            <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Lưu</button>
          </div>
          <?php } ?>
        </div>
      </div>
    </form>
    <?php } ?>
  </div>
</div>