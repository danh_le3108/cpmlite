<table class="table table-bordered" style="width: 70%;margin-bottom: 10px">
  <tr style="background-color: #ffb732;color: white;">
    <th>Nhân viên</th>
    <th>Số đơn hàng</th>
    <th>Số hình</th>
    <th>Số khách hàng</th>
  </tr>
  <?php foreach($plan_users as $pu) { ?>
  <tr>
    <td>
      <?=$pu['usercode']?> - <?=$pu['fullname']?>
    </td>
    <td><?=$pu['order_number']?></td>
    <td><?=$pu['image_number']?></td>
    <td><?=$pu['cus_number']?></td>
  </tr>
  <?php } ?>
</table>

<table class="table table-bordered">
  <tr style="background-color: #99ccff;color: white;">
    <?php if($isUser) { ?>
      <th class="text-center" width="15%" rowspan="2">Người tạo</th>
    <?php } ?>
    <th class="text-center" width="10%" rowspan="2">Mã ĐH</th>
    <th class="text-center" rowspan="2">Thông tin</th>
    <th class="text-center" width="15%" colspan="3">Samples</th>
    <?php if($isUser) { ?>
      <th class="text-center" rowspan="2">Số KH</th>
      <th class="text-center" rowspan="2">Số hình</th>
    <?php } ?>
    <?php if($hasDel) { ?> 
    <th class="text-center" width="4%" rowspan="2"><i class="fa fa-trash" aria-hidden="true"></th>
    <?php } ?>
  </tr>
  <tr style="background-color: #99ccff;color: white;">
    <th class="text-center">KOTEX</th>
    <th class="text-center">DIANA</th>
    <th class="text-center">OTHERS</th>
    <!-- <th class="text-center">TOTAL</th> -->
  </tr>
  <tr style="font-size: 15px; color: #ff7f7f; background-color: #d6eaff; font-weight: bold;" id="order-sumary">
    <td class="text-center" colspan="3"></td>
    <td class="text-center"></td>
    <td class="text-center"></td>
    <td class="text-center"></td>
     <?php if($isUser) { ?>
    <td class="text-center"></td>
    <td class="text-center"></td>
    <?php } ?>
    <?php if($hasDel) { ?> 
    <td></td>
    <?php } ?>
  </tr>
  <?php 
  $total_kotex = $total_diana = $total_others = $order_detail_total = $image_total = 0;

  foreach($plan_orders as $order) { 
    $order_detail_total += $order['order_detail_total'];
    $image_total += $order['image_total'];
  ?>
  <tr style="color: #696969"  id="order_<?=$order['order_id']?>">
    <?php if($isUser) { ?>
    <td>
      <?=$users[$order['user_id']]['fullname']?>
    </td>
    <?php } ?>
    <td class="text-center">
      <a class="edit-order">
        <?=$order['order_code']?>
      </a>
    </td>
    <td>
      <table>
       <!--  <tr>
          <td class="text-center"><i class="fa fa-user-o" aria-hidden="true" title=""></i></td>
          <td style="padding-left: 5px"><?=$order['location']?></td>
        </tr> -->
        <tr>
          <td class="text-center"><i class="fa fa-map-marker" aria-hidden="true" title="Địa chỉ"></i></td>
          <td style="padding-left: 5px">
          <?php 
            $address = array();
            if (!empty($order['no'])) {
              $address[] = $order['no'];
            }
            if (!empty($order['street'])) {
              $address[] = $order['street'];
            }
            if (!empty($order['ward'])) {
              $address[] = $order['ward'];
            }
            if (!empty($order['district']) && isset($districts[$order['district']])) {
              $address[] = $districts[$order['district']];
            } 
            if (!empty($order['province']) && isset($provinces[$order['province']])) {
              $address[] = $provinces[$order['province']];
            } 
            echo implode(', ',$address);
          ?>
          </td>
        </tr>
      </table>
    </td>
    <?php 
      $kotex = $diana = $others = 0;
      foreach ($order['order_detail'] as $od) {
        if ($od['current_brand'] == 1) {
          $kotex++;
        } elseif ($od['current_brand'] == 2) {
          $diana++;
        } elseif ($od['current_brand'] == 3) {
          $others++;
        }
      }
      $total_kotex += $kotex;
      $total_diana += $diana;
      $total_others += $others;
    ?>
    <td class="text-center"><?=$kotex?></td>
    <td class="text-center"><?=$diana?></td>
    <td class="text-center"><?=$others?></td>
    <?php if($isUser) { ?>
    <td class="text-center"><?=$order['order_detail_total']?></td>
    <td class="text-center"><?=$order['image_total']?></td>
    <?php } ?>
    <!-- <td class="text-center"><?=$kotex+$diana+$others?></td> -->
    <?php if($hasDel) { ?> 
    <td class="text-center">
      <a class="delete_order">
        <i class="fa fa-trash text-warning" aria-hidden="true"></i>
      </a>
    </td>
    <?php } ?>
  </tr>
  <?php } ?>
</table>
<script type="text/javascript">
  $('#order-sumary').children().eq(0).html('Total (<?=count($plan_orders)?>)');
  $('#order-sumary').children().eq(1).html('<?=$total_kotex?>');
  $('#order-sumary').children().eq(2).html('<?=$total_diana?>');
  $('#order-sumary').children().eq(3).html('<?=$total_others?>');
  $('#order-sumary').children().eq(4).html('<?=$order_detail_total?>');
  $('#order-sumary').children().eq(5).html('<?=$image_total?>');
  var orders = <?=json_encode($plan_orders)?>;
</script>