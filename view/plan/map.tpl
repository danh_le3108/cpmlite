<?php echo $header;?>
<style>
  #content{position: absolute;top:50px;left:0;right:0;bottom:0;}
  .sidebar-open #content{left:200px;}
  #content>.wrap{position:relative; width:100%; height:100%;}
  #map {
    min-height:500;
    width: 100%;
    height:100%;
    position: absolute;
    top:0px;left:0;right:0;bottom:0;
  }
  .map-info {
    background-color: #000;
    color: #fff;
    min-height: 122px;
    opacity: 0.7;
    position: absolute;
    right: 0;
    top: 0;
    width: 150px;
    z-index: 9990;
  }
  .map-info>div {
    display: block;
    line-height: 25px;
    margin: 5px 0;
    overflow: hidden;
    padding-bottom: 5px;
    padding-left: 50px;
    padding-top: 5px;
    width: 100%;
  }
  .map-info div.icon-green {
  background: rgba(0, 0, 0, 0) url("<?php echo $http_server; ?>assets/image/marker/green.png") no-repeat scroll 0 0;
  }
  .map-info div.icon-red {
  background: rgba(0, 0, 0, 0) url("<?php echo $http_server; ?>assets/image/marker/red.png") no-repeat scroll 0 0;
  }
  .map-info div.icon-yellow {
  background: rgba(0, 0, 0, 0) url("<?php echo $http_server; ?>assets/image/marker/grey.png") no-repeat scroll 0 0;
  }
  #filter-area {
    background-color: #000;
    color: #fff;
    opacity: 0.8;
    position: absolute;
    z-index: 99;
    top: 0;
    left: 120px;
    right: 160px;
    padding: 5px 0 10px;
  }
  #filter-area input, #filter-area select  { opacity: 1; color:#000000; }
 /* #filter-area .form-group { margin-bottom: 5px;}
  #filter-area .form-control,
  #filter-area .chosen-container .chosen-selection--single {
    height: 28px;
    font-size: 14px;
    padding: 0 10px;
  }*/
  #map-container h4.map-title {
    font-family: 'Open Sans Condensed', sans-serif;
    font-size: 22px;
    font-weight: 400;
    padding: 10px;
    background-color: #48b5e9;
    color: white;
    margin: 0;
    width: 100%;
    border-radius: 0;
  }
  #map-container #map-content {
    padding: 10px;
  }
  .gm-style-iw {
    width: 350px !important;
    top: 15px !important;
    left: 0px !important;
    background-color: #fff;
    box-shadow: 0 1px 6px rgba(178, 178, 178, 0.6);
    border: 1px solid rgba(72, 181, 233, 0.6);
    border-radius: 0;
  }
  .gm-style-iw img {max-width: 100%;}
  .store_image { padding: 0 0 10px 0; }
  .breadcrumb { margin: 0; }
  .content { padding: 0 15px; }
  .ml-30{margin-left:30px;}
</style>
<div id="content">
  <div class="wrap">
    <div id="filter-area">
      <div class="col-sm-2">
        <div class="form-group">
          <label class="control-label" for="filter_global"><?php echo $text_search; ?></label>
          <input type="text" id="filter_global" name="filter_global" value="<?php echo $filter_global;?>" class="form-control filter" onchange="javascript:filter()">
        </div>
      </div>
      <div class="col-sm-2">
        <div class="form-group">
          <label class="control-label" for="filter_round_name"><?php echo $text_round; ?></label>
          <select name="filter_round_name" id="filter_round_name" class="form-control filter" onchange="javascript:filter()">
            <option value="*"><?php echo $text_all; ?></option>
            <?php foreach($rounds as $round => $total){?>
            <option value="<?php echo $round;?>" <?php echo ($round==$filter_round_name)?'selected="selected"':'';?>><?php echo $round;?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="form-group">
          <label class="control-label" for="filter_region_code">Vùng</label>
          <select name="filter_region_code" id="filter_region_code" class="form-control chosen filter" onchange="javascript:filter()">
            <option value="*"><?php echo $text_all; ?></option>
            <?php foreach($regions as $r){?>
            <option value="<?php echo $r['region_code'];?>"><?php echo $r['region_code'];?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="form-group">
          <label class="control-label" for="filter_province_id"><?php echo $text_province; ?></label>
          <select name="filter_province_id" id="filter_province_id" class="form-control chosen filter" onchange="javascript:filter()">
            <option value="*"><?php echo $text_all; ?></option>
            <?php foreach($provinces as $province){?>
            <option value="<?php echo $province['province_id'];?>" <?php echo ($province['province_id']==$filter_province_id)?'selected="selected"':'';?>><?php echo $province['name'];?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="form-group">
          <label class="control-label" for="input-filter_date"><?php echo $text_date; ?></label>
          <input type="text" id="input-filter_date" name="filter_date" value="<?php echo $filter_date;?>" placeholder="<?php echo $text_date; ?>" class="form-control datepicker filter" onchange="javascript:filter()">
        </div>
      </div><!--//col-->
      <div class="col-sm-2" style="margin-top: 25px">
        <div class="form-group pull-right">
          <button type="button" id="button-clear" class="btn btn-primary btn-sm"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>
          <br><br>
          <span class="badge alert-danger"><?php echo $plan_total;?> <?php echo $text_store;?> </span>
        </div>
      </div><!--//col-->
      <?php if($isUser) {?>
      <div class="col-sm-3" style="margin-top: -45px">
        <div class="form-group">
          <label class="control-label" for="filter_user_id"><?php echo $text_staff_user; ?></label>
          <select name="filter_user_id" id="filter_user_id" class="form-control chosen filter" onchange="filter()">
            <option value="*"><?php echo $text_select; ?></option>
            <?php foreach($staffs as $f) {?>
            <option value="<?=$f['user_id']?>" <?=$f['user_id'] == $filter_user_id ? 'selected' : '' ?>><?=$f['username'].' - '.$f['fullname']?></option>
            <?php } ?>
          </select>
        </div>
      </div><!--//col-->
      <?php } ?>
    </div>
    <div id="map"></div>
    <div class="map-info">
      <div class="icon-green dat" style="height: 40px;">Thành công<span class="badge"></span></div>
      <div class="icon-yellow ktc" style="height: 40px;">KTC <span class="badge"></span></div>
    </div>
  </div>
</div>
<div id="modal-alert" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
            <div class="mpadding"> Số cửa hàng quá nhiều (<?php echo $plan_total;?> địa điểm), không đủ tài nguyên để hiển thị, vui lòng chọn thêm tiêu chí tìm kiếm!</div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-hidden="true">Ok</button>
           <!-- <a id="btn-modal-alert" class="btn btn-primary pull-right" style="margin-right:15px;">Ok</a>//--> 
        </div>
    </div>
  </div>
</div><!--//module-modal -->

<div id="map-container" style="display: none;">
   <h4 class="map-title">title</h4>
   <div id="map-content">
      <div class="store_image col-md-4"><img src=""></div>
      <div class="col-md-8">
         <p><strong>Địa chỉ: </strong><span></span></p>
         <p><strong>Số điện thoại: </strong><span></span></p>
         <p><strong>Plan: </strong><a href=""><span></span></a></p>
         <p><strong>Kết quả: </strong><span style="color: red"></span></p>
      </div>
   </div>
</div>
<?php echo $footer;?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_MAP_EMBED_API_KEY;?>"></script>
<script type="text/javascript">
  var plan_total = <?=$plan_total?>;
  var lat = 10.824725;
  var lng = 106.640540
  var coordinates = <?php echo json_encode($plans);?>;
  if(coordinates.length > 0) {
    lat = parseFloat(coordinates[0].latitude);
    lng = parseFloat(coordinates[0].longitude);
  }
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 9,
    center: {lat: lat, lng: lng}
  });
  if(plan_total >= 3000){
    $('#modal-alert').modal('show');
  } else {
    if(coordinates.length > 0) {
      var i;
      var infowindow = new google.maps.InfoWindow();
      for (i = 0; i < coordinates.length; i++) {
        if (coordinates[i].plan_rating == 1) {
          var icon = '<?php echo $http_server; ?>assets/image/marker/green.png';
        } else if (coordinates[i].plan_rating == -1) {
          var icon = '<?php echo $http_server; ?>assets/image/marker/red.png';
        } else {
          var icon = '<?php echo $http_server; ?>assets/image/marker/grey.png';
        }
        var image_icon = new google.maps.MarkerImage(
          icon,
          new google.maps.Size(50, 50), // size
          new google.maps.Point(0, 0), // origin
          new google.maps.Point(16, 32) // anchor
        );
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(coordinates[i]['latitude'], coordinates[i]['longitude']),
          icon: image_icon,
          animation: google.maps.Animation.DROP,
          map: map

        });

        google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
          return function() {
            var content = $('#map-container');
            content.find('.map-title').html(coordinates[i].store_code+' - '+coordinates[i].store_name);
            content.find('img').attr('src', coordinates[i].store_image);
            content.find('a').attr('href','index.php?route=plan/plan/edit&plan_id='+coordinates[i].plan_id);
            var spans = content.find('span');
            spans.eq(0).html(coordinates[i].store_address_raw);
            spans.eq(1).html(coordinates[i].store_phone);
            spans.eq(2).html(coordinates[i].plan_name);
            if (coordinates[i].plan_rating == 1) {
              var rating = "Thành công";
            } else if (coordinates[i].plan_rating == -1) {
              var rating = 'Rớt';
            } else {
              var rating = "Không thành công";
            }
            spans.eq(3).html(rating);
            infowindow.setContent('<div id="map-container">'+content.html()+'</div>');
            infowindow.open(map, marker);
         }
        })(marker, i));

        // START INFOWINDOW CUSTOMIZE.
        // The google.maps.event.addListener() event expects
        // the creation of the infowindow HTML structure 'domready'
        // and before the opening of the infowindow, defined styles are applied.
        google.maps.event.addListener(infowindow, 'domready', function() {
          // Reference to the DIV that wraps the bottom of infowindow
          var iwOuter = $('.gm-style-iw');
          iwOuter.children(':nth-child(1)').css({'display' : 'block'});
          // Since this div is in a position prior to .gm-div style-iw.
          // We use jQuery and create a iwBackground variable,
          // and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
          
          var iwBackground = iwOuter.prev();
          // Removes background shadow DIV
          iwBackground.children(':nth-child(2)').css({'display' : 'none'});
          // Removes white background DIV
          iwBackground.children(':nth-child(4)').css({'display' : 'none'});
          // Changes the desired tail shadow color.
          iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});
          // Reference to the div that groups the close button elements.
          var iwCloseBtn = iwOuter.next();
          // Apply the desired effect to the close button
          iwCloseBtn.css({opacity: '1', right: '40px', top: '3px', height: '27px', width: '27px', border: '7px solid #48b5e9', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9'});
          // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
          iwCloseBtn.mouseout(function(){
          $(this).css({opacity: '1'});
          });
        }); 
      }
    }
  }

  // function filter(){
  //   url = 'index.php?route=plan/map';
  //   var filter_global = $('input[name=\'filter_global\']').val();
  //   if (filter_global) {
  //     url += '&filter_global=' + encodeURIComponent(filter_global);
  //   }
  
  //   var filter_round_name = $('select[name=\'filter_round_name\']').val();
  //   if (filter_round_name != '*') {
  //     url += '&filter_round_name=' + encodeURIComponent(filter_round_name);

  //   }

  //   var filter_province_id = $('select[name=\'filter_province_id\']').val();
  //   if (filter_province_id != '*') {
  //     url += '&filter_province_id=' + encodeURIComponent(filter_province_id);
  //   }
      
  //   var filter_user_id = $('select[name=\'filter_user_id\']').val();
  //   if (filter_user_id != '*') {
  //     url += '&filter_user_id=' + encodeURIComponent(filter_user_id);
  //   } 

  //   var filter_date = $('input[name=\'filter_date\']').val();
  //   if (filter_date) {
  //     url += '&filter_date=' + encodeURIComponent(filter_date);
  //   } 
  //   location = url;
  // }
</script>