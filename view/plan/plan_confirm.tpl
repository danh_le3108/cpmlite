<div class="box box-info" id="plan_confirm">
  <div class="box-header">
    <h3 class="box-title">QC xác nhận</h3>
  </div>
  <div class="box-body">
    <div class="clearfix">
     <?php 
     $is_qc = 0;
     foreach($user_groups as $group){?>
     <div class="col-sm-4">
     
     <div class="form-group clearfix">
          <label class="col-sm-12"><?php echo $group['group_name'];?></label>
           <div class="col-sm-12" id="qc_group_id_<?=$group['user_group_id']?>">
     <?php foreach($confirms as $confirm){  ?>
     <?php if($confirm['user_group_id']==$group['user_group_id']){  ?>
          <div class="clearfix" >
          <div class="input-group">
            <div class="form-control"><?php echo $confirm['date_added'];?></div>
            <span class="input-group-addon"><i class="fa fa-check"></i> <?=$confirm['username']?></span>
          </div>
          </div>
     <?php } ?>
     <?php if($confirm['user_id']==$user_id){
      $is_qc = 1;
     } ?>
     <?php } ?>
        </div>
        </div>
     </div>
     <?php } ?>
    </div>
  </div>
  <?php if($hasEdit && !$is_qc) {?>
  <div class="box-footer">
    <div class="col-sm-4">
      <div class="alert alert-success alert-sm message" style="display: none; padding: 5px"><i class="fa fa-exclamation-circle"></i>Xác nhận thành công!<button type="button" class="close" data-dismiss="alert">×</button></div>
    </div>
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
      <div class="input-group btn-block">
        <div class="form-control"><?=$user_log?></div>
        <span class="input-group-btn">
          <button type="button" class="btn btn-success" onclick="save_qc_confirm()"><i class="fa fa-check"></i>Xác nhận đã ký</button>
        </span>
      </div>
    </div>
  </div>
  <?php } ?>
</div>

<script type="text/javascript">
  function save_qc_confirm() {
    $('.btn-block').remove();
    $.get('index.php?route=plan/plan_confirm/save&plan_id=<?=$plan_id?>', function(data){
      if (data) {
        var html = '<div class="clearfix"><div class="input-group"><div class="form-control"><?=date("Y-m-d H:i:s")?></div><span class="input-group-addon"><i class="fa fa-check"></i>'+data.username+'</span></div></div>';
        $('#qc_group_id_'+data.user_group_id).html(html);
        // $('#qc_group_id_2').html(html);
        
        $('.message').css('display','block');
      }
    },"JSON")
  }
</script>