<div id="posm" class="col-sm-12">
   <form method="post" action="index.php?route=plan/plan_survey/save&plan_id=<?=$plan['plan_id']?>" id="plan_survey">
      <div class="box box-info">
         <div class="box-header">   
            <h3 class="box-title">Nhập liệu</h3>
         </div>
         <div class="box-body">
            <div class="panel-body">

               <?php 
                  $group_name = '';
                  foreach($groups as $group_name => $attrs) { 
                  ?>
                  <div class="col-sm-6">
                     <div class="text-success"><?=$group_name?></div>
                     <table class="table">
                     <?php foreach($attrs as $attr) { ?>
                        <tr>
                           <td><?=$attr['attribute_name']?></td>
                           <td>
                           <?php if ($attr['input'] == 'select') { 
                              $attr['options'] = json_decode($attr['options']);
                           ?>
                           <select id="<?=$attr['attribute_code']?>" class="form-control" name="attrs[<?=$attr['attribute_id']?>]">
                              <option value=""> --- Chọn --- </option>
                              <?php foreach($attr['options'] as $key => $op) { ?>
                                 <option value="<?=$op->id?>" <?=!empty($attr_data[$attr['attribute_id']]) && $op->id == $attr_data[$attr['attribute_id']]['value'] ? 'selected' : ''?>><?=$op->name?></option>
                              <?php } ?>
                           </select>
                           <?php } else { ?>
                           <input id="<?=$attr['attribute_code']?>" type="text" class="form-control" name="attrs[<?=$attr['attribute_id']?>]" value="<?=!empty($attr_data[$attr['attribute_id']]) ? $attr_data[$attr['attribute_id']]['value'] : '' ?>">
                           <?php } ?>
                           </td>
                        </tr>
                     <?php } ?>
                     </table>
                </div>
                <?php } ?>
            </div>
         </div>
         <?php if($hasEdit) { ?>
            <div class="box-footer clearfix">
               <div class="col-sm-8 message">
                
               </div>
               <div class="col-sm-4">
                  <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Lưu</button>
               </div>
            </div>
         <?php } ?>
      </div>
   </form>
</div>