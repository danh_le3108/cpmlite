
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
        <a href="<?php echo $user_popup; ?>" class="img-thumbnail popup"><img width="60" src="<?php echo $user_thumb; ?>" /></a>
        <?php echo $staff_fullname; ?> (<?php echo $total; ?>)
        
     
        <button type="button" class="btn btn-warning pull-right btn-sm" data-dismiss="modal" onclick="closeModalPlans();"><i class="fa fa-close"></i> Close</button>
        </h4>
      </div>
      <div class="modal-body">
          <div class="row">
        <div class="col-sm-6 text-left"><?php echo $results; ?></div>
        <div class="col-sm-6 text-right"><?php echo $pagination; ?></div><br/> 
      </div>
        <div class="table-responsive">
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th><?php echo $text_store_code; ?></th>
              <th>Tác phong</th>
              <th><?php echo $text_store_name; ?></th>
              <th>Skin Care</th>
              <th>AdHoc</th>
              <th><?php echo $text_qc_status; ?></th>
              <th><?php echo $text_time_checkin; ?>/<?php echo $text_time_upload; ?></th>
              <th>Upload trễ</th>
            </tr>
          </thead>
          <tbody id="tr_place">
          <?php if ($plans) { ?>
            <?php foreach ($plans as $plan) {?>
              <tr class="tr_info <?php echo (in_array($user_id,$plan['users_read']))?'is_read':'';?>">
                <td width="40" class="text-center">
                  <a href="<?php echo $plan['popup']; ?>" class="img-thumbnail popup"><img width="80" src="<?php echo $plan['store_image']; ?>" /></a>
                  <br/>
                  
                </td>
                <td width="40" class="text-center">
                  <a href="<?php echo $plan['tac_phong_popup']; ?>" class="img-thumbnail popup"><img width="80" src="<?php echo $plan['tac_phong']; ?>" /></a>
                </td>
                <td class="text-left" width="200">
                  <a href="<?php echo $plan['edit']; ?>" title="<?php echo $plan['plan_name']; ?>" target="_blank" class="store_title"><strong><?php echo $plan['store_name']; ?> - <?php echo $plan['store_code']; ?></strong></a><br /> 
                  <span class="blur"><?php echo $plan['store_customer_code']; ?></span><br/> 
                  
                  <span class="blur"><?php echo $plan['store_address_raw']; ?></span>
                </td>
                <td width="150">
                 <div class="clearfix">
                  <?php foreach($plan['plan_survey'] as $sid => $survey){?>
                  <?php if(isset($survey['survey_id'])&&in_array($survey['survey_id'],$survey_audit)){?>
                  <?php echo $surveys[$survey['survey_id']]['survey_name']; ?> - 
                  <span class="red"><?php echo $rating_result[$survey['rating_manual']]; ?></span><br/> 
           		<?php }?>
           		<?php }?>
                  </div>
                  
                  
                </td>
                <td width="150">
                  
                  <div class="clearfix">
                  <?php foreach($plan['plan_survey'] as $sid => $survey){?>
                  <?php if(isset($survey['survey_id'])&&in_array($survey['survey_id'],$survey_adhoc)){?>
                  <?php echo $surveys[$survey['survey_id']]['survey_name']; ?> - 
                  <span class="red"><?php echo $rating_result[$survey['rating_manual']]; ?></span><br/> 
           		<?php }?>
           		<?php }?>
                  </div>
                  
                  
                </td>
                <td width="80">
                  <small class="red">(<?php echo $plan['plan_qc_text']; ?>)</small>
                </td>
                <td width="200">
                  Checkin: <?php echo $plan['time_checkin']; ?> <br>
                  Upload: <?php echo $plan['time_upload']; ?>
                </td>
                <td width="50">
                  <?php if($plan['thoi_gian_tre']>0){ ?>
                  <i class="red fa fa-check-circle fa-2x" data-toggle="tooltip" title="Trễ <?php echo $plan['so_gio_tre'];?> giờ so với thời hạn 24 giờ quy định."></i>
                  <?php } ?>
                </td>
              </tr>
            <?php } ?>
          <?php } else { ?>
            <tr>
              <td class="text-center" colspan="9"><?php echo $text_no_results; ?></td>
            </tr>
          <?php } ?>
          </tbody>
          <!----> 
        </table>
      </div>
          <div class="row">
        <div class="col-sm-6 text-left"><?php echo $results; ?></div>
        <div class="col-sm-6 text-right"><?php echo $pagination; ?></div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning pull-right btn-sm" data-dismiss="modal" onclick="closeModalPlans();"><i class="fa fa-close"></i> Close</button>
      </div>
    </div>
  </div>
<script type="text/javascript"><!--
$(document).ready(function() {
  $('body').magnificPopup({
    type:'image',
    delegate: 'a.popup',
    image: {
      verticalFit: true,
      titleSrc: function (item) {
        var caption = item.el.attr('title');
        var user_id = item.el.attr('data-id');
        var filename = item.el.attr('data-filename');
        var img_type = item.el.attr('data-img-type');
        
        return caption;
      }
    },
    gallery: {
      enabled:true
    }
  });
});
//--></script>