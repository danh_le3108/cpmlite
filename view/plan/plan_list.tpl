<style type="text/css">
  .date {
    width: 25px;
    float: left;
    text-align: center;
  }
  .status_0 {
    color: #F00;
  }
  .status_2 {
    color: #3c8dbc;
  }
  .status_1 {
    color: #00a65a;
  }
</style>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-7"><?php echo $pagination;?> </div>
    <div class="col-sm-5 text-right"><?php echo $results;?></div>
  </div>
  <div class="box box-info">
    <div class="box-header">
      <h3 class="box-title"><?php echo $heading_title;?></h3>
    </div>
    <div class="box-body table-responsive no-padding">
      <div class="panel-body">
        <table class="table table-hover table table-bordered">
          <thead>
            <tr>
              <th class="text-center" width="1">ID</th>
              <?php if($user_id) { ?>
              <th class="text-center" width="1">Selfie</th>
              <?php } ?>
              <th>Cửa hàng</th>
              <th class="text-center" width="10%">Plan name</th>  
              <th class="text-center" width="10%">Trạng thái</th> 
              <?php if($user_id) { ?>
              <th class="text-center" width="5%">QC</th>
              <?php } ?>
              <th class="text-center" width="20%">NV thực hiện</th>
              <th class="text-center" width="15%">Ngày</th>
              <?php if($hasDel) {?>
              <th width="5%" class="text-center"> Xóa </th>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <?php  foreach($plans as $plan){
              $filename = DIR_MEDIA . $plan['image_overview'];
              if (is_file($filename)) {
                $avatar = $model_tool_image->resize($plan['image_overview'], 60, 60);
                $popup = HTTP_SERVER.'media/'.$plan['image_overview'];
              }else{
                $avatar = $thumb;
                $popup = '';
              }

              $no_avatar = $model_tool_image->resize('avatar.jpg', 60, 60);
              $filename = DIR_MEDIA . $plan['image_selfie'];
              if (is_file($filename)) {
                $selfie = $model_tool_image->resize($plan['image_selfie'], 60, 60);
                $selfie_popup = HTTP_SERVER.'media/'.$plan['image_selfie'];
              }else{
                $selfie = $no_avatar;
                $selfie_popup = '';
              }

              $rating = '';
              if ($plan['plan_rating'] == 1) {
                $rating = 'Thành công';
              } elseif ($plan['plan_rating'] == -2) {
                $rating = 'KTC';
              }
            ?>
            <tr class="tr_info" id="plan_id_<?=$plan['plan_id']?>">
              <td class="img-popup text-center"><a href="<?=$popup?>" class="img-thumbnail"><img src="<?=$avatar?>" alt=""/></a><br/><?php echo $plan['plan_id'];?></td>
              <?php if($user_id) { ?>
              <td class="img-popup text-center"><a href="<?=$selfie_popup?>" class="img-thumbnail"><img src="<?=$selfie?>" alt=""/></a></td>
              <?php } ?>
              <td>
                <a target="_blank" href="index.php?route=plan/plan/edit&plan_id=<?=$plan['plan_id']?>"><span <?=!empty($plan['users_read']) && in_array($user_id,explode(',',$plan['users_read'])) ? 'style="color:#800000"' : ''?>><strong><?php echo $plan['store_code'];?> - <?php echo $plan['store_name'];?>
                <span class="blur">
                  <br>
                  <i class="fa fa-map-marker"></i> &nbsp<?php echo $plan['store_address_raw'];?><br> 
                  <?php if(!empty($plan['store_phone'])) { ?>
                  <i class="fa fa-phone"></i>  <?php echo $plan['store_phone'];?>
                  <?php } ?>
                </span>
              </td>
              <td class="text-center">
                <a target="_blank" href="index.php?route=plan/plan/edit&plan_id=<?=$plan['plan_id']?>"><?=$plan['plan_name']?></a><br>
              </td>
              <td class="text-center">
                <span class="status_<?=$plan['plan_status']?>">
                  <?=isset($pl_status[$plan['plan_status']]) ? $pl_status[$plan['plan_status']] : 'Chưa thực hiện'?>
                </span>
                <small style="color: red"><?=$plan['plan_status'] == 1 ? '('.$rating.')' : ''?></small>
              </td>
              <?php if($user_id) { ?>
              <td class="text-center"><i class="fa fa-<?php echo ($plan['plan_qc']==1)?'check text-success':'text-warning';?>"></i></td>
              <?php } ?>
              <td>
                <table>
                  <tr>
                    <td class="date">
                      <i class="fa fa-user-o text-primary" aria-hidden="true"></i>
                    </td>
                    <td>
                      <?php echo $plan['usercode'];?>
                    </td>
                  </tr>
                  <?php if($user_id) { ?>
                  <?php if(!empty($plan['version'])) { ?>
                  <tr>
                    <td class="date">
                      <i class="fa fa-mobile text-primary"></i>
                    </td>
                    <td>App version: <?=$plan['version']; ?></td>
                  </tr>
                  <?php } ?>
                  <?php if(!empty($plan['latitude']) && !empty($plan['longitude'])) { ?>
                  <tr>
                    <td class="date text-primary">
                      <i class="fa fa-map-marker"></i>
                    </td>
                    <td>
                      <a target="_blank" href="<?='https://www.google.com/maps/search/'.$plan['latitude'].','.$plan['longitude'].'/@'.$plan['latitude'].','.$plan['longitude'].',17z'?>"><?=$plan['latitude'].', '.$plan['longitude']?></a>
                    </td>
                  </tr>
                  <?php } ?>
                  <?php } ?>
                </table>
              </td>
              <?php if($user_id) { ?>
              <td>
                <table>
                  <tr>
                    <td class="date">
                      <i class="fa fa-map-marker <?php echo (!empty($plan['time_checkin']) && $plan['time_checkin'] != '0000-00-00 00:00:00')?'check text-success':'text-grey';?>" title="Checkin"></i> </td>
                    <td><?=$plan['time_checkin']?></td>
                  </tr>
                  <tr>
                    <td class="date">
                      <i class="fa fa-cloud-upload <?php echo (!empty($plan['time_upload']) && $plan['time_upload'] != '0000-00-00 00:00:00')?'text-success':'text-grey';?>" title="Upload"></i> 
                    </td>
                    <td><?=$plan['time_upload']?></td>
                  </tr>
                </table>
              </td>
              <?php } else { ?>
              <td class="text-center"><?=date('Y-m-d',strtotime($plan['time_upload']));?></td>
              <?php  } ?>
              <?php if($hasDel) {?>
              <td class="text-center">
                <a class="btn" style="color:#F00;" onclick="deletePlan('<?php echo $plan['plan_id']; ?>','<?php echo $plan['plan_name'].' - '.$plan['store_name']; ?>')"><i class="fa fa-trash"></i></a>
              </td>
              <?php } ?>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-7"><?php echo $pagination;?> </div>
    <div class="col-sm-5 text-right"><?php echo $results;?></div>
  </div>
</div>

<?php if(!empty($staff)) {
  $imag_path = 'files/users/' . $staff['username'].'/'; 
  if ($staff['portrait'] && is_file(DIR_MEDIA .$imag_path. $staff['portrait'])) {
    $avatar = $model_tool_image->resize($imag_path.$staff['portrait'], 100, 100);
    $popup  = HTTP_SERVER.'media/files/users/' . $staff['username'].'/'.$staff['portrait'];
  }else{
    $avatar = $model_tool_image->resize('avatar.jpg', 100, 100);
    $popup = '';
  }
?>
  <div id="staff" style="display: none">
    <a href="<?=$popup?>" class="img-thumbnail popup"><img width="60" src="<?=$avatar?>"></a>
    <?=$staff['fullname'].' ('.$plan_total.')'?>
  </div>
<?php } ?>

<script type="text/javascript">
  $('.img-popup').magnificPopup({
    type:'image',
    delegate: 'a.img-thumbnail',
    gallery: {
      enabled:true
    }
  });
</script>