<style type="text/css">
    .modal-lg {
        width: 70%;
    }
</style>
<div class="box-header">
    <h3 class="box-title">Log Edit Plan #<?=$plan_id?></h3>
</div>
<div class="box-body">
    <div id="status_history">
        <div class="clearfix">
            <table class="table border-0">
                <thead>
                    <tr>
                        <td class="text-left" width="10%">STT</td>
                        <td class="text-left" width="20%">User</td>
                        <td class="text-left" width="25%">Data cũ</td>
                        <td class="text-left" width="25%">Data mới</td>
                        <td class="text-left" width="20%">Ngày tạo</td>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($histories) { ?>
                    <?php foreach ($histories as $key => $h) { ?>
                        <tr>
                            <td class="text-left" width="10%"><?=$h['id']?></td>
                            <td class="text-left" width="20%"><?=$h['fullname']?></td>
                            <td class="text-left" width="25%" style="background-color: #ea1f1f29;">
                            <?php foreach ($h['history'] as $k => $v) { 
                                $check = is_array($v);?>
                                <?php if($check == 1) { ?>
                                <?=$k;?> : <?=$v[0];?>
                                <br>
                                <?php } ?> 
                            <?php } ?> 
                            </td>
                            <td class="text-left" width="25%" style="background-color: #90ee9078;">
                            <?php foreach ($h['history'] as $k => $v) { 
                                $check = is_array($v); 
                                ?> 
                                <?php if($check == 1) { ?>
                                <?=$k;?> : <?=$v[1];?>
                                <br>
                                <?php } ?> 
                            <?php } ?> 
                            </td>
                            <td class="text-left" width="20%"><?=$h['date_added']?></td>
                        </tr>
                        <?php } ?> 
                    <?php } else { ?>
                    <tr>
                        <td class="text-center" colspan="7">Không có thay đổi</td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="clearfix">
            <div class="col-sm-6 text-left"><?php echo $results; ?></div>
            <div class="col-sm-6 text-right"><?php echo str_replace('class="pagination"','class="pagination pagination-sm"', $pagination); ?></div>
        </div>      
    </div>
</div>

<script type="text/javascript">
    var is_array = function(input) {
        if (toString.call(input) === "[object Array]")
            return true;    
        return false;   
    };
</script>    
<script type="text/javascript">
    // function viewDetail(id){
        // var $this = $('#import_history .modal-body');
        // $this.empty();
        // $.ajax({
        //     type: 'POST',
        //     url: 'index.php?route=plan/plan_history/getDetail',
        //     dataType: 'json',
        //     data: '&id=' + id,
        //     success: function(json){
        //         var entries = Object.entries(json);
        //         console.log(entries.length);
        //         var  html = '<a class="pull-right" target="_blank" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times-circle" style="font-size: 2rem; margin-top: -8.5%; margin-right: -3%; color: #ea0e0e;"></i></a>';
        //             html += '<div>';
        //             html += '<h4>Chi tiết log id #' + id + '</h4>';
        //             html += '<table class="table table-bordered">';
        //             html += '<thead>';
        //             html += '    <tr>';
        //             html += '        <th class="text-left" width="50">Data old</th>';
        //             html += '        <th class="text-left" width="50">Data new</th>';
        //             html += '    </tr>';
        //             html += '</thead>';
        //             html += '<tbody>';
        //             if(entries.length > 0 ){
        //                 for (var $i = 0; $i < entries.length; $i++) {
        //                     for (var $j = 0; $j < entries.length; $j++) {
        //                         var data_old = entries[$i][1];
        //                         html += '<tr>';
        //                         html += '    <td class="text-left" style="background-color: #ea1f1f29; border:0" width="50">' + entries[$i][0] + ': ';
        //                         for(var $do = 0; $do < data_old.length; $do++){
        //                             html +=  data_old[$do];
        //                             html += '</td>';
        //                             html += '<td class="text-left" style="background-color: #90ee9078; border:0" width="50">' + entries[$i][0] + ': ' ;
        //                             html +=  data_old[$do+1];
        //                             html += '</td>';
        //                             break;
        //                         }
        //                         html += '</tr>';
        //                         break;
        //                     }
        //                 }
        //             }else{
        //                 html += '<tr><td colspan="2" class="text-center"> Không có dữ liệu </td></tr>';
        //             }
        //             html += '</tbody>';
        //             html += '</table>';
        //             html += '</div>';
        //         $this.html(html);
        //     }
        // });
    // }
</script>