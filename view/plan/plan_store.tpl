<form method="post" action="index.php?route=plan/plan_store/save&plan_id=<?=$plan['plan_id']?>" id="plan_store">
  <div class="box box-info">
    <div class="box-header">
      <h3 class="box-title">Plan #<?=$plan['plan_id']?> - <?=$plan['plan_name']?></h3>
    </div>
    <div class="box-body">
      <table class="table">
        <tr>
          <td width="20%">Overview</td>
          <td width="30%" class="thumbnails">
            <div class="thumbnail" id="overview" style="max-width: 110px">
              <a class="img" href="<?=$plan['popup']?>"><img src="<?=$plan['thumb']?>"/></a>
            </div>
          </td>
          <td width="20%"></td>
          <td width="30%"></td>
        </tr>
        <tr>
          <td>Mã cửa hàng</td>
          <td><a href="index.php?route=store/store/edit&store_id=<?=$plan['store_id']?>"><?=$plan['store_code']?></a></td>
          <td>Tên cửa hàng</td>
          <td><a href="index.php?route=store/store/edit&store_id=<?=$plan['store_id']?>">
          <?=$plan['store_name']?></a></td>
        </tr>
        <tr>
          <td>Mức trưng bày</td>
          <td>
            <select class="form-control" name="reason_id">
            <option value="0" > --- </option>
            <?php foreach($groups as $s) { ?>
              <option value="<?=$s['group_id']?>" <?=$plan['group_id'] == $s['group_id'] ? 'selected' : ''?>><?=$s['group_name']?></option>
            <?php } ?>
            </select>
          </td>
          <td>Loại cửa hàng</td>
          <td>
            <?=!empty($store_types[$plan['store_type_id']]) ? $store_types[$plan['store_type_id']]['type_name'] : ''?>
          </td>
        </tr>
        <tr>
          <td>Điện thoại</td>
          <td><?=$plan['store_phone']?></td>
          <?php if($isUser) { ?>
          <td>Nhân viên KS</td>
          <td>
            <?=$plan['usercode']?>
          </td>
          <?php } else { ?>
          <td></td>
          <td></td>
          <?php } ?>
        </tr> 
        <tr>
          <td>Địa chỉ</td>
          <td colspan="3"><?=$plan['store_address_raw']?></td>
          
        </tr>
        
        <tr>
          <td>Tọa độ gốc CH</td>
          <td colspan="3">
          
        
          <a href="https://www.google.com/maps/search/<?php echo $plan['store_latitude'];?>,<?php echo $plan['store_longitude'];?>/@<?php echo $plan['store_latitude'];?>,<?php echo $plan['store_longitude'];?>,17z" target="_blank">
          <i class="fa <?php echo ($plan['is_verify']==1)?'fa-check-circle':'fa-times-circle';?>"></i>
          <?php echo $plan['store_latitude'];?> , <?php echo $plan['store_longitude'];?></a>
          </td>
        </tr>
        <?php if($isUser) { ?>
        <tr>
          <td>Checkin</td>
          <td><input type="text" name="time_checkin" value="<?=$plan['time_checkin'] == '0000-00-00 00:00:00' ? '' : $plan['time_checkin']?>" class="form-control datetime" data-date-format="YYYY-MM-DD HH:mm:ss" autocomplete="off"></td>
          <td>Tọa độ Checkin</td>
          <td>
            <input type="text" class="form-control" name="latitude" value="<?=$plan['latitude']?>">
            <input type="text" class="form-control" name="longitude" value="<?=$plan['longitude']?>">
            
            <br/> 
            <a target="_blank" href="<?='https://www.google.com/maps/search/'.$plan['latitude'].','.$plan['longitude'].'/@'.$plan['latitude'].','.$plan['longitude'].',17z'?>"><?=$plan['latitude'].', '.$plan['longitude']?></a>
          
             <br/> <a class="btn btn-xs btn-<?php echo ($plan['is_verify']==1)?'success':'danger';?>" onclick="verifyLocation()"><i class="fa <?php echo ($plan['is_verify']==1)?'fa-check-circle':'fa-times-circle';?>"></i> Xác thực tọa độ</a>
       
        </tr>
        <tr>
          <td>Ngày upload</td>
          <td><input type="text" name="time_upload" value="<?=$plan['time_upload']=='0000-00-00 00:00:00' ? '' : $plan['time_upload']?>" class="form-control datetime" data-date-format="YYYY-MM-DD HH:mm:ss" autocomplete="off"></td>
          <td>Device:<br>App version:</td>
          <td><?=$plan['model']?><br><?=$plan['version']?></td>
        </tr>
        <?php } else { ?>
          <tr>
            <td>Ngày upload</td>
            <td><?=date('Y-m-d',strtotime($plan['time_upload']));?></td>
            <td></td>
            <td></td>
          </tr>
        <?php }?>
        <tr>
          <td style="color: red">Trạng thái</td>
          <td colspan="3">
            <select class="form-control" name="plan_status">
              <option value="0" >Chưa thực hiện</option>
              <option value="2" <?=$plan['plan_status'] == 2 ? 'selected' : ''?>>Đang thực hiện</option>
              <option value="1" <?=$plan['plan_status'] == 1 ? 'selected' : ''?>>Đã thực hiện</option>
            </select>
          </td>
        </tr>
        <!-- <tr>
          <td style="color: red">Kết quả</td>
          <td colspan="3" style="color: red; font-weight: bold;">
            <?php if($plan['plan_rating'] == 1) { 
              $rating = 'Đạt';
            } elseif ($plan['plan_rating'] == -1) { 
              $rating = 'Rớt';
            } elseif ($plan['plan_rating'] == -2) { 
              $rating = 'Không thành công';
            } else {
              $rating =  ''; 
            } 
            echo $rating;
            ?>
          </td>
        </tr> -->
        <tr>
          <td style="color: red">Ghi chú TC</td>
          <td colspan="3"><input type="text" name="note" value="<?=$plan['note']?>" class="form-control"></td>
        </tr>
        <tr>
          <td>Lý do KTC</td>
          <td colspan="3">
            <select class="form-control" name="reason_id">
            <option value="0" > --- </option>
            <?php foreach($reasons as $s) { ?>
              <option value="<?=$s['reason_id']?>" <?=$plan['reason_id'] == $s['reason_id'] ? 'selected' : ''?>><?=$s['reason_name']?></option>
            <?php } ?>
            </select>
          </td>
        </tr>
        <tr>
          <td style="color: red">Ghi chú KTC</td>
          <td colspan="3"><input type="text" name="note_ktc" value="<?=$plan['note_ktc']?>" class="form-control"></td>
        </tr>
      </table> 
    </div>
    <?php if($plan['group_id'] == 7) { ?>
    <div class="box-header">
      <h3 class="box-title">POSM yêu cầu lắp đặt</h3>
    </div>

    <div class="box-body">
      <table class="table">
        <tr>
          <td width="50%">Poster Milo Can</td>
          <td width="50%">
            <?=$plan['posm_milo_t4_can']?>
        </tr>
        <tr>
          <td width="50%">Poster Less Surgar</td>
          <td width="50%">
            <?=$plan['posm_milo_t4_sugar']?>
          </td>
        </tr>
        <tr>
          <td width="50%">Wobler Milo can</td>
          <td width="50%">
            <?=$plan['posm_milo_t4_wobler']?>
          </td>
        </tr>
        <tr>
          <td width="50%">Signalboard</td>
          <td width="50%">
            <?=$plan['posm_milo_t4_signboad']?></td>
        </tr>
      </table>
    </div>
    <?php } ?>
    <?php if($hasEdit) { ?>
    <div class="box-footer clearfix">
      <div class="col-sm-8  message">
        
      </div>
      <div class="col-sm-4">
        <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Lưu</button>
      </div>
    </div>
    <?php } ?>
  </div>

</form>

<div id="compare-image" class="modal fade in">
  
</div>
<script type="text/javascript">
  $('#btn_search_image').click(function(){
    $.get('index.php?route=tool/search_image/compare&filter_store_code=<?=$plan['store_code']?>', function(data){
      $('#compare-image').html(data)
      $('#compare-image').modal('show');
    })
  });
  
function verifyLocation() {
		$.ajax({
			url: 'index.php?route=plan/plan_store/verify&plan_id=<?php echo $plan['plan_id'];?>',
			dataType: 'json',
			type: 'post',
			data: 'plan_id=<?php echo $plan['plan_id'];?>',
			success: function(json) {
				if (json['success']) {
					$('#plan_store').load('index.php?route=plan/plan_store/info&plan_id=<?php echo $plan['plan_id'];?>');
				}
			},
		});
}
</script>