<style>
   td.fill_red,
   .fill_red td{background-color: #F66; color:#fff;}
   .bg-grey{background-color:#999;}
   td.fill_blue,
   .fill_blue td{background-color: #7F7FFF; color:#fff;}
   td.fill_grey,
   .fill_grey td{background-color:#ddd;}
   .red{color:#f00;}
   .blur{color:#999;}
   .table-list thead tr td{ font-weight:600;}
   .table > thead > tr > td {
   vertical-align: middle;
   }
   .progress {
   background-color: #838383; 
   }
   .spannote{
   font-size:8px;
   color:#ccc;
   }

   /*.fill_blue td, .fill_grey td, .fill_red td{
      padding-left: 3px !important; 
      padding-right: 3px !important;
   }*/
</style>
<style type="text/css">
   #progress-user .modal-dialog {
   width: 85% !important;
   }
   .tr_info.is_read .store_title{
   color:#800000;
   }
</style>
<div class="container-fluid">
   <div class="message" id="main_message">
      <?php if (!empty($error_warning)) { ?>
      <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
         <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <?php if (!empty($success)) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
         <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
   </div>
   <div class="box box-info" id="filter-area">
      <div class="box-header">
         <h3 class="box-title pull-right"><button type="button" id="button-clear" class="btn btn-primary btn-sm"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button></h3>
      </div>
      <div class="panel-body">
         <div class="row">
            <!-- <div class="col-sm-3">
               <div class="form-group">
               
                 <label class="control-label" for="filter_global"><?php echo $text_search; ?></label>
               
                 <input type="text" name="filter_global" value="" placeholder="<?php echo $text_search; ?>" id="filter_global" class="form-control" onchange="filter()"/>
               
               </div>
               
               </div> --><!--col -->
            <!--  -->
            <div class="col-sm-3">
                <div class="form-group">
                  <label class="control-label">Mức trưng bày</label>
                  <select name="filter_group_id" class="form-control filter" onchange="filter()">
                     <option value="*"> --- Chọn --- </option>
                     <?php foreach($projects as $ps) { ?>
                     <option value="<?=$ps['group_id']?>"><?=$ps['group_name']?></option>
                     <?php } ?>
                  </select>
                </div>
              </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label class="control-label" for="filter_round_name"><?php echo $text_round; ?></label>
                  <select name="filter_round_name" id="filter_round_name" class="form-control filter" onchange="filter()">
                     <option value="*"><?php echo $text_select; ?></option>
                     <?php foreach ($rounds as $round => $total) { ?>
                     <option value="<?php echo $round; ?>" <?php if ($round == $filter_round_name) { ?>selected="selected"<?php } ?>><?php echo $round; ?></option>
                     <?php } ?>
                  </select>
               </div>
            </div>
            <!--col -->
            <div class="col-sm-3">
               <div class="form-group">
                  <label class="control-label" for="filter_date"><?php echo $text_date; ?></label>
                  <div class="input-group">
                     <input type="text" class="form-control datepicker filter" name="filter_date" value="<?php echo $filter_date; ?>" onchange="filter()" placeholder="YYYY-MM-DD" />
                     <a class="input-group-addon" onclick="$('input[name=\'filter_date\']').val('<?php echo $today;?>');javascript:filter();">
                     <small><?php echo $text_today;?> </small>
                     </a>
                  </div>
                  <!--group -->
               </div>
            </div>
            <!--col -->
            <!--col -->
         </div>
         <!--row -->
      </div>
      <!--panel-body -->
   </div>
   <?php if(in_array($group_id, $managers)){ ?>
   <div class="box box-info">
      <div class="box-header">
         <h3 class="box-title"> <?php echo $text_project_progress;?></h3>
         <div class="pull-right box-tools">
            <!--  <a onclick="changeDate();" class="btn btn-primary"><i class="fa fa-calendar"></i> Đổi ngày hết hạn</a>
               <a onclick="changePlan();" class="btn btn-primary"><i class="fa fa-exchange"></i> Chuyển plan cho nhân viên</a> -->
         </div>
      </div>
      <div class="panel-body">
         <form method="post" enctype="multipart/form-data" id="form-plan">
            <div class="progress">
               <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo $all_percent; ?>"
                  aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $all_percent; ?>%">
                  <?php echo $all_percent; ?>% Complete
               </div>
            </div>
            <!---->
            <h4><?php echo $text_plan_total; ?>: <?php echo $all_total; ?></h4>
            <h4><?php echo $text_has_perform; ?>: <?php echo $all_made; ?></h4>
            <h4><?php echo $text_no_perform; ?>: <?php echo $all_total-$all_made; ?></h4>
            <?php if(!empty($filter_date)){?>
            <h4><?php echo $filter_date; ?>: <?php echo $filter_made; ?></h4>
            <div class="progress">
               <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo $filter_percent; ?>"
                  aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $filter_percent; ?>%">
                  <?php echo $filter_percent; ?>% 
               </div>
            </div>
            <!---->
            <?php } ?>
         </form>
      </div>
   </div>
   <?php } ?>
   <?php foreach($regions as $region){?>
   <?php if(isset($region['sup_ids'])&&isset($region['region_code'])){?>
   <div class="box box-info">
      <div class="box-header">
         <h3 class="box-title"> <?php echo $heading_title; ?> (<?=$region['region_code']?>)</h3>
      </div>
      <div class="panel-body">
         <form method="post" enctype="multipart/form-data" id="form-plan">
            <div class="table-responsive" style="margin-bottom:10px;">
               <table class="table table-bordered table-hover table-list">
                  <thead class="fill_blue">
                     <tr>
                        <!-- <td rowspan="2" class="text-center">Code</td> -->
                        <!-- <td rowspan="2" class="text-center">Checkin Today</td> -->
                        <td rowspan="2" class="text-center" style="min-width: 250px"><?php echo $text_sup_staff;?></td>
                        <td colspan="2" class="text-center"><?php echo $filter_date; ?></td>
                        <td rowspan="2" class="text-center"><?php echo $text_progress; ?></td>
                        <td rowspan="2" class="text-center"><?php echo $text_progress_percent; ?></td>
                        <td rowspan="2" class="text-center">TC/KTC</td>
                        <!-- <td colspan="2" class="text-center">Kênh</td> -->
                        
                        <td colspan="5" class="text-center"><?php echo $text_has_qc; ?></td>
                     </tr>
                     <tr>
                        <td>Checkin</td>
                        <td>Đã thực hiện</td>
                        <!-- <td class="text-center">ON</td>
                        <td class="text-center">OFF</td> -->
                        
                        <td class="text-center"><?php echo $text_total_qc; ?></td>
                        <td class="text-center"><?php echo $text_qc_by_pa; ?></td>
                        <td class="text-center"><?php echo $text_qc_by_dc; ?></td>
                        <td class="text-center"><?php echo $text_qc_by_sup; ?></td>
                        <td rowspan="2" class="text-center">% Data sai</td>
                     </tr>
                  </thead>
                  <?php foreach($sups as $sup){?>
                  <?php if($sup['region_code']==$region['region_code']){?>
                  <thead>
                     <tr>
                        <td class="text-center" colspan="18">
                           <div class="progress">
                              <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo $sup['percent']; ?>"
                                 aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $sup['percent']; ?>%">
                                 <?php echo $sup['percent']; ?>% Complete
                              </div>
                           </div>
                        </td>
                     </tr>
                  </thead>
                  <thead class="fill_grey">
                     <tr>
                        <!-- <td class="text-center"><?php echo $sup['usercode'];?> -->
                        <!-- </td> -->
                        
                        <td>
                           #<?php echo $sup['user_id'];?> - <?php echo $sup['usercode'];?> - <?php echo $sup['fullname'];?>  
                           <?php echo !empty($sup['telephone'])? ' - '.$sup['telephone']:''; ?>
                        </td>
                        <td class="text-center"><?php echo $sup['checkin_count'];?></td>
                        <td class="text-center"><?php echo $sup['sup_today']; ?></td>
                        <td class="text-center"><?php echo $sup['sup_plan_made'];?>/<?php echo $sup['plan_total'];?></td>
                        <td class="text-center"><?php echo $sup['percent']; ?>%</td>
                        <td class="text-center"><?php echo $sup['tc']; ?>/<?php echo $sup['ktc']; ?></td>
                        <td class="text-center"><?php echo $sup['sup_qc']; ?>/<?php echo $sup['sup_plan_made'];?></td>
                        <td class="text-center"><?php echo $sup['total_qc_pa']; ?></td>
                        <td class="text-center"><?php echo $sup['total_qc_dc']; ?></td>
                        <td class="text-center"><?php echo $sup['total_qc_sup']; ?></td>
                        <td class="text-center"><span> <?php echo $sup['sup_code_total']; ?> - <?php echo $sup['sup_code_percent']; ?>%</span></td>
                     </tr>
                  </thead>
                  <?php foreach($sup['staff'] as $staff){?>
                  <tbody>
                     <tr>
                        <!-- <td class="text-center">
                           <strong><?php echo $staff['usercode'];?></strong>
                           <br/> <span class="spannote"><a href="index.php?route=plan/plan&filter_plan_status=1&filter_user_id=<?= $staff['user_id']?>&filter_round_name=<?=$filter_round_name?>" target="_blank"><?php echo $staff['user_id']; ?></a></span>
                        </td> -->
                        
                        <!-- href="<?php echo $staff['href']; ?>" target="_blank" -->
                        <td><a onclick="modalPlans('<?php echo $staff['user_id']; ?>');"><strong>#<?php echo $staff['user_id'];?> - <?php echo $staff['usercode'];?> - <?php echo $staff['fullname'];?></strong></a> - <?php echo $staff['telephone'];?>
                           <br/>
                           <span class="label label-default">App: <?php echo $staff['app_version'];?></span>  &nbsp; 
                           <span id="ostaff<?php echo $staff['user_id'];?>">
                           <small class="label label-default" ><?php echo $staff['request_count'];?></small> 
                           Device:<?php echo $staff['device_model'];?>
                           </span>
                           <?php if(in_array($user_group_id,$group_edit)){?> 
                           - Imei:<?php echo $staff['device_imei'];?>
                           <?php if($staff['new_imei']!=$staff['device_imei']&&$staff['new_imei']!=''){?><br/> 
                           <span style="margin-top:10px;" id="nstaff<?php echo $staff['user_id'];?>" class="badge">New :<?php echo $staff['new_model'];?>  - Imei:<?php echo $staff['new_imei'];?>
                           <a onclick="approveDevice('<?php echo $staff['user_id'];?>','1')" class="btn btn-xs btn-success"><?php echo $text_approved;?></a>
                           <a onclick="approveDevice('<?php echo $staff['user_id'];?>','0')" class="btn btn-xs btn-danger">Từ chối</a>
                           </span> 
                           <?php } ?>
                           <?php } ?>
                        </td>
                        <td class="text-center"><?php echo $staff['checkin_count'];?></td>
                        <td class="text-center">
                           <a onclick="modalPlans('<?php echo $staff['user_id']; ?>');" target="_blank" data-toggle="tooltip" title="<?php echo $button_view;?>">
                           <?php echo $staff['staff_today']; ?></a>
                        </td>
                        <td class="text-center"><?php echo $staff['staff_plan_made'];?>/<?php echo $staff['plan_total'];?></td>
                        <td class="text-center"><?php echo $staff['percent']; ?>%</td>
                        <td class="text-center"><?php echo $staff['tc']; ?>/<?php echo $staff['ktc']; ?></td>
                        <td class="text-center"><?php echo $staff['staff_qc']; ?>/<?php echo $staff['staff_plan_made'];?></td>
                        <td class="text-center"><?php echo $staff['qc_by_pa']; ?></td>
                        <td class="text-center"><?php echo $staff['qc_by_dc']; ?></td>
                        <td class="text-center"><?php echo $staff['qc_by_sup']; ?></td>
                        <td class="text-center">
                           <span title="<?php echo $staff['code_total']; ?>"> <?php echo $staff['code_total']; ?> - <?php echo $staff['code_percent']; ?>%</span>
                        </td>
                     </tr>
                  </tbody>
                  <?php } ?>
                  <?php }}?>
               </table>
            </div>
         </form>
      </div>
   </div>
   <?php }} ?>
</div>

<div id="change_plan" class="modal fade">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <div class="modal-title pull-left" style="min-width:400px;">
               <label class="control-label" for="filter_user">Bước 1: Lọc plan chưa thực hiện từ Nhân viên dưới đây</label>
               <select name="filter_user" id="filter_user" class="form-control chosen" onchange="filter_plan()">
                  <option value="*"><?php echo $text_select; ?></option>
                  <?php foreach ($sups as $sup) { ?>
                  <optgroup label="<?php echo $sup['fullname']; ?>">
                     <?php foreach ($sup['staff'] as $staff) { ?>
                     <?php if($staff['plan_rest']>0) { ?>
                     <option value="<?php echo $staff['user_id']; ?>"><?php echo $staff['usercode']; ?> - <?php echo $staff['fullname']; ?> (Còn lại <?php echo $staff['plan_rest'];?>/<?php echo $staff['plan_total'];?> plan) </option>
                     <?php } ?>
                     <?php } ?>
                  </optgroup>
                  <?php } ?>   
               </select>
            </div>
            <button type="button" class="btn btn-danger btn-xs pull-right" data-dismiss="modal" aria-hidden="true">x</button>
            <!--
               <button type="button" class="change_plan-confirm btn btn-success btn-xs pull-right" style="margin-right:15px;">Xác nhận</button>//--> 
         </div>
         <div class="modal-body">
         </div>
         <div class="modal-footer">
            <div class="row">
               <div class="col-sm-8 col-xs-12 message">
                  <div class="btn btn-warning btn-sm"><i class="fa fa-exclamation-circle"></i> Vui lòng kiểm tra cẩn thận, mọi sự nhầm lẫn có thể gây phiền toái cho NV!</div>
               </div>
               <!--//col--> 
               <div class="col-sm-4 col-xs-12">
                  <b>Sau đó xác nhận &nbsp; &nbsp;</b>
                  <button type="button" class="change_plan-confirm btn btn-success pull-right" style="margin-right:15px;">Xác nhận</button>
               </div>
               <!--//col--> 
            </div>
            <!--//row--> 
         </div>
      </div>
   </div>
</div>
<script type="text/javascript"><!--
   function filter_plan(){
   
   var filter_url = 'index.php?route=plan/plan/getList&is_change=1&filter_limit=200&filter_plan_status=0';
   
      
   
    var filter_user = $('select[name=\'filter_user\']').val();
   
    
   
        if (filter_user) {
   
          filter_url += '&filter_user=' + encodeURIComponent(filter_user);
   
      $.ajax({
   
        url:filter_url,
   
        dataType: 'html',
   
        success: function(html) {
   
          $('#change_plan .modal-body').html(html);
   
          $('#change_plan .chosen').chosen({height: "120px",width: "100%"});
   
        }
   
      });
   
        
   
        }
   
    
   
    
   
     
   
   }
   
   function changePlan(){
   
    $('#change_plan').modal('show');
   
    $('.change_plan-confirm').click(function(event) {
   
      event.preventDefault();
   
            $('.change_plan-confirm').button('loading');
   
        $.ajax({
   
          url:'index.php?route=plan/progress/change_plan',
   
          type: 'POST',
   
          dataType: 'json',
   
            data: $('#change_plan input[type=\'text\'],#change_plan input[type=\'hidden\'],#change_plan input[type=\'radio\']:checked,#change_plan input[type=\'checkbox\']:checked,#change_plan select,#change_plan textarea'),
   
          success: function(json) {
   
                    $('.change_plan-confirm').button('reset');
   
            if(json['success']){
   
                $('#change_plan').modal('hide');
   
                $('#change_plan .modal-body').html('');
   
                $('#main_message').html('<div class="alert alert-success alert-sm"><i class="fa fa-info-circle"></i> '+json['success']+'<button type="button" class="close" data-dismiss="alert">×</button></div>');
   
            }
   
            if(json['error']){
   
                $('#change_plan .modal-footer .message').html('<div class="alert alert-danger alert-sm"><i class="fa fa-check-circle"></i> '+json['error']+'<button type="button" class="close" data-dismiss="alert">×</button></div>');
   
            }
   
          }
   
        });
   
    });
   
     
   
   }
   
   function changeDate(){
   
    $('#change_date').modal('show');
   
    $('#change_date .chosen').chosen({height: "120px",width: "100%"});
   
    $('.change_date-confirm').click(function(event) {
   
      event.preventDefault();
   
            $('.change_date-confirm').button('loading');
   
        $.ajax({
   
          url:'index.php?route=plan/progress/change_date',
   
          type: 'POST',
   
          dataType: 'json',
   
            data: $('#change_date input[type=\'text\'],#change_date input[type=\'hidden\'],#change_date input[type=\'radio\']:checked,#change_date input[type=\'checkbox\']:checked,#change_date select,#change_date textarea'),
   
          success: function(json) {
   
                    $('.change_date-confirm').button('reset');
   
            if(json['success']){
   
                $('#change_date').modal('hide');
   
                $('#main_message').html('<div class="alert alert-success alert-sm"><i class="fa fa-info-circle"></i> '+json['success']+'<button type="button" class="close" data-dismiss="alert">×</button></div>');
   
            }
          }
        });
    });
   }
</script>

<script type="text/javascript"><!--
  function approveDevice(user_id,status){
   
    $.ajax({
   
        type: 'POST',
   
        url: 'index.php?route=plan/progress/approveDevice',
   
        dataType: 'json',
   
        data: 'user_id=' + user_id+'&status=' + status,
   
        beforeSend: function(){
   
        },
   
        success: function(json) {
   
          if(json['success']){
   
            $('#ostaff'+user_id).remove();
   
            $('#nstaff'+user_id+' a').remove();
   
          }
   
            }
   
       });
   
   }

  $(document).ready(function(){
 
     <?php if(!empty($filter_global)){?>
 
     $("td span:contains(<?php echo $filter_global;?>)").css({"color":"red","background":"yellow"});
 
     <?php } ?>
 
   
 
     $('.input-daterange').datepicker({
 
       format: "yyyy-mm-dd",
 
     });
 
   });
</script>
<div id="progress-user" class="modal fade" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">
            </h4>
         </div>
         <div class="modal-body">
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-warning pull-right btn-sm" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script type="text/javascript">
   function modalPlans(user_id) {
   
   var href = 'index.php?route=plan/plan/getPlans&filter_user_id='+user_id;
   
   var filter_round_name = $('select[name=\'filter_round_name\']').val();
   
   <?php if($filter_round_name) { ?>
   
    href += '&filter_round_name=<?php echo $filter_round_name; ?>';
   
   <?php } ?>
   
   <?php if($filter_date) { ?>
   
    href += '&filter_date=<?php echo $filter_date; ?>';
   
   <?php } ?>
   
    href += '&sort=time_checkin';
   
   $.ajax({
   
    url: href,
   
    dataType: 'html',
   
    beforeSend: function(){},
   
    success: function(html) {
   
        $('#progress-user').find('.modal-body').html(html); 
   
        $('#progress-user').find('.modal-title').html($('#staff').html()+'<button type="button" class="btn btn-warning pull-right btn-sm" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>'); 
   
        $('#progress-user').find('.box-header').remove();
   
        $('#progress-user').find('.box').attr('class','row');
   
        $('#progress-user').modal('show');  
   
    }
   
   });
   
   }
   
</script>
