<style type="text/css">
  .thumbnail {
    position: relative;
    max-width: 300px;
  }
  .icon{
    width: 38px;
    height: 34px;
    position: absolute;
    display: none;
  }
  .thumbnail:hover>.icon{
    display: block;
  }
  .popover .tooltip+.btn,
  .popover .btn+.btn{margin-top:7px;}
  .popover-content {
    max-width: 50px;
  }
  .au {
    border: 1px solid #ddd;
    height: 40px;
    padding: 3px;
    max-width: 345px;
    margin-bottom: 5px;
  }
  .au a {
    color:red;
    float: right;
    line-height: 20px;
    background-color: #FAFAFA;
  }
  .popup {
    position: absolute;
    min-width: 150px;
    top: -40px;
    right: -27px;
  }
</style>
<!--IMAGE-->
<form method="post" action="index.php?route=plan/plan_image/save&plan_id=<?=$plan_id?>" id="plan_image">
  <div class="box box-info">
    <div class="box-header">
      <h3 class="box-title">Hình ảnh</h3>
    </div>
    <div class="box-body thumbnails plan_image">
      <div class="col-sm-6" id="img_0" style="display: none">
        <div class="thumbnail">
          <?php if($has_edit) { ?>
          <button type="button" class="btn btn-success icon btn-rotate" data-toggle="popover" title="Xoay" style="top: 4px;left: 4px"><i class="fa fa-refresh"></i></button> 
          <button type="button" class="btn btn-primary icon btn-avatar" title="Set avatar" style="top: 4px;right: 4px"><i class="fa fa-map-marker"></i></button>
          <?php } ?>
          <button type="button" class="btn btn-primary icon btn-manual" style="bottom: 40px;left: 4px; display: none;" title="Hình upload thủ công"><i class="fa fa-hand-paper-o"></i></button>
          <?php if($has_del) { ?>
          <button type="button" class="btn btn-danger icon btn-del-img" style="bottom: 40px;right: 4px" onclick=""><i class="fa fa-remove"></i></button>
          <?php } ?>
          <a href="" data-id="">
            <img src="" alt="">
          </a>
          <select class="form-control input-sm">
            <option value="">----</option>
            <?php foreach($image_types as $type) { ?>
            <option value="<?=$type['image_type_id'];?>"><?=$type['image_type']?></option>
            <?php } ?>
          </select>
        </div>
      </div>
    </div>
    <?php if($has_edit) { ?>
    <div class="box-footer clearfix">
      <div class="col-sm-12 message">
        
      </div>
      <div class="clearfix">
        <button type="button" class="btn btn-success btn-sm pull-left" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-upload"></i> Tải lên</button>
        <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Lưu</button>
      </div>
    </div>
    <?php }?>
  </div>
</form>
<div id="modal-upload" class="modal modal-box fade in">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
         </div>
         <div class="modal-body clearfix text-center">
            <form action="/file-upload" class="dropzone" id="my-dropzone">
              <h3 class="sbold">Drop files here or click to upload</h3>
              <p> This is just a demo dropzone. Selected files are not actually uploaded. </p>
              <div class="fallback">
                <input name="file" type="file" multiple />
              </div>
            </form>
         </div>
         <!-- //modal-body--> 
         <div class="modal-footer">
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
  var images = <?=json_encode($images)?>;
  if(images && images.length){   
    for (var i in images) {
      var img = $('#img_0').clone();
      img.attr('id', 'img_'+images[i].image_id);
      img.find('a').attr('href', images[i].popup);
      img.find('a').addClass('img');
      img.find('img').attr('src', images[i].thumb);
      if (parseInt(images[i].manual) == 1) {
        img.find('.btn-manual').css('display','block');
      }
      var select = img.find('select');
      select.attr('name', 'image_ids['+images[i].image_id+']');
      select.val(images[i].image_type_id);
      img.css('display','block');
      img.appendTo('.plan_image');
    }
  }

  $('.plan_image').on('click', '.btn-avatar', function() {
    var image_id = $(this).closest('.col-sm-6').attr('id').replace('img_','');
    $.get('index.php?route=plan/plan_image/set_avatar&plan_id=<?=$plan_id?>&image_id='+image_id, function(data){
      if (data) {
        $('#overview').find('a').attr('href',data.popup);
        $('#overview').find('img').attr('src',data.thumb);
      }
    },'JSON')
  })

  $('.plan_image').on('click', '.btn-del-img', function() {
    var img_id = $(this).closest('.col-sm-6').attr('id').replace('img_','');
    $.get("index.php?route=plan/plan_image/delete_image&plan_id=<?=$plan_id?>&image_id="+img_id, function(data) {
      $('#img_'+img_id).remove();
    })
  })

  Dropzone.options.myDropzone = {
    dictDefaultMessage: "Click to upload",
    url : 'index.php?route=plan/plan_image/upload&plan_id=<?=$plan_id?>',
    init: function() {
      this.on("success", function(file, response) {
        if (response) {
          response = JSON.parse(response);
          var type = response.type;
          if (type == "image") {
            var img = $('#img_0').clone();
            img.attr('id', 'img_'+response.image_id);
            img.find('a').attr('href', response.popup);
            img.find('a').addClass('img');
            img.find('img').attr('src', response.thumb);
            img.css('display','block');
            img.appendTo('.plan_image');
          }
        }
      })
    }
  };

  $('.plan_image').popover({
    html: true,
    placement: 'left',
    selector: '[data-toggle="popover"]',
    title: 'Xoay',
    trigger: 'focus',
    content: function() {
      var img_id = $(this).closest('.col-sm-6').attr('id').replace('img_','');
      var popover = '<a type="button" title="-90" data-toggle="tooltip" class="btn btn-primary btn-xs" onclick="rotate('+img_id+','+"-90"+')"><i class="fa fa-rotate-left"></i></a>';
      popover += '<a type="button" title="90" data-toggle="tooltip" class="btn btn-primary btn-xs" onclick="rotate('+img_id+','+"90"+')"><i class="fa fa-rotate-right"></i></a>';
      popover += '<a type="button" title="180" data-toggle="tooltip" class="btn btn-primary btn-xs" onclick="rotate('+img_id+','+"180"+')"><i class="fa fa-arrow-up"></i></a>';
      return popover;
    }
  });

  function rotate(img_id, angle) {
    $.get("index.php?route=plan/plan_image/rotate&plan_id=<?=$plan_id?>&image_id="+img_id+"&angle="+angle, function(data) {
      if (data) {
        $('#img_'+img_id).find('a').attr('href',data.popup);
        $('#img_'+img_id).find('img').attr('src',data.thumb);
        if (data.is_avatar == 1) {
          $('#overview').find('a').attr('href',data.popup);
          $('#overview').find('img').attr('src',data.thumb);
        }
        if ($.magnificPopup.instance.isOpen) {
          $('#img_'+img_id).find('a').trigger('click');
        }
      }
    }, "JSON")
  }

  $('#plan_audio').on('click', '.btn-del-audio', function() {
    var audio_id = $(this).closest('.au').attr('id').replace('audio_','');
    $.get("index.php?route=plan/plan_image/delete_audio&plan_id=<?=$plan_id?>&audio_id="+audio_id, function(data) {
      $('#audio_'+audio_id).remove();
    })
  })
</script>



