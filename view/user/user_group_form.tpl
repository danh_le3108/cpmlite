<style>
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 5px 8px;
    line-height: 1;
}
td label {
    margin-bottom: 0px;
}
</style>
  <div class="page-header">
  <div class="container-fluid">
    <div class="pull-right">
      <button type="submit" form="form-user-group" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
      <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_back; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a> 
    </div>
  </div>
</div>
<div class="container-fluid">
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-user-group" class="form-horizontal">
    <div class="box">
      <div class="box-header">
        <h2 class="box-title">
        <?php echo $text_general; ?></h3>
        <div class="pull-right box-tools">
        </div>
      </div>
      <div class="panel-body">
        <div class="form-group required">
          <label class="col-sm-2 control-label" for="input-name"><?php echo $text_name; ?></label>
          <div class="col-sm-10">
            <?php echo $group_name; ?>
           
          </div>
        </div>
      </div>
    </div>
    <div class="box">
      <div class="box-header">
        <h2 class="box-title">
        <?php echo $text_system_permission; ?></h3>
      </div>
      <div class="panel-body">
        <?php $x=0;
          foreach ($areas as $area) {?>
        <h3 class="text-center"><?php echo $area['label'];?></h3>
        <h5 class="text-center">
                      <a onclick="$(this).parents().find('input').prop('checked', true);"><?php echo $text_select_all; ?></a>
                       | <a onclick="$(this).parents().find('input').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                      </h5>
        <div class="table-responsive">
        <table class="table">
             <?php
                  $v= $a = $e= $d=0;
                   foreach ($area['folders'] as $folder) {?>
        <thead>
            <tr class="clearfix">
             <td class="text-right"><h3><?php echo $folder['label'];?></h3></td>
             <td style="vertical-align:bottom"><?php echo $text_access_per; ?></td>
             <td style="vertical-align:bottom"><?php echo $text_add_per; ?></td>
             <td style="vertical-align:bottom"><?php echo $text_edit_per; ?></td>
             <td style="vertical-align:bottom"><?php echo $text_delete_per; ?></td>
            </tr>
        </thead>
           
            <?php 
                  $v1= $a1 = $e1= $d1 =0;
                    foreach ($folder['permissions'] as $permission) { ?>
        <tbody>
                     <tr class="clearfix">
             <td class="text-right"><?php echo $permission['label']; ?></td>
             <td width="200" class="v<?php echo $x.'-'.$v;?>">
             <label for="v<?php echo $x.'-'.$v.'-'.$v1;?>">
             &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type="checkbox" name="permission[access][]" id="v<?php echo $x.'-'.$v.'-'.$v1;?>" value="<?php echo $permission['value']; ?>" <?php if (in_array($permission['value'], $access)) { ?>checked="checked"<?php } ?>/>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                    </label>
             </td>
             <td width="200" class="a<?php echo $x.'-'.$a;?>">
             <label for="a<?php echo $x.'-'.$a.'-'.$a1;?>">
             &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type="checkbox" name="permission[add][]" id="a<?php echo $x.'-'.$a.'-'.$a1;?>" value="<?php echo $permission['value']; ?>" <?php if (in_array($permission['value'], $add)) { ?>checked="checked"<?php } ?>/>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </label>
             
             </td>
             <td width="200" class="e<?php echo $x.'-'.$e;?>">
             <label for="e<?php echo $x.'-'.$e.'-'.$e1;?>">
              &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type="checkbox" name="permission[edit][]" id="e<?php echo $x.'-'.$e.'-'.$e1;?>" value="<?php echo $permission['value']; ?>" <?php if (in_array($permission['value'], $edit)) { ?>checked="checked"<?php } ?>/>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </label>
             </td>
             <td width="200" class="d<?php echo $x.'-'.$d;?>">
               <label for="d<?php echo $x.'-'.$d.'-'.$d1;?>">
              &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type="checkbox" name="permission[delete][]" id="d<?php echo $x.'-'.$d.'-'.$d1;?>" value="<?php echo $permission['value']; ?>" <?php if (in_array($permission['value'], $delete)) { ?>checked="checked"<?php } ?>/>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </label>
             </td>
            </tr>
                  <?php
                   $v1++; 
                   $a1++; 
                   $e1++; 
                   $d1++; 
                  }
                  ?>
                  
               
            
             <tr class="clearfix">
                 <td></td>
                 <td>
                      <a onclick="$(this).parents().find('.v<?php echo $x.'-'.$v;?> input').prop('checked', true);"><?php echo $text_select_all; ?></a>
                      / <a onclick="$(this).parents().find('.v<?php echo $x.'-'.$v;?> input').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                 </td>
                 <td>
                      <a onclick="$(this).parents().find('.a<?php echo $x.'-'.$a;?> input').prop('checked', true);"><?php echo $text_select_all; ?></a>
                      / <a onclick="$(this).parents().find('.a<?php echo $x.'-'.$a;?> input').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                 </td>
                 <td>
                 
                      <a onclick="$(this).parents().find('.e<?php echo $x.'-'.$e;?> input').prop('checked', true);"><?php echo $text_select_all; ?></a>
                      / <a onclick="$(this).parents().find('.e<?php echo $x.'-'.$e;?> input').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                 </td>
                 <td>
                      <a onclick="$(this).parents().find('.d<?php echo $x.'-'.$d;?> input').prop('checked', true);"><?php echo $text_select_all; ?></a>
                      / <a onclick="$(this).parents().find('.d<?php echo $x.'-'.$d;?> input').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                 </td>
            </tr>   
                <?php 
                $v++; 
                $a++; 
                $e++; 
                $d++; 
                } ?>
            
        </table>
    </div>
        <!-- table--> 
        <?php $x++;
          } ?>
      </div>
    </div>
    
    
 
  </form>
</div>
</div>
<!-- /.content -->

