
<div class="container-fluid">
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  
  <div class="box" id="filter-area">
    <div class="panel-body">
    <div class="row">
    <div class="col-sm-10">
      <div class="row">
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label" for="filter_coupon_code"><?php echo $text_search; ?></label>
            <input type="text" name="filter_coupon_code" value="<?php echo $filter_coupon_code; ?>" placeholder="<?php echo $text_search; ?>" id="filter_coupon_code" class="form-control" onchange="filter()"/>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label" for="filter_store_code"><?php echo $text_all_store_code; ?></label>
           
            <input type="text" name="filter_store_code" value="<?php echo $filter_store_code; ?>" placeholder="<?php echo $text_search; ?>" id="filter_store_code" class="form-control" onchange="filter()"/>
          </div>
           
          
        </div>
        
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label" for="filter_coupon_status"><?php echo $text_reward_type; ?></label>
            <select name="filter_reward_type_id" id="filter_reward_type_id" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_all; ?></option>
              <?php foreach ($reward_types as $type) { ?>
                  <?php if (in_array($type['reward_type_id'],$allow_filter)) { ?>
                  <option value="<?php echo $type['reward_type_id']; ?>" <?php if ($type['reward_type_id'] == $filter_reward_type_id) { ?>selected="selected"<?php } ?>><?php echo $type['reward_type_name']; ?></option>
                  <?php } ?>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label" for="filter_coupon_scan"><?php echo $text_coupon_scan; ?></label>
            <select name="filter_coupon_scan" id="filter_coupon_scan" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_all; ?></option>
              <?php foreach ($coupon_scans as $status) { ?>
              <option value="<?php echo $status['coupon_scan_id']; ?>" <?php if (!is_null($filter_coupon_scan)&&$status['coupon_scan_id'] == $filter_coupon_scan) { ?>selected="selected"<?php } ?>><?php echo $status['coupon_scan_name']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label" for="filter_yearmonth"><?php echo $text_reward_month; ?></label>
            <select name="filter_yearmonth" id="filter_yearmonth" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_all; ?></option>
              <?php foreach ($yearmonths as $yearmonth) { ?>
              <option value="<?php echo $yearmonth; ?>" <?php if ($yearmonth == $filter_yearmonth) { ?>selected="selected"<?php } ?>><?php echo $yearmonth; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label" for="filter_yearmonth"><?php echo $text_coupon_month; ?></label>
            <select name="filter_round" id="filter_round" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_all; ?></option>
              <?php foreach ($rounds as $round) { ?>
              <option value="<?php echo $round['round_name']; ?>" <?php if ($round['round_name'] == $filter_round) { ?>selected="selected"<?php } ?>><?php echo $round['round_name']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
      </div><!--//row 2--> 
      
      <div class="row">
      
        <div class="col-sm-4">
        
           <div class="form-group">
            <label class="control-label" for="filter_distributor_id"><?php echo $text_distributor; ?></label>
            <select name="filter_distributor_id" id="filter_distributor_id" 
            class="form-control chosen" onchange="filter()" <?php echo ($isLogged)?'disabled="disabled"':'';?> >
              <option value="*"><?php echo $text_all; ?></option>
              <?php foreach ($distributors as $distributor) { ?>
              <option value="<?php echo $distributor['distributor_id']; ?>" <?php if (!is_null($filter_distributor_id)&&$distributor['distributor_id'] == $filter_distributor_id) { ?>selected="selected"<?php } ?>><?php echo $distributor['distributor_code']; ?> - <?php echo $distributor['distributor_name']; ?></option>
              <?php } ?>
            </select>
          </div>
          
        </div><!--//col--> 
        
          <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label" for="filter_date_start"><?php echo $text_from_date; ?></label>
            <input type="text" class="form-control datepicker" name="filter_date_start" value="<?php echo $filter_date_start; ?>" onchange="filter()" placeholder="YYYY-MM-DD" />
            
          </div>
        </div>
        <!--col -->
        
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label" for="filter_date_end"><?php echo $text_to_date; ?></label>
            <input type="text" class="form-control datepicker" name="filter_date_end" value="<?php echo $filter_date_end; ?>" onchange="filter()" placeholder="YYYY-MM-DD" />
             
          </div>
        </div>
        <!--col -->
        
        <div class="col-sm-4">
        <?php
        ?>
            <?php if($has_edit && isset($report)) { ?>
            <a style="margin-top:20px" <?php if($paid_total>0){?>href="<?php echo $report;?>"<?php } ?> class="btn btn-success"><i class="fa fa-download"></i> <?php echo $text_download_report; ?> 
            <span class="badge label-danger"><?php echo $paid_total;?></span> </a> 
           
              <?php } ?>
            
        </div>
        <!--col -->
        
        
      </div><!--//row--> 
      
      
      </div><!--//col 1--> 
        <div class="col-sm-2">
      
          <div class="form-group">
            <label class="control-label">&nbsp;</label>
          <button type="button" id="button-clear" class="btn btn-primary pull-right"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>
        </div>
      </div><!--//col 2--> 
      </div><!--//row 2--> 
      
    </div>
    <!--panel-body -->
  </div>
  <!--box -->
  
   <div class="box" id="import-area">
    <div class="panel-body">
      <div class="clearfix">
        
          <div class="col-sm-10">
           <div class="row">
        <div class="col-sm-6">
        
           <label class=" control-label">Cách 1: Import số lượng lớn </label>
           <div class="well well-sm" style="margin-bottom:0 !important;">
         <form action="<?php echo $import_check;?>" id="import_check" enctype="multipart/form-data" method="post">
         
           <div class="row">
           
        <div class="col-sm-4">
            <a href="<?php echo $template_url;?>" class="btn btn-primary"><i class="fa fa-download"></i> Tải về mẫu excel</a>
        </div> <!-- //col--> 
        <div class="col-sm-8">
         <div class="input-group">
              <input type="file" id="file_import" name="file_import" class="form-control" style="min-width:200px;">
              <div class="input-group-btn">
                <button class="btn btn-primary" form="import_check" type="submit" data-loading-text="Importing..." id="button-import"><?php echo $text_import; ?></button>
              </div>
        </div> <!-- //group--> 
        </div> <!-- //col--> 
        </div> <!-- //row--> 
         </form>
        </div> <!-- //well--> 
        </div> <!-- //col--> 
        
        <div class="col-sm-6">
        <form action="<?php echo $check_form;?>" id="form-check" enctype="multipart/form-data" method="post">
           <label class=" control-label">Cách 2: Kiểm tra riêng lẻ từng Coupon</label>
           <div class="well well-sm" style="margin-bottom:0 !important;">
           <div class="row">
        <div class="col-sm-4">
          
          <div class="form-group">
            <label class="control-label" for="coupon_code"><?php echo $text_coupon_code; ?></label>
            <input type="text" name="coupon_code" value="<?php echo $coupon_code;?>" placeholder="<?php echo $text_code; ?>" id="coupon_code" class="form-control"/>
           <?php if ($error_coupon_code) { ?>
              <div class="text-danger"><?php echo $error_coupon_code; ?></div>
              <?php } ?>
          </div>
        </div> <!-- //--> 
        
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="store_code"><?php echo $text_all_store_code; ?></label>
            <input type="text" name="store_code" value="<?php echo $store_code;?>" placeholder="Store code" id="store_code" class="form-control"/>
           <?php if ($error_store_code) { ?>
              <div class="text-danger"><?php echo $error_store_code; ?></div>
              <?php } ?>
          </div>
        </div> <!-- //--> 
         
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="store_code">&nbsp;</label>  
          <div class="clearfix">
        <button type="submit" form="form-check" data-toggle="tooltip" title="Kiểm tra" class="btn btn-primary"><i class="fa fa-save"></i> Kiểm tra</button>
        </div>
          </div>
        </div> <!-- //col--> 
          </div> <!-- //row--> 
          
          </div> <!-- //well--> 
        </form>
        </div> <!-- //col--> 
          </div> <!-- //row--> 
          </div> <!-- //col--> 
        <div class="col-sm-2">
            <!--//--> 
            <a href="<?php echo $export;?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> 
            <?php echo $text_download_results; ?>
            <span class="badge  label-danger"><?php echo $total;?></span> 
            </a>
          

          </div>
         
      </div>
    </div>
  </div>
</div>
<!-- /.box -->


</div>
<!-- /.container -->


<div class="container-fluid">
  <div class="box">
    <div class="box-header">
      <h2 class="box-title">
      <?php echo $heading_title; ?></h3>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $results; ?></div>
        <div class="col-sm-6 text-right">
          <nav><?php echo $pagination; ?></nav>
        </div>
      </div>
        <div class="table-responsive">
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <th class="text-left"><?php if ($sort == 'coupon_code') { ?>
                  <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $text_coupon_code; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_name; ?>"><?php echo $text_coupon_code; ?></a>
                  <?php } ?>
                </th>
                <th class="text-left"><?php echo $text_coupon_value;?></th>
                <th class="text-left"><?php echo $text_planogram;?></th>
                <th class="text-left"><?php echo $text_store;?></th>
                <th class="text-left"><?php echo $text_reward_month; ?></th>
                <th class="text-left"><?php echo $text_coupon_month; ?></th>
                <th class="text-left"><?php echo $text_distributor_check; ?></th>
                <th class="text-right">Day Remaining</th>
              </tr>
            </thead>
            <tbody>
              <?php if ($coupons) { ?>
              <?php foreach ($coupons as $coupon) { ?>
              <tr>
                <td class="text-left"> <?php echo ($coupon['dist_check']==0)?'':$coupon['coupon_code']; ?> </td>
                <td class="text-left"><?php echo $coupon['coupon_value']; ?></td>
                <td class="text-left"><?php echo $coupon['coupon_prefix']; ?> -  <?php echo $coupon['prefix_title']; ?><br/> 
                
                <?php echo $coupon['reward_type']; ?></td>
                <td class="text-left"><?php echo $coupon['store_name']; ?><br/> 
                <small>Code CPM: <?php echo $coupon['store_code']; ?><br/> 
                Code DEM: <?php echo $coupon['store_customer_code']; ?><br/> 
                Code AD: <?php echo $coupon['store_ad_code']; ?><br/> 
                <!-- <?php echo $coupon['store_address_raw']; ?><br/>//--> 
                
               NPP: <?php echo $coupon['distributor_code']; ?><!-- <?php echo $coupon['distributor_name']; ?>//--> 
               </small>
                </td>
                <td class="text-left"><?php echo $coupon['yearmonth']; ?></td>
                <td class="text-left">
                <?php echo $coupon['date_upload']; ?> - <?php echo $coupon['coupon_status']; ?><br/> 
                <?php echo ($coupon['dist_check']>1)?'<span class="text-red">Trùng mã</span>':''; ?>
                </td>
                
                <td class="text-left"><?php echo $coupon['dist_check_date']; ?><br/><?php echo $coupon['coupon_scan_name']; ?></td>
                <td class="text-right"><?php echo $coupon['time_remaining']; ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $results; ?></div>
        <div class="col-sm-6 text-right">
          <nav><?php echo $pagination; ?></nav>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript"><!--
  function filter() {
    url = 'index.php?route=distributor/coupon_history';
  
    var filter_coupon_code = $('input[name=\'filter_coupon_code\']').val();
  
    if (filter_coupon_code) {
      url += '&filter_coupon_code=' + encodeURIComponent(filter_coupon_code);
    }
	
    var filter_store_code = $('input[name=\'filter_store_code\']').val();
  
    if (filter_store_code) {
      url += '&filter_store_code=' + encodeURIComponent(filter_store_code);
    }


    var filter_coupon_scan = $('select[name=\'filter_coupon_scan\']').val();
  
    if (filter_coupon_scan != '*') {
      url += '&filter_coupon_scan=' + encodeURIComponent(filter_coupon_scan);
    }

    var filter_reward_type_id = $('select[name=\'filter_reward_type_id\']').val();
  
    if (filter_reward_type_id != '*') {
      url += '&filter_reward_type_id=' + encodeURIComponent(filter_reward_type_id);
    }
    
    var filter_yearmonth = $('select[name=\'filter_yearmonth\']').val();
  
    if (filter_yearmonth != '*') {
      url += '&filter_yearmonth=' + encodeURIComponent(filter_yearmonth);
    }
	
    var filter_round = $('select[name=\'filter_round\']').val();
  
    if (filter_round != '*') {
      url += '&filter_round=' + encodeURIComponent(filter_round);
    }
	

    var filter_distributor_id = $('select[name=\'filter_distributor_id\']').val();
  
    if (filter_distributor_id != '*') {
      url += '&filter_distributor_id=' + encodeURIComponent(filter_distributor_id);
    }
	
	  var filter_date_start = $('input[name=\'filter_date_start\']').val();
	
	  if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	  }
		var filter_date_end = $('input[name=\'filter_date_end\']').val();
	
	  if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	  }



    location = url;
  }
  //-->
</script>

