<?php
class ModelHistoryHistory extends Model {
	public function add($data = array()) {
		if (!empty($data)) {
			$sql = "INSERT INTO " . DB_PREFIX . "log_history SET ";
			foreach ($data as $key => $value) {
				$sql .= "{$key} = '" . $this->pdb->escape($value) . "',";
			}
			$sql .= "date_added = NOW()";
			$this->pdb->query($sql);
		}
	}
}