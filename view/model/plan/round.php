<?php
class ModelPlanRound extends Model {
	
	public static $rounds_data = array();
	public function getRounds() {
		if (!empty(self::$rounds_data)) {
			return self::$rounds_data;
		}
		$rounds_arr = array();
		$curr_round = date('Y-m');
		
		$rounds_query = array();
			$query = $this->pdb->query("SELECT `round_name` FROM " . PDB_PREFIX . "plan p GROUP BY `round_name` ORDER BY `round_name` DESC");
			foreach ($query->rows as $row){
				$round_name = $row['round_name'];
				$rounds_arr[$round_name] = $round_name;
				$rounds_query[] = array(
					'round_name'=>$round_name,
					'round_description'=>$round_name
				);
			}
			if(!isset($rounds_arr[$curr_round])){
				$first_arr = array(
					'round_name'=>$curr_round,
					'round_description'=>$curr_round
				);
				array_unshift($rounds_query,$first_arr);
			}
		return self::$rounds_data = $rounds_query;
		
	}

	public function getCounts() {
		$counts_data = array();
			$query = $this->pdb->query("SELECT `upload_count` FROM " . PDB_PREFIX . "plan p GROUP BY `upload_count` ORDER BY `upload_count` ASC");
			foreach ($query->rows as $row){
				$counts_data[] = $row['upload_count'];
			}
		return $counts_data;
		
	}
	
	public function getVersions() {
		$versions_data = array();
			$query = $this->pdb->query("SELECT `version` FROM " . DB_PREFIX . "plan p GROUP BY `version` ORDER BY `version` DESC");
			foreach ($query->rows as $row){
				if($row['version']!=''){
					$versions_data[] = $row['version'];
				}
			}
		return $versions_data;
		
	}

	public function getRound() {
		$sql = "SELECT * FROM " . DB_PREFIX . "round";
		return $this->pdb->query($sql)->row;
	}
}