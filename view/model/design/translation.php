<?php
class ModelDesignTranslation extends Model {
	public function getTranslations($route) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "translation WHERE project_id = '" . (int)$this->config->get('config_project_id') . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "' AND route = '" . $this->db->escape($route) . "'");

		return $query->rows;
	}
}