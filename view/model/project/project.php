<?php
class ModelProjectProject extends Model {
	public function getProjects($data = array()) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "project ORDER BY url");

			$store_data = $query->rows;

		return $store_data;
	}
	public function getProjectsByUserId($user_id){
		$query = $this->db->query("SELECT *, (SELECT ug.group_name FROM `" . DB_PREFIX . "user_group` ug WHERE ug.user_group_id = pu.project_user_group) AS group_name FROM `".DB_PREFIX."project_user` pu LEFT JOIN `".DB_PREFIX."project` p ON(pu.project_id=p.project_id) WHERE user_id = '" .(int)$user_id."'");
		return $query->rows;
	}
	public function getProject($project_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "project WHERE project_id = '" . (int)$project_id . "'");

		return $query->row;
	}
	
	public function editProject($data) {
		$project_id = $this->config->get('config_project_id');
		
		$sql = "UPDATE " . DB_PREFIX . "project SET 
		config_logo = '" . $this->db->escape($data['config_logo']) . "', 
		config_db_hostname = '" . $this->db->escape($data['config_db_hostname']) . "',
		project_db = '" . $this->db->escape($data['project_db']) . "', ";
		
		if(isset($data['project_db_user'])&&isset($data['project_db_pass'])){
			$sql .= "project_db_user = '" . $this->db->escape($data['project_db_user']) . "', 
			project_db_pass = '" . $this->db->escape($data['project_db_pass']) . "', ";
		}
		
		$sql .= "project_source = '" . $this->db->escape($data['project_source']) . "', 
		`url` = '" . $this->db->escape($data['config_url']) . "', 
		`url_ssl` = '" . $this->db->escape($data['config_ssl']) . "',
		date_updated = NOW() WHERE project_id = '" . (int)$project_id . "'";
		
		$this->db->query($sql);
		/*Add PL User*/ 
		$this->copyLayout($project_id);
		$this->copyPermission($project_id);
	
	}
	public function copyPermission($project_id) {
		$project_group_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "project_group_permission WHERE project_id = '".$project_id."'");
		if (!$project_group_query->num_rows) {
			// Layout Route
			$permission_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user_group WHERE 1");
			foreach ($permission_query->rows as $row) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "project_group_permission SET project_id = '" . (int)$project_id. "', user_group_id = '" . (int)$row['user_group_id'] . "', permission = '" . $this->db->escape($row['permission']) . "'");
				
			}	
		}
	}
	public function copyLayout($project_id) {
		$project_layout_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "layout WHERE project_id = '".$project_id."'");
		if (!$project_layout_query->num_rows) {
			// Layout Route
			$layout_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "layout WHERE project_id = '0'");
			foreach ($layout_query->rows as $layout) {
				$route_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "layout_route WHERE layout_id = '" . (int)$layout['layout_id'] . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "layout SET 
				layout_name = '" . $this->db->escape($project_id.' - '.$layout['layout_name']) . "',
				project_id = '" . (int)$project_id. "',
				layout_builder = '" . $this->db->escape($layout['layout_builder']) . "'");
				$layout_id = $this->db->getLastId();
				foreach ($route_query->rows as $route) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "layout_route SET 
					layout_id = '" . (int)$layout_id . "',  
					project_id = '" . (int)$project_id . "',
					route = '" . $this->db->escape($route['route']) . "',
					layout_builder = '" . $this->db->escape($layout['layout_builder']) . "'");
				}
			}	
		}
	}
}