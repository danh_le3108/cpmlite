<?php
class ModelProjectCustomer extends Model {

	public function getCustomersIndexBy($index_by = 'username') {
		$customer_data = array();
		$sql = "SELECT DISTINCT cu.*,cl.level_name,cl.child_level_id, 
			(SELECT up.fullname FROM `" . DB_PREFIX . "customer_user` up WHERE up.customer_user_id = cu.customer_parent_id LIMIT 0,1) AS parent_fullname 
			FROM " . DB_PREFIX . "customer_user cu 
			LEFT JOIN " . DB_PREFIX . "customer_level cl ON (cl.customer_level_id = cu.customer_level_id) WHERE 1";
 
		$query = $this->pdb->query($sql);

		foreach($query->rows as $row){
			$customer_data[$row[$index_by]] = $row;
		}		
		return $customer_data;
	}
	public function updateStore() {
		$sql = "SELECT * FROM " . PDB_PREFIX . "customer_user c WHERE 1";
		$query = $this->pdb->query($sql);

		foreach ($query->rows as $row){
			$sql2 ="UPDATE " . PDB_PREFIX . "store SET customer_user = '" . $this->db->escape(trim($row['fullname'])) . "', customer_username = '" . $this->db->escape(trim($row['username'])) . "' WHERE customer_user_id = '" . (int)$row['customer_user_id']. "'";
	$this->pdb->query($sql2);
		}
		
		foreach ($query->rows as $row){
			$sql1= "UPDATE " . PDB_PREFIX . "store SET customer_user_id = '" . (int)$row['customer_user_id'] . "', customer_username = '" . $this->db->escape(trim($row['username'])) . "' WHERE customer_user = '" . $this->db->escape(trim($row['fullname'])). "' OR customer_user LIKE '%" . $this->db->escape(trim($row['fullname'])). "'";
	$this->pdb->query($sql1);
		}
		
		$this->pdb->query("UPDATE `" . PDB_PREFIX . "plan` a INNER JOIN `" . PDB_PREFIX . "store` b ON a.`store_id` = b.`store_id` SET a.`customer_user_id`= b.`customer_user_id`;");
		
		return true;
	}
	public function addCustomer($data) {
		$user_register = isset($data['user_register'])?$data['user_register']:$this->user->getUserName();
		
		
		$this->pdb->query("INSERT INTO " . PDB_PREFIX . "customer_user SET 
		username = '" . $this->db->escape($data['username']) . "', 
		fullname = '" . $this->db->escape($data['fullname']) . "', 
		email = '" . $this->db->escape($data['email']) . "', 
		telephone = '" . $this->db->escape($data['telephone']) . "', 
		status = '1', 
		customer_level_id = '" . (int)$data['customer_level_id'] . "', 
		user_register = '" . $this->db->escape($user_register) . "', 
		safe = '0', 
		date_added = NOW()");
		
		$customer_user_id = $this->pdb->getLastId();
		
		
		if (isset($data['password'])&&!empty($data['password'])) {
			$this->pdb->query("UPDATE " . PDB_PREFIX . "customer_user SET 
			salt = '" . $this->db->escape($salt = token(9)) . "', 
			password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "' , 
			real_token = '" . $this->db->escape(base64_encode(base64_encode($data['password']))) . "' 
			WHERE customer_user_id = '" . (int)$customer_user_id . "'");
		}
		
		$data['customer_user_id'] = $customer_user_id;
		
		$this->load->model('project/customer');
		$this->model_project_customer->activeCustomerProject($data);
		$this->updateStore();
		return $customer_user_id;
	}

	public function editCustomer($customer_user_id, $data) {
		$sql ="UPDATE " . PDB_PREFIX . "customer_user SET
		username = '" . $this->db->escape($data['username']) . "',
		fullname = '" . $this->db->escape($data['fullname']) . "',
		email = '" . $this->db->escape($data['email']) . "',";
		
		if (isset($data['customer_level_id'])&&!empty($data['customer_level_id'])) {
			$sql .="customer_level_id = '" . (int)$data['customer_level_id'] . "',";
		}
		
		$sql .="telephone = '" . $this->db->escape($data['telephone']) . "' WHERE customer_user_id = '" . (int)$customer_user_id . "'";
		 
		$this->pdb->query($sql);
		if (isset($data['password'])&&!empty($data['password'])) {
			$this->pdb->query("UPDATE " . PDB_PREFIX . "customer_user SET 
			salt = '" . $this->db->escape($salt = token(9)) . "', 
			password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "' , 
			real_token = '" . $this->db->escape(base64_encode(base64_encode($data['password']))) . "' 
			WHERE customer_user_id = '" . (int)$customer_user_id . "'");
		}

		$this->updateStore();
	}

	public function deleteCustomer($customer_user_id) {
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "customer_user WHERE customer_user_id = '" . (int)$customer_user_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "history_activity WHERE customer_user_id = '" . (int)$customer_user_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "history_ip WHERE customer_user_id = '" . (int)$customer_user_id . "' AND project_id = '" . (int)$this->config->get('config_project_id'). "'");
	}

	public function getCustomer($customer_user_id) {
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "customer_user WHERE customer_user_id = '" . (int)$customer_user_id . "'");

		return $query->row;
	}
	public function getCustomerByFullName($fullname) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "customer_user WHERE `fullname` = '" . $this->db->escape($fullname) . "'");

		return $query->row;
	}
	public function getCustomerByUsername($username) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "customer_user c WHERE c.`username` = '" . $this->db->escape($username) . "'");

		return $query->row;
	}
	public function getCustomerByEmail($email) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "customer_user c WHERE LCASE(c.email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

	public function getCustomers($data = array()) {
		$sql = "SELECT c.*, c.fullname AS name FROM " . PDB_PREFIX . "customer_user c WHERE 1";

		$filter_global = array(
			'c.username',
			'c.fullname',
			'c.email',
			'c.telephone',
			'c.address'
		);

		if (!empty($data['filter_global'])) {
			$sql .= " AND (";
			foreach ($filter_global as $filter) {
				$fimplode[] = " LCASE(".$filter.") LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_global'])) . "%'";
			}
			$sql .= " " . implode(" OR ", $fimplode) . "";
			$sql .= ")";
		}


		$implode = array();

		if (!empty($data['filter_username'])) {
			$implode[] = "c.username LIKE '%" . $this->db->escape($data['filter_username']) . "%'";
		}
		if (!empty($data['filter_fullname'])) {
			$implode[] = "c.fullname LIKE '%" . $this->db->escape($data['filter_fullname']) . "%'";
		}

		if (!empty($data['filter_email'])) {
			$implode[] = "c.email LIKE '" . $this->db->escape($data['filter_email']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$implode[] = "c.status = '" . (int)$data['filter_status'] . "'";
		}

		if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
			$implode[] = "c.approved = '" . (int)$data['filter_approved'] . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$implode[] = "DATE(c.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		$sort_data = array(
			'c.username',
			'c.fullname',
			'c.email',
			'customer_level',
			'c.status',
			'c.approved',
			'c.ip',
			'c.date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->pdb->query($sql);

		return $query->rows;
	}

	public function getTotalCustomers($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "customer_user c WHERE 1";

		$filter_global = array(
			'c.username',
			'c.fullname',
			'c.email',
			'c.telephone',
			'c.address'
		);

		if (!empty($data['filter_global'])) {
			$sql .= " AND (";
			foreach ($filter_global as $filter) {
				$fimplode[] = " LCASE(".$filter.") LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_global'])) . "%'";
			}
			$sql .= " " . implode(" OR ", $fimplode) . "";
			$sql .= ")";
		}

		$implode = array();

		if (!empty($data['filter_username'])) {
			$implode[] = "c.username LIKE '%" . $this->db->escape($data['filter_username']) . "%'";
		}
		if (!empty($data['filter_fullname'])) {
			$implode[] = "c.fullname LIKE '%" . $this->db->escape($data['filter_fullname']) . "%'";
		}

		if (!empty($data['filter_email'])) {
			$implode[] = "c.email LIKE '" . $this->db->escape($data['filter_email']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$implode[] = "c.status = '" . (int)$data['filter_status'] . "'";
		}

		if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
			$implode[] = "c.approved = '" . (int)$data['filter_approved'] . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$implode[] = "DATE(c.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		$query = $this->pdb->query($sql);

		return $query->row['total'];
	}
	
	public function updateCustomer($customer_user_id,$data) {
		
		$sql = "UPDATE " . PDB_PREFIX . "customer_user SET";
		if(isset($data['customer_parent_id'])){
			$sql .= " customer_parent_id = '" . (int)$data['customer_parent_id'] . "',";
		}
		if(isset($data['customer_level_id'])){
			$sql .= " customer_level_id = '" . (int)$data['customer_level_id'] . "',";
		}
		$sql .= " status = '1', user_update = '" . $this->user->getUserName() . "' WHERE customer_user_id = '" . (int)$customer_user_id . "'";
				$this->pdb->query($sql);
	}
	//Y
	public function removeCustomer($customer_user_id) {
		$sql = "UPDATE " . PDB_PREFIX . "customer_user SET status = '0', user_update = '" . $this->user->getUserName() . "' WHERE customer_user_id = '" . (int)$customer_user_id . "'";
		$this->pdb->query($sql);
	}
 	//Y
	public function getProjectCustomers($data=array()) {
		$user_data = array();
		$sql = "SELECT DISTINCT u.*, 
		(SELECT up.fullname FROM `" . PDB_PREFIX . "customer_user` up WHERE up.customer_user_id = u.customer_parent_id LIMIT 0,1) AS parent_fullname, 
		(SELECT ug.level_name FROM `" . PDB_PREFIX . "customer_level` ug WHERE ug.customer_level_id = u.customer_level_id) AS level_name 
		FROM " . PDB_PREFIX . "customer_user u WHERE 1";
		if (isset($data['customers_manager'])&& !empty($data['customers_manager'])) {
			$sql .= " AND u.`customer_user_id` IN('".implode("','",$data['customers_manager'])."')";
		}
		$query = $this->pdb->query($sql);
		// $count = $this->getTotalPlansGroupByCustomer(); 
		foreach($query->rows as $k => $u){
			$user_data[$u['customer_user_id']] = $u;
			// $user_data[$u['customer_user_id']]['plan_total'] = isset($count[$u['customer_user_id']])?$count[$u['customer_user_id']]:0;
		}		
		return $user_data;
	}
	//Y
	public function getTotalPlansGroupByCustomer() {
			$sql = "SELECT pl.customer_user_id, COUNT(*) AS plan_total FROM " . PDB_PREFIX . "plan pl WHERE 1 GROUP BY pl.customer_user_id";
			$query = $this->pdb->query($sql);
			$plan_total = array();
		foreach($query->rows as $row){
			$plan_total[$row['customer_user_id']] = $row['plan_total'];
		}
		return $plan_total;
	}

	//Y
	public function activeCustomerProject($data = array()){
			$sql = "UPDATE `" . PDB_PREFIX . "customer_user` SET 
				`customer_level_id` = '" . (int)$data['customer_level_id']."',
				`user_update` = '" . $this->user->getUserName() . "',
				`date_start` = NOW() WHERE customer_user_id = '" . (int)$data['customer_user_id'] . "'";
				
				$this->pdb->query($sql);
	}

	public function getCustomersByGroup($group_id) {
		$sql = "SELECT * FROM " . PDB_PREFIX . 'customer_user WHERE customer_level_id = ' . (int)$group_id;
		if ($this->customer->isLogged()) {
			$sql .= ' AND (customer_user_id IN (' . implode(',', $this->customer->users_manager()) . ') OR customer_user_id = '. $this->customer->getId() .')';
		}
		return $this->pdb->query($sql)->rows;
	}
}

