<?php
class ModelProjectCustomerLevel extends Model {
	public function addUserGroup($data) {
		$this->pdb->query("INSERT INTO " . PDB_PREFIX . "customer_level SET 
		level_name = '" . $this->db->escape($data['level_name']) . "', 
		sort_order = '" . (int)$data['sort_order'] . "',  
		child_level_id = '" . (int)$data['child_level_id'] . "',  
		level_description = '" . $this->db->escape($data['level_description']) . "'");
	
		return $this->db->getLastId();
	}

	public function editUserGroup($customer_level_id, $data) {
		$this->pdb->query("UPDATE " . PDB_PREFIX . "customer_level SET 
		level_name = '" . $this->db->escape($data['level_name']) . "',
		sort_order = '" . (int)$data['sort_order'] . "', 
		child_level_id = '" . (int)$data['child_level_id'] . "',  
		level_description = '" . $this->db->escape($data['level_description']) . "' WHERE customer_level_id = '" . (int)$customer_level_id . "'");
	}

	public function deleteUserGroup($customer_level_id) {
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "customer_level WHERE customer_level_id = '" . (int)$customer_level_id . "'");
	}

	public function getCustomerLevel($customer_level_id) {
		$customer_level = array();
		$query = $this->pdb->query("SELECT DISTINCT *, (SELECT c.level_name FROM `" . PDB_PREFIX . "customer_level` c WHERE c.customer_level_id = l.child_level_id) AS child_level FROM " . PDB_PREFIX . "customer_level l WHERE l.customer_level_id = '" . (int)$customer_level_id . "'");
		if ($query->num_rows) {
			$customer_level = array(
				'customer_level_id'       => $query->row['customer_level_id'],
				'child_level_id'       => $query->row['child_level_id'],
				'child_level'       => $query->row['child_level'],
				'level_name'       => $query->row['level_name'],
				'sort_order'       => $query->row['sort_order'],
				'level_description'       => $query->row['level_description']
			);
		}

		return $customer_level;
	}

	public function getCustomerLevels($data = array(),$index_by=NULL) {
		$sql = "SELECT *, (SELECT c.level_name FROM `" . PDB_PREFIX . "customer_level` c WHERE c.customer_level_id = l.child_level_id) AS child_level FROM " . PDB_PREFIX . "customer_level l WHERE 1";

		$sql .= " ORDER BY sort_order";

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->pdb->query($sql);
		$return = array();
		if($index_by!=NULL){
			foreach($query->rows as $row){
				$return[$row[$index_by]]= $row;
			}
		}else{
			$return = $query->rows;
		}
		
		return $return;
	}

	public function getTotalCustomerLevel() {
		$query = $this->pdb->query("SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "customer_level WHERE 1");

		return $query->row['total'];
	}
}