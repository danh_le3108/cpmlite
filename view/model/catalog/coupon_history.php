<?php

class ModelCatalogCouponHistory extends Model {
	/*Create Coupon Beta*/ 
	public function generateCoupon($prefix_code){
		$sql = "SELECT COUNT(*) AS total FROM `".PDB_PREFIX."plan_coupon_history` WHERE `coupon_beta` LIKE '" . $this->pdb->escape($prefix_code) ."%'";
		
		$query = $this->pdb->query($sql);
		
		$total = ((int)$query->row['total']>0)?$query->row['total']:1;
		
		return $this->createCoupon($prefix_code, $total);
		
	}
	public function createCoupon($prefix_code, $total){
		$coupon_beta = $prefix_code.sprintf('%07d', $total);
		$sql = "SELECT COUNT(*) AS total FROM `".PDB_PREFIX."plan_coupon_history` WHERE `coupon_beta` = '" . $this->pdb->escape($coupon_beta) ."'";
		
		$query = $this->pdb->query($sql);

		if((int)$query->row['total']>0){
			$total++;
			return $this->createCoupon($prefix_code, $total);
		}

		return $coupon_beta;
	}
	/*End */ 

	public function time_remaining($date_start) {
		
		$config_day_valid = $this->config->get('config_day_valid');
		
		$date_end =  date('Y-m-d H:i:s');
		$return = '';
		
		$time_start = new DateTime($date_start);
		$since_start = $time_start->diff(new DateTime($date_end));
		$days_total = $since_start->days.' days total ';
		$years = $since_start->y;
		$months = $since_start->m;
		$days =  $since_start->d;
		$time_remaining = $config_day_valid-$days;
		return ($time_remaining>=0&&$time_remaining<=$config_day_valid)?$time_remaining:'';
	}
  
	  public function mappingCouponsByStoreID($coupons){
		$query_data = array();
		if(!empty($coupons)){
			foreach($coupons as $row){
				if(!empty($row['coupon_prefix'])){
					$yearmonth = (string)datetimeConvert($row['yearmonth'], 'm');
					$query_data[$row['store_id']]['date_upload'] = $row['date_upload'];
					$query_data[$row['store_id']]['distributor_id'] = $row['distributor_id'];
					$query_data[$row['store_id']]['coupons'][$yearmonth]['time_remaining'] = $this->time_remaining($row['date_upload']);
					$query_data[$row['store_id']]['coupons'][$yearmonth]['dist_check_id'] = $row['dist_check_id'];
					$query_data[$row['store_id']]['coupons'][$yearmonth]['coupon_prefix'] = $row['coupon_prefix'];
					$query_data[$row['store_id']]['coupons'][$yearmonth]['coupon_code'] = $row['coupon_code'];
				}
			}
		}
	
		return $query_data;
	  }
	  
	public function updateDistributorCheck($coupon){
		$config_day_valid = $this->config->get('config_day_valid');
		
		$sql2 = "UPDATE `".PDB_PREFIX."plan_coupon_history` SET `dist_check` = `dist_check`+1, 
		`dist_check_id`='" . (int)$this->config->get('config_dist_valid') . "', 
		`time_diff`= TIMESTAMPDIFF(DAY,DATE(`date_upload`),DATE(NOW())) WHERE (TIMESTAMPDIFF(DAY,DATE(`date_upload`),DATE(NOW()))<=".$config_day_valid.") AND `coupon_history_id`='" . (int)$coupon['coupon_history_id'] . "'";
		
		$query2 = $this->pdb->query($sql2);
		
		
		$sql1 = "UPDATE `".PDB_PREFIX."plan_coupon_history` SET `dist_check` = `dist_check`+1, `dist_check_date` = NOW()";
		
		if(isset($coupon['dist_import_id'])){
			$sql1 .= ",`dist_import_id`='" . (int)$coupon['dist_import_id'] . "'";
		}
			$sql1 .= " WHERE `coupon_history_id`='" . (int)$coupon['coupon_history_id'] . "' AND `dist_check_date` = '0000-00-00'";//
		
		$query = $this->pdb->query($sql1);
			return 	$query;				
	}
	
	public function getCouponsHistoryByCode($coupon_code,$data = array()) {
		$sql = "SELECT DISTINCT c.*,pl.`plan_status`,pl.`plan_qc`,s.`store_code`,s.`store_ad_code`,s.`store_customer_code`";
		
		$sql .= " FROM " . PDB_PREFIX . "plan_coupon_history c LEFT JOIN " . PDB_PREFIX . "plan pl ON (pl.plan_id = c.plan_id)";
		$sql .= " LEFT JOIN " . PDB_PREFIX . "store s ON (s.store_id = c.store_id)";
		
		$sql .= " WHERE c.plan_id >0";
		
		$sql .= " AND c.coupon_code = '" . $this->pdb->escape($coupon_code) . "'";
		
		//Nếu không phải user CPM dăng nhập
		if (!$this->user->isLogged()&&($this->distributor->isLogged()||$this->customer->isLogged())) {
			$sql .= " AND pl.plan_status=1 AND pl.plan_qc=1";
		}
		$implode = array();
		
		if (isset($data['filter_distributor_id'])&&!empty($data['filter_distributor_id'])) {
			$implode[] = "c.distributor_id = '" . $this->db->escape($data['filter_distributor_id']) . "'";
		}
		$query = $this->pdb->query($sql);

		return $query->row;
	}
	  
	public function getCouponsHistory($data = array()){
		$sql = "SELECT c.* FROM `".PDB_PREFIX."plan_coupon_history` c LEFT JOIN " . PDB_PREFIX . "plan pl ON (pl.plan_id = c.plan_id) WHERE c.plan_id >0 AND c.coupon_code!=''";
		//Nếu không phải user CPM dăng nhập
		if (!$this->user->isLogged()&&($this->distributor->isLogged()||$this->customer->isLogged())) {
			$sql .= " AND pl.plan_status=1 AND pl.plan_qc=1";
		}
		/*Filter start*/ 
		$implode = array();
		if (isset($data['filter_coupon_code'])&&!empty($data['filter_coupon_code'])) {
			$implode[] = "c.coupon_code LIKE '%" . $this->db->escape($data['filter_coupon_code']) . "%'";
		}
		if (isset($data['filter_qc_status'])&&!empty($data['filter_qc_status'])) {
			$implode[] = "pl.plan_qc = '" . $this->db->escape($data['filter_qc_status']) . "'";
		}
		if (isset($data['filter_store_code'])&&!empty($data['filter_store_code'])) {
			$implode[] = "pl.store_code = '" . $this->db->escape($data['filter_store_code']) . "'";
		}
		if (isset($data['customers_manager'])&& is_array($data['customers_manager'])) {
			$sql .= " AND pl.`customer_user_id` IN('".implode("','",$data['customers_manager'])."')";
		} 
		if (isset($data['filter_sup_id'])&&!empty($data['filter_sup_id'])) {
			$implode[] = "c.sup_id = '" . $this->db->escape($data['filter_sup_id']) . "'";
		}
		if (isset($data['filter_distributor_id'])&&!empty($data['filter_distributor_id'])) {
			$implode[] = "c.distributor_id = '" . $this->db->escape($data['filter_distributor_id']) . "'";
		}
		if (isset($data['filter_yearmonth'])&&!empty($data['filter_yearmonth'])) {
			$implode[] = "c.yearmonth = '" . $this->db->escape($data['filter_yearmonth']) . "'";
		}
		if (isset($data['filter_round'])&&!empty($data['filter_round'])) {
			$implode[] = "c.round_name = '" . $this->db->escape($data['filter_round']) . "'";
		}
		if (isset($data['filter_coupon_status'])) {
			$implode[] = "c.coupon_status_id = '" . (int)$data['filter_coupon_status'] . "'";
		}
		if (isset($data['filter_coupon_scan'])) {
			$implode[] = "c.dist_check_id = '" . (int)$data['filter_coupon_scan'] . "'";
		}
		if (isset($data['filter_reward_type_id'])&&!empty($data['filter_reward_type_id'])) {
			$implode[] = "c.reward_type_id = '" . (int)$data['filter_reward_type_id'] . "'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
		/*Filter end*/
		if ((!empty($data['filter_date_start'])&& !is_null($data['filter_date_start'])) && (!empty($data['filter_date_end'])&& !is_null($data['filter_date_end']))) {
			$sql .= " AND DATE(`date_upload`) BETWEEN '" . $data['filter_date_start'] . "' AND '". $data['filter_date_end'] ."'";
		}
		 
		//Nếu là NPP đăng nhập
		if ($this->distributor->isLogged()&&isset($data['filter_coupon_code'])&&!empty($data['filter_coupon_code'])) {
			$sql .= " AND c.`dist_check`>0";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->pdb->query($sql);
		return $query->rows;
	}

	public function getTotalCouponsHistory($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "plan_coupon_history c LEFT JOIN " . PDB_PREFIX . "plan pl ON (pl.plan_id = c.plan_id) WHERE c.plan_id >0 AND c.coupon_code!=''";
		
		//Nếu không phải user CPM dăng nhập
		if (!$this->user->isLogged()&&($this->distributor->isLogged()||$this->customer->isLogged())) {
			$sql .= " AND pl.plan_status=1 AND pl.plan_qc=1";
		}
		
		/*Filter start*/ 
		$implode = array();

		if (isset($data['filter_coupon_code'])&&!empty($data['filter_coupon_code'])) {
			$implode[] = "c.coupon_code LIKE '%" . $this->db->escape($data['filter_coupon_code']) . "%'";
		}
		if (isset($data['filter_store_code'])&&!empty($data['filter_store_code'])) {
			$implode[] = "pl.store_code = '" . $this->db->escape($data['filter_store_code']) . "'";
		}
		if (isset($data['filter_qc_status'])&&!empty($data['filter_qc_status'])) {
			$implode[] = "pl.plan_qc = '" . $this->db->escape($data['filter_qc_status']) . "'";
		}
		if (isset($data['customers_manager'])&& is_array($data['customers_manager'])) {
			$sql .= " AND pl.`customer_user_id` IN('".implode("','",$data['customers_manager'])."')";
		}
		if (isset($data['filter_sup_id'])&&!empty($data['filter_sup_id'])) {
			$implode[] = "c.sup_id = '" . $this->db->escape($data['filter_sup_id']) . "'";
		}
		if (isset($data['filter_distributor_id'])&&!empty($data['filter_distributor_id'])) {
			$implode[] = "c.distributor_id = '" . $this->db->escape($data['filter_distributor_id']) . "'";
		}
		if (isset($data['filter_yearmonth'])&&!empty($data['filter_yearmonth'])) {
			$implode[] = "c.yearmonth = '" . $this->db->escape($data['filter_yearmonth']) . "'";
		}
		if (isset($data['filter_round'])&&!empty($data['filter_round'])) {
			$implode[] = "c.round_name = '" . $this->db->escape($data['filter_round']) . "'";
		}
		if (isset($data['filter_coupon_status'])) {
			$implode[] = "c.coupon_status_id = '" . (int)$data['filter_coupon_status'] . "'";
		}
		if (isset($data['filter_coupon_scan'])) {
			$implode[] = "c.dist_check_id = '" . (int)$data['filter_coupon_scan'] . "'";
		}
		if (isset($data['filter_reward_type_id'])&&!empty($data['filter_reward_type_id'])) {
			$implode[] = "c.reward_type_id = '" . (int)$data['filter_reward_type_id'] . "'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
		/*Filter end*/
		if ((!empty($data['filter_date_start'])&& !is_null($data['filter_date_start'])) && (!empty($data['filter_date_end'])&& !is_null($data['filter_date_end']))) {
			$sql .= " AND DATE(`date_upload`) BETWEEN '" . $data['filter_date_start'] . "' AND '". $data['filter_date_end'] ."'";
		}
		//Nếu là NPP đăng nhập
		if ($this->distributor->isLogged()&&isset($data['filter_coupon_code'])&&!empty($data['filter_coupon_code'])) {
			$sql .= " AND c.`dist_check`>0";
		}
		$query = $this->pdb->query($sql);
		
		return $query->row['total'];
	}
	public function getTotalCouponsByMonth($data = array()) {
		
		$config_dist_valid = $this->config->get('config_dist_valid');
		$sql = "SELECT c.`yearmonth` , c.`coupon_prefix` , COUNT(*) AS total FROM " . PDB_PREFIX . "plan_coupon_history c";
		$sql .= " LEFT JOIN " . PDB_PREFIX . "plan pl ON (pl.plan_id = c.plan_id)";
		$sql .= " WHERE c.`dist_check_id` ='".(int)$config_dist_valid."'";
		
		
		/*Filter start*/ 
		$implode = array();

		if (isset($data['filter_coupon_code'])&&!empty($data['filter_coupon_code'])) {
			$implode[] = "c.coupon_code LIKE '%" . $this->db->escape($data['filter_coupon_code']) . "%'";
		}
		if (isset($data['filter_sup_id'])&&!empty($data['filter_sup_id'])) {
			$implode[] = "c.sup_id = '" . $this->db->escape($data['filter_sup_id']) . "'";
		}
		if (isset($data['filter_distributor_id'])&&!empty($data['filter_distributor_id'])) {
			$implode[] = "c.distributor_id = '" . $this->db->escape($data['filter_distributor_id']) . "'";
		}
		if (isset($data['filter_yearmonth'])&&!empty($data['filter_yearmonth'])) {
			$implode[] = "c.yearmonth = '" . $this->db->escape($data['filter_yearmonth']) . "'";
		}
		if (isset($data['filter_round'])&&!empty($data['filter_round'])) {
			$implode[] = "c.round_name = '" . $this->db->escape($data['filter_round']) . "'";
		}
		if (isset($data['filter_coupon_status'])) {
			$implode[] = "c.coupon_status_id = '" . (int)$data['filter_coupon_status'] . "'";
		}
		if (isset($data['filter_coupon_scan'])) {
			$implode[] = "c.dist_check_id = '" . (int)$data['filter_coupon_scan'] . "'";
		}
		if (isset($data['filter_reward_type_id'])&&!empty($data['filter_reward_type_id'])) {
			$implode[] = "c.reward_type_id = '" . (int)$data['filter_reward_type_id'] . "'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
		/*Filter end*/
		if ((!empty($data['filter_date_start'])&& !is_null($data['filter_date_start'])) && (!empty($data['filter_date_end'])&& !is_null($data['filter_date_end']))) {
			$sql .= " AND DATE(`date_upload`) BETWEEN '" . $data['filter_date_start'] . "' AND '". $data['filter_date_end'] ."'";
		}
		//Nếu là NPP đăng nhập
		if ($this->distributor->isLogged()&&isset($data['filter_coupon_code'])&&!empty($data['filter_coupon_code'])) {
			$sql .= " AND c.`dist_check`>0";
		}
		
		if (isset($data['customers_manager'])&& is_array($data['customers_manager'])) {
			$sql .= " AND pl.`customer_user_id` IN('".implode("','",$data['customers_manager'])."')";
		}
		$sql .= " GROUP BY c.`yearmonth`, c.`coupon_prefix` ";
	
		$query = $this->pdb->query($sql);
		
		$return_data = array();
		if($query->rows){
			foreach($query->rows as $row){
					$month = (string)datetimeConvert($row['yearmonth'], 'm');
				$return_data[$month][$row['coupon_prefix']] = $row['total'];
			}
		}
		
		return $return_data;
		
	}
	
	  public function mappingCouponsByMonth($coupons){
		$config_dist_valid = $this->config->get('config_dist_valid');
		$query_data = array();
		if(!empty($coupons)){
			foreach($coupons as $row){
				if($row['dist_check_id']==$config_dist_valid){
					$query_data[$row['yearmonth']][$row['coupon_prefix']] += 1;;
				}
			}
		}
	
		return $query_data;
	  }
	
	public function getYearMonths() {
		$yearmonths_data = array();
			$query = $this->pdb->query("SELECT `yearmonth` FROM " . DB_PREFIX . "plan_coupon_history WHERE plan_id > 0 AND coupon_code!='' GROUP BY `yearmonth` ORDER BY `yearmonth` DESC");
			foreach ($query->rows as $row){
				if($row['yearmonth']!=''){
					$yearmonths_data[] = $row['yearmonth'];
				}
			}
		return $yearmonths_data;
		
	}
}