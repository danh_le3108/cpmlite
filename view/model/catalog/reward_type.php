<?php
class ModelCatalogRewardType extends Model {
	public function addRewardType($data) {
		$this->pdb->query("INSERT INTO " . PDB_PREFIX . "catalog_reward_type SET reward_prefix = '" . $this->db->escape($data['reward_prefix']) . "',reward_type_name = '" . $this->db->escape($data['reward_type_name']) . "'");
		
		return $this->db->getLastId();
	}

	public function editRewardType($reward_type_id, $data) {
		$this->pdb->query("UPDATE " . PDB_PREFIX . "catalog_reward_type SET  
		reward_prefix = '" . $this->db->escape($data['reward_prefix']) . "',
		reward_type_name = '" . $this->db->escape($data['reward_type_name']) . "'
		WHERE reward_type_id = '" . (int)$reward_type_id . "'");
	}

	public function deleteRewardType($reward_type_id) {
		$this->pdb->query("DELETE FROM " . PDB_PREFIX . "catalog_reward_type WHERE reward_type_id = '" . (int)$reward_type_id . "'");
	}

	public function getRewardType($reward_type_id) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "catalog_reward_type WHERE reward_type_id = '" . (int)$reward_type_id . "'");

		return $query->row;
	}

	public function getRewardTypes($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . PDB_PREFIX . "catalog_reward_type p WHERE 1";
			
			/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "p.reward_type_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 

			$sort_data = array(
				'p.reward_type_id'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY p.reward_type_id";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->pdb->query($sql);

			return $query->rows;
		} else {
			$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "catalog_reward_type p ORDER BY p.reward_type_id ASC");

			$query_data = $query->rows;

			return $query_data;
		}
	}

	public function getTotalRewardTypes($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "catalog_reward_type p WHERE 1";
			
			/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "p.reward_type_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 
			
			$query = $this->pdb->query($sql);

		return $query->row['total'];
	}
	public function getRewardTypesIndexBy($index_by = 'reward_type_id') {
		$query_data = array();
		$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "catalog_reward_type t");

		foreach ($query->rows as $row){
			$query_data[$row[$index_by]] = $row;
		}

		return $query_data;
	}
}