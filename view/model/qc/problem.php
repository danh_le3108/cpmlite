<?php
class ModelQcProblem extends Model {
	public function addProblem($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "qc_problem SET 
		problem_name = '" . $this->db->escape($data['problem_name']) . "', 
		code_id = '" . (int)$data['code_id'] . "'");
		
		return $this->db->getLastId();
	}

	public function editProblem($problem_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "qc_problem SET  
		problem_name = '" . $this->db->escape($data['problem_name']) . "', 
		code_id = '" . (int)$data['code_id'] . "'
		WHERE problem_id = '" . (int)$problem_id . "'");
	}

	public function deleteProblem($problem_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "qc_problem WHERE problem_id = '" . (int)$problem_id . "'");
	}

	public function getProblem($problem_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "qc_problem WHERE problem_id = '" . (int)$problem_id . "'");

		return $query->row;
	}

	public function getProblems($data = array()) {
		if ($data) {
			$sql = "SELECT *, (SELECT p.code_name FROM `" . DB_PREFIX . "qc_code` p WHERE p.code_id = d.code_id) AS code_name FROM " . DB_PREFIX . "qc_problem d WHERE 1";
			
		/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "d.problem_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_code'])) {
			$implode[] = "d.code_id = '" . (int)$data['filter_code'] . "'";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 

			$sort_data = array(
				'd.problem_name'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY d.problem_name";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);
			
			$query_data = array();
			if($query->rows){
				foreach($query->rows as $row){
					$query_data[$row['problem_id']] = $row;
				}
			}
			return $query_data;
		} else {
			$query = $this->db->query("SELECT *, (SELECT p.code_name FROM `" . DB_PREFIX . "qc_code` p WHERE p.code_id = d.code_id) AS code_name FROM " . DB_PREFIX . "qc_problem d ORDER BY d.problem_name ASC");

			$query_data = $query->rows;

			return $query_data;
		}
	}

	public function getTotalProblems($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "qc_problem d WHERE 1";
			
		/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "d.problem_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if (!empty($data['filter_code'])) {
			$implode[] = "d.code_id = '" . (int)$data['filter_code'] . "'";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
		/*Filter end*/ 

		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	public function getProblemsByCodeId($code_id) {

			$sql = "SELECT * FROM " . DB_PREFIX . "qc_problem";
			
			if($code_id!=0&&$code_id!=''){
				$sql .= " WHERE code_id = '" . (int)$code_id . "'";
			}
			
			$sql .= " ORDER BY problem_name";
			
			$query = $this->db->query($sql);

			$problem_data = $query->rows;

		return $problem_data;
	}
	public function getProblemsIndexBy($index_by = 'problem_id') {
		$query_data = array();
		$query = $this->db->query("SELECT * FROM " . PDB_PREFIX . "qc_problem t");

		foreach ($query->rows as $row){
			$query_data[$row[$index_by]] = $row;
		}

		return $query_data;
	}
}