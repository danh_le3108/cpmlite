<?php
class ModelToolOnline extends Model {
	public function addOnline($ip,$user_id,$customer_user_id,$username, $url, $referer,$user_agent) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "history_online` WHERE date_added < '" . date('Y-m-d H:i:s', strtotime('-12 hour')) . "'");
		$sql = "REPLACE INTO `" . DB_PREFIX . "history_online` SET `ip` = '" . $this->db->escape($ip) . "', 
		`project_id` = '" . (int)$this->config->get('config_project_id') . "', 
		`user_id` = '" . (int)$user_id . "', 
		`customer_user_id` = '" . (int)$customer_user_id . "', 
		`username` = '" . $this->db->escape($username) . "', 
		`url` = '" . $this->db->escape($url) . "', 
		`referer` = '" . $this->db->escape($referer) . "', 
		`user_agent` = '" . $this->db->escape($user_agent) . "', 
		`date_added` = '" . $this->db->escape(date('Y-m-d H:i:s')) . "'";
		$this->db->query($sql);
	}
}
