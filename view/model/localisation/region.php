<?php
class ModelLocalisationRegion extends Model {
	
	public function getCPMRegions() {
		$query = $this->db->query("SELECT * FROM " . PDB_PREFIX . "local_region r ORDER BY r.sort_order ASC");
		$query_data = $query->rows;
		return $query_data;
	}
	
	 public function getRegionsByAreaId($area_id){
	  $sql = "SELECT * FROM `" .PDB_PREFIX. "local_region` WHERE `area_id` = '" . (int)$area_id . "'";
	  $query = $this->pdb->query($sql);
	  return $query->rows;
	 }
	public function addRegion($data) {
		$this->pdb->query("INSERT INTO " . PDB_PREFIX . "local_region SET
		region_name = '" . $this->pdb->escape($data['region_name']) . "',
		region_code = '" . $this->pdb->escape($data['region_code']) . "',
		sort_order = '" . (int)$data['sort_order'] . "'");
		return $this->pdb->getLastId();
	}

	public function editRegion($region_id, $data) {
		$this->pdb->query("UPDATE " . PDB_PREFIX . "local_region SET
		region_name = '" . $this->pdb->escape($data['region_name']) . "',
		region_code = '" . $this->pdb->escape($data['region_code']) . "',
		sort_order = '" . (int)$data['sort_order'] . "'
		WHERE region_id = '" . (int)$region_id . "'");
	}

	public function deleteRegion($region_id) {
		$this->pdb->query("DELETE FROM " . DB_PREFIX . "local_region WHERE region_id = '" . (int)$region_id . "'");
	}

	public function getRegion($region_id) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "local_region WHERE region_id = '" . (int)$region_id . "'");

		return $query->row;
	}
	public function getRegionByCode($region_code) {
		$query = $this->pdb->query("SELECT DISTINCT * FROM " . PDB_PREFIX . "local_region WHERE region_code = '" . $region_code . "'");

		return $query->row;
	}

	public function getRegions($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . PDB_PREFIX . "local_region r WHERE 1";
			
			/*Filter start*/
			$implode = array();
	
			if (!empty($data['filter_name'])) {
				$implode[] = "r.region_name LIKE '%" . $this->pdb->escape($data['filter_name']) . "%'";
			}
			if (!empty($data['filter_area_id'])) {
				$implode[] = "r.area_id  = '" . (int)$data['filter_area_id'] . "'";
			}
			if (!empty($data['filter_region_codes'])) {
				$implode[] = "r.region_code IN (".$data['filter_region_codes'].")";
			}
			if ($implode) {
				$sql .= " AND " . implode(" AND ", $implode);
			}
			/*Filter end*/

			$sort_data = array(
				'r.region_name'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY r.sort_order,r.region_id";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

		} else {
			$sql = "SELECT * FROM " . PDB_PREFIX . "local_region r ORDER BY r.sort_order,r.region_id ASC";

		}
			$query = $this->pdb->query($sql);

			return $query->rows;
	}

	public function getTotalRegions($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "local_region r WHERE 1";

			/*Filter start*/
		$implode = array();
		if (!empty($data['filter_region_codes'])) {
			$implode[] = "r.region_code IN (".$data['filter_region_codes'].")";
		}
		if (!empty($data['filter_name'])) {
			$implode[] = "r.region_name LIKE '%" . $this->pdb->escape($data['filter_name']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/

		$query = $this->pdb->query($sql);
		return $query->row['total'];
	}
 	public function getRegionsIndexBy($index_by = 'region_code') {
	  $query_data = array();
	  $query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "local_region p");

	  foreach ($query->rows as $row){
	   	$query_data[$row[$index_by]] = $row;
	  }
	  return $query_data;
 	}
}