<?php
class ModelLocalisationImageType extends Model {
	public static $static_type = array();
	public function getAllTypes() {
		if (!empty(self::$static_type)) {
			return self::$static_type;
		}
		$list_type = $this->config->get('config_project_images');
		$type_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "image_type a ORDER BY a.sort_order ASC");

		foreach($query->rows as $row){
			if(in_array($row['image_type_id'], $list_type)){
				$type_data[] = $row;
			}
		}
	 	return self::$static_type = $type_data;
	}
	
	public function addImageType($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "image_type SET 
		project_id = '" . (int)$this->config->get('config_project_id') . "', 
		sort_order = '" . (int)$data['sort_order'] . "', 
		required = '" . (int)$data['required'] . "', 
		image_type = '" . $this->db->escape($data['image_type']) . "',
		upload_limit = '" . $this->db->escape($data['upload_limit']) . "'");
		
		return $this->db->getLastId();
	}

	public function editImageType($image_type_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "image_type SET  
		project_id = '" . (int)$this->config->get('config_project_id') . "',
		sort_order = '" . (int)$data['sort_order'] . "', 
		required = '" . (int)$data['required'] . "', 
		image_type = '" . $this->db->escape($data['image_type']) . "',
		upload_limit = '" . $this->db->escape($data['upload_limit']) . "'
		WHERE image_type_id = '" . (int)$image_type_id . "'");
	}

	public function deleteImageType($image_type_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "image_type WHERE image_type_id = '" . (int)$image_type_id . "'");
	}

	public function getImageType($image_type_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "image_type WHERE image_type_id = '" . (int)$image_type_id . "'");

		return $query->row;
	}
	public function getImageTypes($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "image_type i WHERE 1 AND (i.project_id = '" . (int)$this->config->get('config_project_id') . "' OR i.project_id = 0)";
			

			/*Filter start*/ 
			$implode = array();
	
			if (!empty($data['filter_name'])) {
				$implode[] = "i.image_type LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			}
			if ($implode) {
				$sql .= " AND " . implode(" AND ", $implode);
			}
			
			/*Filter end*/ 
			
			$sort_data = array(
				'i.image_type'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY i.sort_order";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "image_type i WHERE 1 AND (i.project_id = '" . (int)$this->config->get('config_project_id') . "' OR i.project_id = 0) ORDER BY i.sort_order ASC");

			$query_data = $query->rows;

			return $query_data;
		}
	}

	public function getTotalImageType($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "image_type i WHERE 1 AND (i.project_id = '" . (int)$this->config->get('config_project_id') . "' OR i.project_id = 0)";
		
			/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "i.image_type LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}