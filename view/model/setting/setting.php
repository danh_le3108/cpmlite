<?php
class ModelSettingSetting extends Model {
	public function getSetting($code) {
		$data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE project_id = '" . (int)$this->config->get('config_project_id') . "' AND `code` = '" . $this->db->escape($code) . "'");

		foreach ($query->rows as $result) {
			if (!$result['serialized']) {
				$data[$result['key']] = $result['value'];
			} else {
				$data[$result['key']] = json_decode($result['value'], true);
			}
		}

		return $data;
	}
	public function editSetting($code, $data) {
		foreach ($data as $key => $value) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE project_id = '" . (int)$this->config->get('config_project_id') . "' AND `code` = '" . $this->db->escape($code) . "' AND `key` = '" . $this->db->escape($key) . "'");	
		}
		foreach ($data as $key => $value) {
				if (!is_array($value)) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET project_id = '" . (int)$this->config->get('config_project_id'). "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
				} else {
					$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET project_id = '" . (int)$this->config->get('config_project_id') . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(json_encode($value, true)) . "', serialized = '1'");
				}
		}
	}
}