<?php
class ModelWarehouseWarehouse extends Model {
	private $no_filter_key = array(
		'_url',
		'route',
		'filter_global',
		'start',
		'limit',
		'sort',
		'order',
		'page'
	);

	
	public function countTotal($data = array()) {
		$sql = "SELECT count(*) as total FROM " . DB_PREFIX . "warehouse WHERE is_deleted = 0";
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				if (!in_array($key, $this->no_filter_key)) {
					$key = str_replace('filter_', '', $key);
					$sql .= " AND {$key} = '" . $this->pdb->escape($value) . "'";
				}	
			}

			if (!empty($data['filter_global'])) {
				$filter_global = array();
				$sql .= " AND (";
				foreach ($filter_global as $filter) {
					$implode[] = " LCASE(".$filter.") LIKE '%" . $this->pdb->escape(utf8_strtolower($data['filter_global'])) . "%'";
				}
				$sql .= " " . implode(" OR ", $implode) . "";
				$sql .= ")";
			}
		}

		return $this->pdb->query($sql)->row['total'];
	}

	public function getWarehouses($data = array()) { 
		$sql = "SELECT * FROM " . DB_PREFIX . "warehouse WHERE is_deleted = 0";
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				if (!in_array($key, $this->no_filter_key)) {
					$key = str_replace('filter_', '', $key);
					$sql .= " AND {$key} = '" . $this->pdb->escape($value) . "'";
				}	
			}

			if (!empty($data['filter_global'])) {
				$filter_global = array(
					
				);
				$sql .= " AND (";
				foreach ($filter_global as $filter) {
					$implode[] = " LCASE(".$filter.") LIKE '%" . $this->pdb->escape(utf8_strtolower($data['filter_global'])) . "%'";
				}
				$sql .= " " . implode(" OR ", $implode) . "";
				$sql .= ")";
			}

			$sql .= ' ORDER BY warehouse_id DESC';
			if (isset($data['start']) && isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}
				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}
		}
		return $this->pdb->query($sql)->rows;
	}

	public function getWarehouse($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "warehouse WHERE is_deleted = 0";
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				if (!in_array($key, $this->no_filter_key)) {
					$key = str_replace('filter_', '', $key);
					$sql .= " AND {$key} = '" . $this->pdb->escape($value) . "'";
				}	
			}
		}
		$wh = $this->pdb->query($sql)->row;
		return $wh;
	}
	
	public function addWarehouse($data = array()) {
		$sql = "INSERT INTO `" . PDB_PREFIX . "warehouse` SET ";
		foreach ($data as $field => $value) {
			$sql .= " $field = '" . $this->pdb->escape($value) . "', ";
		}
		$sql .= "date_added = NOW()";
		$this->pdb->query($sql);
		return $this->pdb->getLastId();
	}
	
	public function updateWarehouse($warehouse_id, $data) {
		$sql = "UPDATE `" . PDB_PREFIX . "warehouse` SET ";
		foreach ($data as $field => $value) {
			$sql .= " $field = '" . $this->pdb->escape($value) . "', ";
		}
		$sql .= "date_modified = NOW() WHERE warehouse_id = " . (int)$warehouse_id;
		$this->pdb->query($sql);
	}

	public function deleteWarehouse($warehouse_id) {
		$sql = "UPDATE " . DB_PREFIX . "warehouse SET is_deleted = 1, user_deleted = " . $this->user->getId() . ", date_deleted = NOW() WHERE warehouse_id = " . (int)$warehouse_id;
		$this->pdb->query($sql);
	}
}