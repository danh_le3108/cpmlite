<?php
class ModelAccountOnlineHistory extends Model {
	
	
	
	public function getOnlineHistories($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . PDB_PREFIX . "customer_login cl WHERE cl.customer_user_id>0";
			
		
		$filter_global = array(
			'cl.username',
			'cl.ip',
			'cl.date_added'
		);
		if (!empty($data['filter_global'])) {
			$sql .= " AND (";
			foreach ($filter_global as $filter) {
				$gimplode[] = " LCASE(".$filter.") LIKE '%" . $this->pdb->escape(utf8_strtolower($data['filter_global'])) . "%'";
			}
			$sql .= " " . implode(" OR ", $gimplode) . "";
			$sql .= ")";
		}
		/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "cl.username LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_round'])) {
			$implode[] = "cl.date_modified LIKE '" . $this->db->escape($data['filter_round']) . "%'";
		}
		if (!empty($data['filter_level_id'])) {
			$implode[] = "cl.customer_level_id = '" . $this->db->escape($data['filter_level_id']) . "'";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			/*Filter end*/ 

			$sort_data = array(
				'cl.date_modified'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY cl.date_modified,cl.customer_user_id";
			}

			if (isset($data['order']) && ($data['order'] == 'ASC')) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->pdb->query($sql);

			return $query->rows;
		} else {
			$query = $this->pdb->query("SELECT * FROM " . PDB_PREFIX . "customer_login cl WHERE  cl.customer_user_id>0 ORDER BY cl.date_modified,cl.customer_user_id DESC");

			$query_data = $query->rows;

			return $query_data;
		}
	}

	public function getTotalOnlineHistories($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . PDB_PREFIX . "customer_login cl WHERE  cl.customer_user_id>0";
		
		$filter_global = array(
			'cl.username',
			'cl.ip',
			'cl.date_added'
		);
		if (!empty($data['filter_global'])) {
			$sql .= " AND (";
			foreach ($filter_global as $filter) {
				$gimplode[] = " LCASE(".$filter.") LIKE '%" . $this->pdb->escape(utf8_strtolower($data['filter_global'])) . "%'";
			}
			$sql .= " " . implode(" OR ", $gimplode) . "";
			$sql .= ")";
		}
		/*Filter start*/ 
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "cl.username LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if (!empty($data['filter_round'])) {
			$implode[] = "cl.date_modified LIKE '" . $this->db->escape($data['filter_round']) . "%'";
		}
		if (!empty($data['filter_level_id'])) {
			$implode[] = "cl.customer_level_id = '" . $this->db->escape($data['filter_level_id']) . "'";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
		/*Filter end*/ 

		$query = $this->pdb->query($sql);
		
		return $query->row['total'];
	}
}