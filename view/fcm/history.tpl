<style media="screen">
  .message-content ul li * {
    display: block;
  }

  .message-content ul li .title {
    font-size: 14px;
  }

  .message-content ul li .message {
    padding: 5px;
    border: 1px dashed #ddd;
    margin-bottom: 5px;
  }
</style>
<div class="well">
  <?php if($histories){ ?>
  <ul class="message-content">
      <?php foreach ($histories as $history) { ?>
          <li class="<?php echo $history['user_id']==$logged_id?'text-right':'text-left' ; ?>">
              <label class="title"><?php echo $history['title']; ?></label>
              <span class="message"><?php echo $history['message']; ?></span>
              <span class="fullname small"><?php echo $history['fullname']; ?> - <?php echo $history['date_added']; ?></span>
          </li>
      <?php } ?>
  </ul>
  <?php } else { ?>
    <span>Không có tin nhắn</span>
  <?php } ?>
</div>
