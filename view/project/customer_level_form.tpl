 <div class="modal-dialog modal-lg">
        <div class="modal-content">
        
    <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal">×</button>
    </div>
            <div class="modal-body">
            
              <div class="form-group row">
                    <div class="col-sm-6">
                        <label class="control-label" for="level_name"><?php echo $text_group_name;?></label>
                        <div class="clearfix">
                          
                      <input type="text" name="level_name" value="<?php echo $info['level_name']; ?>" placeholder="<?php echo $text_group_name; ?>" id="level_name" class="form-control" />
                        </div>
                      </div>
                    <div class="col-sm-6">
                        <label class="col-sm-2 control-label" for="input-description"><?php echo $text_description; ?></label>
                        <div class="clearfix">
                          
                      <input type="text" name="level_description" value="<?php echo $info['level_description']; ?>" placeholder="<?php echo $text_description; ?>" id="input-description" class="form-control" />
                        </div>
                      </div>
               </div>
              <div class="form-group row">
                  <div class="col-sm-6">
                    <label class="control-label" for="input-sort_order"><?php echo $text_sort_order; ?></label>
                     <div class="clearfix">
                      <input type="text" name="sort_order" value="<?php echo $info['sort_order']; ?>" placeholder="<?php echo $text_sort_order; ?>" id="input-sort_order" class="form-control" />
                      
                    </div>
                  </div>
                 <div class="col-sm-6"> 
                  <label class=" control-label" for="child_level_id"><?php echo 'Child group'; ?></label>
                 <div class="clearfix">
                 <select name="child_level_id" id="child_level_id" class="form-control">
                    <option value="0"  <?php if (0 == $info['child_level_id']) { ?>selected="selected" <?php } ?>><?php echo $text_none; ?></option>
                      <?php foreach ($customer_levels as $level) { ?>
                      <option value="<?php echo $level['customer_level_id']; ?>"  <?php if ($level['customer_level_id'] == $info['child_level_id']) { ?>selected="selected" <?php } ?>><?php echo $level['level_name']; ?></option>
                      <?php } ?>
                    </select>
                   
                  </div>
               </div><!--//--> 
               
            </div>
            <!--panel body --> 
          
    <div class="modal-footer">
    <div class="row">
    
    <div class="col-sm-6 message">
    </div>
    
    <div class="col-sm-6">
     <a onClick="saveRoute('customer_level','<?php echo $customer_level_id;?>','<?php echo $customer_level_id;?>');" class="btn btn-primary btn-sm pull-right"><i class="fa fa-save"></i> <?php echo $button_save; ?></a>
    </div>
    
    </div>
    </div>
    <!--//modal-footer -->
            </div>
    </div>
    <!--// modal -->