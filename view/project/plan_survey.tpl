<div class="box box-info" id="posm">
  <div class="box-header">
    <h3 class="box-title">Danh sách đơn hàng</h3>
    <?php if($hasAdd) { ?> 
    <button class="btn btn-success btn-sm pull-right" title="Tạo đơn hàng" id="add-order" data-toggle="modal" data-target="#order-modal"><i class="fa fa-plus" aria-hidden="true"></i></button>
    <?php } ?>
  </div>
  <div class="box-body" id="order-list">
    
  </div>
  <style type="text/css">
  .group {
    border-bottom: 2px solid #dd4b39;
    padding-bottom: 5px;
    color: #dd4b39;
    margin-bottom: 10px;
    overflow: auto;
    position: relative;
    padding-left: 0px;
  }

  .price, .price_total,.tr-del,.stt {
    line-height: 34px !important;
  }
</style>
<?php if ($isUser) { ?>
<div class="modal fade" id="order-modal" role="dialog" style="overflow-y:scroll">
    <div class="modal-dialog" style="width: 70%">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tạo đơn hàng</h4>
        </div>
        <div class="modal-body">
          <!-- <div class="col-sm-12 group">HÌNH KHÁCH HÀNG</div>
          <div class="col-sm-12 thumbnails order-image" style="padding-left: 0px;padding-right: 0px;">
        
          </div>
          <?php if($hasEdit) { ?> 
          <div class="col-sm-12">
            <button type="button" class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#upload-order-img"><i class="fa fa-upload"></i> Tải lên</button>
          </div>
          <?php } ?> -->
          <form id="sku-form">
            <input type="hidden" id="order_id" value="">
          <?php foreach($attrs as $group_code => $attributes) { 
            if ($group_code != 'CUSTOMER') { 
          ?>
            <div class="col-sm-12 group"><?=$attributes[0]['group_name']?></div>
            <table class="table">
              <?php
              $count = count($attributes);
              for ($i = 0; $i < $count; $i+=2) {
              ?>
              <tr>
                <td width="15%"><?=isset($attributes[$i]) ? $attributes[$i]['attribute_name'] : ''?></td>
                <td width="35%">
                <?php 
                  if (isset($attributes[$i])) {
                    if ($attributes[$i]['input'] == 'select') {
                ?>
                  <select class="form-control" id="<?=$attributes[$i]['attribute_code']?>" name="<?=$attributes[$i]['attribute_code']?>">
                  <option value="0">--- Chọn ---</option>
                  <?php 
                  $options = json_decode($attributes[$i]['options']);
                  foreach($options as $op) {
                  ?>
                  <option value="<?=$op->id?>"><?=$op->name?></option>
                  <?php } ?>
                  </select>
                <?php
                  }else {
                ?>
                  <input type="text" id="<?=$attributes[$i]['attribute_code']?>" name="<?=$attributes[$i]['attribute_code']?>" class="form-control">
                <?php
                    }
                  } else {
                    echo '';
                  }
                ?>
                </td>
                <td width="15%"><?=isset($attributes[$i+1]) ? $attributes[$i+1]['attribute_name'] : ''?></td>
                <td width="35%">
                <?php 
                  if (isset($attributes[$i+1])) {
                    if ($attributes[$i+1]['input'] == 'select') {
                ?>
                  <select class="form-control" id="<?=$attributes[$i+1]['attribute_code']?>" name="<?=$attributes[$i+1]['attribute_code']?>">
                  <option value="0">--- Chọn ---</option>
                  <?php 
                  $options = json_decode($attributes[$i+1]['options']);
                  if (!empty($options)) {
                  foreach($options as $op) {
                  ?>
                  <option value="<?=$op->id?>"><?=$op->name?></option>
                  <?php } ?>
                  </select>
                <?php
                    }} else {
                ?>
                  <input type="text" id="<?=$attributes[$i+1]['attribute_code']?>" name="<?=$attributes[$i+1]['attribute_code']?>" class="form-control">
                <?php
                    }
                  } else {
                    echo '';
                  }
                ?>
                </td>
              </tr>
              <?php } ?>
            </table>
            <?php
              } else {
            ?>
            <div class="col-sm-12 group">
              <span style="position: absolute;bottom: 5px;"><?=$attributes[0]['group_name']?></span>
              <?php if($hasEdit) { ?> 
              <button class="btn btn-success btn-sm pull-right" title="Thêm sản phẩm" id="add-product" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button>
              <?php } ?>
            </div>
            <table class="table">
              <thead>
                <tr style="background-color: #99ccff;color: white;">
                  <th class="text-center" width="1">STT</th>
                  <th class="text-center">Full name</th>
                  <th class="text-center" width="15%">YOB</th>
                  <th class="text-center" width="15%">PHONE</th>
                  <th class="text-center" width="15%">Current brand</th>
                  <th class="text-center" width="15%">Note</th>
                  <?php if($hasEdit) {?>
                  <th class="text-center" width="1"><i class="fa fa-trash" aria-hidden="true"></i></th>
                  <?php } ?>
                </tr>
              </thead>
              <tbody id="sku-list">
                <tr id="product-0" style="display: none">
                  <td class="stt text-center"></td>
                  <?php foreach($attributes as $attr) { 
                    if ($attr['input'] == 'select') {
                      $options = json_decode($attr['options']);
                  ?>
                  <td>
                    <select class="form-control <?=$attr['attribute_code']?>" name="<?=$attr['attribute_code']?>">
                      <option value="0">--- Chọn ---</option>
                      <?php foreach($options as $op) { ?>
                      <option value="<?=$op->id?>"><?=$op->name?></option>
                      <?php } ?>
                    </select>
                  </td>
                  <?php 
                    } elseif ($attr['input'] == 'date') {
                  ?>
                  <td>
                    <input type="text" name="<?=$attr['attribute_code']?>" value="" class="form-control datetime" data-date-format="YYYY-MM-DD" autocomplete="off">
                  </td>
                  <?php
                    } else {
                  ?>
                  <td>
                    <input class="form-control <?=$attr['attribute_code']?>" type="text"  name="<?=$attr['attribute_code']?>" value="">
                  </td>
                  <?php
                    }
                  } ?>
                  <td style="display: none">
                    <input type="hidden" class="detail_id" name="detail_id" value="0">
                  </td>
                  <?php if($hasEdit) {?>
                  <td class="text-center tr-del">
                    <a class="delete_product">
                      <i class="fa fa-trash text-warning" aria-hidden="true"></i>
                    </a>
                  </td>
                  <?php }?>
                </tr>
              </tbody>
            </table>
            <?php
              }
            ?> 
          <?php } ?>
          </form>
        </div>
        <div class="modal-footer">
          <?php if($hasEdit) {?>
          <button id="save-order" type="button" class="btn btn-primary">Lưu</button>
          <?php } ?>
        </div>
      </div>
      
    </div>
  </div>
</div>
<?php }?>
<div id="modal-delete-order" class="modal fade in">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            
         </div>
         <div class="modal-body">
            <div class="mpadding">
               Bạn có chắc chắn muốn xóa?
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-success pull-right" data-dismiss="modal" aria-hidden="true" autocomplete="off">Không</button>
            <a id="btn-delete-order" class="btn btn-danger pull-right" style="margin-right:15px;">Xác nhận Xóa</a>
         </div>
      </div>
   </div>
</div>

<div id="upload-order-img" class="modal modal-box fade in">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
         </div>
         <div class="modal-body clearfix text-center">
            <form action="" class="dropzone" id="my-dropzone2">
              <h3 class="sbold">Drop files here or click to upload</h3>
              <p> This is just a demo dropzone. Selected files are not actually uploaded. </p>
              <div class="fallback">
                <input name="file" type="file" multiple />
              </div>
            </form>
         </div>
         <!-- //modal-body--> 
         <div class="modal-footer">
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
  function loadOrderList() {
    $('#order-list').load('index.php?route=plan/plan_survey/getOrder&plan_id=<?=$plan_id?>');
  }
  loadOrderList();
  var skus = <?=json_encode($skus)?>;

  function addSku(data) {
    var tr = $('#product-0').clone();
    tr.css('display','table-row');
    var stt = $('#sku-list').find('tr').length;
    tr.find('.stt').html(stt);
    tr_id = 'order-detail-'+stt;
    $('#sku-list').append('<tr class="order-row" id="'+tr_id+'">'+tr.html()+'</tr>');
    if (data) {
      $('#'+tr_id).find('.full_name').val(data.full_name);
      $('#'+tr_id).find('.yob').val(data.yob);
      $('#'+tr_id).find('.phone').val(data.phone);
      $('#'+tr_id).find('.current_brand').val(data.current_brand);
      $('#'+tr_id).find('.note').val(data.note);
      $('#'+tr_id).find('.detail_id').val(data.detail_id);
    }
    // $('#'+tr_id).find('.sku').trigger('change');
  }

  $('#sku-list').on('change', '.sku', function() {
    var tr = $(this).closest('tr');
    tr.find('.price').html(formatMoney(skus[$(this).val()]));
    tr.find('.price_hidden').val(skus[$(this).val()]);
    calculate(this);
  })


  $('#add-product').click(function(){
    addSku();
  })

  $('#save-order').click(function(event) {
    var order_id = $('#order_id').val();
    $.post('index.php?route=plan/plan_survey/save&plan_id=<?=$plan['plan_id']?>&order_id='+order_id,$('#sku-form').serialize(),function(data) {
      $('#order-modal').modal('hide');
      $('#order-list').html(data);
    })
  })

  $('#order-modal').on('hide.bs.modal', function () {
    $('#order_id').val(0);
    $('#sku-form').trigger('reset'); 
    $('.order-image').children().remove();
    $('.order-row').remove();
  })

  $('#order-list').on('click', '.edit-order', function() {
    var order_id = $(this).closest('tr').attr('id').replace('order_', '');
    $('#order_id').val(order_id);

    // $('#my-dropzone2').attr('action','/index.php?route=plan/plan/edit&plan_id=<?=$plan_id?>&orderid='+order_id);
    var order = orders[order_id];
    $('#location').val(order.location);
    $('#no').val(order.no);
    $('#street').val(order.street);
    $('#ward').val(order.ward);
    $('#province').val(order.province);
    filter_district($('#province').val());
    $('#district').val(order.district);
    var district_id = 0;
    if (typeof order.order_detail !== 'undefined') {
      for (var i in order.order_detail) {
        addSku(order.order_detail[i]);
      }
    }
   
    // if (typeof order.images !== 'undefined') {
    //   for (var i in order.images) {
    //     var img = addImage(order.images[i]);
    //     img.find('select').remove();
    //     img.find('.btn-del-img').css('bottom','4px');
    //     img.find('.btn-manual').css('bottom','4px');
    //     img.appendTo('.order-image');
    //   }
    // }

    $('#order-modal .modal-title').html('Đơn hàng '+order.order_code);
    $('#order-modal').modal('show');
  })

  $('#order-list').on('click', '.delete_order', function() {
    var tr = $(this).closest('tr');
    var order_id = tr.attr('id').replace('order_', '');
    $('#modal-delete-order .modal-header').html('<h4>Xóa đơn hàng #' + order_id + '</h4>');
    $('#modal-delete-order').modal('show');
    $('#btn-delete-order').click(function(event) {
      event.preventDefault();
      $('#btn-delete-order').unbind('click');
      $.get('index.php?route=plan/plan_survey/deleteOrder&plan_id=<?=$plan['plan_id']?>&order_id='+order_id, function(data){
        $('#modal-delete-order').modal('hide');
        $('#order-list').html(data);
      })
    });
  })
  
  $('#sku-list').on('click', '.delete_product', function() {
    var tr = $(this).closest('tr');
    var detail_id = tr.find('.detail_id').val()*-1;
    if (detail_id != 0) {
      tr.find('.detail_id').val(detail_id);
      tr.css('display','none');
    } else {
      tr.remove();
    }
  }) 

  var districts = <?= json_encode($districts) ?>;
  
  function filter_district (province_id) {
    // console.log
    $('#district option').remove();
    $('#district').append($('<option>', { 
      value: 0,
      text : '--- Chọn ---',
    }));
    if (typeof districts[province_id] !== 'undefined') {
      for (var i in districts[province_id]) {
        $('#district').append($('<option>', { 
            value: districts[province_id][i].district_id,
            text : districts[province_id][i].name,
        }));
      }
    } 
  } 

  $('#order-modal').on('change', '#province', function() {
    filter_district(this.value)
  })

  Dropzone.options.myDropzone2 = {
    dictDefaultMessage: "Click to upload",
    url : 'index.php?route=plan/plan_image_audio/asdas',
    init: function() {
      this.on("processing", function(file) {
        var order_id = $('#order_id').val();
        this.options.url = "index.php?route=plan/plan_image_audio/upload&plan_id=<?=$plan_id?>&order_id="+order_id;
      });
      this.on("success", function(file, response) {
        if (response) {
          response = JSON.parse(response);
          var type = response.type;
          if (type == "image") {
            var img = addImage(response);
            img.find('select').remove();
            img.find('.btn-del-img').css('bottom','4px');
            img.find('.btn-manual').css('bottom','4px');
            img.appendTo('.order-image');
            var img2 = addImage(response);
            img2.appendTo('.plan_image');
            loadOrderList();
          }
        }
      })
    }
  };
</script>
