
  <style>
  .item-box {
    border: 1px solid #ddd;
    padding: 10px;
    margin-bottom: 5px;
    background: #fff;
}

</style>
  
  
  
  <div class="container-fluid">
  
    
  <div class="box box-info" id="filter-area">
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-6">
        <a onclick="clear_data();" class="btn btn-danger"><i class="fa fa-reply"></i> <?php echo $button_clear_data;?></a>
        </div>
        <div class="col-sm-3 text-right pull-right">
        <button type="submit" form="form-project" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
      
        </div>
      </div>
    </div>
    <!--panel-body -->
  </div>
  <!--box -->
  
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	
                  <div class="row">
                  <div class="col-sm-6">
                  
              
                   </div><!--//col --> 
                  <div class="col-sm-6">
                 </div><!--//col --> 
                   </div><!--//row --> 
                   
                   
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-project">
          <div class="row">
<div class="col-md-6">

	   <div class="box">
      <div class="box-header">
        <h2 class="box-title"> <?php echo $text_general; ?></h3>
      </div>
      <div class="panel-body">
	  
       <div class="form-group">
                  <label class="control-label" for="input-status"><span><?php echo $text_status; ?></span></label>
                  <div class="clearfix">
                    <div name="project_status" id="input-status" class="form-control">
<?php echo ($project_status==1)?$text_active:$text_deactive; ?>
                    </div>
                  </div>
                </div>
                
			  
                <div class="form-group">
                  <label class="control-label" for="input-customer-group"><span data-toggle="tooltip" title="<?php echo $help_customer_group; ?>"><?php echo $entry_customer_group; ?></span></label>
                  <div class="clearfix">
                    <div name="config_customer_group_id" id="input-customer-group" class="form-control">
                      <?php foreach ($customer_groups as $customer_group) { ?>
                      <?php if ($customer_group['customer_group_id'] == $config_customer_group_id) { ?><?php echo $customer_group['group_name']; ?>
                      <?php } ?>
                      <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label"><span>Nhóm Nationwide của KH</span></label>
                  <div class="clearfix">
                  
                    <select name="config_customer_top" id="config_customer_top" class="form-control chosen">
                      <?php foreach ($levels as $level) { ?>
                      <option value="<?php echo $level['customer_level_id']; ?>"  <?php if ($level['customer_level_id'] == $config_customer_top) { ?>selected="selected"<?php } ?>><?php echo $level['level_name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
            
            
                <div class="form-group">
                  <label class="control-label"><span>Nhóm RSM của KH</span></label>
                  <div class="clearfix">
                  
                    <select name="config_customer_rsm" id="config_customer_rsm" class="form-control chosen">
                      <?php foreach ($levels as $level) { ?>
                      <option value="<?php echo $level['customer_level_id']; ?>"  <?php if ($level['customer_level_id'] == $config_customer_rsm) { ?>selected="selected"<?php } ?>><?php echo $level['level_name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label"><span>Nhóm Cat/ASM của KH</span></label>
                  <div class="clearfix">
                  
                    <select name="config_customer_asm" id="config_customer_asm" class="form-control chosen">
                      <?php foreach ($levels as $level) { ?>
                      <option value="<?php echo $level['customer_level_id']; ?>"  <?php if ($level['customer_level_id'] == $config_customer_asm) { ?>selected="selected"<?php } ?>><?php echo $level['level_name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
            
                  
                <div class="form-group">
                  <label class="control-label"><span>Nhóm Sup của KH - gắn với cửa hàng</span></label>
                  <div class="clearfix">
                  
                    <select name="config_customer_sup" id="config_customer_sup" class="form-control chosen">
                      <?php foreach ($levels as $level) { ?>
                      <option value="<?php echo $level['customer_level_id']; ?>"  <?php if ($level['customer_level_id'] == $config_customer_sup) { ?>selected="selected"<?php } ?>><?php echo $level['level_name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                
              <div class="form-group required">
                <label class="control-label" for="input-url"><span data-toggle="tooltip" data-html="true" title="<?php echo htmlspecialchars($help_url); ?>"><?php echo $entry_url; ?></span></label>
                <div class="form-line clearfix">
                    <input type="text" name="config_url" value="<?php echo $config_url; ?>" placeholder="<?php echo $entry_url; ?>" id="input-url" class="form-control" />
                  <?php if ($error_url) { ?>
                  <div class="text-danger"><?php echo $error_url; ?></div>
                  <?php } ?>
                </div>
              </div>
              
                  
              <div class="form-group">
                <label class="control-label" for="input-ssl"><span data-toggle="tooltip" data-html="true" title="<?php echo $help_ssl; ?>"><?php echo $entry_ssl; ?></span></label>
                <div class="form-line clearfix">
                    <input type="text" name="config_ssl" value="<?php echo $config_ssl; ?>" placeholder="<?php echo $entry_ssl; ?>" id="input-ssl" class="form-control" />
                </div>
              </div>
                  
              
              
              
               <div class="form-group">
                <label class="control-label" for="project_source"><?php echo $entry_source_path; ?></label>
                <div class="clearfix">
                
                  <div class="row">
                  <div class="col-sm-6">
                  <select name="project_source" id="project_source" class="form-control chosen">
                    <?php foreach ($sources as $source) { ?>
                    <option value="<?php echo $source['value']; ?>" <?php if ($source['value'] == $project_source) { ?>selected="selected"<?php } ?>><?php echo $source['label']; ?></option>
                    <?php } ?>
                  </select>
                   </div><!--//col --> 
                  <div class="col-sm-6">
                <div class="input-group">
                    <input name="copy_to" id="copy_to" type="text" class="form-control" placeholder="Or copy to new folder...">
                    <a onclick="javascript:clone_source();" class="input-group-addon" data-toggle="tooltip" title="Copy now">
                      <i class="fa fa-copy"></i>
                    </a>
                  </div>
                  
                  
                   </div><!--//col --> 
                   </div><!--//row --> 
                  
                  
              <div class="form-group">
                <label class="control-label"><span data-toggle="tooltip" title="<?php echo $help_secure; ?>"><?php echo $entry_secure; ?></span></label>
                <div class="clearfix">
                    
                    <input type="radio" name="config_secure" value="1" <?php if ($config_secure) { ?>checked="checked"<?php } ?> id="config_secure1"/>
                  <label for="config_secure1" class="radio-inline">
                    <?php echo $text_yes; ?>
                   
                  </label>
                 
                    <input type="radio" name="config_secure" value="0"  <?php if (!$config_secure) { ?>checked="checked"<?php } ?> id="config_secure0"/>
                    <label for="config_secure0" class="radio-inline"> <?php echo $text_no; ?>
                    
                  </label>
                </div>
              </div>
              
              
                <div class="form-group">
                  <label class=" control-label"><?php echo $entry_error_display; ?></label>
                  <div class="clearfix">
                   
                    <input type="radio" name="config_error_display" id="config_error_display1" value="1"  <?php if ($config_error_display) { ?>checked="checked"<?php } ?>/>
                    <label for="config_error_display1" class="radio-inline">
                    <?php echo $text_yes; ?>
                    </label>
                    
                    <input type="radio" name="config_error_display" id="config_error_display0" value="0"  <?php if (!$config_error_display) { ?>checked="checked"<?php } ?> />
                    <label for="config_error_display0" class="radio-inline"><?php echo $text_no; ?>
                    </label>
                  </div>
                </div>
              
              <div class="form-group required">
                <label class="control-label" for="input-user_id"><?php echo $text_user_pl; ?></label>
                <div class="form-line clearfix">
                    <input type="text" name="config_user" value="<?php echo $config_user; ?>" placeholder="<?php echo $text_user_pl; ?>" id="input-user_id" class="form-control" />
                  <input type="hidden" name="config_project_pl" value="<?php echo $config_project_pl; ?>"/>
                  <?php if ($error_user_id) { ?>
                  <div class="text-danger"><?php echo $error_user_id; ?></div>
                  <?php } ?>
                </div>
              </div>
                  
                  </div>
              </div>
      </div><!--body -->
	  </div><!--box -->
    
       <div class="box">
      <div class="box-header">
        <h2 class="box-title"> <?php echo $text_database; ?></h3>
      </div>
      <div class="panel-body">
	    <div class="alert alert-info"><?php echo $help_database;?></div>
      
                  <div class="row">
                  <div class="col-sm-6">
       <div class="form-group required">
                <label class="control-label" for="config_db_hostname">DB hostname:</label>
                <div class="form-line clearfix">
                    <input type="text" name="config_db_hostname" value="<?php echo $config_db_hostname; ?>" id="config_db_hostname" class="form-control"/>
                   
                </div>
              </div>
              
                   </div><!--//col --> 
                  <div class="col-sm-6">
      
              <div class="form-group required">
                <label class="control-label" for="project_db"><span><?php echo $entry_dbname; ?></span></label>
                <div class="clearfix">
                <div class="input-group">
					<input type="text" name="project_db" value="<?php echo $project_db; ?>" id="project_db" class="form-control"/>
                   <a onclick="javascript:checkdb();" class="input-group-addon" data-toggle="tooltip" title="<?php echo $text_check_db;?>">
                      <i class="fa fa-legal "></i>
                    </a>
                  </div>
                  <?php if ($error_db) { ?>
                  <div class="text-danger"><?php echo $error_db; ?></div>
                  <?php } ?>
                </div>
              </div>
              
                  </div><!--//col --> 
                   </div><!--//row --> 
                   
                   <?php if(in_array($user_id, $config_user_trusted)){?>
                  <div class="row ">
               <div class="col-sm-6">
                  
              <div class="form-group required">
                <label class="control-label" for="project_db_user"><span><?php echo $entry_dbuser; ?></span></label>
                <div class="form-line clearfix">
                    <input type="text" name="project_db_user" value="<?php echo $project_db_user; ?>" placeholder="<?php echo $entry_dbuser; ?>" id="project_db_user" class="form-control" />
                </div>
              </div>
              
                   </div><!--//col --> 
                  <div class="col-sm-6">
                  
              <div class="form-group required">
                <label class="control-label" for="project_db_pass"><span><?php echo $entry_dbpass; ?></span></label>
                <div class="form-line clearfix">
                    <input type="text" name="project_db_pass" value="<?php echo $project_db_pass; ?>" placeholder="<?php echo $entry_dbpass; ?>" id="project_db_pass" class="form-control" />
                </div>
              </div>
                  </div><!--//col --> 
                   </div><!--//row --> 
                 <?php } ?>  
                   
                   
               <div class="form-group required">
                <label class=" control-label" for="input-error-filename">Error file name</label>
                <div class="form-line clearfix">
                  <input type="text" name="config_error_filename" value="<?php echo $config_error_filename; ?>" placeholder="Error file name" id="input-error-filename" class="form-control" />
                  <?php if ($error_filename) { ?>
                  <div class="text-danger"><?php echo $error_filename; ?></div>
                  <?php } ?>
                </div>
              </div>
              
              
      </div><!--body -->
	  </div><!--box -->
      
      
      <div class="box">
      <div class="box-header">
        <h2 class="box-title"> <?php echo $text_design; ?></h3>
      </div>
      <div class="panel-body">
      
       <div class="form-group">
                  <label class="control-label" for="input-config_color_mode"><span>Color Mode</span></label>
                  <div class="clearfix">
                    <select name="config_color_mode" id="input-config_color_mode" class="form-control chosen">
                      <?php foreach ($color_modes as $k =>$lb) { ?>
<option value="<?php echo $k;?>" <?php if ($config_color_mode==$k){?>selected="selected"<?php } ?>><?php echo $lb; ?></option>
                    <?php } ?>
                    </select>
                  </div>
                </div>
                
                
       <div class="form-group">
                  <label class="control-label" for="input-config_nav"><span><?php echo $text_nav_style; ?></span></label>
                  <div class="clearfix">
                    <select name="config_nav" id="input-config_nav" class="form-control chosen">
<option value="nav_sidebar" <?php if ($config_nav=='nav_sidebar'){?>selected="selected"<?php } ?>><?php echo $text_nav_sidebar; ?></option>
<option value="nav_top" <?php if ($config_nav=='nav_top'){?>selected="selected"<?php } ?>><?php echo $text_nav_top; ?></option>
                    </select>
                  </div>
                </div>
                
                
                <div class="form-group required">
                  <label class=" control-label" for="config_limit_project"><?php echo $entry_limit_project; ?></label>
                  <div class="form-line clearfix">
                    <input type="text" name="config_limit_project" value="<?php echo $config_limit_project; ?>" placeholder="<?php echo $entry_limit_project; ?>" id="entry_limit_project" class="form-control" />
                    <?php if ($error_limit_project) { ?>
                    <div class="text-danger"><?php echo $error_limit_project; ?></div>
                    <?php } ?>
                  </div>
                </div>
                
                <div class="form-group required">
                  <label class=" control-label" for="config_limit_ajax"><?php echo $entry_limit_ajax; ?></label>
                  <div class="form-line clearfix">
                    <input type="text" name="config_limit_ajax" value="<?php echo $config_limit_ajax; ?>" placeholder="<?php echo $entry_limit_ajax; ?>" id="entry_limit_ajax" class="form-control" />
                    <?php if ($error_limit_ajax) { ?>
                    <div class="text-danger"><?php echo $error_limit_ajax; ?></div>
                    <?php } ?>
                  </div>
                </div>
                
                
                
      </div><!--panel-body --> 
      </div><!--box -->
      
       
 <div class="box">
      <div class="box-header">
        <h2 class="box-title"> <?php echo $text_qc_code; ?></h3>
      </div>
      <div class="panel-body">
       <?php foreach ($codes as $code) { ?>
                <div class="form-group">
                  <label class=" control-label"><?php echo $code['code_name']; ?></label>
                  <div class="clearfix">
                  <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>/ <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                   <div class="well well-sm" style="height: 150px; overflow: auto;">
                      <?php foreach ($code['problem'] as $problem) { ?>
                      <div class="xcheckbox">
                       
<input type="checkbox" name="config_qc_code[<?php echo $problem['problem_id']; ?>]" value="<?php echo $problem['problem_id']; ?>" <?php if (in_array($problem['problem_id'], $config_qc_code)) { ?>checked="checked"<?php } ?> id="<?php echo $problem['problem_id']; ?>"/>
 <label for="<?php echo $problem['problem_id']; ?>"><?php echo $problem['problem_name']; ?>
                        </label>
                      </div>
                      <?php } ?>
                    </div>
                    <?php if ($error_qc_code) { ?>
                    <div class="text-danger"><?php echo $error_qc_code; ?></div>
                    <?php } ?>
                    
                  </div>
                </div>
             <?php } ?>   
                
      </div><!--panel-body --> 
      </div><!--box -->
        <div class="box">
      <div class="box-header">
        <h2 class="box-title"> Code bỏ qua </h3>
        <div class="pull-right box-tools"> </div>
      </div>
      <div class="panel-body">
      
                  <label class=" control-label">Code không tính lỗi cho NV</label>
                  <div class="clearfix">
                  <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>/ <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                   <div class="well well-sm" style="height: 150px; overflow: auto;">
                      <?php foreach ($problems as $problem) { ?>
                      <div class="xcheckbox">
                       
<input type="checkbox" name="config_ignore_code[<?php echo $problem['problem_id']; ?>]" value="<?php echo $problem['problem_id']; ?>" 
<?php if (in_array($problem['problem_id'], $config_ignore_code)) { ?>checked="checked"<?php } ?> />
 <label for="<?php echo $problem['problem_id']; ?>"><?php echo $problem['problem_id']; ?> - <?php echo $problem['problem_name']; ?>
                        </label>
                      </div>
                      <?php } ?>
                    </div>
                    
                  </div>
             
                
      </div><!--body -->
	  </div><!--box -->
      
      
	   <div class="box">
      <div class="box-header">
        <h2 class="box-title"> Thiết lập Notes</h3>
        <div class="pull-right box-tools"> </div>
      </div>
      <div class="panel-body">
      <div class="form-group">
                  <label class=" control-label">Loại note trong dự án</label>
                  <div class="clearfix">
                  <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>/ <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                   <div class="well well-sm" style="height: 250px; overflow: auto;">
                      <?php foreach ($note_types as $type) { ?>
                      <div class="xcheckbox">
                       
<input type="checkbox" name="config_note[<?php echo $type['note_id']; ?>]" value="<?php echo $type['note_id']; ?>" <?php if (in_array($type['note_id'], $config_note)) { ?>checked="checked"<?php } ?> id="ni<?php echo $type['note_id']; ?>"/> 
<label for="ni<?php echo $type['note_id']; ?>"><?php echo $type['note_id']; ?> -  <?php echo $type['note_name']; ?>
                        </label>
                      </div>
                      <?php } ?>
                    </div>
                    <?php if ($error_note) { ?>
                    <div class="text-danger"><?php echo $error_note; ?></div>
                    <?php } ?>
                    
                  </div>
                </div>
                
      </div><!--body -->
	  </div><!--box -->
       <div class="box">
            <div class="box-header">
              <h2 class="box-title"> Thiết lập  Khảo sát </h3>
            </div>
            <div class="panel-body">
            
                
       <div class="form-group">
                  <label class="control-label" for="input-config_survey_general"><span>Khảo sát Tổng quan</span></label>
                  <div class="clearfix">
                    <select name="config_survey_general" id="input-config_survey_general" class="form-control chosen">
                    <?php foreach ($surveys as $survey) {?>
<option value="<?php echo $survey['survey_id']; ?>" <?php if ($config_survey_general==$survey['survey_id']){?>selected="selected"<?php } ?>><?php echo $survey['survey_id']; ?> -  <?php echo $survey['survey_name']; ?></option>
                      
                    <?php } ?>
                    </select>
                  </div>
                </div>
                
                
                
            	<label class="control-label">Khảo sát trưng bày</label>
              <div class="form-group">
                <div class="clearfix">
                  <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>/ <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                 
                  <div class="well well-sm" style="height: 250px; overflow: auto;">
                    <?php foreach ($surveys as $survey) {?>
                    <div class="xcheckbox">
                     
                      <input type="checkbox" name="config_survey_audit[<?php echo $survey['survey_id']; ?>]" value="<?php echo $survey['survey_id']; ?>" <?php if (in_array($survey['survey_id'], $config_survey_audit)) { ?>checked="checked"<?php } ?> id="u<?php echo $survey['survey_id']; ?>"/> 
                      <label for="u<?php echo $survey['survey_id']; ?>"><?php echo $survey['survey_id']; ?> -  <?php echo $survey['survey_name']; ?>
                      </label>
                    </div>
                    <?php } ?>
                  </div>
                  
                </div>
              </div>
            
            	<label class="control-label">Khảo sát ADHOC</label>
                  <div class="form-group">
                <div class="clearfix">
                  <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>/ <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                 
                  <div class="well well-sm" style="height: 250px; overflow: auto;">
                    <?php foreach ($surveys as $survey) {?>
                    <div class="xcheckbox">
                     
                      <input type="checkbox" name="config_survey_adhoc[<?php echo $survey['survey_id']; ?>]" value="<?php echo $survey['survey_id']; ?>" <?php if (in_array($survey['survey_id'], $config_survey_adhoc)) { ?>checked="checked"<?php } ?> id="u<?php echo $survey['survey_id']; ?>"/> 
                      <label for="u<?php echo $survey['survey_id']; ?>"><?php echo $survey['survey_id']; ?> -  <?php echo $survey['survey_name']; ?>
                      </label>
                    </div>
                    <?php } ?>
                  </div>
                  
                </div>
              </div>
            </div>
            <!--panel-body -->
          </div>
          <!--box -->
          <div class="box">
      <div class="box-header">
        <h2 class="box-title">Các phiên bản App khả dụng</h3>
      </div>
      <div class="panel-body">
       <div class="form-group">
      		 <div class="input-group">
                <input type="text" name="new_version" value="" placeholder="Thêm version"class="form-control" />
                    
                  <a type="button" onclick="addVersion();" class="input-group-addon"><i class="fa fa-plus"></i> Add</a>
                    
                    </div>
                    
                    <br/> 
                <div id="app_version" class="well well-sm" style="height: 350px; overflow: auto;">
                  <?php 
                  if(!empty($config_app_versions)){
                  foreach ($config_app_versions as $version) { ?>
                  <div class="item-box">
                    <?php echo $version; ?>
                    <button type="button" class="btn btn-xs btn-danger pull-right"><i class="fa fa-minus-circle"></i></button>
                    <input type="hidden" name="config_app_versions[]" value="<?php echo $version; ?>" />
                  </div>
                  <?php } ?>
                  <?php } ?>
                </div>
            </div>
      
      </div><!--panel-body --> 
      </div><!--box -->  
          
          
</div><!--col -->
<div class="col-md-6">

	   <div class="box">
      <div class="box-header">
        <h2 class="box-title"> <?php echo $text_project; ?></h3>
        <div class="pull-right box-tools"> </div>
      </div>
      <div class="panel-body">
	  
                  <div class="row">
                  <div class="col-sm-12">
                  
              
              <div class="form-group required">
                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                <div class="form-line clearfix">
                    <div class="form-control"> <?php echo $project_name; ?></div>
                
                </div>
              </div>
                   </div><!--//col --> 
                   
                   </div><!--//row --> 
                   
                   
                   
                  <div class="row">
                  <div class="col-sm-6">
                  
               <div class="form-group required">
                <label class="control-label" for="project_code"><?php echo $entry_code; ?></label>
                <div class="form-line clearfix">
                    <input type="text" name="project_code" value="<?php echo $project_code; ?>" placeholder="<?php echo $entry_code; ?>" id="project_code" class="form-control" />
                 
                </div>
              </div>
              
              
                   </div><!--//col --> 
                  <div class="col-sm-6">
                  
              
       <div class="form-group">
                  <label class="control-label" for="input-project_type"><span><?php echo $text_project_type; ?></span></label>
                  <div class="clearfix">
                    <div name="project_type" id="input-project_type" class="form-control">
<?php echo ($project_type==0)?$text_fulltime:$text_parttime;  ?>
                    </div>
                  </div>
                </div>
                
                
                 </div><!--//col --> 
                   </div><!--//row --> 
                    
               <div class="form-group required">
                <label class="control-label" for="config_project_folder"><?php echo $text_project_folder; ?></label>
                <div class="form-line clearfix">
                    <input type="text" name="config_project_folder" value="<?php echo $config_project_folder; ?>" placeholder="<?php echo $text_project_folder; ?>" id="config_project_folder" class="form-control" />
                   <?php if ($error_project_folder) { ?>
                            <div class="text-danger"><?php echo $error_project_folder; ?></div>
                          <?php } ?>
                </div>
              </div>
              
                   
                  <div class="row">
                  <div class="col-sm-12">
              
              <div class="form-group required">
                <label class="control-label" for="input-meta-title"><?php echo $entry_meta_title; ?></label>
                <div class="form-line clearfix">
                    <input type="text" name="config_meta_title" value="<?php echo $config_meta_title; ?>" placeholder="<?php echo $entry_meta_title; ?>" id="input-meta-title" class="form-control" />
                  <?php if ($error_meta_title) { ?>
                  <div class="text-danger"><?php echo $error_meta_title; ?></div>
                  <?php } ?>
                </div>
              </div>
                 </div><!--//col --> 
                   </div><!--//row --> 
                   
                      
              
                  <div class="row">
                  <div class="col-sm-6">
                  
              <div class="form-group">
                <label class="control-label" for="input-logo"><?php echo $entry_logo; ?></label>
                <div class="clearfix"><a href="" id="thumb-logo" data-toggle="image" class="img-thumbnail"><img src="<?php echo $logo; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                  <input type="hidden" name="config_logo" value="<?php echo $config_logo; ?>" id="input-logo" />
                </div>
              </div>
              
                   </div><!--//col --> 
                  <div class="col-sm-6">
                  
              <div class="form-group">
                <label class="control-label" for="input-icon"><span data-toggle="tooltip" title="<?php echo $help_icon; ?>"><?php echo $entry_icon; ?></span></label>
                <div class="clearfix"><a href="" id="thumb-icon" data-toggle="image" class="img-thumbnail"><img src="<?php echo $icon; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                  <input type="hidden" name="config_icon" value="<?php echo $config_icon; ?>" id="input-icon" />
                </div>
              </div>
              
                   </div><!--//col --> 
                   </div><!--//row --> 
                   
                   
			  
              <div class="form-group">
                <label class="control-label" for="input-layout"><?php echo $entry_layout; ?></label>
                <div class="clearfix">
                  <select name="config_layout_id" id="input-layout" class="form-control chosen">
                    <?php foreach ($layouts as $layout) { ?>
<option value="<?php echo $layout['layout_id']; ?>" <?php if ($layout['layout_id'] == $config_layout_id) { ?>selected="selected"<?php } ?>><?php echo $layout['layout_name']; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
			  
			  
              <div class="form-group">
                <label class="control-label" for="input-language"><?php echo $entry_language; ?></label>
                <div class="clearfix">
                  <select name="config_language" id="input-language" class="form-control chosen">
                    <?php foreach ($languages as $language) { ?>
<option value="<?php echo $language['code']; ?>" <?php if ($language['code'] == $config_language) { ?>selected="selected"<?php } ?>><?php echo $language['name']; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              
                
              <div class="form-group">
                  <label class=" control-label" for="config_api"><?php echo $entry_config_api; ?></label>
                  <div class="clearfix">
                  
                <div class="input-group">
                    <input name="config_api" value="<?php echo $config_api; ?>" placeholder="<?php echo $entry_config_api; ?>" id="config_api" class="form-control"> <a type="button" id="button-generate" class="input-group-addon"><i class="fa fa-refresh"></i> <?php echo $button_generate; ?></a>
                    
                    </div>
                    <?php if ($error_api) { ?>
                    <div class="text-danger"><?php echo $error_api; ?></div>
                    <?php } ?>
                     <br />
                 
                  </div>
                </div>
                
              <div class="form-group">
                  <label class=" control-label" for="config_fcm_api_key">FCM api key</label>
                  <div class="clearfix">
                    <input name="config_fcm_api_key" value="<?php echo $config_fcm_api_key; ?>" placeholder="<?php echo $entry_config_api; ?>" id="config_fcm_api_key" class="form-control">
                    
                  </div>
                </div> 
			  
              <div class="form-group">
                  <label class=" control-label" for="config_test_users">ID nhân viên test</label>
                  <div class="clearfix">
                    <textarea name="config_test_users" rows="5" id="config_test_users" class="form-control"><?php echo $config_test_users; ?></textarea>
                  </div>
                </div> 
			  
      </div><!--body -->
	  </div><!--box -->
      
       <div class="box">
            <div class="box-header">
              <h2 class="box-title">Cửa hàng</h3>
            </div>
            <div class="panel-body">
              <div class="form-group">
                <label class=" control-label">Cửa hàng cập nhật
               <?php
               
               ?>
                </label>
				<input type="hidden" name="config_fields_update" value=""/>
                <div class="clearfix">
                  <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>/ <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                 
                  <div class="well well-sm" style="height: 250px; overflow: auto;">
                    <?php foreach ($fields_update as $field=>$value) {?>
                    <div class="xcheckbox">
                      
                    
                     
                       <input type="hidden" name="config_fields_update[<?php echo $field; ?>][label]" value="<?php echo $value['label']; ?>"/> 
                      <input type="checkbox" name="config_fields_update[<?php echo $field; ?>][key]" value="<?php echo $value['key']; ?>" 
                      <?php if (isset($config_fields_update[$field]['key'])) { ?>checked="checked"<?php } ?> id="fi_<?php echo $field; ?>"/> 
                      <label for="fi_<?php echo $field; ?>"><?php echo $value['label']; ?> (<?php echo $value['key']; ?>)
                      </label>
                      
                     
                    </div>
                    <?php } ?>
                  </div>
                  
                </div>
              </div>
            
            </div>
            <!--panel-body -->
          </div>
          <!--box -->
          
       <div class="box">
            <div class="box-header">
              <h2 class="box-title"> <?php echo $text_group_sign; ?></h3>
            </div>
            <div class="panel-body">
              <div class="form-group">
                <label class=" control-label"><?php echo $help_group_sign; ?></label>
                <div class="clearfix">
                  <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>/ <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                 
                  <div class="well well-sm" style="height: 250px; overflow: auto;">
                    <?php foreach ($user_groups as $group) { 
                    //$group['user_group_id']!=1&&
                    if($group['user_group_id']!=$config_user_staff&&$group['user_group_id']!=$config_user_cb){
                    ?>
                    <div class="xcheckbox">
                     
                      <input type="checkbox" name="config_group_sign[<?php echo $group['user_group_id']; ?>]" value="<?php echo $group['user_group_id']; ?>" <?php if (in_array($group['user_group_id'], $config_group_sign)) { ?>checked="checked"<?php } ?> id="u<?php echo $group['user_group_id']; ?>"/> 
                      <label for="u<?php echo $group['user_group_id']; ?>"><?php echo $group['user_group_id']; ?> -  <?php echo $group['group_name']; ?>
                      </label>
                    </div>
                    <?php } ?>
                    <?php } ?>
                  </div>
                  
                  <?php if ($error_group_sign) { ?>
                  <div class="text-danger"><?php echo $error_group_sign; ?></div>
                  <?php } ?>
                </div>
              </div>
            
            </div>
            <!--panel-body -->
          </div>
          <!--box -->
     
      <div class="box">
      <div class="box-header">
        <h2 class="box-title"> <?php echo $text_image_group; ?></h3>
      </div>
      <div class="panel-body">
      
                <div class="form-group">
                  <label class=" control-label">Hình ảnh trong dự án</label>
                  <div class="clearfix">
                  <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>/ <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                   <div class="well well-sm" style="height: 250px; overflow: auto;">
                      <?php foreach ($image_types as $type) { ?>
                      <div class="xcheckbox">
                       
<input type="checkbox" name="config_project_images[<?php echo $type['image_type_id']; ?>]" value="<?php echo $type['image_type_id']; ?>" <?php if (in_array($type['image_type_id'], $config_project_images)) { ?>checked="checked"<?php } ?> id="pi<?php echo $type['image_type_id']; ?>"/> 
<label for="pi<?php echo $type['image_type_id']; ?>"><?php echo $type['image_type_id']; ?> -  <?php echo $type['image_type']; ?>
                        </label>
                      </div>
                      <?php } ?>
                    </div>
                    <?php if ($error_image_project) { ?>
                    <div class="text-danger"><?php echo $error_image_project; ?></div>
                    <?php } ?>
                    
                  </div>
                </div>
                
                
                
       <div class="form-group">
                  <label class="control-label" for="input-config_image_overview"><span>Store Overview</span></label>
                  <div class="clearfix">
                    <select name="config_image_overview" id="input-config_image_overview" class="form-control chosen">
                      <?php foreach ($image_types as $type) { ?>
                     <?php if(in_array($type['image_type_id'],$config_project_images)){?>
<option value="<?php echo $type['image_type_id']; ?>" <?php if ($config_image_overview==$type['image_type_id']){?>selected="selected"<?php } ?>><?php echo $type['image_type']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                
                
       <div class="form-group">
                  <label class="control-label" for="input-config_image_selfie"><span>Tác phong</span></label>
                  <div class="clearfix">
                    <select name="config_image_selfie" id="input-config_image_selfie" class="form-control chosen">
                      <?php foreach ($image_types as $type) { ?>
                     <?php if(in_array($type['image_type_id'],$config_project_images)){?>
<option value="<?php echo $type['image_type_id']; ?>" <?php if ($config_image_selfie==$type['image_type_id']){?>selected="selected"<?php } ?>><?php echo $type['image_type']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                
                
                
                <div class="form-group">
                  <label class=" control-label"><?php echo $text_image_store; ?></label>
				<input type="hidden" name="config_image_store" value=""/>
                  <div class="clearfix">
                  <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>/ <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                   <div class="well well-sm" style="height: 250px; overflow: auto;">
                      <?php foreach ($image_types as $type) { ?>
                     <?php if(in_array($type['image_type_id'],$config_project_images)){?>
                      
                      <div class="xcheckbox">
<input type="checkbox" name="config_image_store[<?php echo $type['image_type_id']; ?>]" value="<?php echo $type['image_type_id']; ?>" 
<?php if (!empty($config_image_store)&&in_array($type['image_type_id'], $config_image_store)) { ?>checked="checked"<?php } ?> id="s<?php echo $type['image_type_id']; ?>"/> 
<label for="s<?php echo $type['image_type_id']; ?>"><?php echo $type['image_type_id']; ?> -  <?php echo $type['image_type']; ?>
                        </label>
                      </div>
                      <?php } ?>
                      <?php } ?>
                    </div>
                  </div>
                </div>
                
                <div class="form-group">
                  <label class=" control-label"><?php echo $text_image_audit; ?></label>
                  
				<input type="hidden" name="config_image_audit" value=""/>
                  <div class="clearfix">
                  <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>/ <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                   <div class="well well-sm" style="height: 250px; overflow: auto;">
                      <?php foreach ($image_types as $type) { ?>
                     <?php if(in_array($type['image_type_id'],$config_project_images)){?>
                      <div class="xcheckbox">
                        
<input type="checkbox" name="config_image_audit[<?php echo $type['image_type_id']; ?>]" value="<?php echo $type['image_type_id']; ?>" 
<?php if (!empty($config_image_audit)&&in_array($type['image_type_id'], $config_image_audit)) { ?>checked="checked"<?php } ?> id="z<?php echo $type['image_type_id']; ?>"/>
<label for="z<?php echo $type['image_type_id']; ?>"><?php echo $type['image_type_id']; ?> -  <?php echo $type['image_type']; ?>
                        </label>
                      </div>
                      <?php } ?>
                      <?php } ?>
                    </div>
                    
                  </div>
                </div><!--// --> 
                
                <div class="form-group">
                  <label class=" control-label"><?php echo $text_image_coupon; ?></label>
				<input type="hidden" name="config_image_coupon" value=""/>
                  <div class="clearfix">
                  <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>/ <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                   <div class="well well-sm" style="height: 250px; overflow: auto;">
                      <?php foreach ($image_types as $type) { ?>
                     <?php if(in_array($type['image_type_id'],$config_project_images)){?>
                      <div class="xcheckbox">
                        
<input type="checkbox" name="config_image_coupon[<?php echo $type['image_type_id']; ?>]" value="<?php echo $type['image_type_id']; ?>" 
<?php if (!empty($config_image_coupon)&&in_array($type['image_type_id'], $config_image_coupon)) { ?>checked="checked"<?php } ?> id="cp<?php echo $type['image_type_id']; ?>"/>
<label for="cp<?php echo $type['image_type_id']; ?>"><?php echo $type['image_type_id']; ?> -  <?php echo $type['image_type']; ?>
                        </label>
                      </div>
                      <?php } ?>
                      <?php } ?>
                    </div>
                    
                  </div>
                </div><!--// --> 
                
				<div class="form-group">
                <label class=" control-label"><?php echo $text_image_ignore; ?></label>
                <div class="clearfix">
                  <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a>/ <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                  <div class="well well-sm" style="height: 250px; overflow: auto;">
                    <?php foreach ($image_types as $type) { ?>
                     <?php if(in_array($type['image_type_id'],$config_project_images)){?>
                    <div class="xcheckbox">
                      
                      <input type="checkbox" name="config_image_ignore[<?php echo $type['image_type_id']; ?>]" value="<?php echo $type['image_type_id']; ?>" <?php if (in_array($type['image_type_id'], $config_image_ignore)) { ?>checked="checked"<?php } ?> id="i<?php echo $type['image_type_id']; ?>"/>
                      <label for="i<?php echo $type['image_type_id']; ?>"><?php echo $type['image_type_id']; ?> -  <?php echo $type['image_type']; ?>
                      </label>
                    </div>
                    <?php } ?>
                    <?php } ?>
                  </div>
                </div>
              </div><!-- //form-g-->
			  
                
      </div><!--panel-body --> 
      </div><!--box -->
      
     
      <div class="box">
      <div class="box-header">
        <h2 class="box-title"> Coupon Status</h3>
      </div>
      <div class="panel-body">
      
       <div class="form-group">
                  <label class="control-label" for="config_coupon_valid" title="config_coupon_valid"><span>Hợp lệ</span></label>
                  <div class="clearfix">
                    <select name="config_coupon_valid" id="config_coupon_valid" class="form-control chosen">
                    <option value="0" <?php if ($config_coupon_valid==0){?>selected="selected"<?php } ?>> --0--</option>
                      <?php if (!empty($coupon_statuses)) { ?>
                      <?php foreach ($coupon_statuses as $coupon_status) { ?>
<option value="<?php echo $coupon_status['coupon_status_id']; ?>" <?php if ($config_coupon_valid==$coupon_status['coupon_status_id']){?>selected="selected"<?php } ?>><?php echo $coupon_status['coupon_status_id']; ?> - <?php echo $coupon_status['coupon_status_name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                
       <div class="form-group">
                  <label class="control-label" for="config_coupon_wrong_distributor" title="config_coupon_wrong_distributor"><span>Sai nhà phân phối</span></label>
                  <div class="clearfix">
                    <select name="config_coupon_wrong_distributor" id="config_coupon_wrong_distributor" class="form-control chosen">
                    <option value="0" <?php if ($config_coupon_wrong_distributor==0){?>selected="selected"<?php } ?>> --0--</option>
                      <?php if (!empty($coupon_statuses)) { ?>
                      <?php foreach ($coupon_statuses as $coupon_status) { ?>
<option value="<?php echo $coupon_status['coupon_status_id']; ?>" <?php if ($config_coupon_wrong_distributor==$coupon_status['coupon_status_id']){?>selected="selected"<?php } ?>><?php echo $coupon_status['coupon_status_id']; ?> - <?php echo $coupon_status['coupon_status_name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
      
       <div class="form-group">
                  <label class="control-label" for="config_coupon_wrong_sup" title="config_coupon_wrong_sup"><span>Sai SUP</span></label>
                  <div class="clearfix">
                    <select name="config_coupon_wrong_sup" id="config_coupon_wrong_sup" class="form-control chosen">
                    <option value="0" <?php if ($config_coupon_wrong_sup==0){?>selected="selected"<?php } ?>> --0--</option>
                      <?php if (!empty($coupon_statuses)) { ?>
                      <?php foreach ($coupon_statuses as $coupon_status) { ?>
<option value="<?php echo $coupon_status['coupon_status_id']; ?>" <?php if ($config_coupon_wrong_sup==$coupon_status['coupon_status_id']){?>selected="selected"<?php } ?>><?php echo $coupon_status['coupon_status_id']; ?> - <?php echo $coupon_status['coupon_status_name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                
       <div class="form-group">
                  <label class="control-label" for="config_coupon_duplicate" title="config_coupon_duplicate"><span>Đã sử dụng</span></label>
                  <div class="clearfix">
                    <select name="config_coupon_duplicate" id="config_coupon_duplicate" class="form-control chosen">
                    <option value="0" <?php if ($config_coupon_duplicate==0){?>selected="selected"<?php } ?>> --0--</option>
                      <?php if (!empty($coupon_statuses)) { ?>
                      <?php foreach ($coupon_statuses as $coupon_status) { ?>
<option value="<?php echo $coupon_status['coupon_status_id']; ?>" <?php if ($config_coupon_duplicate==$coupon_status['coupon_status_id']){?>selected="selected"<?php } ?>><?php echo $coupon_status['coupon_status_id']; ?> - <?php echo $coupon_status['coupon_status_name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              
       <div class="form-group">
                  <label class="control-label" for="config_coupon_has_deleted" title="config_coupon_has_deleted"><span>Đã bị hủy</span></label>
                  <div class="clearfix">
                    <select name="config_coupon_has_deleted" id="config_coupon_has_deleted" class="form-control chosen">
                    <option value="0" <?php if ($config_coupon_has_deleted==0){?>selected="selected"<?php } ?>> --0--</option>
                      <?php if (!empty($coupon_statuses)) { ?>
                      <?php foreach ($coupon_statuses as $coupon_status) { ?>
<option value="<?php echo $coupon_status['coupon_status_id']; ?>" <?php if ($config_coupon_has_deleted==$coupon_status['coupon_status_id']){?>selected="selected"<?php } ?>><?php echo $coupon_status['coupon_status_id']; ?> - <?php echo $coupon_status['coupon_status_name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                  
               
       <div class="form-group">
                  <label class="control-label" for="config_coupon_not_exist" title="config_coupon_not_exist"><span>Không tồn tại</span></label>
                  <div class="clearfix">
                    <select name="config_coupon_not_exist" id="config_coupon_not_exist" class="form-control chosen">
                    <option value="0" <?php if ($config_coupon_not_exist==0){?>selected="selected"<?php } ?>> --0--</option>
                      <?php if (!empty($coupon_statuses)) { ?>
                      <?php foreach ($coupon_statuses as $coupon_status) { ?>
<option value="<?php echo $coupon_status['coupon_status_id']; ?>" <?php if ($config_coupon_not_exist==$coupon_status['coupon_status_id']){?>selected="selected"<?php } ?>><?php echo $coupon_status['coupon_status_id']; ?> - <?php echo $coupon_status['coupon_status_name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              
               
       <div class="form-group">
                  <label class="control-label" for="config_coupon_has_paid" title="config_coupon_has_paid"><span>Đã trả thưởng</span></label>
                  <div class="clearfix">
                    <select name="config_coupon_has_paid" id="config_coupon_has_paid" class="form-control chosen">
                    <option value="0" <?php if ($config_coupon_has_paid==0){?>selected="selected"<?php } ?>> --0--</option>
                      <?php if (!empty($coupon_statuses)) { ?>
                      <?php foreach ($coupon_statuses as $coupon_status) { ?>
<option value="<?php echo $coupon_status['coupon_status_id']; ?>" <?php if ($config_coupon_has_paid==$coupon_status['coupon_status_id']){?>selected="selected"<?php } ?>><?php echo $coupon_status['coupon_status_id']; ?> - <?php echo $coupon_status['coupon_status_name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                      
                
      </div><!--panel-body --> 
      </div><!--box -->
      
       <div class="box">
      <div class="box-header">
        <h2 class="box-title"> Reward Status</h3>
      </div>
      <div class="panel-body">
      
       <div class="form-group">
                  <label class="control-label" for="config_reward_audit" title="config_coupon_valid"><span>Trả thưởng</span></label>
                  <div class="clearfix">
                    <select name="config_reward_audit" id="config_reward_audit" class="form-control chosen">
                    <option value="0" <?php if ($config_reward_audit==0){?>selected="selected"<?php } ?>> --0--</option>
                      <?php if (!empty($reward_types)) { ?>
                      <?php foreach ($reward_types as $reward_type) { ?>
<option value="<?php echo $reward_type['reward_type_id']; ?>" <?php if ($config_reward_audit==$reward_type['reward_type_id']){?>selected="selected"<?php } ?>><?php echo $reward_type['reward_type_name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
               
       <div class="form-group">
                  <label class="control-label" for="config_reward_old" title="config_reward_old"><span>Trả bù</span></label>
                  <div class="clearfix">
                    <select name="config_reward_old" id="config_reward_old" class="form-control chosen">
                    <option value="0" <?php if ($config_reward_old==0){?>selected="selected"<?php } ?>> --0--</option>
                      <?php if (!empty($reward_types)) { ?>
                      <?php foreach ($reward_types as $reward_type) { ?>
<option value="<?php echo $reward_type['reward_type_id']; ?>" <?php if ($config_reward_old==$reward_type['reward_type_id']){?>selected="selected"<?php } ?>><?php echo $reward_type['reward_type_name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>     
                
       <div class="form-group">
                  <label class="control-label" for="config_reward_sales" title="config_reward_sales"><span>Doanh số</span></label>
                  <div class="clearfix">
                    <select name="config_reward_sales" id="config_reward_sales" class="form-control chosen">
                    <option value="0" <?php if ($config_reward_sales==0){?>selected="selected"<?php } ?>> --0--</option>
                      <?php if (!empty($reward_types)) { ?>
                      <?php foreach ($reward_types as $reward_type) { ?>
<option value="<?php echo $reward_type['reward_type_id']; ?>" <?php if ($config_reward_sales==$reward_type['reward_type_id']){?>selected="selected"<?php } ?>><?php echo $reward_type['reward_type_name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                  
       <div class="form-group">
                  <label class="control-label" for="config_reward_recall" title="config_reward_recall"><span>Thu hồi</span></label>
                  <div class="clearfix">
                    <select name="config_reward_recall" id="config_reward_recall" class="form-control chosen">
                    <option value="0" <?php if ($config_reward_recall==0){?>selected="selected"<?php } ?>> --0--</option>
                      <?php if (!empty($reward_types)) { ?>
                      <?php foreach ($reward_types as $reward_type) { ?>
<option value="<?php echo $reward_type['reward_type_id']; ?>" <?php if ($config_reward_recall==$reward_type['reward_type_id']){?>selected="selected"<?php } ?>><?php echo $reward_type['reward_type_name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>     
                
                
      </div><!--panel-body --> 
      </div><!--box -->
        <div class="box">
      <div class="box-header">
        <h2 class="box-title"> NPP kiểm tra</h3>
      </div>
      <div class="panel-body">
       <div class="form-group">
                  <label class="control-label" for="config_day_valid"><span>Số ngày check hợp lệ</span></label>
                  <div class="clearfix">
                    <input name="config_day_valid" value="<?php echo $config_day_valid;?>" id="config_day_valid" class="form-control"/>
                   
                  </div>
                </div>
                
           
           
       <div class="form-group">
                  <label class="control-label" for="config_dist_valid" title="config_dist_valid"><span>Coupon hợp lệ</span></label>
                  <div class="clearfix">
                    <select name="config_dist_valid" id="config_dist_valid" class="form-control chosen">
                    <option value="0" <?php if ($config_coupon_valid==0){?>selected="selected"<?php } ?>> --0--</option>
                      <?php if (!empty($coupon_scans)) { ?>
                      <?php foreach ($coupon_scans as $coupon_scan) { ?>
<option value="<?php echo $coupon_scan['coupon_scan_id']; ?>" 
<?php if ($config_dist_valid==$coupon_scan['coupon_scan_id']){?>selected="selected"<?php } ?>>
<?php echo $coupon_scan['coupon_scan_id']; ?> - <?php echo $coupon_scan['coupon_scan_name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                
           
       <div class="form-group">
                  <label class="control-label" for="config_dist_expires" title="config_dist_expires"><span>Coupon quá hạn</span></label>
                  <div class="clearfix">
                    <select name="config_dist_expires" id="config_dist_expires" class="form-control chosen">
                    <option value="0" <?php if ($config_coupon_valid==0){?>selected="selected"<?php } ?>> --0--</option>
                      <?php if (!empty($coupon_scans)) { ?>
                      <?php foreach ($coupon_scans as $coupon_scan) { ?>
<option value="<?php echo $coupon_scan['coupon_scan_id']; ?>" 
<?php if ($config_dist_expires==$coupon_scan['coupon_scan_id']){?>selected="selected"<?php } ?>>
<?php echo $coupon_scan['coupon_scan_id']; ?> - <?php echo $coupon_scan['coupon_scan_name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
      </div><!--panel-body --> 
      </div><!--box -->   
      
       <div class="box">
      <div class="box-header">
        <h2 class="box-title">User check coupon của Nivea</h3>
      </div>
      <div class="panel-body">
       <div class="form-group">
                <input type="text" name="filter_customer" value="" placeholder="Thêm user" id="filter_customer" class="form-control" />
                <div id="check_coupon" class="well well-sm" style="height: 350px; overflow: auto;">
                  <?php foreach ($customers as $customer) { ?>
                  <?php if (in_array($customer['customer_user_id'],$config_check_coupon_user_ids)) { ?>
                  <div id="check_coupon<?php echo $customer['customer_user_id']; ?>" class="item-box">
                    <?php echo $customer['username']; ?> - <?php echo $customer['fullname']; ?>
                    <button type="button" class="btn btn-xs btn-danger pull-right"><i class="fa fa-minus-circle"></i></button>
                    <input type="hidden" name="config_check_coupon_user_ids[]" value="<?php echo $customer['customer_user_id']; ?>" />
                  </div>
                  <?php } ?>
                  <?php } ?>
                </div>
            </div>
      
      </div><!--panel-body --> 
      </div><!--box -->   
      
        
</div><!--col -->
	   </div><!--row -->
	   
	  
	  
        <div class="row">
<div class="col-md-6">

      
      
</div><!--col -->
<div class="col-md-6">
</div><!--col -->
	   </div><!--row -->
	  
      
	    <?php echo $customer_permission;?>
	    <?php echo $distributor_permission;?>
    
        </form>
  </div>
  
<div id="modal-confirm" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Bạn có chắc chắn muốn xóa sạch dữ liệu? 
        
        <button type="button" class="btn btn-success pull-right" data-dismiss="modal" aria-hidden="true"><?php echo $text_no;?></button>
        <a class="btn-modal-confirm btn btn-danger pull-right" style="margin-right:15px;"><?php echo $text_yes;?></a></h4>
      </div>
      <div class="modal-body">
        <div class="mpadding">
         <input type="text" name="hidden_key" value="" placeholder="Khóa bí mật" id="hidden_key" class="form-control"/><br/> 
          Bảng bị xóa sẽ không thể khôi phục! 
          <table class="table">
          <tr>
          <td>Bảng</td>
          <td>Chức năng</td>
          </tr>
          <?php foreach($clear_tables as $table => $label){?>
          <tr>
          <td><?php echo $table;?></td>
          <td><?php echo $label;?></td>
          </tr>
          <?php } ?>
          </table>
         
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success pull-right" data-dismiss="modal" aria-hidden="true"><?php echo $text_no;?></button>
        <a class="btn-modal-confirm btn btn-danger pull-right" style="margin-right:15px;"><?php echo $text_yes;?></a>
      </div>
    </div>
  </div>
</div>
<!--//module-modal -->
<script type="text/javascript"><!--

// filter province
function addVersion() {
   var  new_version =  $('input[name=\'new_version\']').val();
	if(new_version!=''){
		$('#app_version input[value=\''+new_version+'\']').remove();
	
		$('#app_version').append('<div class="item-box">' + new_version + '<button type="button" class="btn btn-xs btn-danger pull-right"><i class="fa fa-minus-circle"></i></button><input type="hidden" name="config_app_versions[]" value="' + new_version + '" /></div>');
		$('input[name=\'new_version\']').val('');
	}
}
$('#app_version').delegate('button', 'click', function() {
	$(this).parent().remove();
});


// filter province
$('input[name=\'filter_customer\']').autocomplete({
  'source': function(request, response) {
  if(request.length>0){
    $.ajax({
      url: 'index.php?route=project/customer/autocomplete&filter_global=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['username'] + ' - ' +item['fullname'],
            value: item['customer_user_id']
          }
        }));
      }
    });
  }
  },
  'select': function(item) {
    $('input[name=\'filter_customer\']').val('');

		$('#check_coupon' + item['value']).remove();

		$('#check_coupon').append('<div id="check_coupon' + item['value'] + '" class="item-box">' + item['label'] + '<button type="button" class="btn btn-xs btn-danger pull-right"><i class="fa fa-minus-circle"></i></button><input type="hidden" name="config_check_coupon_user_ids[]" value="' + item['value'] + '" /></div>');
  }
});

$('#province').delegate('button', 'click', function() {
	$(this).parent().remove();
});


$('#button-generate').on('click', function() {
	rand = '';

	string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

	for (i = 0; i < 32; i++) {
		rand += string[Math.floor(Math.random() * (string.length - 1))];
	}

	$('#config_api').val(rand);
});
//--></script>

  <script type="text/javascript"><!--
$('input[name=\'config_user\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=user/user/autocomplete&filter_global=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['fullname'],
						value: item['user_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'config_user\']').val(item['label']);
		$('input[name=\'config_project_pl\']').val(item['value']).after('<br><div class="alert alert-success alert-sm">Success select PL user!<button type="button" class="close" data-dismiss="alert">x</button></div>');		
	}	
});
//--></script>

  <script type="text/javascript"><!--
  function checkdb(){
	var project_code = $('#project_code').val();
	var project_source =$('#project_source').val();
	var project_db_user =$('#project_db_user').val();
	var project_db_pass =$('#project_db_pass').val();
	
  $.ajax({
			type: 'POST',
			url: 'index.php?route=project/project_settings/checkdb',
			dataType: 'json',
			data: 'project_id=<?php echo $project_id; ?>&project_code=' + project_code+'&project_source=' + project_source+'&project_db_user=' + project_db_user+'&project_db_pass=' + project_db_pass,
			beforeSend: function() {
				$('.alert').remove();
				if(project_code.length=''){
					$('#form-project').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> Project Code can\'t be empty! <button type="button" class="close" data-dismiss="alert">x</button></div>');
				}
			},
			complete: function() {				
			},
			error: function(jqXHR, textStatus, errorThrown){
                  console.log('Error: '+ errorThrown);
            },
			success: function(json) {	
				$('.alert').remove();		
				if (json['error']) {
						$('#form-project').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">x</button></div>');
				} 
				if (json['project_db']) {	
					$('#project_db').val(json['project_db']);
					$('#project_db_label').html(json['project_db']);
				}
				if (json['success']) {	
					$('#form-project').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">x</button></div>');
				}
			}
	});
  }
  
  function clone_source(){
	var copy_from = $('#project_source').val();
	var copy_to =$('#copy_to').val();
	
  $.ajax({
			url: 'index.php?route=project/project_settings/clone_source',
			type: 'POST',
			dataType: 'json',
			data: 'copy_from=' + copy_from +'&copy_to=' + copy_to,
			beforeSend: function() {
				$('.alert').remove();
				if(copy_to.length=''){
					$('#form-project').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> New name can\t be empty! <button type="button" class="close" data-dismiss="alert">x</button></div>');
				}
				if(copy_to==copy_from){
					$('#form-project').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> New name can\t be the same source name! <button type="button" class="close" data-dismiss="alert">x</button></div>');
				}
			},
			complete: function() {				
			},
			success: function(json) {	
				$('.alert').remove();		
				if (json['error']) {
						$('#form-project').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">x</button></div>');
				} 
				if (json['success']) {	
					$('#form-project').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">x</button></div>');
					
				if (json['sources'] && json['sources'] != '') {
					html = '';
					for (i = 0; i < json['sources'].length; i++) {
						html += '<option value="' + json['sources'][i]['value'] + '"';
	
						if (json['sources'][i]['value'] == copy_to) {
							html += ' selected="selected"';
						}
	
						html += '>' + json['sources'][i]['label'] + '</option>';
					}
					$('select#project_source').html(html);
				} 

			
					
				}
			}
	});
  }
  function clear_data(){
	  $('#modal-confirm').modal('show');
	  $('.btn-modal-confirm').on('click',function(e) {
			e.preventDefault();
			 $.ajax({
				  url: 'index.php?route=project/project_settings/clear_data',
				  type: 'POST',
				  dataType: 'json',
				  data: 'hidden_key=' + $('#hidden_key').val(),
				  beforeSend: function(){},
				  success: function(json) {	
					if (json['error']) {
							$('#form-project').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">x</button></div>');
					} 
					if (json['success']) {	
						$('#hidden_key').val('');
						$('#form-project').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">x</button></div>');
					
						if(json['redirect']) {
							location = json['redirect'];
						}
					}
					$('#modal-confirm').modal('hide');
				  }
			});
		});
  }
//--></script> 