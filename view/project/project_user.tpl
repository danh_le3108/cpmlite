<style>
   .badge{ font-size:13px; padding:3px 8px;}
   small.help{
   color:#999;
   }
   span.fullname{ font-size:16px; font-weight:600;}
   .img-thumbnail img{max-width:50px;}
   .box-header.active+.panel-body{
   display:block !important;
   }
   .ds_accordion .chosen-container{ 
   min-width: 250px !important;
   max-width: 250px !important;
   }
</style>
<div id="content">
   <div class="page-header">
      <div class="container-fluid">
         <div class="pull-right">
            <button type="submit" form="form-project" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
         </div>
         <h1><?php echo $heading_title; ?></h1>
      </div>
   </div>
   <div class="container-fluid">
      <!-- <div id="quick-add-user" class="box box-info">
         <div class="box-header">
           <h3 class="box-title"> <?php echo $text_add_user; ?></h3>
           <div class="pull-right">
           <!-- <button type="button" id="button-quick-add-save" class="btn btn-primary"><i class="fa fa-save"></i> <?php echo $text_add_user; ?></button>
           </div>
         </div>
         <div class="panel-body">
         
          <div class="row">
          <div class="col-sm-6">
            <div class="form-group required">
              <label class="control-label" for="input-fullname"><?php echo $text_fullname; ?></label>
              <div class="clearfix">
                <input type="text" name="fullname" value="" placeholder="<?php echo $text_fullname; ?>" id="input-fullname" class="form-control" />
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group required">
              <label class="control-label" for="input-identity_number"><?php echo $text_identity_number; ?></label>
              <div class="clearfix">
                <input type="text" name="p_identity_number" value="" placeholder="<?php echo $text_identity_number; ?>" id="input-identity_number" class="form-control" />
              </div>
            </div>
          </div>
           </div>
         
          <div class="row">
          <div class="col-sm-6">
            <div class="form-group required">
              <label class="control-label" for="input-email"><?php echo $text_email; ?></label>
              <div class="clearfix">
                <input type="text" name="email" value="" placeholder="<?php echo $text_email; ?>" id="input-email" class="form-control" />
                <?php if ($error_email) { ?>
                <div class="text-danger"><?php echo $error_email; ?></div>
                <?php } ?>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group required">
              <label class="control-label" for="input-telephone"><?php echo $text_phone; ?></label>
              <div class="clearfix">
                <input type="text" name="telephone" value="" placeholder="<?php echo $text_phone; ?>" id="input-identity_number" class="form-control" />
                <?php if ($error_telephone) { ?>
                <div class="text-danger"><?php echo $error_telephone; ?></div>
                <?php } ?>
              </div>
            </div>
          </div>
           </div>
         
         <div class="row">
          <div class="col-sm-6">
            <div class="form-group required">
              <label class="control-label" for="input-workplace"><?php echo $text_workplace; ?></label>
              <div class="clearfix">
                <select name="province_id" id="input-province_id" class="form-control chosen">
                  <option value="*"><?php echo $text_select; ?></option>
                  <?php foreach($provinces as $province){?>
                  <option value="<?php echo $province['province_id'];?>"><?php echo $province['name'];?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group required">
             <label class="control-label" for="input-workplace"><?php echo $text_user_group; ?></label>
             <div class="clearfix">
                <select name="user_group_id" id="input-user_group" class="form-control chosen">
                 <option value="0"><?php echo $text_select; ?></option>
                 <?php foreach($user_groups as $user_group){?>
                 <option value="<?php echo $user_group['user_group_id'];?>"><?php echo $user_group['group_name'];?></option>
                 <?php } ?>
               </select>
             </div>
           </div>
          </div>
         
         </div>
         </div> -->
      <!-- quick add user -->
      <?php if ($error_warning) { ?>
      <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
         <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i>
         Hướng dẫn: Khi điền data import trong Excel dùng USERCODE do một số User chuyển từ hệ thống cũ sang Username khác Usercode.
         <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <form method="post" enctype="multipart/form-data" id="form-project" class="form-horizontal">
         <div class="row">
            <div class="col-sm-3">
               <div class="box box-primary">
                  <div class="box-header with-border">
                     <h3 class="box-title"><?php echo $text_none_group_user; ?></h3>
                  </div>
                  <div class="panel-body" id="none_group_user">
                  </div>
               </div>
            </div>
            <!--//col &&$staff['group_id']>=$user_group['user_group_id'] -->
            <div class="col-sm-9 ds_accordion">
               <?php foreach($user_groups as $user_group){
                  if ($user_group['user_group_id'] < 17) { 
               ?>
               <div class="box box-primary" id="group<?php echo $user_group['user_group_id'];?>">
                  <div class="box-header with-border">
                     <h3 style="min-width:200px; float:left;" class="box-title"><?php echo $user_group['user_group_id']; ?> - <?php echo $user_group['group_name']; ?></h3>
                     <div style="max-width:300px;min-width:250px; float:left;">
                        <?php if($user_group['user_group_id']>$logged_group &&$user_group['user_group_id']!=$user_staff){?>
                        <select onchange="addUser('<?php echo $user_group['user_group_id'];?>','0','<?php echo $user_group['user_group_id']; ?>',this.options[this.selectedIndex].value);" class="form-control chosen" style="width:250px;">
                           <option value="">--<?php echo $text_add;?> <?php echo $user_group['group_description']; ?>--</option>
                           <?php foreach ($project_users as $staff) { ?>
                           <?php if($staff['user_status']==0) { ?>
                           <option value="<?php echo $staff['user_id'];?>"><?php echo $staff['fullname'];?> - <?php echo $staff['usercode'];?></option>
                           <?php } ?>
                           <?php } ?>
                        </select>
                        <?php } ?>
                     </div>
                     <div class="pull-right">
                        <button type="button" onclick="loadGroup('<?php echo $user_group['user_group_id'];?>');" data-toggle="tooltip" title="<?php echo $button_refresh;?>" class="btn btn-xs btn-primary"><i class="fa fa-refresh"></i></button>
                     </div>
                  </div>
                  <div class="panel-body">
                     <table class="table" id="user_group<?php echo $user_group['user_group_id'];?>">
                        <?php foreach ($project_users as $user) { ?>
                        <?php if($user['user_group_id']==$user_group['user_group_id'] && $user['user_status']==1){?>
                        <tbody id="user-row<?php echo $user['user_id']; ?>">
                           <tr>
                              <td width="1">
                                 <div class="img-thumbnail" title="<?php echo $user['user_id']; ?> - <?php echo $user['fullname']; ?>"><img src="<?php echo $user['thumb'];?>" /></div>
                              </td>
                              <td>
                                 <!--
                                    <small class="help"><?php echo $user['parent_fullname'];?> &raquo; </small>//--> 
                                 <span class="fullname" data-toggle="tooltip" title="#<?php echo $user['user_id'];?> - Last update: <?php echo $user['user_update'];?>"><?php echo $user['fullname'];?></span>
                                 - <small class="help">Usercode: <?php echo $user['usercode'];?></small> - <small class="help">Username: <?php echo $user['username'];?></small> 
                                 <!--  <span><?php echo $user_group['group_description'];?> - </span>//--> 
                                 <?php echo ($user['user_group_id']==$user_sup)?'<br/>'.$user['region_code']:'';?> 
                                 <?php if($user['user_group_id']==$user_staff ){?>
                                 <?php echo ($user['user_parent_id']==0)?'<br/><span class="badge bg-red">Chưa có SUP</span>':'';?> 
                                 <br />Device: <span class="badge"><?php echo $user['device_model'];?></span>
                                 <?php if($user['new_model']!=$user['device_model']&&$user['new_model']!=''){?>
                                 - New device: <span class="badge"><?php echo $user['new_model'];?> 
                                 <a onclick="approveDevice('<?php echo $user['user_group_id'];?>','<?php echo $user['user_id'];?>')" class="btn btn-xs btn-success">
                                 <?php echo $text_approved;?></a></span> 
                                 <?php } ?>
                                 <?php } ?>
                              </td>
                              <td class="text-left">
                                 <?php if($user_group['user_group_id']==$user_sup){?>
                                 <div class="input-group">
                                    <select name="region_code" id="region_code" class="form-control" onchange="updateRegion('<?php echo $user_group['user_group_id'];?>','<?php echo $user['user_id']; ?>',this.options[this.selectedIndex].value)" style="width:150px;">
                                       <option value="*"> - <?php echo $text_belong_to;?> <?php echo $text_region;?> - </option>
                                       <?php if(!empty($regions)){?>
                                       <?php foreach($regions as $region){?>
                                       <option value="<?php echo $region['region_code'];?>" <?php echo ($region['region_code']==$user['region_code'])?'selected="selected"':'';?> ><?php echo $region['region_code'];?></option>
                                       <?php } ?>
                                       <?php } ?>
                                    </select>
                                 </div>
                                 <?php } ?>
                              </td>
                              <td class="text-right"> 
                                 <span class="text-<?php echo ($user['user_status']==1)?'success':'error';?>">
                                 <?php echo $text_status;?>:
                                 <?php echo ($user['user_status']==1)?$text_active:$text_deactive;?></span>
                              </td>
                              <td class="text-right">
                                 <button type="button" onclick="deleteUser('<?php echo $user_group['user_group_id'];?>','<?php echo $user['user_id']; ?>');" data-toggle="tooltip" title="<?php echo $button_remove;?>" class="btn btn-xs btn-danger"><i class="fa fa-minus-circle"></i></button>
                              </td>
                           </tr>
                           <tr>
                              <td class="text-left"></td>
                              <td class="text-left">
                                 <!--// Nếu không phải nhóm Staff--> 
                                 <?php if($user_group['child_group_id']!= 0){?>
                                 <!--//User is DC--> 
                                 <?php if($user['user_group_id']==$user_dc){?>    
                                 <select onchange="updateManager('<?php echo $user['user_group_id']; ?>','<?php echo $user['user_id']; ?>',this.options[this.selectedIndex].value,'1');" class="form-control chosen" style="width:250px;" title="<?php echo $user_group['child_group_id'];?>">
                                    <option value="">-- <?php echo $text_manager;?> <?php echo $user_group['child_group'];?> --</option>
                                    <?php foreach ($project_users as $staff) {?>
                                    <?php if(!in_array($staff['user_id'],$user['user_managers'])&&$staff['user_group_id']==$user_group['child_group_id']){?>
                                    <option value="<?php echo $staff['user_id'];?>"><?php echo $staff['fullname'];?> - <?php echo $staff['usercode'];?></option>
                                    <?php } ?>
                                    <?php } ?>
                                 </select>
                                 <!--//not is DC--> 
                                 <?php }else{?>      
                                 <select onchange="addUser('<?php echo $user['user_group_id']; ?>','<?php echo $user['user_id']; ?>','<?php echo $user['child_group_id']; ?>',this.options[this.selectedIndex].value);" class="form-control chosen" style="width:250px;" title="<?php echo $user_group['child_group_id'];?>">
                                    <option value="">-- <?php echo $text_manager;?> <?php echo $user_group['child_group'];?> --</option>
                                    <?php foreach ($project_users as $staff) {?>
                                    <?php if($staff['user_group_id']==$user['child_group_id']&&($staff['user_parent_id']!=$user['user_id']||$staff['user_status']==0)) { ?>
                                    <option value="<?php echo $staff['user_id'];?>"><?php echo $staff['fullname'];?> - <?php echo $staff['usercode'];?></option>
                                    <?php } ?>
                                    <?php } ?>
                                 </select>
                                 <?php } ?>
                                 <!--//end not is DC--> 
                                 <?php } ?>
                              </td>
                              <td class="text-left"></td>
                              <td class="text-left"></td>
                              <td class="text-left"></td>
                           </tr>
                           <!-- Staft user here -->
                           <?php if($user_group['user_group_id']!=$user_staff){?>
                           <tr>
                              <td width="100">
                                 &raquo; <?php echo $user_group['child_group'];?>
                              </td>
                              <td class="text-left" colspan="4">
                                 <table class="table">
                                    <!--//User is DC--> 
                                    <?php if($user['user_group_id']==$user_dc){?>
                                    <?php
                                       foreach ($project_users as $staff) {  ?>
                                    <?php if(in_array($staff['user_id'],$user['user_managers'])){?>
                                    <tbody id="user-row<?php echo $staff['user_id']; ?>">
                                       <tr>
                                          <td>
                                             <small class="help"><?php echo $user['fullname'];?> &raquo; </small> 
                                             <span class="fullname" data-toggle="tooltip" title="Last update: <?php echo $staff['user_update'];?>"><?php echo $staff['fullname'];?></span>
                                             - <small class="help">Usercode: <?php echo $staff['usercode'];?></small>  -  <small class="help">Username: <?php echo $staff['username'];?></small>  
                                          </td>
                                          <td>
                                          </td>
                                          <td class="text-right"> 
                                             <span class="text-<?php echo ($staff['user_status']==1)?'success':'error';?>">
                                             <?php echo $text_status;?>:
                                             <?php echo ($staff['user_status']==1)?$text_active:$text_deactive;?></span>
                                          </td>
                                          <td class="text-right">
                                             <button type="button" onclick="updateManager('<?php echo $user['user_group_id'];?>','<?php echo $user['user_id'];?>','<?php echo $staff['user_id']; ?>','0');" data-toggle="tooltip" title="<?php echo $button_remove;?> <?php echo $staff['user_id']; ?>" class="btn btn-xs btn-danger"><i class="fa fa-minus-circle"></i></button>
                                          </td>
                                       </tr>
                                    </tbody>
                                    <?php } ?>
                                    <?php } ?>
                                    <!--//not is DC--> 
                                    <?php }else{?>
                                    <?php foreach ($project_users as $staff) {  ?>
                                    <?php if($staff['user_group_id']==$user['child_group_id']&&$staff['user_parent_id']==$user['user_id']){?>
                                    <tbody id="user-row<?php echo $staff['user_id']; ?>">
                                       <tr>
                                          <td>
                                             <!--
                                                <small class="help"><?php echo $staff['parent_fullname'];?>&raquo; </small>//--> 
                                             <span class="fullname" data-toggle="tooltip" title="Last update: <?php echo $staff['user_update'];?>"><?php echo $staff['fullname'];?></span>
                                             - <small class="help">Usercode: <?php echo $staff['usercode'];?></small> -   <small class="help">Username: <?php echo $staff['username'];?></small> 
                                             <?php if($staff['user_group_id']==$user_staff ){?>
                                             <br />Device: <span class="badge"><?php echo $staff['device_model'];?></span> <?php if($staff['new_model']!=$staff['device_model']&&$staff['new_model']!=''){?>- New device: <span class="badge"><?php echo $staff['new_model'];?> <a onclick="approveDevice('<?php echo $staff['user_group_id'];?>','<?php echo $staff['user_id'];?>')" class="btn btn-xs btn-success"><?php echo $text_approved;?></a></span> <?php } ?>
                                             <?php } ?>
                                          </td>
                                          <td>
                                          </td>
                                          <td class="text-right"> 
                                             <span class="text-<?php echo ($staff['user_status']==1)?'success':'error';?>">
                                             <?php echo $text_status;?>:
                                             <?php echo ($staff['user_status']==1)?$text_active:$text_deactive;?></span>
                                          </td>
                                          <td class="text-right">
                                             <?php if($staff['user_status']==1){?>
                                             <button type="button" onclick="deleteUser('<?php echo $user_group['user_group_id'];?>','<?php echo $staff['user_id']; ?>');" data-toggle="tooltip" title="<?php echo $button_remove;?>" class="btn btn-xs btn-danger"><i class="fa fa-minus-circle"></i></button>
                                             <?php }else{?>
                                             <button type="button" onclick="addUser('<?php echo $user['user_group_id']; ?>','<?php echo $user['user_id']; ?>','<?php echo $user['child_group_id']; ?>','<?php echo $staff['user_id']; ?>');" data-toggle="tooltip" title="<?php echo $text_reactive;?>" class="btn btn-xs btn-success"><i class="fa fa-plus-circle"></i></button>
                                             <?php } ?>
                                          </td>
                                       </tr>
                                    </tbody>
                                    <?php } ?>
                                    <?php } ?>
                                    <?php } ?>
                                    <!--//end not is DC--> 
                                 </table>
                              </td>
                           </tr>
                           <?php } ?>
                           <!-- End staft user -->
                        </tbody>
                        <?php } ?>
                        <?php } ?>
                        <tfoot></tfoot>
                     </table>
                  </div>
                  <div class="box-footer with-border"> </div>
               </div>
               <?php }} ?>
            </div>
            <!--//col -->
         </div>
         <!--//row -->
      </form>
   </div>
   <script type="text/javascript"><!--
      $(document).ready(function() {  
        $(".ds_accordion .chosen").chosen({ search_contains: true });
        $(".ds_accordion .panel-body").css("display", "none");
        $(".ds_accordion .box-header").click(function() {
          if (!$(this).hasClass("active")) {
            $('.ds_accordion .box-header').removeClass("active");
            $(this).addClass("active");
            $(this).siblings(".panel-body").css("display", "none");
          }
        });
        $('.ds_accordion >.box:first-child .box-header').trigger('click');
        });
        
        
      $('#none_group_user').load('index.php?route=project/project_user/listuser');
      
      function loadGroup(group_id){
      $('#group'+group_id).load('index.php?route=project/project_user/load_group&&user_group_id='+group_id);
      }
      function updateManager(parent_group_id,user_parent_id,user_id,handle){
      $.ajax({
        type: 'POST',
             url: 'index.php?route=project/project_user/update_manager',
             dataType: 'json',
        data: 'user_parent_id=' + user_parent_id+'&user_id=' + user_id+'&handle=' + handle,
             success: function(json) {
          if(json['success']){
            loadGroup(parent_group_id);
            $('#group'+parent_group_id+' .box-header').trigger('click');
            
            
            $('#none_group_user').load('index.php?route=project/project_user/listuser');
          }
             }
         });
        
      }
      
      function deleteUser(parent_group_id,user_id){
      $.ajax({
             url: 'index.php?route=project/project_user/remove_user&user_id=' +  user_id,
             dataType: 'json',
             success: function(json) {
          if(json['success']){
            loadGroup(parent_group_id);
            $('#group'+parent_group_id+' .box-header').trigger('click');
            
            
            $('#none_group_user').load('index.php?route=project/project_user/listuser');
          }
             }
         });
        
      }
      
      function updateRegion(user_group_id,user_id,region_code){
      if(region_code!='*'){
        $.ajax({
            type: 'POST',
            url: 'index.php?route=project/project_user/update_region',
            dataType: 'json',
            data: 'region_code=' + region_code+'&user_id=' + user_id+'&user_group_id=' + user_group_id,
            beforeSend: function(){
            },
            success: function(json) {
              if(json['success']){
                
                loadGroup(user_group_id);
            
                $('#group'+user_group_id+' .box-header').trigger('click');
              }
            }
        });
      }
      }
      function approveDevice(user_group_id, user_id){
      $.ajax({
          type: 'POST',
          url: 'index.php?route=project/project_user/approveDevice',
          dataType: 'json',
          data: 'user_group_id=' + user_group_id+'&user_id=' + user_id+'&status=1',
          beforeSend: function(){
          },
          success: function(json) {
            if(json['success']){
              loadGroup(user_group_id);
              $('#group'+user_group_id+' .box-header').trigger('click');
            }
              }
         });
      }
      
      function addUser(parent_group_id, user_parent_id, user_group_id, user_id){
      $.ajax({
          type: 'POST',
          url: 'index.php?route=project/project_user/add_user',
          dataType: 'json',
          data: 'user_parent_id=' + user_parent_id+'&user_group_id=' + user_group_id+'&user_id=' + user_id,
          beforeSend: function(){
          },
          success: function(json) {
            if(json['success']){
              if(parent_group_id!=0){
                
                loadGroup(parent_group_id);
                $('#group'+parent_group_id+' .box-header').trigger('click');  
              }else{
                loadGroup(user_group_id);
                $('#group'+user_group_id+' .box-header').trigger('click');  
                
              }
              
              $('#none_group_user').load('index.php?route=project/project_user/listuser');
              
              
            }
              }
         });
      }
      
      
      
         //-->
       
   </script>
   <script type="text/javascript">
      $('#button-quick-add-save').on('click', function() {
      
        var province_id     = $('#quick-add-user select[name=\'province_id\']').val();
        var fullname        = $('#quick-add-user input[name=\'fullname\']').val();
        var email           = $('#quick-add-user input[name=\'email\']').val();
        var identity_number = $('#quick-add-user input[name=\'p_identity_number\']').val();
        var telephone       = $('#quick-add-user input[name=\'telephone\']').val();
        var user_group_id       = $('#quick-add-user select[name=\'user_group_id\']').val();
      
        $.ajax({
          type: 'POST',
          url: 'index.php?route=user/user/quickAddUser',
          dataType: 'json',
          data: 'province_id=' + province_id + '&fullname=' + fullname+'&email=' + email+'&p_identity_number=' + identity_number+'&telephone=' + telephone + '&user_group_id='+user_group_id,
          beforeSend: function(){
            $('#button-quick-add-save').button('loading');
            $('.text-danger').remove();
          },
          success: function(json) {
            $('#button-quick-add-save').button('reset');
      
            if(json['error']) {
              if(json['error']['province_id']){
                $('#quick-add-user select[name=\'province_id\']').after('<div class="text-danger">'+ json['error']['province_id'] +'</div>');
              }
      
              if(json['error']['fullname']){
                $('#quick-add-user input[name=\'fullname\']').after('<div class="text-danger">'+ json['error']['fullname'] +'</div>');
              }
      
              if(json['error']['email']){
                $('#quick-add-user input[name=\'email\']').after('<div class="text-danger">'+ json['error']['email'] +'</div>');
              }
      
              if(json['error']['identity_number']){
                $('#quick-add-user input[name=\'p_identity_number\']').after('<div class="text-danger">'+ json['error']['identity_number'] +'</div>');
              }
      
              if(json['error']['telephone']){
                $('#quick-add-user input[name=\'telephone\']').after('<div class="text-danger">'+ json['error']['telephone'] +'</div>');
              }
      
            }
      
            if(json['success']) {
              $('#quick-add-user').before('<div class="alert alert-success alert-sm">'+json['success']+'<button type="button" class="close" data-dismiss="alert">x</button></div>');
      
              $('#quick-add-user input[name=\'fullname\']').val('');
              $('#quick-add-user input[name=\'email\']').val('');
              $('#quick-add-user input[name=\'p_identity_number\']').val('');
              $('#quick-add-user input[name=\'telephone\']').val('');
      
              $('#none_group_user').load('index.php?route=project/project_user/listuser');
            }
          }
        });
      });
      
      $('#button-quick-add-close').on('click', function() {
        $('#quick-add-user').remove();
      });
   </script>
</div>