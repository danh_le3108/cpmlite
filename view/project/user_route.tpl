
<style>
#main_box{min-height:800px;}
.nav-stacked>li>p {
    border-radius: 0;
    border-top: 0;
    border-left: 3px solid transparent;
    color: #444;
}
.nav-pills>li>p{
    border-radius: 0;
    border-top: 3px solid transparent;
    color: #444;
}
.nav-pills>li>p {
    border-radius: 4px;
}
.nav>li>p{
    position: relative;
    display: block;
    padding: 10px 15px;
    margin: 0 0 0;
}
  #map-container h4.map-title {
	  font-family: 'Open Sans Condensed', sans-serif;
	  font-size: 16px;
	  font-weight: 400;
	  padding: 10px;
	  background-color: #48b5e9;
	  color: white;
	  margin: 0;
	  width: 100%;
	  border-radius: 0;
  }
  #map-container #map-content {
  	padding: 10px;
  }
  .gm-style-iw {
	  width: 320px !important;
	  top: 15px !important;
	  left: 0px !important;
	  background-color: #fff;
	  box-shadow: 0 1px 6px rgba(178, 178, 178, 0.6);
	  border: 1px solid rgba(72, 181, 233, 0.6);
	  border-radius: 0;
  }
  .gm-style-iw img {max-width: 100%;}
  .store_image { padding: 0 0 10px 0; }
  .scrolldiv table td{}
</style>
    <!-- Main content -->
    <section class="content">
          <div class="box box-solid" id="filter-area">
            <div class="box-header with-border">
            <div class="row">
            
            <div class="col-sm-3">
             <div class="form-group">
            <label class="control-label" for="filter_round_name"><?php echo $text_round; ?></label>
            <select name="filter_round_name" id="filter_round_name" class="form-control chosen filter" onchange="filter()">
             <option value="*"><?php echo $text_select; ?></option>
              <?php foreach ($rounds as $round => $total) { ?>
              <option value="<?php echo $round; ?>" <?php if ($round == $filter_round_name) { ?>selected="selected"<?php } ?>><?php echo $round; ?></option>
              <?php } ?>
            </select>
          </div>
              </div><!--col -->
              
              
            <div class="col-sm-3">
            <label class="control-label" for="filter_round_name"><?php echo $text_user; ?></label>
         <div class="form-group">
            <select name="filter_user_id" id="filter_user_id" class="form-control chosen filter" onchange="filter()">
             <option value="*"><?php echo $text_select; ?></option>
             <?php foreach($staffs as $f) {?>
             <option value="<?=$f['user_id']?>" <?=$f['user_id'] == $filter_user_id ? 'selected' : ''?>><?=$f['username'].' - '.$f['fullname']?> (<?=!empty($count_staff[$f['user_id']]) ? $count_staff[$f['user_id']] : 0 ?>)</option>
             <?php }?>
            </select>
              </div><!--form-group -->
            
              </div><!--col -->
              
              
              
            <div class="col-sm-3">
            <div class="clearfix <?php echo !empty($dates)?'':'hide';?>">
            <label class="control-label" for="filter_round_name"><?php echo $text_date; ?></label>
             <select name="filter_date_checkin" id="filter_date_checkin" class="form-control chosen filter" onchange="filter()">
             	<option value="*"><?php echo $text_select; ?></option>
              
            <?php if(!empty($dates)) { ?>
              <?php foreach ($dates as $date => $total) { ?>
              <option value="<?php echo $date; ?>" <?php if ($date == $filter_date_checkin) { ?>selected="selected"<?php } ?>><?php echo $date; ?> (<?php echo $total; ?>)</option>
              <?php } ?>  <?php } ?>
            </select>
             </div><!--clearfix -->
             
            
              </div><!--col -->
              
              
            <div class="col-sm-3">
         <div class="form-group"> 
            <p>
              <a id="button-clear" class="btn btn-primary pull-right"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></a>
              </p>
              </div><!--form-group -->
              </div><!--col -->
              </div><!--row -->
              
              
              
            </div><!--box-header -->
            </div><!--box -->
            
      <div class="row">
        <div class="col-sm-4  col-xs-12">
        
      
          
          
          
          <?php if($filter_user_id>0){?>
         
            <!-- /************************* NẾU CÓ PLAN ************************* -->     

          <div class="box box-solid">
            <div class="box-header with-border">
            
              <h3 class="box-title">
              <?php
              $urls = array();
              $a =1;
              $b='';
              foreach($plans as $plan){
                  if($a =1){
                  $b='/@'.$plan['latitude'].','.$plan['longitude'];
                  //.'!1d'.$plan['latitude'].'!2d'.$plan['longitude']
                  }
              	$urls[] = $plan['latitude'].','.$plan['longitude'];//.'/'.$plan['latitude'].','.$plan['longitude']
                $a++;
              }
              ?>
              <a target="_blank" href="https://www.google.com/maps/dir/<?php echo implode('/',$urls);?><?php echo $b;?>,10z/data=!3m1!4b1!4m2!4m1!3e0">
                  <span class="fa fa-map-marker"></span>
              <?php echo $staffs[$filter_user_id]['fullname'];?>
                  </a>
              
              </h3>

            </div>
            <!-- /.box-header -->
           <?php if(!is_null($filter_date_checkin)&&isset($time_diff)&&!is_null($time_diff)){?>
            <div class="box-footer with-border">
            Cửa hàng đầu: <?php echo $time_start;?><br/> 
            Cửa hàng cuối: <?php echo $time_end;?><br/> 
            Thời gian chênh lệch: <strong><?php echo $time_diff;?> </strong>
          </div>  <!-- /.box-header -->
          <?php } ?>
          </div>
          <!-- /. box -->
          <div class="box box-solid">
          
        
               <div id="listplan" class="box-body no-padding" style="max-height:600px;">
               <div class="scrolldiv">
              <table class="table table-hover">
              
         <?php 
         $i=1;
         foreach ($plans as $plan) { ?>
         <tr>
          <td width="80">
         <a href="<?php echo $plan['popup'];?>" class="img-thumbnail popup"><img width="100px" src="<?php echo $plan['store_image'];?>"/></a>
         </td>
                <td>
                <p style="width: 85%"><span class="badge btn-<?=$plan['plan_status'] != 0  ? 'success' : 'danger'?>"><?php echo $i;?></span>
                 
               <a target="_blank" href="index.php?route=plan/plan/edit&plan_id=<?=$plan['plan_id']?>"><?php echo $plan['store_code'];?> - <?php echo $plan['store_name'];?> </a> <br/>
               <a target="_blank" href="https://www.google.com/maps/search/<?php echo $plan['latitude'];?>,<?php echo $plan['longitude'];?>/@<?php echo $plan['latitude'];?>,<?php echo $plan['longitude'];?>,17z"><i class="fa fa-map-marker"></i>
                <?php echo $plan['latitude'];?>,<?php echo $plan['longitude'];?></a> <br/>
                <?php echo $plan['store_address_raw'];?> <br/>
                <?php echo $text_checkin;?> <?php echo $plan['time_checkin'];?> <br/>
                Device: <?php echo $plan['model'];?> <br/>
              + <?php echo $plan['time_diff'];?> <br><br>
                <?=$plan['plan_status'] == 1  ? 'Đã upload' : 'Chưa upload'?>
                
                
                  </p>
                  
                  </td>
                  </tr>
          <?php
          $i++;
           } ?>
              </table>
              </div>
            <!-- /.scrolldiv -->
            </div> <!-- /.box-body -->
          
          </div>  <!-- /.box -->
          <?php } ?>
          
          
  <script type="text/javascript">
   $("#listplan .scrolldiv").slimscroll({
		height: $('#listplan').height()+ "px",
		color: "rgba(0,0,0,0.7)",
		size: "10px"
	  });
  </script>
          
          
          
          
          
          
        </div>
        <!-- /.col -->
        <div class="col-sm-8 col-xs-12">
          <div id="main_box" class="box box-primary">
           
            <div class="box-body no-padding" id="map" style="height:800px; width:100%;">
              
            </div>
            <!-- /.box-body -->
          
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <div id="map-container" style="display: none;">
     <h4 class="map-title"></h4>
     <div id="map-content">
        <div class="store_image col-md-4"><img src="" class="img-responsive"></div>
        <div class="col-md-8">
           <div class="clearfix"></div>
           <br> 
           <div class="clearfix"></div>
           <br> 
           <div class="clearfix"><a target="_blank" class="btn btn-xs btn-primary" href="">Xem</a></div>
           <br>
        </div>
     </div>
  </div>
  <script type="text/javascript"><!--
  //   function filter() {
  //     url = 'index.php?route=project/user_route';
	 // 	var filter_round_name = $('select[name=\'filter_round_name\']').val();
  
  //     if (filter_round_name != '*') {
  //       url += '&filter_round_name=' + encodeURIComponent(filter_round_name);
  //     }
  
	 // var filter_date_checkin = $('select[name=\'filter_date_checkin\']').val();
  
  //     if (filter_date_checkin != '*') {
  //       url += '&filter_date_checkin=' + encodeURIComponent(filter_date_checkin);
  //     }
	 // var filter_user_id = $('select[name=\'filter_user_id\']').val();
  
  //     if (filter_user_id != '*') {
  //       url += '&filter_user_id=' + encodeURIComponent(filter_user_id);
  //     }
  //     location = url;
  //   }
    //-->
</script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_MAP_EMBED_API_KEY;?>"></script>
<script type="text/javascript"><!--
  $(document).ready(function(){
    $('.table').magnificPopup({
      type:'image',
      delegate: 'a.popup',
      gallery: {
        enabled:true
      }
    });
  }); 

  var lat = 10.824725;
  var lng = 106.640540
  var coordinates = <?php echo json_encode($plans);?>;
  if(coordinates.length > 0) {
    lat = parseFloat(coordinates[0].latitude);
    lng = parseFloat(coordinates[0].longitude);
  }
  
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 9,
    center: {lat: lat, lng: lng}
  });

  var i;
  var infowindow = new google.maps.InfoWindow();
  for (i = 0; i < coordinates.length; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(coordinates[i]['latitude'], coordinates[i]['longitude']),
      // icon: image_icon,
      // animation: google.maps.Animation.DROP,
      label:(i+1).toString(),
      map: map
    });
    
    google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
      return function() {
        var content = $('#map-container');
        content.find('.map-title').html('<span class="badge btn-danger">'+(i+1)+'</span>'+coordinates[i].store_code+' - '+coordinates[i].store_name);
        content.find('img').attr('src', coordinates[i].store_image);
        content.find('a').attr('href','index.php?route=plan/plan/edit&plan_id='+coordinates[i].plan_id);
        var spans = content.find('.clearfix');
        spans.eq(0).html('Checkin: '+coordinates[i].time_checkin);
        spans.eq(1).html(coordinates[i].time_diff);
        infowindow.setContent('<div id="map-container">'+content.html()+'</div>');
        infowindow.open(map, marker);
     }
    })(marker, i));

    // START INFOWINDOW CUSTOMIZE.
    // The google.maps.event.addListener() event expects
    // the creation of the infowindow HTML structure 'domready'
    // and before the opening of the infowindow, defined styles are applied.
    google.maps.event.addListener(infowindow, 'domready', function() {
      // Reference to the DIV that wraps the bottom of infowindow
      var iwOuter = $('.gm-style-iw');
      iwOuter.children(':nth-child(1)').css({'display' : 'block'});
      // Since this div is in a position prior to .gm-div style-iw.
      // We use jQuery and create a iwBackground variable,
      // and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
      
      var iwBackground = iwOuter.prev();
      // Removes background shadow DIV
      iwBackground.children(':nth-child(2)').css({'display' : 'none'});
      // Removes white background DIV
      iwBackground.children(':nth-child(4)').css({'display' : 'none'});
      // Changes the desired tail shadow color.
      iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});
      // Reference to the div that groups the close button elements.
      var iwCloseBtn = iwOuter.next();
      // Apply the desired effect to the close button
      iwCloseBtn.css({opacity: '1', right: '40px', top: '3px', height: '27px', width: '27px', border: '7px solid #48b5e9', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9'});
      // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
      iwCloseBtn.mouseout(function(){
      $(this).css({opacity: '1'});
      });
    }); 
  }
      
</script>
