
<style type="text/css">
	.audit-download {
		background-color: #337ab7; 
		color: #ffffff;
		position: fixed;
		bottom: 30px;
		right: 20px;
		width: 50px;
		height: 50px;
		border-radius: 100%;
		font-size: 20px;
		text-align: center;
		line-height: 45px;
		z-index: 1000;

		-webkit-transition: all 0.5s ease;
	  -moz-transition: all 0.5s ease;
	  -o-transition: all 0.5s ease;
	  -ms-transition: all 0.5s ease;
	  transition: all 0.5s ease;
	}
	.audit-download:hover {
		cursor: pointer;
		color: #ffffff;
		background: #f00;
		-webkit-transition: all 0.5s ease;
	  -moz-transition: all 0.5s ease;
	  -o-transition: all 0.5s ease;
	  -ms-transition: all 0.5s ease;
	  transition: all 0.5s ease;
	  width: 70px;
	  height: 70px;
	  line-height: 65px;
	  font-size: 25px;
	}
</style>
<div class="col-sm-12">
  <a <?php if(!is_null($filter_round)){?> href="<?php echo $export;?>"<?php }else{?>onclick="exportAlert('<?php echo $export;?>');"<?php } ?>   class="audit-download" data-toggle="tooltip" title="Kết quả thực hiện"><i class="fa fa-download"></i></a>
</div>


<div id="modal-export" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        Vui lòng chọn Tháng
      </div>
      <div class="modal-body">
           <div class="form-group">
              <label class="control-label" for="export_round"> </label>
              <select id="export_round" class="form-control" onchange="export_filter(this.options[this.selectedIndex].value);">
                <option value="*"><?php echo $text_select; ?></option>
                <?php foreach($rounds as $round){?>
                <option value="<?php echo $round['round_name'];?>"><?php echo $round['round_name'];?></option>
                <?php } ?>  
              </select>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal" aria-hidden="true"><?php echo $button_cancel;?></button>
        <a href="" data-href="" id="export_download" class="btn btn-success pull-right" style="margin-right:15px; display:none;"><?php echo $button_download;?></a>
      </div>
    </div>
  </div>
</div>
<!--//module-modal -->

<script type="text/javascript"><!--
	function export_filter(otp_value){
		  org_href = $('#export_download').attr('data-href');
		  if(otp_value!='*'){
		    org_href += '&filter_round=' + encodeURIComponent(otp_value);
			$('#export_download').attr('href',org_href).show();
		  }else{
			 $('#export_download').hide(); 
		  }
	}
    function exportAlert(url){
		$('#export_download').attr('href','');
		$('#export_download').attr('data-href',url);
		$('#export_download').hide();
        $('#export_round option:selected').removeAttr('selected');
        $('#export_round option').first().attr('selected', 'selected');
      	$('#modal-export').modal('show');
    }  //-->
</script>
