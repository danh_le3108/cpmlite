<style>
tr.is_active{
	background-color:#0C9;
}
</style>
          <a id="hide_modal" class="badge label-danger pull-right" onclick="$(this).parent().html('');">x</a>
  <div class="panel-body">
    <table class="table table-hover">
      <tr>
        <th>#</th>
        <th><?php echo $text_filename;?></th>
        <th><?php echo $text_date_added;?></th>
        <th><?php echo $text_total_row;?></th>
        <th><?php echo $text_success_row;?></th>
        <th><?php echo $text_update_row;?></th>
        <th><?php echo $text_error_row;?></th>
        <th class="text-right"><?php echo $text_action;?></th>
      </tr>
      <?php foreach($histories as $history_info){ ?>
      <tr class="<?php echo ($history_info['import_id']==$history_id)?'is_active':'';?>">
      	<td><?php echo $history_info['import_id'];?></td>
        <td><a href="<?php echo $history_info['href'];?>"><?php echo basename($history_info['template_path']); ?></a></td>
        <td><?php echo $history_info['date_added']; ?></td>
        <td><?php echo $history_info['rows'] ; ?></td>
        <td><?php echo $history_info['success_row'] ; ?></td>
        <td><?php echo $history_info['update_row'] ; ?></td>
        <td><?php echo $history_info['error_row']; ?></td>
        <td class="text-right"><?php if($history_info['error_row']>0){ ?>
        <a href="<?php echo $history_info['download']; ?>" class="btn btn-danger"><i class="fa fa-download"></i></a>
        <?php } ?>
        </td>
      </tr>
      <?php } ?>
    </table>
  </div>
  
<div class="clearfix">
  <div class="col-sm-6 text-left"><?php echo $results; ?></div>
  <div class="col-sm-6 text-right"><?php echo str_replace('class="pagination"','class="pagination pagination-sm"',$pagination); ?></div>
  
  
</div>  