<?php if($has_import) {?>
<div class="form-group clearfix" style="margin-top:20px;">
	<div class="col-sm-12">
    <a onclick="loadImport('<?php echo $template_id;?>');" class="pull-right"><i class="fa fa-download"></i> <?php echo $text_import_history; ?></a>
    </div>
    <form action="<?php echo $form;?>" enctype="multipart/form-data" method="post">
	  <label class="control-label col-sm-1" for="input-import"><?php echo $text_import; ?></label>
	  <div class="col-sm-5">
	    <div class="input-group">
	      <div class="input-group-btn">
	    <a href="<?php echo $template_url; ?>" class="btn btn-success"><i class="fa fa-download"></i> Tải mẫu excel</a>
	    </div>
	      <input type="file" id="file_import" name="file_import" class="form-control">
	      <div class="input-group-btn">
	        <button class="btn btn-default" type="submit" data-loading-text="<?php echo $text_importing; ?>" id="button-import"><?php echo $text_import; ?></button>
	      </div>
	    </div>
	  </div>
	</form>
</div>
<?php } ?>
<?php if($has_export) {?>
<div class="menu-goo">
  <input type="checkbox" href="#" class="menu-goo-open" name="menu-goo-open" id="menu-open"/>
  <label class="menu-goo-open-button" for="menu-open">
    <span class="fa fa-download"></span>
  </label>
  <a  href="<?php echo $export;?>" class="menu-goo-item btn-primary" data-toggle="tooltip" title="<?php echo $button_download; ?>"><i class="fa fa-download"></i></a>
</div>
<?php } ?>
<?php if(!empty($history_id)){?>
  <script type="text/javascript"><!--
   loadImport('<?php echo $template_id;?>');
    //-->
  </script>
<?php } ?>
