<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-layout" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_back; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
      </div>
      <h1><?php echo $heading_title; ?></h1>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-layout" class="form-horizontal">
          <fieldset>
            <legend><?php echo $text_table; ?></legend>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group required">
                  <label class="col-sm-3 control-label" for="input-name"><?php echo $entry_name; ?></label>
                  <div class="col-sm-9">
                    <input type="text" name="template_name" value="<?php echo $template_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                    <?php if ($error_name) { ?>
                    <div class="text-danger"><?php echo $error_name; ?></div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-name"><?php echo $entry_hcolor; ?></label>
                  <div class="col-sm-9">
                    <input type="hidden" name="settings[heading_color]" value="<?php echo (isset($settings['heading_color']))?$settings['heading_color']:'#ccc'; ?>" class="colorpicker"/>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-table"><?php echo $entry_table; ?></label>
                  <div class="col-sm-9">
                    <div class="well well-sm" style="height: 150px; overflow: auto;">
                      <?php foreach ($tables as $tab) { ?>
                      <div class="checkbox">
                        <label>
                        <input type="checkbox" name="table[]" value="<?php echo $tab['value']; ?>" <?php if (in_array($tab['value'], $table)) { ?>checked="checked"<?php } ?>/>
                        <?php echo $tab['label']; ?>
                        </label>
                      </div>
                      <?php } ?>
                    </div>
                    <button type="submit" form="form-layout" class="btn btn-primary"><i class="fa fa-angle-right"></i> Load field</button>
                    <?php if ($error_table) { ?>
                    <div class="text-danger"><?php echo $error_table; ?></div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-ptable"><?php echo $entry_ptable; ?></label>
                  <div class="col-sm-9">
                    <select name="settings[primary_table]" class="form-control" id="input-ptable">
                      <option value="">- none -</option>
                      <?php foreach ($fields['tables'] as $ptable => $field) { ?>
                      	<option value="<?php echo $ptable; ?>" <?php echo (isset($settings['primary_table']) && $settings['primary_table']==$ptable)?'selected="selected"':''; ?>><?php echo $ptable; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-sheet_index">Sheet Index</label>
                  <div class="col-sm-9">
                    <input type="text" name="settings[sheet_index]" value="<?php echo isset($settings['sheet_index'])?$settings['sheet_index']:''; ?>" class="form-control" placeholder="Sheet Index" id="input-sheet_index">
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-start_row"><?php echo $entry_start_row; ?></label>
                  <div class="col-sm-9">
                    <input type="text" name="settings[start_row]" value="<?php echo isset($settings['start_row'])?$settings['start_row']:''; ?>" class="form-control" placeholder="<?php echo $entry_start_row; ?>" id="input-start_row">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-file-template"><?php echo $entry_file; ?></label>
                  <div class="col-sm-9">
                  	<div class="input-group">
											<input name="file_path" class="form-control" type="text" value="<?php echo $file_path; ?>" disabled="disabled" id="file_path">
											<span class="input-group-btn"><button type="button" class="btn btn-danger btn-flat"><i class="fa fa-close"></i></button></span>
										</div>
                   	<div class="input-group">
											<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-flat" id="btn-upload"><i class="fa fa-upload"></i> <?php echo $text_upload; ?></button>
											<a <?php echo !empty($file_path)?'href="'.$dowload_url.$file_path.'"':'';?> type="button" class="btn btn-success btn-flat"><i class="fa fa-download"></i> <?php echo $text_download; ?></a>
											</span>
										</div>
                  </div>
                </div>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" charset="utf-8">

$(document).ready(function() {
$('tbody.sortrow').sortable({connectWith: 'tbody.sortrow'}); 
}); 
</script>
<script type="text/javascript" charset="utf-8" async defer>

$('input.colorpicker').each(function(index, element) {
	$(element).attr('readonly', true).spectrum({
		showInput: true,
		showAlpha: true,
		preferredFormat: "hex",
		allowEmpty: true,
		move: function(color) {
			$(this).val(color).trigger('change');
		},
		change: function(color) {
			$(this).val(color).trigger('change');
		}
	});
});

$('#btn-upload').on('click', function() {
  	var elem = $(this);
  	new AjaxUpload(elem, {
  		action: '<?php echo $url_upload; ?>',
  		name: 'file',
  		autoSubmit: false,
  		responseType: 'json',
  		data: {template_id:'<?php echo $template_id; ?>'},
  		onChange: function(file, extension) {
  			this.submit();
  		},
  		onSubmit: function(file, extension) {
  		},
  		onComplete: function(file, json) {
  			if (json['success']) {
  				$('#file_path').attr('value', json['filename']);
  			}
  			if (json['error']) {
  				$('#'+elem_id).after('<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> '+json['error']+'<button type="button" class="close" data-dismiss="alert">×</button></div>');
  			}
  			$('#loading').remove();
  		}
  	});
});
</script>
