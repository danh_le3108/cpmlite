<?php echo $header; ?>

	
    
  <div class="page-header">
  
    
    <div class="container-fluid">
      <div class="pull-right">
      
        <a href="<?php echo $clear_cache;?> " class="btn btn-danger btn-sm"><i class="fa fa-eraser"></i> <?php echo $text_clear_cache;?> </a>
        <a href="<?php echo $clear;?>" class="btn btn-primary btn-sm"><span><i class="fa fa-save"></i> <?php echo $text_clear; ?></span></a>
        <a href="<?php echo $uninstall;?>" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i> <?php echo $text_uninstall; ?></a>
        
      </div>
     	 

    </div>
  </div>
  <div class="container-fluid">   
      <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">x</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">x</button>
    </div>
    <?php } ?>
    
    <ul class="nav nav-tabs nav-mgbot">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $text_image;?></a></li>
            <li><a href="#tab-log" data-toggle="tab"><?php echo $text_log;?></a></li>
            <li><a href="#tab-setting" data-toggle="tab"><?php echo $text_setting;?></a></li>
          </ul>
          
     <div class="panel panel-default">
       <div class="panel-body">
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
            
    <?php $image_command=(!empty($image_manager_plus_command))?$image_manager_plus_command:array();
            $commands = array(
                    'mkdir'			=>$text_mkdir,
                    'mkfile'		=>$text_mkfile,
                    'upload'		=>$text_upload,
                    'reload'		=>$text_reload,
                    'getfile'		=>$text_getfile,
                    'up'			=>$text_up,
                    'download'		=>$text_download,
                    'rm'			=>$text_rm,
                    'duplicate'		=>$text_duplicate,
                    'rename'		=>$text_rename,
                    'copy'			=>$text_copy,
                    'cut'			=>$text_cut,
                    'paste'			=>$text_paste,
                    'edit'			=>$button_edit,
                    'extract'		=>$text_extract,
                    'archive'		=>$text_archive,
                    'view'			=>$text_view,
                    'resize'		=>$text_resize,
                    'sort'			=>$text_sort,
                    'search'		=>$text_search
                );  
                $cmd = "commands: [";
                 foreach ($commands as $command=>$value) { 
                     if(!empty($image_command[$user_group_id][$command])){
                     	$cmd .="'".$command."',";
                     }
                  } 
                $cmd .="],";
        ?> 
    
    <div class="content" id="elfinder" style="min-height:550px; width:100%;"></div>
    </div>
    
    <div class="tab-pane" id="tab-log">
    
       <iframe style="min-height:500px; width:100%;" frameborder="0" src="<?php echo $log_url;?>"></iframe>       
      
    </div>
    
    <div class="tab-pane" id="tab-setting">
    
              
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form" class="form-horizontal">
      
              <div class="form-group">
                <label class="col-sm-3 control-label" for=""><?php echo $entry_manager_status; ?></label>
                <div class="col-sm-9">
                <div class="input-group">
              <select name="image_manager_plus_status" class="form-control">
                  <option value="1" <?php echo ($image_manager_plus_status=='1')?'selected="selected"':'';?>><?php echo $text_enabled;?></option>
                  <option value="0" <?php echo ($image_manager_plus_status=='0')?'selected="selected"':'';?>><?php echo $text_disabled;?></option>
              </select>
              <div class="input-group-addon">
              <a onclick="$('#form').submit();" ><span><i class="fa fa-save"></i> <?php echo $button_save; ?></span></a>
              </div>
              </div>
                                  </div>
              </div><!-- form-group--> 
              <br/><br/>  
    <ul class="nav nav-tabs">
           <?php $i=0; foreach ($user_groups as $user_group) { ?>
        <li <?php if($i==0){?>class="active"<?php } ?>> <a  href="#user_group_<?php echo $user_group['user_group_id']; ?>" data-toggle="tab"><?php echo $user_group['group_name']; ?></a></li>
          <?php $i++; } ?>   
          </ul>
          
          <div class="tab-content">
                <?php $i=0;
                foreach ($user_groups as $user_group) { ?>
                <div class="tab-pane <?php if($i==0){?>active<?php } ?> in" id="user_group_<?php echo $user_group['user_group_id']; ?>">
                
              <div class="form-group">
                <label class="col-sm-2 control-label" for=""><?php echo $user_group['group_name']; ?></label>
                <div class="col-sm-10">
                
              <a onclick="$(this).parent().find(':checkbox').prop('checked',true);"><?php echo $text_select_all; ?></a>/ <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a><br><br> 
             
             
          <div class="well well-sm" style="min-height:300px;overflow: auto;">
              
                <?php $x=0; foreach ($commands as $command=>$label) { ?>
                 <div class="xcheckbox">
               
                  <input type="checkbox" name="image_manager_plus_command[<?php echo $user_group['user_group_id']; ?>][<?php echo $command; ?>]" value="1" 
                  <?php echo (!empty($image_manager_plus_command[$user_group['user_group_id']][$command]))?'checked="checked"':''; ?>  id="x<?php echo $x;?>"/>
                 <label fox="x<?php echo $i;?>">  <?php echo ucwords($label); ?>
                  
                </label>
                </div>
                <?php $x++; } ?>
              </div>
                                  </div><!--col --> 
              </div><!-- form-group--> 
          </div>
                <?php $i++; } ?>
                </div><!--tab-content --> 
                
    </form>
    </div>
    </div><!--tab-content --> 
    </div>
    </div><!--panel --> 
  </div><!--//container-fluid --> 
</div>
<script type="text/javascript"><!--
 $(document).ready(function() {
		 $('#elfinder').elfinder({
			 url: '<?php echo str_replace('&amp;','&',$filemanager);?>',  
			 lang: 'en',
			 resizable: false,
			 <?php echo str_replace(',]',']',$cmd);?> 
			 contextmenu: {
				 navbar: ['open', '|', 'copy', 'cut', 'paste', 'duplicate', '|', 'rm', '|', 'info'],
				 cwd: ['reload', 'back', '|', 'upload', 'mkdir', 'mkfile', 'paste', '|', 'info'],
				 files: ['getfile', '|', 'open', 'quicklook', '|', 'download', '|', 'copy', 'cut', 'paste', 'duplicate', '|','rm', '|', 'edit', 'rename', 'resize', '|', 'archive', 'extract', '|', 'info']
			 },
			});

	 });	
//--></script>
<?php echo $footer; ?>