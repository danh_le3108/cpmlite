<div class="container-fluid" id="filter-area">
  <div class="box box-info">
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label" for="input-filter_global"><?php echo $text_filter; ?></label>
            <input type="text" name="filter_global" value="" placeholder="<?php echo $text_filter; ?>" id="input-filter_global" class="form-control filter" onchange="javascript:filter()"/>
          </div>
        </div>
        <!-- staff -->
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label"><?=$text_staff_user?></label>
            <select name="filter_user_id" class="form-control chosen filter" onchange="filter()">
               <option value="*"> --- Chọn --- </option>
               <?php foreach($users as $s) { 
                if(in_array($s['user_id'],$users_manager)){
               ?>
               <option value="<?=$s['user_id']?>"><?=$s['username'].' - '.$s['fullname']?></option>
               <?php }} ?>
            </select>
          </div>
        </div>
        <!-- round name-->
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label"><?=$text_month?></label>
            <select name="filter_round" class="form-control filter" onchange="filter()">
               <option value="*"> --- Chọn --- </option>
               <?php foreach($rounds as $r) { ?>
               <option value="<?=$r['round_name']?>"><?=$r['round_name']?></option>
               <?php } ?>
            </select>
          </div>
        </div>
        
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label"><?php echo $text_image_type; ?></label>
            <select name="filter_image_type" class="form-control filter" onchange="filter()">
               <option value="*"> --- Chọn --- </option>
               <?php foreach($image_types as $type) { 
                if(in_array($type['image_type_id'],$config_project_images)){
               ?>
               <option value="<?php echo $type['image_type_id']; ?>"><?php echo $type['image_type']; ?></option>
               <?php }} ?>
            </select>
          </div>
        </div>

        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label">Hình ảnh có thay đổi</label>
            <select name="filter_is_diff" class="form-control filter" onchange="filter()">
               <option value="*"> --- Chọn --- </option>
               <?php foreach($is_diffs as $value =>$label) { 
               ?>
               <option value="<?php echo  $value; ?>"><?php echo $label; ?></option>
               <?php } ?>
            </select>
          </div>
        </div>

        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label" for="filter_manual">Loại upload</label>
            <select name="filter_manual" class="form-control filter" onchange="javascript:filter()">
              <option value="*"><?php echo $text_all; ?></option>
              <?php foreach($is_manuals as $value =>$label){?>
              <option value="<?php echo $value;?>"><?php echo $label;?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label" for="filter_limit">Limit</label>
            <select name="filter_limit" id="filter_limit" class="form-control filter" onchange="javascript:filter()">
              <?php foreach($limits as $limit){?>
              <option value="<?php echo $limit;?>"><?php echo $limit;?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-sm-3">
          <button type="button" id="button-clear" class="btn btn-primary btn-sm pull-right" autocomplete="off"><i class="fa fa-eraser"></i><?=$button_clear?></button>
        </div>
      </div> 
    </div>
  </div>
</div>
<?php if($images){ ?>
  <div class="box box-info">
    <div class="panel-footer">
      <div class="text-left"><?php echo $results; ?></div>
      
      <div class="text-right"><?php echo $pagination; ?></div>
    </div>
    <div class="panel-body">
      <div class="row thumbnails">
        <?php foreach($images as $image){ ?>
        <div class="col-sm-3 text-center">
        <div class="well clearfix mb-20">
          <a id="img<?php echo $image['image_id']; ?>" 
          title="<?php echo $image['store_code']; ?>" 
          href="<?php echo $image['popup']; ?>" 
          data-image_id="<?php echo $image['image_id']; ?>" 
          class="img-thumbnail popup">
          <img class="img-responsive lazy" src="<?php echo $image['thumb']; ?>" />
          </a>
          <p>NV: <?php echo $image['fullname']; ?></p>
          <p>Tháng: <?php echo $image['round_name']; ?></p>
          <p>Checkin: <?php echo $image['time_checkin']; ?></p>
          <p>Upload: <?php echo $image['date_added']; ?></p>
          
          <a target="_blank" href="<?php echo $image['href']; ?>" class="btn btn-primary btn-sm">#<?php echo $image['plan_id']; ?> - Store: <?php echo $image['store_code']; ?></a>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="panel-footer">
      <div class="text-left"><?php echo $results; ?></div>
      
      <div class="text-right"><?php echo $pagination; ?></div>
    </div>
  </div>
  <?php } ?>

<script type="text/javascript">
  function initManific(){
  $('.thumbnails').magnificPopup({
    type:'image',
    delegate: 'a.popup',
    image: {
      verticalFit: true,
      titleSrc: function (item) {
        var caption = item.el.attr('title');
        var image_id = item.el.data('image_id');
        // if(image_id>0){
        //  caption += ' &middot; <a onclick="img_rotator(\''+image_id+'\',\'-90\');" class="btn btn-success rotatora" title="-90"><i class="fa fa-rotate-left"></i></a>';
        //  caption += ' <a onclick="img_rotator(\''+image_id+'\',\'180\');" class="btn btn-success rotatorb" title="180"><i class="fa fa-arrow-up"></i></a>';
        //  caption += ' <a onclick="img_rotator(\''+image_id+'\',\'90\');" class="btn btn-success rotatorc" title="+90"><i class="fa fa-rotate-right"></i></a>';
        // }
        return caption;
      }
    },
    gallery: {
      enabled:true
    }
  });
}

$(document).ready(function() {
  initManific();
});
</script>