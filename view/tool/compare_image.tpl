

  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Lọc hình ảnh
     
        <button type="button" class="btn btn-warning pull-right btn-sm" data-dismiss="modal" onclick="closeModalCompare();"><i class="fa fa-close"></i> Close</button>
        </h4>
      </div>
      <div class="modal-body">


<div class="container-fluid">

        
        
	<div class="box box-info" id="filter-area">
    <div class="panel-body">
      <div class="row">
      <input type="hidden" name="filter_store_code" value="<?php echo $filter_store_code;?>" />
      
      
      <div class="col-sm-2">
        <div class="form-group">
          <label class="control-label" for="filter_is_diff">Hình ảnh có thay đổi</label>
          <select name="filter_is_diff" id="filter_is_diff" class="form-control" onchange="javascript:filter()">
            <option value="*"><?php echo $text_all; ?></option>
            <?php foreach($is_diffs as $value =>$label){?>
            <option value="<?php echo $value;?>" <?php echo (!is_null($filter_is_diff)&&$value==$filter_is_diff)?'selected="selected"':'';?>><?php echo $label;?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="form-group">
          <label class="control-label" for="filter_manual">Loại upload</label>
          <select name="filter_manual" id="filter_manual" class="form-control" onchange="javascript:filter()">
            <option value="*"><?php echo $text_all; ?></option>
            <?php foreach($is_manuals as $value =>$label){?>
            <option value="<?php echo $value;?>" <?php echo (!is_null($filter_manual)&&$value==$filter_manual)?'selected="selected"':'';?>><?php echo $label;?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      
        
      <div class="col-sm-2">
        <div class="form-group">
          <label class="control-label" for="filter_round"><?php echo $text_round; ?></label>
          <select name="filter_round" id="filter_round" class="form-control" onchange="javascript:filter()">
            <option value="*"><?php echo $text_all; ?></option>
            <?php foreach($rounds as $round){?>
            <option value="<?php echo $round;?>" <?php echo ($round==$filter_round)?'selected="selected"':'';?>><?php echo $round;?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label" for="input-user_id"><?php echo $text_image_type; ?></label>
            <select name="filter_image_type" id="" class="form-control" onchange="filter();">
            	<option value=""><?php echo $text_select; ?></option>
            	<?php foreach($image_types as $type){ ?>
                <?php if(in_array($type['image_type_id'],$config_project_images)){ ?>
            	<option value="<?php echo $type['image_type_id']; ?>" <?php echo $filter_image_type==$type['image_type_id']? 'selected="selected"':''; ?>><?php echo $type['image_type']; ?></option>
            	<?php } ?>
                <?php } ?>
            </select>
          </div>
        </div>
      </div><!--//--> 
      
    </div>
  </div>
  <style>
  .round_btn{
  margin-bottom:10px;
  }
  </style>
  <?php if($images){ ?>
  <div class="box box-info">
    <div class="panel-footer">
    	<div class="text-left"><?php echo $results; ?></div>
    	
    	<div class="text-right"><?php echo $pagination; ?></div>
    </div>
    <div class="panel-body">
    
            <?php foreach($rounds as $round){?>
          <button type="button" class="btn btn-default btn-block round_btn" style=""><?php echo $round;?></button>  
      <div class="row popup_thumbnails">
      	<?php foreach($images as $image){ ?>
        <?php if($image['round_name']==$round){?>
        <div class="col-sm-3 text-center">
        <div class="well clearfix mb-20">
          <p>NV: <?php echo $image['fullname']; ?></p>
          <a id="img<?php echo $image['image_id']; ?>" 
          title="<?php echo $image['store_code']; ?>" 
          href="<?php echo $image['popup']; ?>" 
          data-image_id="<?php echo $image['image_id']; ?>" 
          class="img-thumbnail popup">
          <img class="img-responsive lazy" src="<?php echo $image['thumb']; ?>" />
          </a>
          <a target="_blank" href="<?php echo $image['href']; ?>" class="btn btn-primary btn-sm">#<?php echo $image['plan_id']; ?> - Store: <?php echo $image['store_code']; ?></a>
          </div>
        </div>
        <?php } ?>
        <?php } ?>
    
      </div>
  <?php } ?>
  
    </div>
  
    <div class="panel-footer">
    	<div class="text-left"><?php echo $results; ?></div>
    	
    	<div class="text-right"><?php echo $pagination; ?></div>
    </div>
  </div>
  <?php } ?>
</div>

</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning pull-right btn-sm" data-dismiss="modal" onclick="closeModalCompare();"><i class="fa fa-close"></i> Close</button>
      </div>
    </div>
  </div>
<script type="text/javascript">
	function filter(){
		url = 'index.php?route=tool/search_image/compare';
    
  	var filter_store_code = $('input[name=\'filter_store_code\']').val();
  	if (filter_store_code) {
  		url += '&filter_store_code=' + encodeURIComponent(filter_store_code);
  	}

    var filter_image_type = $('select[name=\'filter_image_type\']').val();
  
    if (filter_image_type) {
      url += '&filter_image_type=' + encodeURIComponent(filter_image_type);
    }

    var filter_round = $('select[name=\'filter_round\']').val();
  
    if (filter_round!='*') {
      url += '&filter_round=' + encodeURIComponent(filter_round);
    }
    var filter_is_diff = $('select[name=\'filter_is_diff\']').val();
  
    if (filter_is_diff!='*') {
      url += '&filter_is_diff=' + encodeURIComponent(filter_is_diff);
    }
    var filter_manual = $('select[name=\'filter_manual\']').val();
  
    if (filter_manual!='*') {
      url += '&filter_manual=' + encodeURIComponent(filter_manual);
    }

		$('#compare-image').load(url);

	}
</script>
<script type="text/javascript"><!--

function initManific(){
	$('.popup_thumbnails').magnificPopup({
		type:'image',
		delegate: 'a.popup',
		image: {
			verticalFit: true,
			titleSrc: function (item) {
				var caption = item.el.attr('title');
				var image_id = item.el.data('image_id');
				if(image_id>0){
				 caption += ' &middot; <a onclick="img_rotator(\''+image_id+'\',\'-90\');" class="btn btn-success rotatora" title="-90"><i class="fa fa-rotate-left"></i></a>';
				 caption += ' <a onclick="img_rotator(\''+image_id+'\',\'180\');" class="btn btn-success rotatorb" title="180"><i class="fa fa-arrow-up"></i></a>';
				 caption += ' <a onclick="img_rotator(\''+image_id+'\',\'90\');" class="btn btn-success rotatorc" title="+90"><i class="fa fa-rotate-right"></i></a>';
				}
				return caption;
			}
		},
		gallery: {
			enabled:true
		}
	});
}

$(document).ready(function() {
	initManific();
});
//--></script>