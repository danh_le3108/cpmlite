
<div class="clearfix">
  
  <div class="row">
    <div id="content" class="col-sm-12">
    
     <div class="box box-info">
     
      <div class="box-header">
        <h3 class="box-title"><?php echo $heading_title; ?></h3>
        <div class="pull-right box-tools"> </div>
      </div>
      <div class="panel-body">
      
      <?php echo $text_message; ?>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      
      </div><!--panel-body --> 
      </div><!--box -->
      
      
      </div>
    </div>
</div>
