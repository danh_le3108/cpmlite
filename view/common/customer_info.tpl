  <?php if($customer_logged){?>
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="<?php echo $customer['image'];?>" class="user-image" alt="<?php echo $customer['fullname'];?>">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xxs"><?php echo $customer['fullname'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="<?php echo $customer['image'];?>" class="img-circle" alt="<?php echo $customer['fullname'];?>">

                <p>
                  <?php echo $customer['fullname'];?>
                  <small><?php echo $customer['group'];?></small>
                </p>
              </li>

              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo $customer['edit'];?>" class="btn btn-default btn-flat"><?php echo $text_profile;?></a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo $customer['logout'];?>" class="btn btn-default btn-flat"><?php echo $text_logout;?></a>
                </div>
              </li>
            </ul>
          </li>
  <?php } ?>