<?php if (count($languages) > 1) { ?>
          <li class="dropdown notifications-menu">
            <a href="jabascript:;" class="dropdown-toggle" data-toggle="dropdown">
    <?php foreach ($languages as $language) { ?>
    <?php if ($language['code'] == $code) { ?>
             <img src="<?php echo $http_server;?>assets/image/flags/<?php echo $language['code']; ?>.png"/>
             <?php } ?>
    <?php } ?>
            </a>
            <ul class="dropdown-menu" style="width:100px;">
              <li>
                <!-- Inner Menu: contains the notifications -->
                <ul class="menu">
 <!-- Language Menu -->
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-language">
      <?php foreach ($languages as $language) { ?>
    	<?php if ($language['code'] !== $code) { ?>
                      <li>
                        <button class="btn btn-link language-select" type="button" name="<?php echo $language['code']; ?>"><img src="<?php echo $http_server;?>assets/image/flags/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>" /> &nbsp; <?php echo $language['name']; ?></button>
                      </li>
          <?php } ?>
      <?php } ?>
  <input type="hidden" name="code" value="" />
  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
</form>
                </ul>
              </li>
            </ul>
          </li>
<?php } ?>
       