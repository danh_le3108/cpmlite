  <?php if($distributor_logged){?>
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="<?php echo $distributor['image'];?>" class="user-image" alt="<?php echo $distributor['fullname'];?>">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xxs"><?php echo $distributor['fullname'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="<?php echo $distributor['image'];?>" class="img-circle" alt="<?php echo $distributor['fullname'];?>">

                <p>
                  <?php echo $distributor['fullname'];?>
                  <small><?php echo $distributor['group'];?></small>
                </p>
              </li>

              <li class="user-footer">
                <div class="pull-left">
                <!--<a href="<?php echo $distributor['edit'];?>" class="btn btn-default btn-flat"><?php echo $text_profile;?></a>//--> 
                </div>
                <div class="pull-right">
                  <a href="<?php echo $distributor['logout'];?>" class="btn btn-default btn-flat"><?php echo $text_logout;?></a>
                </div>
              </li>
            </ul>
          </li>
  <?php } ?>