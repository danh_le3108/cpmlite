<?php
  foreach ($menus as $menu) {	   ?>
      <?php if ($menu['children']) {
		   ?>
      <li class="treeview">
          <a <?php if ($menu['href']) { ?>href="<?php echo $menu['href']; ?>"<?php } ?>><i class="fa <?php echo $menu['icon']; ?>"></i> <span><?php echo $menu['name']; ?></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
      <ul class="treeview-menu">
        <?php foreach ($menu['children'] as $children_1) { ?>
        <li class="">
          <?php if ($children_1['href']) { ?>
          <a href="<?php echo $children_1['href']; ?>"><i class="fa fa-circle-o"></i>  <?php echo $children_1['name']; ?></a>
          <?php } else { ?>
          <a class="parent"><i class="fa fa-circle-o"></i> <?php echo $children_1['name']; ?>
              	<span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <?php } ?>
          <?php if ($children_1['children']) { ?>
          <ul class="treeview-menu">
            <?php foreach ($children_1['children'] as $children_2) { ?>
            <li class="">
              <?php if ($children_2['href']) { ?>
              <a href="<?php echo $children_2['href']; ?>"><i class="fa fa-circle-o"></i> <?php echo $children_2['name']; ?></a>
              <?php } else { ?>
              <a class="parent"><?php echo $children_2['name']; ?>
              	<span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                </span>
              	</a>
              <?php } ?>
              <?php if ($children_2['children']) { ?>
              <ul class="treeview-menu">
                <?php foreach ($children_2['children'] as $children_3) { ?>
                <li><a href="<?php echo $children_3['href']; ?>"><i class="fa fa-circle-o"></i> <?php echo $children_3['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
          <?php } ?>
        </li>
        <?php } ?>
      </ul>
      <?php }else{ ?>
      
    <li id="<?php echo $menu['id']; ?>" class="">
      <a <?php if ($menu['href']) { ?>href="<?php echo $menu['href']; ?>"<?php } ?>><i class="fa <?php echo $menu['icon']; ?>"></i> <span><?php echo $menu['name']; ?></span></a>
        <?php } ?>
    </li>
    <?php } ?>