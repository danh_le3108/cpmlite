<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
<base href="<?php echo $base; ?>" />
<title><?php echo $title; ?></title>
  <!-- Tell the browser to be responsive to screen width -->

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo $http_server;?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo $http_server;?>assets/plugins/font-awesome/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo $http_server;?>assets/admin-lte/css/AdminLTE.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
  <link rel="stylesheet" href="<?php echo $http_server;?>assets/admin-lte/css/skins/<?php echo $config_color_mode;?>.min.css">
  <!-- /* daterange picker */ --> 
  <link rel="stylesheet" href="<?php echo $http_server;?>assets/plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?php echo $http_server;?>assets/plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="<?php echo $http_server;?>assets/plugins/datetimepicker/bootstrap-datetimepicker.min.css">
  <link rel="stylesheet" href="<?php echo $http_server;?>assets/plugins/select2/select2.css">
  <link rel="stylesheet" href="<?php echo $http_server;?>assets/editor/plugins/jquery-chosen/chosen.css">
  
  
            <!-- CSS -->
    <?php foreach ($styles as $style) { ?>
    <link type="text/css" href="<?php echo (strpos($style['href'], '//')=== false)?$http_server:'';?><?php echo $style['href']; ?>" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>      

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
<!-- jQuery 2.2.3 -->
<script src="<?php echo $http_server;?>assets/plugins/jquery/jquery-2.2.3.min.js"></script>
<script src="<?php echo $http_server;?>assets/editor/plugins/jquery-ui/jquery-ui.js"></script>

<!-- Bootstrap 3.3.6 -->
<script src="<?php echo $http_server;?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- JS -->
<script src="<?php echo $http_server;?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo $http_server;?>assets/plugins/datetimepicker/moment.js"></script>

<script src="<?php echo $http_server;?>assets/plugins/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo $http_server;?>assets/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo $http_server;?>assets/editor/plugins/jquery-chosen/chosen-min.js"></script>
    <!-- SlimScroll 1.3.0 -->
<script src="<?php echo $http_server;?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $http_server;?>assets/project/common-default.js?t=<?php echo rand(5, 15);?>"></script>

<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo (strpos($script, '//')=== false)?$http_server:'';?><?php echo $script; ?>"></script>
<?php } ?>
<!-- AdminLTE App -->
<script src="<?php echo $http_server;?>assets/admin-lte/js/app.js"></script>

	<script type="text/javascript"><!--
		function activeObj(obj,val) {
			$('.'+obj).hide();
			$('.'+obj+'.otp_'+val).show();
		}
        function loadImport(template_id){
		   $.ajax({
				  url: 'index.php?route=excel/template/import_history&template_id='+template_id,
				  dataType: 'html',
				  beforeSend: function(){},
				  success: function(html) {
					$('#import_history .modal-body').html(html);
					$('#import_history').modal('show');
					
				  }
			});
        }
		
						
		function img_rotator(image_id,angle){
			var magnificPopup = $.magnificPopup.instance; 
			// save instance in magnificPopup variable
			magnificPopup.close(); 
			// Close popup that is currently opened
				$.ajax({
					url: 'index.php?route=plan/plan/image_rotator&image_id='+image_id+'&angle=' + encodeURIComponent(angle),
					dataType: 'json',
					success: function(json) {
						$('#img'+image_id).data('filename',json['filename']).attr({'href':json['popup']});
						$('#img'+image_id + ' img').attr({'src':json['thumb']});
						//$('#img'+image_id).trigger('click');
					}
				});
		}

		$(document).on('click', '#hide_modal', function(event) {
			$('#import_history').modal('hide');
		});
		$(document).on('click', '#import_history .pagination-sm a', function(event) {
			event.preventDefault();
			$('#import_history .modal-body').load(this.href);
		});
		$(document).on('click', '#progress-user .pagination a', function(event) {
			event.preventDefault();
			$('#progress-user').load(this.href);
		});
	 	$(document).ready(function() {
			$(":input").attr("autocomplete","off");
		});
        //-->
      </script>
      <style>
body {
    font-family: 'Roboto', Arial, Tahoma, sans-serif;
    font-size: 13px;
}.sidebar-menu .treeview-menu>li>a {
    font-size: 13px;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice {
    color: #333;
}
	.pagination {margin: 0px 0 10px 0;}
	.table-responsive > table{margin-bottom:120px;}
	
	.chosen-container-single .chosen-single {
    position: relative;
    display: block;
    overflow: hidden;
    padding: 0 0 0 8px;
    height: 34px;
    border: 1px solid #ccc;
    border-radius: 0;
    background: #fff;
    color: #444;
    text-decoration: none;
    white-space: nowrap;
    line-height: 34px;
}
.text-success{
	color:#00a65a;
	font-weight:700;
}
.text-warning{
	color:#F33;
	font-weight:700;
}
.nav>li.plan_ajax{
	display:none;
}
.navbar-nav>.messages-menu>.dropdown-menu>li .menu>li>a {
	color:#666;
}
.navbar-nav>.messages-menu>.dropdown-menu>li .menu>li>a>.fa {
	min-width:20px;
}
.header+li>ul.nt{
	list-style:none !important;
	padding: 0 10px;

}
.header+li>ul.nt>li>a{
	line-height:26px;
	color:#666 !important;
}

</style>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | nav_top                          |
|               | sidebar-collapse                        |
|               | nav_sidebar                            |
|---------------------------------------------------------|
-->
<body class="hold-transition <?php echo $config_color_mode;?> <?php echo $config_nav;?>" data-minify-checker>
<div id="import_history" class="modal fade">
        <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="modal-body">
            </div>
        </div>
      </div>
</div><!--//import_history -->
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

  <?php if($config_nav!='nav_top'){?>
    <!-- Logo -->
    <a href="<?php echo $home;?>" class="logo" style="background: #000">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>CPM</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
        <img src="https://retailbuild.vn/media/files/CPM_POSMT7/logo.png" width="100px">
      </span>
    </a>
    
    <?php }else{ ?>
        
    <?php } ?>
        
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
    	<div class="clearfif">
 	 <?php if($config_nav=='nav_top'){?>
     
 	<div class="navbar-header">
          <a href="<?php echo $home;?>" class="navbar-brand"><b>CPM</b>CMS</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>
  	 <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul id="menu" class="nav navbar-nav">
          	<?php echo $navigation; ?>
          </ul>
          <!--
          <form class="navbar-form navbar-left" role="search">
            <div class="form-group">
              <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
            </div>
          </form> --> 
        </div>
        <!-- /.navbar-collapse -->
  <?php }else{?>
      <!-- Sidebar toggle button-->
      <a id="button-menu" class="sidebar-toggle" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
  <?php } ?>
    
    
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        
       
          <!-- Messages: style can be found in dropdown.less
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <ul class="menu">
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="assets/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>-->
          <!-- /.messages-menu -->

          <!-- Notifications Menu
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li> -->
          <!-- Tasks Menu
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <ul class="menu">
                  <li>
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li> -->
		  <?php //echo $language;?>
		 <?php echo $distributor_info;?>
		 <?php echo $customer_info;?>
         
    <?php if($user_logged&&in_array($user_group_id,$group_edit)){?>
         <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-info-circle"></i>
              <span class="label label-danger">12</span>
              Hướng dẫn
            </a>
            <ul class="dropdown-menu">
              <li class="header"><i class="fa fa-list"></i> <?php echo $text_list_of;?></li>
              <li>
                <ul class="nt">
                  <li><a href="<?php echo $region;?>">
                     - <?php echo $text_list_of.$text_region;?> (Region)
                    </a></li>
                  <li><a href="<?php echo $province;?>">
                     - <?php echo $text_list_of.$text_province;?>
                    </a></li>   
                  <li><a href="<?php echo $information;?>">
                     - <?php echo $text_user_guide;?>
                    </a></li>
                </ul>
              </li>
              <li class="header"><i class="fa fa-user"></i>  <?php echo $text_manager;?></li>
              <li>
                <ul class="nt">
                  <li><a href="<?php echo $project_user;?>">
                     - <?php echo $text_project_user;?>
                    </a></li>
                  <li><a href="<?php echo $project_customer;?>">
                    - <?php echo $text_customer_user;?>
                    </a></li>
                  <li><a href="<?php echo $project_distributor;?>">
                     - <?php echo $text_distributor;?>
                    </a></li>   
                </ul>
              </li>
              
              <li class="header"><i class="fa fa-search"></i> <?php echo $text_search;?></li>
              <li>
                <ul class="nt">     
                  <li><a href="<?php echo $coupon_history;?>">
                      - <?php echo $text_coupon_history;?>
                    </a></li>          
                  <li><a href="<?php echo $search_image;?>">
                      - <?php echo $text_search_image;?>
                    </a></li>      
                </ul>
              </li>
              
              <li class="header"><i class="fa fa-upload"></i> Excel Template</li>
              <li>
                <ul class="nt">     
                  <li><a href="<?php echo $import_distributor;?>">
                      1 - Import NPP
                    </a></li>          
                  <li><a href="<?php echo $import_store;?>">
                     2 - Import Store
                    </a></li>    
                  <li><a href="<?php echo $import_plan;?>">
                     3 - Import Plan
                    </a></li>    
                  <li><a href="<?php echo $import_coupon;?>">
                     4 - Import Coupon
                    </a></li>      
                </ul>
              </li>
              
              <li class="header"><a style="color:#F00; font-weight:bold" href="https://retailbuild.vn/media/files/NIVEA13001/help/layhinh.png" target="_blank"><i class="fa fa-list"></i> 
                     - HƯỚNG DẪN SUP LẤY HÌNH
                    </a></li>
            </ul>
          </li>
        
  <?php } ?>
  
 			<?php echo $user_info;?>
          <?php if(!$customer_logged&&!$distributor_logged&&!$user_logged){?>
             <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xxs"><?php echo $text_login;?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-body">
<form action="<?php echo $login_action;?>" method="post" enctype="multipart/form-data" id="form_login">
  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
              <div class="clearfix">
                <label for="user-username"><?php echo $text_username;?></label>
                <div class="input-group"><span class="input-group-addon"><i class="fa fa-user"></i></span>
                  <input type="text" name="username" value="" placeholder="<?php echo $text_username;?>" id="user-username" class="form-control" autocomplete="false">
                </div>
              </div>
              <div class="clearfix">
                <label for="user-password"><?php echo $text_password;?></label>
                <div class="input-group"><span class="input-group-addon"><i class="fa fa-lock"></i></span>
                  <input type="password" name="password" value="" placeholder="<?php echo $text_password;?>" id="user-password" class="form-control" autocomplete="false">
                </div>
               </div>
              <div class="clearfix">
                <div class="pull-left">
                </div>
                <div class="pull-right">
              <br /> 
                  <button type="submit" class="btn btn-default btn-flat"><?php echo $button_login;?></button>
                </div>
              </div>
</form>
              </li>
            </ul>
          </li>
          
          <?php } ?>
          <!-- Control Sidebar Toggle Button -->
          <li class="plan_ajax">
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-eye"></i> Plan chưa xem</a>
          </li>
        </ul>
      </div>
      </div><!--clearfix --> 
    </nav>
  </header>
  
  <?php if($config_nav!='nav_top'){?>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
     <!--  <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $logo;?>" class="img-circlex" style="border:2px solid #3c8dbc;border-radius:5px;" alt="<?php echo $config_name;?>">
        </div>
        <div class="pull-left info">
          <p><?php echo $customer;?></p> -->
          <!-- Status -->
      <!--     <a title="<?php echo $project_id;?>"><i class="fa fa-circle text-success"></i> <?php echo $config_name;?></a>
        </div>
      </div> -->

      <!-- search form (Optional)
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul id="menu" class="sidebar-menu">
  <?php echo $navigation; ?>
        <!-- Optionally, you can add icons to the links
        <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
        <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Link in level 2</a></li>
            <li><a href="#">Link in level 2</a></li>
          </ul>
        </li> -->
      </ul>
      <!-- /.sidebar-menu -->
        <?php if($user_logged&&in_array($user_group_id,$group_edit)){?>
        <div class="user-panel"><div class="small-box bg-red" style="white-space: normal; padding:5px;">
        Khi Import dữ liệu Excel vui lòng download Template mới nhất 
        vì Cấu trúc Template có thể thay đổi!</div></div>
        <?php } ?>
    </section>
    <!-- /.sidebar -->
  </aside>
 <?php } ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
<div class="contentx">