 <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="btn btn-xs btn-danger pull-right" data-dismiss="modal" aria-hidden="true">x</button>
     <a onClick="saveRoute('group','<?php echo $info['survey_id'];?>','<?php echo $group_id;?>');" class="btn btn-primary btn-xs pull-right"><i class="fa fa-save"></i> <?php echo $button_save; ?></a>
          </div>
            <div class="modal-body">
            
           <input type="hidden" name="survey_id" value="<?php echo $info['survey_id'];?>"/>
          
            <div class="row form-group">
            <div class="col-sm-6">

            <label class="control-label" for="group_title"><?php echo $text_group_title; ?></label>

            <div class="clearfix">

              <input type="text" name="group_title" value="<?php echo $info['group_title'];?>" placeholder="<?php echo $text_group_title; ?>" id="group_title" class="form-control" />

            </div>
          </div>
            <div class="col-sm-6">
            <label class="control-label" for="group_target"><?php echo $text_target; ?></label>
            <div class="clearfix">
              <input type="text" name="group_target" value="<?php echo $info['group_target'];?>" placeholder="<?php echo $text_target; ?>" id="group_target" class="form-control" />
            </div>
          </div>
          
          </div><!--//row--> 
          
            <div class="row form-group">
            <div class="col-sm-6">
            <label class="control-label" for="group_code"><?php echo $text_code; ?></label>
            <div class="clearfix">
             <select name="group_code" id="group_code" class="form-control">
                <option value="">-None-</option>
                <?php foreach ($attr_groups as $group) { ?>
                <option value="<?php echo $group['group_code'];?>" <?php echo ($info['group_code']==$group['group_code'])?'selected="selected"':'';?>><?php echo $group['group_name'];?></option>
              <?php } ?>
              </select>
            </div>
          </div>
            <div class="col-sm-6">
            <label class="control-label" for="sort_order"><?php echo $text_sort_order; ?></label>
            <div class="clearfix">
              <input type="text" name="sort_order" value="<?php echo $info['sort_order'];?>" placeholder="<?php echo $text_sort_order; ?>" id="sort_order" class="form-control" />
            </div>
          </div>
          </div><!--//row--> 
          
            </div>
    <div class="modal-footer">
     <a onClick="saveRoute('group','<?php echo $info['survey_id'];?>','<?php echo $group_id;?>');" class="btn btn-primary btn-xs pull-right"><i class="fa fa-save"></i> <?php echo $button_save; ?></a>
    </div>
    <!--//box-footer -->
        </div>