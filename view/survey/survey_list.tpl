

  <div class="page-header">

    <div class="container-fluid">


     <div class="box" id="filter-area">

    <div class="panel-body">

      <div class="row">

        <div class="col-sm-6">

          <div class="form-group">

            <label class="control-label" for="filter_name"><?php echo $text_search; ?></label>

            <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $text_search; ?>" id="filter_name" class="form-control" onchange="filter()"/>

          </div>

        </div>

        

        <div class="col-sm-6">

        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger pull-right" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-code').submit() : false;" style="margin-left:20px;"><i class="fa fa-trash-o"></i></button>
        
        
          <button type="button" id="button-clear" class="btn btn-primary pull-right"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>



        </div>

      </div>

    </div>

    <!--panel-body -->

  </div>

  <!--box -->
  

    </div>

  </div>

  <div class="container-fluid">

  

  

      <?php if ($error_warning) { ?>

    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>

      <button type="button" class="close" data-dismiss="alert">&times;</button>

    </div>

    <?php } ?>

    <?php if ($success) { ?>

    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>

      <button type="button" class="close" data-dismiss="alert">&times;</button>

    </div>

    <?php } ?>

     


  
     <div class="box">

    <div class="panel-body">


     <form action="<?php echo $add; ?>" method="post" enctype="multipart/form-data" id="form-add" class="form-horizontal">

        

      <div class="row">

        <div class="col-sm-2">
          <div class="form-group required">

            <label class="clearfix col-sm-12" for="input-name"><?php echo $text_name; ?></label>

            <div class="clearfix col-sm-12">

              <input type="text" name="survey_name" value="" placeholder="<?php echo $text_name; ?>" id="input-name" class="form-control" />


            </div>

          </div>

        </div>


        <div class="col-sm-2">
          <div class="form-group">

            <label class="clearfix col-sm-12" for="sort_order"><?php echo $text_copy_from; ?></label>

            <div class="clearfix">
 			<select name="copy_id" id="copy_id" class="form-control">
                <option value="">-None-</option>
                <?php foreach ($surveys as $survey) { ?>
                <option value="<?php echo $survey['survey_id'];?>"><?php echo $survey['survey_name'];?></option>
               <?php } ?>
              </select>

            </div>

          </div>

      </div>
        

        <div class="col-sm-2">
          <div class="form-group required">

            <label class="clearfix col-sm-12" for="is_rating"><?php echo $text_is_rating; ?></label>
            <div class="clearfix col-sm-12">
            <select name="is_rating" id="is_rating" class="form-control">
                <option value="0"><?php echo $text_no;?></option>
                <option value="1"><?php echo $text_yes;?></option>
              </select>
            </div>
            
            <label class="clearfix col-sm-12" for="sort_order"><?php echo $text_sort_order; ?></label>

            <div class="clearfix col-sm-12">

              <input type="text" name="sort_order" value="" placeholder="<?php echo $text_sort_order; ?>" id="sort_order" class="form-control" />

            </div>

          </div>

      </div>
      
      
          <div class="col-sm-2">
          

             <label class="clearfix col-sm-12" for="coupon_prefix"><?php echo $text_coupon_prefix; ?></label>
           <div class="clearfix col-sm-12">
            <select name="coupon_prefix" id="coupon_prefix" class="form-control">
                <option value="" ><?php echo $text_none;?></option>
                <?php foreach ($prefixs as $prefix){?>
                <option value="<?php echo $prefix['coupon_prefix'];?>"><?php echo $prefix['coupon_prefix'];?> - <?php echo $prefix['prefix_value'];?></option>
                <?php } ?>
              </select>
            </div>
            
            
          </div>
          
          
          <div class="col-sm-2">
            <label class="clearfix col-sm-12" for="pass_point"><?php echo $text_pass_point; ?></label>
            <div class="clearfix">
              <input type="text" name="pass_point" value="" placeholder="<?php echo $text_pass_point; ?>" id="pass_point" class="form-control" />
            </div>
            
             <label class="clearfix col-sm-12" for="ratio"><?php echo $text_passratio; ?></label>
            <div class="clearfix">
              <input type="text" name="ratio" value="" placeholder="<?php echo $text_passratio; ?>" id="ratio" class="form-control" />
            </div>
            
          </div>
          
          
        <div class="col-sm-2">
            <label class="control-label col-sm-12" for="sort_order">&nbsp;</label>
          <div class="form-group">
            <div class="clearfix col-sm-12">
        <button type="submit" form="form-add" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-success pull-right"><i class="fa fa-save"></i> <?php echo $button_add; ?></button>
        </div>
        </div>
        </div>

    </div>
          

        </form>

		
    </div>

    <!--panel-body -->

  </div>

  <!--box -->
  

    <div class="box">

      <div class="box-header">

        <h2 class="box-title"> <?php echo $heading_title; ?></h3>

      </div>

      <div class="panel-body">

	 <div class="row">

          <div class="col-sm-6 text-left"><?php echo $results; ?></div>

          <div class="col-sm-6 text-right"><nav><?php echo $pagination; ?></nav></div>

        </div>

        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-code">

          <div class="table-responsive">

            <table class="table table-bordered table-hover">

              <thead>

                <tr>

                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td style="width: 1px;" class="text-center">#</td>

                  <td class="text-left"><?php if ($sort == 'survey_name') { ?>

                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $text_name; ?></a>

                    <?php } else { ?>

                    <a href="<?php echo $sort_name; ?>"><?php echo $text_name; ?></a>

                    <?php } ?></td>

                

                  <td class="text-left"><?php echo $text_description; ?></td>
                  <td class="text-left"><?php echo $text_coupon_prefix; ?></td>
                  <td class="text-left"><?php echo $text_pass_point; ?></td>
                  <td class="text-left"><?php echo $text_sort_order; ?></td>

                  <td class="text-right"><?php echo $text_action; ?></td>

                </tr>

              </thead>

              <tbody>

                <?php
                 if ($surveys) { ?>

                <?php foreach ($surveys as $survey) { ?>

                <tr>

                  <td class="text-center">

                    <input type="checkbox" name="selected[]" value="<?php echo $survey['survey_id']; ?>" <?php if (in_array($survey['survey_id'], $selected)) { ?>checked="checked"<?php } ?> id="pr<?php echo $survey['survey_id']; ?>"/><label for="pr<?php echo $survey['survey_id']; ?>">&nbsp;</label>

                    </td>

                  <td class="text-center">
<?php echo $survey['survey_id']; ?>

                    </td>
                  <td class="text-left"><?php echo $survey['survey_name']; ?></td>

                  <td class="text-left"><?php echo $survey['survey_description']; ?></td>
                  <td class="text-left"><?php echo $survey['coupon_prefix']; ?>
                   - <?php echo isset($coupon_prefix[$survey['coupon_prefix']])?$coupon_prefix[$survey['coupon_prefix']]['prefix_value']:''; ?></td>
                  <td class="text-left"><?php echo $survey['pass_point']; ?></td>
                  <td class="text-left"><?php echo $survey['sort_order']; ?></td>

                  <td class="text-right"><a href="<?php echo $survey['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>

                </tr>

                <?php } ?>

                <?php } else { ?>

                <tr>

                  <td class="text-center" colspan="3"><?php echo $text_no_results; ?></td>

                </tr>

                <?php } ?>

              </tbody>

            </table>

          </div>

        </form>

        <div class="row">

          <div class="col-sm-6 text-left"><?php echo $results; ?></div>

          <div class="col-sm-6 text-right"><nav><?php echo $pagination; ?></nav></div>

        </div>

      </div>

    </div>

	

  </div>

  

  

  <script type="text/javascript"><!--

  function filter() {

  	url = 'index.php?route=catalog/group';



	var filter_name = $('input[name=\'filter_name\']').val();



  	if (filter_name) {

  		url += '&filter_name=' + encodeURIComponent(filter_name);

  	}

  	location = url;

  }

  //-->

</script>