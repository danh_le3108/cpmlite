
<div class="page-header">
  <div class="container-fluid">
      <div class="pull-left"> <h1><?php echo $title; ?></h1></div>
    <div class="pull-right">
      <a href="<?php echo $continue; ?>" data-toggle="tooltip" title="<?php echo $button_back; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="box">
    <div class="box-header">
      <h2 class="box-title"><?php echo isset($title) ? $title : $heading_title; ?></h2>
    </div>
    <div class="panel-body">
        <div class="form-group required">
          <div class="col-sm-12">
            <div class="form-line">
              <?php echo isset($description) ? $description : ''; ?>
            </div>
           
          </div>
        </div>   
    </div>
  </div>
</div>
