

<style>
  .text-success{color:#008d4c;}
  .text-error{color:#d73925;}
</style>
<div class="box-header">
  <h3 class="box-title">
    <!--//--> <?php echo isset($survey_info['survey_name'])?$survey_info['survey_name']:$heading_plan_survey; ?>
  </h3>
</div>
<div class="box-body">
          <input type="hidden" name="survey_id" value="<?php echo $survey_id; ?>" /> 
          <input type="hidden" name="rating_manual" value="<?php echo $survey[$survey_id]['rating_manual']; ?>" /> 
  <table id="posm-question" class="table table-hover">
    <thead>
      <tr>
        <th class="text-left" width="300"> </th>
        <th class="text-center" width="1">Target</th>
        <th width="150"></th>
        <th class="text-value" width="100"><?php echo $text_point_total;?></th>
        <th class="text-value" width="100"><?php echo $text_result;?></th>
      </tr>
    </thead>
    


    <?php
         // print_r('<pre>');print_r($survey_answer);print_r('</pre>');   
      $pass_point = (float)$survey_info['pass_point'];
      $ratio = (float)$survey_info['ratio'];
      $rating_status = array();
             $total = 0;
      $group_sum = array();
      
      if(!empty($survey_info['groups'])) {
      
             
             
      foreach ($survey_info['groups'] as $group) {
          foreach ($group['questions'] as $question) {
      			$id = $question['question_id'];
          
                $answer_point[$id] = 0;
            
             if(isset($survey_answer[$id])){
                 foreach($survey_answer[$id] as $i=> $ianswer){
                    if($question['sum_group']==1){
                         $group_sum[$question['group_id']][] = $ianswer['answer_value'];
                    }
                    if(isset($survey_answer[$id])&&$question['sum_value']==1){
                        $answer_point[$id] = $ianswer['answer_value']; 
                    }
                 }
             }
          }
      }
      ?>
      
<textarea name="data_old" style="display:none;"><?php echo json_encode($survey_answer);?></textarea>

      <?php 
      foreach ($survey_info['groups'] as $group) {
      
      ?>
       <tbody>
      <tr>
      <th colspan="4">
      
      <h4><?php $group_id = $group['group_id']; echo $group['group_title'];?></h4></th>
       <th class="text-center">
       
          <?php if(isset($group_sum[$group['group_id']])){?>
          <i class="fa fa-<?php echo (array_sum($group_sum[$group_id])>0)?'check text-success':'times text-error';?>"></i>
          <?php } ?>
       
       </th>
      </tr>
      </tbody>
      <?php
          foreach ($group['questions'] as $question) {
          
      $id = $question['question_id'];
      ?>
    <tbody>
      <tr>
        <td class="text-left" width="300">
          <?php if($question['input_type'] == 'title'){ ?>
          <strong> <?php echo $question['question_title']; ?></strong>
          <?php } else {?>
          <?php echo $question['question_title']; ?>
          <?php } ?>
        </td>
        <td class="text-center" width="1">
          <?php echo (!empty($question['target']))?$question['target']:'';?>
        </td>
        <td width="150">
        <input disabled="disabled" type="hidden" class="form-control" name="survey_data[<?php echo $id; ?>][question_id]" value="<?php echo $id;?>"/>  
          <?php if($question['input_type'] == 'checkbox'){ ?>
          <?php foreach($question['answers'] as $qanswer){ ?>
          <div class="checkbox">
            <label>
            <?php
              $selected = (isset($survey_answer[$id][$qanswer['answer_id']]))?'checked="checked"':'';    ?>
            
            <input disabled="disabled" type="checkbox" name="survey_data[<?php echo $id; ?>][answer][<?php echo $qanswer['answer_id']; ?>][answer_value]" value="<?php echo $qanswer['answer_id']; ?>" <?php echo $selected;?>/> <?php echo $qanswer['answer_title']; ?>
            </label>
          </div>
          <?php } ?>
          <?php } elseif($question['input_type'] == 'radio'){ ?>
          <?php foreach($question['answers'] as $qanswer){ ?>
          <div class="radio">
            <label>
            <?php
              $selected = (isset($survey_answer[$id][$qanswer['answer_id']]))?'checked="checked"':''; ?>
             
            <input disabled="disabled" type="radio" class="survey_radio" data-group="q<?php echo $survey_id;?>-<?php echo $id; ?>" name="survey_data[<?php echo $id; ?>][answer][<?php echo $qanswer['answer_id']; ?>][answer_value]" value="<?php echo $qanswer['answer_id']; ?>" <?php echo $selected;?>/> <?php echo $qanswer['answer_title']; ?>
            </label>
          </div>
          <?php } ?>
          
          <?php } elseif ($question['input_type'] == 'select'){ ?>
          <select name="survey_data[<?php echo $id; ?>][answer][0][answer_value]" class="form-control chosen">
            <option value="" <?php echo (!isset($survey_answer[$id][0])||$survey_answer[$id][0]['answer_value']=='')?'selected="selected"':'';;?> ><?php echo $text_select; ?></option>
            
            <?php foreach($question['answers'] as  $qi=>$qanswer){ ?>
            <?php
              $selected = (isset($survey_answer[$id][$qanswer['answer_id']])&&$survey_answer[$id][$qanswer['answer_id']]['answer_id']==$qanswer['answer_id'])?'selected="selected"':'';         
                                          ?>
            <option value="<?php echo $qanswer['answer_id']; ?>" <?php echo $selected;?>>
            
            <?php echo $qanswer['answer_title']; ?>
            
            </option>
            <?php } ?>
          </select>
          <?php }  elseif($question['input_type'] == 'text'||$question['input_type'] == 'number'){ ?>
          <?php                          
            $answer_value = (isset($survey_answer[$id][0]['answer_value']))?$survey_answer[$id][0]['answer_value']:'';  ?>
          <input disabled="disabled" type="text" class="form-control" name="survey_data[<?php echo $id; ?>][answer][0][answer_value]" value="<?php echo $answer_value;?>"/>  
          <?php } ?>
          <?php if($question['input_type'] == 'textarea'){ ?>
          <?php
            $answer_value = (isset($survey_answer[$id][0]['answer_value']))?$survey_answer[$id][0]['answer_value']:'';?>
            
            
          <textarea disabled="disabled" class="form-control" rows="3" style="resize: none;" name="survey_data[<?php echo $id; ?>][answer][0][answer_value]"><?php echo $answer_value;?></textarea>
          <?php } ?>
        </td>
        <td class="text-center">
          <?php
            if($question['sum_group']==1&&isset($group_sum[$id])){
            $array_sum =  array_sum($group_sum[$id]);
               echo $array_sum;
               $rating_status[]= ($array_sum>0)?1:0;
            }
            ?>
          <?php echo ($question['sum_value']==1)?$answer_point[$id]:'';?>
        </td>
        <td class="text-center ">
          <?php if($question['sum_value']==1){?>
          <i class="fa fa-<?php echo ($answer_point[$id]>=$question['target'])?'check text-success':'times text-error';?>"></i>
          <?php } ?>
          
          
        </td>
        <?php } ?>
      </tr>
    </tbody>
    <?php  } ?>
    <?php  } ?>
    
    <tfoot>
      <?php if($survey_info['is_rating']==1){?>
      
      
      <tr>
        <td class="text-left" width="300"><strong><?php echo $text_rating;?></strong></td>
        <td class="text-center"></td>
        <td class="text-right" width="150"></td>
        
        <td class="text-center">
          <strong><?php echo array_sum($answer_point); ?>/<?php echo $pass_point;?></strong>
          </td>
        <td class="text-center">
          <i class="fa fa-<?php echo ($survey[$survey_id]['rating_manual']==1)?'check text-success':'times text-error';?>"></i> 
          <?php echo ($survey[$survey_id]['rating_manual']==1)?$text_rating_passed:$text_rating_not_passed;?>
        </td>
      </tr>
      
      <tr>
        <td class="text-left" width="300"><strong><?php echo $text_reason_not_pass;?></strong></td>
        <td class="text-center"></td>
        <td class="text-left" colspan="2">
         <select name="not_pass_id" class="form-control" disabled="disabled">
            <option value="0">----</option>
            <?php foreach($not_pass_reasons as $reason){?>
            <option value="<?php echo $reason['not_pass_id'];?>" <?php echo ($survey[$survey_id]['not_pass_id']==$reason['not_pass_id'])?'selected="selected"':'';?>><?php echo $reason['reason_name'];?></option>
            <?php } ?>                                            
          </select> 
        </td>
      
        <td class="text-center">
          
        </td>
      </tr>
        <tr>
        <td class="text-left" width="300"><strong><?php echo $text_comment;?></strong></td>
        <td class="text-center"></td>
        <td class="text-left" colspan="2">
        <input type="text" name="note" class="form-control" value="<?php echo $survey[$survey_id]['note'];?>" disabled="disabled"/>
        
        </td>
      
        <td class="text-center">
          
        </td>
      </tr>
    <?php  } ?>
    
    </tfoot>
  </table>
</div>
<div class="box-footer clearfix">
  <div class="row">
    <div class="col-sm-8">
     
    </div>
    <div class="col-sm-4">
    </div>
  </div>
</div>
<!--//box-footer -->
