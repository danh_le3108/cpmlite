
  <style>
  .red{color:#f00;}
  .blur{color:#999;}
  .orange{color:#F60;}
  .table-list thead tr td{ font-weight:600;}
  .t_map td{position:relative;}
  .t_map .btn{
	  z-index:999;
	  position:absolute;
	  right:10px;
	  top:10px;
  }
  .box-map #bodymap .btn{display:none;}
  #bodymap{ }
  #bodymap.mopen{}
  .table .table>thead>tr>th,
  .table .table>tbody>tr>th,
  .table .table>tfoot>tr>th,
  .table .table>thead>tr>td,
  .table .table>tbody>tr>td,
  .table .table>tfoot>tr>td {
    border-top: none !important;
    font-weight: 500;
}
.table-responsive table table{
    margin-bottom: 20px;
}
  </style>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
      <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#filter-area" data-toggle="tab"><?php echo $text_total;?> <span class="badge alert-error"><?php echo $total;?></span></a></li>
      <!-- <li><a href="#import-export" data-toggle="tab">Import/Export</a></li> -->
      <li class="pull-right"><button type="button" id="button-clear" class="btn btn-primary btn-sm pull-right"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button></li>
  
  
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="filter-area">
      
    <div class="panel-body">
      <div class="row">
      
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label" for="filter_global"><?php echo $text_search; ?></label>
            <input type="text" name="filter_global" value="<?php echo $filter_global; ?>" planholder="<?php echo $text_search; ?>" id="filter_global" class="form-control" onchange="filter()"/>
          </div>
        </div><!--col -->
        <div class="col-sm-3">
          <div class="form-group <?php echo ($isNationwide==1||$getLevel==$config_rsm)?'':'hide';?>">
        
            <label class="control-label" for="filter_rsm"><?php echo $text_nationwide; ?> / <?php echo $text_rsm;?></label>
            <select name="filter_rsm" id="filter_rsm" class="form-control chosen" onchange="filter()">
            
              <option value="*"> - <?php echo ($isNationwide==1)?$text_nationwide:$text_rsm; ?> - </option>
            
              <?php foreach ($rsms as $rsm) {?>
              <option value="<?php echo $rsm['customer_user_id']; ?>" <?php if ($rsm['customer_user_id'] == $filter_rsm) { ?>selected="selected"<?php } ?>><?php echo $rsm['fullname']; ?> </option>
              <?php } ?>
            </select>
          </div>
        </div><!--col -->
        <div class="col-sm-3">
          <div class="form-group <?php echo ($isNationwide==1||$getLevel==$config_rsm||$getLevel==$config_asm)?'':'hide';?>">
            <label class="control-label" for="filter_asm"><?php echo $text_asm;?></label>
            <select name="filter_asm" id="filter_asm" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_select; ?></option>
              <?php foreach ($asms as $asm) {?>
              <?php if (is_null($filter_rsm)||$asm['customer_parent_id']==$filter_rsm) { ?>
              <option value="<?php echo $asm['customer_user_id']; ?>" <?php if ($asm['customer_user_id'] == $filter_asm) { ?>selected="selected"<?php } ?>><?php echo $asm['fullname']; ?> </option>
              <?php } ?>
               <?php } ?>
            </select>
          </div>
        </div><!--col -->
        <div class="col-sm-3">
          <div class="form-group">
            <label class="control-label" for="filter_customer_user_id"><?php echo $text_ssup;?></label>
            <select name="filter_customer_user_id" id="filter_customer_user_id" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_select; ?></option>
              <?php foreach ($sups as $sup) {?>
              <?php if (is_null($filter_asm)||$sup['customer_parent_id']==$filter_asm) { ?>
              <option value="<?php echo $sup['customer_user_id']; ?>" <?php if ($sup['customer_user_id'] == $filter_customer_user_id) { ?>selected="selected"<?php } ?>><?php echo $sup['fullname']; ?> <?php echo isset($customer_count[$sup['customer_user_id']])?'('.$customer_count[$sup['customer_user_id']].')':'(0)';?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </div>
        </div><!--col -->
        
        
      </div><!--//row -->
      
      <div class="row">
      
       
          <div class="col-sm-2">
            <div class="form-group">
              <label class="control-label" for="filter_survey_group"><?php echo $text_planogram; ?></label>
              <select name="filter_survey_group" id="filter_survey_group" class="form-control" onchange="filter()">
                <option value="*"><?php echo $text_select; ?></option>
                <?php foreach($survey_groups as $value=>$label){?>
                <option value="<?php echo $value;?>" <?php echo ($value==$filter_survey_group&& !is_null($filter_survey_group))?'selected="selected"':'';?>><?php echo $label;?> <?php //echo isset($rating_count[$value])?'('.$rating_count[$value].')':'(0)';?></option>
                <?php } ?> 
              </select>
            </div>
          </div>
          <!--col -->
        <div class="col-sm-2">
          <div class="form-group <?php echo is_null($filter_survey_group)?'hide':'';?>">
            <label class="control-label" for="filter_rating_status"><?php echo $text_plan_result; ?></label>
            <select name="filter_rating_status" id="filter_rating_status" class="form-control" onchange="filter()">
              <option value="*"><?php echo $text_select; ?></option>
               <?php foreach($rstatuses as $value=>$label){?>
<option value="<?php echo $value;?>" <?php echo ($value==$filter_rating_status&& !is_null($filter_rating_status))?'selected="selected"':'';?>><?php echo $label;?></option>
              <?php } ?>  
            </select>
          </div>
        </div><!--col  -->
        
        
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label" for="filter_province_id"><?php echo $text_province; ?></label>
            <select name="filter_province_id" id="filter_province_id" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_select; ?></option>
              <?php foreach ($provinces as $province) { ?>
              <?php if (isset($province_count[$province['province_id']])&&$province_count[$province['province_id']]>0) { ?>
              <option value="<?php echo $province['province_id']; ?>" <?php if ($province['province_id'] == $filter_province_id) { ?>selected="selected"<?php } ?>><?php echo $province['name']; ?> <?php echo isset($province_count[$province['province_id']])?'('.$province_count[$province['province_id']].')':'(0)';?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </div>
        </div><!--col -->
        
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label" for="filter_distributor_id"><?php echo $text_distributor; ?></label>
            <select name="filter_distributor_id" id="filter_distributor_id" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_select; ?></option>
              <?php foreach ($distributors as $distributor) {?>
              <option value="<?php echo $distributor['distributor_id']; ?>" <?php if ($distributor['distributor_id'] == $filter_distributor_id) { ?>selected="selected"<?php } ?>><?php echo $distributor['distributor_code']; ?> - <?php echo $distributor['distributor_name']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div><!--col -->
        
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label" for="filter_round"><?php echo $text_round; ?></label>
            <select name="filter_round" id="filter_round" class="form-control" onchange="filter()">
              <option value="*"><?php echo $text_select; ?></option>
               <?php foreach($rounds as $round){?>
<option value="<?php echo $round['round_name'];?>" <?php echo ($round['round_name']==$filter_round)?'selected="selected"':'';?>><?php echo $round['round_name'];?><?php echo isset($round_count[$round['round_name']])?'('.$round_count[$round['round_name']].')':'(0)';?></option>
              <?php } ?>  
            </select>
          </div>
        </div><!--col -->
        <div class="col-sm-2">
        <div class="form-group">
          <label class="control-label" for="input-filter_date"><?php echo $text_date; ?></label>
          
          <div class="input-group">
          <input type="text" id="filter_date" value="<?php echo $filter_date;?>" name="filter_date" placeholder="<?php echo $text_date; ?>" class="form-control datepicker" onchange="javascript:filter()">
          
                  <a class="input-group-addon" onclick="$('input[name=\'filter_date\']').val('<?php echo $today;?>');javascript:filter();">
                    <small><?php echo $text_today;?></small>
                  </a>
        </div><!--group -->
        
        </div>
        </div><!--col -->
        
        <div class="col-sm-3">
        <div class="form-group">
        <?php echo $export_plan;?>
        </div>
        </div>
    </div><!--row -->
        
    </div><!--panel-body -->
  </div>
  <!--box -->
  
      </div>
    </div>
    <!-- /.tab-content -->
  
  
  
  
    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title"> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body"><div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
        
        <form action="" method="post" enctype="multipart/form-data" id="form-plan">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="width: 80px;"></th>
                <th class="text-left" width="300"><a href="<?php echo $sort_name; ?>" <?php if ($sort == 'plan_name') { ?>class="<?php echo strtolower($order); ?>"<?php } ?>><?php echo $text_store_name; ?></a></th>
                <th width="100"><?php echo $text_plan_name; ?></th>
                <th width="150"><?php echo $text_plan_status; ?></th>
                <th width="60"><?php echo $text_qc_status; ?></th>
                <th width="150">SkinCare</th>
                <th width="150">AdHoc</th>
                <th width="200"><?php echo $text_staff_user; ?></th>
               <!--<th width="180"><?php echo $text_date; ?></th>//-->  
              
              </tr>
            </thead>
            <tbody id="tr_place">
            <?php if ($plans) { ?>
            <?php foreach ($plans as $plan) {?>
              <tr class="tr_info">
                <td class="text-center" style="vertical-align:top">
                  <a href="<?php echo $plan['popup']; ?>" class="img-thumbnail popup"><img width="60" class="lazy" src="<?php echo $plan['store_image']; ?>" /></a>
                  
                  
                </td>
                <td class="text-left" width="220">
                  <a href="<?php echo $plan['edit']; ?>" title="<?php echo $plan['plan_name']; ?>" class="store_title"><strong><?php echo $plan['store_name']; ?></strong></a><br /> 
                  <span><?php echo $plan['plan_id']; ?></span> -   <span><?php echo $plan['store_code']; ?></span> <br/> 
                   <span><?php echo $text_store_customer_code;?>: <?php echo $plan['store_customer_code']; ?> - <?php echo $text_store_ad_code;?>: <?php echo $plan['store_ad_code']; ?></span><br /> 
                  <span class="blur"><?php echo $plan['store_address_raw']; ?></span><br />
                  <span>NPP: <?php echo $plan['store_distributor']; ?></span>
                <!-- <p> Created: <span><?php echo $plan['date_added']; ?></p>//--> 
                </td>
                <td>
                  <a href="<?php echo $plan['edit']; ?>" title="<?php echo $plan['plan_name']; ?>"><?php echo $plan['plan_name']; ?></a>
                  
                <?php if($plan['upload_count']>1){?>
                <span class="label label-danger pull-right"><?php echo $plan['upload_count'];?></span>
                <?php } ?>
                
                </td>
                <td>
                  <small class="red"><?php echo $plan['plan_status_text']; ?></small> - <?php echo $plan['plan_rating_text']; ?>
                </td>
                <td>
               <i class="fa fa-<?php echo ($plan['plan_qc']==1)?'check text-success':'text-warning';?>"></i>  
                </td>
                 <td>
                  <div class="clearfix">
                  <?php foreach($plan['plan_survey'] as $sid => $survey){?>
                  <?php if(isset($survey['survey_id'])&&in_array($survey['survey_id'],$survey_audit)){?>
                  <?php echo $surveys[$survey['survey_id']]['survey_name']; ?> - 
                  <span class="red"><?php echo $rating_result[$survey['rating_manual']]; ?></span><br/> 
           		<?php }?>
           		<?php }?>
                  </div>
                  
                </td>
                <td>
                  <div class="clearfix">
                  <?php foreach($plan['plan_survey'] as $sid => $survey){?>
                  <?php if(isset($survey['survey_id'])&&in_array($survey['survey_id'],$survey_adhoc)){?>
                  <?php echo $surveys[$survey['survey_id']]['survey_name']; ?> - 
                  <span class="red"><?php echo $rating_result[$survey['rating_manual']]; ?></span><br/> 
           		<?php }?>
           		<?php }?>
                  </div>
                  
                </td>
                <td width="180">
                <span><?php echo $plan['fullname']; ?> </span>
                </td>
                <!--
                <td>
                <p>  Start/End: <?php echo $plan['date_start']; ?>&nbsp;-&nbsp;<?php echo $plan['date_end']; ?></p> 
                   
                
               <p><span data-toggle="tooltip" title="Version: <?php echo $plan['version']; ?>">Upload:</span> 
               <i class="fa fa-map-marker <?php echo (!empty($plan['time_checkin']))?'check text-success':'text-warning';?>" data-toggle="tooltip" title="Checkin: <?php echo $plan['time_checkin']; ?>"></i>  
               &nbsp;&nbsp;<i class="fa fa-photo <?php echo (!empty($plan['time_image']))?'check text-success':'text-warning';?>" data-toggle="tooltip" title="Image: <?php echo $plan['time_image']; ?>"></i>  
               &nbsp;&nbsp;<i class="fa fa-calendar-check-o <?php echo (!empty($plan['time_survey']))?'check text-success':'text-warning';?>" data-toggle="tooltip" title="Survey: <?php echo $plan['time_survey']; ?>"></i>  
               &nbsp;&nbsp;<i class="fa fa-gift <?php echo (!empty($plan['time_coupon']))?'check text-success':'text-warning';?>" data-toggle="tooltip" title="Coupon: <?php echo $plan['time_coupon']; ?>"></i>  
               &nbsp;&nbsp;<i class="fa fa-info-circle <?php echo (!empty($plan['time_upload']))?'text-success':'text-warning';?>" data-toggle="tooltip" title="Info: <?php echo $plan['time_upload']; ?>"></i>
                 
                 </p> 
                </td>//--> 
               
              </tr>
            <?php } ?>
            <?php } else { ?>
              <tr>
                <td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
      </form><div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
          </div>
          </div>
         <!--
     <div class="box box-info box-map" >
      <div class="panel-body">
         
            <table class="table">
      <thead id="bodyhead"></thead>
      <tbody id="bodymap">
              <tr class="t_map"><td colspan="10">
              <button class="btn btn-danger btn-xs close_map">&nbsp;x&nbsp;</button>
              <div id="map_popup" class="mpadding" style="width:100%; height: 100%; min-height:400px;"></div></td></tr>
              </tbody>
               
            </table>
      </div>
      </div> --> 
          
  </div>
  <script type="text/javascript"><!--
  
$(document).ready(function(){
	$('.table').magnificPopup({
		type:'image',
		delegate: 'a.popup',
		gallery: {
			enabled:true
		}
	});
	<?php if(!empty($filter_global)){?>
	$("td span:contains(<?php echo $filter_global;?>)").css({"color":"red","background":"yellow"});
	<?php } ?>
});

  function filter() {
  	url = 'index.php?route=client/plan';
	
	var filter_global = $('input[name=\'filter_global\']').val();

  	if (filter_global) {
  		url += '&filter_global=' + encodeURIComponent(filter_global);
  	}
	var filter_date = $('input[name=\'filter_date\']').val();

  	if (filter_date) {
  		url += '&filter_date=' + encodeURIComponent(filter_date);
  	}



  	var filter_province_id = $('select[name=\'filter_province_id\']').val();

  	if (filter_province_id != '*') {
  		url += '&filter_province_id=' + encodeURIComponent(filter_province_id);
  	}

  	var filter_distributor_id = $('select[name=\'filter_distributor_id\']').val();

  	if (filter_distributor_id != '*') {
  		url += '&filter_distributor_id=' + encodeURIComponent(filter_distributor_id);
  	}
	
	
	
  	var filter_round = $('select[name=\'filter_round\']').val();

  	if (filter_round != '*') {
  		url += '&filter_round=' + encodeURIComponent(filter_round);
  	}
	
      var filter_survey_group = $('select[name=\'filter_survey_group\']').val();
  
      if (filter_survey_group != '*') {
        	url += '&filter_survey_group=' + encodeURIComponent(filter_survey_group);
			//NẾU CÓ FILTER MÚC ĐĂNG KÝ
			var filter_rating_status = $('select[name=\'filter_rating_status\']').val();
	  
		  if (filter_rating_status != '*') {
			url += '&filter_rating_status=' + encodeURIComponent(filter_rating_status);
		  }
      }
	
  	var filter_rsm = $('select[name=\'filter_rsm\']').val();

  	if (filter_rsm != '*') {
  		url += '&filter_rsm=' + encodeURIComponent(filter_rsm);
  	}
  	var filter_asm = $('select[name=\'filter_asm\']').val();

  	if (filter_asm != '*') {
  		url += '&filter_asm=' + encodeURIComponent(filter_asm);
  	}
  	var filter_customer_user_id = $('select[name=\'filter_customer_user_id\']').val();

  	if (filter_customer_user_id != '*') {
  		url += '&filter_customer_user_id=' + encodeURIComponent(filter_customer_user_id);
  	}
	
  	location = url;
  }
$(document).ready(function(){
	
  /*
  map = new GMaps({
	div: '#map_popup',
	lat: 10.789388,
	lng: 106.699827,
	zoom:17,
  });
  
 var marker = map.addMarker({
	lat: 10.789388,
	lng: 106.699827,
	title: 'CPM Việt Nam',
	zoom:17,
	infoWindow: {
		content: 'CPM Việt Nam <br> 60 Nguyễn Đình Chiểu,<br> Phường ĐaKao,<br> Quận 1,<br><a href="https://www.google.com/maps/@'+10.789388+ ','+106.699827+',15z" target=_blank>Full map</a>'
	}
});*/ 

	$(".close_map").click(function(){
        var tmap = $('#bodymap');
        tmap.insertAfter($('#bodyhead'));
		
    });
    $(".openmap").click(function(){
        var tmap = $('#bodymap');
        tmap.insertAfter($(this).parents("tbody"));
		
    });
	
});

  
   function openmap(mlat,mlng,mtitle,mcontent,addfull){
               var marker = map.addMarker({
                    lat: mlat,
                    lng: mlng,
                    title: mtitle,
					zoom:17,
                    infoWindow: {
                        content: mcontent+'<br><a href="https://www.google.com/maps/search/'+mlat+ ','+mlng+'/@'+mlat+ ','+mlng+',17z" target=_blank>Địa điểm checkin</a><br><a title="Tọa độ tương đối với vị trí checkin" href="https://www.google.com/maps/dir/'+mlat+ ','+mlng+'/'+addfull+'" target=_blank>Tọa độ tương đối</a>'
                    }
                });
               marker.infoWindow.open(map, marker);
			   
             //search: mcontent+'<br><a href="https://www.google.com/maps/search/'+mlat+ ','+mlng+'/'+addfull+',17z" target=_blank>Full map</a>'
             //direction: mcontent+'<br><a href="https://www.google.com/maps/dir/'+mlat+ ','+mlng+'/'+addfull+',17z" target=_blank>Full map</a>'
   }
  //-->
</script>