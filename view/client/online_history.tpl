<div class="container-fluid">

    <?php if ($error_warning) { ?>

    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>

      <button type="button" class="close" data-dismiss="alert">&times;</button>

    </div>

    <?php } ?>

    <?php if ($success) { ?>

    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>

      <button type="button" class="close" data-dismiss="alert">&times;</button>

    </div>

    <?php } ?>

    

     <div class="box" id="filter-area">

    <div class="panel-body">

      <div class="row">

        <div class="col-sm-3">

          <div class="form-group">

            <label class="control-label" for="filter_name"><?php echo $text_search; ?></label>

            <input type="text" name="filter_global" value="<?php echo $filter_global; ?>" placeholder="<?php echo $text_search; ?> username, ip, date" id="filter_global" class="form-control" onchange="filter()"/>

          </div>

        </div>

        <div class="col-sm-3">

          <div class="form-group">

            <label class="control-label" for="filter_level_id"><?php echo $text_customer_group; ?></label>

            <select name="filter_level_id" id="filter_level_id" class="form-control chosen" onchange="filter()">

              <option value="*"><?php echo $text_all; ?></option>

              <?php foreach ($groups as $group) { ?>

              <option value="<?php echo $group['customer_level_id']; ?>" <?php if ($group['customer_level_id'] == $filter_level_id) { ?>selected="selected"<?php } ?>><?php echo $group['customer_level_id'].' - '.$group['level_name']; ?></option>

              <?php } ?>

            </select>

          </div>

        </div>

        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label" for="filter_round"><?php echo $text_round; ?></label>
            <select name="filter_round" id="filter_round" class="form-control chosen" onchange="filter()">
              <option value="*"><?php echo $text_all; ?></option>
              <?php foreach ($rounds as $round) { ?>
              <option value="<?php echo $round['round_name']; ?>" <?php if ($round['round_name'] == $filter_round) { ?>selected="selected"<?php } ?>><?php echo $round['round_name']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div> 
       

        <div class="col-sm-2">

          <div class="form-group">
<br/> 
      <a href="<?php echo $download; ?>" data-toggle="tooltip" title="<?php echo $button_download; ?>" class="btn btn-success"><i class="fa fa-download"></i> <?php echo $button_download; ?></a>

          </div>
        </div>
          <div class="col-sm-2 text-right">
      
          <div class="form-group">
<br/> 
          <button type="button" id="button-clear" class="btn btn-primary pull-right"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>

          </div>
    
      </div>


      </div>

    </div>

    <!--panel-body -->

  </div>

  <!--box -->

		

    <div class="box">

      <div class="box-header">

        <h2 class="box-title"> <?php echo $heading_title; ?></h3>

      </div>

      <div class="panel-body">

        <div class="row">

          <div class="col-sm-6 text-left"><?php echo $results; ?></div>

          <div class="col-sm-6 text-right"><nav><?php echo $pagination; ?></nav></div>

        </div>

		

          <div class="table-responsive">

            <table class="table table-bordered table-hover">

              <thead>

                <tr>

             
                
                  <td class="text-left"><?php echo $text_user; ?></td>
                  <td class="text-left"><?php echo $text_customer_group; ?></td>
                  <td class="text-right"><?php echo $text_ip_address; ?></td>


                  <td class="text-right"><?php echo $text_total_login; ?></td>

                  <td class="text-right"><?php echo $text_last_login; ?></td>

                </tr>

              </thead>

              <tbody>

                <?php if ($online_histories) { ?>

                <?php foreach ($online_histories as $history) { ?>

                <tr>

                  <td class="text-left">
                  
                  <?php echo $history['fullname']; ?><br/> 
                  <small><?php echo $history['username']; ?></small>
                  
                  </td>
                  <td class="text-left"><?php echo $history['customer_level']; ?></td>
                  <td class="text-right"><a target="_blank" href="http://whatismyipaddress.com/ip/<?php echo $history['ip']; ?>"><?php echo $history['ip']; ?></a></td>


                  <td class="text-right"><?php echo $history['total']; ?></td>
                  <td class="text-right"><?php echo $history['last_login']; ?>
                  
                  </td>

                </tr>

                <?php } ?>

                <?php } else { ?>

                <tr>

                  <td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>

                </tr>

                <?php } ?>

              </tbody>

            </table>

          </div>


        <div class="row">

          <div class="col-sm-6 text-left"><?php echo $results; ?></div>

          <div class="col-sm-6 text-right"><nav><?php echo $pagination; ?></nav></div>

        </div>

      </div>

    </div>

	

     

  </div>

  <script type="text/javascript"><!--

  function filter() {

  	url = 'index.php?route=client/online_history';



	var filter_global = $('input[name=\'filter_global\']').val();



  	if (filter_global) {

  		url += '&filter_global=' + encodeURIComponent(filter_global);

  	}

  	var filter_level_id = $('select[name=\'filter_level_id\']').val();



  	if (filter_level_id != '*') {

  		url += '&filter_level_id=' + encodeURIComponent(filter_level_id);

  	}

    var filter_round = $('select[name=\'filter_round\']').val();
  
    if (filter_round != '*') {
      url += '&filter_round=' + encodeURIComponent(filter_round);
    }
  	location = url;

  }

  //-->

</script>