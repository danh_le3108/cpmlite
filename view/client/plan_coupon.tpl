 <div class="box-header">
        <h3 class="box-title"><?php echo $heading_reward; ?></h3>
      </div>
      <div class="box-body">
                    <div class="clearfix">
                     <table class="table">
                   <thead>
                       <tr>
                       <td><?php echo $text_month;?></td>
                       <td><?php echo $text_coupon_code;?></td>
                       <td class="text-center"><?php echo $text_status;?></td>
                       </tr>
                   </thead>
                   
             <?php
             $status = array();
              foreach($reward_types as $type){
             ?>
                 <?php if(in_array($type['reward_type_id'],$allow_view)){?>
               <?php if($type['reward_type_id']!=$config_reward_recall){  ?>
               <thead>
                       <tr>
                       <td colspan="3" class="text-center"><h4><?php echo $type['reward_type_name'];?></h4></td>
                       </tr>
                   </thead>
                  
                   <tbody>
                  
             <?php foreach($plan_coupons as $coupon){?>
             <?php $coupon_value = isset($prefix[$coupon['coupon_prefix']]['prefix_value'])?$prefix[$coupon['coupon_prefix']]['prefix_value']:0;?>
                 <?php if($coupon['reward_type_id']==$type['reward_type_id']){ ?>
                 
                 <?php if($coupon['reward_type_id']!=$config_reward_audit||($coupon['reward_type_id']==$config_reward_audit&&$plan_info['plan_rating']>0)){ ?>
                 <tr>
                      	<td>
                        <?php
                        ?>
                        <?php echo $coupon['yearmonth'];?>
                        <input disabled="disabled" name="coupons[<?php echo $coupon['coupon_history_id'];?>][yearmonth]" type="hidden" value="<?php echo $coupon['yearmonth'];?>" />
                        <input disabled="disabled" name="coupons[<?php echo $coupon['coupon_history_id'];?>][reward_type_id]" type="hidden" value="<?php echo $coupon['reward_type_id'];?>" />
                     </td>
                     	<td>
                <div class="input-group"><span class="input-group-addon"><?php echo number_format($coupon_value,0,",",".");?></span>
                        <input disabled="disabled" name="coupons[<?php echo $coupon['coupon_history_id'];?>][coupon_code]" value="<?php if(!empty($coupon['message'])){?><?php echo $coupon['message'];?><?php } ?>" type="text" class="form-control"/>
                 </div>  
                       
                        </td>
                     	<td class="text-center">
                        <i class="fa fa-<?php echo (in_array($coupon['coupon_status_id'],$valid))?'check text-success':'times text-error';?>"></i>
                     </td>
                  </tr>
                  
                 <?php } ?>
                 <?php } ?>
               
             <?php } ?>
             
                    </tbody>
             <?php } ?>
             <?php } ?>
             <?php } ?>
             
             </table>
                
             
			</div>
             
   
            
      </div>

      <div class="box-footer clearfix" id="reward_note">
      
      <div class="col-sm-9 col-xs-12" id="reward_message">
 
      </div>
      <div class="col-sm-3 col-xs-12">
              
                
              </div><!-- //col--> 
              
              
            </div>