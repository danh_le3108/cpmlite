<section class="clearfix">
<style>
.thumbnail{position:relative; margin:0 auto;}
.thumbnails .thumbnail{
	max-width:300px;
}
.thumbnails img,
.thumbnails .form-control{
	margin-bottom:5px;
}
.del-img{position:absolute; right:0; top:0;}
.alert.alert-sm{padding: 5px;
    margin-bottom: 0px;}
.table td, .table th {
    padding: 5px !important;
}
.table td .checkbox,
.table td .radio {
	margin-top: 3px !important; 
    margin-bottom: 3px !important;
}
</style>
<!-- Your Page Content Here -->
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
     <!--    <button type="submit" form="form-country" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>--> 
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_back; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      
    </div>
  </div>
  <div class="container-fluid">
  
        <form method="post" enctype="multipart/form-data" id="form-country">
 
    
    
    <div class="row" id="plan_store">
      	<?php echo $plan_store;?>
    </div><!-- //row--> 
    
   
   
        <div class="row">
    <div class="col-sm-6">
    
  <?php foreach($plan_surveys as $survey_id => $plan_survey){?>
        <div class="box box-info" id="plan_survey<?php echo $survey_id;?>">
             <?php echo isset($plan_survey)?$plan_survey:'';?>
        </div><!--//box--> 
    <?php } ?>
    
    
    </div><!-- //col--> 
    <div class="col-sm-6">
    
     <?php if(!empty($config_image_store)){?>
    <div class="box box-info" id="plan_image_store">
         <?php echo isset($plan_image_store)?$plan_image_store:'';?>
    </div><!--//box--> 
    <?php } ?>
    
     <?php if(!empty($config_image_audit)){?>
    <div class="box box-info" id="plan_image_audit">
         <?php echo isset($plan_image_audit)?$plan_image_audit:'';?>
    </div><!--//box--> 
    <?php } ?>
    
    </div><!-- //col--> 
    </div><!-- //row--> 

     
        <div class="row">
    <div class="col-sm-6">
    
     <div class="box box-info" id="plan_coupon">
         <?php echo isset($plan_coupon)?$plan_coupon:'';?>
    </div><!--//box--> 
    
    </div><!-- //col--> 
    <div class="col-sm-6">
    
     <?php if(!empty($config_image_coupon)){?>
    <div class="box box-info" id="plan_image_coupon">
         <?php echo isset($plan_image_coupon)?$plan_image_coupon:'';?>
    </div><!--//box--> 
    <?php } ?>
    
    </div><!-- //col--> 
    </div><!-- //row--> 
    
        </form>
  </div>
</div>
</section>
    <!-- /.content -->


<script type="text/javascript"><!--
$(document).ready(function() {
	$(".survey_radio").click(function () {
			var data_group = $(this).data('group');
			$('input[data-group='+data_group+']').prop("checked", false);
			$(this).prop("checked", true);
	});
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a.img',
		gallery: {
			enabled:true
		}
	});
});
//--></script>