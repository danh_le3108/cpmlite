
<style>
.remove-img{margin-bottom:20px;
}
</style>
 <div class="box-header">
        <h3 class="box-title"><?php echo $heading_title; ?></h3>
      </div>
      <div class="box-body">
      <?php if($route_image=='image_coupon'&&$isNationwide==1||$route_image!='image_coupon'){?>
   <?php
 $image_ignore = !is_array($image_ignore)?array():$image_ignore; 
   ?>   
<div class="thumbnails">
<div class="row">
<?php foreach($images as $image){?>
<?php if(!empty($config_image)&&!in_array($image['image_type_id'],$image_ignore)&&in_array($image['image_type_id'],$config_image)||$image['image_type_id']==0){?>
<div class="col-sm-6 col-xs-6 text-center remove-img">
                              <div class="thumbnail">
<a class="img" href="<?php echo $image['popup'];?>" title="<?php echo $image['filename'];?>" id="img<?php echo $image['image_id'];?>" data-id="<?php echo $image['image_id'];?>" data-filename="<?php echo $image['filename'];?>">
                              <img class="lazy" src="<?php echo $image['thumb'];?>" alt="<?php echo $image['filename'];?>" title="<?php echo $image['filename'];?>">
                              </a>
          <?php foreach($image_types as $type){?>
          <?php echo ($type['image_type_id']==$image['image_type_id'])?$type['image_type']:'';?>
            <?php } ?>
                              </div>
                              
                  </div>
<?php } ?>
<?php } ?>
</div>
</div>
<?php }else{ ?>

          <div class="btn btn-block btn-default btn-sm"><i class="fa fa-exclamation-circle"></i> Hình ảnh được bảo mật!</div>

<?php } ?>
</div>

<script type="text/javascript"><!--
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a.img',
		gallery: {
			enabled:true
		}
	});
//--></script>