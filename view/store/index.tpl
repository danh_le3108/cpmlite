<div class="page-header">
  <div class="container-fluid">
    <div class="pull-right">
      <?php if($hasAdd) { ?>
      <a href="index.php?route=store/store/add" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
      <?php } ?>
    </div>
  </div>
</div>
<div class="container-fluid" style="margin-top: 20px">
  <?php if (!empty($success)) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if (!empty($error_warning)) { ?>
    <div class="alert alert-danger"><i class="fa fa-check-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
  <?php } ?>
</div>
<div class="container-fluid" id="filter-area">
  <div class="box box-info">
    <div class="panel-body">
      <div class="row">
          <div class="col-sm-12">
            <button type="button" id="button-clear" class="btn btn-primary btn-sm pull-right" autocomplete="off"><i class="fa fa-eraser"></i><?=$button_clear?></button>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label class="control-label" for="input-filter_global"><?php echo $text_filter; ?></label>
              <input type="text" name="filter_global" value="<?=isset($filter_global) ? $filter_global : ''?>" placeholder="<?php echo $text_filter; ?>" id="input-filter_global" class="form-control filter" onchange="javascript:filter()"/>
            </div>
          </div>
         <!--  <div class="col-sm-3">
            <div class="form-group">
              <label class="control-label" for="input-region_code"><?=$text_region?></label>
              <select name="filter_region_code" id="input-region_code" class="form-control filter" onchange="javascript:filter()">
                <option value="*"><?php echo $text_select; ?></option>
                <?php foreach($regions as $r) { ?>
                <option value="<?=$r['region_code']?>"><?=$r['region_code']?></option>
                <?php }?>
              </select>
            </div>
          </div> -->
          <div class="col-sm-3">
            <div class="form-group">
              <label class="control-label" for="input-province_id"><?=$text_province?></label>
              <select name="filter_province_id" id="input-province_id" class="form-control filter chosen" onchange="javascript:filter()">
                <option value="*"><?php echo $text_select; ?></option>
                <?php foreach($provinces as $p) { ?>
                <option value="<?=$p['province_id']?>"><?=$p['name']?></option>
                <?php }?>
              </select>
            </div>
          </div>
          <!-- <div class="col-sm-3">
            <div class="form-group">
              <label class="control-label" for="input-chanel">Kênh</label>
              <select name="filter_chanel" id="input-chanel" class="form-control filter" onchange="javascript:filter()">
                <option value="*"><?php echo $text_select; ?></option>
                <?php //foreach($chanels as $p) { ?>
                <option value=""></option>
                <?php // }?>
              </select>
            </div>
          </div> -->
          <div class="col-sm-3">
            <div class="form-group">
              <label class="control-label" for="input-store_type_id">Loại cửa hàng</label>
              <select name="filter_store_type_id" id="input-store_type_id" class="form-control filter" onchange="javascript:filter()">
                <option value="*"><?php echo $text_select; ?></option>
                <?php foreach($store_types as $p) { ?>
                <option value="<?=$p['store_type_id']?>"><?=$p['type_name']?></option>
                <?php }?>
              </select>
            </div>
          </div>
          <div class="clearfix">
            <div class="col-sm-12"><?php echo $import_store; ?></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="ajax_list"></div>
<div id="modal-confirm" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        Bạn có chắc chắn muốn xóa Store <span></span>?
      </div>
      <div class="modal-body">
        <div class="mpadding">
          Store này không thể khôi phục nhưng lịch sử sẽ được lưu lại.
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success pull-right" data-dismiss="modal" aria-hidden="true">Không</button>
        <a id="btn-modal-confirm" class="btn btn-danger pull-right" style="margin-right:15px;">Xác nhận Xóa</a>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
  $(document).ready(function(){
    var url = '<?=str_replace("amp;","",$url)?>';
    $('#ajax_list').load(url);
    $('.img-popup').magnificPopup({
      type:'image',
      delegate: 'a.img-thumbnail',
      gallery: {
        enabled:true
      }
    });

    <?php if(!empty($filter_global)){?>
    $("td span:contains(<?php echo $filter_global;?>)").css({"color":"red","background":"yellow"});
    <?php } ?>
  });

  $('input[name=\'filter_global\']').autocomplete({
    'source': function(request, response) {
      $.ajax({
        url: 'index.php?route=store/store/autocomplete&filter_global=' +  encodeURIComponent(request),
        dataType: 'json',
        success: function(json) {
          response($.map(json, function(item) {
            return {
              label: item['store_code'] + ' - ' + item['store_name'],
              value: item['store_id']
            }
          }));
        }
      });
    },
    'select': function(item) {
      $('input[name=\'filter_global\']').val(item['label']);
    }
  });

  function deleteStore(store_id,store_name){
      $('#modal-confirm .modal-header span').html(store_name);
      $('#modal-confirm').modal('show');
      $('#btn-modal-confirm').click(function(event) {
        event.preventDefault();
        $('#btn-modal-confirm').unbind('click');
        $.get('index.php?route=store/store/delete&store_id='+store_id, function(data){
          $('#modal-confirm').modal('hide');
          $('#store_id_'+store_id).remove();
        })
      });
  }
</script>