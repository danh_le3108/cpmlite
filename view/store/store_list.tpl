<div class="container-fluid">
  <div class="row">
    <div class="col-sm-7"><?php echo $pagination;?> </div>
    <div class="col-sm-5 text-right"><?php echo $results;?></div>
  </div>
  <div class="box box-info">
    <div class="box-header">
      <h3 class="box-title"><?php echo $heading_title;?></h3>
    </div>
    <div class="box-body table-responsive no-padding">
      <div class="panel-body">
        <table class="table table-hover table table-bordered">
          <thead>
            <tr>
              <th class="text-center" width="1">ID</th>
              <!-- <th>Mã DMS</th> -->
              <th><?php echo $text_store_info;?></th>
              <th>Loại CH</th>
              <th class="text-center" width="15%">Tỉnh thành</th> 
              <th class="text-center" width="15%"><?php echo $text_date_added;?></th>
              <?php if($hasDel) {?>
              <th width="5%" class="text-center"> Xóa </th>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <?php  foreach($stores as $store){
              $filename = DIR_MEDIA . $store['store_image'];
              if (is_file($filename)) {
                $avatar = $model_tool_image->resize($store['store_image'], 60, 60);
                $popup = HTTP_SERVER.'media/'.$store['store_image'];
              }else{
                $avatar = $thumb;
                $popup = '';
              }
            ?>
            <tr class="tr_info" id="store_id_<?=$store['store_id']?>">
              <td class="img-popup text-center"><a href="<?=$popup?>" class="img-thumbnail"><img src="<?= $avatar ?>" alt=""/></a><br/><?=$store['store_id']?></td>
              
              <td><a target="_blank" href="index.php?route=store/store/edit&store_id=<?=$store['store_id']?>"><span><?php echo $store['store_code'];?> - <?php echo $store['store_name'];?></span></a><br />
              <?php echo $store['store_address_raw'];?><br>
              SĐT: <?=$store['store_phone']?>
              </td>
              <td><?= $store['store_type'];?></td>
              <td class="text-center"><?php echo $store['store_province'];?></td>
              <td class="text-center"><?php echo $store['date_added'];?></td>
              <?php if($hasDel) {?>
              <td class="text-center">
                <a class="btn" style="color:#F00;" onclick="deleteStore('<?php echo $store['store_id']; ?>','<?php echo $store['store_name']; ?>')"><i class="fa fa-trash"></i></a>
              </td>
              <?php } ?>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-7"><?php echo $pagination;?> </div>
    <div class="col-sm-5 text-right"><?php echo $results;?></div>
  </div>
</div>